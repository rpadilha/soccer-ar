﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerable_1_t7945_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ObliqueNear>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ObliqueNear>
extern Il2CppType IEnumerator_1_t6212_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44464_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ObliqueNear>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44464_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7945_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6212_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44464_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7945_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44464_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
static TypeInfo* IEnumerable_1_t7945_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7945_0_0_0;
extern Il2CppType IEnumerable_1_t7945_1_0_0;
struct IEnumerable_1_t7945;
extern Il2CppGenericClass IEnumerable_1_t7945_GenericClass;
TypeInfo IEnumerable_1_t7945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7945_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7945_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7945_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7945_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7945_0_0_0/* byval_arg */
	, &IEnumerable_1_t7945_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7944_il2cpp_TypeInfo;

// ObliqueNear
#include "AssemblyU2DUnityScript_ObliqueNear.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"


// System.Int32 System.Collections.Generic.IList`1<ObliqueNear>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ObliqueNear>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ObliqueNear>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ObliqueNear>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ObliqueNear>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ObliqueNear>
extern MethodInfo IList_1_get_Item_m44465_MethodInfo;
extern MethodInfo IList_1_set_Item_m44466_MethodInfo;
static PropertyInfo IList_1_t7944____Item_PropertyInfo = 
{
	&IList_1_t7944_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44465_MethodInfo/* get */
	, &IList_1_set_Item_m44466_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7944_PropertyInfos[] =
{
	&IList_1_t7944____Item_PropertyInfo,
	NULL
};
extern Il2CppType ObliqueNear_t215_0_0_0;
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo IList_1_t7944_IList_1_IndexOf_m44467_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44467_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ObliqueNear>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44467_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7944_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7944_IList_1_IndexOf_m44467_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44467_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo IList_1_t7944_IList_1_Insert_m44468_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44468_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ObliqueNear>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44468_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7944_IList_1_Insert_m44468_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44468_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7944_IList_1_RemoveAt_m44469_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44469_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ObliqueNear>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44469_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7944_IList_1_RemoveAt_m44469_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44469_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7944_IList_1_get_Item_m44465_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ObliqueNear_t215_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44465_GenericMethod;
// T System.Collections.Generic.IList`1<ObliqueNear>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44465_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7944_il2cpp_TypeInfo/* declaring_type */
	, &ObliqueNear_t215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7944_IList_1_get_Item_m44465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44465_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo IList_1_t7944_IList_1_set_Item_m44466_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44466_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ObliqueNear>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44466_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7944_IList_1_set_Item_m44466_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44466_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7944_MethodInfos[] =
{
	&IList_1_IndexOf_m44467_MethodInfo,
	&IList_1_Insert_m44468_MethodInfo,
	&IList_1_RemoveAt_m44469_MethodInfo,
	&IList_1_get_Item_m44465_MethodInfo,
	&IList_1_set_Item_m44466_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t7943_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7944_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7943_il2cpp_TypeInfo,
	&IEnumerable_1_t7945_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7944_0_0_0;
extern Il2CppType IList_1_t7944_1_0_0;
struct IList_1_t7944;
extern Il2CppGenericClass IList_1_t7944_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7944_MethodInfos/* methods */
	, IList_1_t7944_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7944_il2cpp_TypeInfo/* element_class */
	, IList_1_t7944_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7944_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7944_0_0_0/* byval_arg */
	, &IList_1_t7944_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_62.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3135_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_62MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Events.InvokableCall`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_58.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3136_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_58MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15935_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15937_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3135____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3135_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3135, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3135_FieldInfos[] =
{
	&CachedInvokableCall_1_t3135____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3135_CachedInvokableCall_1__ctor_m15933_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15933_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15933_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3135_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3135_CachedInvokableCall_1__ctor_m15933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15933_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3135_CachedInvokableCall_1_Invoke_m15934_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15934_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15934_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3135_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3135_CachedInvokableCall_1_Invoke_m15934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15934_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3135_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15933_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15934_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m15934_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15938_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3135_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15934_MethodInfo,
	&InvokableCall_1_Find_m15938_MethodInfo,
};
extern Il2CppType UnityAction_1_t3137_0_0_0;
extern TypeInfo UnityAction_1_t3137_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisObliqueNear_t215_m34001_MethodInfo;
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15940_MethodInfo;
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3135_RGCTXData[8] = 
{
	&UnityAction_1_t3137_0_0_0/* Type Usage */,
	&UnityAction_1_t3137_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObliqueNear_t215_m34001_MethodInfo/* Method Usage */,
	&ObliqueNear_t215_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15940_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15935_MethodInfo/* Method Usage */,
	&ObliqueNear_t215_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15937_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3135_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3135_1_0_0;
struct CachedInvokableCall_1_t3135;
extern Il2CppGenericClass CachedInvokableCall_1_t3135_GenericClass;
TypeInfo CachedInvokableCall_1_t3135_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3135_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3135_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3135_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3135_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3135_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3135_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3135_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3135_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3135_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3135)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_65.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo UnityAction_1_t3137_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_65MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ObliqueNear>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ObliqueNear>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisObliqueNear_t215_m34001(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<ObliqueNear>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<ObliqueNear>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<ObliqueNear>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<ObliqueNear>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<ObliqueNear>
extern Il2CppType UnityAction_1_t3137_0_0_1;
FieldInfo InvokableCall_1_t3136____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3137_0_0_1/* type */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3136, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3136_FieldInfos[] =
{
	&InvokableCall_1_t3136____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3136_InvokableCall_1__ctor_m15935_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15935_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ObliqueNear>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15935_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3136_InvokableCall_1__ctor_m15935_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15935_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3137_0_0_0;
static ParameterInfo InvokableCall_1_t3136_InvokableCall_1__ctor_m15936_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3137_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15936_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ObliqueNear>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15936_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3136_InvokableCall_1__ctor_m15936_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15936_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3136_InvokableCall_1_Invoke_m15937_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15937_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ObliqueNear>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15937_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3136_InvokableCall_1_Invoke_m15937_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15937_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3136_InvokableCall_1_Find_m15938_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15938_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<ObliqueNear>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15938_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3136_InvokableCall_1_Find_m15938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15938_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3136_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15935_MethodInfo,
	&InvokableCall_1__ctor_m15936_MethodInfo,
	&InvokableCall_1_Invoke_m15937_MethodInfo,
	&InvokableCall_1_Find_m15938_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3136_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15937_MethodInfo,
	&InvokableCall_1_Find_m15938_MethodInfo,
};
extern TypeInfo UnityAction_1_t3137_il2cpp_TypeInfo;
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3136_RGCTXData[5] = 
{
	&UnityAction_1_t3137_0_0_0/* Type Usage */,
	&UnityAction_1_t3137_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObliqueNear_t215_m34001_MethodInfo/* Method Usage */,
	&ObliqueNear_t215_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15940_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3136_0_0_0;
extern Il2CppType InvokableCall_1_t3136_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t3136;
extern Il2CppGenericClass InvokableCall_1_t3136_GenericClass;
TypeInfo InvokableCall_1_t3136_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3136_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3136_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3136_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3136_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3136_0_0_0/* byval_arg */
	, &InvokableCall_1_t3136_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3136_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3136_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3136)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<ObliqueNear>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<ObliqueNear>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ObliqueNear>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<ObliqueNear>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<ObliqueNear>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3137_UnityAction_1__ctor_m15939_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15939_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ObliqueNear>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15939_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3137_UnityAction_1__ctor_m15939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15939_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo UnityAction_1_t3137_UnityAction_1_Invoke_m15940_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15940_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ObliqueNear>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15940_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3137_UnityAction_1_Invoke_m15940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15940_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNear_t215_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3137_UnityAction_1_BeginInvoke_m15941_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15941_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ObliqueNear>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15941_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3137_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3137_UnityAction_1_BeginInvoke_m15941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15941_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3137_UnityAction_1_EndInvoke_m15942_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15942_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ObliqueNear>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15942_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3137_UnityAction_1_EndInvoke_m15942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15942_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3137_MethodInfos[] =
{
	&UnityAction_1__ctor_m15939_MethodInfo,
	&UnityAction_1_Invoke_m15940_MethodInfo,
	&UnityAction_1_BeginInvoke_m15941_MethodInfo,
	&UnityAction_1_EndInvoke_m15942_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15941_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15942_MethodInfo;
static MethodInfo* UnityAction_1_t3137_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15940_MethodInfo,
	&UnityAction_1_BeginInvoke_m15941_MethodInfo,
	&UnityAction_1_EndInvoke_m15942_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t3137_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3137_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t3137;
extern Il2CppGenericClass UnityAction_1_t3137_GenericClass;
TypeInfo UnityAction_1_t3137_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3137_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3137_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3137_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3137_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3137_0_0_0/* byval_arg */
	, &UnityAction_1_t3137_1_0_0/* this_arg */
	, UnityAction_1_t3137_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3137_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3137)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_17.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3138_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_17MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Camera>
extern Il2CppType Camera_t168_0_0_6;
FieldInfo CastHelper_1_t3138____t_0_FieldInfo = 
{
	"t"/* name */
	, &Camera_t168_0_0_6/* type */
	, &CastHelper_1_t3138_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3138, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3138____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3138_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3138, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3138_FieldInfos[] =
{
	&CastHelper_1_t3138____t_0_FieldInfo,
	&CastHelper_1_t3138____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3138_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* CastHelper_1_t3138_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3138_0_0_0;
extern Il2CppType CastHelper_1_t3138_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass CastHelper_1_t3138_GenericClass;
TypeInfo CastHelper_1_t3138_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3138_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3138_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3138_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3138_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3138_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3138_0_0_0/* byval_arg */
	, &CastHelper_1_t3138_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3138_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3138)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6214_il2cpp_TypeInfo;

// PlayerRelativeControl
#include "AssemblyU2DUnityScript_PlayerRelativeControl.h"


// T System.Collections.Generic.IEnumerator`1<PlayerRelativeControl>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<PlayerRelativeControl>
extern MethodInfo IEnumerator_1_get_Current_m44470_MethodInfo;
static PropertyInfo IEnumerator_1_t6214____Current_PropertyInfo = 
{
	&IEnumerator_1_t6214_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6214_PropertyInfos[] =
{
	&IEnumerator_1_t6214____Current_PropertyInfo,
	NULL
};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44470_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<PlayerRelativeControl>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44470_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6214_il2cpp_TypeInfo/* declaring_type */
	, &PlayerRelativeControl_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44470_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6214_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44470_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6214_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6214_0_0_0;
extern Il2CppType IEnumerator_1_t6214_1_0_0;
struct IEnumerator_1_t6214;
extern Il2CppGenericClass IEnumerator_1_t6214_GenericClass;
TypeInfo IEnumerator_1_t6214_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6214_MethodInfos/* methods */
	, IEnumerator_1_t6214_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6214_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6214_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6214_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6214_0_0_0/* byval_arg */
	, &IEnumerator_1_t6214_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6214_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<PlayerRelativeControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_150.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3139_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<PlayerRelativeControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_150MethodDeclarations.h"

// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15947_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPlayerRelativeControl_t217_m34003_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<PlayerRelativeControl>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<PlayerRelativeControl>(System.Int32)
#define Array_InternalArray__get_Item_TisPlayerRelativeControl_t217_m34003(__this, p0, method) (PlayerRelativeControl_t217 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<PlayerRelativeControl>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<PlayerRelativeControl>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<PlayerRelativeControl>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<PlayerRelativeControl>::MoveNext()
// T System.Array/InternalEnumerator`1<PlayerRelativeControl>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<PlayerRelativeControl>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3139____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3139, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3139____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3139, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3139_FieldInfos[] =
{
	&InternalEnumerator_1_t3139____array_0_FieldInfo,
	&InternalEnumerator_1_t3139____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3139____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3139_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3139____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3139_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3139_PropertyInfos[] =
{
	&InternalEnumerator_1_t3139____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3139____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3139_InternalEnumerator_1__ctor_m15943_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15943_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<PlayerRelativeControl>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15943_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3139_InternalEnumerator_1__ctor_m15943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15943_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<PlayerRelativeControl>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15945_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<PlayerRelativeControl>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15945_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15945_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15946_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<PlayerRelativeControl>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15946_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15946_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15947_GenericMethod;
// T System.Array/InternalEnumerator`1<PlayerRelativeControl>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15947_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* declaring_type */
	, &PlayerRelativeControl_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15947_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3139_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15943_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_MethodInfo,
	&InternalEnumerator_1_Dispose_m15945_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15946_MethodInfo,
	&InternalEnumerator_1_get_Current_m15947_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15946_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15945_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3139_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15944_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15946_MethodInfo,
	&InternalEnumerator_1_Dispose_m15945_MethodInfo,
	&InternalEnumerator_1_get_Current_m15947_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3139_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6214_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3139_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6214_il2cpp_TypeInfo, 7},
};
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3139_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15947_MethodInfo/* Method Usage */,
	&PlayerRelativeControl_t217_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPlayerRelativeControl_t217_m34003_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3139_0_0_0;
extern Il2CppType InternalEnumerator_1_t3139_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3139_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t3139_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3139_MethodInfos/* methods */
	, InternalEnumerator_1_t3139_PropertyInfos/* properties */
	, InternalEnumerator_1_t3139_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3139_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3139_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3139_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3139_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3139_1_0_0/* this_arg */
	, InternalEnumerator_1_t3139_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3139_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3139_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3139)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7946_il2cpp_TypeInfo;

#include "Assembly-UnityScript_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<PlayerRelativeControl>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<PlayerRelativeControl>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<PlayerRelativeControl>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<PlayerRelativeControl>
extern MethodInfo ICollection_1_get_Count_m44471_MethodInfo;
static PropertyInfo ICollection_1_t7946____Count_PropertyInfo = 
{
	&ICollection_1_t7946_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44472_MethodInfo;
static PropertyInfo ICollection_1_t7946____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7946_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44472_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7946_PropertyInfos[] =
{
	&ICollection_1_t7946____Count_PropertyInfo,
	&ICollection_1_t7946____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44471_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<PlayerRelativeControl>::get_Count()
MethodInfo ICollection_1_get_Count_m44471_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44471_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44472_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<PlayerRelativeControl>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44472_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44472_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo ICollection_1_t7946_ICollection_1_Add_m44473_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44473_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Add(T)
MethodInfo ICollection_1_Add_m44473_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7946_ICollection_1_Add_m44473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44473_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44474_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Clear()
MethodInfo ICollection_1_Clear_m44474_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44474_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo ICollection_1_t7946_ICollection_1_Contains_m44475_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44475_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Contains(T)
MethodInfo ICollection_1_Contains_m44475_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7946_ICollection_1_Contains_m44475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44475_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControlU5BU5D_t5776_0_0_0;
extern Il2CppType PlayerRelativeControlU5BU5D_t5776_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7946_ICollection_1_CopyTo_m44476_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControlU5BU5D_t5776_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44476_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<PlayerRelativeControl>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44476_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7946_ICollection_1_CopyTo_m44476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44476_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo ICollection_1_t7946_ICollection_1_Remove_m44477_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44477_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<PlayerRelativeControl>::Remove(T)
MethodInfo ICollection_1_Remove_m44477_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7946_ICollection_1_Remove_m44477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44477_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7946_MethodInfos[] =
{
	&ICollection_1_get_Count_m44471_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44472_MethodInfo,
	&ICollection_1_Add_m44473_MethodInfo,
	&ICollection_1_Clear_m44474_MethodInfo,
	&ICollection_1_Contains_m44475_MethodInfo,
	&ICollection_1_CopyTo_m44476_MethodInfo,
	&ICollection_1_Remove_m44477_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7948_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7946_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7948_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7946_0_0_0;
extern Il2CppType ICollection_1_t7946_1_0_0;
struct ICollection_1_t7946;
extern Il2CppGenericClass ICollection_1_t7946_GenericClass;
TypeInfo ICollection_1_t7946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7946_MethodInfos/* methods */
	, ICollection_1_t7946_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7946_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7946_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7946_0_0_0/* byval_arg */
	, &ICollection_1_t7946_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<PlayerRelativeControl>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<PlayerRelativeControl>
extern Il2CppType IEnumerator_1_t6214_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44478_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<PlayerRelativeControl>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44478_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7948_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6214_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44478_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7948_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44478_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7948_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7948_0_0_0;
extern Il2CppType IEnumerable_1_t7948_1_0_0;
struct IEnumerable_1_t7948;
extern Il2CppGenericClass IEnumerable_1_t7948_GenericClass;
TypeInfo IEnumerable_1_t7948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7948_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7948_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7948_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7948_0_0_0/* byval_arg */
	, &IEnumerable_1_t7948_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7947_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<PlayerRelativeControl>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<PlayerRelativeControl>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<PlayerRelativeControl>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<PlayerRelativeControl>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<PlayerRelativeControl>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<PlayerRelativeControl>
extern MethodInfo IList_1_get_Item_m44479_MethodInfo;
extern MethodInfo IList_1_set_Item_m44480_MethodInfo;
static PropertyInfo IList_1_t7947____Item_PropertyInfo = 
{
	&IList_1_t7947_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44479_MethodInfo/* get */
	, &IList_1_set_Item_m44480_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7947_PropertyInfos[] =
{
	&IList_1_t7947____Item_PropertyInfo,
	NULL
};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo IList_1_t7947_IList_1_IndexOf_m44481_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44481_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<PlayerRelativeControl>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44481_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7947_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7947_IList_1_IndexOf_m44481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44481_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo IList_1_t7947_IList_1_Insert_m44482_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44482_GenericMethod;
// System.Void System.Collections.Generic.IList`1<PlayerRelativeControl>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44482_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7947_IList_1_Insert_m44482_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44482_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7947_IList_1_RemoveAt_m44483_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44483_GenericMethod;
// System.Void System.Collections.Generic.IList`1<PlayerRelativeControl>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44483_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7947_IList_1_RemoveAt_m44483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44483_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7947_IList_1_get_Item_m44479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44479_GenericMethod;
// T System.Collections.Generic.IList`1<PlayerRelativeControl>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44479_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7947_il2cpp_TypeInfo/* declaring_type */
	, &PlayerRelativeControl_t217_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7947_IList_1_get_Item_m44479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44479_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo IList_1_t7947_IList_1_set_Item_m44480_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44480_GenericMethod;
// System.Void System.Collections.Generic.IList`1<PlayerRelativeControl>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44480_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7947_IList_1_set_Item_m44480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44480_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7947_MethodInfos[] =
{
	&IList_1_IndexOf_m44481_MethodInfo,
	&IList_1_Insert_m44482_MethodInfo,
	&IList_1_RemoveAt_m44483_MethodInfo,
	&IList_1_get_Item_m44479_MethodInfo,
	&IList_1_set_Item_m44480_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7947_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7946_il2cpp_TypeInfo,
	&IEnumerable_1_t7948_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7947_0_0_0;
extern Il2CppType IList_1_t7947_1_0_0;
struct IList_1_t7947;
extern Il2CppGenericClass IList_1_t7947_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7947_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7947_MethodInfos/* methods */
	, IList_1_t7947_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7947_il2cpp_TypeInfo/* element_class */
	, IList_1_t7947_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7947_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7947_0_0_0/* byval_arg */
	, &IList_1_t7947_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7947_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_63.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3140_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_63MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_59.h"
extern TypeInfo InvokableCall_1_t3141_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_59MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15950_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15952_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3140____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3140_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3140, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3140_FieldInfos[] =
{
	&CachedInvokableCall_1_t3140____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3140_CachedInvokableCall_1__ctor_m15948_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15948_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15948_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3140_CachedInvokableCall_1__ctor_m15948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15948_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3140_CachedInvokableCall_1_Invoke_m15949_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15949_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15949_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3140_CachedInvokableCall_1_Invoke_m15949_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15949_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3140_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15948_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15949_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15949_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15953_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3140_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15949_MethodInfo,
	&InvokableCall_1_Find_m15953_MethodInfo,
};
extern Il2CppType UnityAction_1_t3142_0_0_0;
extern TypeInfo UnityAction_1_t3142_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisPlayerRelativeControl_t217_m34013_MethodInfo;
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15955_MethodInfo;
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3140_RGCTXData[8] = 
{
	&UnityAction_1_t3142_0_0_0/* Type Usage */,
	&UnityAction_1_t3142_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPlayerRelativeControl_t217_m34013_MethodInfo/* Method Usage */,
	&PlayerRelativeControl_t217_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15955_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15950_MethodInfo/* Method Usage */,
	&PlayerRelativeControl_t217_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15952_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3140_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3140_1_0_0;
struct CachedInvokableCall_1_t3140;
extern Il2CppGenericClass CachedInvokableCall_1_t3140_GenericClass;
TypeInfo CachedInvokableCall_1_t3140_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3140_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3140_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3140_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3140_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3140_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3140_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3140_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3140_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3140_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3140)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_66.h"
extern TypeInfo UnityAction_1_t3142_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_66MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<PlayerRelativeControl>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<PlayerRelativeControl>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisPlayerRelativeControl_t217_m34013(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>
extern Il2CppType UnityAction_1_t3142_0_0_1;
FieldInfo InvokableCall_1_t3141____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3142_0_0_1/* type */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3141, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3141_FieldInfos[] =
{
	&InvokableCall_1_t3141____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3141_InvokableCall_1__ctor_m15950_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15950_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15950_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3141_InvokableCall_1__ctor_m15950_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15950_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3142_0_0_0;
static ParameterInfo InvokableCall_1_t3141_InvokableCall_1__ctor_m15951_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3142_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15951_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15951_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3141_InvokableCall_1__ctor_m15951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15951_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3141_InvokableCall_1_Invoke_m15952_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15952_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15952_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3141_InvokableCall_1_Invoke_m15952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15952_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3141_InvokableCall_1_Find_m15953_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15953_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15953_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3141_InvokableCall_1_Find_m15953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15953_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3141_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15950_MethodInfo,
	&InvokableCall_1__ctor_m15951_MethodInfo,
	&InvokableCall_1_Invoke_m15952_MethodInfo,
	&InvokableCall_1_Find_m15953_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3141_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15952_MethodInfo,
	&InvokableCall_1_Find_m15953_MethodInfo,
};
extern TypeInfo UnityAction_1_t3142_il2cpp_TypeInfo;
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3141_RGCTXData[5] = 
{
	&UnityAction_1_t3142_0_0_0/* Type Usage */,
	&UnityAction_1_t3142_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPlayerRelativeControl_t217_m34013_MethodInfo/* Method Usage */,
	&PlayerRelativeControl_t217_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15955_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3141_0_0_0;
extern Il2CppType InvokableCall_1_t3141_1_0_0;
struct InvokableCall_1_t3141;
extern Il2CppGenericClass InvokableCall_1_t3141_GenericClass;
TypeInfo InvokableCall_1_t3141_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3141_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3141_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3141_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3141_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3141_0_0_0/* byval_arg */
	, &InvokableCall_1_t3141_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3141_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3141_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3141)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<PlayerRelativeControl>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3142_UnityAction_1__ctor_m15954_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15954_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15954_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3142_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3142_UnityAction_1__ctor_m15954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15954_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
static ParameterInfo UnityAction_1_t3142_UnityAction_1_Invoke_m15955_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15955_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15955_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3142_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3142_UnityAction_1_Invoke_m15955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15955_GenericMethod/* genericMethod */

};
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3142_UnityAction_1_BeginInvoke_m15956_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PlayerRelativeControl_t217_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15956_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15956_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3142_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3142_UnityAction_1_BeginInvoke_m15956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15956_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3142_UnityAction_1_EndInvoke_m15957_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15957_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<PlayerRelativeControl>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15957_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3142_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3142_UnityAction_1_EndInvoke_m15957_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15957_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3142_MethodInfos[] =
{
	&UnityAction_1__ctor_m15954_MethodInfo,
	&UnityAction_1_Invoke_m15955_MethodInfo,
	&UnityAction_1_BeginInvoke_m15956_MethodInfo,
	&UnityAction_1_EndInvoke_m15957_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15956_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15957_MethodInfo;
static MethodInfo* UnityAction_1_t3142_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15955_MethodInfo,
	&UnityAction_1_BeginInvoke_m15956_MethodInfo,
	&UnityAction_1_EndInvoke_m15957_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3142_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3142_1_0_0;
struct UnityAction_1_t3142;
extern Il2CppGenericClass UnityAction_1_t3142_GenericClass;
TypeInfo UnityAction_1_t3142_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3142_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3142_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3142_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3142_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3142_0_0_0/* byval_arg */
	, &UnityAction_1_t3142_1_0_0/* this_arg */
	, UnityAction_1_t3142_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3142_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3142)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6216_il2cpp_TypeInfo;

// RollABall
#include "AssemblyU2DUnityScript_RollABall.h"


// T System.Collections.Generic.IEnumerator`1<RollABall>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<RollABall>
extern MethodInfo IEnumerator_1_get_Current_m44484_MethodInfo;
static PropertyInfo IEnumerator_1_t6216____Current_PropertyInfo = 
{
	&IEnumerator_1_t6216_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44484_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6216_PropertyInfos[] =
{
	&IEnumerator_1_t6216____Current_PropertyInfo,
	NULL
};
extern Il2CppType RollABall_t218_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44484_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<RollABall>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44484_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6216_il2cpp_TypeInfo/* declaring_type */
	, &RollABall_t218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44484_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6216_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44484_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6216_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6216_0_0_0;
extern Il2CppType IEnumerator_1_t6216_1_0_0;
struct IEnumerator_1_t6216;
extern Il2CppGenericClass IEnumerator_1_t6216_GenericClass;
TypeInfo IEnumerator_1_t6216_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6216_MethodInfos/* methods */
	, IEnumerator_1_t6216_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6216_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6216_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6216_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6216_0_0_0/* byval_arg */
	, &IEnumerator_1_t6216_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6216_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<RollABall>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_151.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3143_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<RollABall>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_151MethodDeclarations.h"

extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15962_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRollABall_t218_m34015_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<RollABall>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<RollABall>(System.Int32)
#define Array_InternalArray__get_Item_TisRollABall_t218_m34015(__this, p0, method) (RollABall_t218 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<RollABall>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<RollABall>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<RollABall>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<RollABall>::MoveNext()
// T System.Array/InternalEnumerator`1<RollABall>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<RollABall>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3143____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3143, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3143____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3143, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3143_FieldInfos[] =
{
	&InternalEnumerator_1_t3143____array_0_FieldInfo,
	&InternalEnumerator_1_t3143____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3143____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3143_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3143____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3143_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15962_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3143_PropertyInfos[] =
{
	&InternalEnumerator_1_t3143____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3143____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3143_InternalEnumerator_1__ctor_m15958_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15958_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<RollABall>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15958_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3143_InternalEnumerator_1__ctor_m15958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15958_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<RollABall>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15960_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<RollABall>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15960_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15960_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15961_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<RollABall>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15961_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15961_GenericMethod/* genericMethod */

};
extern Il2CppType RollABall_t218_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15962_GenericMethod;
// T System.Array/InternalEnumerator`1<RollABall>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15962_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* declaring_type */
	, &RollABall_t218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15962_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3143_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15958_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_MethodInfo,
	&InternalEnumerator_1_Dispose_m15960_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15961_MethodInfo,
	&InternalEnumerator_1_get_Current_m15962_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15961_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15960_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3143_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15959_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15961_MethodInfo,
	&InternalEnumerator_1_Dispose_m15960_MethodInfo,
	&InternalEnumerator_1_get_Current_m15962_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3143_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6216_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3143_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6216_il2cpp_TypeInfo, 7},
};
extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3143_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15962_MethodInfo/* Method Usage */,
	&RollABall_t218_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRollABall_t218_m34015_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3143_0_0_0;
extern Il2CppType InternalEnumerator_1_t3143_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3143_GenericClass;
TypeInfo InternalEnumerator_1_t3143_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3143_MethodInfos/* methods */
	, InternalEnumerator_1_t3143_PropertyInfos/* properties */
	, InternalEnumerator_1_t3143_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3143_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3143_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3143_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3143_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3143_1_0_0/* this_arg */
	, InternalEnumerator_1_t3143_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3143_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3143_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3143)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7949_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<RollABall>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<RollABall>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<RollABall>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<RollABall>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<RollABall>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<RollABall>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<RollABall>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<RollABall>
extern MethodInfo ICollection_1_get_Count_m44485_MethodInfo;
static PropertyInfo ICollection_1_t7949____Count_PropertyInfo = 
{
	&ICollection_1_t7949_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44485_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44486_MethodInfo;
static PropertyInfo ICollection_1_t7949____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7949_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44486_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7949_PropertyInfos[] =
{
	&ICollection_1_t7949____Count_PropertyInfo,
	&ICollection_1_t7949____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44485_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<RollABall>::get_Count()
MethodInfo ICollection_1_get_Count_m44485_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44485_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44486_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<RollABall>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44486_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44486_GenericMethod/* genericMethod */

};
extern Il2CppType RollABall_t218_0_0_0;
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo ICollection_1_t7949_ICollection_1_Add_m44487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44487_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<RollABall>::Add(T)
MethodInfo ICollection_1_Add_m44487_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7949_ICollection_1_Add_m44487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44487_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44488_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<RollABall>::Clear()
MethodInfo ICollection_1_Clear_m44488_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44488_GenericMethod/* genericMethod */

};
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo ICollection_1_t7949_ICollection_1_Contains_m44489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44489_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<RollABall>::Contains(T)
MethodInfo ICollection_1_Contains_m44489_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7949_ICollection_1_Contains_m44489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44489_GenericMethod/* genericMethod */

};
extern Il2CppType RollABallU5BU5D_t5777_0_0_0;
extern Il2CppType RollABallU5BU5D_t5777_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7949_ICollection_1_CopyTo_m44490_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RollABallU5BU5D_t5777_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44490_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<RollABall>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44490_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7949_ICollection_1_CopyTo_m44490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44490_GenericMethod/* genericMethod */

};
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo ICollection_1_t7949_ICollection_1_Remove_m44491_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44491_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<RollABall>::Remove(T)
MethodInfo ICollection_1_Remove_m44491_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7949_ICollection_1_Remove_m44491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44491_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7949_MethodInfos[] =
{
	&ICollection_1_get_Count_m44485_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44486_MethodInfo,
	&ICollection_1_Add_m44487_MethodInfo,
	&ICollection_1_Clear_m44488_MethodInfo,
	&ICollection_1_Contains_m44489_MethodInfo,
	&ICollection_1_CopyTo_m44490_MethodInfo,
	&ICollection_1_Remove_m44491_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7951_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7949_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7951_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7949_0_0_0;
extern Il2CppType ICollection_1_t7949_1_0_0;
struct ICollection_1_t7949;
extern Il2CppGenericClass ICollection_1_t7949_GenericClass;
TypeInfo ICollection_1_t7949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7949_MethodInfos/* methods */
	, ICollection_1_t7949_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7949_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7949_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7949_0_0_0/* byval_arg */
	, &ICollection_1_t7949_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7949_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<RollABall>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<RollABall>
extern Il2CppType IEnumerator_1_t6216_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44492_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<RollABall>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44492_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7951_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6216_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44492_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7951_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44492_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7951_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7951_0_0_0;
extern Il2CppType IEnumerable_1_t7951_1_0_0;
struct IEnumerable_1_t7951;
extern Il2CppGenericClass IEnumerable_1_t7951_GenericClass;
TypeInfo IEnumerable_1_t7951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7951_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7951_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7951_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7951_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7951_0_0_0/* byval_arg */
	, &IEnumerable_1_t7951_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7951_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7950_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<RollABall>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<RollABall>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<RollABall>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<RollABall>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<RollABall>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<RollABall>
extern MethodInfo IList_1_get_Item_m44493_MethodInfo;
extern MethodInfo IList_1_set_Item_m44494_MethodInfo;
static PropertyInfo IList_1_t7950____Item_PropertyInfo = 
{
	&IList_1_t7950_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44493_MethodInfo/* get */
	, &IList_1_set_Item_m44494_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7950_PropertyInfos[] =
{
	&IList_1_t7950____Item_PropertyInfo,
	NULL
};
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo IList_1_t7950_IList_1_IndexOf_m44495_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44495_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<RollABall>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44495_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7950_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7950_IList_1_IndexOf_m44495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44495_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo IList_1_t7950_IList_1_Insert_m44496_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44496_GenericMethod;
// System.Void System.Collections.Generic.IList`1<RollABall>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44496_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7950_IList_1_Insert_m44496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44496_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7950_IList_1_RemoveAt_m44497_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44497_GenericMethod;
// System.Void System.Collections.Generic.IList`1<RollABall>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44497_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7950_IList_1_RemoveAt_m44497_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44497_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7950_IList_1_get_Item_m44493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType RollABall_t218_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44493_GenericMethod;
// T System.Collections.Generic.IList`1<RollABall>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44493_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7950_il2cpp_TypeInfo/* declaring_type */
	, &RollABall_t218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7950_IList_1_get_Item_m44493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44493_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo IList_1_t7950_IList_1_set_Item_m44494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44494_GenericMethod;
// System.Void System.Collections.Generic.IList`1<RollABall>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44494_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7950_IList_1_set_Item_m44494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44494_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7950_MethodInfos[] =
{
	&IList_1_IndexOf_m44495_MethodInfo,
	&IList_1_Insert_m44496_MethodInfo,
	&IList_1_RemoveAt_m44497_MethodInfo,
	&IList_1_get_Item_m44493_MethodInfo,
	&IList_1_set_Item_m44494_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7950_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7949_il2cpp_TypeInfo,
	&IEnumerable_1_t7951_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7950_0_0_0;
extern Il2CppType IList_1_t7950_1_0_0;
struct IList_1_t7950;
extern Il2CppGenericClass IList_1_t7950_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7950_MethodInfos/* methods */
	, IList_1_t7950_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7950_il2cpp_TypeInfo/* element_class */
	, IList_1_t7950_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7950_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7950_0_0_0/* byval_arg */
	, &IList_1_t7950_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<RollABall>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_64.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3144_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<RollABall>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_64MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<RollABall>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_60.h"
extern TypeInfo InvokableCall_1_t3145_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<RollABall>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_60MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15965_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15967_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<RollABall>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<RollABall>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<RollABall>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3144____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3144_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3144, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3144_FieldInfos[] =
{
	&CachedInvokableCall_1_t3144____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3144_CachedInvokableCall_1__ctor_m15963_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15963_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<RollABall>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15963_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3144_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3144_CachedInvokableCall_1__ctor_m15963_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15963_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3144_CachedInvokableCall_1_Invoke_m15964_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15964_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<RollABall>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15964_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3144_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3144_CachedInvokableCall_1_Invoke_m15964_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15964_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3144_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15963_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15964_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15964_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15968_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3144_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15964_MethodInfo,
	&InvokableCall_1_Find_m15968_MethodInfo,
};
extern Il2CppType UnityAction_1_t3146_0_0_0;
extern TypeInfo UnityAction_1_t3146_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRollABall_t218_m34025_MethodInfo;
extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15970_MethodInfo;
extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3144_RGCTXData[8] = 
{
	&UnityAction_1_t3146_0_0_0/* Type Usage */,
	&UnityAction_1_t3146_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRollABall_t218_m34025_MethodInfo/* Method Usage */,
	&RollABall_t218_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15970_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15965_MethodInfo/* Method Usage */,
	&RollABall_t218_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15967_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3144_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3144_1_0_0;
struct CachedInvokableCall_1_t3144;
extern Il2CppGenericClass CachedInvokableCall_1_t3144_GenericClass;
TypeInfo CachedInvokableCall_1_t3144_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3144_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3144_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3144_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3144_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3144_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3144_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3144_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3144_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3144_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3144)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<RollABall>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_67.h"
extern TypeInfo UnityAction_1_t3146_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<RollABall>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_67MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<RollABall>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<RollABall>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRollABall_t218_m34025(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<RollABall>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<RollABall>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<RollABall>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<RollABall>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<RollABall>
extern Il2CppType UnityAction_1_t3146_0_0_1;
FieldInfo InvokableCall_1_t3145____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3146_0_0_1/* type */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3145, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3145_FieldInfos[] =
{
	&InvokableCall_1_t3145____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3145_InvokableCall_1__ctor_m15965_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15965_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<RollABall>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15965_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3145_InvokableCall_1__ctor_m15965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15965_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3146_0_0_0;
static ParameterInfo InvokableCall_1_t3145_InvokableCall_1__ctor_m15966_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3146_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15966_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<RollABall>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15966_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3145_InvokableCall_1__ctor_m15966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15966_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3145_InvokableCall_1_Invoke_m15967_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15967_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<RollABall>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15967_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3145_InvokableCall_1_Invoke_m15967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15967_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3145_InvokableCall_1_Find_m15968_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15968_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<RollABall>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15968_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3145_InvokableCall_1_Find_m15968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15968_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3145_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15965_MethodInfo,
	&InvokableCall_1__ctor_m15966_MethodInfo,
	&InvokableCall_1_Invoke_m15967_MethodInfo,
	&InvokableCall_1_Find_m15968_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3145_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15967_MethodInfo,
	&InvokableCall_1_Find_m15968_MethodInfo,
};
extern TypeInfo UnityAction_1_t3146_il2cpp_TypeInfo;
extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3145_RGCTXData[5] = 
{
	&UnityAction_1_t3146_0_0_0/* Type Usage */,
	&UnityAction_1_t3146_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRollABall_t218_m34025_MethodInfo/* Method Usage */,
	&RollABall_t218_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15970_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3145_0_0_0;
extern Il2CppType InvokableCall_1_t3145_1_0_0;
struct InvokableCall_1_t3145;
extern Il2CppGenericClass InvokableCall_1_t3145_GenericClass;
TypeInfo InvokableCall_1_t3145_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3145_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3145_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3145_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3145_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3145_0_0_0/* byval_arg */
	, &InvokableCall_1_t3145_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3145_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3145_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3145)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<RollABall>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<RollABall>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<RollABall>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<RollABall>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<RollABall>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3146_UnityAction_1__ctor_m15969_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15969_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<RollABall>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3146_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3146_UnityAction_1__ctor_m15969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15969_GenericMethod/* genericMethod */

};
extern Il2CppType RollABall_t218_0_0_0;
static ParameterInfo UnityAction_1_t3146_UnityAction_1_Invoke_m15970_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15970_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<RollABall>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15970_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3146_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3146_UnityAction_1_Invoke_m15970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15970_GenericMethod/* genericMethod */

};
extern Il2CppType RollABall_t218_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3146_UnityAction_1_BeginInvoke_m15971_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RollABall_t218_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15971_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<RollABall>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15971_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3146_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3146_UnityAction_1_BeginInvoke_m15971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15971_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3146_UnityAction_1_EndInvoke_m15972_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15972_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<RollABall>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15972_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3146_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3146_UnityAction_1_EndInvoke_m15972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15972_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3146_MethodInfos[] =
{
	&UnityAction_1__ctor_m15969_MethodInfo,
	&UnityAction_1_Invoke_m15970_MethodInfo,
	&UnityAction_1_BeginInvoke_m15971_MethodInfo,
	&UnityAction_1_EndInvoke_m15972_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15971_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15972_MethodInfo;
static MethodInfo* UnityAction_1_t3146_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15970_MethodInfo,
	&UnityAction_1_BeginInvoke_m15971_MethodInfo,
	&UnityAction_1_EndInvoke_m15972_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3146_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3146_1_0_0;
struct UnityAction_1_t3146;
extern Il2CppGenericClass UnityAction_1_t3146_GenericClass;
TypeInfo UnityAction_1_t3146_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3146_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3146_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3146_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3146_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3146_0_0_0/* byval_arg */
	, &UnityAction_1_t3146_1_0_0/* this_arg */
	, UnityAction_1_t3146_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3146_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3146)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_18.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3147_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_18MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Collider>
extern Il2CppType Collider_t70_0_0_6;
FieldInfo CastHelper_1_t3147____t_0_FieldInfo = 
{
	"t"/* name */
	, &Collider_t70_0_0_6/* type */
	, &CastHelper_1_t3147_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3147, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3147____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3147_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3147, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3147_FieldInfos[] =
{
	&CastHelper_1_t3147____t_0_FieldInfo,
	&CastHelper_1_t3147____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3147_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3147_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3147_0_0_0;
extern Il2CppType CastHelper_1_t3147_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3147_GenericClass;
TypeInfo CastHelper_1_t3147_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3147_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3147_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3147_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3147_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3147_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3147_0_0_0/* byval_arg */
	, &CastHelper_1_t3147_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3147_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3147)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6218_il2cpp_TypeInfo;

// ConstraintAxis
#include "AssemblyU2DUnityScript_ConstraintAxis.h"


// T System.Collections.Generic.IEnumerator`1<ConstraintAxis>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ConstraintAxis>
extern MethodInfo IEnumerator_1_get_Current_m44498_MethodInfo;
static PropertyInfo IEnumerator_1_t6218____Current_PropertyInfo = 
{
	&IEnumerator_1_t6218_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44498_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6218_PropertyInfos[] =
{
	&IEnumerator_1_t6218____Current_PropertyInfo,
	NULL
};
extern Il2CppType ConstraintAxis_t219_0_0_0;
extern void* RuntimeInvoker_ConstraintAxis_t219 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44498_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ConstraintAxis>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44498_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6218_il2cpp_TypeInfo/* declaring_type */
	, &ConstraintAxis_t219_0_0_0/* return_type */
	, RuntimeInvoker_ConstraintAxis_t219/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44498_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6218_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44498_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6218_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6218_0_0_0;
extern Il2CppType IEnumerator_1_t6218_1_0_0;
struct IEnumerator_1_t6218;
extern Il2CppGenericClass IEnumerator_1_t6218_GenericClass;
TypeInfo IEnumerator_1_t6218_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6218_MethodInfos/* methods */
	, IEnumerator_1_t6218_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6218_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6218_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6218_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6218_0_0_0/* byval_arg */
	, &IEnumerator_1_t6218_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6218_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ConstraintAxis>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_152.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3148_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ConstraintAxis>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_152MethodDeclarations.h"

extern TypeInfo ConstraintAxis_t219_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15977_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisConstraintAxis_t219_m34027_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ConstraintAxis>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ConstraintAxis>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisConstraintAxis_t219_m34027 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<ConstraintAxis>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m15973_MethodInfo;
 void InternalEnumerator_1__ctor_m15973 (InternalEnumerator_1_t3148 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ConstraintAxis>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974 (InternalEnumerator_1_t3148 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m15977(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m15977_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ConstraintAxis_t219_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ConstraintAxis>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m15975_MethodInfo;
 void InternalEnumerator_1_Dispose_m15975 (InternalEnumerator_1_t3148 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ConstraintAxis>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m15976_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m15976 (InternalEnumerator_1_t3148 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ConstraintAxis>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15977 (InternalEnumerator_1_t3148 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisConstraintAxis_t219_m34027(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisConstraintAxis_t219_m34027_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<ConstraintAxis>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3148____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3148, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3148____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3148, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3148_FieldInfos[] =
{
	&InternalEnumerator_1_t3148____array_0_FieldInfo,
	&InternalEnumerator_1_t3148____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3148____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3148_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3148____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3148_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3148_PropertyInfos[] =
{
	&InternalEnumerator_1_t3148____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3148____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3148_InternalEnumerator_1__ctor_m15973_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15973_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ConstraintAxis>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15973_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m15973/* method */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3148_InternalEnumerator_1__ctor_m15973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15973_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ConstraintAxis>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974/* method */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15975_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ConstraintAxis>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15975_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m15975/* method */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15975_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15976_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ConstraintAxis>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15976_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m15976/* method */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15976_GenericMethod/* genericMethod */

};
extern Il2CppType ConstraintAxis_t219_0_0_0;
extern void* RuntimeInvoker_ConstraintAxis_t219 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15977_GenericMethod;
// T System.Array/InternalEnumerator`1<ConstraintAxis>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15977_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m15977/* method */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* declaring_type */
	, &ConstraintAxis_t219_0_0_0/* return_type */
	, RuntimeInvoker_ConstraintAxis_t219/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15977_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3148_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15973_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_MethodInfo,
	&InternalEnumerator_1_Dispose_m15975_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15976_MethodInfo,
	&InternalEnumerator_1_get_Current_m15977_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3148_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15974_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15976_MethodInfo,
	&InternalEnumerator_1_Dispose_m15975_MethodInfo,
	&InternalEnumerator_1_get_Current_m15977_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3148_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6218_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3148_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6218_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3148_0_0_0;
extern Il2CppType InternalEnumerator_1_t3148_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3148_GenericClass;
TypeInfo InternalEnumerator_1_t3148_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3148_MethodInfos/* methods */
	, InternalEnumerator_1_t3148_PropertyInfos/* properties */
	, InternalEnumerator_1_t3148_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3148_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3148_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3148_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3148_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3148_1_0_0/* this_arg */
	, InternalEnumerator_1_t3148_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3148_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3148)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7952_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<ConstraintAxis>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ConstraintAxis>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ConstraintAxis>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ConstraintAxis>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ConstraintAxis>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ConstraintAxis>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ConstraintAxis>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ConstraintAxis>
extern MethodInfo ICollection_1_get_Count_m44499_MethodInfo;
static PropertyInfo ICollection_1_t7952____Count_PropertyInfo = 
{
	&ICollection_1_t7952_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44499_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44500_MethodInfo;
static PropertyInfo ICollection_1_t7952____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7952_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44500_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7952_PropertyInfos[] =
{
	&ICollection_1_t7952____Count_PropertyInfo,
	&ICollection_1_t7952____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44499_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ConstraintAxis>::get_Count()
MethodInfo ICollection_1_get_Count_m44499_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44499_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44500_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ConstraintAxis>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44500_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44500_GenericMethod/* genericMethod */

};
extern Il2CppType ConstraintAxis_t219_0_0_0;
extern Il2CppType ConstraintAxis_t219_0_0_0;
static ParameterInfo ICollection_1_t7952_ICollection_1_Add_m44501_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstraintAxis_t219_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44501_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ConstraintAxis>::Add(T)
MethodInfo ICollection_1_Add_m44501_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t7952_ICollection_1_Add_m44501_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44501_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44502_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ConstraintAxis>::Clear()
MethodInfo ICollection_1_Clear_m44502_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44502_GenericMethod/* genericMethod */

};
extern Il2CppType ConstraintAxis_t219_0_0_0;
static ParameterInfo ICollection_1_t7952_ICollection_1_Contains_m44503_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstraintAxis_t219_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44503_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ConstraintAxis>::Contains(T)
MethodInfo ICollection_1_Contains_m44503_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7952_ICollection_1_Contains_m44503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44503_GenericMethod/* genericMethod */

};
extern Il2CppType ConstraintAxisU5BU5D_t5778_0_0_0;
extern Il2CppType ConstraintAxisU5BU5D_t5778_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7952_ICollection_1_CopyTo_m44504_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ConstraintAxisU5BU5D_t5778_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44504_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ConstraintAxis>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44504_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7952_ICollection_1_CopyTo_m44504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44504_GenericMethod/* genericMethod */

};
extern Il2CppType ConstraintAxis_t219_0_0_0;
static ParameterInfo ICollection_1_t7952_ICollection_1_Remove_m44505_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstraintAxis_t219_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44505_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ConstraintAxis>::Remove(T)
MethodInfo ICollection_1_Remove_m44505_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7952_ICollection_1_Remove_m44505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44505_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7952_MethodInfos[] =
{
	&ICollection_1_get_Count_m44499_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44500_MethodInfo,
	&ICollection_1_Add_m44501_MethodInfo,
	&ICollection_1_Clear_m44502_MethodInfo,
	&ICollection_1_Contains_m44503_MethodInfo,
	&ICollection_1_CopyTo_m44504_MethodInfo,
	&ICollection_1_Remove_m44505_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7954_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7952_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7954_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7952_0_0_0;
extern Il2CppType ICollection_1_t7952_1_0_0;
struct ICollection_1_t7952;
extern Il2CppGenericClass ICollection_1_t7952_GenericClass;
TypeInfo ICollection_1_t7952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7952_MethodInfos/* methods */
	, ICollection_1_t7952_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7952_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7952_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7952_0_0_0/* byval_arg */
	, &ICollection_1_t7952_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ConstraintAxis>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ConstraintAxis>
extern Il2CppType IEnumerator_1_t6218_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44506_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ConstraintAxis>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44506_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7954_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44506_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7954_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44506_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7954_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7954_0_0_0;
extern Il2CppType IEnumerable_1_t7954_1_0_0;
struct IEnumerable_1_t7954;
extern Il2CppGenericClass IEnumerable_1_t7954_GenericClass;
TypeInfo IEnumerable_1_t7954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7954_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7954_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7954_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7954_0_0_0/* byval_arg */
	, &IEnumerable_1_t7954_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7953_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<ConstraintAxis>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ConstraintAxis>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ConstraintAxis>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ConstraintAxis>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ConstraintAxis>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ConstraintAxis>
extern MethodInfo IList_1_get_Item_m44507_MethodInfo;
extern MethodInfo IList_1_set_Item_m44508_MethodInfo;
static PropertyInfo IList_1_t7953____Item_PropertyInfo = 
{
	&IList_1_t7953_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44507_MethodInfo/* get */
	, &IList_1_set_Item_m44508_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7953_PropertyInfos[] =
{
	&IList_1_t7953____Item_PropertyInfo,
	NULL
};
extern Il2CppType ConstraintAxis_t219_0_0_0;
static ParameterInfo IList_1_t7953_IList_1_IndexOf_m44509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstraintAxis_t219_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44509_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ConstraintAxis>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44509_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7953_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7953_IList_1_IndexOf_m44509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44509_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ConstraintAxis_t219_0_0_0;
static ParameterInfo IList_1_t7953_IList_1_Insert_m44510_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ConstraintAxis_t219_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44510_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ConstraintAxis>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44510_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7953_IList_1_Insert_m44510_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44510_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7953_IList_1_RemoveAt_m44511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44511_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ConstraintAxis>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44511_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7953_IList_1_RemoveAt_m44511_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44511_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7953_IList_1_get_Item_m44507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ConstraintAxis_t219_0_0_0;
extern void* RuntimeInvoker_ConstraintAxis_t219_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44507_GenericMethod;
// T System.Collections.Generic.IList`1<ConstraintAxis>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44507_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7953_il2cpp_TypeInfo/* declaring_type */
	, &ConstraintAxis_t219_0_0_0/* return_type */
	, RuntimeInvoker_ConstraintAxis_t219_Int32_t123/* invoker_method */
	, IList_1_t7953_IList_1_get_Item_m44507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44507_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ConstraintAxis_t219_0_0_0;
static ParameterInfo IList_1_t7953_IList_1_set_Item_m44508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ConstraintAxis_t219_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44508_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ConstraintAxis>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44508_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7953_IList_1_set_Item_m44508_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44508_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7953_MethodInfos[] =
{
	&IList_1_IndexOf_m44509_MethodInfo,
	&IList_1_Insert_m44510_MethodInfo,
	&IList_1_RemoveAt_m44511_MethodInfo,
	&IList_1_get_Item_m44507_MethodInfo,
	&IList_1_set_Item_m44508_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7953_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7952_il2cpp_TypeInfo,
	&IEnumerable_1_t7954_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7953_0_0_0;
extern Il2CppType IList_1_t7953_1_0_0;
struct IList_1_t7953;
extern Il2CppGenericClass IList_1_t7953_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7953_MethodInfos/* methods */
	, IList_1_t7953_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7953_il2cpp_TypeInfo/* element_class */
	, IList_1_t7953_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7953_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7953_0_0_0/* byval_arg */
	, &IList_1_t7953_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7953_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6220_il2cpp_TypeInfo;

// RotationConstraint
#include "AssemblyU2DUnityScript_RotationConstraint.h"


// T System.Collections.Generic.IEnumerator`1<RotationConstraint>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<RotationConstraint>
extern MethodInfo IEnumerator_1_get_Current_m44512_MethodInfo;
static PropertyInfo IEnumerator_1_t6220____Current_PropertyInfo = 
{
	&IEnumerator_1_t6220_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6220_PropertyInfos[] =
{
	&IEnumerator_1_t6220____Current_PropertyInfo,
	NULL
};
extern Il2CppType RotationConstraint_t220_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44512_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<RotationConstraint>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44512_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6220_il2cpp_TypeInfo/* declaring_type */
	, &RotationConstraint_t220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44512_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6220_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44512_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6220_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6220_0_0_0;
extern Il2CppType IEnumerator_1_t6220_1_0_0;
struct IEnumerator_1_t6220;
extern Il2CppGenericClass IEnumerator_1_t6220_GenericClass;
TypeInfo IEnumerator_1_t6220_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6220_MethodInfos/* methods */
	, IEnumerator_1_t6220_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6220_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6220_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6220_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6220_0_0_0/* byval_arg */
	, &IEnumerator_1_t6220_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6220_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<RotationConstraint>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_153.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3149_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<RotationConstraint>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_153MethodDeclarations.h"

extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15982_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRotationConstraint_t220_m34038_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<RotationConstraint>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<RotationConstraint>(System.Int32)
#define Array_InternalArray__get_Item_TisRotationConstraint_t220_m34038(__this, p0, method) (RotationConstraint_t220 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<RotationConstraint>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<RotationConstraint>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<RotationConstraint>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<RotationConstraint>::MoveNext()
// T System.Array/InternalEnumerator`1<RotationConstraint>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<RotationConstraint>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3149____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3149, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3149____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3149, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3149_FieldInfos[] =
{
	&InternalEnumerator_1_t3149____array_0_FieldInfo,
	&InternalEnumerator_1_t3149____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3149____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3149_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3149____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3149_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15982_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3149_PropertyInfos[] =
{
	&InternalEnumerator_1_t3149____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3149____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3149_InternalEnumerator_1__ctor_m15978_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15978_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<RotationConstraint>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15978_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3149_InternalEnumerator_1__ctor_m15978_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15978_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<RotationConstraint>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15980_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<RotationConstraint>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15980_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15980_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15981_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<RotationConstraint>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15981_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15981_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraint_t220_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15982_GenericMethod;
// T System.Array/InternalEnumerator`1<RotationConstraint>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15982_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* declaring_type */
	, &RotationConstraint_t220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15982_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3149_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15978_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_MethodInfo,
	&InternalEnumerator_1_Dispose_m15980_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15981_MethodInfo,
	&InternalEnumerator_1_get_Current_m15982_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15981_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15980_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3149_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15979_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15981_MethodInfo,
	&InternalEnumerator_1_Dispose_m15980_MethodInfo,
	&InternalEnumerator_1_get_Current_m15982_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3149_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6220_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3149_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6220_il2cpp_TypeInfo, 7},
};
extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3149_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15982_MethodInfo/* Method Usage */,
	&RotationConstraint_t220_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRotationConstraint_t220_m34038_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3149_0_0_0;
extern Il2CppType InternalEnumerator_1_t3149_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3149_GenericClass;
TypeInfo InternalEnumerator_1_t3149_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3149_MethodInfos/* methods */
	, InternalEnumerator_1_t3149_PropertyInfos/* properties */
	, InternalEnumerator_1_t3149_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3149_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3149_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3149_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3149_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3149_1_0_0/* this_arg */
	, InternalEnumerator_1_t3149_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3149_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3149_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3149)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7955_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<RotationConstraint>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<RotationConstraint>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<RotationConstraint>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<RotationConstraint>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<RotationConstraint>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<RotationConstraint>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<RotationConstraint>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<RotationConstraint>
extern MethodInfo ICollection_1_get_Count_m44513_MethodInfo;
static PropertyInfo ICollection_1_t7955____Count_PropertyInfo = 
{
	&ICollection_1_t7955_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44514_MethodInfo;
static PropertyInfo ICollection_1_t7955____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7955_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44514_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7955_PropertyInfos[] =
{
	&ICollection_1_t7955____Count_PropertyInfo,
	&ICollection_1_t7955____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44513_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<RotationConstraint>::get_Count()
MethodInfo ICollection_1_get_Count_m44513_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44513_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44514_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<RotationConstraint>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44514_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44514_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraint_t220_0_0_0;
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo ICollection_1_t7955_ICollection_1_Add_m44515_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44515_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<RotationConstraint>::Add(T)
MethodInfo ICollection_1_Add_m44515_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7955_ICollection_1_Add_m44515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44515_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44516_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<RotationConstraint>::Clear()
MethodInfo ICollection_1_Clear_m44516_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44516_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo ICollection_1_t7955_ICollection_1_Contains_m44517_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44517_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<RotationConstraint>::Contains(T)
MethodInfo ICollection_1_Contains_m44517_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7955_ICollection_1_Contains_m44517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44517_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraintU5BU5D_t5779_0_0_0;
extern Il2CppType RotationConstraintU5BU5D_t5779_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7955_ICollection_1_CopyTo_m44518_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraintU5BU5D_t5779_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44518_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<RotationConstraint>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44518_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7955_ICollection_1_CopyTo_m44518_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44518_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo ICollection_1_t7955_ICollection_1_Remove_m44519_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44519_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<RotationConstraint>::Remove(T)
MethodInfo ICollection_1_Remove_m44519_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7955_ICollection_1_Remove_m44519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44519_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7955_MethodInfos[] =
{
	&ICollection_1_get_Count_m44513_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44514_MethodInfo,
	&ICollection_1_Add_m44515_MethodInfo,
	&ICollection_1_Clear_m44516_MethodInfo,
	&ICollection_1_Contains_m44517_MethodInfo,
	&ICollection_1_CopyTo_m44518_MethodInfo,
	&ICollection_1_Remove_m44519_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7957_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7955_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7957_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7955_0_0_0;
extern Il2CppType ICollection_1_t7955_1_0_0;
struct ICollection_1_t7955;
extern Il2CppGenericClass ICollection_1_t7955_GenericClass;
TypeInfo ICollection_1_t7955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7955_MethodInfos/* methods */
	, ICollection_1_t7955_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7955_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7955_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7955_0_0_0/* byval_arg */
	, &ICollection_1_t7955_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7955_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<RotationConstraint>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<RotationConstraint>
extern Il2CppType IEnumerator_1_t6220_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44520_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<RotationConstraint>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44520_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7957_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44520_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7957_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44520_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7957_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7957_0_0_0;
extern Il2CppType IEnumerable_1_t7957_1_0_0;
struct IEnumerable_1_t7957;
extern Il2CppGenericClass IEnumerable_1_t7957_GenericClass;
TypeInfo IEnumerable_1_t7957_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7957_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7957_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7957_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7957_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7957_0_0_0/* byval_arg */
	, &IEnumerable_1_t7957_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7957_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7956_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<RotationConstraint>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<RotationConstraint>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<RotationConstraint>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<RotationConstraint>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<RotationConstraint>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<RotationConstraint>
extern MethodInfo IList_1_get_Item_m44521_MethodInfo;
extern MethodInfo IList_1_set_Item_m44522_MethodInfo;
static PropertyInfo IList_1_t7956____Item_PropertyInfo = 
{
	&IList_1_t7956_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44521_MethodInfo/* get */
	, &IList_1_set_Item_m44522_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7956_PropertyInfos[] =
{
	&IList_1_t7956____Item_PropertyInfo,
	NULL
};
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo IList_1_t7956_IList_1_IndexOf_m44523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44523_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<RotationConstraint>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44523_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7956_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7956_IList_1_IndexOf_m44523_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44523_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo IList_1_t7956_IList_1_Insert_m44524_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44524_GenericMethod;
// System.Void System.Collections.Generic.IList`1<RotationConstraint>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44524_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7956_IList_1_Insert_m44524_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44524_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7956_IList_1_RemoveAt_m44525_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44525_GenericMethod;
// System.Void System.Collections.Generic.IList`1<RotationConstraint>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44525_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7956_IList_1_RemoveAt_m44525_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44525_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7956_IList_1_get_Item_m44521_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType RotationConstraint_t220_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44521_GenericMethod;
// T System.Collections.Generic.IList`1<RotationConstraint>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44521_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7956_il2cpp_TypeInfo/* declaring_type */
	, &RotationConstraint_t220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7956_IList_1_get_Item_m44521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44521_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo IList_1_t7956_IList_1_set_Item_m44522_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44522_GenericMethod;
// System.Void System.Collections.Generic.IList`1<RotationConstraint>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44522_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7956_IList_1_set_Item_m44522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44522_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7956_MethodInfos[] =
{
	&IList_1_IndexOf_m44523_MethodInfo,
	&IList_1_Insert_m44524_MethodInfo,
	&IList_1_RemoveAt_m44525_MethodInfo,
	&IList_1_get_Item_m44521_MethodInfo,
	&IList_1_set_Item_m44522_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7956_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7955_il2cpp_TypeInfo,
	&IEnumerable_1_t7957_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7956_0_0_0;
extern Il2CppType IList_1_t7956_1_0_0;
struct IList_1_t7956;
extern Il2CppGenericClass IList_1_t7956_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7956_MethodInfos/* methods */
	, IList_1_t7956_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7956_il2cpp_TypeInfo/* element_class */
	, IList_1_t7956_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7956_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7956_0_0_0/* byval_arg */
	, &IList_1_t7956_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_65.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3150_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_65MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_61.h"
extern TypeInfo InvokableCall_1_t3151_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_61MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15985_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15987_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3150____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3150_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3150, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3150_FieldInfos[] =
{
	&CachedInvokableCall_1_t3150____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3150_CachedInvokableCall_1__ctor_m15983_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15983_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15983_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3150_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3150_CachedInvokableCall_1__ctor_m15983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15983_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3150_CachedInvokableCall_1_Invoke_m15984_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15984_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15984_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3150_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3150_CachedInvokableCall_1_Invoke_m15984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15984_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3150_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15983_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15984_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15984_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15988_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3150_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15984_MethodInfo,
	&InvokableCall_1_Find_m15988_MethodInfo,
};
extern Il2CppType UnityAction_1_t3152_0_0_0;
extern TypeInfo UnityAction_1_t3152_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRotationConstraint_t220_m34048_MethodInfo;
extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15990_MethodInfo;
extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3150_RGCTXData[8] = 
{
	&UnityAction_1_t3152_0_0_0/* Type Usage */,
	&UnityAction_1_t3152_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRotationConstraint_t220_m34048_MethodInfo/* Method Usage */,
	&RotationConstraint_t220_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15990_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15985_MethodInfo/* Method Usage */,
	&RotationConstraint_t220_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15987_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3150_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3150_1_0_0;
struct CachedInvokableCall_1_t3150;
extern Il2CppGenericClass CachedInvokableCall_1_t3150_GenericClass;
TypeInfo CachedInvokableCall_1_t3150_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3150_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3150_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3150_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3150_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3150_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3150_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3150_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3150_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3150_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3150)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_68.h"
extern TypeInfo UnityAction_1_t3152_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_68MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<RotationConstraint>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<RotationConstraint>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRotationConstraint_t220_m34048(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<RotationConstraint>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<RotationConstraint>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<RotationConstraint>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<RotationConstraint>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<RotationConstraint>
extern Il2CppType UnityAction_1_t3152_0_0_1;
FieldInfo InvokableCall_1_t3151____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3152_0_0_1/* type */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3151, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3151_FieldInfos[] =
{
	&InvokableCall_1_t3151____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3151_InvokableCall_1__ctor_m15985_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15985_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<RotationConstraint>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15985_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3151_InvokableCall_1__ctor_m15985_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15985_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3152_0_0_0;
static ParameterInfo InvokableCall_1_t3151_InvokableCall_1__ctor_m15986_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3152_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15986_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<RotationConstraint>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15986_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3151_InvokableCall_1__ctor_m15986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15986_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3151_InvokableCall_1_Invoke_m15987_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15987_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<RotationConstraint>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15987_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3151_InvokableCall_1_Invoke_m15987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15987_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3151_InvokableCall_1_Find_m15988_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15988_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<RotationConstraint>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15988_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3151_InvokableCall_1_Find_m15988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15988_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3151_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15985_MethodInfo,
	&InvokableCall_1__ctor_m15986_MethodInfo,
	&InvokableCall_1_Invoke_m15987_MethodInfo,
	&InvokableCall_1_Find_m15988_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3151_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15987_MethodInfo,
	&InvokableCall_1_Find_m15988_MethodInfo,
};
extern TypeInfo UnityAction_1_t3152_il2cpp_TypeInfo;
extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3151_RGCTXData[5] = 
{
	&UnityAction_1_t3152_0_0_0/* Type Usage */,
	&UnityAction_1_t3152_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRotationConstraint_t220_m34048_MethodInfo/* Method Usage */,
	&RotationConstraint_t220_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15990_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3151_0_0_0;
extern Il2CppType InvokableCall_1_t3151_1_0_0;
struct InvokableCall_1_t3151;
extern Il2CppGenericClass InvokableCall_1_t3151_GenericClass;
TypeInfo InvokableCall_1_t3151_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3151_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3151_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3151_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3151_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3151_0_0_0/* byval_arg */
	, &InvokableCall_1_t3151_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3151_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3151_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3151)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<RotationConstraint>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<RotationConstraint>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<RotationConstraint>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<RotationConstraint>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<RotationConstraint>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3152_UnityAction_1__ctor_m15989_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15989_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<RotationConstraint>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15989_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3152_UnityAction_1__ctor_m15989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15989_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraint_t220_0_0_0;
static ParameterInfo UnityAction_1_t3152_UnityAction_1_Invoke_m15990_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15990_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<RotationConstraint>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15990_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3152_UnityAction_1_Invoke_m15990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15990_GenericMethod/* genericMethod */

};
extern Il2CppType RotationConstraint_t220_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3152_UnityAction_1_BeginInvoke_m15991_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RotationConstraint_t220_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15991_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<RotationConstraint>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15991_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3152_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3152_UnityAction_1_BeginInvoke_m15991_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15991_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3152_UnityAction_1_EndInvoke_m15992_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15992_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<RotationConstraint>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15992_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3152_UnityAction_1_EndInvoke_m15992_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15992_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3152_MethodInfos[] =
{
	&UnityAction_1__ctor_m15989_MethodInfo,
	&UnityAction_1_Invoke_m15990_MethodInfo,
	&UnityAction_1_BeginInvoke_m15991_MethodInfo,
	&UnityAction_1_EndInvoke_m15992_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15991_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15992_MethodInfo;
static MethodInfo* UnityAction_1_t3152_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15990_MethodInfo,
	&UnityAction_1_BeginInvoke_m15991_MethodInfo,
	&UnityAction_1_EndInvoke_m15992_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3152_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3152_1_0_0;
struct UnityAction_1_t3152;
extern Il2CppGenericClass UnityAction_1_t3152_GenericClass;
TypeInfo UnityAction_1_t3152_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3152_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3152_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3152_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3152_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3152_0_0_0/* byval_arg */
	, &UnityAction_1_t3152_1_0_0/* this_arg */
	, UnityAction_1_t3152_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3152_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3152)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6222_il2cpp_TypeInfo;

// SidescrollControl
#include "AssemblyU2DUnityScript_SidescrollControl.h"


// T System.Collections.Generic.IEnumerator`1<SidescrollControl>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<SidescrollControl>
extern MethodInfo IEnumerator_1_get_Current_m44526_MethodInfo;
static PropertyInfo IEnumerator_1_t6222____Current_PropertyInfo = 
{
	&IEnumerator_1_t6222_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44526_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6222_PropertyInfos[] =
{
	&IEnumerator_1_t6222____Current_PropertyInfo,
	NULL
};
extern Il2CppType SidescrollControl_t221_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44526_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<SidescrollControl>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44526_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6222_il2cpp_TypeInfo/* declaring_type */
	, &SidescrollControl_t221_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44526_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6222_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44526_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6222_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6222_0_0_0;
extern Il2CppType IEnumerator_1_t6222_1_0_0;
struct IEnumerator_1_t6222;
extern Il2CppGenericClass IEnumerator_1_t6222_GenericClass;
TypeInfo IEnumerator_1_t6222_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6222_MethodInfos/* methods */
	, IEnumerator_1_t6222_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6222_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6222_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6222_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6222_0_0_0/* byval_arg */
	, &IEnumerator_1_t6222_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6222_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<SidescrollControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_154.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3153_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<SidescrollControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_154MethodDeclarations.h"

extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15997_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSidescrollControl_t221_m34050_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<SidescrollControl>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<SidescrollControl>(System.Int32)
#define Array_InternalArray__get_Item_TisSidescrollControl_t221_m34050(__this, p0, method) (SidescrollControl_t221 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<SidescrollControl>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<SidescrollControl>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<SidescrollControl>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<SidescrollControl>::MoveNext()
// T System.Array/InternalEnumerator`1<SidescrollControl>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<SidescrollControl>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3153____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3153, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3153____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3153, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3153_FieldInfos[] =
{
	&InternalEnumerator_1_t3153____array_0_FieldInfo,
	&InternalEnumerator_1_t3153____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3153____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3153_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3153____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3153_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15997_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3153_PropertyInfos[] =
{
	&InternalEnumerator_1_t3153____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3153____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3153_InternalEnumerator_1__ctor_m15993_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15993_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SidescrollControl>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15993_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3153_InternalEnumerator_1__ctor_m15993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15993_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<SidescrollControl>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15995_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SidescrollControl>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15995_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15995_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15996_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<SidescrollControl>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15996_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15996_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControl_t221_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15997_GenericMethod;
// T System.Array/InternalEnumerator`1<SidescrollControl>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15997_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* declaring_type */
	, &SidescrollControl_t221_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15997_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3153_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15993_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_MethodInfo,
	&InternalEnumerator_1_Dispose_m15995_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15996_MethodInfo,
	&InternalEnumerator_1_get_Current_m15997_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15996_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15995_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3153_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15994_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15996_MethodInfo,
	&InternalEnumerator_1_Dispose_m15995_MethodInfo,
	&InternalEnumerator_1_get_Current_m15997_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3153_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6222_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3153_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6222_il2cpp_TypeInfo, 7},
};
extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3153_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15997_MethodInfo/* Method Usage */,
	&SidescrollControl_t221_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSidescrollControl_t221_m34050_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3153_0_0_0;
extern Il2CppType InternalEnumerator_1_t3153_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3153_GenericClass;
TypeInfo InternalEnumerator_1_t3153_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3153_MethodInfos/* methods */
	, InternalEnumerator_1_t3153_PropertyInfos/* properties */
	, InternalEnumerator_1_t3153_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3153_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3153_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3153_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3153_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3153_1_0_0/* this_arg */
	, InternalEnumerator_1_t3153_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3153_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3153_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3153)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7958_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<SidescrollControl>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<SidescrollControl>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<SidescrollControl>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<SidescrollControl>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<SidescrollControl>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<SidescrollControl>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<SidescrollControl>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<SidescrollControl>
extern MethodInfo ICollection_1_get_Count_m44527_MethodInfo;
static PropertyInfo ICollection_1_t7958____Count_PropertyInfo = 
{
	&ICollection_1_t7958_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44527_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44528_MethodInfo;
static PropertyInfo ICollection_1_t7958____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7958_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7958_PropertyInfos[] =
{
	&ICollection_1_t7958____Count_PropertyInfo,
	&ICollection_1_t7958____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44527_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<SidescrollControl>::get_Count()
MethodInfo ICollection_1_get_Count_m44527_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44527_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44528_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SidescrollControl>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44528_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44528_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControl_t221_0_0_0;
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo ICollection_1_t7958_ICollection_1_Add_m44529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44529_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SidescrollControl>::Add(T)
MethodInfo ICollection_1_Add_m44529_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7958_ICollection_1_Add_m44529_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44529_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44530_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SidescrollControl>::Clear()
MethodInfo ICollection_1_Clear_m44530_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44530_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo ICollection_1_t7958_ICollection_1_Contains_m44531_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44531_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SidescrollControl>::Contains(T)
MethodInfo ICollection_1_Contains_m44531_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7958_ICollection_1_Contains_m44531_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44531_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControlU5BU5D_t5780_0_0_0;
extern Il2CppType SidescrollControlU5BU5D_t5780_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7958_ICollection_1_CopyTo_m44532_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControlU5BU5D_t5780_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44532_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SidescrollControl>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44532_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7958_ICollection_1_CopyTo_m44532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44532_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo ICollection_1_t7958_ICollection_1_Remove_m44533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44533_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SidescrollControl>::Remove(T)
MethodInfo ICollection_1_Remove_m44533_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7958_ICollection_1_Remove_m44533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44533_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7958_MethodInfos[] =
{
	&ICollection_1_get_Count_m44527_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44528_MethodInfo,
	&ICollection_1_Add_m44529_MethodInfo,
	&ICollection_1_Clear_m44530_MethodInfo,
	&ICollection_1_Contains_m44531_MethodInfo,
	&ICollection_1_CopyTo_m44532_MethodInfo,
	&ICollection_1_Remove_m44533_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7960_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7958_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7960_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7958_0_0_0;
extern Il2CppType ICollection_1_t7958_1_0_0;
struct ICollection_1_t7958;
extern Il2CppGenericClass ICollection_1_t7958_GenericClass;
TypeInfo ICollection_1_t7958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7958_MethodInfos/* methods */
	, ICollection_1_t7958_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7958_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7958_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7958_0_0_0/* byval_arg */
	, &ICollection_1_t7958_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7958_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SidescrollControl>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<SidescrollControl>
extern Il2CppType IEnumerator_1_t6222_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44534_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SidescrollControl>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44534_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7960_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44534_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7960_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44534_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7960_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7960_0_0_0;
extern Il2CppType IEnumerable_1_t7960_1_0_0;
struct IEnumerable_1_t7960;
extern Il2CppGenericClass IEnumerable_1_t7960_GenericClass;
TypeInfo IEnumerable_1_t7960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7960_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7960_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7960_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7960_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7960_0_0_0/* byval_arg */
	, &IEnumerable_1_t7960_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7960_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7959_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<SidescrollControl>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<SidescrollControl>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<SidescrollControl>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<SidescrollControl>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<SidescrollControl>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<SidescrollControl>
extern MethodInfo IList_1_get_Item_m44535_MethodInfo;
extern MethodInfo IList_1_set_Item_m44536_MethodInfo;
static PropertyInfo IList_1_t7959____Item_PropertyInfo = 
{
	&IList_1_t7959_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44535_MethodInfo/* get */
	, &IList_1_set_Item_m44536_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7959_PropertyInfos[] =
{
	&IList_1_t7959____Item_PropertyInfo,
	NULL
};
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo IList_1_t7959_IList_1_IndexOf_m44537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44537_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<SidescrollControl>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44537_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7959_IList_1_IndexOf_m44537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44537_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo IList_1_t7959_IList_1_Insert_m44538_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44538_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SidescrollControl>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44538_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7959_IList_1_Insert_m44538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44538_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7959_IList_1_RemoveAt_m44539_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44539_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SidescrollControl>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44539_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7959_IList_1_RemoveAt_m44539_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44539_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7959_IList_1_get_Item_m44535_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SidescrollControl_t221_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44535_GenericMethod;
// T System.Collections.Generic.IList`1<SidescrollControl>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44535_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7959_il2cpp_TypeInfo/* declaring_type */
	, &SidescrollControl_t221_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7959_IList_1_get_Item_m44535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44535_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo IList_1_t7959_IList_1_set_Item_m44536_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44536_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SidescrollControl>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44536_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7959_IList_1_set_Item_m44536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44536_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7959_MethodInfos[] =
{
	&IList_1_IndexOf_m44537_MethodInfo,
	&IList_1_Insert_m44538_MethodInfo,
	&IList_1_RemoveAt_m44539_MethodInfo,
	&IList_1_get_Item_m44535_MethodInfo,
	&IList_1_set_Item_m44536_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7959_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7958_il2cpp_TypeInfo,
	&IEnumerable_1_t7960_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7959_0_0_0;
extern Il2CppType IList_1_t7959_1_0_0;
struct IList_1_t7959;
extern Il2CppGenericClass IList_1_t7959_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7959_MethodInfos/* methods */
	, IList_1_t7959_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7959_il2cpp_TypeInfo/* element_class */
	, IList_1_t7959_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7959_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7959_0_0_0/* byval_arg */
	, &IList_1_t7959_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7959_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_66.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3154_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_66MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_62.h"
extern TypeInfo InvokableCall_1_t3155_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_62MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m16000_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m16002_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3154____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3154_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3154, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3154_FieldInfos[] =
{
	&CachedInvokableCall_1_t3154____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3154_CachedInvokableCall_1__ctor_m15998_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15998_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15998_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3154_CachedInvokableCall_1__ctor_m15998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15998_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3154_CachedInvokableCall_1_Invoke_m15999_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15999_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15999_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3154_CachedInvokableCall_1_Invoke_m15999_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15999_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3154_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15998_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15999_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15999_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m16003_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3154_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15999_MethodInfo,
	&InvokableCall_1_Find_m16003_MethodInfo,
};
extern Il2CppType UnityAction_1_t3156_0_0_0;
extern TypeInfo UnityAction_1_t3156_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSidescrollControl_t221_m34060_MethodInfo;
extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m16005_MethodInfo;
extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3154_RGCTXData[8] = 
{
	&UnityAction_1_t3156_0_0_0/* Type Usage */,
	&UnityAction_1_t3156_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSidescrollControl_t221_m34060_MethodInfo/* Method Usage */,
	&SidescrollControl_t221_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16005_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m16000_MethodInfo/* Method Usage */,
	&SidescrollControl_t221_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m16002_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3154_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3154_1_0_0;
struct CachedInvokableCall_1_t3154;
extern Il2CppGenericClass CachedInvokableCall_1_t3154_GenericClass;
TypeInfo CachedInvokableCall_1_t3154_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3154_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3154_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3154_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3154_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3154_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3154_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3154_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3154_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3154_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3154)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_69.h"
extern TypeInfo UnityAction_1_t3156_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_69MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SidescrollControl>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SidescrollControl>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSidescrollControl_t221_m34060(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<SidescrollControl>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<SidescrollControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<SidescrollControl>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<SidescrollControl>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<SidescrollControl>
extern Il2CppType UnityAction_1_t3156_0_0_1;
FieldInfo InvokableCall_1_t3155____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3156_0_0_1/* type */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3155, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3155_FieldInfos[] =
{
	&InvokableCall_1_t3155____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3155_InvokableCall_1__ctor_m16000_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16000_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SidescrollControl>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m16000_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3155_InvokableCall_1__ctor_m16000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16000_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3156_0_0_0;
static ParameterInfo InvokableCall_1_t3155_InvokableCall_1__ctor_m16001_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3156_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16001_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SidescrollControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m16001_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3155_InvokableCall_1__ctor_m16001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16001_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3155_InvokableCall_1_Invoke_m16002_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m16002_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SidescrollControl>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m16002_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3155_InvokableCall_1_Invoke_m16002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m16002_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3155_InvokableCall_1_Find_m16003_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m16003_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<SidescrollControl>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m16003_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3155_InvokableCall_1_Find_m16003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m16003_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3155_MethodInfos[] =
{
	&InvokableCall_1__ctor_m16000_MethodInfo,
	&InvokableCall_1__ctor_m16001_MethodInfo,
	&InvokableCall_1_Invoke_m16002_MethodInfo,
	&InvokableCall_1_Find_m16003_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3155_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m16002_MethodInfo,
	&InvokableCall_1_Find_m16003_MethodInfo,
};
extern TypeInfo UnityAction_1_t3156_il2cpp_TypeInfo;
extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3155_RGCTXData[5] = 
{
	&UnityAction_1_t3156_0_0_0/* Type Usage */,
	&UnityAction_1_t3156_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSidescrollControl_t221_m34060_MethodInfo/* Method Usage */,
	&SidescrollControl_t221_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16005_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3155_0_0_0;
extern Il2CppType InvokableCall_1_t3155_1_0_0;
struct InvokableCall_1_t3155;
extern Il2CppGenericClass InvokableCall_1_t3155_GenericClass;
TypeInfo InvokableCall_1_t3155_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3155_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3155_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3155_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3155_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3155_0_0_0/* byval_arg */
	, &InvokableCall_1_t3155_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3155_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3155_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3155)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<SidescrollControl>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<SidescrollControl>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SidescrollControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<SidescrollControl>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<SidescrollControl>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3156_UnityAction_1__ctor_m16004_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m16004_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SidescrollControl>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m16004_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3156_UnityAction_1__ctor_m16004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m16004_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControl_t221_0_0_0;
static ParameterInfo UnityAction_1_t3156_UnityAction_1_Invoke_m16005_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m16005_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SidescrollControl>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m16005_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3156_UnityAction_1_Invoke_m16005_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m16005_GenericMethod/* genericMethod */

};
extern Il2CppType SidescrollControl_t221_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3156_UnityAction_1_BeginInvoke_m16006_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SidescrollControl_t221_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m16006_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SidescrollControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m16006_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3156_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3156_UnityAction_1_BeginInvoke_m16006_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m16006_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3156_UnityAction_1_EndInvoke_m16007_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m16007_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SidescrollControl>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m16007_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3156_UnityAction_1_EndInvoke_m16007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m16007_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3156_MethodInfos[] =
{
	&UnityAction_1__ctor_m16004_MethodInfo,
	&UnityAction_1_Invoke_m16005_MethodInfo,
	&UnityAction_1_BeginInvoke_m16006_MethodInfo,
	&UnityAction_1_EndInvoke_m16007_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m16006_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m16007_MethodInfo;
static MethodInfo* UnityAction_1_t3156_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m16005_MethodInfo,
	&UnityAction_1_BeginInvoke_m16006_MethodInfo,
	&UnityAction_1_EndInvoke_m16007_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3156_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3156_1_0_0;
struct UnityAction_1_t3156;
extern Il2CppGenericClass UnityAction_1_t3156_GenericClass;
TypeInfo UnityAction_1_t3156_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3156_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3156_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3156_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3156_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3156_0_0_0/* byval_arg */
	, &UnityAction_1_t3156_1_0_0/* this_arg */
	, UnityAction_1_t3156_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3156_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3156)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6224_il2cpp_TypeInfo;

// SmoothFollow2D
#include "AssemblyU2DUnityScript_SmoothFollow2D.h"


// T System.Collections.Generic.IEnumerator`1<SmoothFollow2D>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<SmoothFollow2D>
extern MethodInfo IEnumerator_1_get_Current_m44540_MethodInfo;
static PropertyInfo IEnumerator_1_t6224____Current_PropertyInfo = 
{
	&IEnumerator_1_t6224_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6224_PropertyInfos[] =
{
	&IEnumerator_1_t6224____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44540_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<SmoothFollow2D>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44540_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6224_il2cpp_TypeInfo/* declaring_type */
	, &SmoothFollow2D_t222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44540_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6224_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44540_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6224_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6224_0_0_0;
extern Il2CppType IEnumerator_1_t6224_1_0_0;
struct IEnumerator_1_t6224;
extern Il2CppGenericClass IEnumerator_1_t6224_GenericClass;
TypeInfo IEnumerator_1_t6224_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6224_MethodInfos/* methods */
	, IEnumerator_1_t6224_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6224_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6224_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6224_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6224_0_0_0/* byval_arg */
	, &IEnumerator_1_t6224_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6224_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<SmoothFollow2D>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_155.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3157_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<SmoothFollow2D>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_155MethodDeclarations.h"

extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16012_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmoothFollow2D_t222_m34062_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<SmoothFollow2D>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<SmoothFollow2D>(System.Int32)
#define Array_InternalArray__get_Item_TisSmoothFollow2D_t222_m34062(__this, p0, method) (SmoothFollow2D_t222 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<SmoothFollow2D>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<SmoothFollow2D>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<SmoothFollow2D>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<SmoothFollow2D>::MoveNext()
// T System.Array/InternalEnumerator`1<SmoothFollow2D>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<SmoothFollow2D>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3157____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3157, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3157____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3157, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3157_FieldInfos[] =
{
	&InternalEnumerator_1_t3157____array_0_FieldInfo,
	&InternalEnumerator_1_t3157____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3157____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3157_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3157____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3157_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16012_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3157_PropertyInfos[] =
{
	&InternalEnumerator_1_t3157____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3157____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3157_InternalEnumerator_1__ctor_m16008_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16008_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SmoothFollow2D>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16008_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3157_InternalEnumerator_1__ctor_m16008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16008_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<SmoothFollow2D>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16010_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SmoothFollow2D>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16010_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16010_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16011_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<SmoothFollow2D>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16011_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16011_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16012_GenericMethod;
// T System.Array/InternalEnumerator`1<SmoothFollow2D>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16012_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* declaring_type */
	, &SmoothFollow2D_t222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16012_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3157_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16008_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_MethodInfo,
	&InternalEnumerator_1_Dispose_m16010_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16011_MethodInfo,
	&InternalEnumerator_1_get_Current_m16012_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16011_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16010_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3157_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16009_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16011_MethodInfo,
	&InternalEnumerator_1_Dispose_m16010_MethodInfo,
	&InternalEnumerator_1_get_Current_m16012_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3157_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6224_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3157_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6224_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3157_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16012_MethodInfo/* Method Usage */,
	&SmoothFollow2D_t222_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmoothFollow2D_t222_m34062_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3157_0_0_0;
extern Il2CppType InternalEnumerator_1_t3157_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3157_GenericClass;
TypeInfo InternalEnumerator_1_t3157_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3157_MethodInfos/* methods */
	, InternalEnumerator_1_t3157_PropertyInfos/* properties */
	, InternalEnumerator_1_t3157_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3157_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3157_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3157_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3157_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3157_1_0_0/* this_arg */
	, InternalEnumerator_1_t3157_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3157_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3157_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3157)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7961_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<SmoothFollow2D>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<SmoothFollow2D>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<SmoothFollow2D>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<SmoothFollow2D>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<SmoothFollow2D>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<SmoothFollow2D>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<SmoothFollow2D>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<SmoothFollow2D>
extern MethodInfo ICollection_1_get_Count_m44541_MethodInfo;
static PropertyInfo ICollection_1_t7961____Count_PropertyInfo = 
{
	&ICollection_1_t7961_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44542_MethodInfo;
static PropertyInfo ICollection_1_t7961____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7961_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44542_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7961_PropertyInfos[] =
{
	&ICollection_1_t7961____Count_PropertyInfo,
	&ICollection_1_t7961____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44541_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<SmoothFollow2D>::get_Count()
MethodInfo ICollection_1_get_Count_m44541_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44541_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44542_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SmoothFollow2D>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44542_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44542_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo ICollection_1_t7961_ICollection_1_Add_m44543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44543_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SmoothFollow2D>::Add(T)
MethodInfo ICollection_1_Add_m44543_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7961_ICollection_1_Add_m44543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44543_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44544_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SmoothFollow2D>::Clear()
MethodInfo ICollection_1_Clear_m44544_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44544_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo ICollection_1_t7961_ICollection_1_Contains_m44545_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44545_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SmoothFollow2D>::Contains(T)
MethodInfo ICollection_1_Contains_m44545_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7961_ICollection_1_Contains_m44545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44545_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2DU5BU5D_t5781_0_0_0;
extern Il2CppType SmoothFollow2DU5BU5D_t5781_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7961_ICollection_1_CopyTo_m44546_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2DU5BU5D_t5781_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44546_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SmoothFollow2D>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44546_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7961_ICollection_1_CopyTo_m44546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44546_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo ICollection_1_t7961_ICollection_1_Remove_m44547_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44547_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SmoothFollow2D>::Remove(T)
MethodInfo ICollection_1_Remove_m44547_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7961_ICollection_1_Remove_m44547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44547_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7961_MethodInfos[] =
{
	&ICollection_1_get_Count_m44541_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44542_MethodInfo,
	&ICollection_1_Add_m44543_MethodInfo,
	&ICollection_1_Clear_m44544_MethodInfo,
	&ICollection_1_Contains_m44545_MethodInfo,
	&ICollection_1_CopyTo_m44546_MethodInfo,
	&ICollection_1_Remove_m44547_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7963_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7961_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7963_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7961_0_0_0;
extern Il2CppType ICollection_1_t7961_1_0_0;
struct ICollection_1_t7961;
extern Il2CppGenericClass ICollection_1_t7961_GenericClass;
TypeInfo ICollection_1_t7961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7961_MethodInfos/* methods */
	, ICollection_1_t7961_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7961_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7961_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7961_0_0_0/* byval_arg */
	, &ICollection_1_t7961_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7961_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SmoothFollow2D>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<SmoothFollow2D>
extern Il2CppType IEnumerator_1_t6224_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44548_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SmoothFollow2D>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44548_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7963_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44548_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7963_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44548_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7963_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7963_0_0_0;
extern Il2CppType IEnumerable_1_t7963_1_0_0;
struct IEnumerable_1_t7963;
extern Il2CppGenericClass IEnumerable_1_t7963_GenericClass;
TypeInfo IEnumerable_1_t7963_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7963_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7963_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7963_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7963_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7963_0_0_0/* byval_arg */
	, &IEnumerable_1_t7963_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7963_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7962_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<SmoothFollow2D>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<SmoothFollow2D>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<SmoothFollow2D>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<SmoothFollow2D>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<SmoothFollow2D>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<SmoothFollow2D>
extern MethodInfo IList_1_get_Item_m44549_MethodInfo;
extern MethodInfo IList_1_set_Item_m44550_MethodInfo;
static PropertyInfo IList_1_t7962____Item_PropertyInfo = 
{
	&IList_1_t7962_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44549_MethodInfo/* get */
	, &IList_1_set_Item_m44550_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7962_PropertyInfos[] =
{
	&IList_1_t7962____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo IList_1_t7962_IList_1_IndexOf_m44551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44551_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<SmoothFollow2D>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44551_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7962_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7962_IList_1_IndexOf_m44551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44551_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo IList_1_t7962_IList_1_Insert_m44552_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44552_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SmoothFollow2D>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44552_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7962_IList_1_Insert_m44552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44552_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7962_IList_1_RemoveAt_m44553_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44553_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SmoothFollow2D>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44553_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7962_IList_1_RemoveAt_m44553_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44553_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7962_IList_1_get_Item_m44549_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44549_GenericMethod;
// T System.Collections.Generic.IList`1<SmoothFollow2D>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44549_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7962_il2cpp_TypeInfo/* declaring_type */
	, &SmoothFollow2D_t222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7962_IList_1_get_Item_m44549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44549_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo IList_1_t7962_IList_1_set_Item_m44550_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44550_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SmoothFollow2D>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44550_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7962_IList_1_set_Item_m44550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44550_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7962_MethodInfos[] =
{
	&IList_1_IndexOf_m44551_MethodInfo,
	&IList_1_Insert_m44552_MethodInfo,
	&IList_1_RemoveAt_m44553_MethodInfo,
	&IList_1_get_Item_m44549_MethodInfo,
	&IList_1_set_Item_m44550_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7962_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7961_il2cpp_TypeInfo,
	&IEnumerable_1_t7963_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7962_0_0_0;
extern Il2CppType IList_1_t7962_1_0_0;
struct IList_1_t7962;
extern Il2CppGenericClass IList_1_t7962_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7962_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7962_MethodInfos/* methods */
	, IList_1_t7962_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7962_il2cpp_TypeInfo/* element_class */
	, IList_1_t7962_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7962_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7962_0_0_0/* byval_arg */
	, &IList_1_t7962_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7962_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_67.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3158_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_67MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_63.h"
extern TypeInfo InvokableCall_1_t3159_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_63MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m16015_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m16017_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3158____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3158_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3158, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3158_FieldInfos[] =
{
	&CachedInvokableCall_1_t3158____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3158_CachedInvokableCall_1__ctor_m16013_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m16013_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m16013_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3158_CachedInvokableCall_1__ctor_m16013_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m16013_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3158_CachedInvokableCall_1_Invoke_m16014_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m16014_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m16014_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3158_CachedInvokableCall_1_Invoke_m16014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m16014_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3158_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m16013_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16014_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m16014_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m16018_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3158_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16014_MethodInfo,
	&InvokableCall_1_Find_m16018_MethodInfo,
};
extern Il2CppType UnityAction_1_t3160_0_0_0;
extern TypeInfo UnityAction_1_t3160_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSmoothFollow2D_t222_m34072_MethodInfo;
extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m16020_MethodInfo;
extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3158_RGCTXData[8] = 
{
	&UnityAction_1_t3160_0_0_0/* Type Usage */,
	&UnityAction_1_t3160_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSmoothFollow2D_t222_m34072_MethodInfo/* Method Usage */,
	&SmoothFollow2D_t222_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16020_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m16015_MethodInfo/* Method Usage */,
	&SmoothFollow2D_t222_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m16017_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3158_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3158_1_0_0;
struct CachedInvokableCall_1_t3158;
extern Il2CppGenericClass CachedInvokableCall_1_t3158_GenericClass;
TypeInfo CachedInvokableCall_1_t3158_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3158_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3158_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3158_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3158_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3158_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3158_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3158_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3158_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3158_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3158)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_70.h"
extern TypeInfo UnityAction_1_t3160_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_70MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SmoothFollow2D>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SmoothFollow2D>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSmoothFollow2D_t222_m34072(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<SmoothFollow2D>
extern Il2CppType UnityAction_1_t3160_0_0_1;
FieldInfo InvokableCall_1_t3159____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3160_0_0_1/* type */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3159, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3159_FieldInfos[] =
{
	&InvokableCall_1_t3159____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3159_InvokableCall_1__ctor_m16015_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16015_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m16015_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3159_InvokableCall_1__ctor_m16015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16015_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3160_0_0_0;
static ParameterInfo InvokableCall_1_t3159_InvokableCall_1__ctor_m16016_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3160_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16016_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m16016_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3159_InvokableCall_1__ctor_m16016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16016_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3159_InvokableCall_1_Invoke_m16017_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m16017_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m16017_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3159_InvokableCall_1_Invoke_m16017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m16017_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3159_InvokableCall_1_Find_m16018_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m16018_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m16018_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3159_InvokableCall_1_Find_m16018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m16018_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3159_MethodInfos[] =
{
	&InvokableCall_1__ctor_m16015_MethodInfo,
	&InvokableCall_1__ctor_m16016_MethodInfo,
	&InvokableCall_1_Invoke_m16017_MethodInfo,
	&InvokableCall_1_Find_m16018_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3159_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m16017_MethodInfo,
	&InvokableCall_1_Find_m16018_MethodInfo,
};
extern TypeInfo UnityAction_1_t3160_il2cpp_TypeInfo;
extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3159_RGCTXData[5] = 
{
	&UnityAction_1_t3160_0_0_0/* Type Usage */,
	&UnityAction_1_t3160_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSmoothFollow2D_t222_m34072_MethodInfo/* Method Usage */,
	&SmoothFollow2D_t222_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16020_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3159_0_0_0;
extern Il2CppType InvokableCall_1_t3159_1_0_0;
struct InvokableCall_1_t3159;
extern Il2CppGenericClass InvokableCall_1_t3159_GenericClass;
TypeInfo InvokableCall_1_t3159_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3159_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3159_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3159_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3159_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3159_0_0_0/* byval_arg */
	, &InvokableCall_1_t3159_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3159_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3159_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3159)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<SmoothFollow2D>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<SmoothFollow2D>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SmoothFollow2D>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<SmoothFollow2D>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<SmoothFollow2D>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3160_UnityAction_1__ctor_m16019_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m16019_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SmoothFollow2D>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m16019_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3160_UnityAction_1__ctor_m16019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m16019_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
static ParameterInfo UnityAction_1_t3160_UnityAction_1_Invoke_m16020_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m16020_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SmoothFollow2D>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m16020_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3160_UnityAction_1_Invoke_m16020_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m16020_GenericMethod/* genericMethod */

};
extern Il2CppType SmoothFollow2D_t222_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3160_UnityAction_1_BeginInvoke_m16021_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SmoothFollow2D_t222_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m16021_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SmoothFollow2D>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m16021_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3160_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3160_UnityAction_1_BeginInvoke_m16021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m16021_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3160_UnityAction_1_EndInvoke_m16022_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m16022_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SmoothFollow2D>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m16022_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3160_UnityAction_1_EndInvoke_m16022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m16022_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3160_MethodInfos[] =
{
	&UnityAction_1__ctor_m16019_MethodInfo,
	&UnityAction_1_Invoke_m16020_MethodInfo,
	&UnityAction_1_BeginInvoke_m16021_MethodInfo,
	&UnityAction_1_EndInvoke_m16022_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m16021_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m16022_MethodInfo;
static MethodInfo* UnityAction_1_t3160_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m16020_MethodInfo,
	&UnityAction_1_BeginInvoke_m16021_MethodInfo,
	&UnityAction_1_EndInvoke_m16022_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3160_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3160_1_0_0;
struct UnityAction_1_t3160;
extern Il2CppGenericClass UnityAction_1_t3160_GenericClass;
TypeInfo UnityAction_1_t3160_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3160_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3160_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3160_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3160_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3160_0_0_0/* byval_arg */
	, &UnityAction_1_t3160_1_0_0/* this_arg */
	, UnityAction_1_t3160_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3160_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3160)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6226_il2cpp_TypeInfo;

// ControlState
#include "AssemblyU2DUnityScript_ControlState.h"


// T System.Collections.Generic.IEnumerator`1<ControlState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ControlState>
extern MethodInfo IEnumerator_1_get_Current_m44554_MethodInfo;
static PropertyInfo IEnumerator_1_t6226____Current_PropertyInfo = 
{
	&IEnumerator_1_t6226_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6226_PropertyInfos[] =
{
	&IEnumerator_1_t6226____Current_PropertyInfo,
	NULL
};
extern Il2CppType ControlState_t223_0_0_0;
extern void* RuntimeInvoker_ControlState_t223 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44554_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ControlState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44554_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6226_il2cpp_TypeInfo/* declaring_type */
	, &ControlState_t223_0_0_0/* return_type */
	, RuntimeInvoker_ControlState_t223/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44554_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6226_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44554_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6226_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6226_0_0_0;
extern Il2CppType IEnumerator_1_t6226_1_0_0;
struct IEnumerator_1_t6226;
extern Il2CppGenericClass IEnumerator_1_t6226_GenericClass;
TypeInfo IEnumerator_1_t6226_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6226_MethodInfos/* methods */
	, IEnumerator_1_t6226_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6226_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6226_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6226_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6226_0_0_0/* byval_arg */
	, &IEnumerator_1_t6226_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6226_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ControlState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_156.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3161_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ControlState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_156MethodDeclarations.h"

extern TypeInfo ControlState_t223_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16027_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisControlState_t223_m34074_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ControlState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ControlState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisControlState_t223_m34074 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<ControlState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m16023_MethodInfo;
 void InternalEnumerator_1__ctor_m16023 (InternalEnumerator_1_t3161 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ControlState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024 (InternalEnumerator_1_t3161 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m16027(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m16027_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ControlState_t223_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ControlState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m16025_MethodInfo;
 void InternalEnumerator_1_Dispose_m16025 (InternalEnumerator_1_t3161 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ControlState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m16026_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m16026 (InternalEnumerator_1_t3161 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ControlState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m16027 (InternalEnumerator_1_t3161 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisControlState_t223_m34074(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisControlState_t223_m34074_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<ControlState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3161____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3161, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3161____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3161, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3161_FieldInfos[] =
{
	&InternalEnumerator_1_t3161____array_0_FieldInfo,
	&InternalEnumerator_1_t3161____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3161____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3161_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3161____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3161_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16027_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3161_PropertyInfos[] =
{
	&InternalEnumerator_1_t3161____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3161____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3161_InternalEnumerator_1__ctor_m16023_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16023_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ControlState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16023_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m16023/* method */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3161_InternalEnumerator_1__ctor_m16023_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16023_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ControlState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024/* method */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16025_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ControlState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16025_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m16025/* method */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16025_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16026_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ControlState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16026_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m16026/* method */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16026_GenericMethod/* genericMethod */

};
extern Il2CppType ControlState_t223_0_0_0;
extern void* RuntimeInvoker_ControlState_t223 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16027_GenericMethod;
// T System.Array/InternalEnumerator`1<ControlState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16027_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m16027/* method */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* declaring_type */
	, &ControlState_t223_0_0_0/* return_type */
	, RuntimeInvoker_ControlState_t223/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16027_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3161_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16023_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_MethodInfo,
	&InternalEnumerator_1_Dispose_m16025_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16026_MethodInfo,
	&InternalEnumerator_1_get_Current_m16027_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3161_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16024_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16026_MethodInfo,
	&InternalEnumerator_1_Dispose_m16025_MethodInfo,
	&InternalEnumerator_1_get_Current_m16027_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3161_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6226_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3161_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6226_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3161_0_0_0;
extern Il2CppType InternalEnumerator_1_t3161_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3161_GenericClass;
TypeInfo InternalEnumerator_1_t3161_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3161_MethodInfos/* methods */
	, InternalEnumerator_1_t3161_PropertyInfos/* properties */
	, InternalEnumerator_1_t3161_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3161_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3161_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3161_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3161_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3161_1_0_0/* this_arg */
	, InternalEnumerator_1_t3161_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3161_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3161)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7964_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<ControlState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ControlState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ControlState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ControlState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ControlState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ControlState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ControlState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ControlState>
extern MethodInfo ICollection_1_get_Count_m44555_MethodInfo;
static PropertyInfo ICollection_1_t7964____Count_PropertyInfo = 
{
	&ICollection_1_t7964_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44555_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44556_MethodInfo;
static PropertyInfo ICollection_1_t7964____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7964_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7964_PropertyInfos[] =
{
	&ICollection_1_t7964____Count_PropertyInfo,
	&ICollection_1_t7964____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44555_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ControlState>::get_Count()
MethodInfo ICollection_1_get_Count_m44555_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44555_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44556_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ControlState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44556_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44556_GenericMethod/* genericMethod */

};
extern Il2CppType ControlState_t223_0_0_0;
extern Il2CppType ControlState_t223_0_0_0;
static ParameterInfo ICollection_1_t7964_ICollection_1_Add_m44557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ControlState_t223_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44557_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ControlState>::Add(T)
MethodInfo ICollection_1_Add_m44557_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t7964_ICollection_1_Add_m44557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44557_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44558_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ControlState>::Clear()
MethodInfo ICollection_1_Clear_m44558_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44558_GenericMethod/* genericMethod */

};
extern Il2CppType ControlState_t223_0_0_0;
static ParameterInfo ICollection_1_t7964_ICollection_1_Contains_m44559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ControlState_t223_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44559_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ControlState>::Contains(T)
MethodInfo ICollection_1_Contains_m44559_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7964_ICollection_1_Contains_m44559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44559_GenericMethod/* genericMethod */

};
extern Il2CppType ControlStateU5BU5D_t5782_0_0_0;
extern Il2CppType ControlStateU5BU5D_t5782_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7964_ICollection_1_CopyTo_m44560_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ControlStateU5BU5D_t5782_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44560_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ControlState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44560_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7964_ICollection_1_CopyTo_m44560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44560_GenericMethod/* genericMethod */

};
extern Il2CppType ControlState_t223_0_0_0;
static ParameterInfo ICollection_1_t7964_ICollection_1_Remove_m44561_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ControlState_t223_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44561_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ControlState>::Remove(T)
MethodInfo ICollection_1_Remove_m44561_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7964_ICollection_1_Remove_m44561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44561_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7964_MethodInfos[] =
{
	&ICollection_1_get_Count_m44555_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44556_MethodInfo,
	&ICollection_1_Add_m44557_MethodInfo,
	&ICollection_1_Clear_m44558_MethodInfo,
	&ICollection_1_Contains_m44559_MethodInfo,
	&ICollection_1_CopyTo_m44560_MethodInfo,
	&ICollection_1_Remove_m44561_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7966_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7964_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7966_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7964_0_0_0;
extern Il2CppType ICollection_1_t7964_1_0_0;
struct ICollection_1_t7964;
extern Il2CppGenericClass ICollection_1_t7964_GenericClass;
TypeInfo ICollection_1_t7964_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7964_MethodInfos/* methods */
	, ICollection_1_t7964_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7964_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7964_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7964_0_0_0/* byval_arg */
	, &ICollection_1_t7964_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7964_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ControlState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ControlState>
extern Il2CppType IEnumerator_1_t6226_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44562_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ControlState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44562_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7966_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44562_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7966_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44562_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7966_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7966_0_0_0;
extern Il2CppType IEnumerable_1_t7966_1_0_0;
struct IEnumerable_1_t7966;
extern Il2CppGenericClass IEnumerable_1_t7966_GenericClass;
TypeInfo IEnumerable_1_t7966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7966_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7966_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7966_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7966_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7966_0_0_0/* byval_arg */
	, &IEnumerable_1_t7966_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7966_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7965_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<ControlState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ControlState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ControlState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ControlState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ControlState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ControlState>
extern MethodInfo IList_1_get_Item_m44563_MethodInfo;
extern MethodInfo IList_1_set_Item_m44564_MethodInfo;
static PropertyInfo IList_1_t7965____Item_PropertyInfo = 
{
	&IList_1_t7965_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44563_MethodInfo/* get */
	, &IList_1_set_Item_m44564_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7965_PropertyInfos[] =
{
	&IList_1_t7965____Item_PropertyInfo,
	NULL
};
extern Il2CppType ControlState_t223_0_0_0;
static ParameterInfo IList_1_t7965_IList_1_IndexOf_m44565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ControlState_t223_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44565_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ControlState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44565_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7965_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7965_IList_1_IndexOf_m44565_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44565_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ControlState_t223_0_0_0;
static ParameterInfo IList_1_t7965_IList_1_Insert_m44566_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ControlState_t223_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44566_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ControlState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44566_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7965_IList_1_Insert_m44566_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44566_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7965_IList_1_RemoveAt_m44567_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44567_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ControlState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44567_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7965_IList_1_RemoveAt_m44567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44567_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7965_IList_1_get_Item_m44563_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ControlState_t223_0_0_0;
extern void* RuntimeInvoker_ControlState_t223_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44563_GenericMethod;
// T System.Collections.Generic.IList`1<ControlState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44563_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7965_il2cpp_TypeInfo/* declaring_type */
	, &ControlState_t223_0_0_0/* return_type */
	, RuntimeInvoker_ControlState_t223_Int32_t123/* invoker_method */
	, IList_1_t7965_IList_1_get_Item_m44563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44563_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ControlState_t223_0_0_0;
static ParameterInfo IList_1_t7965_IList_1_set_Item_m44564_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ControlState_t223_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44564_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ControlState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44564_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7965_IList_1_set_Item_m44564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44564_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7965_MethodInfos[] =
{
	&IList_1_IndexOf_m44565_MethodInfo,
	&IList_1_Insert_m44566_MethodInfo,
	&IList_1_RemoveAt_m44567_MethodInfo,
	&IList_1_get_Item_m44563_MethodInfo,
	&IList_1_set_Item_m44564_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7965_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7964_il2cpp_TypeInfo,
	&IEnumerable_1_t7966_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7965_0_0_0;
extern Il2CppType IList_1_t7965_1_0_0;
struct IList_1_t7965;
extern Il2CppGenericClass IList_1_t7965_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7965_MethodInfos/* methods */
	, IList_1_t7965_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7965_il2cpp_TypeInfo/* element_class */
	, IList_1_t7965_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7965_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7965_0_0_0/* byval_arg */
	, &IList_1_t7965_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7965_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6228_il2cpp_TypeInfo;

// TapControl
#include "AssemblyU2DUnityScript_TapControl.h"


// T System.Collections.Generic.IEnumerator`1<TapControl>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<TapControl>
extern MethodInfo IEnumerator_1_get_Current_m44568_MethodInfo;
static PropertyInfo IEnumerator_1_t6228____Current_PropertyInfo = 
{
	&IEnumerator_1_t6228_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6228_PropertyInfos[] =
{
	&IEnumerator_1_t6228____Current_PropertyInfo,
	NULL
};
extern Il2CppType TapControl_t226_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44568_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<TapControl>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44568_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6228_il2cpp_TypeInfo/* declaring_type */
	, &TapControl_t226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44568_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6228_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44568_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6228_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6228_0_0_0;
extern Il2CppType IEnumerator_1_t6228_1_0_0;
struct IEnumerator_1_t6228;
extern Il2CppGenericClass IEnumerator_1_t6228_GenericClass;
TypeInfo IEnumerator_1_t6228_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6228_MethodInfos/* methods */
	, IEnumerator_1_t6228_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6228_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6228_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6228_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6228_0_0_0/* byval_arg */
	, &IEnumerator_1_t6228_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6228_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<TapControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_157.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3162_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<TapControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_157MethodDeclarations.h"

extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16032_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTapControl_t226_m34085_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<TapControl>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<TapControl>(System.Int32)
#define Array_InternalArray__get_Item_TisTapControl_t226_m34085(__this, p0, method) (TapControl_t226 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<TapControl>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<TapControl>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<TapControl>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<TapControl>::MoveNext()
// T System.Array/InternalEnumerator`1<TapControl>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<TapControl>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3162____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3162, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3162____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3162, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3162_FieldInfos[] =
{
	&InternalEnumerator_1_t3162____array_0_FieldInfo,
	&InternalEnumerator_1_t3162____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3162____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3162_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3162____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3162_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16032_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3162_PropertyInfos[] =
{
	&InternalEnumerator_1_t3162____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3162____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3162_InternalEnumerator_1__ctor_m16028_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16028_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<TapControl>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16028_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3162_InternalEnumerator_1__ctor_m16028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16028_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<TapControl>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16030_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<TapControl>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16030_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16030_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16031_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<TapControl>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16031_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16031_GenericMethod/* genericMethod */

};
extern Il2CppType TapControl_t226_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16032_GenericMethod;
// T System.Array/InternalEnumerator`1<TapControl>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16032_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* declaring_type */
	, &TapControl_t226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16032_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3162_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16028_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_MethodInfo,
	&InternalEnumerator_1_Dispose_m16030_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16031_MethodInfo,
	&InternalEnumerator_1_get_Current_m16032_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16031_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16030_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3162_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16029_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16031_MethodInfo,
	&InternalEnumerator_1_Dispose_m16030_MethodInfo,
	&InternalEnumerator_1_get_Current_m16032_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3162_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6228_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3162_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6228_il2cpp_TypeInfo, 7},
};
extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3162_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16032_MethodInfo/* Method Usage */,
	&TapControl_t226_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTapControl_t226_m34085_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3162_0_0_0;
extern Il2CppType InternalEnumerator_1_t3162_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3162_GenericClass;
TypeInfo InternalEnumerator_1_t3162_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3162_MethodInfos/* methods */
	, InternalEnumerator_1_t3162_PropertyInfos/* properties */
	, InternalEnumerator_1_t3162_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3162_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3162_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3162_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3162_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3162_1_0_0/* this_arg */
	, InternalEnumerator_1_t3162_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3162_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3162_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3162)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7967_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<TapControl>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<TapControl>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<TapControl>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<TapControl>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<TapControl>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<TapControl>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<TapControl>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<TapControl>
extern MethodInfo ICollection_1_get_Count_m44569_MethodInfo;
static PropertyInfo ICollection_1_t7967____Count_PropertyInfo = 
{
	&ICollection_1_t7967_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44569_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44570_MethodInfo;
static PropertyInfo ICollection_1_t7967____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7967_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44570_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7967_PropertyInfos[] =
{
	&ICollection_1_t7967____Count_PropertyInfo,
	&ICollection_1_t7967____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44569_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<TapControl>::get_Count()
MethodInfo ICollection_1_get_Count_m44569_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44569_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44570_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<TapControl>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44570_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44570_GenericMethod/* genericMethod */

};
extern Il2CppType TapControl_t226_0_0_0;
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo ICollection_1_t7967_ICollection_1_Add_m44571_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44571_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<TapControl>::Add(T)
MethodInfo ICollection_1_Add_m44571_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7967_ICollection_1_Add_m44571_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44571_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44572_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<TapControl>::Clear()
MethodInfo ICollection_1_Clear_m44572_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44572_GenericMethod/* genericMethod */

};
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo ICollection_1_t7967_ICollection_1_Contains_m44573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44573_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<TapControl>::Contains(T)
MethodInfo ICollection_1_Contains_m44573_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7967_ICollection_1_Contains_m44573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44573_GenericMethod/* genericMethod */

};
extern Il2CppType TapControlU5BU5D_t5783_0_0_0;
extern Il2CppType TapControlU5BU5D_t5783_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7967_ICollection_1_CopyTo_m44574_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TapControlU5BU5D_t5783_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44574_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<TapControl>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44574_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7967_ICollection_1_CopyTo_m44574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44574_GenericMethod/* genericMethod */

};
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo ICollection_1_t7967_ICollection_1_Remove_m44575_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44575_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<TapControl>::Remove(T)
MethodInfo ICollection_1_Remove_m44575_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7967_ICollection_1_Remove_m44575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44575_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7967_MethodInfos[] =
{
	&ICollection_1_get_Count_m44569_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44570_MethodInfo,
	&ICollection_1_Add_m44571_MethodInfo,
	&ICollection_1_Clear_m44572_MethodInfo,
	&ICollection_1_Contains_m44573_MethodInfo,
	&ICollection_1_CopyTo_m44574_MethodInfo,
	&ICollection_1_Remove_m44575_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7969_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7967_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7969_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7967_0_0_0;
extern Il2CppType ICollection_1_t7967_1_0_0;
struct ICollection_1_t7967;
extern Il2CppGenericClass ICollection_1_t7967_GenericClass;
TypeInfo ICollection_1_t7967_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7967_MethodInfos/* methods */
	, ICollection_1_t7967_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7967_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7967_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7967_0_0_0/* byval_arg */
	, &ICollection_1_t7967_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7967_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<TapControl>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<TapControl>
extern Il2CppType IEnumerator_1_t6228_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44576_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<TapControl>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44576_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7969_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6228_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44576_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7969_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44576_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7969_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7969_0_0_0;
extern Il2CppType IEnumerable_1_t7969_1_0_0;
struct IEnumerable_1_t7969;
extern Il2CppGenericClass IEnumerable_1_t7969_GenericClass;
TypeInfo IEnumerable_1_t7969_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7969_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7969_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7969_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7969_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7969_0_0_0/* byval_arg */
	, &IEnumerable_1_t7969_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7969_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7968_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<TapControl>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<TapControl>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<TapControl>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<TapControl>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<TapControl>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<TapControl>
extern MethodInfo IList_1_get_Item_m44577_MethodInfo;
extern MethodInfo IList_1_set_Item_m44578_MethodInfo;
static PropertyInfo IList_1_t7968____Item_PropertyInfo = 
{
	&IList_1_t7968_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44577_MethodInfo/* get */
	, &IList_1_set_Item_m44578_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7968_PropertyInfos[] =
{
	&IList_1_t7968____Item_PropertyInfo,
	NULL
};
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo IList_1_t7968_IList_1_IndexOf_m44579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44579_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<TapControl>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44579_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7968_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7968_IList_1_IndexOf_m44579_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44579_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo IList_1_t7968_IList_1_Insert_m44580_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44580_GenericMethod;
// System.Void System.Collections.Generic.IList`1<TapControl>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44580_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7968_IList_1_Insert_m44580_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44580_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7968_IList_1_RemoveAt_m44581_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44581_GenericMethod;
// System.Void System.Collections.Generic.IList`1<TapControl>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44581_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7968_IList_1_RemoveAt_m44581_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44581_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7968_IList_1_get_Item_m44577_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TapControl_t226_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44577_GenericMethod;
// T System.Collections.Generic.IList`1<TapControl>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44577_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7968_il2cpp_TypeInfo/* declaring_type */
	, &TapControl_t226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7968_IList_1_get_Item_m44577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44577_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo IList_1_t7968_IList_1_set_Item_m44578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44578_GenericMethod;
// System.Void System.Collections.Generic.IList`1<TapControl>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44578_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7968_IList_1_set_Item_m44578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44578_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7968_MethodInfos[] =
{
	&IList_1_IndexOf_m44579_MethodInfo,
	&IList_1_Insert_m44580_MethodInfo,
	&IList_1_RemoveAt_m44581_MethodInfo,
	&IList_1_get_Item_m44577_MethodInfo,
	&IList_1_set_Item_m44578_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7968_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7967_il2cpp_TypeInfo,
	&IEnumerable_1_t7969_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7968_0_0_0;
extern Il2CppType IList_1_t7968_1_0_0;
struct IList_1_t7968;
extern Il2CppGenericClass IList_1_t7968_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7968_MethodInfos/* methods */
	, IList_1_t7968_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7968_il2cpp_TypeInfo/* element_class */
	, IList_1_t7968_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7968_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7968_0_0_0/* byval_arg */
	, &IList_1_t7968_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7968_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<TapControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_68.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3163_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<TapControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_68MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<TapControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_64.h"
extern TypeInfo InvokableCall_1_t3164_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<TapControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_64MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m16035_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m16037_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<TapControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<TapControl>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<TapControl>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3163____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3163_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3163, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3163_FieldInfos[] =
{
	&CachedInvokableCall_1_t3163____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3163_CachedInvokableCall_1__ctor_m16033_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m16033_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<TapControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m16033_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3163_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3163_CachedInvokableCall_1__ctor_m16033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m16033_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3163_CachedInvokableCall_1_Invoke_m16034_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m16034_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<TapControl>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m16034_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3163_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3163_CachedInvokableCall_1_Invoke_m16034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m16034_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3163_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m16033_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16034_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m16034_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m16038_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3163_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16034_MethodInfo,
	&InvokableCall_1_Find_m16038_MethodInfo,
};
extern Il2CppType UnityAction_1_t3165_0_0_0;
extern TypeInfo UnityAction_1_t3165_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTapControl_t226_m34095_MethodInfo;
extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m16040_MethodInfo;
extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3163_RGCTXData[8] = 
{
	&UnityAction_1_t3165_0_0_0/* Type Usage */,
	&UnityAction_1_t3165_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTapControl_t226_m34095_MethodInfo/* Method Usage */,
	&TapControl_t226_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16040_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m16035_MethodInfo/* Method Usage */,
	&TapControl_t226_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m16037_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3163_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3163_1_0_0;
struct CachedInvokableCall_1_t3163;
extern Il2CppGenericClass CachedInvokableCall_1_t3163_GenericClass;
TypeInfo CachedInvokableCall_1_t3163_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3163_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3163_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3163_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3163_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3163_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3163_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3163_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3163_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3163_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3163)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<TapControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_71.h"
extern TypeInfo UnityAction_1_t3165_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<TapControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_71MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<TapControl>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<TapControl>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTapControl_t226_m34095(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<TapControl>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<TapControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<TapControl>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<TapControl>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<TapControl>
extern Il2CppType UnityAction_1_t3165_0_0_1;
FieldInfo InvokableCall_1_t3164____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3165_0_0_1/* type */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3164, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3164_FieldInfos[] =
{
	&InvokableCall_1_t3164____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3164_InvokableCall_1__ctor_m16035_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16035_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<TapControl>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m16035_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3164_InvokableCall_1__ctor_m16035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16035_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3165_0_0_0;
static ParameterInfo InvokableCall_1_t3164_InvokableCall_1__ctor_m16036_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3165_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16036_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<TapControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m16036_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3164_InvokableCall_1__ctor_m16036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16036_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3164_InvokableCall_1_Invoke_m16037_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m16037_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<TapControl>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m16037_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3164_InvokableCall_1_Invoke_m16037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m16037_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3164_InvokableCall_1_Find_m16038_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m16038_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<TapControl>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m16038_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3164_InvokableCall_1_Find_m16038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m16038_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3164_MethodInfos[] =
{
	&InvokableCall_1__ctor_m16035_MethodInfo,
	&InvokableCall_1__ctor_m16036_MethodInfo,
	&InvokableCall_1_Invoke_m16037_MethodInfo,
	&InvokableCall_1_Find_m16038_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3164_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m16037_MethodInfo,
	&InvokableCall_1_Find_m16038_MethodInfo,
};
extern TypeInfo UnityAction_1_t3165_il2cpp_TypeInfo;
extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3164_RGCTXData[5] = 
{
	&UnityAction_1_t3165_0_0_0/* Type Usage */,
	&UnityAction_1_t3165_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTapControl_t226_m34095_MethodInfo/* Method Usage */,
	&TapControl_t226_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16040_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3164_0_0_0;
extern Il2CppType InvokableCall_1_t3164_1_0_0;
struct InvokableCall_1_t3164;
extern Il2CppGenericClass InvokableCall_1_t3164_GenericClass;
TypeInfo InvokableCall_1_t3164_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3164_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3164_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3164_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3164_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3164_0_0_0/* byval_arg */
	, &InvokableCall_1_t3164_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3164_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3164_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3164)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<TapControl>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<TapControl>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<TapControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<TapControl>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<TapControl>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3165_UnityAction_1__ctor_m16039_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m16039_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<TapControl>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m16039_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3165_UnityAction_1__ctor_m16039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m16039_GenericMethod/* genericMethod */

};
extern Il2CppType TapControl_t226_0_0_0;
static ParameterInfo UnityAction_1_t3165_UnityAction_1_Invoke_m16040_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m16040_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<TapControl>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m16040_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3165_UnityAction_1_Invoke_m16040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m16040_GenericMethod/* genericMethod */

};
extern Il2CppType TapControl_t226_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3165_UnityAction_1_BeginInvoke_m16041_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TapControl_t226_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m16041_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<TapControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m16041_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3165_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3165_UnityAction_1_BeginInvoke_m16041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m16041_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3165_UnityAction_1_EndInvoke_m16042_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m16042_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<TapControl>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m16042_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3165_UnityAction_1_EndInvoke_m16042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m16042_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3165_MethodInfos[] =
{
	&UnityAction_1__ctor_m16039_MethodInfo,
	&UnityAction_1_Invoke_m16040_MethodInfo,
	&UnityAction_1_BeginInvoke_m16041_MethodInfo,
	&UnityAction_1_EndInvoke_m16042_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m16041_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m16042_MethodInfo;
static MethodInfo* UnityAction_1_t3165_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m16040_MethodInfo,
	&UnityAction_1_BeginInvoke_m16041_MethodInfo,
	&UnityAction_1_EndInvoke_m16042_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3165_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3165_1_0_0;
struct UnityAction_1_t3165;
extern Il2CppGenericClass UnityAction_1_t3165_GenericClass;
TypeInfo UnityAction_1_t3165_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3165_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3165_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3165_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3165_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3165_0_0_0/* byval_arg */
	, &UnityAction_1_t3165_1_0_0/* this_arg */
	, UnityAction_1_t3165_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3165_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3165)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6229_il2cpp_TypeInfo;

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
extern MethodInfo IEnumerator_1_get_Current_m44582_MethodInfo;
static PropertyInfo IEnumerator_1_t6229____Current_PropertyInfo = 
{
	&IEnumerator_1_t6229_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44582_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6229_PropertyInfos[] =
{
	&IEnumerator_1_t6229____Current_PropertyInfo,
	NULL
};
extern Il2CppType Vector2_t99_0_0_0;
extern void* RuntimeInvoker_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44582_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44582_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6229_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t99_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44582_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6229_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44582_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6229_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6229_0_0_0;
extern Il2CppType IEnumerator_1_t6229_1_0_0;
struct IEnumerator_1_t6229;
extern Il2CppGenericClass IEnumerator_1_t6229_GenericClass;
TypeInfo IEnumerator_1_t6229_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6229_MethodInfos/* methods */
	, IEnumerator_1_t6229_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6229_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6229_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6229_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6229_0_0_0/* byval_arg */
	, &IEnumerator_1_t6229_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6229_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_158.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3166_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_158MethodDeclarations.h"

extern TypeInfo Vector2_t99_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16047_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVector2_t99_m34097_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
 Vector2_t99  Array_InternalArray__get_Item_TisVector2_t99_m34097 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m16043_MethodInfo;
 void InternalEnumerator_1__ctor_m16043 (InternalEnumerator_1_t3166 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044 (InternalEnumerator_1_t3166 * __this, MethodInfo* method){
	{
		Vector2_t99  L_0 = InternalEnumerator_1_get_Current_m16047(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m16047_MethodInfo);
		Vector2_t99  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Vector2_t99_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m16045_MethodInfo;
 void InternalEnumerator_1_Dispose_m16045 (InternalEnumerator_1_t3166 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m16046_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m16046 (InternalEnumerator_1_t3166 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
 Vector2_t99  InternalEnumerator_1_get_Current_m16047 (InternalEnumerator_1_t3166 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		Vector2_t99  L_8 = Array_InternalArray__get_Item_TisVector2_t99_m34097(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisVector2_t99_m34097_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Vector2>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3166____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3166, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3166____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3166, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3166_FieldInfos[] =
{
	&InternalEnumerator_1_t3166____array_0_FieldInfo,
	&InternalEnumerator_1_t3166____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3166____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3166_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3166____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3166_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16047_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3166_PropertyInfos[] =
{
	&InternalEnumerator_1_t3166____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3166____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3166_InternalEnumerator_1__ctor_m16043_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16043_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m16043/* method */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3166_InternalEnumerator_1__ctor_m16043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16043_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044/* method */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16045_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16045_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m16045/* method */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16045_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16046_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16046_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m16046/* method */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16046_GenericMethod/* genericMethod */

};
extern Il2CppType Vector2_t99_0_0_0;
extern void* RuntimeInvoker_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16047_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16047_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m16047/* method */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t99_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16047_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3166_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16043_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_MethodInfo,
	&InternalEnumerator_1_Dispose_m16045_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16046_MethodInfo,
	&InternalEnumerator_1_get_Current_m16047_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3166_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16044_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16046_MethodInfo,
	&InternalEnumerator_1_Dispose_m16045_MethodInfo,
	&InternalEnumerator_1_get_Current_m16047_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3166_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6229_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3166_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6229_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3166_0_0_0;
extern Il2CppType InternalEnumerator_1_t3166_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3166_GenericClass;
TypeInfo InternalEnumerator_1_t3166_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3166_MethodInfos/* methods */
	, InternalEnumerator_1_t3166_PropertyInfos/* properties */
	, InternalEnumerator_1_t3166_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3166_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3166_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3166_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3166_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3166_1_0_0/* this_arg */
	, InternalEnumerator_1_t3166_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3166_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3166)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7970_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
extern MethodInfo ICollection_1_get_Count_m44583_MethodInfo;
static PropertyInfo ICollection_1_t7970____Count_PropertyInfo = 
{
	&ICollection_1_t7970_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44583_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44584_MethodInfo;
static PropertyInfo ICollection_1_t7970____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7970_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44584_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7970_PropertyInfos[] =
{
	&ICollection_1_t7970____Count_PropertyInfo,
	&ICollection_1_t7970____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44583_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::get_Count()
MethodInfo ICollection_1_get_Count_m44583_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44583_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44584_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44584_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44584_GenericMethod/* genericMethod */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo ICollection_1_t7970_ICollection_1_Add_m44585_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44585_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Add(T)
MethodInfo ICollection_1_Add_m44585_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Vector2_t99/* invoker_method */
	, ICollection_1_t7970_ICollection_1_Add_m44585_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44585_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44586_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Clear()
MethodInfo ICollection_1_Clear_m44586_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44586_GenericMethod/* genericMethod */

};
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo ICollection_1_t7970_ICollection_1_Contains_m44587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44587_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Contains(T)
MethodInfo ICollection_1_Contains_m44587_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector2_t99/* invoker_method */
	, ICollection_1_t7970_ICollection_1_Contains_m44587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44587_GenericMethod/* genericMethod */

};
extern Il2CppType Vector2U5BU5D_t225_0_0_0;
extern Il2CppType Vector2U5BU5D_t225_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7970_ICollection_1_CopyTo_m44588_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Vector2U5BU5D_t225_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44588_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44588_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7970_ICollection_1_CopyTo_m44588_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44588_GenericMethod/* genericMethod */

};
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo ICollection_1_t7970_ICollection_1_Remove_m44589_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44589_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Remove(T)
MethodInfo ICollection_1_Remove_m44589_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector2_t99/* invoker_method */
	, ICollection_1_t7970_ICollection_1_Remove_m44589_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44589_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7970_MethodInfos[] =
{
	&ICollection_1_get_Count_m44583_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44584_MethodInfo,
	&ICollection_1_Add_m44585_MethodInfo,
	&ICollection_1_Clear_m44586_MethodInfo,
	&ICollection_1_Contains_m44587_MethodInfo,
	&ICollection_1_CopyTo_m44588_MethodInfo,
	&ICollection_1_Remove_m44589_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7972_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7970_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7972_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7970_0_0_0;
extern Il2CppType ICollection_1_t7970_1_0_0;
struct ICollection_1_t7970;
extern Il2CppGenericClass ICollection_1_t7970_GenericClass;
TypeInfo ICollection_1_t7970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7970_MethodInfos/* methods */
	, ICollection_1_t7970_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7970_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7970_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7970_0_0_0/* byval_arg */
	, &ICollection_1_t7970_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7970_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
extern Il2CppType IEnumerator_1_t6229_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44590_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44590_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7972_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6229_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44590_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7972_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44590_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7972_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7972_0_0_0;
extern Il2CppType IEnumerable_1_t7972_1_0_0;
struct IEnumerable_1_t7972;
extern Il2CppGenericClass IEnumerable_1_t7972_GenericClass;
TypeInfo IEnumerable_1_t7972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7972_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7972_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7972_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7972_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7972_0_0_0/* byval_arg */
	, &IEnumerable_1_t7972_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7972_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7971_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Vector2>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Vector2>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Vector2>
extern MethodInfo IList_1_get_Item_m44591_MethodInfo;
extern MethodInfo IList_1_set_Item_m44592_MethodInfo;
static PropertyInfo IList_1_t7971____Item_PropertyInfo = 
{
	&IList_1_t7971_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44591_MethodInfo/* get */
	, &IList_1_set_Item_m44592_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7971_PropertyInfos[] =
{
	&IList_1_t7971____Item_PropertyInfo,
	NULL
};
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo IList_1_t7971_IList_1_IndexOf_m44593_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44593_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Vector2>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44593_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7971_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Vector2_t99/* invoker_method */
	, IList_1_t7971_IList_1_IndexOf_m44593_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44593_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo IList_1_t7971_IList_1_Insert_m44594_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44594_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44594_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Vector2_t99/* invoker_method */
	, IList_1_t7971_IList_1_Insert_m44594_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44594_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7971_IList_1_RemoveAt_m44595_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44595_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44595_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7971_IList_1_RemoveAt_m44595_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44595_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7971_IList_1_get_Item_m44591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Vector2_t99_0_0_0;
extern void* RuntimeInvoker_Vector2_t99_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44591_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Vector2>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44591_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7971_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t99_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t99_Int32_t123/* invoker_method */
	, IList_1_t7971_IList_1_get_Item_m44591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44591_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo IList_1_t7971_IList_1_set_Item_m44592_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Vector2_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44592_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44592_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Vector2_t99/* invoker_method */
	, IList_1_t7971_IList_1_set_Item_m44592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44592_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7971_MethodInfos[] =
{
	&IList_1_IndexOf_m44593_MethodInfo,
	&IList_1_Insert_m44594_MethodInfo,
	&IList_1_RemoveAt_m44595_MethodInfo,
	&IList_1_get_Item_m44591_MethodInfo,
	&IList_1_set_Item_m44592_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7971_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7970_il2cpp_TypeInfo,
	&IEnumerable_1_t7972_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7971_0_0_0;
extern Il2CppType IList_1_t7971_1_0_0;
struct IList_1_t7971;
extern Il2CppGenericClass IList_1_t7971_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7971_MethodInfos/* methods */
	, IList_1_t7971_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7971_il2cpp_TypeInfo/* element_class */
	, IList_1_t7971_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7971_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7971_0_0_0/* byval_arg */
	, &IList_1_t7971_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7971_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6231_il2cpp_TypeInfo;

// ZoomCamera
#include "AssemblyU2DUnityScript_ZoomCamera.h"


// T System.Collections.Generic.IEnumerator`1<ZoomCamera>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ZoomCamera>
extern MethodInfo IEnumerator_1_get_Current_m44596_MethodInfo;
static PropertyInfo IEnumerator_1_t6231____Current_PropertyInfo = 
{
	&IEnumerator_1_t6231_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44596_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6231_PropertyInfos[] =
{
	&IEnumerator_1_t6231____Current_PropertyInfo,
	NULL
};
extern Il2CppType ZoomCamera_t224_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44596_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ZoomCamera>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44596_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6231_il2cpp_TypeInfo/* declaring_type */
	, &ZoomCamera_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44596_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6231_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44596_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6231_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6231_0_0_0;
extern Il2CppType IEnumerator_1_t6231_1_0_0;
struct IEnumerator_1_t6231;
extern Il2CppGenericClass IEnumerator_1_t6231_GenericClass;
TypeInfo IEnumerator_1_t6231_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6231_MethodInfos/* methods */
	, IEnumerator_1_t6231_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6231_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6231_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6231_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6231_0_0_0/* byval_arg */
	, &IEnumerator_1_t6231_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6231_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ZoomCamera>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_159.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3167_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ZoomCamera>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_159MethodDeclarations.h"

extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16052_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisZoomCamera_t224_m34108_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ZoomCamera>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ZoomCamera>(System.Int32)
#define Array_InternalArray__get_Item_TisZoomCamera_t224_m34108(__this, p0, method) (ZoomCamera_t224 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<ZoomCamera>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<ZoomCamera>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<ZoomCamera>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<ZoomCamera>::MoveNext()
// T System.Array/InternalEnumerator`1<ZoomCamera>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<ZoomCamera>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3167____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3167, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3167____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3167, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3167_FieldInfos[] =
{
	&InternalEnumerator_1_t3167____array_0_FieldInfo,
	&InternalEnumerator_1_t3167____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3167____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3167_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3167____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3167_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3167_PropertyInfos[] =
{
	&InternalEnumerator_1_t3167____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3167____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3167_InternalEnumerator_1__ctor_m16048_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16048_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ZoomCamera>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16048_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3167_InternalEnumerator_1__ctor_m16048_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16048_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ZoomCamera>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16050_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ZoomCamera>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16050_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16050_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16051_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ZoomCamera>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16051_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16051_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCamera_t224_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16052_GenericMethod;
// T System.Array/InternalEnumerator`1<ZoomCamera>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16052_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* declaring_type */
	, &ZoomCamera_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16052_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3167_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16048_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_MethodInfo,
	&InternalEnumerator_1_Dispose_m16050_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16051_MethodInfo,
	&InternalEnumerator_1_get_Current_m16052_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16051_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16050_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3167_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16049_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16051_MethodInfo,
	&InternalEnumerator_1_Dispose_m16050_MethodInfo,
	&InternalEnumerator_1_get_Current_m16052_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3167_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6231_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3167_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6231_il2cpp_TypeInfo, 7},
};
extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3167_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16052_MethodInfo/* Method Usage */,
	&ZoomCamera_t224_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisZoomCamera_t224_m34108_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3167_0_0_0;
extern Il2CppType InternalEnumerator_1_t3167_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3167_GenericClass;
TypeInfo InternalEnumerator_1_t3167_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3167_MethodInfos/* methods */
	, InternalEnumerator_1_t3167_PropertyInfos/* properties */
	, InternalEnumerator_1_t3167_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3167_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3167_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3167_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3167_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3167_1_0_0/* this_arg */
	, InternalEnumerator_1_t3167_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3167_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3167_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3167)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7973_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<ZoomCamera>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ZoomCamera>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ZoomCamera>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ZoomCamera>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ZoomCamera>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ZoomCamera>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ZoomCamera>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ZoomCamera>
extern MethodInfo ICollection_1_get_Count_m44597_MethodInfo;
static PropertyInfo ICollection_1_t7973____Count_PropertyInfo = 
{
	&ICollection_1_t7973_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44597_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44598_MethodInfo;
static PropertyInfo ICollection_1_t7973____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7973_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44598_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7973_PropertyInfos[] =
{
	&ICollection_1_t7973____Count_PropertyInfo,
	&ICollection_1_t7973____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44597_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ZoomCamera>::get_Count()
MethodInfo ICollection_1_get_Count_m44597_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44597_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44598_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ZoomCamera>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44598_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44598_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCamera_t224_0_0_0;
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo ICollection_1_t7973_ICollection_1_Add_m44599_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44599_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ZoomCamera>::Add(T)
MethodInfo ICollection_1_Add_m44599_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7973_ICollection_1_Add_m44599_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44599_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44600_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ZoomCamera>::Clear()
MethodInfo ICollection_1_Clear_m44600_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44600_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo ICollection_1_t7973_ICollection_1_Contains_m44601_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44601_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ZoomCamera>::Contains(T)
MethodInfo ICollection_1_Contains_m44601_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7973_ICollection_1_Contains_m44601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44601_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCameraU5BU5D_t5784_0_0_0;
extern Il2CppType ZoomCameraU5BU5D_t5784_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7973_ICollection_1_CopyTo_m44602_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCameraU5BU5D_t5784_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44602_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ZoomCamera>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44602_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7973_ICollection_1_CopyTo_m44602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44602_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo ICollection_1_t7973_ICollection_1_Remove_m44603_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44603_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ZoomCamera>::Remove(T)
MethodInfo ICollection_1_Remove_m44603_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7973_ICollection_1_Remove_m44603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44603_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7973_MethodInfos[] =
{
	&ICollection_1_get_Count_m44597_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44598_MethodInfo,
	&ICollection_1_Add_m44599_MethodInfo,
	&ICollection_1_Clear_m44600_MethodInfo,
	&ICollection_1_Contains_m44601_MethodInfo,
	&ICollection_1_CopyTo_m44602_MethodInfo,
	&ICollection_1_Remove_m44603_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7975_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7973_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7975_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7973_0_0_0;
extern Il2CppType ICollection_1_t7973_1_0_0;
struct ICollection_1_t7973;
extern Il2CppGenericClass ICollection_1_t7973_GenericClass;
TypeInfo ICollection_1_t7973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7973_MethodInfos/* methods */
	, ICollection_1_t7973_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7973_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7973_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7973_0_0_0/* byval_arg */
	, &ICollection_1_t7973_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7973_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ZoomCamera>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ZoomCamera>
extern Il2CppType IEnumerator_1_t6231_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44604_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ZoomCamera>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44604_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7975_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6231_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44604_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7975_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44604_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7975_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7975_0_0_0;
extern Il2CppType IEnumerable_1_t7975_1_0_0;
struct IEnumerable_1_t7975;
extern Il2CppGenericClass IEnumerable_1_t7975_GenericClass;
TypeInfo IEnumerable_1_t7975_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7975_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7975_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7975_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7975_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7975_0_0_0/* byval_arg */
	, &IEnumerable_1_t7975_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7975_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7974_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<ZoomCamera>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ZoomCamera>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ZoomCamera>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ZoomCamera>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ZoomCamera>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ZoomCamera>
extern MethodInfo IList_1_get_Item_m44605_MethodInfo;
extern MethodInfo IList_1_set_Item_m44606_MethodInfo;
static PropertyInfo IList_1_t7974____Item_PropertyInfo = 
{
	&IList_1_t7974_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44605_MethodInfo/* get */
	, &IList_1_set_Item_m44606_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7974_PropertyInfos[] =
{
	&IList_1_t7974____Item_PropertyInfo,
	NULL
};
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo IList_1_t7974_IList_1_IndexOf_m44607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44607_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ZoomCamera>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44607_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7974_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7974_IList_1_IndexOf_m44607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44607_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo IList_1_t7974_IList_1_Insert_m44608_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44608_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ZoomCamera>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44608_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7974_IList_1_Insert_m44608_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44608_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7974_IList_1_RemoveAt_m44609_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44609_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ZoomCamera>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44609_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7974_IList_1_RemoveAt_m44609_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44609_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7974_IList_1_get_Item_m44605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ZoomCamera_t224_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44605_GenericMethod;
// T System.Collections.Generic.IList`1<ZoomCamera>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44605_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7974_il2cpp_TypeInfo/* declaring_type */
	, &ZoomCamera_t224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7974_IList_1_get_Item_m44605_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44605_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo IList_1_t7974_IList_1_set_Item_m44606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44606_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ZoomCamera>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44606_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7974_IList_1_set_Item_m44606_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44606_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7974_MethodInfos[] =
{
	&IList_1_IndexOf_m44607_MethodInfo,
	&IList_1_Insert_m44608_MethodInfo,
	&IList_1_RemoveAt_m44609_MethodInfo,
	&IList_1_get_Item_m44605_MethodInfo,
	&IList_1_set_Item_m44606_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7974_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7973_il2cpp_TypeInfo,
	&IEnumerable_1_t7975_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7974_0_0_0;
extern Il2CppType IList_1_t7974_1_0_0;
struct IList_1_t7974;
extern Il2CppGenericClass IList_1_t7974_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7974_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7974_MethodInfos/* methods */
	, IList_1_t7974_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7974_il2cpp_TypeInfo/* element_class */
	, IList_1_t7974_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7974_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7974_0_0_0/* byval_arg */
	, &IList_1_t7974_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7974_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_69.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3168_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_69MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_65.h"
extern TypeInfo InvokableCall_1_t3169_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_65MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m16055_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m16057_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3168____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3168_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3168, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3168_FieldInfos[] =
{
	&CachedInvokableCall_1_t3168____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3168_CachedInvokableCall_1__ctor_m16053_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m16053_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m16053_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3168_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3168_CachedInvokableCall_1__ctor_m16053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m16053_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3168_CachedInvokableCall_1_Invoke_m16054_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m16054_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m16054_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3168_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3168_CachedInvokableCall_1_Invoke_m16054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m16054_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3168_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m16053_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16054_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m16054_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m16058_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3168_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16054_MethodInfo,
	&InvokableCall_1_Find_m16058_MethodInfo,
};
extern Il2CppType UnityAction_1_t3170_0_0_0;
extern TypeInfo UnityAction_1_t3170_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisZoomCamera_t224_m34118_MethodInfo;
extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m16060_MethodInfo;
extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3168_RGCTXData[8] = 
{
	&UnityAction_1_t3170_0_0_0/* Type Usage */,
	&UnityAction_1_t3170_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisZoomCamera_t224_m34118_MethodInfo/* Method Usage */,
	&ZoomCamera_t224_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16060_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m16055_MethodInfo/* Method Usage */,
	&ZoomCamera_t224_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m16057_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3168_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3168_1_0_0;
struct CachedInvokableCall_1_t3168;
extern Il2CppGenericClass CachedInvokableCall_1_t3168_GenericClass;
TypeInfo CachedInvokableCall_1_t3168_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3168_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3168_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3168_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3168_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3168_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3168_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3168_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3168_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3168_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3168)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_72.h"
extern TypeInfo UnityAction_1_t3170_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_72MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ZoomCamera>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ZoomCamera>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisZoomCamera_t224_m34118(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<ZoomCamera>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<ZoomCamera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<ZoomCamera>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<ZoomCamera>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<ZoomCamera>
extern Il2CppType UnityAction_1_t3170_0_0_1;
FieldInfo InvokableCall_1_t3169____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3170_0_0_1/* type */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3169, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3169_FieldInfos[] =
{
	&InvokableCall_1_t3169____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3169_InvokableCall_1__ctor_m16055_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16055_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ZoomCamera>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m16055_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3169_InvokableCall_1__ctor_m16055_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16055_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3170_0_0_0;
static ParameterInfo InvokableCall_1_t3169_InvokableCall_1__ctor_m16056_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3170_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16056_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ZoomCamera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m16056_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3169_InvokableCall_1__ctor_m16056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16056_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3169_InvokableCall_1_Invoke_m16057_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m16057_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ZoomCamera>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m16057_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3169_InvokableCall_1_Invoke_m16057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m16057_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3169_InvokableCall_1_Find_m16058_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m16058_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<ZoomCamera>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m16058_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3169_InvokableCall_1_Find_m16058_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m16058_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3169_MethodInfos[] =
{
	&InvokableCall_1__ctor_m16055_MethodInfo,
	&InvokableCall_1__ctor_m16056_MethodInfo,
	&InvokableCall_1_Invoke_m16057_MethodInfo,
	&InvokableCall_1_Find_m16058_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3169_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m16057_MethodInfo,
	&InvokableCall_1_Find_m16058_MethodInfo,
};
extern TypeInfo UnityAction_1_t3170_il2cpp_TypeInfo;
extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3169_RGCTXData[5] = 
{
	&UnityAction_1_t3170_0_0_0/* Type Usage */,
	&UnityAction_1_t3170_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisZoomCamera_t224_m34118_MethodInfo/* Method Usage */,
	&ZoomCamera_t224_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16060_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3169_0_0_0;
extern Il2CppType InvokableCall_1_t3169_1_0_0;
struct InvokableCall_1_t3169;
extern Il2CppGenericClass InvokableCall_1_t3169_GenericClass;
TypeInfo InvokableCall_1_t3169_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3169_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3169_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3169_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3169_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3169_0_0_0/* byval_arg */
	, &InvokableCall_1_t3169_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3169_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3169_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3169)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<ZoomCamera>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<ZoomCamera>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ZoomCamera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<ZoomCamera>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<ZoomCamera>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3170_UnityAction_1__ctor_m16059_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m16059_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ZoomCamera>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m16059_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3170_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3170_UnityAction_1__ctor_m16059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m16059_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCamera_t224_0_0_0;
static ParameterInfo UnityAction_1_t3170_UnityAction_1_Invoke_m16060_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m16060_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ZoomCamera>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m16060_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3170_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3170_UnityAction_1_Invoke_m16060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m16060_GenericMethod/* genericMethod */

};
extern Il2CppType ZoomCamera_t224_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3170_UnityAction_1_BeginInvoke_m16061_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ZoomCamera_t224_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m16061_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ZoomCamera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m16061_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3170_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3170_UnityAction_1_BeginInvoke_m16061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m16061_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3170_UnityAction_1_EndInvoke_m16062_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m16062_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ZoomCamera>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m16062_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3170_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3170_UnityAction_1_EndInvoke_m16062_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m16062_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3170_MethodInfos[] =
{
	&UnityAction_1__ctor_m16059_MethodInfo,
	&UnityAction_1_Invoke_m16060_MethodInfo,
	&UnityAction_1_BeginInvoke_m16061_MethodInfo,
	&UnityAction_1_EndInvoke_m16062_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m16061_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m16062_MethodInfo;
static MethodInfo* UnityAction_1_t3170_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m16060_MethodInfo,
	&UnityAction_1_BeginInvoke_m16061_MethodInfo,
	&UnityAction_1_EndInvoke_m16062_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3170_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3170_1_0_0;
struct UnityAction_1_t3170;
extern Il2CppGenericClass UnityAction_1_t3170_GenericClass;
TypeInfo UnityAction_1_t3170_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3170_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3170_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3170_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3170_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3170_0_0_0/* byval_arg */
	, &UnityAction_1_t3170_1_0_0/* this_arg */
	, UnityAction_1_t3170_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3170_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3170)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6233_il2cpp_TypeInfo;

// UnityEngine.EventSystems.EventHandle
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventHandle>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventHandle>
extern MethodInfo IEnumerator_1_get_Current_m44610_MethodInfo;
static PropertyInfo IEnumerator_1_t6233____Current_PropertyInfo = 
{
	&IEnumerator_1_t6233_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44610_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6233_PropertyInfos[] =
{
	&IEnumerator_1_t6233____Current_PropertyInfo,
	NULL
};
extern Il2CppType EventHandle_t232_0_0_0;
extern void* RuntimeInvoker_EventHandle_t232 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44610_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventHandle>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44610_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6233_il2cpp_TypeInfo/* declaring_type */
	, &EventHandle_t232_0_0_0/* return_type */
	, RuntimeInvoker_EventHandle_t232/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44610_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6233_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44610_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6233_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6233_0_0_0;
extern Il2CppType IEnumerator_1_t6233_1_0_0;
struct IEnumerator_1_t6233;
extern Il2CppGenericClass IEnumerator_1_t6233_GenericClass;
TypeInfo IEnumerator_1_t6233_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6233_MethodInfos/* methods */
	, IEnumerator_1_t6233_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6233_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6233_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6233_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6233_0_0_0/* byval_arg */
	, &IEnumerator_1_t6233_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6233_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_160.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3171_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_160MethodDeclarations.h"

extern TypeInfo EventHandle_t232_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16067_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEventHandle_t232_m34120_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.EventHandle>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.EventHandle>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEventHandle_t232_m34120 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m16063_MethodInfo;
 void InternalEnumerator_1__ctor_m16063 (InternalEnumerator_1_t3171 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064 (InternalEnumerator_1_t3171 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m16067(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m16067_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EventHandle_t232_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m16065_MethodInfo;
 void InternalEnumerator_1_Dispose_m16065 (InternalEnumerator_1_t3171 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m16066_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m16066 (InternalEnumerator_1_t3171 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m16067 (InternalEnumerator_1_t3171 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEventHandle_t232_m34120(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEventHandle_t232_m34120_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3171____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3171, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3171____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3171, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3171_FieldInfos[] =
{
	&InternalEnumerator_1_t3171____array_0_FieldInfo,
	&InternalEnumerator_1_t3171____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3171____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3171_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3171____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3171_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16067_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3171_PropertyInfos[] =
{
	&InternalEnumerator_1_t3171____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3171____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3171_InternalEnumerator_1__ctor_m16063_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16063_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16063_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m16063/* method */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3171_InternalEnumerator_1__ctor_m16063_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16063_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064/* method */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16065_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16065_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m16065/* method */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16065_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16066_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16066_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m16066/* method */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16066_GenericMethod/* genericMethod */

};
extern Il2CppType EventHandle_t232_0_0_0;
extern void* RuntimeInvoker_EventHandle_t232 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16067_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventHandle>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16067_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m16067/* method */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* declaring_type */
	, &EventHandle_t232_0_0_0/* return_type */
	, RuntimeInvoker_EventHandle_t232/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16067_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3171_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16063_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_MethodInfo,
	&InternalEnumerator_1_Dispose_m16065_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16066_MethodInfo,
	&InternalEnumerator_1_get_Current_m16067_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3171_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16064_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16066_MethodInfo,
	&InternalEnumerator_1_Dispose_m16065_MethodInfo,
	&InternalEnumerator_1_get_Current_m16067_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3171_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6233_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3171_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6233_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3171_0_0_0;
extern Il2CppType InternalEnumerator_1_t3171_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3171_GenericClass;
TypeInfo InternalEnumerator_1_t3171_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3171_MethodInfos/* methods */
	, InternalEnumerator_1_t3171_PropertyInfos/* properties */
	, InternalEnumerator_1_t3171_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3171_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3171_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3171_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3171_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3171_1_0_0/* this_arg */
	, InternalEnumerator_1_t3171_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3171_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3171)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7976_il2cpp_TypeInfo;

#include "UnityEngine.UI_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>
extern MethodInfo ICollection_1_get_Count_m44611_MethodInfo;
static PropertyInfo ICollection_1_t7976____Count_PropertyInfo = 
{
	&ICollection_1_t7976_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44611_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44612_MethodInfo;
static PropertyInfo ICollection_1_t7976____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7976_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44612_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7976_PropertyInfos[] =
{
	&ICollection_1_t7976____Count_PropertyInfo,
	&ICollection_1_t7976____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44611_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::get_Count()
MethodInfo ICollection_1_get_Count_m44611_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44611_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44612_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44612_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44612_GenericMethod/* genericMethod */

};
extern Il2CppType EventHandle_t232_0_0_0;
extern Il2CppType EventHandle_t232_0_0_0;
static ParameterInfo ICollection_1_t7976_ICollection_1_Add_m44613_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventHandle_t232_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44613_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Add(T)
MethodInfo ICollection_1_Add_m44613_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t7976_ICollection_1_Add_m44613_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44613_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44614_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Clear()
MethodInfo ICollection_1_Clear_m44614_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44614_GenericMethod/* genericMethod */

};
extern Il2CppType EventHandle_t232_0_0_0;
static ParameterInfo ICollection_1_t7976_ICollection_1_Contains_m44615_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventHandle_t232_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44615_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Contains(T)
MethodInfo ICollection_1_Contains_m44615_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7976_ICollection_1_Contains_m44615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44615_GenericMethod/* genericMethod */

};
extern Il2CppType EventHandleU5BU5D_t5785_0_0_0;
extern Il2CppType EventHandleU5BU5D_t5785_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7976_ICollection_1_CopyTo_m44616_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EventHandleU5BU5D_t5785_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44616_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44616_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7976_ICollection_1_CopyTo_m44616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44616_GenericMethod/* genericMethod */

};
extern Il2CppType EventHandle_t232_0_0_0;
static ParameterInfo ICollection_1_t7976_ICollection_1_Remove_m44617_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventHandle_t232_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44617_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventHandle>::Remove(T)
MethodInfo ICollection_1_Remove_m44617_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7976_ICollection_1_Remove_m44617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44617_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7976_MethodInfos[] =
{
	&ICollection_1_get_Count_m44611_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44612_MethodInfo,
	&ICollection_1_Add_m44613_MethodInfo,
	&ICollection_1_Clear_m44614_MethodInfo,
	&ICollection_1_Contains_m44615_MethodInfo,
	&ICollection_1_CopyTo_m44616_MethodInfo,
	&ICollection_1_Remove_m44617_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7978_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7976_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7978_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7976_0_0_0;
extern Il2CppType ICollection_1_t7976_1_0_0;
struct ICollection_1_t7976;
extern Il2CppGenericClass ICollection_1_t7976_GenericClass;
TypeInfo ICollection_1_t7976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7976_MethodInfos/* methods */
	, ICollection_1_t7976_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7976_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7976_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7976_0_0_0/* byval_arg */
	, &ICollection_1_t7976_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7976_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventHandle>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventHandle>
extern Il2CppType IEnumerator_1_t6233_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44618_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventHandle>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44618_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7978_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6233_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44618_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7978_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44618_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7978_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7978_0_0_0;
extern Il2CppType IEnumerable_1_t7978_1_0_0;
struct IEnumerable_1_t7978;
extern Il2CppGenericClass IEnumerable_1_t7978_GenericClass;
TypeInfo IEnumerable_1_t7978_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7978_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7978_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7978_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7978_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7978_0_0_0/* byval_arg */
	, &IEnumerable_1_t7978_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7978_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7977_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>
extern MethodInfo IList_1_get_Item_m44619_MethodInfo;
extern MethodInfo IList_1_set_Item_m44620_MethodInfo;
static PropertyInfo IList_1_t7977____Item_PropertyInfo = 
{
	&IList_1_t7977_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44619_MethodInfo/* get */
	, &IList_1_set_Item_m44620_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7977_PropertyInfos[] =
{
	&IList_1_t7977____Item_PropertyInfo,
	NULL
};
extern Il2CppType EventHandle_t232_0_0_0;
static ParameterInfo IList_1_t7977_IList_1_IndexOf_m44621_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventHandle_t232_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44621_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44621_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7977_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7977_IList_1_IndexOf_m44621_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44621_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventHandle_t232_0_0_0;
static ParameterInfo IList_1_t7977_IList_1_Insert_m44622_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EventHandle_t232_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44622_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44622_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7977_IList_1_Insert_m44622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44622_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7977_IList_1_RemoveAt_m44623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44623_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44623_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7977_IList_1_RemoveAt_m44623_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44623_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7977_IList_1_get_Item_m44619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EventHandle_t232_0_0_0;
extern void* RuntimeInvoker_EventHandle_t232_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44619_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44619_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7977_il2cpp_TypeInfo/* declaring_type */
	, &EventHandle_t232_0_0_0/* return_type */
	, RuntimeInvoker_EventHandle_t232_Int32_t123/* invoker_method */
	, IList_1_t7977_IList_1_get_Item_m44619_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44619_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventHandle_t232_0_0_0;
static ParameterInfo IList_1_t7977_IList_1_set_Item_m44620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EventHandle_t232_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44620_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventHandle>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44620_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7977_IList_1_set_Item_m44620_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44620_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7977_MethodInfos[] =
{
	&IList_1_IndexOf_m44621_MethodInfo,
	&IList_1_Insert_m44622_MethodInfo,
	&IList_1_RemoveAt_m44623_MethodInfo,
	&IList_1_get_Item_m44619_MethodInfo,
	&IList_1_set_Item_m44620_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7977_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7976_il2cpp_TypeInfo,
	&IEnumerable_1_t7978_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7977_0_0_0;
extern Il2CppType IList_1_t7977_1_0_0;
struct IList_1_t7977;
extern Il2CppGenericClass IList_1_t7977_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7977_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7977_MethodInfos/* methods */
	, IList_1_t7977_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7977_il2cpp_TypeInfo/* element_class */
	, IList_1_t7977_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7977_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7977_0_0_0/* byval_arg */
	, &IList_1_t7977_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7977_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6235_il2cpp_TypeInfo;

// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventSystem>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventSystem>
extern MethodInfo IEnumerator_1_get_Current_m44624_MethodInfo;
static PropertyInfo IEnumerator_1_t6235____Current_PropertyInfo = 
{
	&IEnumerator_1_t6235_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44624_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6235_PropertyInfos[] =
{
	&IEnumerator_1_t6235____Current_PropertyInfo,
	NULL
};
extern Il2CppType EventSystem_t237_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44624_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventSystem>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44624_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6235_il2cpp_TypeInfo/* declaring_type */
	, &EventSystem_t237_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44624_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6235_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44624_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6235_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6235_0_0_0;
extern Il2CppType IEnumerator_1_t6235_1_0_0;
struct IEnumerator_1_t6235;
extern Il2CppGenericClass IEnumerator_1_t6235_GenericClass;
TypeInfo IEnumerator_1_t6235_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6235_MethodInfos/* methods */
	, IEnumerator_1_t6235_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6235_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6235_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6235_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6235_0_0_0/* byval_arg */
	, &IEnumerator_1_t6235_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6235_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_161.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3172_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_161MethodDeclarations.h"

extern TypeInfo EventSystem_t237_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16072_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEventSystem_t237_m34131_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.EventSystem>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.EventSystem>(System.Int32)
#define Array_InternalArray__get_Item_TisEventSystem_t237_m34131(__this, p0, method) (EventSystem_t237 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3172____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3172, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3172____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3172, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3172_FieldInfos[] =
{
	&InternalEnumerator_1_t3172____array_0_FieldInfo,
	&InternalEnumerator_1_t3172____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3172____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3172_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3172____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3172_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16072_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3172_PropertyInfos[] =
{
	&InternalEnumerator_1_t3172____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3172____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3172_InternalEnumerator_1__ctor_m16068_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16068_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16068_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3172_InternalEnumerator_1__ctor_m16068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16068_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16070_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16070_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16070_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16071_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16071_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16071_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystem_t237_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16072_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventSystem>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16072_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* declaring_type */
	, &EventSystem_t237_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16072_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3172_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16068_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_MethodInfo,
	&InternalEnumerator_1_Dispose_m16070_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16071_MethodInfo,
	&InternalEnumerator_1_get_Current_m16072_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16071_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16070_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3172_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16069_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16071_MethodInfo,
	&InternalEnumerator_1_Dispose_m16070_MethodInfo,
	&InternalEnumerator_1_get_Current_m16072_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3172_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6235_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3172_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6235_il2cpp_TypeInfo, 7},
};
extern TypeInfo EventSystem_t237_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3172_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16072_MethodInfo/* Method Usage */,
	&EventSystem_t237_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisEventSystem_t237_m34131_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3172_0_0_0;
extern Il2CppType InternalEnumerator_1_t3172_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3172_GenericClass;
TypeInfo InternalEnumerator_1_t3172_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3172_MethodInfos/* methods */
	, InternalEnumerator_1_t3172_PropertyInfos/* properties */
	, InternalEnumerator_1_t3172_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3172_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3172_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3172_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3172_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3172_1_0_0/* this_arg */
	, InternalEnumerator_1_t3172_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3172_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3172_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3172)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7979_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>
extern MethodInfo ICollection_1_get_Count_m44625_MethodInfo;
static PropertyInfo ICollection_1_t7979____Count_PropertyInfo = 
{
	&ICollection_1_t7979_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44625_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44626_MethodInfo;
static PropertyInfo ICollection_1_t7979____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7979_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44626_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7979_PropertyInfos[] =
{
	&ICollection_1_t7979____Count_PropertyInfo,
	&ICollection_1_t7979____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44625_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::get_Count()
MethodInfo ICollection_1_get_Count_m44625_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44625_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44626_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44626_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44626_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystem_t237_0_0_0;
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo ICollection_1_t7979_ICollection_1_Add_m44627_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44627_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Add(T)
MethodInfo ICollection_1_Add_m44627_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7979_ICollection_1_Add_m44627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44627_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44628_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Clear()
MethodInfo ICollection_1_Clear_m44628_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44628_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo ICollection_1_t7979_ICollection_1_Contains_m44629_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44629_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Contains(T)
MethodInfo ICollection_1_Contains_m44629_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7979_ICollection_1_Contains_m44629_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44629_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystemU5BU5D_t5786_0_0_0;
extern Il2CppType EventSystemU5BU5D_t5786_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7979_ICollection_1_CopyTo_m44630_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EventSystemU5BU5D_t5786_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44630_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44630_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7979_ICollection_1_CopyTo_m44630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44630_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo ICollection_1_t7979_ICollection_1_Remove_m44631_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44631_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventSystem>::Remove(T)
MethodInfo ICollection_1_Remove_m44631_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7979_ICollection_1_Remove_m44631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44631_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7979_MethodInfos[] =
{
	&ICollection_1_get_Count_m44625_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44626_MethodInfo,
	&ICollection_1_Add_m44627_MethodInfo,
	&ICollection_1_Clear_m44628_MethodInfo,
	&ICollection_1_Contains_m44629_MethodInfo,
	&ICollection_1_CopyTo_m44630_MethodInfo,
	&ICollection_1_Remove_m44631_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7981_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7979_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7981_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7979_0_0_0;
extern Il2CppType ICollection_1_t7979_1_0_0;
struct ICollection_1_t7979;
extern Il2CppGenericClass ICollection_1_t7979_GenericClass;
TypeInfo ICollection_1_t7979_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7979_MethodInfos/* methods */
	, ICollection_1_t7979_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7979_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7979_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7979_0_0_0/* byval_arg */
	, &ICollection_1_t7979_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7979_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventSystem>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType IEnumerator_1_t6235_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44632_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventSystem>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44632_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7981_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6235_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44632_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7981_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44632_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7981_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7981_0_0_0;
extern Il2CppType IEnumerable_1_t7981_1_0_0;
struct IEnumerable_1_t7981;
extern Il2CppGenericClass IEnumerable_1_t7981_GenericClass;
TypeInfo IEnumerable_1_t7981_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7981_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7981_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7981_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7981_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7981_0_0_0/* byval_arg */
	, &IEnumerable_1_t7981_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7981_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7980_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>
extern MethodInfo IList_1_get_Item_m44633_MethodInfo;
extern MethodInfo IList_1_set_Item_m44634_MethodInfo;
static PropertyInfo IList_1_t7980____Item_PropertyInfo = 
{
	&IList_1_t7980_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44633_MethodInfo/* get */
	, &IList_1_set_Item_m44634_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7980_PropertyInfos[] =
{
	&IList_1_t7980____Item_PropertyInfo,
	NULL
};
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo IList_1_t7980_IList_1_IndexOf_m44635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44635_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44635_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7980_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7980_IList_1_IndexOf_m44635_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44635_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo IList_1_t7980_IList_1_Insert_m44636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44636_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44636_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7980_IList_1_Insert_m44636_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44636_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7980_IList_1_RemoveAt_m44637_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44637_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44637_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7980_IList_1_RemoveAt_m44637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44637_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7980_IList_1_get_Item_m44633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EventSystem_t237_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44633_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44633_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7980_il2cpp_TypeInfo/* declaring_type */
	, &EventSystem_t237_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7980_IList_1_get_Item_m44633_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44633_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo IList_1_t7980_IList_1_set_Item_m44634_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44634_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventSystem>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44634_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7980_IList_1_set_Item_m44634_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44634_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7980_MethodInfos[] =
{
	&IList_1_IndexOf_m44635_MethodInfo,
	&IList_1_Insert_m44636_MethodInfo,
	&IList_1_RemoveAt_m44637_MethodInfo,
	&IList_1_get_Item_m44633_MethodInfo,
	&IList_1_set_Item_m44634_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7980_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7979_il2cpp_TypeInfo,
	&IEnumerable_1_t7981_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7980_0_0_0;
extern Il2CppType IList_1_t7980_1_0_0;
struct IList_1_t7980;
extern Il2CppGenericClass IList_1_t7980_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7980_MethodInfos/* methods */
	, IList_1_t7980_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7980_il2cpp_TypeInfo/* element_class */
	, IList_1_t7980_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7980_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7980_0_0_0/* byval_arg */
	, &IList_1_t7980_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7980_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7982_il2cpp_TypeInfo;

// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>
extern MethodInfo ICollection_1_get_Count_m44638_MethodInfo;
static PropertyInfo ICollection_1_t7982____Count_PropertyInfo = 
{
	&ICollection_1_t7982_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44639_MethodInfo;
static PropertyInfo ICollection_1_t7982____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7982_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44639_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7982_PropertyInfos[] =
{
	&ICollection_1_t7982____Count_PropertyInfo,
	&ICollection_1_t7982____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44638_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m44638_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44638_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44639_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44639_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44639_GenericMethod/* genericMethod */

};
extern Il2CppType UIBehaviour_t238_0_0_0;
extern Il2CppType UIBehaviour_t238_0_0_0;
static ParameterInfo ICollection_1_t7982_ICollection_1_Add_m44640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UIBehaviour_t238_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44640_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m44640_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7982_ICollection_1_Add_m44640_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44640_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44641_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m44641_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44641_GenericMethod/* genericMethod */

};
extern Il2CppType UIBehaviour_t238_0_0_0;
static ParameterInfo ICollection_1_t7982_ICollection_1_Contains_m44642_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UIBehaviour_t238_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44642_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m44642_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7982_ICollection_1_Contains_m44642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44642_GenericMethod/* genericMethod */

};
extern Il2CppType UIBehaviourU5BU5D_t5787_0_0_0;
extern Il2CppType UIBehaviourU5BU5D_t5787_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7982_ICollection_1_CopyTo_m44643_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UIBehaviourU5BU5D_t5787_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44643_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44643_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7982_ICollection_1_CopyTo_m44643_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44643_GenericMethod/* genericMethod */

};
extern Il2CppType UIBehaviour_t238_0_0_0;
static ParameterInfo ICollection_1_t7982_ICollection_1_Remove_m44644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UIBehaviour_t238_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44644_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.UIBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m44644_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7982_ICollection_1_Remove_m44644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44644_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7982_MethodInfos[] =
{
	&ICollection_1_get_Count_m44638_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44639_MethodInfo,
	&ICollection_1_Add_m44640_MethodInfo,
	&ICollection_1_Clear_m44641_MethodInfo,
	&ICollection_1_Contains_m44642_MethodInfo,
	&ICollection_1_CopyTo_m44643_MethodInfo,
	&ICollection_1_Remove_m44644_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7984_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7982_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7984_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7982_0_0_0;
extern Il2CppType ICollection_1_t7982_1_0_0;
struct ICollection_1_t7982;
extern Il2CppGenericClass ICollection_1_t7982_GenericClass;
TypeInfo ICollection_1_t7982_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7982_MethodInfos/* methods */
	, ICollection_1_t7982_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7982_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7982_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7982_0_0_0/* byval_arg */
	, &ICollection_1_t7982_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7982_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.UIBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType IEnumerator_1_t6237_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44645_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.UIBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44645_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7984_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6237_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44645_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7984_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44645_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7984_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7984_0_0_0;
extern Il2CppType IEnumerable_1_t7984_1_0_0;
struct IEnumerable_1_t7984;
extern Il2CppGenericClass IEnumerable_1_t7984_GenericClass;
TypeInfo IEnumerable_1_t7984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7984_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7984_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7984_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7984_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7984_0_0_0/* byval_arg */
	, &IEnumerable_1_t7984_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7984_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6237_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.UIBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m44646_MethodInfo;
static PropertyInfo IEnumerator_1_t6237____Current_PropertyInfo = 
{
	&IEnumerator_1_t6237_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6237_PropertyInfos[] =
{
	&IEnumerator_1_t6237____Current_PropertyInfo,
	NULL
};
extern Il2CppType UIBehaviour_t238_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44646_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44646_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6237_il2cpp_TypeInfo/* declaring_type */
	, &UIBehaviour_t238_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44646_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6237_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44646_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6237_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6237_0_0_0;
extern Il2CppType IEnumerator_1_t6237_1_0_0;
struct IEnumerator_1_t6237;
extern Il2CppGenericClass IEnumerator_1_t6237_GenericClass;
TypeInfo IEnumerator_1_t6237_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6237_MethodInfos/* methods */
	, IEnumerator_1_t6237_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6237_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6237_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6237_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6237_0_0_0/* byval_arg */
	, &IEnumerator_1_t6237_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6237_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_162.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3173_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_162MethodDeclarations.h"

extern TypeInfo UIBehaviour_t238_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16077_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUIBehaviour_t238_m34142_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.UIBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.UIBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisUIBehaviour_t238_m34142(__this, p0, method) (UIBehaviour_t238 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3173____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3173, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3173____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3173, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3173_FieldInfos[] =
{
	&InternalEnumerator_1_t3173____array_0_FieldInfo,
	&InternalEnumerator_1_t3173____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3173____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3173_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3173____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3173_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3173_PropertyInfos[] =
{
	&InternalEnumerator_1_t3173____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3173____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3173_InternalEnumerator_1__ctor_m16073_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16073_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16073_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3173_InternalEnumerator_1__ctor_m16073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16073_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16075_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16075_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16075_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16076_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16076_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16076_GenericMethod/* genericMethod */

};
extern Il2CppType UIBehaviour_t238_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16077_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.UIBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16077_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* declaring_type */
	, &UIBehaviour_t238_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16077_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3173_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16073_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_MethodInfo,
	&InternalEnumerator_1_Dispose_m16075_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16076_MethodInfo,
	&InternalEnumerator_1_get_Current_m16077_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16076_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16075_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3173_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16074_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16076_MethodInfo,
	&InternalEnumerator_1_Dispose_m16075_MethodInfo,
	&InternalEnumerator_1_get_Current_m16077_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3173_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6237_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3173_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6237_il2cpp_TypeInfo, 7},
};
extern TypeInfo UIBehaviour_t238_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3173_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16077_MethodInfo/* Method Usage */,
	&UIBehaviour_t238_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUIBehaviour_t238_m34142_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3173_0_0_0;
extern Il2CppType InternalEnumerator_1_t3173_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3173_GenericClass;
TypeInfo InternalEnumerator_1_t3173_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3173_MethodInfos/* methods */
	, InternalEnumerator_1_t3173_PropertyInfos/* properties */
	, InternalEnumerator_1_t3173_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3173_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3173_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3173_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3173_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3173_1_0_0/* this_arg */
	, InternalEnumerator_1_t3173_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3173_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3173_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3173)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7983_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>
extern MethodInfo IList_1_get_Item_m44647_MethodInfo;
extern MethodInfo IList_1_set_Item_m44648_MethodInfo;
static PropertyInfo IList_1_t7983____Item_PropertyInfo = 
{
	&IList_1_t7983_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44647_MethodInfo/* get */
	, &IList_1_set_Item_m44648_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7983_PropertyInfos[] =
{
	&IList_1_t7983____Item_PropertyInfo,
	NULL
};
extern Il2CppType UIBehaviour_t238_0_0_0;
static ParameterInfo IList_1_t7983_IList_1_IndexOf_m44649_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UIBehaviour_t238_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44649_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44649_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7983_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7983_IList_1_IndexOf_m44649_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44649_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UIBehaviour_t238_0_0_0;
static ParameterInfo IList_1_t7983_IList_1_Insert_m44650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UIBehaviour_t238_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44650_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44650_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7983_IList_1_Insert_m44650_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44650_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7983_IList_1_RemoveAt_m44651_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44651_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44651_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7983_IList_1_RemoveAt_m44651_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44651_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7983_IList_1_get_Item_m44647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UIBehaviour_t238_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44647_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44647_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7983_il2cpp_TypeInfo/* declaring_type */
	, &UIBehaviour_t238_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7983_IList_1_get_Item_m44647_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44647_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UIBehaviour_t238_0_0_0;
static ParameterInfo IList_1_t7983_IList_1_set_Item_m44648_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UIBehaviour_t238_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44648_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.UIBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44648_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7983_IList_1_set_Item_m44648_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44648_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7983_MethodInfos[] =
{
	&IList_1_IndexOf_m44649_MethodInfo,
	&IList_1_Insert_m44650_MethodInfo,
	&IList_1_RemoveAt_m44651_MethodInfo,
	&IList_1_get_Item_m44647_MethodInfo,
	&IList_1_set_Item_m44648_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7983_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7982_il2cpp_TypeInfo,
	&IEnumerable_1_t7984_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7983_0_0_0;
extern Il2CppType IList_1_t7983_1_0_0;
struct IList_1_t7983;
extern Il2CppGenericClass IList_1_t7983_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7983_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7983_MethodInfos/* methods */
	, IList_1_t7983_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7983_il2cpp_TypeInfo/* element_class */
	, IList_1_t7983_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7983_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7983_0_0_0/* byval_arg */
	, &IList_1_t7983_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7983_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_70.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3174_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_70MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_66.h"
extern TypeInfo InvokableCall_1_t3175_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_66MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m16080_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m16082_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3174____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3174_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3174, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3174_FieldInfos[] =
{
	&CachedInvokableCall_1_t3174____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3174_CachedInvokableCall_1__ctor_m16078_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m16078_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m16078_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3174_CachedInvokableCall_1__ctor_m16078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m16078_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3174_CachedInvokableCall_1_Invoke_m16079_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m16079_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m16079_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3174_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3174_CachedInvokableCall_1_Invoke_m16079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m16079_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3174_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m16078_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16079_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m16079_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m16083_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3174_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m16079_MethodInfo,
	&InvokableCall_1_Find_m16083_MethodInfo,
};
extern Il2CppType UnityAction_1_t3176_0_0_0;
extern TypeInfo UnityAction_1_t3176_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisEventSystem_t237_m34152_MethodInfo;
extern TypeInfo EventSystem_t237_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m16085_MethodInfo;
extern TypeInfo EventSystem_t237_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3174_RGCTXData[8] = 
{
	&UnityAction_1_t3176_0_0_0/* Type Usage */,
	&UnityAction_1_t3176_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisEventSystem_t237_m34152_MethodInfo/* Method Usage */,
	&EventSystem_t237_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16085_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m16080_MethodInfo/* Method Usage */,
	&EventSystem_t237_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m16082_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3174_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3174_1_0_0;
struct CachedInvokableCall_1_t3174;
extern Il2CppGenericClass CachedInvokableCall_1_t3174_GenericClass;
TypeInfo CachedInvokableCall_1_t3174_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3174_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3174_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3174_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3174_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3174_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3174_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3174_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3174_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3174_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3174)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_73.h"
extern TypeInfo UnityAction_1_t3176_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_73MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.EventSystems.EventSystem>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.EventSystems.EventSystem>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisEventSystem_t237_m34152(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType UnityAction_1_t3176_0_0_1;
FieldInfo InvokableCall_1_t3175____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3176_0_0_1/* type */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3175, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3175_FieldInfos[] =
{
	&InvokableCall_1_t3175____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3175_InvokableCall_1__ctor_m16080_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16080_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m16080_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3175_InvokableCall_1__ctor_m16080_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16080_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3176_0_0_0;
static ParameterInfo InvokableCall_1_t3175_InvokableCall_1__ctor_m16081_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3176_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m16081_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m16081_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3175_InvokableCall_1__ctor_m16081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m16081_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3175_InvokableCall_1_Invoke_m16082_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m16082_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m16082_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3175_InvokableCall_1_Invoke_m16082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m16082_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3175_InvokableCall_1_Find_m16083_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m16083_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m16083_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3175_InvokableCall_1_Find_m16083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m16083_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3175_MethodInfos[] =
{
	&InvokableCall_1__ctor_m16080_MethodInfo,
	&InvokableCall_1__ctor_m16081_MethodInfo,
	&InvokableCall_1_Invoke_m16082_MethodInfo,
	&InvokableCall_1_Find_m16083_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3175_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m16082_MethodInfo,
	&InvokableCall_1_Find_m16083_MethodInfo,
};
extern TypeInfo UnityAction_1_t3176_il2cpp_TypeInfo;
extern TypeInfo EventSystem_t237_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3175_RGCTXData[5] = 
{
	&UnityAction_1_t3176_0_0_0/* Type Usage */,
	&UnityAction_1_t3176_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisEventSystem_t237_m34152_MethodInfo/* Method Usage */,
	&EventSystem_t237_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m16085_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3175_0_0_0;
extern Il2CppType InvokableCall_1_t3175_1_0_0;
struct InvokableCall_1_t3175;
extern Il2CppGenericClass InvokableCall_1_t3175_GenericClass;
TypeInfo InvokableCall_1_t3175_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3175_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3175_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3175_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3175_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3175_0_0_0/* byval_arg */
	, &InvokableCall_1_t3175_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3175_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3175_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3175)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3176_UnityAction_1__ctor_m16084_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m16084_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m16084_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3176_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3176_UnityAction_1__ctor_m16084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m16084_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystem_t237_0_0_0;
static ParameterInfo UnityAction_1_t3176_UnityAction_1_Invoke_m16085_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m16085_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m16085_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3176_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3176_UnityAction_1_Invoke_m16085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m16085_GenericMethod/* genericMethod */

};
extern Il2CppType EventSystem_t237_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3176_UnityAction_1_BeginInvoke_m16086_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &EventSystem_t237_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m16086_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m16086_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3176_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3176_UnityAction_1_BeginInvoke_m16086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m16086_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3176_UnityAction_1_EndInvoke_m16087_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m16087_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m16087_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3176_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3176_UnityAction_1_EndInvoke_m16087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m16087_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3176_MethodInfos[] =
{
	&UnityAction_1__ctor_m16084_MethodInfo,
	&UnityAction_1_Invoke_m16085_MethodInfo,
	&UnityAction_1_BeginInvoke_m16086_MethodInfo,
	&UnityAction_1_EndInvoke_m16087_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m16086_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m16087_MethodInfo;
static MethodInfo* UnityAction_1_t3176_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m16085_MethodInfo,
	&UnityAction_1_BeginInvoke_m16086_MethodInfo,
	&UnityAction_1_EndInvoke_m16087_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3176_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3176_1_0_0;
struct UnityAction_1_t3176;
extern Il2CppGenericClass UnityAction_1_t3176_GenericClass;
TypeInfo UnityAction_1_t3176_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3176_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3176_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3176_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3176_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3176_0_0_0/* byval_arg */
	, &UnityAction_1_t3176_1_0_0/* this_arg */
	, UnityAction_1_t3176_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3176_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3176)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo List_1_t233_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"

// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModule.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_3.h"
// System.Predicate`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Predicate_1_gen_7.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_2.h"
// System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Comparison_1_gen_6.h"
extern TypeInfo BaseInputModule_t234_il2cpp_TypeInfo;
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern TypeInfo NullReferenceException_t199_il2cpp_TypeInfo;
extern TypeInfo InvalidCastException_t2275_il2cpp_TypeInfo;
extern TypeInfo ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t3180_il2cpp_TypeInfo;
extern TypeInfo BaseInputModuleU5BU5D_t3177_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3184_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t3178_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t3179_il2cpp_TypeInfo;
extern TypeInfo ReadOnlyCollection_1_t3181_il2cpp_TypeInfo;
extern TypeInfo ArgumentNullException_t1224_il2cpp_TypeInfo;
extern TypeInfo Predicate_1_t3182_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t3190_il2cpp_TypeInfo;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_3MethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Predicate`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Predicate_1_gen_7MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27MethodDeclarations.h"
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_2MethodDeclarations.h"
extern MethodInfo List_1_get_Item_m2062_MethodInfo;
extern MethodInfo List_1_set_Item_m16133_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m8054_MethodInfo;
extern MethodInfo Array_Resize_TisBaseInputModule_t234_m34165_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7836_MethodInfo;
extern MethodInfo List_1_CheckIndex_m16121_MethodInfo;
extern MethodInfo Object__ctor_m312_MethodInfo;
extern MethodInfo List_1_CheckCollection_m16123_MethodInfo;
extern MethodInfo List_1_AddEnumerable_m16109_MethodInfo;
extern MethodInfo ICollection_1_get_Count_m44652_MethodInfo;
extern MethodInfo List_1_AddCollection_m16108_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m16118_MethodInfo;
extern MethodInfo Array_Copy_m9801_MethodInfo;
extern MethodInfo List_1_Add_m16106_MethodInfo;
extern MethodInfo List_1_Contains_m16113_MethodInfo;
extern MethodInfo List_1_IndexOf_m16119_MethodInfo;
extern MethodInfo List_1_Insert_m16122_MethodInfo;
extern MethodInfo List_1_Remove_m16124_MethodInfo;
extern MethodInfo List_1_GrowIfNeeded_m16107_MethodInfo;
extern MethodInfo List_1_get_Capacity_m16131_MethodInfo;
extern MethodInfo Math_Max_m8994_MethodInfo;
extern MethodInfo List_1_set_Capacity_m16132_MethodInfo;
extern MethodInfo ICollection_1_CopyTo_m44653_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m44654_MethodInfo;
extern MethodInfo IEnumerator_1_get_Current_m44655_MethodInfo;
extern MethodInfo IEnumerator_MoveNext_m4590_MethodInfo;
extern MethodInfo IDisposable_Dispose_m333_MethodInfo;
extern MethodInfo ReadOnlyCollection_1__ctor_m16145_MethodInfo;
extern MethodInfo Array_Clear_m8967_MethodInfo;
extern MethodInfo Array_IndexOf_TisBaseInputModule_t234_m34167_MethodInfo;
extern MethodInfo List_1_CheckMatch_m16116_MethodInfo;
extern MethodInfo List_1_GetIndex_m16117_MethodInfo;
extern MethodInfo ArgumentNullException__ctor_m6710_MethodInfo;
extern MethodInfo Predicate_1_Invoke_m16220_MethodInfo;
extern MethodInfo Enumerator__ctor_m16139_MethodInfo;
extern MethodInfo List_1_Shift_m16120_MethodInfo;
extern MethodInfo List_1_RemoveAt_m2063_MethodInfo;
extern MethodInfo Array_Reverse_m9026_MethodInfo;
extern MethodInfo Comparer_1_get_Default_m16226_MethodInfo;
extern MethodInfo Array_Sort_TisBaseInputModule_t234_m34169_MethodInfo;
extern MethodInfo Array_Sort_TisBaseInputModule_t234_m34177_MethodInfo;
extern MethodInfo Array_Copy_m9800_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
 void Array_Resize_TisObject_t_m32633_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t130** p0, int32_t p1, MethodInfo* method);
#define Array_Resize_TisObject_t_m32633(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32633_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130**)p0, (int32_t)p1, method)
// Declaration System.Void System.Array::Resize<UnityEngine.EventSystems.BaseInputModule>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<UnityEngine.EventSystems.BaseInputModule>(!!0[]&,System.Int32)
#define Array_Resize_TisBaseInputModule_t234_m34165(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32633_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130**)p0, (int32_t)p1, method)
struct Array_t;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_2.h"
struct Array_t;
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
// Declaration System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
 int32_t Array_IndexOf_TisObject_t_m13987_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t130* p0, Object_t * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define Array_IndexOf_TisObject_t_m13987(__this/* static, unused */, p0, p1, p2, p3, method) (int32_t)Array_IndexOf_TisObject_t_m13987_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130*)p0, (Object_t *)p1, (int32_t)p2, (int32_t)p3, method)
// Declaration System.Int32 System.Array::IndexOf<UnityEngine.EventSystems.BaseInputModule>(!!0[],!!0,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<UnityEngine.EventSystems.BaseInputModule>(!!0[],!!0,System.Int32,System.Int32)
#define Array_IndexOf_TisBaseInputModule_t234_m34167(__this/* static, unused */, p0, p1, p2, p3, method) (int32_t)Array_IndexOf_TisObject_t_m13987_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130*)p0, (Object_t *)p1, (int32_t)p2, (int32_t)p3, method)
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
 void Array_Sort_TisObject_t_m32702_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t130* p0, int32_t p1, int32_t p2, Object_t* p3, MethodInfo* method);
#define Array_Sort_TisObject_t_m32702(__this/* static, unused */, p0, p1, p2, p3, method) (void)Array_Sort_TisObject_t_m32702_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130*)p0, (int32_t)p1, (int32_t)p2, (Object_t*)p3, method)
// Declaration System.Void System.Array::Sort<UnityEngine.EventSystems.BaseInputModule>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
// System.Void System.Array::Sort<UnityEngine.EventSystems.BaseInputModule>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
#define Array_Sort_TisBaseInputModule_t234_m34169(__this/* static, unused */, p0, p1, p2, p3, method) (void)Array_Sort_TisObject_t_m32702_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130*)p0, (int32_t)p1, (int32_t)p2, (Object_t*)p3, method)
struct Array_t;
// System.Exception
#include "mscorlib_System_Exception.h"
struct Array_t;
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3.h"
// Declaration System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
 void Array_Sort_TisObject_t_m32853_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t130* p0, int32_t p1, Comparison_1_t2833 * p2, MethodInfo* method);
#define Array_Sort_TisObject_t_m32853(__this/* static, unused */, p0, p1, p2, method) (void)Array_Sort_TisObject_t_m32853_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130*)p0, (int32_t)p1, (Comparison_1_t2833 *)p2, method)
// Declaration System.Void System.Array::Sort<UnityEngine.EventSystems.BaseInputModule>(!!0[],System.Int32,System.Comparison`1<!!0>)
// System.Void System.Array::Sort<UnityEngine.EventSystems.BaseInputModule>(!!0[],System.Int32,System.Comparison`1<!!0>)
#define Array_Sort_TisBaseInputModule_t234_m34177(__this/* static, unused */, p0, p1, p2, method) (void)Array_Sort_TisObject_t_m32853_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t130*)p0, (int32_t)p1, (Comparison_1_t2833 *)p2, method)


// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.cctor()
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IEnumerable.GetEnumerator()
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Add(System.Object)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Contains(System.Object)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.IndexOf(System.Object)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Insert(System.Int32,System.Object)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Remove(System.Object)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_SyncRoot()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsFixedSize()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsReadOnly()
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_Item(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.set_Item(System.Int32,System.Object)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Add(T)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GrowIfNeeded(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddCollection(System.Collections.Generic.ICollection`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AsReadOnly()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Clear()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Contains(T)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CopyTo(T[],System.Int32)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Find(System.Predicate`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckMatch(System.Predicate`1<T>)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GetEnumerator()
 Enumerator_t3184  List_1_GetEnumerator_m16118 (List_1_t233 * __this, MethodInfo* method){
	{
		Enumerator_t3184  L_0 = {0};
		Enumerator__ctor_m16139(&L_0, __this, /*hidden argument*/&Enumerator__ctor_m16139_MethodInfo);
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::IndexOf(T)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Shift(System.Int32,System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckIndex(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Remove(T)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAll(System.Predicate`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAt(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Reverse()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Sort()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Sort(System.Comparison`1<T>)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::ToArray()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::TrimExcess()
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Capacity()
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::set_Capacity(System.Int32)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Count()
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Item(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo List_1_t233____DefaultCapacity_0_FieldInfo = 
{
	"DefaultCapacity"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &List_1_t233_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType BaseInputModuleU5BU5D_t3177_0_0_1;
FieldInfo List_1_t233_____items_1_FieldInfo = 
{
	"_items"/* name */
	, &BaseInputModuleU5BU5D_t3177_0_0_1/* type */
	, &List_1_t233_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t233, ____items_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo List_1_t233_____size_2_FieldInfo = 
{
	"_size"/* name */
	, &Int32_t123_0_0_1/* type */
	, &List_1_t233_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t233, ____size_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo List_1_t233_____version_3_FieldInfo = 
{
	"_version"/* name */
	, &Int32_t123_0_0_1/* type */
	, &List_1_t233_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t233, ____version_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType BaseInputModuleU5BU5D_t3177_0_0_49;
FieldInfo List_1_t233____EmptyArray_4_FieldInfo = 
{
	"EmptyArray"/* name */
	, &BaseInputModuleU5BU5D_t3177_0_0_49/* type */
	, &List_1_t233_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t233_StaticFields, ___EmptyArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* List_1_t233_FieldInfos[] =
{
	&List_1_t233____DefaultCapacity_0_FieldInfo,
	&List_1_t233_____items_1_FieldInfo,
	&List_1_t233_____size_2_FieldInfo,
	&List_1_t233_____version_3_FieldInfo,
	&List_1_t233____EmptyArray_4_FieldInfo,
	NULL
};
static const int32_t List_1_t233____DefaultCapacity_0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry List_1_t233____DefaultCapacity_0_DefaultValue = 
{
	&List_1_t233____DefaultCapacity_0_FieldInfo/* field */
	, { (char*)&List_1_t233____DefaultCapacity_0_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* List_1_t233_FieldDefaultValues[] = 
{
	&List_1_t233____DefaultCapacity_0_DefaultValue,
	NULL
};
extern MethodInfo List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_MethodInfo;
static PropertyInfo List_1_t233____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_ICollection_get_IsSynchronized_m16100_MethodInfo;
static PropertyInfo List_1_t233____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &List_1_System_Collections_ICollection_get_IsSynchronized_m16100_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_ICollection_get_SyncRoot_m16101_MethodInfo;
static PropertyInfo List_1_t233____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &List_1_System_Collections_ICollection_get_SyncRoot_m16101_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_IList_get_IsFixedSize_m16102_MethodInfo;
static PropertyInfo List_1_t233____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsFixedSize"/* name */
	, &List_1_System_Collections_IList_get_IsFixedSize_m16102_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_IList_get_IsReadOnly_m16103_MethodInfo;
static PropertyInfo List_1_t233____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsReadOnly"/* name */
	, &List_1_System_Collections_IList_get_IsReadOnly_m16103_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_IList_get_Item_m16104_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_set_Item_m16105_MethodInfo;
static PropertyInfo List_1_t233____System_Collections_IList_Item_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.Item"/* name */
	, &List_1_System_Collections_IList_get_Item_m16104_MethodInfo/* get */
	, &List_1_System_Collections_IList_set_Item_m16105_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo List_1_t233____Capacity_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "Capacity"/* name */
	, &List_1_get_Capacity_m16131_MethodInfo/* get */
	, &List_1_set_Capacity_m16132_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_get_Count_m2061_MethodInfo;
static PropertyInfo List_1_t233____Count_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &List_1_get_Count_m2061_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo List_1_t233____Item_PropertyInfo = 
{
	&List_1_t233_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &List_1_get_Item_m2062_MethodInfo/* get */
	, &List_1_set_Item_m16133_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* List_1_t233_PropertyInfos[] =
{
	&List_1_t233____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&List_1_t233____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&List_1_t233____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&List_1_t233____System_Collections_IList_IsFixedSize_PropertyInfo,
	&List_1_t233____System_Collections_IList_IsReadOnly_PropertyInfo,
	&List_1_t233____System_Collections_IList_Item_PropertyInfo,
	&List_1_t233____Capacity_PropertyInfo,
	&List_1_t233____Count_PropertyInfo,
	&List_1_t233____Item_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__ctor_m2058_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor()
MethodInfo List_1__ctor_m2058_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&List_1__ctor_m14461_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__ctor_m2058_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t3178_0_0_0;
extern Il2CppType IEnumerable_1_t3178_0_0_0;
static ParameterInfo List_1_t233_List_1__ctor_m16088_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t3178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__ctor_m16088_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1__ctor_m16088_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&List_1__ctor_m14463_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1__ctor_m16088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__ctor_m16088_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1__ctor_m16089_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__ctor_m16089_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Int32)
MethodInfo List_1__ctor_m16089_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&List_1__ctor_m14465_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, List_1_t233_List_1__ctor_m16089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__ctor_m16089_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__cctor_m16090_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.cctor()
MethodInfo List_1__cctor_m16090_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&List_1__cctor_m14467_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__cctor_m16090_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t3179_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, (methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14469_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3179_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_ICollection_CopyTo_m16092_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_ICollection_CopyTo_m16092_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo List_1_System_Collections_ICollection_CopyTo_m16092_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m14471_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, List_1_t233_List_1_System_Collections_ICollection_CopyTo_m16092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_ICollection_CopyTo_m16092_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IEnumerable_GetEnumerator_m16093_GenericMethod;
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo List_1_System_Collections_IEnumerable_GetEnumerator_m16093_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m14473_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IEnumerable_GetEnumerator_m16093_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_Add_m16094_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Add_m16094_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Add(System.Object)
MethodInfo List_1_System_Collections_IList_Add_m16094_MethodInfo = 
{
	"System.Collections.IList.Add"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Add_m14475_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_Add_m16094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Add_m16094_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_Contains_m16095_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Contains_m16095_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Contains(System.Object)
MethodInfo List_1_System_Collections_IList_Contains_m16095_MethodInfo = 
{
	"System.Collections.IList.Contains"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Contains_m14477_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_Contains_m16095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Contains_m16095_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_IndexOf_m16096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_IndexOf_m16096_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.IndexOf(System.Object)
MethodInfo List_1_System_Collections_IList_IndexOf_m16096_MethodInfo = 
{
	"System.Collections.IList.IndexOf"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_IndexOf_m14479_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_IndexOf_m16096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_IndexOf_m16096_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_Insert_m16097_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Insert_m16097_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Insert(System.Int32,System.Object)
MethodInfo List_1_System_Collections_IList_Insert_m16097_MethodInfo = 
{
	"System.Collections.IList.Insert"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Insert_m14481_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_Insert_m16097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Insert_m16097_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_Remove_m16098_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Remove_m16098_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Remove(System.Object)
MethodInfo List_1_System_Collections_IList_Remove_m16098_MethodInfo = 
{
	"System.Collections.IList.Remove"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Remove_m14483_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_Remove_m16098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Remove_m16098_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14485_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_ICollection_get_IsSynchronized_m16100_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo List_1_System_Collections_ICollection_get_IsSynchronized_m16100_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m14487_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_ICollection_get_IsSynchronized_m16100_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_ICollection_get_SyncRoot_m16101_GenericMethod;
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_SyncRoot()
MethodInfo List_1_System_Collections_ICollection_get_SyncRoot_m16101_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m14489_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_ICollection_get_SyncRoot_m16101_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_get_IsFixedSize_m16102_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsFixedSize()
MethodInfo List_1_System_Collections_IList_get_IsFixedSize_m16102_MethodInfo = 
{
	"System.Collections.IList.get_IsFixedSize"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m14491_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_get_IsFixedSize_m16102_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_get_IsReadOnly_m16103_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsReadOnly()
MethodInfo List_1_System_Collections_IList_get_IsReadOnly_m16103_MethodInfo = 
{
	"System.Collections.IList.get_IsReadOnly"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m14493_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_get_IsReadOnly_m16103_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_get_Item_m16104_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_get_Item_m16104_GenericMethod;
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_Item(System.Int32)
MethodInfo List_1_System_Collections_IList_get_Item_m16104_MethodInfo = 
{
	"System.Collections.IList.get_Item"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_get_Item_m14495_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_get_Item_m16104_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_get_Item_m16104_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t233_List_1_System_Collections_IList_set_Item_m16105_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_set_Item_m16105_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.set_Item(System.Int32,System.Object)
MethodInfo List_1_System_Collections_IList_set_Item_m16105_MethodInfo = 
{
	"System.Collections.IList.set_Item"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_set_Item_m14497_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_System_Collections_IList_set_Item_m16105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_set_Item_m16105_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo List_1_t233_List_1_Add_m16106_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Add_m16106_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Add(T)
MethodInfo List_1_Add_m16106_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&List_1_Add_m14499_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_Add_m16106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Add_m16106_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_GrowIfNeeded_m16107_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_GrowIfNeeded_m16107_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GrowIfNeeded(System.Int32)
MethodInfo List_1_GrowIfNeeded_m16107_MethodInfo = 
{
	"GrowIfNeeded"/* name */
	, (methodPointerType)&List_1_GrowIfNeeded_m14501_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, List_1_t233_List_1_GrowIfNeeded_m16107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_GrowIfNeeded_m16107_GenericMethod/* genericMethod */

};
extern Il2CppType ICollection_1_t3180_0_0_0;
extern Il2CppType ICollection_1_t3180_0_0_0;
static ParameterInfo List_1_t233_List_1_AddCollection_m16108_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &ICollection_1_t3180_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AddCollection_m16108_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddCollection(System.Collections.Generic.ICollection`1<T>)
MethodInfo List_1_AddCollection_m16108_MethodInfo = 
{
	"AddCollection"/* name */
	, (methodPointerType)&List_1_AddCollection_m14503_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_AddCollection_m16108_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AddCollection_m16108_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t3178_0_0_0;
static ParameterInfo List_1_t233_List_1_AddEnumerable_m16109_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t3178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AddEnumerable_m16109_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1_AddEnumerable_m16109_MethodInfo = 
{
	"AddEnumerable"/* name */
	, (methodPointerType)&List_1_AddEnumerable_m14505_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_AddEnumerable_m16109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AddEnumerable_m16109_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t3178_0_0_0;
static ParameterInfo List_1_t233_List_1_AddRange_m16110_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t3178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AddRange_m16110_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1_AddRange_m16110_MethodInfo = 
{
	"AddRange"/* name */
	, (methodPointerType)&List_1_AddRange_m14506_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_AddRange_m16110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AddRange_m16110_GenericMethod/* genericMethod */

};
extern Il2CppType ReadOnlyCollection_1_t3181_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AsReadOnly_m16111_GenericMethod;
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AsReadOnly()
MethodInfo List_1_AsReadOnly_m16111_MethodInfo = 
{
	"AsReadOnly"/* name */
	, (methodPointerType)&List_1_AsReadOnly_m14508_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &ReadOnlyCollection_1_t3181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AsReadOnly_m16111_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Clear_m16112_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Clear()
MethodInfo List_1_Clear_m16112_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&List_1_Clear_m14510_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Clear_m16112_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo List_1_t233_List_1_Contains_m16113_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Contains_m16113_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Contains(T)
MethodInfo List_1_Contains_m16113_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&List_1_Contains_m14512_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, List_1_t233_List_1_Contains_m16113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Contains_m16113_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModuleU5BU5D_t3177_0_0_0;
extern Il2CppType BaseInputModuleU5BU5D_t3177_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_CopyTo_m16114_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModuleU5BU5D_t3177_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CopyTo_m16114_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CopyTo(T[],System.Int32)
MethodInfo List_1_CopyTo_m16114_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&List_1_CopyTo_m14514_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, List_1_t233_List_1_CopyTo_m16114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CopyTo_m16114_GenericMethod/* genericMethod */

};
extern Il2CppType Predicate_1_t3182_0_0_0;
extern Il2CppType Predicate_1_t3182_0_0_0;
static ParameterInfo List_1_t233_List_1_Find_m16115_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t3182_0_0_0},
};
extern Il2CppType BaseInputModule_t234_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Find_m16115_GenericMethod;
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Find(System.Predicate`1<T>)
MethodInfo List_1_Find_m16115_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&List_1_Find_m14516_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &BaseInputModule_t234_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, List_1_t233_List_1_Find_m16115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Find_m16115_GenericMethod/* genericMethod */

};
extern Il2CppType Predicate_1_t3182_0_0_0;
static ParameterInfo List_1_t233_List_1_CheckMatch_m16116_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t3182_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CheckMatch_m16116_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckMatch(System.Predicate`1<T>)
MethodInfo List_1_CheckMatch_m16116_MethodInfo = 
{
	"CheckMatch"/* name */
	, (methodPointerType)&List_1_CheckMatch_m14518_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_CheckMatch_m16116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CheckMatch_m16116_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Predicate_1_t3182_0_0_0;
static ParameterInfo List_1_t233_List_1_GetIndex_m16117_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t3182_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_GetIndex_m16117_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
MethodInfo List_1_GetIndex_m16117_MethodInfo = 
{
	"GetIndex"/* name */
	, (methodPointerType)&List_1_GetIndex_m14520_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_GetIndex_m16117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_GetIndex_m16117_GenericMethod/* genericMethod */

};
extern Il2CppType Enumerator_t3184_0_0_0;
extern void* RuntimeInvoker_Enumerator_t3184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_GetEnumerator_m16118_GenericMethod;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GetEnumerator()
MethodInfo List_1_GetEnumerator_m16118_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&List_1_GetEnumerator_m16118/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t3184_0_0_0/* return_type */
	, RuntimeInvoker_Enumerator_t3184/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_GetEnumerator_m16118_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo List_1_t233_List_1_IndexOf_m16119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_IndexOf_m16119_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::IndexOf(T)
MethodInfo List_1_IndexOf_m16119_MethodInfo = 
{
	"IndexOf"/* name */
	, (methodPointerType)&List_1_IndexOf_m14522_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_IndexOf_m16119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_IndexOf_m16119_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_Shift_m16120_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Shift_m16120_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Shift(System.Int32,System.Int32)
MethodInfo List_1_Shift_m16120_MethodInfo = 
{
	"Shift"/* name */
	, (methodPointerType)&List_1_Shift_m14524_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, List_1_t233_List_1_Shift_m16120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Shift_m16120_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_CheckIndex_m16121_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CheckIndex_m16121_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckIndex(System.Int32)
MethodInfo List_1_CheckIndex_m16121_MethodInfo = 
{
	"CheckIndex"/* name */
	, (methodPointerType)&List_1_CheckIndex_m14526_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, List_1_t233_List_1_CheckIndex_m16121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CheckIndex_m16121_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo List_1_t233_List_1_Insert_m16122_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Insert_m16122_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Insert(System.Int32,T)
MethodInfo List_1_Insert_m16122_MethodInfo = 
{
	"Insert"/* name */
	, (methodPointerType)&List_1_Insert_m14528_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_Insert_m16122_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Insert_m16122_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t3178_0_0_0;
static ParameterInfo List_1_t233_List_1_CheckCollection_m16123_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t3178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CheckCollection_m16123_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1_CheckCollection_m16123_MethodInfo = 
{
	"CheckCollection"/* name */
	, (methodPointerType)&List_1_CheckCollection_m14530_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_CheckCollection_m16123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CheckCollection_m16123_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo List_1_t233_List_1_Remove_m16124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Remove_m16124_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Remove(T)
MethodInfo List_1_Remove_m16124_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&List_1_Remove_m14532_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, List_1_t233_List_1_Remove_m16124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Remove_m16124_GenericMethod/* genericMethod */

};
extern Il2CppType Predicate_1_t3182_0_0_0;
static ParameterInfo List_1_t233_List_1_RemoveAll_m16125_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t3182_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_RemoveAll_m16125_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAll(System.Predicate`1<T>)
MethodInfo List_1_RemoveAll_m16125_MethodInfo = 
{
	"RemoveAll"/* name */
	, (methodPointerType)&List_1_RemoveAll_m14534_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_RemoveAll_m16125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_RemoveAll_m16125_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_RemoveAt_m2063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_RemoveAt_m2063_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAt(System.Int32)
MethodInfo List_1_RemoveAt_m2063_MethodInfo = 
{
	"RemoveAt"/* name */
	, (methodPointerType)&List_1_RemoveAt_m14536_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, List_1_t233_List_1_RemoveAt_m2063_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_RemoveAt_m2063_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Reverse_m16126_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Reverse()
MethodInfo List_1_Reverse_m16126_MethodInfo = 
{
	"Reverse"/* name */
	, (methodPointerType)&List_1_Reverse_m14538_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Reverse_m16126_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Sort_m16127_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Sort()
MethodInfo List_1_Sort_m16127_MethodInfo = 
{
	"Sort"/* name */
	, (methodPointerType)&List_1_Sort_m14540_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Sort_m16127_GenericMethod/* genericMethod */

};
extern Il2CppType Comparison_1_t3183_0_0_0;
extern Il2CppType Comparison_1_t3183_0_0_0;
static ParameterInfo List_1_t233_List_1_Sort_m16128_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &Comparison_1_t3183_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Sort_m16128_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Sort(System.Comparison`1<T>)
MethodInfo List_1_Sort_m16128_MethodInfo = 
{
	"Sort"/* name */
	, (methodPointerType)&List_1_Sort_m14542_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, List_1_t233_List_1_Sort_m16128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Sort_m16128_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModuleU5BU5D_t3177_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_ToArray_m16129_GenericMethod;
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::ToArray()
MethodInfo List_1_ToArray_m16129_MethodInfo = 
{
	"ToArray"/* name */
	, (methodPointerType)&List_1_ToArray_m14544_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &BaseInputModuleU5BU5D_t3177_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_ToArray_m16129_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_TrimExcess_m16130_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::TrimExcess()
MethodInfo List_1_TrimExcess_m16130_MethodInfo = 
{
	"TrimExcess"/* name */
	, (methodPointerType)&List_1_TrimExcess_m14546_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_TrimExcess_m16130_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_get_Capacity_m16131_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Capacity()
MethodInfo List_1_get_Capacity_m16131_MethodInfo = 
{
	"get_Capacity"/* name */
	, (methodPointerType)&List_1_get_Capacity_m14548_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_get_Capacity_m16131_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_set_Capacity_m16132_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_set_Capacity_m16132_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::set_Capacity(System.Int32)
MethodInfo List_1_set_Capacity_m16132_MethodInfo = 
{
	"set_Capacity"/* name */
	, (methodPointerType)&List_1_set_Capacity_m14550_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, List_1_t233_List_1_set_Capacity_m16132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_set_Capacity_m16132_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_get_Count_m2061_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Count()
MethodInfo List_1_get_Count_m2061_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&List_1_get_Count_m14552_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_get_Count_m2061_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo List_1_t233_List_1_get_Item_m2062_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BaseInputModule_t234_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_get_Item_m2062_GenericMethod;
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Item(System.Int32)
MethodInfo List_1_get_Item_m2062_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&List_1_get_Item_m14554_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &BaseInputModule_t234_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, List_1_t233_List_1_get_Item_m2062_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_get_Item_m2062_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo List_1_t233_List_1_set_Item_m16133_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_set_Item_m16133_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::set_Item(System.Int32,T)
MethodInfo List_1_set_Item_m16133_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&List_1_set_Item_m14556_gshared/* method */
	, &List_1_t233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, List_1_t233_List_1_set_Item_m16133_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_set_Item_m16133_GenericMethod/* genericMethod */

};
static MethodInfo* List_1_t233_MethodInfos[] =
{
	&List_1__ctor_m2058_MethodInfo,
	&List_1__ctor_m16088_MethodInfo,
	&List_1__ctor_m16089_MethodInfo,
	&List_1__cctor_m16090_MethodInfo,
	&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091_MethodInfo,
	&List_1_System_Collections_ICollection_CopyTo_m16092_MethodInfo,
	&List_1_System_Collections_IEnumerable_GetEnumerator_m16093_MethodInfo,
	&List_1_System_Collections_IList_Add_m16094_MethodInfo,
	&List_1_System_Collections_IList_Contains_m16095_MethodInfo,
	&List_1_System_Collections_IList_IndexOf_m16096_MethodInfo,
	&List_1_System_Collections_IList_Insert_m16097_MethodInfo,
	&List_1_System_Collections_IList_Remove_m16098_MethodInfo,
	&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_MethodInfo,
	&List_1_System_Collections_ICollection_get_IsSynchronized_m16100_MethodInfo,
	&List_1_System_Collections_ICollection_get_SyncRoot_m16101_MethodInfo,
	&List_1_System_Collections_IList_get_IsFixedSize_m16102_MethodInfo,
	&List_1_System_Collections_IList_get_IsReadOnly_m16103_MethodInfo,
	&List_1_System_Collections_IList_get_Item_m16104_MethodInfo,
	&List_1_System_Collections_IList_set_Item_m16105_MethodInfo,
	&List_1_Add_m16106_MethodInfo,
	&List_1_GrowIfNeeded_m16107_MethodInfo,
	&List_1_AddCollection_m16108_MethodInfo,
	&List_1_AddEnumerable_m16109_MethodInfo,
	&List_1_AddRange_m16110_MethodInfo,
	&List_1_AsReadOnly_m16111_MethodInfo,
	&List_1_Clear_m16112_MethodInfo,
	&List_1_Contains_m16113_MethodInfo,
	&List_1_CopyTo_m16114_MethodInfo,
	&List_1_Find_m16115_MethodInfo,
	&List_1_CheckMatch_m16116_MethodInfo,
	&List_1_GetIndex_m16117_MethodInfo,
	&List_1_GetEnumerator_m16118_MethodInfo,
	&List_1_IndexOf_m16119_MethodInfo,
	&List_1_Shift_m16120_MethodInfo,
	&List_1_CheckIndex_m16121_MethodInfo,
	&List_1_Insert_m16122_MethodInfo,
	&List_1_CheckCollection_m16123_MethodInfo,
	&List_1_Remove_m16124_MethodInfo,
	&List_1_RemoveAll_m16125_MethodInfo,
	&List_1_RemoveAt_m2063_MethodInfo,
	&List_1_Reverse_m16126_MethodInfo,
	&List_1_Sort_m16127_MethodInfo,
	&List_1_Sort_m16128_MethodInfo,
	&List_1_ToArray_m16129_MethodInfo,
	&List_1_TrimExcess_m16130_MethodInfo,
	&List_1_get_Capacity_m16131_MethodInfo,
	&List_1_set_Capacity_m16132_MethodInfo,
	&List_1_get_Count_m2061_MethodInfo,
	&List_1_get_Item_m2062_MethodInfo,
	&List_1_set_Item_m16133_MethodInfo,
	NULL
};
extern MethodInfo List_1_System_Collections_IEnumerable_GetEnumerator_m16093_MethodInfo;
extern MethodInfo List_1_System_Collections_ICollection_CopyTo_m16092_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Add_m16094_MethodInfo;
extern MethodInfo List_1_Clear_m16112_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Contains_m16095_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_IndexOf_m16096_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Insert_m16097_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Remove_m16098_MethodInfo;
extern MethodInfo List_1_CopyTo_m16114_MethodInfo;
extern MethodInfo List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091_MethodInfo;
static MethodInfo* List_1_t233_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&List_1_System_Collections_IEnumerable_GetEnumerator_m16093_MethodInfo,
	&List_1_get_Count_m2061_MethodInfo,
	&List_1_System_Collections_ICollection_get_IsSynchronized_m16100_MethodInfo,
	&List_1_System_Collections_ICollection_get_SyncRoot_m16101_MethodInfo,
	&List_1_System_Collections_ICollection_CopyTo_m16092_MethodInfo,
	&List_1_System_Collections_IList_get_IsFixedSize_m16102_MethodInfo,
	&List_1_System_Collections_IList_get_IsReadOnly_m16103_MethodInfo,
	&List_1_System_Collections_IList_get_Item_m16104_MethodInfo,
	&List_1_System_Collections_IList_set_Item_m16105_MethodInfo,
	&List_1_System_Collections_IList_Add_m16094_MethodInfo,
	&List_1_Clear_m16112_MethodInfo,
	&List_1_System_Collections_IList_Contains_m16095_MethodInfo,
	&List_1_System_Collections_IList_IndexOf_m16096_MethodInfo,
	&List_1_System_Collections_IList_Insert_m16097_MethodInfo,
	&List_1_System_Collections_IList_Remove_m16098_MethodInfo,
	&List_1_RemoveAt_m2063_MethodInfo,
	&List_1_get_Count_m2061_MethodInfo,
	&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099_MethodInfo,
	&List_1_Add_m16106_MethodInfo,
	&List_1_Clear_m16112_MethodInfo,
	&List_1_Contains_m16113_MethodInfo,
	&List_1_CopyTo_m16114_MethodInfo,
	&List_1_Remove_m16124_MethodInfo,
	&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091_MethodInfo,
	&List_1_IndexOf_m16119_MethodInfo,
	&List_1_Insert_m16122_MethodInfo,
	&List_1_RemoveAt_m2063_MethodInfo,
	&List_1_get_Item_m2062_MethodInfo,
	&List_1_set_Item_m16133_MethodInfo,
};
extern TypeInfo ICollection_t1259_il2cpp_TypeInfo;
extern TypeInfo IList_t1488_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3186_il2cpp_TypeInfo;
static TypeInfo* List_1_t233_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_t1259_il2cpp_TypeInfo,
	&IList_t1488_il2cpp_TypeInfo,
	&ICollection_1_t3180_il2cpp_TypeInfo,
	&IEnumerable_1_t3178_il2cpp_TypeInfo,
	&IList_1_t3186_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair List_1_t233_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1179_il2cpp_TypeInfo, 4},
	{ &ICollection_t1259_il2cpp_TypeInfo, 5},
	{ &IList_t1488_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3180_il2cpp_TypeInfo, 20},
	{ &IEnumerable_1_t3178_il2cpp_TypeInfo, 27},
	{ &IList_1_t3186_il2cpp_TypeInfo, 28},
};
extern TypeInfo List_1_t233_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t3180_il2cpp_TypeInfo;
extern TypeInfo BaseInputModuleU5BU5D_t3177_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3184_il2cpp_TypeInfo;
extern TypeInfo BaseInputModule_t234_il2cpp_TypeInfo;
extern TypeInfo ReadOnlyCollection_1_t3181_il2cpp_TypeInfo;
static Il2CppRGCTXData List_1_t233_RGCTXData[37] = 
{
	&List_1_t233_il2cpp_TypeInfo/* Static Usage */,
	&List_1_CheckCollection_m16123_MethodInfo/* Method Usage */,
	&ICollection_1_t3180_il2cpp_TypeInfo/* Class Usage */,
	&List_1_AddEnumerable_m16109_MethodInfo/* Method Usage */,
	&ICollection_1_get_Count_m44652_MethodInfo/* Method Usage */,
	&BaseInputModuleU5BU5D_t3177_il2cpp_TypeInfo/* Array Usage */,
	&List_1_AddCollection_m16108_MethodInfo/* Method Usage */,
	&List_1_GetEnumerator_m16118_MethodInfo/* Method Usage */,
	&Enumerator_t3184_il2cpp_TypeInfo/* Class Usage */,
	&BaseInputModule_t234_il2cpp_TypeInfo/* Class Usage */,
	&List_1_Add_m16106_MethodInfo/* Method Usage */,
	&List_1_Contains_m16113_MethodInfo/* Method Usage */,
	&List_1_IndexOf_m16119_MethodInfo/* Method Usage */,
	&List_1_CheckIndex_m16121_MethodInfo/* Method Usage */,
	&List_1_Insert_m16122_MethodInfo/* Method Usage */,
	&List_1_Remove_m16124_MethodInfo/* Method Usage */,
	&List_1_get_Item_m2062_MethodInfo/* Method Usage */,
	&List_1_set_Item_m16133_MethodInfo/* Method Usage */,
	&List_1_GrowIfNeeded_m16107_MethodInfo/* Method Usage */,
	&List_1_get_Capacity_m16131_MethodInfo/* Method Usage */,
	&List_1_set_Capacity_m16132_MethodInfo/* Method Usage */,
	&ICollection_1_CopyTo_m44653_MethodInfo/* Method Usage */,
	&IEnumerable_1_GetEnumerator_m44654_MethodInfo/* Method Usage */,
	&IEnumerator_1_get_Current_m44655_MethodInfo/* Method Usage */,
	&ReadOnlyCollection_1_t3181_il2cpp_TypeInfo/* Class Usage */,
	&ReadOnlyCollection_1__ctor_m16145_MethodInfo/* Method Usage */,
	&Array_IndexOf_TisBaseInputModule_t234_m34167_MethodInfo/* Method Usage */,
	&List_1_CheckMatch_m16116_MethodInfo/* Method Usage */,
	&List_1_GetIndex_m16117_MethodInfo/* Method Usage */,
	&Predicate_1_Invoke_m16220_MethodInfo/* Method Usage */,
	&Enumerator__ctor_m16139_MethodInfo/* Method Usage */,
	&List_1_Shift_m16120_MethodInfo/* Method Usage */,
	&List_1_RemoveAt_m2063_MethodInfo/* Method Usage */,
	&Comparer_1_get_Default_m16226_MethodInfo/* Method Usage */,
	&Array_Sort_TisBaseInputModule_t234_m34169_MethodInfo/* Method Usage */,
	&Array_Sort_TisBaseInputModule_t234_m34177_MethodInfo/* Method Usage */,
	&Array_Resize_TisBaseInputModule_t234_m34165_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType List_1_t233_0_0_0;
extern Il2CppType List_1_t233_1_0_0;
struct List_1_t233;
extern Il2CppGenericClass List_1_t233_GenericClass;
extern CustomAttributesCache List_1_t1871__CustomAttributeCache;
TypeInfo List_1_t233_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "List`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, List_1_t233_MethodInfos/* methods */
	, List_1_t233_PropertyInfos/* properties */
	, List_1_t233_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &List_1_t233_il2cpp_TypeInfo/* element_class */
	, List_1_t233_InterfacesTypeInfos/* implemented_interfaces */
	, List_1_t233_VTable/* vtable */
	, &List_1_t1871__CustomAttributeCache/* custom_attributes_cache */
	, &List_1_t233_il2cpp_TypeInfo/* cast_class */
	, &List_1_t233_0_0_0/* byval_arg */
	, &List_1_t233_1_0_0/* this_arg */
	, List_1_t233_InterfacesOffsets/* interface_offsets */
	, &List_1_t233_GenericClass/* generic_class */
	, NULL/* generic_container */
	, List_1_t233_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, List_1_t233_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (List_1_t233)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(List_1_t233_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 50/* method_count */
	, 9/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>
static PropertyInfo ICollection_1_t3180____Count_PropertyInfo = 
{
	&ICollection_1_t3180_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44652_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44656_MethodInfo;
static PropertyInfo ICollection_1_t3180____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3180_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44656_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3180_PropertyInfos[] =
{
	&ICollection_1_t3180____Count_PropertyInfo,
	&ICollection_1_t3180____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44652_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::get_Count()
MethodInfo ICollection_1_get_Count_m44652_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44652_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44656_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44656_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44656_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo ICollection_1_t3180_ICollection_1_Add_m44657_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44657_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Add(T)
MethodInfo ICollection_1_Add_m44657_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t3180_ICollection_1_Add_m44657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44657_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44658_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Clear()
MethodInfo ICollection_1_Clear_m44658_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44658_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo ICollection_1_t3180_ICollection_1_Contains_m44659_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44659_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Contains(T)
MethodInfo ICollection_1_Contains_m44659_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t3180_ICollection_1_Contains_m44659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44659_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModuleU5BU5D_t3177_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t3180_ICollection_1_CopyTo_m44653_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModuleU5BU5D_t3177_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44653_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44653_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t3180_ICollection_1_CopyTo_m44653_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44653_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInputModule_t234_0_0_0;
static ParameterInfo ICollection_1_t3180_ICollection_1_Remove_m44660_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseInputModule_t234_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44660_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>::Remove(T)
MethodInfo ICollection_1_Remove_m44660_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t3180_ICollection_1_Remove_m44660_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44660_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3180_MethodInfos[] =
{
	&ICollection_1_get_Count_m44652_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44656_MethodInfo,
	&ICollection_1_Add_m44657_MethodInfo,
	&ICollection_1_Clear_m44658_MethodInfo,
	&ICollection_1_Contains_m44659_MethodInfo,
	&ICollection_1_CopyTo_m44653_MethodInfo,
	&ICollection_1_Remove_m44660_MethodInfo,
	NULL
};
static TypeInfo* ICollection_1_t3180_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t3178_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3180_1_0_0;
struct ICollection_1_t3180;
extern Il2CppGenericClass ICollection_1_t3180_GenericClass;
TypeInfo ICollection_1_t3180_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3180_MethodInfos/* methods */
	, ICollection_1_t3180_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3180_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3180_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3180_0_0_0/* byval_arg */
	, &ICollection_1_t3180_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3180_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseInputModule>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseInputModule>
extern Il2CppType IEnumerator_1_t3179_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44654_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseInputModule>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44654_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3178_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3179_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44654_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3178_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44654_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3178_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3178_1_0_0;
struct IEnumerable_1_t3178;
extern Il2CppGenericClass IEnumerable_1_t3178_GenericClass;
TypeInfo IEnumerable_1_t3178_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3178_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3178_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3178_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3178_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3178_0_0_0/* byval_arg */
	, &IEnumerable_1_t3178_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3178_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseInputModule>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseInputModule>
static PropertyInfo IEnumerator_1_t3179____Current_PropertyInfo = 
{
	&IEnumerator_1_t3179_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44655_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3179_PropertyInfos[] =
{
	&IEnumerator_1_t3179____Current_PropertyInfo,
	NULL
};
extern Il2CppType BaseInputModule_t234_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44655_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseInputModule>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44655_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3179_il2cpp_TypeInfo/* declaring_type */
	, &BaseInputModule_t234_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44655_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3179_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44655_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3179_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3179_0_0_0;
extern Il2CppType IEnumerator_1_t3179_1_0_0;
struct IEnumerator_1_t3179;
extern Il2CppGenericClass IEnumerator_1_t3179_GenericClass;
TypeInfo IEnumerator_1_t3179_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3179_MethodInfos/* methods */
	, IEnumerator_1_t3179_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3179_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3179_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3179_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3179_0_0_0/* byval_arg */
	, &IEnumerator_1_t3179_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3179_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
