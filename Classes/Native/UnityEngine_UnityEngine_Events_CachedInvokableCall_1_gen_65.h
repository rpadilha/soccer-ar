﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<RotationConstraint>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_61.h"
// UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>
struct CachedInvokableCall_1_t3150  : public InvokableCall_1_t3151
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<RotationConstraint>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
