﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4
struct U3CClickRepeatU3Ec__Iterator4_t393;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::.ctor()
 void U3CClickRepeatU3Ec__Iterator4__ctor_m1518 (U3CClickRepeatU3Ec__Iterator4_t393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
 Object_t * U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1519 (U3CClickRepeatU3Ec__Iterator4_t393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::System.Collections.IEnumerator.get_Current()
 Object_t * U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1520 (U3CClickRepeatU3Ec__Iterator4_t393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::MoveNext()
 bool U3CClickRepeatU3Ec__Iterator4_MoveNext_m1521 (U3CClickRepeatU3Ec__Iterator4_t393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::Dispose()
 void U3CClickRepeatU3Ec__Iterator4_Dispose_m1522 (U3CClickRepeatU3Ec__Iterator4_t393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::Reset()
 void U3CClickRepeatU3Ec__Iterator4_Reset_m1523 (U3CClickRepeatU3Ec__Iterator4_t393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
