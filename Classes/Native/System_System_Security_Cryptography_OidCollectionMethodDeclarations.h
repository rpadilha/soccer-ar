﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.OidCollection
struct OidCollection_t1434;
// System.Security.Cryptography.Oid
struct Oid_t1405;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;

// System.Void System.Security.Cryptography.OidCollection::.ctor()
 void OidCollection__ctor_m7317 (OidCollection_t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.OidCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void OidCollection_System_Collections_ICollection_CopyTo_m7318 (OidCollection_t1434 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.OidCollection::System.Collections.IEnumerable.GetEnumerator()
 Object_t * OidCollection_System_Collections_IEnumerable_GetEnumerator_m7319 (OidCollection_t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.OidCollection::get_Count()
 int32_t OidCollection_get_Count_m7320 (OidCollection_t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.OidCollection::get_IsSynchronized()
 bool OidCollection_get_IsSynchronized_m7321 (OidCollection_t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.OidCollection::get_Item(System.Int32)
 Oid_t1405 * OidCollection_get_Item_m7322 (OidCollection_t1434 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.OidCollection::get_SyncRoot()
 Object_t * OidCollection_get_SyncRoot_m7323 (OidCollection_t1434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.OidCollection::Add(System.Security.Cryptography.Oid)
 int32_t OidCollection_Add_m7324 (OidCollection_t1434 * __this, Oid_t1405 * ___oid, MethodInfo* method) IL2CPP_METHOD_ATTR;
