﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t817;
struct __StaticArrayInitTypeSizeU3D24_t817_marshaled;

void __StaticArrayInitTypeSizeU3D24_t817_marshal(const __StaticArrayInitTypeSizeU3D24_t817& unmarshaled, __StaticArrayInitTypeSizeU3D24_t817_marshaled& marshaled);
void __StaticArrayInitTypeSizeU3D24_t817_marshal_back(const __StaticArrayInitTypeSizeU3D24_t817_marshaled& marshaled, __StaticArrayInitTypeSizeU3D24_t817& unmarshaled);
void __StaticArrayInitTypeSizeU3D24_t817_marshal_cleanup(__StaticArrayInitTypeSizeU3D24_t817_marshaled& marshaled);
