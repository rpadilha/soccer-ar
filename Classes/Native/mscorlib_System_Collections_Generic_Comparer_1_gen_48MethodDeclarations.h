﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>
struct Comparer_1_t4593;
// System.Object
struct Object_t;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t815;

// System.Void System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m27809(__this, method) (void)Comparer_1__ctor_m14672_gshared((Comparer_1_t2848 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>::.cctor()
#define Comparer_1__cctor_m27810(__this/* static, unused */, method) (void)Comparer_1__cctor_m14673_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m27811(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14674_gshared((Comparer_1_t2848 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>::get_Default()
#define Comparer_1_get_Default_m27812(__this/* static, unused */, method) (Comparer_1_t4593 *)Comparer_1_get_Default_m14675_gshared((Object_t *)__this/* static, unused */, method)
