﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyValuePair_2_t4443 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::value
	ProfileData_t791  ___value_1;
};
