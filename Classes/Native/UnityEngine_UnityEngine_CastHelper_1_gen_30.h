﻿#pragma once
#include <stdint.h>
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t51;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.SurfaceAbstractBehaviour>
struct CastHelper_1_t4329 
{
	// T UnityEngine.CastHelper`1<Vuforia.SurfaceAbstractBehaviour>::t
	SurfaceAbstractBehaviour_t51 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.SurfaceAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
