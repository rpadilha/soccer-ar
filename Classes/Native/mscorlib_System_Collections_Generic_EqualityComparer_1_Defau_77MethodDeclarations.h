﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t5339;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
 void DefaultComparer__ctor_m32141 (DefaultComparer_t5339 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m32142 (DefaultComparer_t5339 * __this, DateTimeOffset_t2251  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
 bool DefaultComparer_Equals_m32143 (DefaultComparer_t5339 * __this, DateTimeOffset_t2251  ___x, DateTimeOffset_t2251  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
