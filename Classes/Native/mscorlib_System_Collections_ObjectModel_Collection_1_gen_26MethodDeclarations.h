﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>
struct Collection_1_t3995;
// System.Object
struct Object_t;
// Vuforia.Trackable
struct Trackable_t594;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.Trackable[]
struct TrackableU5BU5D_t3973;
// System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>
struct IEnumerator_1_t846;
// System.Collections.Generic.IList`1<Vuforia.Trackable>
struct IList_1_t3994;

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m22258(__this, method) (void)Collection_1__ctor_m14594_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22259(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14595_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m22260(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14596_gshared((Collection_1_t2839 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22261(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14597_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m22262(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14598_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m22263(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14599_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m22264(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14600_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m22265(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14601_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m22266(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14602_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22267(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14603_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22268(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14604_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22269(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14605_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22270(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14606_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m22271(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14607_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m22272(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14608_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::Add(T)
#define Collection_1_Add_m22273(__this, ___item, method) (void)Collection_1_Add_m14609_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::Clear()
#define Collection_1_Clear_m22274(__this, method) (void)Collection_1_Clear_m14610_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::ClearItems()
#define Collection_1_ClearItems_m22275(__this, method) (void)Collection_1_ClearItems_m14611_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::Contains(T)
#define Collection_1_Contains_m22276(__this, ___item, method) (bool)Collection_1_Contains_m14612_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m22277(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14613_gshared((Collection_1_t2839 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::GetEnumerator()
#define Collection_1_GetEnumerator_m22278(__this, method) (Object_t*)Collection_1_GetEnumerator_m14614_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::IndexOf(T)
#define Collection_1_IndexOf_m22279(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14615_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::Insert(System.Int32,T)
#define Collection_1_Insert_m22280(__this, ___index, ___item, method) (void)Collection_1_Insert_m14616_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m22281(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14617_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::Remove(T)
#define Collection_1_Remove_m22282(__this, ___item, method) (bool)Collection_1_Remove_m14618_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m22283(__this, ___index, method) (void)Collection_1_RemoveAt_m14619_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m22284(__this, ___index, method) (void)Collection_1_RemoveItem_m14620_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::get_Count()
#define Collection_1_get_Count_m22285(__this, method) (int32_t)Collection_1_get_Count_m14621_gshared((Collection_1_t2839 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::get_Item(System.Int32)
#define Collection_1_get_Item_m22286(__this, ___index, method) (Object_t *)Collection_1_get_Item_m14622_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m22287(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14623_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m22288(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14624_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m22289(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14625_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m22290(__this/* static, unused */, ___item, method) (Object_t *)Collection_1_ConvertItem_m14626_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m22291(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14627_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m22292(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14628_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Trackable>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m22293(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14629_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
