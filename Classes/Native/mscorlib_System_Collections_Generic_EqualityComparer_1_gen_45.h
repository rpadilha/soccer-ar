﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ISmartTerrainEventHandler>
struct EqualityComparer_1_t4249;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ISmartTerrainEventHandler>
struct EqualityComparer_1_t4249  : public Object_t
{
};
struct EqualityComparer_1_t4249_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ISmartTerrainEventHandler>::_default
	EqualityComparer_1_t4249 * ____default_0;
};
