﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Boolean>
struct InternalEnumerator_1_t5024;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30441 (InternalEnumerator_1_t5024 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442 (InternalEnumerator_1_t5024 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
 void InternalEnumerator_1_Dispose_m30443 (InternalEnumerator_1_t5024 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30444 (InternalEnumerator_1_t5024 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
 bool InternalEnumerator_1_get_Current_m30445 (InternalEnumerator_1_t5024 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
