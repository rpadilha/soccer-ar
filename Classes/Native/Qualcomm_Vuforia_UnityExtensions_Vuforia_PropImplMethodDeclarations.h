﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropImpl
struct PropImpl_t719;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t625;
// UnityEngine.Mesh
struct Mesh_t174;
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"

// System.Void Vuforia.PropImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
 void PropImpl__ctor_m3226 (PropImpl_t719 * __this, int32_t ___id, Object_t * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.OrientedBoundingBox3D Vuforia.PropImpl::get_BoundingBox()
 OrientedBoundingBox3D_t635  PropImpl_get_BoundingBox_m3227 (PropImpl_t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropImpl::SetMesh(System.Int32,UnityEngine.Mesh)
 void PropImpl_SetMesh_m3228 (PropImpl_t719 * __this, int32_t ___meshRev, Mesh_t174 * ___mesh, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropImpl::SetObb(Vuforia.OrientedBoundingBox3D)
 void PropImpl_SetObb_m3229 (PropImpl_t719 * __this, OrientedBoundingBox3D_t635  ___obb3D, MethodInfo* method) IL2CPP_METHOD_ATTR;
