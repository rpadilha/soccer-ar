﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct $ArrayType$256_t2330;
struct $ArrayType$256_t2330_marshaled;

void $ArrayType$256_t2330_marshal(const $ArrayType$256_t2330& unmarshaled, $ArrayType$256_t2330_marshaled& marshaled);
void $ArrayType$256_t2330_marshal_back(const $ArrayType$256_t2330_marshaled& marshaled, $ArrayType$256_t2330& unmarshaled);
void $ArrayType$256_t2330_marshal_cleanup($ArrayType$256_t2330_marshaled& marshaled);
