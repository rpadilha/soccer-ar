﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ScorerTimeHUD
struct ScorerTimeHUD_t90;

// System.Void ScorerTimeHUD::.ctor()
 void ScorerTimeHUD__ctor_m144 (ScorerTimeHUD_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScorerTimeHUD::Start()
 void ScorerTimeHUD_Start_m145 (ScorerTimeHUD_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScorerTimeHUD::Update()
 void ScorerTimeHUD_Update_m146 (ScorerTimeHUD_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
