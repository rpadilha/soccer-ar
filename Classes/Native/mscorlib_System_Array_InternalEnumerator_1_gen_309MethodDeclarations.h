﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>
struct InternalEnumerator_1_t3830;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m20897 (InternalEnumerator_1_t3830 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20898 (InternalEnumerator_1_t3830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::Dispose()
 void InternalEnumerator_1_Dispose_m20899 (InternalEnumerator_1_t3830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m20900 (InternalEnumerator_1_t3830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::get_Current()
 EyewearCalibrationReading_t591  InternalEnumerator_1_get_Current_m20901 (InternalEnumerator_1_t3830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
