﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t2;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
 int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m2773 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
 void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m2774 (BackgroundPlaneAbstractBehaviour_t2 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
 void BackgroundPlaneAbstractBehaviour_SetEditorValues_m2775 (BackgroundPlaneAbstractBehaviour_t2 * __this, int32_t ___numDivisions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
 bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m2776 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
 void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2777 (BackgroundPlaneAbstractBehaviour_t2 * __this, float ___depth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
 void BackgroundPlaneAbstractBehaviour_Start_m2778 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
 void BackgroundPlaneAbstractBehaviour_Update_m2779 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
 Quaternion_t108  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m2780 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
 void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m2781 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
 void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2782 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
 bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2783 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
 void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m201 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
 void BackgroundPlaneAbstractBehaviour__ctor_m196 (BackgroundPlaneAbstractBehaviour_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
 void BackgroundPlaneAbstractBehaviour__cctor_m2784 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
