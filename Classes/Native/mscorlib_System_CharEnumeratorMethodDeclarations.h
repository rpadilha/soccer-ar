﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.CharEnumerator
struct CharEnumerator_t2247;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.CharEnumerator::.ctor(System.String)
 void CharEnumerator__ctor_m12735 (CharEnumerator_t2247 * __this, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.CharEnumerator::System.Collections.IEnumerator.get_Current()
 Object_t * CharEnumerator_System_Collections_IEnumerator_get_Current_m12736 (CharEnumerator_t2247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CharEnumerator::System.IDisposable.Dispose()
 void CharEnumerator_System_IDisposable_Dispose_m12737 (CharEnumerator_t2247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.CharEnumerator::get_Current()
 uint16_t CharEnumerator_get_Current_m12738 (CharEnumerator_t2247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CharEnumerator::MoveNext()
 bool CharEnumerator_MoveNext_m12739 (CharEnumerator_t2247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
