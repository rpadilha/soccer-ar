﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo RaycastHit_t228_il2cpp_TypeInfo;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t309  : public MulticastDelegate_t373
{
};
