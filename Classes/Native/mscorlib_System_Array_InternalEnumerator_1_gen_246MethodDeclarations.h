﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
struct InternalEnumerator_1_t3608;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m19539 (InternalEnumerator_1_t3608 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19540 (InternalEnumerator_1_t3608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
 void InternalEnumerator_1_Dispose_m19541 (InternalEnumerator_1_t3608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m19542 (InternalEnumerator_1_t3608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m19543 (InternalEnumerator_1_t3608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
