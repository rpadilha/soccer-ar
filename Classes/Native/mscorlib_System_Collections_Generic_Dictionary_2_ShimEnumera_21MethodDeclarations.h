﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct ShimEnumerator_t4379;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t911;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m26033 (ShimEnumerator_t4379 * __this, Dictionary_2_t911 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
 bool ShimEnumerator_MoveNext_m26034 (ShimEnumerator_t4379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m26035 (ShimEnumerator_t4379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Key()
 Object_t * ShimEnumerator_get_Key_m26036 (ShimEnumerator_t4379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Value()
 Object_t * ShimEnumerator_get_Value_m26037 (ShimEnumerator_t4379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
 Object_t * ShimEnumerator_get_Current_m26038 (ShimEnumerator_t4379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
