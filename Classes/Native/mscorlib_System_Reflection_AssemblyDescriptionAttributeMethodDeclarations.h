﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_t574;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
 void AssemblyDescriptionAttribute__ctor_m2748 (AssemblyDescriptionAttribute_t574 * __this, String_t* ___description, MethodInfo* method) IL2CPP_METHOD_ATTR;
