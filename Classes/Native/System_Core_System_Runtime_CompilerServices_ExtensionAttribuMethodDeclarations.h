﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t822;

// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
 void ExtensionAttribute__ctor_m4522 (ExtensionAttribute_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
