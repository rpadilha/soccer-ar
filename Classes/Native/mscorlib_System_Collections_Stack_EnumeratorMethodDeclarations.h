﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Stack/Enumerator
struct Enumerator_t1893;
// System.Object
struct Object_t;
// System.Collections.Stack
struct Stack_t1150;

// System.Void System.Collections.Stack/Enumerator::.ctor(System.Collections.Stack)
 void Enumerator__ctor_m10616 (Enumerator_t1893 * __this, Stack_t1150 * ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Stack/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m10617 (Enumerator_t1893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Stack/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m10618 (Enumerator_t1893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
