﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Corner_Script
struct Corner_Script_t76;
// UnityEngine.Collider
struct Collider_t70;

// System.Void Corner_Script::.ctor()
 void Corner_Script__ctor_m117 (Corner_Script_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Corner_Script::Start()
 void Corner_Script_Start_m118 (Corner_Script_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Corner_Script::Update()
 void Corner_Script_Update_m119 (Corner_Script_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Corner_Script::OnTriggerEnter(UnityEngine.Collider)
 void Corner_Script_OnTriggerEnter_m120 (Corner_Script_t76 * __this, Collider_t70 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
