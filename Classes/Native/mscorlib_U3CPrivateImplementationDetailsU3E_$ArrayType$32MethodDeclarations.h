﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct $ArrayType$32_t2321;
struct $ArrayType$32_t2321_marshaled;

void $ArrayType$32_t2321_marshal(const $ArrayType$32_t2321& unmarshaled, $ArrayType$32_t2321_marshaled& marshaled);
void $ArrayType$32_t2321_marshal_back(const $ArrayType$32_t2321_marshaled& marshaled, $ArrayType$32_t2321& unmarshaled);
void $ArrayType$32_t2321_marshal_cleanup($ArrayType$32_t2321_marshaled& marshaled);
