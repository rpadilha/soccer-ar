﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRendererImpl
struct QCARRendererImpl_t707;
// UnityEngine.Texture2D
struct Texture2D_t196;
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"

// UnityEngine.Texture2D Vuforia.QCARRendererImpl::get_VideoBackgroundTexture()
 Texture2D_t196 * QCARRendererImpl_get_VideoBackgroundTexture_m3179 (QCARRendererImpl_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRendererImpl::GetVideoBackgroundConfig()
 VideoBGCfgData_t705  QCARRendererImpl_GetVideoBackgroundConfig_m3180 (QCARRendererImpl_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::ClearVideoBackgroundConfig()
 void QCARRendererImpl_ClearVideoBackgroundConfig_m3181 (QCARRendererImpl_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
 void QCARRendererImpl_SetVideoBackgroundConfig_m3182 (QCARRendererImpl_t707 * __this, VideoBGCfgData_t705  ___config, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRendererImpl::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
 bool QCARRendererImpl_SetVideoBackgroundTexture_m3183 (QCARRendererImpl_t707 * __this, Texture2D_t196 * ___texture, int32_t ___nativeTextureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRendererImpl::IsVideoBackgroundInfoAvailable()
 bool QCARRendererImpl_IsVideoBackgroundInfoAvailable_m3184 (QCARRendererImpl_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRendererImpl::GetVideoTextureInfo()
 VideoTextureInfo_t588  QCARRendererImpl_GetVideoTextureInfo_m3185 (QCARRendererImpl_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::Pause(System.Boolean)
 void QCARRendererImpl_Pause_m3186 (QCARRendererImpl_t707 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::UnityRenderEvent(Vuforia.QCARRendererImpl/RenderEvent)
 void QCARRendererImpl_UnityRenderEvent_m3187 (QCARRendererImpl_t707 * __this, int32_t ___renderEvent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::.ctor()
 void QCARRendererImpl__ctor_m3188 (QCARRendererImpl_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
