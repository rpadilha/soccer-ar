﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1958;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1763;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
 MarshalAsAttribute_t1763 * UnmanagedMarshal_ToMarshalAsAttribute_m11303 (UnmanagedMarshal_t1958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
