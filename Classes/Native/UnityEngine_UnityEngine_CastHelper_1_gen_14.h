﻿#pragma once
#include <stdint.h>
// Sphere
struct Sphere_t71;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Sphere>
struct CastHelper_1_t3089 
{
	// T UnityEngine.CastHelper`1<Sphere>::t
	Sphere_t71 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Sphere>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
