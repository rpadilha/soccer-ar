﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>
struct InternalEnumerator_1_t5243;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31587 (InternalEnumerator_1_t5243 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588 (InternalEnumerator_1_t5243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::Dispose()
 void InternalEnumerator_1_Dispose_m31589 (InternalEnumerator_1_t5243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31590 (InternalEnumerator_1_t5243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31591 (InternalEnumerator_1_t5243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
