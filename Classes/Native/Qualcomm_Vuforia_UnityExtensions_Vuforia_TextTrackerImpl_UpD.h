﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
// Vuforia.TextTrackerImpl/UpDirection
struct UpDirection_t724 
{
	// System.Int32 Vuforia.TextTrackerImpl/UpDirection::value__
	int32_t ___value___1;
};
