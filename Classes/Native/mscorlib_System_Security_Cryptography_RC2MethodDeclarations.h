﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RC2
struct RC2_t1730;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.RC2::.ctor()
 void RC2__ctor_m12134 (RC2_t1730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RC2 System.Security.Cryptography.RC2::Create()
 RC2_t1730 * RC2_Create_m9055 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RC2 System.Security.Cryptography.RC2::Create(System.String)
 RC2_t1730 * RC2_Create_m12135 (Object_t * __this/* static, unused */, String_t* ___AlgName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RC2::get_EffectiveKeySize()
 int32_t RC2_get_EffectiveKeySize_m12136 (RC2_t1730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RC2::get_KeySize()
 int32_t RC2_get_KeySize_m12137 (RC2_t1730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2::set_KeySize(System.Int32)
 void RC2_set_KeySize_m12138 (RC2_t1730 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
