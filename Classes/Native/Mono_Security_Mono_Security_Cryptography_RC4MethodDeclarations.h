﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.RC4
struct RC4_t1607;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void Mono.Security.Cryptography.RC4::.ctor()
 void RC4__ctor_m8298 (RC4_t1607 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.RC4::.cctor()
 void RC4__cctor_m8299 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.RC4::get_IV()
 ByteU5BU5D_t653* RC4_get_IV_m8300 (RC4_t1607 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.RC4::set_IV(System.Byte[])
 void RC4_set_IV_m8301 (RC4_t1607 * __this, ByteU5BU5D_t653* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
