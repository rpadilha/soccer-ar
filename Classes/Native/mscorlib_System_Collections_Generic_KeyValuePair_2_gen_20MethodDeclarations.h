﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct KeyValuePair_2_t4368;
// System.String
struct String_t;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m25951 (KeyValuePair_2_t4368 * __this, int32_t ___key, VirtualButtonData_t685  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Key()
 int32_t KeyValuePair_2_get_Key_m25952 (KeyValuePair_2_t4368 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25953 (KeyValuePair_2_t4368 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Value()
 VirtualButtonData_t685  KeyValuePair_2_get_Value_m25954 (KeyValuePair_2_t4368 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25955 (KeyValuePair_2_t4368 * __this, VirtualButtonData_t685  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ToString()
 String_t* KeyValuePair_2_ToString_m25956 (KeyValuePair_2_t4368 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
