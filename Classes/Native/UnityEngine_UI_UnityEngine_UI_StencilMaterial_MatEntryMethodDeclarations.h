﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t410;

// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
 void MatEntry__ctor_m1732 (MatEntry_t410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
