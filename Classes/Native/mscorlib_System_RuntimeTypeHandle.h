﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t1754 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t121 ___value_0;
};
