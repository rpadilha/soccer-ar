﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>
struct Enumerator_t896;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t15;
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t766;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"
#define Enumerator__ctor_m25430(__this, ___l, method) (void)Enumerator__ctor_m14558_gshared((Enumerator_t2837 *)__this, (List_1_t149 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25431(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m25432(__this, method) (void)Enumerator_Dispose_m14560_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::VerifyState()
#define Enumerator_VerifyState_m25433(__this, method) (void)Enumerator_VerifyState_m14561_gshared((Enumerator_t2837 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m5341(__this, method) (bool)Enumerator_MoveNext_m14562_gshared((Enumerator_t2837 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m5339(__this, method) (Object_t *)Enumerator_get_Current_m14563_gshared((Enumerator_t2837 *)__this, method)
