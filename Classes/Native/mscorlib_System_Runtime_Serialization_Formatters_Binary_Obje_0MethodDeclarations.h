﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
struct ArrayNullFiller_t2117;

// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller::.ctor(System.Int32)
 void ArrayNullFiller__ctor_m11902 (ArrayNullFiller_t2117 * __this, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
