﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t4044;
// Vuforia.DataSet
struct DataSet_t612;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>
struct IEnumerator_1_t843;

// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::.ctor()
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIteratorU3Ec_0MethodDeclarations.h"
#define U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22748(__this, method) (void)U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22741_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22749(__this, method) (DataSet_t612 *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22742_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::System.Collections.IEnumerator.get_Current()
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22750(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22743_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::System.Collections.IEnumerable.GetEnumerator()
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22751(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22744_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22752(__this, method) (Object_t*)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22745_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::MoveNext()
#define U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22753(__this, method) (bool)U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22746_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::Dispose()
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22754(__this, method) (void)U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22747_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
