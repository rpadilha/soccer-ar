﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t152;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.EventInfo/AddEventAdapter
struct AddEventAdapter_t1979  : public MulticastDelegate_t373
{
};
