﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t57;

// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
 void VideoBackgroundBehaviour__ctor_m92 (VideoBackgroundBehaviour_t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
