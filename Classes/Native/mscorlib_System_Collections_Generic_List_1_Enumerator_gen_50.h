﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t781;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
struct Enumerator_t4399 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::l
	List_1_t781 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::current
	TargetSearchResult_t776  ___current_3;
};
