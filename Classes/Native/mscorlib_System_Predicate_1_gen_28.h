﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t594;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Trackable>
struct Predicate_1_t3992  : public MulticastDelegate_t373
{
};
