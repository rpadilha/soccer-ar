﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1556;
// System.Type
struct Type_t;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1213;
// System.Object
#include "mscorlib_System_Object.h"
// System.MonoCustomAttrs
struct MonoCustomAttrs_t2284  : public Object_t
{
};
struct MonoCustomAttrs_t2284_StaticFields{
	// System.Reflection.Assembly System.MonoCustomAttrs::corlib
	Assembly_t1556 * ___corlib_0;
	// System.Type System.MonoCustomAttrs::AttributeUsageType
	Type_t * ___AttributeUsageType_1;
	// System.AttributeUsageAttribute System.MonoCustomAttrs::DefaultAttributeUsage
	AttributeUsageAttribute_t1213 * ___DefaultAttributeUsage_2;
};
