﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>
struct InternalEnumerator_1_t4946;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29889 (InternalEnumerator_1_t4946 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890 (InternalEnumerator_1_t4946 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::Dispose()
 void InternalEnumerator_1_Dispose_m29891 (InternalEnumerator_1_t4946 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29892 (InternalEnumerator_1_t4946 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29893 (InternalEnumerator_1_t4946 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
