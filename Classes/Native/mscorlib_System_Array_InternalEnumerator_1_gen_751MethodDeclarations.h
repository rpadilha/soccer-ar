﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>
struct InternalEnumerator_1_t5297;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatus.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31857 (InternalEnumerator_1_t5297 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31858 (InternalEnumerator_1_t5297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::Dispose()
 void InternalEnumerator_1_Dispose_m31859 (InternalEnumerator_1_t5297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31860 (InternalEnumerator_1_t5297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m31861 (InternalEnumerator_1_t5297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
