﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>
struct ShimEnumerator_t4294  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::host_enumerator
	Enumerator_t889  ___host_enumerator_0;
};
