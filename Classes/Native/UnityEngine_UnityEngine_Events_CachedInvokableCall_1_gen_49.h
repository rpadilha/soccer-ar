﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<ShieldMenu>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_45.h"
// UnityEngine.Events.CachedInvokableCall`1<ShieldMenu>
struct CachedInvokableCall_1_t3055  : public InvokableCall_1_t3056
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ShieldMenu>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
