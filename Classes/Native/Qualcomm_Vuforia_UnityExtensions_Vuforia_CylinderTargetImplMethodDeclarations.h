﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetImpl
struct CylinderTargetImpl_t641;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t612;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.CylinderTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
 void CylinderTargetImpl__ctor_m2994 (CylinderTargetImpl_t641 * __this, String_t* ___name, int32_t ___id, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetImpl::SetSize(UnityEngine.Vector3)
 void CylinderTargetImpl_SetSize_m2995 (CylinderTargetImpl_t641 * __this, Vector3_t73  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetImpl::GetSideLength()
 float CylinderTargetImpl_GetSideLength_m2996 (CylinderTargetImpl_t641 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetImpl::GetTopDiameter()
 float CylinderTargetImpl_GetTopDiameter_m2997 (CylinderTargetImpl_t641 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetImpl::GetBottomDiameter()
 float CylinderTargetImpl_GetBottomDiameter_m2998 (CylinderTargetImpl_t641 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetImpl::SetSideLength(System.Single)
 bool CylinderTargetImpl_SetSideLength_m2999 (CylinderTargetImpl_t641 * __this, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetImpl::SetTopDiameter(System.Single)
 bool CylinderTargetImpl_SetTopDiameter_m3000 (CylinderTargetImpl_t641 * __this, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetImpl::SetBottomDiameter(System.Single)
 bool CylinderTargetImpl_SetBottomDiameter_m3001 (CylinderTargetImpl_t641 * __this, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetImpl::ScaleCylinder(System.Single)
 void CylinderTargetImpl_ScaleCylinder_m3002 (CylinderTargetImpl_t641 * __this, float ___scale, MethodInfo* method) IL2CPP_METHOD_ATTR;
