﻿#pragma once
#include <stdint.h>
// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t1678;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1569;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct CertificateValidationCallback2_t1680  : public MulticastDelegate_t373
{
};
