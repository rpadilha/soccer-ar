﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<InGameState_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_46.h"
// UnityEngine.Events.CachedInvokableCall`1<InGameState_Script>
struct CachedInvokableCall_1_t3058  : public InvokableCall_1_t3059
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<InGameState_Script>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
