﻿#pragma once
#include <stdint.h>
// Vuforia.Word[]
struct WordU5BU5D_t4159;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Word>
struct List_1_t740  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Word>::_items
	WordU5BU5D_t4159* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::_version
	int32_t ____version_3;
};
struct List_1_t740_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.Word>::EmptyArray
	WordU5BU5D_t4159* ___EmptyArray_4;
};
