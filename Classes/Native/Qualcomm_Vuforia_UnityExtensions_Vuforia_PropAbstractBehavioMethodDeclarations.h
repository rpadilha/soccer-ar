﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t43;
// Vuforia.Prop
struct Prop_t15;
// UnityEngine.MeshFilter
struct MeshFilter_t84;
// UnityEngine.MeshCollider
struct MeshCollider_t626;
// UnityEngine.BoxCollider
struct BoxCollider_t197;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;

// Vuforia.Prop Vuforia.PropAbstractBehaviour::get_Prop()
 Object_t * PropAbstractBehaviour_get_Prop_m4148 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::UpdateMeshAndColliders()
 void PropAbstractBehaviour_UpdateMeshAndColliders_m404 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Start()
 void PropAbstractBehaviour_Start_m405 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::InternalUnregisterTrackable()
 void PropAbstractBehaviour_InternalUnregisterTrackable_m403 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.InitializeProp(Vuforia.Prop)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m406 (PropAbstractBehaviour_t43 * __this, Object_t * ___prop, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m407 (PropAbstractBehaviour_t43 * __this, MeshFilter_t84 * ___meshFilterToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshFilterToUpdate()
 MeshFilter_t84 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m408 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m409 (PropAbstractBehaviour_t43 * __this, MeshCollider_t626 * ___meshColliderToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshColliderToUpdate()
 MeshCollider_t626 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m410 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetBoxColliderToUpdate(UnityEngine.BoxCollider)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m411 (PropAbstractBehaviour_t43 * __this, BoxCollider_t197 * ___boxColliderToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_BoxColliderToUpdate()
 BoxCollider_t197 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m412 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::.ctor()
 void PropAbstractBehaviour__ctor_m398 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m399 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m400 (PropAbstractBehaviour_t43 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m401 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m402 (PropAbstractBehaviour_t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
