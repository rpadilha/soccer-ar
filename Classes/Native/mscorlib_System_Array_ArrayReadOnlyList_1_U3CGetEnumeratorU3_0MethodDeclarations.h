﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t5144;
// System.Object
struct Object_t;

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
 void U3CGetEnumeratorU3Ec__Iterator0__ctor_m31104_gshared (U3CGetEnumeratorU3Ec__Iterator0_t5144 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m31104(__this, method) (void)U3CGetEnumeratorU3Ec__Iterator0__ctor_m31104_gshared((U3CGetEnumeratorU3Ec__Iterator0_t5144 *)__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
 Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m31105_gshared (U3CGetEnumeratorU3Ec__Iterator0_t5144 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m31105(__this, method) (Object_t *)U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m31105_gshared((U3CGetEnumeratorU3Ec__Iterator0_t5144 *)__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m31106_gshared (U3CGetEnumeratorU3Ec__Iterator0_t5144 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m31106(__this, method) (Object_t *)U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m31106_gshared((U3CGetEnumeratorU3Ec__Iterator0_t5144 *)__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
 bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m31107_gshared (U3CGetEnumeratorU3Ec__Iterator0_t5144 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m31107(__this, method) (bool)U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m31107_gshared((U3CGetEnumeratorU3Ec__Iterator0_t5144 *)__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
 void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m31108_gshared (U3CGetEnumeratorU3Ec__Iterator0_t5144 * __this, MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m31108(__this, method) (void)U3CGetEnumeratorU3Ec__Iterator0_Dispose_m31108_gshared((U3CGetEnumeratorU3Ec__Iterator0_t5144 *)__this, method)
