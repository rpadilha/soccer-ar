﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
// Vuforia.WordTemplateMode
struct WordTemplateMode_t651 
{
	// System.Int32 Vuforia.WordTemplateMode::value__
	int32_t ___value___1;
};
