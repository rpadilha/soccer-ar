﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t338;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.RectTransform>
struct CastHelper_1_t3504 
{
	// T UnityEngine.CastHelper`1<UnityEngine.RectTransform>::t
	RectTransform_t338 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.RectTransform>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
