﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$2048
struct $ArrayType$2048_t2329;
struct $ArrayType$2048_t2329_marshaled;

void $ArrayType$2048_t2329_marshal(const $ArrayType$2048_t2329& unmarshaled, $ArrayType$2048_t2329_marshaled& marshaled);
void $ArrayType$2048_t2329_marshal_back(const $ArrayType$2048_t2329_marshaled& marshaled, $ArrayType$2048_t2329& unmarshaled);
void $ArrayType$2048_t2329_marshal_cleanup($ArrayType$2048_t2329_marshaled& marshaled);
