﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collision
struct Collision_t82;
// UnityEngine.Rigidbody
struct Rigidbody_t180;
// UnityEngine.Collider
struct Collider_t70;

// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
 Rigidbody_t180 * Collision_get_rigidbody_m716 (Collision_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.Collision::get_collider()
 Collider_t70 * Collision_get_collider_m624 (Collision_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
