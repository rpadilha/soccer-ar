﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
struct InternalEnumerator_1_t5117;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30959 (InternalEnumerator_1_t5117 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960 (InternalEnumerator_1_t5117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::Dispose()
 void InternalEnumerator_1_Dispose_m30961 (InternalEnumerator_1_t5117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30962 (InternalEnumerator_1_t5117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30963 (InternalEnumerator_1_t5117 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
