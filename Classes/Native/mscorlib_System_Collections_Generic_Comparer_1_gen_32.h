﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct Comparer_1_t4108;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct Comparer_1_t4108  : public Object_t
{
};
struct Comparer_1_t4108_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>::_default
	Comparer_1_t4108 * ____default_0;
};
