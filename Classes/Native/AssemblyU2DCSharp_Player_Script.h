﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Sphere
struct Sphere_t71;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t95;
// UnityEngine.Transform
struct Transform_t74;
// InGameState_Script
struct InGameState_Script_t83;
// UnityEngine.Texture
struct Texture_t107;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Player_Script/TypePlayer
#include "AssemblyU2DCSharp_Player_Script_TypePlayer.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Player_Script/Player_State
#include "AssemblyU2DCSharp_Player_Script_Player_State.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Player_Script
struct Player_Script_t94  : public MonoBehaviour_t10
{
	// System.String Player_Script::Name
	String_t* ___Name_6;
	// Player_Script/TypePlayer Player_Script::type
	int32_t ___type_7;
	// System.Single Player_Script::Speed
	float ___Speed_8;
	// System.Single Player_Script::Strong
	float ___Strong_9;
	// System.Single Player_Script::Control
	float ___Control_10;
	// UnityEngine.Vector3 Player_Script::actualVelocityPlayer
	Vector3_t73  ___actualVelocityPlayer_11;
	// UnityEngine.Vector3 Player_Script::oldVelocityPlayer
	Vector3_t73  ___oldVelocityPlayer_12;
	// Sphere Player_Script::sphere
	Sphere_t71 * ___sphere_13;
	// UnityEngine.GameObject[] Player_Script::players
	GameObjectU5BU5D_t95* ___players_14;
	// UnityEngine.GameObject[] Player_Script::oponents
	GameObjectU5BU5D_t95* ___oponents_15;
	// UnityEngine.Vector3 Player_Script::resetPosition
	Vector3_t73  ___resetPosition_16;
	// UnityEngine.Vector3 Player_Script::initialPosition
	Vector3_t73  ___initialPosition_17;
	// System.Single Player_Script::inputSteer
	float ___inputSteer_18;
	// UnityEngine.Transform Player_Script::goalPosition
	Transform_t74 * ___goalPosition_19;
	// UnityEngine.Transform Player_Script::headTransform
	Transform_t74 * ___headTransform_20;
	// System.Boolean Player_Script::temporallyUnselectable
	bool ___temporallyUnselectable_21;
	// System.Single Player_Script::timeToBeSelectable
	float ___timeToBeSelectable_22;
	// System.Single Player_Script::maxDistanceFromPosition
	float ___maxDistanceFromPosition_23;
	// Player_Script/Player_State Player_Script::state
	int32_t ___state_24;
	// System.Single Player_Script::timeToRemove
	float ___timeToRemove_25;
	// System.Single Player_Script::timeToPass
	float ___timeToPass_26;
	// UnityEngine.Transform Player_Script::hand_bone
	Transform_t74 * ___hand_bone_27;
	// InGameState_Script Player_Script::inGame
	InGameState_Script_t83 * ___inGame_28;
	// UnityEngine.Texture Player_Script::barTexture
	Texture_t107 * ___barTexture_29;
	// UnityEngine.Texture Player_Script::barStaminaTexture
	Texture_t107 * ___barStaminaTexture_30;
	// System.Int32 Player_Script::barPosition
	int32_t ___barPosition_31;
	// UnityEngine.Quaternion Player_Script::initialRotation
	Quaternion_t108  ___initialRotation_32;
	// System.Single Player_Script::stamina
	float ___stamina_33;
};
