﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"
// Vuforia.QCARUnity/InitError
struct InitError_t124 
{
	// System.Int32 Vuforia.QCARUnity/InitError::value__
	int32_t ___value___1;
};
