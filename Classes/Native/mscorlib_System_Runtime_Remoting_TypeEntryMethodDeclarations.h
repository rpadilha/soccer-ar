﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.TypeEntry
struct TypeEntry_t2091;
// System.String
struct String_t;

// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
 String_t* TypeEntry_get_AssemblyName_m11880 (TypeEntry_t2091 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
 String_t* TypeEntry_get_TypeName_m11881 (TypeEntry_t2091 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
