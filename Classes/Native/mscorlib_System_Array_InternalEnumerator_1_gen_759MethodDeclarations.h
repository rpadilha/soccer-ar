﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>
struct InternalEnumerator_1_t5305;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31897 (InternalEnumerator_1_t5305 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31898 (InternalEnumerator_1_t5305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>::Dispose()
 void InternalEnumerator_1_Dispose_m31899 (InternalEnumerator_1_t5305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31900 (InternalEnumerator_1_t5305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.CspProviderFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31901 (InternalEnumerator_1_t5305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
