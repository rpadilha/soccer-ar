﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t5044;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t5043;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
 void LinkedList_1__ctor_m30524_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1__ctor_m30524(__this, method) (void)LinkedList_1__ctor_m30524_gshared((LinkedList_1_t5044 *)__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void LinkedList_1__ctor_m30525_gshared (LinkedList_1_t5044 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method);
#define LinkedList_1__ctor_m30525(__this, ___info, ___context, method) (void)LinkedList_1__ctor_m30525_gshared((LinkedList_1_t5044 *)__this, (SerializationInfo_t1118 *)___info, (StreamingContext_t1119 )___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
 void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526(__this, ___value, method) (void)LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_gshared((LinkedList_1_t5044 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void LinkedList_1_System_Collections_ICollection_CopyTo_m30527_gshared (LinkedList_1_t5044 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m30527(__this, ___array, ___index, method) (void)LinkedList_1_System_Collections_ICollection_CopyTo_m30527_gshared((LinkedList_1_t5044 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528(__this, method) (Object_t*)LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_gshared((LinkedList_1_t5044 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529(__this, method) (Object_t *)LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_gshared((LinkedList_1_t5044 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530(__this, method) (bool)LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_gshared((LinkedList_1_t5044 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531(__this, method) (bool)LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_gshared((LinkedList_1_t5044 *)__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532(__this, method) (Object_t *)LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_gshared((LinkedList_1_t5044 *)__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_VerifyReferencedNode_m30533_gshared (LinkedList_1_t5044 * __this, LinkedListNode_1_t5043 * ___node, MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m30533(__this, ___node, method) (void)LinkedList_1_VerifyReferencedNode_m30533_gshared((LinkedList_1_t5044 *)__this, (LinkedListNode_1_t5043 *)___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
 LinkedListNode_1_t5043 * LinkedList_1_AddLast_m30534_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_AddLast_m30534(__this, ___value, method) (LinkedListNode_1_t5043 *)LinkedList_1_AddLast_m30534_gshared((LinkedList_1_t5044 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
 void LinkedList_1_Clear_m30535_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_Clear_m30535(__this, method) (void)LinkedList_1_Clear_m30535_gshared((LinkedList_1_t5044 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
 bool LinkedList_1_Contains_m30536_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_Contains_m30536(__this, ___value, method) (bool)LinkedList_1_Contains_m30536_gshared((LinkedList_1_t5044 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
 void LinkedList_1_CopyTo_m30537_gshared (LinkedList_1_t5044 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method);
#define LinkedList_1_CopyTo_m30537(__this, ___array, ___index, method) (void)LinkedList_1_CopyTo_m30537_gshared((LinkedList_1_t5044 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
 LinkedListNode_1_t5043 * LinkedList_1_Find_m30538_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_Find_m30538(__this, ___value, method) (LinkedListNode_1_t5043 *)LinkedList_1_Find_m30538_gshared((LinkedList_1_t5044 *)__this, (Object_t *)___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
 Enumerator_t5045  LinkedList_1_GetEnumerator_m30539 (LinkedList_1_t5044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void LinkedList_1_GetObjectData_m30540_gshared (LinkedList_1_t5044 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method);
#define LinkedList_1_GetObjectData_m30540(__this, ___info, ___context, method) (void)LinkedList_1_GetObjectData_m30540_gshared((LinkedList_1_t5044 *)__this, (SerializationInfo_t1118 *)___info, (StreamingContext_t1119 )___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
 void LinkedList_1_OnDeserialization_m30541_gshared (LinkedList_1_t5044 * __this, Object_t * ___sender, MethodInfo* method);
#define LinkedList_1_OnDeserialization_m30541(__this, ___sender, method) (void)LinkedList_1_OnDeserialization_m30541_gshared((LinkedList_1_t5044 *)__this, (Object_t *)___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
 bool LinkedList_1_Remove_m30542_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_Remove_m30542(__this, ___value, method) (bool)LinkedList_1_Remove_m30542_gshared((LinkedList_1_t5044 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_Remove_m30543_gshared (LinkedList_1_t5044 * __this, LinkedListNode_1_t5043 * ___node, MethodInfo* method);
#define LinkedList_1_Remove_m30543(__this, ___node, method) (void)LinkedList_1_Remove_m30543_gshared((LinkedList_1_t5044 *)__this, (LinkedListNode_1_t5043 *)___node, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
 int32_t LinkedList_1_get_Count_m30544_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_get_Count_m30544(__this, method) (int32_t)LinkedList_1_get_Count_m30544_gshared((LinkedList_1_t5044 *)__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
 LinkedListNode_1_t5043 * LinkedList_1_get_First_m30545_gshared (LinkedList_1_t5044 * __this, MethodInfo* method);
#define LinkedList_1_get_First_m30545(__this, method) (LinkedListNode_1_t5043 *)LinkedList_1_get_First_m30545_gshared((LinkedList_1_t5044 *)__this, method)
