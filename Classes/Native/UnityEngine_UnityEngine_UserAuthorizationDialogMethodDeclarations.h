﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UserAuthorizationDialog
struct UserAuthorizationDialog_t1145;

// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
 void UserAuthorizationDialog__ctor_m6564 (UserAuthorizationDialog_t1145 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UserAuthorizationDialog::Start()
 void UserAuthorizationDialog_Start_m6565 (UserAuthorizationDialog_t1145 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
 void UserAuthorizationDialog_OnGUI_m6566 (UserAuthorizationDialog_t1145 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
 void UserAuthorizationDialog_DoUserAuthorizationDialog_m6567 (UserAuthorizationDialog_t1145 * __this, int32_t ___windowID, MethodInfo* method) IL2CPP_METHOD_ATTR;
