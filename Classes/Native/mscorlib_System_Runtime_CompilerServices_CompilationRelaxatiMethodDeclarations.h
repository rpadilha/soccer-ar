﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t949;
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0.h"

// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
 void CompilationRelaxationsAttribute__ctor_m5569 (CompilationRelaxationsAttribute_t949 * __this, int32_t ___relaxations, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Runtime.CompilerServices.CompilationRelaxations)
 void CompilationRelaxationsAttribute__ctor_m6890 (CompilationRelaxationsAttribute_t949 * __this, int32_t ___relaxations, MethodInfo* method) IL2CPP_METHOD_ATTR;
