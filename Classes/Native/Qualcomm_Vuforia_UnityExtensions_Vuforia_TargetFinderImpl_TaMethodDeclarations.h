﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinderImpl/TargetFinderState
struct TargetFinderState_t779;
struct TargetFinderState_t779_marshaled;

void TargetFinderState_t779_marshal(const TargetFinderState_t779& unmarshaled, TargetFinderState_t779_marshaled& marshaled);
void TargetFinderState_t779_marshal_back(const TargetFinderState_t779_marshaled& marshaled, TargetFinderState_t779& unmarshaled);
void TargetFinderState_t779_marshal_cleanup(TargetFinderState_t779_marshaled& marshaled);
