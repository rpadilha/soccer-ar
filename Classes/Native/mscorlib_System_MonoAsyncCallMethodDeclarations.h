﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoAsyncCall
struct MonoAsyncCall_t2282;

// System.Void System.MonoAsyncCall::.ctor()
 void MonoAsyncCall__ctor_m13159 (MonoAsyncCall_t2282 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
