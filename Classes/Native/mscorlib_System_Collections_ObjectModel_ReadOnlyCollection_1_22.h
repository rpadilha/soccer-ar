﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ICloudRecoEventHandler>
struct IList_1_t3870;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ICloudRecoEventHandler>
struct ReadOnlyCollection_1_t3866  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ICloudRecoEventHandler>::list
	Object_t* ___list_0;
};
