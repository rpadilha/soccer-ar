﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_t980;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t1094;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t968;

// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
 UserProfile_t1094 * GcUserProfileData_ToUserProfile_m6420 (GcUserProfileData_t980 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
 void GcUserProfileData_AddToArray_m6421 (GcUserProfileData_t980 * __this, UserProfileU5BU5D_t968** ___array, int32_t ___number, MethodInfo* method) IL2CPP_METHOD_ATTR;
