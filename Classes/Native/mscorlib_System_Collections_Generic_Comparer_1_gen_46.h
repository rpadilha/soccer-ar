﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ITextRecoEventHandler>
struct Comparer_1_t4534;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ITextRecoEventHandler>
struct Comparer_1_t4534  : public Object_t
{
};
struct Comparer_1_t4534_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ITextRecoEventHandler>::_default
	Comparer_1_t4534 * ____default_0;
};
