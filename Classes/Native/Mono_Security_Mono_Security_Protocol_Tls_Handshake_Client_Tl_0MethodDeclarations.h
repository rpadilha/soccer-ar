﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
struct TlsClientCertificateVerify_t1690;
// Mono.Security.Protocol.Tls.Context
struct Context_t1642;
// System.Security.Cryptography.RSA
struct RSA_t1409;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::.ctor(Mono.Security.Protocol.Tls.Context)
 void TlsClientCertificateVerify__ctor_m8885 (TlsClientCertificateVerify_t1690 * __this, Context_t1642 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::Update()
 void TlsClientCertificateVerify_Update_m8886 (TlsClientCertificateVerify_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::ProcessAsSsl3()
 void TlsClientCertificateVerify_ProcessAsSsl3_m8887 (TlsClientCertificateVerify_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::ProcessAsTls1()
 void TlsClientCertificateVerify_ProcessAsTls1_m8888 (TlsClientCertificateVerify_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::getClientCertRSA(System.Security.Cryptography.RSA)
 RSA_t1409 * TlsClientCertificateVerify_getClientCertRSA_m8889 (TlsClientCertificateVerify_t1690 * __this, RSA_t1409 * ___privKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::getUnsignedBigInteger(System.Byte[])
 ByteU5BU5D_t653* TlsClientCertificateVerify_getUnsignedBigInteger_m8890 (TlsClientCertificateVerify_t1690 * __this, ByteU5BU5D_t653* ___integer, MethodInfo* method) IL2CPP_METHOD_ATTR;
