﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Enumerator_t3454;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t332;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t506;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t334;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m18626(__this, ___dictionary, method) (void)Enumerator__ctor_m18373_gshared((Enumerator_t3460 *)__this, (Dictionary_2_t3452 *)___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18627(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m18374_gshared((Enumerator_t3460 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18628(__this, method) (DictionaryEntry_t1355 )Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18375_gshared((Enumerator_t3460 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18629(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18376_gshared((Enumerator_t3460 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18630(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18377_gshared((Enumerator_t3460 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m18631(__this, method) (bool)Enumerator_MoveNext_m18378_gshared((Enumerator_t3460 *)__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
 KeyValuePair_2_t3450  Enumerator_get_Current_m18632 (Enumerator_t3454 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18633(__this, method) (Font_t332 *)Enumerator_get_CurrentKey_m18380_gshared((Enumerator_t3460 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18634(__this, method) (List_1_t506 *)Enumerator_get_CurrentValue_m18381_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyState()
#define Enumerator_VerifyState_m18635(__this, method) (void)Enumerator_VerifyState_m18382_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18636(__this, method) (void)Enumerator_VerifyCurrent_m18383_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m18637(__this, method) (void)Enumerator_Dispose_m18384_gshared((Enumerator_t3460 *)__this, method)
