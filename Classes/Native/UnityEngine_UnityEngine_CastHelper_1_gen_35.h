﻿#pragma once
#include <stdint.h>
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>
struct CastHelper_1_t4595 
{
	// T UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>::t
	ImageTargetAbstractBehaviour_t23 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
