﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_29.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
struct CachedInvokableCall_1_t2977  : public InvokableCall_1_t2978
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
