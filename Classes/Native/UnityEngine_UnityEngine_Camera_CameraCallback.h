﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.Camera
struct Camera_t168;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t1036  : public MulticastDelegate_t373
{
};
