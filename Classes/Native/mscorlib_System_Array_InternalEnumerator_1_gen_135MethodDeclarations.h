﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Player_Script/TypePlayer>
struct InternalEnumerator_1_t3096;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Player_Script/TypePlayer
#include "AssemblyU2DCSharp_Player_Script_TypePlayer.h"

// System.Void System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m15778 (InternalEnumerator_1_t3096 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779 (InternalEnumerator_1_t3096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::Dispose()
 void InternalEnumerator_1_Dispose_m15780 (InternalEnumerator_1_t3096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m15781 (InternalEnumerator_1_t3096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15782 (InternalEnumerator_1_t3096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
