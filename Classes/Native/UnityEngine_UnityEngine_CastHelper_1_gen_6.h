﻿#pragma once
#include <stdint.h>
// InGameState_Script
struct InGameState_Script_t83;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<InGameState_Script>
struct CastHelper_1_t3006 
{
	// T UnityEngine.CastHelper`1<InGameState_Script>::t
	InGameState_Script_t83 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<InGameState_Script>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
