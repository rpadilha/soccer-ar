﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ImageTarget>
struct Comparer_1_t4433;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ImageTarget>
struct Comparer_1_t4433  : public Object_t
{
};
struct Comparer_1_t4433_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ImageTarget>::_default
	Comparer_1_t4433 * ____default_0;
};
