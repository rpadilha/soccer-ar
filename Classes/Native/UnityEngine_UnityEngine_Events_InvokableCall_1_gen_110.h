﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneAbstractBehaviour>
struct UnityAction_1_t3829;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneAbstractBehaviour>
struct InvokableCall_1_t3828  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Delegate
	UnityAction_1_t3829 * ___Delegate_0;
};
