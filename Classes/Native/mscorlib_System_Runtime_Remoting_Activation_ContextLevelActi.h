﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2033;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t2038  : public Object_t
{
	// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ContextLevelActivator::m_NextActivator
	Object_t * ___m_NextActivator_0;
};
