﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_35.h"
// UnityEngine.Events.CachedInvokableCall`1<Camera_Script>
struct CachedInvokableCall_1_t3009  : public InvokableCall_1_t3010
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Camera_Script>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
