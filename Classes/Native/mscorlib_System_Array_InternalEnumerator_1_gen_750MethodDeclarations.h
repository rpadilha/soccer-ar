﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>
struct InternalEnumerator_1_t5296;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31852 (InternalEnumerator_1_t5296 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31853 (InternalEnumerator_1_t5296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>::Dispose()
 void InternalEnumerator_1_Dispose_m31854 (InternalEnumerator_1_t5296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31855 (InternalEnumerator_1_t5296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.TypeFilterLevel>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31856 (InternalEnumerator_1_t5296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
