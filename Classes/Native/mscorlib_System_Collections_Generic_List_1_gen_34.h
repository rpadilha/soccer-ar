﻿#pragma once
#include <stdint.h>
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t871;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t742  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::_items
	WordAbstractBehaviourU5BU5D_t871* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::_version
	int32_t ____version_3;
};
struct List_1_t742_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::EmptyArray
	WordAbstractBehaviourU5BU5D_t871* ___EmptyArray_4;
};
