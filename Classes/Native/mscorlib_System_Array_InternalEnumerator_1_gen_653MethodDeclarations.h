﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IO.FileOptions>
struct InternalEnumerator_1_t5197;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptions.h"

// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31359 (InternalEnumerator_1_t5197 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileOptions>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360 (InternalEnumerator_1_t5197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::Dispose()
 void InternalEnumerator_1_Dispose_m31361 (InternalEnumerator_1_t5197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileOptions>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31362 (InternalEnumerator_1_t5197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.IO.FileOptions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31363 (InternalEnumerator_1_t5197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
