﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>
struct InternalEnumerator_1_t5306;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31902 (InternalEnumerator_1_t5306 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31903 (InternalEnumerator_1_t5306 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>::Dispose()
 void InternalEnumerator_1_Dispose_m31904 (InternalEnumerator_1_t5306 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31905 (InternalEnumerator_1_t5306 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.PaddingMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31906 (InternalEnumerator_1_t5306 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
