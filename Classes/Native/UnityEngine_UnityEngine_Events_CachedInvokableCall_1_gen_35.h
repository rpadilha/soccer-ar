﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_31.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>
struct CachedInvokableCall_1_t2987  : public InvokableCall_1_t2988
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
