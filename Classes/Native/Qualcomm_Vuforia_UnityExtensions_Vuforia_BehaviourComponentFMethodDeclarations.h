﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory
struct BehaviourComponentFactory_t637;
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t153;

// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::get_Instance()
 Object_t * BehaviourComponentFactory_get_Instance_m2982 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
 void BehaviourComponentFactory_set_Instance_m334 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::.ctor()
 void BehaviourComponentFactory__ctor_m2983 (BehaviourComponentFactory_t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
