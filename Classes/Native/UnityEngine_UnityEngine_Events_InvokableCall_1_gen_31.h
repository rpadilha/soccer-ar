﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>
struct UnityAction_1_t2989;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>
struct InvokableCall_1_t2988  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Delegate
	UnityAction_1_t2989 * ___Delegate_0;
};
