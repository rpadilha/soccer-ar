﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffAbstractBehaviour>
struct UnityAction_1_t4538;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffAbstractBehaviour>
struct InvokableCall_1_t4537  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffAbstractBehaviour>::Delegate
	UnityAction_1_t4538 * ___Delegate_0;
};
