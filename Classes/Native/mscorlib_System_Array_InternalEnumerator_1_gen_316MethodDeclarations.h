﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.DataSet/StorageType>
struct InternalEnumerator_1_t3908;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSet/StorageType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21454 (InternalEnumerator_1_t3908 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSet/StorageType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21455 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSet/StorageType>::Dispose()
 void InternalEnumerator_1_Dispose_m21456 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSet/StorageType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21457 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.DataSet/StorageType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21458 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
