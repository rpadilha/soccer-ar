﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$16
struct $ArrayType$16_t1707;
struct $ArrayType$16_t1707_marshaled;

void $ArrayType$16_t1707_marshal(const $ArrayType$16_t1707& unmarshaled, $ArrayType$16_t1707_marshaled& marshaled);
void $ArrayType$16_t1707_marshal_back(const $ArrayType$16_t1707_marshaled& marshaled, $ArrayType$16_t1707& unmarshaled);
void $ArrayType$16_t1707_marshal_cleanup($ArrayType$16_t1707_marshaled& marshaled);
