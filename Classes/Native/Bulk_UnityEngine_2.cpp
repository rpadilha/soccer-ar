﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo ScreenOrientation_t137_il2cpp_TypeInfo;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientationMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition UnityEngine.ScreenOrientation
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo ScreenOrientation_t137____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, offsetof(ScreenOrientation_t137, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____Unknown_2_FieldInfo = 
{
	"Unknown"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____Portrait_3_FieldInfo = 
{
	"Portrait"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____PortraitUpsideDown_4_FieldInfo = 
{
	"PortraitUpsideDown"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____LandscapeLeft_5_FieldInfo = 
{
	"LandscapeLeft"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____LandscapeRight_6_FieldInfo = 
{
	"LandscapeRight"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____AutoRotation_7_FieldInfo = 
{
	"AutoRotation"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t137_0_0_32854;
FieldInfo ScreenOrientation_t137____Landscape_8_FieldInfo = 
{
	"Landscape"/* name */
	, &ScreenOrientation_t137_0_0_32854/* type */
	, &ScreenOrientation_t137_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ScreenOrientation_t137_FieldInfos[] =
{
	&ScreenOrientation_t137____value___1_FieldInfo,
	&ScreenOrientation_t137____Unknown_2_FieldInfo,
	&ScreenOrientation_t137____Portrait_3_FieldInfo,
	&ScreenOrientation_t137____PortraitUpsideDown_4_FieldInfo,
	&ScreenOrientation_t137____LandscapeLeft_5_FieldInfo,
	&ScreenOrientation_t137____LandscapeRight_6_FieldInfo,
	&ScreenOrientation_t137____AutoRotation_7_FieldInfo,
	&ScreenOrientation_t137____Landscape_8_FieldInfo,
	NULL
};
static const int32_t ScreenOrientation_t137____Unknown_2_DefaultValueData = 0;
extern Il2CppType Int32_t123_0_0_0;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____Unknown_2_DefaultValue = 
{
	&ScreenOrientation_t137____Unknown_2_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____Unknown_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t137____Portrait_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____Portrait_3_DefaultValue = 
{
	&ScreenOrientation_t137____Portrait_3_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____Portrait_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t137____PortraitUpsideDown_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____PortraitUpsideDown_4_DefaultValue = 
{
	&ScreenOrientation_t137____PortraitUpsideDown_4_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____PortraitUpsideDown_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t137____LandscapeLeft_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____LandscapeLeft_5_DefaultValue = 
{
	&ScreenOrientation_t137____LandscapeLeft_5_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____LandscapeLeft_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t137____LandscapeRight_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____LandscapeRight_6_DefaultValue = 
{
	&ScreenOrientation_t137____LandscapeRight_6_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____LandscapeRight_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t137____AutoRotation_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____AutoRotation_7_DefaultValue = 
{
	&ScreenOrientation_t137____AutoRotation_7_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____AutoRotation_7_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ScreenOrientation_t137____Landscape_8_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry ScreenOrientation_t137____Landscape_8_DefaultValue = 
{
	&ScreenOrientation_t137____Landscape_8_FieldInfo/* field */
	, { (char*)&ScreenOrientation_t137____Landscape_8_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ScreenOrientation_t137_FieldDefaultValues[] = 
{
	&ScreenOrientation_t137____Unknown_2_DefaultValue,
	&ScreenOrientation_t137____Portrait_3_DefaultValue,
	&ScreenOrientation_t137____PortraitUpsideDown_4_DefaultValue,
	&ScreenOrientation_t137____LandscapeLeft_5_DefaultValue,
	&ScreenOrientation_t137____LandscapeRight_6_DefaultValue,
	&ScreenOrientation_t137____AutoRotation_7_DefaultValue,
	&ScreenOrientation_t137____Landscape_8_DefaultValue,
	NULL
};
static MethodInfo* ScreenOrientation_t137_MethodInfos[] =
{
	NULL
};
extern MethodInfo Enum_Equals_m587_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Enum_GetHashCode_m588_MethodInfo;
extern MethodInfo Enum_ToString_m589_MethodInfo;
extern MethodInfo Enum_ToString_m590_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m591_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m592_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m593_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m594_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m595_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m596_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m597_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m598_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m599_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m600_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m601_MethodInfo;
extern MethodInfo Enum_ToString_m602_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m603_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m604_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m605_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m606_MethodInfo;
extern MethodInfo Enum_CompareTo_m607_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m608_MethodInfo;
static MethodInfo* ScreenOrientation_t137_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
extern TypeInfo IFormattable_t182_il2cpp_TypeInfo;
extern TypeInfo IConvertible_t183_il2cpp_TypeInfo;
extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair ScreenOrientation_t137_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern Il2CppType ScreenOrientation_t137_1_0_0;
extern TypeInfo Enum_t185_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
TypeInfo ScreenOrientation_t137_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenOrientation"/* name */
	, "UnityEngine"/* namespaze */
	, ScreenOrientation_t137_MethodInfos/* methods */
	, NULL/* properties */
	, ScreenOrientation_t137_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ScreenOrientation_t137_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &ScreenOrientation_t137_0_0_0/* byval_arg */
	, &ScreenOrientation_t137_1_0_0/* this_arg */
	, ScreenOrientation_t137_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ScreenOrientation_t137_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenOrientation_t137)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo FilterMode_t1099_il2cpp_TypeInfo;
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterModeMethodDeclarations.h"



// Metadata Definition UnityEngine.FilterMode
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo FilterMode_t1099____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &FilterMode_t1099_il2cpp_TypeInfo/* parent */
	, offsetof(FilterMode_t1099, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FilterMode_t1099_0_0_32854;
FieldInfo FilterMode_t1099____Point_2_FieldInfo = 
{
	"Point"/* name */
	, &FilterMode_t1099_0_0_32854/* type */
	, &FilterMode_t1099_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FilterMode_t1099_0_0_32854;
FieldInfo FilterMode_t1099____Bilinear_3_FieldInfo = 
{
	"Bilinear"/* name */
	, &FilterMode_t1099_0_0_32854/* type */
	, &FilterMode_t1099_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FilterMode_t1099_0_0_32854;
FieldInfo FilterMode_t1099____Trilinear_4_FieldInfo = 
{
	"Trilinear"/* name */
	, &FilterMode_t1099_0_0_32854/* type */
	, &FilterMode_t1099_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* FilterMode_t1099_FieldInfos[] =
{
	&FilterMode_t1099____value___1_FieldInfo,
	&FilterMode_t1099____Point_2_FieldInfo,
	&FilterMode_t1099____Bilinear_3_FieldInfo,
	&FilterMode_t1099____Trilinear_4_FieldInfo,
	NULL
};
static const int32_t FilterMode_t1099____Point_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry FilterMode_t1099____Point_2_DefaultValue = 
{
	&FilterMode_t1099____Point_2_FieldInfo/* field */
	, { (char*)&FilterMode_t1099____Point_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t FilterMode_t1099____Bilinear_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry FilterMode_t1099____Bilinear_3_DefaultValue = 
{
	&FilterMode_t1099____Bilinear_3_FieldInfo/* field */
	, { (char*)&FilterMode_t1099____Bilinear_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t FilterMode_t1099____Trilinear_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry FilterMode_t1099____Trilinear_4_DefaultValue = 
{
	&FilterMode_t1099____Trilinear_4_FieldInfo/* field */
	, { (char*)&FilterMode_t1099____Trilinear_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* FilterMode_t1099_FieldDefaultValues[] = 
{
	&FilterMode_t1099____Point_2_DefaultValue,
	&FilterMode_t1099____Bilinear_3_DefaultValue,
	&FilterMode_t1099____Trilinear_4_DefaultValue,
	NULL
};
static MethodInfo* FilterMode_t1099_MethodInfos[] =
{
	NULL
};
static MethodInfo* FilterMode_t1099_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair FilterMode_t1099_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType FilterMode_t1099_0_0_0;
extern Il2CppType FilterMode_t1099_1_0_0;
TypeInfo FilterMode_t1099_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FilterMode"/* name */
	, "UnityEngine"/* namespaze */
	, FilterMode_t1099_MethodInfos/* methods */
	, NULL/* properties */
	, FilterMode_t1099_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, FilterMode_t1099_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &FilterMode_t1099_0_0_0/* byval_arg */
	, &FilterMode_t1099_1_0_0/* this_arg */
	, FilterMode_t1099_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, FilterMode_t1099_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FilterMode_t1099)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextureWrapMode_t1100_il2cpp_TypeInfo;
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapModeMethodDeclarations.h"



// Metadata Definition UnityEngine.TextureWrapMode
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo TextureWrapMode_t1100____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &TextureWrapMode_t1100_il2cpp_TypeInfo/* parent */
	, offsetof(TextureWrapMode_t1100, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureWrapMode_t1100_0_0_32854;
FieldInfo TextureWrapMode_t1100____Repeat_2_FieldInfo = 
{
	"Repeat"/* name */
	, &TextureWrapMode_t1100_0_0_32854/* type */
	, &TextureWrapMode_t1100_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureWrapMode_t1100_0_0_32854;
FieldInfo TextureWrapMode_t1100____Clamp_3_FieldInfo = 
{
	"Clamp"/* name */
	, &TextureWrapMode_t1100_0_0_32854/* type */
	, &TextureWrapMode_t1100_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextureWrapMode_t1100_FieldInfos[] =
{
	&TextureWrapMode_t1100____value___1_FieldInfo,
	&TextureWrapMode_t1100____Repeat_2_FieldInfo,
	&TextureWrapMode_t1100____Clamp_3_FieldInfo,
	NULL
};
static const int32_t TextureWrapMode_t1100____Repeat_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TextureWrapMode_t1100____Repeat_2_DefaultValue = 
{
	&TextureWrapMode_t1100____Repeat_2_FieldInfo/* field */
	, { (char*)&TextureWrapMode_t1100____Repeat_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureWrapMode_t1100____Clamp_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TextureWrapMode_t1100____Clamp_3_DefaultValue = 
{
	&TextureWrapMode_t1100____Clamp_3_FieldInfo/* field */
	, { (char*)&TextureWrapMode_t1100____Clamp_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TextureWrapMode_t1100_FieldDefaultValues[] = 
{
	&TextureWrapMode_t1100____Repeat_2_DefaultValue,
	&TextureWrapMode_t1100____Clamp_3_DefaultValue,
	NULL
};
static MethodInfo* TextureWrapMode_t1100_MethodInfos[] =
{
	NULL
};
static MethodInfo* TextureWrapMode_t1100_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair TextureWrapMode_t1100_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextureWrapMode_t1100_0_0_0;
extern Il2CppType TextureWrapMode_t1100_1_0_0;
TypeInfo TextureWrapMode_t1100_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, TextureWrapMode_t1100_MethodInfos/* methods */
	, NULL/* properties */
	, TextureWrapMode_t1100_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextureWrapMode_t1100_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &TextureWrapMode_t1100_0_0_0/* byval_arg */
	, &TextureWrapMode_t1100_1_0_0/* this_arg */
	, TextureWrapMode_t1100_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TextureWrapMode_t1100_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureWrapMode_t1100)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextureFormat_t841_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"



// Metadata Definition UnityEngine.TextureFormat
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo TextureFormat_t841____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, offsetof(TextureFormat_t841, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____Alpha8_2_FieldInfo = 
{
	"Alpha8"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ARGB4444_3_FieldInfo = 
{
	"ARGB4444"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGB24_4_FieldInfo = 
{
	"RGB24"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGBA32_5_FieldInfo = 
{
	"RGBA32"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ARGB32_6_FieldInfo = 
{
	"ARGB32"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGB565_7_FieldInfo = 
{
	"RGB565"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____R16_8_FieldInfo = 
{
	"R16"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____DXT1_9_FieldInfo = 
{
	"DXT1"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____DXT5_10_FieldInfo = 
{
	"DXT5"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGBA4444_11_FieldInfo = 
{
	"RGBA4444"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____BGRA32_12_FieldInfo = 
{
	"BGRA32"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RHalf_13_FieldInfo = 
{
	"RHalf"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGHalf_14_FieldInfo = 
{
	"RGHalf"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGBAHalf_15_FieldInfo = 
{
	"RGBAHalf"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RFloat_16_FieldInfo = 
{
	"RFloat"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGFloat_17_FieldInfo = 
{
	"RGFloat"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____RGBAFloat_18_FieldInfo = 
{
	"RGBAFloat"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____YUY2_19_FieldInfo = 
{
	"YUY2"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____PVRTC_RGB2_20_FieldInfo = 
{
	"PVRTC_RGB2"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____PVRTC_RGBA2_21_FieldInfo = 
{
	"PVRTC_RGBA2"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____PVRTC_RGB4_22_FieldInfo = 
{
	"PVRTC_RGB4"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____PVRTC_RGBA4_23_FieldInfo = 
{
	"PVRTC_RGBA4"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ETC_RGB4_24_FieldInfo = 
{
	"ETC_RGB4"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ATC_RGB4_25_FieldInfo = 
{
	"ATC_RGB4"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ATC_RGBA8_26_FieldInfo = 
{
	"ATC_RGBA8"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____EAC_R_27_FieldInfo = 
{
	"EAC_R"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____EAC_R_SIGNED_28_FieldInfo = 
{
	"EAC_R_SIGNED"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____EAC_RG_29_FieldInfo = 
{
	"EAC_RG"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____EAC_RG_SIGNED_30_FieldInfo = 
{
	"EAC_RG_SIGNED"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ETC2_RGB_31_FieldInfo = 
{
	"ETC2_RGB"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ETC2_RGBA1_32_FieldInfo = 
{
	"ETC2_RGBA1"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ETC2_RGBA8_33_FieldInfo = 
{
	"ETC2_RGBA8"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGB_4x4_34_FieldInfo = 
{
	"ASTC_RGB_4x4"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGB_5x5_35_FieldInfo = 
{
	"ASTC_RGB_5x5"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGB_6x6_36_FieldInfo = 
{
	"ASTC_RGB_6x6"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGB_8x8_37_FieldInfo = 
{
	"ASTC_RGB_8x8"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGB_10x10_38_FieldInfo = 
{
	"ASTC_RGB_10x10"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGB_12x12_39_FieldInfo = 
{
	"ASTC_RGB_12x12"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGBA_4x4_40_FieldInfo = 
{
	"ASTC_RGBA_4x4"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGBA_5x5_41_FieldInfo = 
{
	"ASTC_RGBA_5x5"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGBA_6x6_42_FieldInfo = 
{
	"ASTC_RGBA_6x6"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGBA_8x8_43_FieldInfo = 
{
	"ASTC_RGBA_8x8"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGBA_10x10_44_FieldInfo = 
{
	"ASTC_RGBA_10x10"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextureFormat_t841_0_0_32854;
FieldInfo TextureFormat_t841____ASTC_RGBA_12x12_45_FieldInfo = 
{
	"ASTC_RGBA_12x12"/* name */
	, &TextureFormat_t841_0_0_32854/* type */
	, &TextureFormat_t841_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextureFormat_t841_FieldInfos[] =
{
	&TextureFormat_t841____value___1_FieldInfo,
	&TextureFormat_t841____Alpha8_2_FieldInfo,
	&TextureFormat_t841____ARGB4444_3_FieldInfo,
	&TextureFormat_t841____RGB24_4_FieldInfo,
	&TextureFormat_t841____RGBA32_5_FieldInfo,
	&TextureFormat_t841____ARGB32_6_FieldInfo,
	&TextureFormat_t841____RGB565_7_FieldInfo,
	&TextureFormat_t841____R16_8_FieldInfo,
	&TextureFormat_t841____DXT1_9_FieldInfo,
	&TextureFormat_t841____DXT5_10_FieldInfo,
	&TextureFormat_t841____RGBA4444_11_FieldInfo,
	&TextureFormat_t841____BGRA32_12_FieldInfo,
	&TextureFormat_t841____RHalf_13_FieldInfo,
	&TextureFormat_t841____RGHalf_14_FieldInfo,
	&TextureFormat_t841____RGBAHalf_15_FieldInfo,
	&TextureFormat_t841____RFloat_16_FieldInfo,
	&TextureFormat_t841____RGFloat_17_FieldInfo,
	&TextureFormat_t841____RGBAFloat_18_FieldInfo,
	&TextureFormat_t841____YUY2_19_FieldInfo,
	&TextureFormat_t841____PVRTC_RGB2_20_FieldInfo,
	&TextureFormat_t841____PVRTC_RGBA2_21_FieldInfo,
	&TextureFormat_t841____PVRTC_RGB4_22_FieldInfo,
	&TextureFormat_t841____PVRTC_RGBA4_23_FieldInfo,
	&TextureFormat_t841____ETC_RGB4_24_FieldInfo,
	&TextureFormat_t841____ATC_RGB4_25_FieldInfo,
	&TextureFormat_t841____ATC_RGBA8_26_FieldInfo,
	&TextureFormat_t841____EAC_R_27_FieldInfo,
	&TextureFormat_t841____EAC_R_SIGNED_28_FieldInfo,
	&TextureFormat_t841____EAC_RG_29_FieldInfo,
	&TextureFormat_t841____EAC_RG_SIGNED_30_FieldInfo,
	&TextureFormat_t841____ETC2_RGB_31_FieldInfo,
	&TextureFormat_t841____ETC2_RGBA1_32_FieldInfo,
	&TextureFormat_t841____ETC2_RGBA8_33_FieldInfo,
	&TextureFormat_t841____ASTC_RGB_4x4_34_FieldInfo,
	&TextureFormat_t841____ASTC_RGB_5x5_35_FieldInfo,
	&TextureFormat_t841____ASTC_RGB_6x6_36_FieldInfo,
	&TextureFormat_t841____ASTC_RGB_8x8_37_FieldInfo,
	&TextureFormat_t841____ASTC_RGB_10x10_38_FieldInfo,
	&TextureFormat_t841____ASTC_RGB_12x12_39_FieldInfo,
	&TextureFormat_t841____ASTC_RGBA_4x4_40_FieldInfo,
	&TextureFormat_t841____ASTC_RGBA_5x5_41_FieldInfo,
	&TextureFormat_t841____ASTC_RGBA_6x6_42_FieldInfo,
	&TextureFormat_t841____ASTC_RGBA_8x8_43_FieldInfo,
	&TextureFormat_t841____ASTC_RGBA_10x10_44_FieldInfo,
	&TextureFormat_t841____ASTC_RGBA_12x12_45_FieldInfo,
	NULL
};
static const int32_t TextureFormat_t841____Alpha8_2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____Alpha8_2_DefaultValue = 
{
	&TextureFormat_t841____Alpha8_2_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____Alpha8_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ARGB4444_3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ARGB4444_3_DefaultValue = 
{
	&TextureFormat_t841____ARGB4444_3_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ARGB4444_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGB24_4_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGB24_4_DefaultValue = 
{
	&TextureFormat_t841____RGB24_4_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGB24_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGBA32_5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGBA32_5_DefaultValue = 
{
	&TextureFormat_t841____RGBA32_5_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGBA32_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ARGB32_6_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ARGB32_6_DefaultValue = 
{
	&TextureFormat_t841____ARGB32_6_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ARGB32_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGB565_7_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGB565_7_DefaultValue = 
{
	&TextureFormat_t841____RGB565_7_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGB565_7_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____R16_8_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____R16_8_DefaultValue = 
{
	&TextureFormat_t841____R16_8_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____R16_8_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____DXT1_9_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____DXT1_9_DefaultValue = 
{
	&TextureFormat_t841____DXT1_9_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____DXT1_9_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____DXT5_10_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____DXT5_10_DefaultValue = 
{
	&TextureFormat_t841____DXT5_10_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____DXT5_10_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGBA4444_11_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGBA4444_11_DefaultValue = 
{
	&TextureFormat_t841____RGBA4444_11_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGBA4444_11_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____BGRA32_12_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____BGRA32_12_DefaultValue = 
{
	&TextureFormat_t841____BGRA32_12_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____BGRA32_12_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RHalf_13_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RHalf_13_DefaultValue = 
{
	&TextureFormat_t841____RHalf_13_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RHalf_13_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGHalf_14_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGHalf_14_DefaultValue = 
{
	&TextureFormat_t841____RGHalf_14_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGHalf_14_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGBAHalf_15_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGBAHalf_15_DefaultValue = 
{
	&TextureFormat_t841____RGBAHalf_15_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGBAHalf_15_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RFloat_16_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RFloat_16_DefaultValue = 
{
	&TextureFormat_t841____RFloat_16_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RFloat_16_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGFloat_17_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGFloat_17_DefaultValue = 
{
	&TextureFormat_t841____RGFloat_17_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGFloat_17_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____RGBAFloat_18_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____RGBAFloat_18_DefaultValue = 
{
	&TextureFormat_t841____RGBAFloat_18_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____RGBAFloat_18_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____YUY2_19_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____YUY2_19_DefaultValue = 
{
	&TextureFormat_t841____YUY2_19_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____YUY2_19_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____PVRTC_RGB2_20_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____PVRTC_RGB2_20_DefaultValue = 
{
	&TextureFormat_t841____PVRTC_RGB2_20_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____PVRTC_RGB2_20_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____PVRTC_RGBA2_21_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____PVRTC_RGBA2_21_DefaultValue = 
{
	&TextureFormat_t841____PVRTC_RGBA2_21_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____PVRTC_RGBA2_21_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____PVRTC_RGB4_22_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____PVRTC_RGB4_22_DefaultValue = 
{
	&TextureFormat_t841____PVRTC_RGB4_22_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____PVRTC_RGB4_22_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____PVRTC_RGBA4_23_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____PVRTC_RGBA4_23_DefaultValue = 
{
	&TextureFormat_t841____PVRTC_RGBA4_23_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____PVRTC_RGBA4_23_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ETC_RGB4_24_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ETC_RGB4_24_DefaultValue = 
{
	&TextureFormat_t841____ETC_RGB4_24_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ETC_RGB4_24_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ATC_RGB4_25_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ATC_RGB4_25_DefaultValue = 
{
	&TextureFormat_t841____ATC_RGB4_25_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ATC_RGB4_25_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ATC_RGBA8_26_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ATC_RGBA8_26_DefaultValue = 
{
	&TextureFormat_t841____ATC_RGBA8_26_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ATC_RGBA8_26_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____EAC_R_27_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____EAC_R_27_DefaultValue = 
{
	&TextureFormat_t841____EAC_R_27_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____EAC_R_27_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____EAC_R_SIGNED_28_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____EAC_R_SIGNED_28_DefaultValue = 
{
	&TextureFormat_t841____EAC_R_SIGNED_28_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____EAC_R_SIGNED_28_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____EAC_RG_29_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____EAC_RG_29_DefaultValue = 
{
	&TextureFormat_t841____EAC_RG_29_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____EAC_RG_29_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____EAC_RG_SIGNED_30_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____EAC_RG_SIGNED_30_DefaultValue = 
{
	&TextureFormat_t841____EAC_RG_SIGNED_30_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____EAC_RG_SIGNED_30_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ETC2_RGB_31_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ETC2_RGB_31_DefaultValue = 
{
	&TextureFormat_t841____ETC2_RGB_31_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ETC2_RGB_31_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ETC2_RGBA1_32_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ETC2_RGBA1_32_DefaultValue = 
{
	&TextureFormat_t841____ETC2_RGBA1_32_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ETC2_RGBA1_32_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ETC2_RGBA8_33_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ETC2_RGBA8_33_DefaultValue = 
{
	&TextureFormat_t841____ETC2_RGBA8_33_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ETC2_RGBA8_33_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGB_4x4_34_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGB_4x4_34_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGB_4x4_34_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGB_4x4_34_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGB_5x5_35_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGB_5x5_35_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGB_5x5_35_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGB_5x5_35_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGB_6x6_36_DefaultValueData = 50;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGB_6x6_36_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGB_6x6_36_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGB_6x6_36_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGB_8x8_37_DefaultValueData = 51;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGB_8x8_37_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGB_8x8_37_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGB_8x8_37_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGB_10x10_38_DefaultValueData = 52;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGB_10x10_38_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGB_10x10_38_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGB_10x10_38_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGB_12x12_39_DefaultValueData = 53;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGB_12x12_39_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGB_12x12_39_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGB_12x12_39_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGBA_4x4_40_DefaultValueData = 54;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGBA_4x4_40_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGBA_4x4_40_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGBA_4x4_40_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGBA_5x5_41_DefaultValueData = 55;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGBA_5x5_41_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGBA_5x5_41_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGBA_5x5_41_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGBA_6x6_42_DefaultValueData = 56;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGBA_6x6_42_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGBA_6x6_42_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGBA_6x6_42_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGBA_8x8_43_DefaultValueData = 57;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGBA_8x8_43_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGBA_8x8_43_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGBA_8x8_43_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGBA_10x10_44_DefaultValueData = 58;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGBA_10x10_44_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGBA_10x10_44_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGBA_10x10_44_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextureFormat_t841____ASTC_RGBA_12x12_45_DefaultValueData = 59;
static Il2CppFieldDefaultValueEntry TextureFormat_t841____ASTC_RGBA_12x12_45_DefaultValue = 
{
	&TextureFormat_t841____ASTC_RGBA_12x12_45_FieldInfo/* field */
	, { (char*)&TextureFormat_t841____ASTC_RGBA_12x12_45_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TextureFormat_t841_FieldDefaultValues[] = 
{
	&TextureFormat_t841____Alpha8_2_DefaultValue,
	&TextureFormat_t841____ARGB4444_3_DefaultValue,
	&TextureFormat_t841____RGB24_4_DefaultValue,
	&TextureFormat_t841____RGBA32_5_DefaultValue,
	&TextureFormat_t841____ARGB32_6_DefaultValue,
	&TextureFormat_t841____RGB565_7_DefaultValue,
	&TextureFormat_t841____R16_8_DefaultValue,
	&TextureFormat_t841____DXT1_9_DefaultValue,
	&TextureFormat_t841____DXT5_10_DefaultValue,
	&TextureFormat_t841____RGBA4444_11_DefaultValue,
	&TextureFormat_t841____BGRA32_12_DefaultValue,
	&TextureFormat_t841____RHalf_13_DefaultValue,
	&TextureFormat_t841____RGHalf_14_DefaultValue,
	&TextureFormat_t841____RGBAHalf_15_DefaultValue,
	&TextureFormat_t841____RFloat_16_DefaultValue,
	&TextureFormat_t841____RGFloat_17_DefaultValue,
	&TextureFormat_t841____RGBAFloat_18_DefaultValue,
	&TextureFormat_t841____YUY2_19_DefaultValue,
	&TextureFormat_t841____PVRTC_RGB2_20_DefaultValue,
	&TextureFormat_t841____PVRTC_RGBA2_21_DefaultValue,
	&TextureFormat_t841____PVRTC_RGB4_22_DefaultValue,
	&TextureFormat_t841____PVRTC_RGBA4_23_DefaultValue,
	&TextureFormat_t841____ETC_RGB4_24_DefaultValue,
	&TextureFormat_t841____ATC_RGB4_25_DefaultValue,
	&TextureFormat_t841____ATC_RGBA8_26_DefaultValue,
	&TextureFormat_t841____EAC_R_27_DefaultValue,
	&TextureFormat_t841____EAC_R_SIGNED_28_DefaultValue,
	&TextureFormat_t841____EAC_RG_29_DefaultValue,
	&TextureFormat_t841____EAC_RG_SIGNED_30_DefaultValue,
	&TextureFormat_t841____ETC2_RGB_31_DefaultValue,
	&TextureFormat_t841____ETC2_RGBA1_32_DefaultValue,
	&TextureFormat_t841____ETC2_RGBA8_33_DefaultValue,
	&TextureFormat_t841____ASTC_RGB_4x4_34_DefaultValue,
	&TextureFormat_t841____ASTC_RGB_5x5_35_DefaultValue,
	&TextureFormat_t841____ASTC_RGB_6x6_36_DefaultValue,
	&TextureFormat_t841____ASTC_RGB_8x8_37_DefaultValue,
	&TextureFormat_t841____ASTC_RGB_10x10_38_DefaultValue,
	&TextureFormat_t841____ASTC_RGB_12x12_39_DefaultValue,
	&TextureFormat_t841____ASTC_RGBA_4x4_40_DefaultValue,
	&TextureFormat_t841____ASTC_RGBA_5x5_41_DefaultValue,
	&TextureFormat_t841____ASTC_RGBA_6x6_42_DefaultValue,
	&TextureFormat_t841____ASTC_RGBA_8x8_43_DefaultValue,
	&TextureFormat_t841____ASTC_RGBA_10x10_44_DefaultValue,
	&TextureFormat_t841____ASTC_RGBA_12x12_45_DefaultValue,
	NULL
};
static MethodInfo* TextureFormat_t841_MethodInfos[] =
{
	NULL
};
static MethodInfo* TextureFormat_t841_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair TextureFormat_t841_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextureFormat_t841_0_0_0;
extern Il2CppType TextureFormat_t841_1_0_0;
TypeInfo TextureFormat_t841_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, TextureFormat_t841_MethodInfos/* methods */
	, NULL/* properties */
	, TextureFormat_t841_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextureFormat_t841_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &TextureFormat_t841_0_0_0/* byval_arg */
	, &TextureFormat_t841_1_0_0/* this_arg */
	, TextureFormat_t841_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TextureFormat_t841_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t841)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RenderTextureFormat_t1101_il2cpp_TypeInfo;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormatMethodDeclarations.h"



// Metadata Definition UnityEngine.RenderTextureFormat
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo RenderTextureFormat_t1101____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, offsetof(RenderTextureFormat_t1101, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGB32_2_FieldInfo = 
{
	"ARGB32"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____Depth_3_FieldInfo = 
{
	"Depth"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGBHalf_4_FieldInfo = 
{
	"ARGBHalf"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____Shadowmap_5_FieldInfo = 
{
	"Shadowmap"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RGB565_6_FieldInfo = 
{
	"RGB565"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGB4444_7_FieldInfo = 
{
	"ARGB4444"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGB1555_8_FieldInfo = 
{
	"ARGB1555"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____Default_9_FieldInfo = 
{
	"Default"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGB2101010_10_FieldInfo = 
{
	"ARGB2101010"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____DefaultHDR_11_FieldInfo = 
{
	"DefaultHDR"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGBFloat_12_FieldInfo = 
{
	"ARGBFloat"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RGFloat_13_FieldInfo = 
{
	"RGFloat"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RGHalf_14_FieldInfo = 
{
	"RGHalf"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RFloat_15_FieldInfo = 
{
	"RFloat"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RHalf_16_FieldInfo = 
{
	"RHalf"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____R8_17_FieldInfo = 
{
	"R8"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____ARGBInt_18_FieldInfo = 
{
	"ARGBInt"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RGInt_19_FieldInfo = 
{
	"RGInt"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_32854;
FieldInfo RenderTextureFormat_t1101____RInt_20_FieldInfo = 
{
	"RInt"/* name */
	, &RenderTextureFormat_t1101_0_0_32854/* type */
	, &RenderTextureFormat_t1101_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* RenderTextureFormat_t1101_FieldInfos[] =
{
	&RenderTextureFormat_t1101____value___1_FieldInfo,
	&RenderTextureFormat_t1101____ARGB32_2_FieldInfo,
	&RenderTextureFormat_t1101____Depth_3_FieldInfo,
	&RenderTextureFormat_t1101____ARGBHalf_4_FieldInfo,
	&RenderTextureFormat_t1101____Shadowmap_5_FieldInfo,
	&RenderTextureFormat_t1101____RGB565_6_FieldInfo,
	&RenderTextureFormat_t1101____ARGB4444_7_FieldInfo,
	&RenderTextureFormat_t1101____ARGB1555_8_FieldInfo,
	&RenderTextureFormat_t1101____Default_9_FieldInfo,
	&RenderTextureFormat_t1101____ARGB2101010_10_FieldInfo,
	&RenderTextureFormat_t1101____DefaultHDR_11_FieldInfo,
	&RenderTextureFormat_t1101____ARGBFloat_12_FieldInfo,
	&RenderTextureFormat_t1101____RGFloat_13_FieldInfo,
	&RenderTextureFormat_t1101____RGHalf_14_FieldInfo,
	&RenderTextureFormat_t1101____RFloat_15_FieldInfo,
	&RenderTextureFormat_t1101____RHalf_16_FieldInfo,
	&RenderTextureFormat_t1101____R8_17_FieldInfo,
	&RenderTextureFormat_t1101____ARGBInt_18_FieldInfo,
	&RenderTextureFormat_t1101____RGInt_19_FieldInfo,
	&RenderTextureFormat_t1101____RInt_20_FieldInfo,
	NULL
};
static const int32_t RenderTextureFormat_t1101____ARGB32_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGB32_2_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGB32_2_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGB32_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____Depth_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____Depth_3_DefaultValue = 
{
	&RenderTextureFormat_t1101____Depth_3_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____Depth_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____ARGBHalf_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGBHalf_4_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGBHalf_4_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGBHalf_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____Shadowmap_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____Shadowmap_5_DefaultValue = 
{
	&RenderTextureFormat_t1101____Shadowmap_5_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____Shadowmap_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RGB565_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RGB565_6_DefaultValue = 
{
	&RenderTextureFormat_t1101____RGB565_6_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RGB565_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____ARGB4444_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGB4444_7_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGB4444_7_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGB4444_7_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____ARGB1555_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGB1555_8_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGB1555_8_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGB1555_8_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____Default_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____Default_9_DefaultValue = 
{
	&RenderTextureFormat_t1101____Default_9_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____Default_9_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____ARGB2101010_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGB2101010_10_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGB2101010_10_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGB2101010_10_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____DefaultHDR_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____DefaultHDR_11_DefaultValue = 
{
	&RenderTextureFormat_t1101____DefaultHDR_11_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____DefaultHDR_11_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____ARGBFloat_12_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGBFloat_12_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGBFloat_12_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGBFloat_12_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RGFloat_13_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RGFloat_13_DefaultValue = 
{
	&RenderTextureFormat_t1101____RGFloat_13_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RGFloat_13_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RGHalf_14_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RGHalf_14_DefaultValue = 
{
	&RenderTextureFormat_t1101____RGHalf_14_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RGHalf_14_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RFloat_15_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RFloat_15_DefaultValue = 
{
	&RenderTextureFormat_t1101____RFloat_15_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RFloat_15_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RHalf_16_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RHalf_16_DefaultValue = 
{
	&RenderTextureFormat_t1101____RHalf_16_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RHalf_16_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____R8_17_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____R8_17_DefaultValue = 
{
	&RenderTextureFormat_t1101____R8_17_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____R8_17_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____ARGBInt_18_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____ARGBInt_18_DefaultValue = 
{
	&RenderTextureFormat_t1101____ARGBInt_18_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____ARGBInt_18_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RGInt_19_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RGInt_19_DefaultValue = 
{
	&RenderTextureFormat_t1101____RGInt_19_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RGInt_19_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureFormat_t1101____RInt_20_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry RenderTextureFormat_t1101____RInt_20_DefaultValue = 
{
	&RenderTextureFormat_t1101____RInt_20_FieldInfo/* field */
	, { (char*)&RenderTextureFormat_t1101____RInt_20_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* RenderTextureFormat_t1101_FieldDefaultValues[] = 
{
	&RenderTextureFormat_t1101____ARGB32_2_DefaultValue,
	&RenderTextureFormat_t1101____Depth_3_DefaultValue,
	&RenderTextureFormat_t1101____ARGBHalf_4_DefaultValue,
	&RenderTextureFormat_t1101____Shadowmap_5_DefaultValue,
	&RenderTextureFormat_t1101____RGB565_6_DefaultValue,
	&RenderTextureFormat_t1101____ARGB4444_7_DefaultValue,
	&RenderTextureFormat_t1101____ARGB1555_8_DefaultValue,
	&RenderTextureFormat_t1101____Default_9_DefaultValue,
	&RenderTextureFormat_t1101____ARGB2101010_10_DefaultValue,
	&RenderTextureFormat_t1101____DefaultHDR_11_DefaultValue,
	&RenderTextureFormat_t1101____ARGBFloat_12_DefaultValue,
	&RenderTextureFormat_t1101____RGFloat_13_DefaultValue,
	&RenderTextureFormat_t1101____RGHalf_14_DefaultValue,
	&RenderTextureFormat_t1101____RFloat_15_DefaultValue,
	&RenderTextureFormat_t1101____RHalf_16_DefaultValue,
	&RenderTextureFormat_t1101____R8_17_DefaultValue,
	&RenderTextureFormat_t1101____ARGBInt_18_DefaultValue,
	&RenderTextureFormat_t1101____RGInt_19_DefaultValue,
	&RenderTextureFormat_t1101____RInt_20_DefaultValue,
	NULL
};
static MethodInfo* RenderTextureFormat_t1101_MethodInfos[] =
{
	NULL
};
static MethodInfo* RenderTextureFormat_t1101_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair RenderTextureFormat_t1101_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
extern Il2CppType RenderTextureFormat_t1101_1_0_0;
TypeInfo RenderTextureFormat_t1101_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureFormat_t1101_MethodInfos/* methods */
	, NULL/* properties */
	, RenderTextureFormat_t1101_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RenderTextureFormat_t1101_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &RenderTextureFormat_t1101_0_0_0/* byval_arg */
	, &RenderTextureFormat_t1101_1_0_0/* this_arg */
	, RenderTextureFormat_t1101_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, RenderTextureFormat_t1101_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureFormat_t1101)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RenderTextureReadWrite_t1102_il2cpp_TypeInfo;
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWriteMethodDeclarations.h"



// Metadata Definition UnityEngine.RenderTextureReadWrite
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo RenderTextureReadWrite_t1102____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &RenderTextureReadWrite_t1102_il2cpp_TypeInfo/* parent */
	, offsetof(RenderTextureReadWrite_t1102, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_32854;
FieldInfo RenderTextureReadWrite_t1102____Default_2_FieldInfo = 
{
	"Default"/* name */
	, &RenderTextureReadWrite_t1102_0_0_32854/* type */
	, &RenderTextureReadWrite_t1102_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_32854;
FieldInfo RenderTextureReadWrite_t1102____Linear_3_FieldInfo = 
{
	"Linear"/* name */
	, &RenderTextureReadWrite_t1102_0_0_32854/* type */
	, &RenderTextureReadWrite_t1102_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_32854;
FieldInfo RenderTextureReadWrite_t1102____sRGB_4_FieldInfo = 
{
	"sRGB"/* name */
	, &RenderTextureReadWrite_t1102_0_0_32854/* type */
	, &RenderTextureReadWrite_t1102_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* RenderTextureReadWrite_t1102_FieldInfos[] =
{
	&RenderTextureReadWrite_t1102____value___1_FieldInfo,
	&RenderTextureReadWrite_t1102____Default_2_FieldInfo,
	&RenderTextureReadWrite_t1102____Linear_3_FieldInfo,
	&RenderTextureReadWrite_t1102____sRGB_4_FieldInfo,
	NULL
};
static const int32_t RenderTextureReadWrite_t1102____Default_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry RenderTextureReadWrite_t1102____Default_2_DefaultValue = 
{
	&RenderTextureReadWrite_t1102____Default_2_FieldInfo/* field */
	, { (char*)&RenderTextureReadWrite_t1102____Default_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureReadWrite_t1102____Linear_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry RenderTextureReadWrite_t1102____Linear_3_DefaultValue = 
{
	&RenderTextureReadWrite_t1102____Linear_3_FieldInfo/* field */
	, { (char*)&RenderTextureReadWrite_t1102____Linear_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t RenderTextureReadWrite_t1102____sRGB_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry RenderTextureReadWrite_t1102____sRGB_4_DefaultValue = 
{
	&RenderTextureReadWrite_t1102____sRGB_4_FieldInfo/* field */
	, { (char*)&RenderTextureReadWrite_t1102____sRGB_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* RenderTextureReadWrite_t1102_FieldDefaultValues[] = 
{
	&RenderTextureReadWrite_t1102____Default_2_DefaultValue,
	&RenderTextureReadWrite_t1102____Linear_3_DefaultValue,
	&RenderTextureReadWrite_t1102____sRGB_4_DefaultValue,
	NULL
};
static MethodInfo* RenderTextureReadWrite_t1102_MethodInfos[] =
{
	NULL
};
static MethodInfo* RenderTextureReadWrite_t1102_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair RenderTextureReadWrite_t1102_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
extern Il2CppType RenderTextureReadWrite_t1102_1_0_0;
TypeInfo RenderTextureReadWrite_t1102_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureReadWrite"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureReadWrite_t1102_MethodInfos/* methods */
	, NULL/* properties */
	, RenderTextureReadWrite_t1102_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RenderTextureReadWrite_t1102_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &RenderTextureReadWrite_t1102_0_0_0/* byval_arg */
	, &RenderTextureReadWrite_t1102_1_0_0/* this_arg */
	, RenderTextureReadWrite_t1102_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, RenderTextureReadWrite_t1102_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureReadWrite_t1102)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ReflectionProbeBlendInfo_t1103_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"



// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern Il2CppType ReflectionProbe_t989_0_0_6;
FieldInfo ReflectionProbeBlendInfo_t1103____probe_0_FieldInfo = 
{
	"probe"/* name */
	, &ReflectionProbe_t989_0_0_6/* type */
	, &ReflectionProbeBlendInfo_t1103_il2cpp_TypeInfo/* parent */
	, offsetof(ReflectionProbeBlendInfo_t1103, ___probe_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo ReflectionProbeBlendInfo_t1103____weight_1_FieldInfo = 
{
	"weight"/* name */
	, &Single_t170_0_0_6/* type */
	, &ReflectionProbeBlendInfo_t1103_il2cpp_TypeInfo/* parent */
	, offsetof(ReflectionProbeBlendInfo_t1103, ___weight_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ReflectionProbeBlendInfo_t1103_FieldInfos[] =
{
	&ReflectionProbeBlendInfo_t1103____probe_0_FieldInfo,
	&ReflectionProbeBlendInfo_t1103____weight_1_FieldInfo,
	NULL
};
static MethodInfo* ReflectionProbeBlendInfo_t1103_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* ReflectionProbeBlendInfo_t1103_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ReflectionProbeBlendInfo_t1103_0_0_0;
extern Il2CppType ReflectionProbeBlendInfo_t1103_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
TypeInfo ReflectionProbeBlendInfo_t1103_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, ReflectionProbeBlendInfo_t1103_MethodInfos/* methods */
	, NULL/* properties */
	, ReflectionProbeBlendInfo_t1103_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ReflectionProbeBlendInfo_t1103_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ReflectionProbeBlendInfo_t1103_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t1103_il2cpp_TypeInfo/* cast_class */
	, &ReflectionProbeBlendInfo_t1103_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t1103_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t1103)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LocalUser_t969_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Void
#include "mscorlib_System_Void.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
extern TypeInfo UserProfileU5BU5D_t968_il2cpp_TypeInfo;
extern TypeInfo UserProfile_t1094_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
extern MethodInfo UserProfile__ctor_m6430_MethodInfo;


// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern MethodInfo LocalUser__ctor_m6425_MethodInfo;
 void LocalUser__ctor_m6425 (LocalUser_t969 * __this, MethodInfo* method){
	{
		UserProfile__ctor_m6430(__this, /*hidden argument*/&UserProfile__ctor_m6430_MethodInfo);
		__this->___m_Friends_5 = (IUserProfileU5BU5D_t1104*)((UserProfileU5BU5D_t968*)SZArrayNew(InitializedTypeInfo(&UserProfileU5BU5D_t968_il2cpp_TypeInfo), 0));
		__this->___m_Authenticated_6 = 0;
		__this->___m_Underage_7 = 0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern MethodInfo LocalUser_SetFriends_m6426_MethodInfo;
 void LocalUser_SetFriends_m6426 (LocalUser_t969 * __this, IUserProfileU5BU5D_t1104* ___friends, MethodInfo* method){
	{
		__this->___m_Friends_5 = ___friends;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern MethodInfo LocalUser_SetAuthenticated_m6427_MethodInfo;
 void LocalUser_SetAuthenticated_m6427 (LocalUser_t969 * __this, bool ___value, MethodInfo* method){
	{
		__this->___m_Authenticated_6 = ___value;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern MethodInfo LocalUser_SetUnderage_m6428_MethodInfo;
 void LocalUser_SetUnderage_m6428 (LocalUser_t969 * __this, bool ___value, MethodInfo* method){
	{
		__this->___m_Underage_7 = ___value;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern MethodInfo LocalUser_get_authenticated_m6429_MethodInfo;
 bool LocalUser_get_authenticated_m6429 (LocalUser_t969 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_Authenticated_6);
		return L_0;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern Il2CppType IUserProfileU5BU5D_t1104_0_0_1;
FieldInfo LocalUser_t969____m_Friends_5_FieldInfo = 
{
	"m_Friends"/* name */
	, &IUserProfileU5BU5D_t1104_0_0_1/* type */
	, &LocalUser_t969_il2cpp_TypeInfo/* parent */
	, offsetof(LocalUser_t969, ___m_Friends_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo LocalUser_t969____m_Authenticated_6_FieldInfo = 
{
	"m_Authenticated"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &LocalUser_t969_il2cpp_TypeInfo/* parent */
	, offsetof(LocalUser_t969, ___m_Authenticated_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo LocalUser_t969____m_Underage_7_FieldInfo = 
{
	"m_Underage"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &LocalUser_t969_il2cpp_TypeInfo/* parent */
	, offsetof(LocalUser_t969, ___m_Underage_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* LocalUser_t969_FieldInfos[] =
{
	&LocalUser_t969____m_Friends_5_FieldInfo,
	&LocalUser_t969____m_Authenticated_6_FieldInfo,
	&LocalUser_t969____m_Underage_7_FieldInfo,
	NULL
};
static PropertyInfo LocalUser_t969____authenticated_PropertyInfo = 
{
	&LocalUser_t969_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &LocalUser_get_authenticated_m6429_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* LocalUser_t969_PropertyInfos[] =
{
	&LocalUser_t969____authenticated_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
MethodInfo LocalUser__ctor_m6425_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LocalUser__ctor_m6425/* method */
	, &LocalUser_t969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IUserProfileU5BU5D_t1104_0_0_0;
extern Il2CppType IUserProfileU5BU5D_t1104_0_0_0;
static ParameterInfo LocalUser_t969_LocalUser_SetFriends_m6426_ParameterInfos[] = 
{
	{"friends", 0, 134219275, &EmptyCustomAttributesCache, &IUserProfileU5BU5D_t1104_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
MethodInfo LocalUser_SetFriends_m6426_MethodInfo = 
{
	"SetFriends"/* name */
	, (methodPointerType)&LocalUser_SetFriends_m6426/* method */
	, &LocalUser_t969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, LocalUser_t969_LocalUser_SetFriends_m6426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo LocalUser_t969_LocalUser_SetAuthenticated_m6427_ParameterInfos[] = 
{
	{"value", 0, 134219276, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
MethodInfo LocalUser_SetAuthenticated_m6427_MethodInfo = 
{
	"SetAuthenticated"/* name */
	, (methodPointerType)&LocalUser_SetAuthenticated_m6427/* method */
	, &LocalUser_t969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, LocalUser_t969_LocalUser_SetAuthenticated_m6427_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo LocalUser_t969_LocalUser_SetUnderage_m6428_ParameterInfos[] = 
{
	{"value", 0, 134219277, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
MethodInfo LocalUser_SetUnderage_m6428_MethodInfo = 
{
	"SetUnderage"/* name */
	, (methodPointerType)&LocalUser_SetUnderage_m6428/* method */
	, &LocalUser_t969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, LocalUser_t969_LocalUser_SetUnderage_m6428_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
MethodInfo LocalUser_get_authenticated_m6429_MethodInfo = 
{
	"get_authenticated"/* name */
	, (methodPointerType)&LocalUser_get_authenticated_m6429/* method */
	, &LocalUser_t969_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LocalUser_t969_MethodInfos[] =
{
	&LocalUser__ctor_m6425_MethodInfo,
	&LocalUser_SetFriends_m6426_MethodInfo,
	&LocalUser_SetAuthenticated_m6427_MethodInfo,
	&LocalUser_SetUnderage_m6428_MethodInfo,
	&LocalUser_get_authenticated_m6429_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo UserProfile_ToString_m6432_MethodInfo;
extern MethodInfo UserProfile_get_userName_m6436_MethodInfo;
extern MethodInfo UserProfile_get_id_m6437_MethodInfo;
extern MethodInfo UserProfile_get_isFriend_m6438_MethodInfo;
extern MethodInfo UserProfile_get_state_m6439_MethodInfo;
static MethodInfo* LocalUser_t969_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UserProfile_ToString_m6432_MethodInfo,
	&UserProfile_get_userName_m6436_MethodInfo,
	&UserProfile_get_id_m6437_MethodInfo,
	&UserProfile_get_isFriend_m6438_MethodInfo,
	&UserProfile_get_state_m6439_MethodInfo,
	&LocalUser_get_authenticated_m6429_MethodInfo,
};
extern TypeInfo ILocalUser_t972_il2cpp_TypeInfo;
extern TypeInfo IUserProfile_t1162_il2cpp_TypeInfo;
static TypeInfo* LocalUser_t969_InterfacesTypeInfos[] = 
{
	&ILocalUser_t972_il2cpp_TypeInfo,
	&IUserProfile_t1162_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LocalUser_t969_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1162_il2cpp_TypeInfo, 4},
	{ &ILocalUser_t972_il2cpp_TypeInfo, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType LocalUser_t969_0_0_0;
extern Il2CppType LocalUser_t969_1_0_0;
struct LocalUser_t969;
TypeInfo LocalUser_t969_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, LocalUser_t969_MethodInfos/* methods */
	, LocalUser_t969_PropertyInfos/* properties */
	, LocalUser_t969_FieldInfos/* fields */
	, NULL/* events */
	, &UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LocalUser_t969_il2cpp_TypeInfo/* element_class */
	, LocalUser_t969_InterfacesTypeInfos/* implemented_interfaces */
	, LocalUser_t969_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &LocalUser_t969_il2cpp_TypeInfo/* cast_class */
	, &LocalUser_t969_0_0_0/* byval_arg */
	, &LocalUser_t969_1_0_0/* this_arg */
	, LocalUser_t969_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t969)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.String
#include "mscorlib_System_String.h"
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
extern TypeInfo Texture2D_t196_il2cpp_TypeInfo;
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo UserState_t1111_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern MethodInfo Object__ctor_m312_MethodInfo;
extern MethodInfo Texture2D__ctor_m4981_MethodInfo;
extern MethodInfo String_Concat_m516_MethodInfo;


// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
 void UserProfile__ctor_m6430 (UserProfile_t1094 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		__this->___m_UserName_0 = (String_t*) &_stringLiteral414;
		__this->___m_ID_1 = (String_t*) &_stringLiteral324;
		__this->___m_IsFriend_2 = 0;
		__this->___m_State_3 = 3;
		Texture2D_t196 * L_0 = (Texture2D_t196 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Texture2D_t196_il2cpp_TypeInfo));
		Texture2D__ctor_m4981(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/&Texture2D__ctor_m4981_MethodInfo);
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern MethodInfo UserProfile__ctor_m6431_MethodInfo;
 void UserProfile__ctor_m6431 (UserProfile_t1094 * __this, String_t* ___name, String_t* ___id, bool ___friend, int32_t ___state, Texture2D_t196 * ___image, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		__this->___m_UserName_0 = ___name;
		__this->___m_ID_1 = ___id;
		__this->___m_IsFriend_2 = ___friend;
		__this->___m_State_3 = ___state;
		__this->___m_Image_4 = ___image;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
 String_t* UserProfile_ToString_m6432 (UserProfile_t1094 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t130* L_0 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 7));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&UserProfile_get_id_m6437_MethodInfo, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t130* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&UserProfile_get_userName_m6436_MethodInfo, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2)) = (Object_t *)L_4;
		ObjectU5BU5D_t130* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_6 = L_5;
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(&UserProfile_get_isFriend_m6438_MethodInfo, __this);
		bool L_8 = L_7;
		Object_t * L_9 = Box(InitializedTypeInfo(&Boolean_t122_il2cpp_TypeInfo), &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4)) = (Object_t *)L_9;
		ObjectU5BU5D_t130* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_11 = L_10;
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&UserProfile_get_state_m6439_MethodInfo, __this);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(InitializedTypeInfo(&UserState_t1111_il2cpp_TypeInfo), &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6)) = (Object_t *)L_14;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_15 = String_Concat_m516(NULL /*static, unused*/, L_11, /*hidden argument*/&String_Concat_m516_MethodInfo);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern MethodInfo UserProfile_SetUserName_m6433_MethodInfo;
 void UserProfile_SetUserName_m6433 (UserProfile_t1094 * __this, String_t* ___name, MethodInfo* method){
	{
		__this->___m_UserName_0 = ___name;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern MethodInfo UserProfile_SetUserID_m6434_MethodInfo;
 void UserProfile_SetUserID_m6434 (UserProfile_t1094 * __this, String_t* ___id, MethodInfo* method){
	{
		__this->___m_ID_1 = ___id;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern MethodInfo UserProfile_SetImage_m6435_MethodInfo;
 void UserProfile_SetImage_m6435 (UserProfile_t1094 * __this, Texture2D_t196 * ___image, MethodInfo* method){
	{
		__this->___m_Image_4 = ___image;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
 String_t* UserProfile_get_userName_m6436 (UserProfile_t1094 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_UserName_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
 String_t* UserProfile_get_id_m6437 (UserProfile_t1094 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_ID_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
 bool UserProfile_get_isFriend_m6438 (UserProfile_t1094 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_IsFriend_2);
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
 int32_t UserProfile_get_state_m6439 (UserProfile_t1094 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_State_3);
		return L_0;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern Il2CppType String_t_0_0_4;
FieldInfo UserProfile_t1094____m_UserName_0_FieldInfo = 
{
	"m_UserName"/* name */
	, &String_t_0_0_4/* type */
	, &UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1094, ___m_UserName_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_4;
FieldInfo UserProfile_t1094____m_ID_1_FieldInfo = 
{
	"m_ID"/* name */
	, &String_t_0_0_4/* type */
	, &UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1094, ___m_ID_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_4;
FieldInfo UserProfile_t1094____m_IsFriend_2_FieldInfo = 
{
	"m_IsFriend"/* name */
	, &Boolean_t122_0_0_4/* type */
	, &UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1094, ___m_IsFriend_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1111_0_0_4;
FieldInfo UserProfile_t1094____m_State_3_FieldInfo = 
{
	"m_State"/* name */
	, &UserState_t1111_0_0_4/* type */
	, &UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1094, ___m_State_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Texture2D_t196_0_0_4;
FieldInfo UserProfile_t1094____m_Image_4_FieldInfo = 
{
	"m_Image"/* name */
	, &Texture2D_t196_0_0_4/* type */
	, &UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, offsetof(UserProfile_t1094, ___m_Image_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserProfile_t1094_FieldInfos[] =
{
	&UserProfile_t1094____m_UserName_0_FieldInfo,
	&UserProfile_t1094____m_ID_1_FieldInfo,
	&UserProfile_t1094____m_IsFriend_2_FieldInfo,
	&UserProfile_t1094____m_State_3_FieldInfo,
	&UserProfile_t1094____m_Image_4_FieldInfo,
	NULL
};
static PropertyInfo UserProfile_t1094____userName_PropertyInfo = 
{
	&UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, "userName"/* name */
	, &UserProfile_get_userName_m6436_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo UserProfile_t1094____id_PropertyInfo = 
{
	&UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &UserProfile_get_id_m6437_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo UserProfile_t1094____isFriend_PropertyInfo = 
{
	&UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, "isFriend"/* name */
	, &UserProfile_get_isFriend_m6438_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo UserProfile_t1094____state_PropertyInfo = 
{
	&UserProfile_t1094_il2cpp_TypeInfo/* parent */
	, "state"/* name */
	, &UserProfile_get_state_m6439_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* UserProfile_t1094_PropertyInfos[] =
{
	&UserProfile_t1094____userName_PropertyInfo,
	&UserProfile_t1094____id_PropertyInfo,
	&UserProfile_t1094____isFriend_PropertyInfo,
	&UserProfile_t1094____state_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
MethodInfo UserProfile__ctor_m6430_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m6430/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType UserState_t1111_0_0_0;
extern Il2CppType UserState_t1111_0_0_0;
extern Il2CppType Texture2D_t196_0_0_0;
extern Il2CppType Texture2D_t196_0_0_0;
static ParameterInfo UserProfile_t1094_UserProfile__ctor_m6431_ParameterInfos[] = 
{
	{"name", 0, 134219278, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"id", 1, 134219279, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"friend", 2, 134219280, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"state", 3, 134219281, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
	{"image", 4, 134219282, &EmptyCustomAttributesCache, &Texture2D_t196_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_SByte_t125_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
MethodInfo UserProfile__ctor_m6431_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m6431/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_SByte_t125_Int32_t123_Object_t/* invoker_method */
	, UserProfile_t1094_UserProfile__ctor_m6431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
MethodInfo UserProfile_ToString_m6432_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UserProfile_ToString_m6432/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UserProfile_t1094_UserProfile_SetUserName_m6433_ParameterInfos[] = 
{
	{"name", 0, 134219283, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
MethodInfo UserProfile_SetUserName_m6433_MethodInfo = 
{
	"SetUserName"/* name */
	, (methodPointerType)&UserProfile_SetUserName_m6433/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UserProfile_t1094_UserProfile_SetUserName_m6433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UserProfile_t1094_UserProfile_SetUserID_m6434_ParameterInfos[] = 
{
	{"id", 0, 134219284, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
MethodInfo UserProfile_SetUserID_m6434_MethodInfo = 
{
	"SetUserID"/* name */
	, (methodPointerType)&UserProfile_SetUserID_m6434/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UserProfile_t1094_UserProfile_SetUserID_m6434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Texture2D_t196_0_0_0;
static ParameterInfo UserProfile_t1094_UserProfile_SetImage_m6435_ParameterInfos[] = 
{
	{"image", 0, 134219285, &EmptyCustomAttributesCache, &Texture2D_t196_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
MethodInfo UserProfile_SetImage_m6435_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&UserProfile_SetImage_m6435/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UserProfile_t1094_UserProfile_SetImage_m6435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
MethodInfo UserProfile_get_userName_m6436_MethodInfo = 
{
	"get_userName"/* name */
	, (methodPointerType)&UserProfile_get_userName_m6436/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
MethodInfo UserProfile_get_id_m6437_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&UserProfile_get_id_m6437/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
MethodInfo UserProfile_get_isFriend_m6438_MethodInfo = 
{
	"get_isFriend"/* name */
	, (methodPointerType)&UserProfile_get_isFriend_m6438/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserState_t1111_0_0_0;
extern void* RuntimeInvoker_UserState_t1111 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
MethodInfo UserProfile_get_state_m6439_MethodInfo = 
{
	"get_state"/* name */
	, (methodPointerType)&UserProfile_get_state_m6439/* method */
	, &UserProfile_t1094_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1111_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserProfile_t1094_MethodInfos[] =
{
	&UserProfile__ctor_m6430_MethodInfo,
	&UserProfile__ctor_m6431_MethodInfo,
	&UserProfile_ToString_m6432_MethodInfo,
	&UserProfile_SetUserName_m6433_MethodInfo,
	&UserProfile_SetUserID_m6434_MethodInfo,
	&UserProfile_SetImage_m6435_MethodInfo,
	&UserProfile_get_userName_m6436_MethodInfo,
	&UserProfile_get_id_m6437_MethodInfo,
	&UserProfile_get_isFriend_m6438_MethodInfo,
	&UserProfile_get_state_m6439_MethodInfo,
	NULL
};
static MethodInfo* UserProfile_t1094_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UserProfile_ToString_m6432_MethodInfo,
	&UserProfile_get_userName_m6436_MethodInfo,
	&UserProfile_get_id_m6437_MethodInfo,
	&UserProfile_get_isFriend_m6438_MethodInfo,
	&UserProfile_get_state_m6439_MethodInfo,
};
static TypeInfo* UserProfile_t1094_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1162_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UserProfile_t1094_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1162_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserProfile_t1094_0_0_0;
extern Il2CppType UserProfile_t1094_1_0_0;
struct UserProfile_t1094;
TypeInfo UserProfile_t1094_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfile_t1094_MethodInfos/* methods */
	, UserProfile_t1094_PropertyInfos/* properties */
	, UserProfile_t1094_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserProfile_t1094_il2cpp_TypeInfo/* element_class */
	, UserProfile_t1094_InterfacesTypeInfos/* implemented_interfaces */
	, UserProfile_t1094_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UserProfile_t1094_il2cpp_TypeInfo/* cast_class */
	, &UserProfile_t1094_0_0_0/* byval_arg */
	, &UserProfile_t1094_1_0_0/* this_arg */
	, UserProfile_t1094_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t1094)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Achievement_t1096_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"

// System.Double
#include "mscorlib_System_Double.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
extern TypeInfo Void_t111_il2cpp_TypeInfo;
extern TypeInfo Double_t853_il2cpp_TypeInfo;
extern TypeInfo DateTime_t674_il2cpp_TypeInfo;
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
extern MethodInfo Achievement_set_id_m6445_MethodInfo;
extern MethodInfo Achievement_set_percentCompleted_m6447_MethodInfo;
extern MethodInfo Achievement__ctor_m6441_MethodInfo;
extern MethodInfo Achievement_get_id_m6444_MethodInfo;
extern MethodInfo Achievement_get_percentCompleted_m6446_MethodInfo;
extern MethodInfo Achievement_get_completed_m6448_MethodInfo;
extern MethodInfo Achievement_get_hidden_m6449_MethodInfo;
extern MethodInfo Achievement_get_lastReportedDate_m6450_MethodInfo;


// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern MethodInfo Achievement__ctor_m6440_MethodInfo;
 void Achievement__ctor_m6440 (Achievement_t1096 * __this, String_t* ___id, double ___percentCompleted, bool ___completed, bool ___hidden, DateTime_t674  ___lastReportedDate, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		VirtActionInvoker1< String_t* >::Invoke(&Achievement_set_id_m6445_MethodInfo, __this, ___id);
		VirtActionInvoker1< double >::Invoke(&Achievement_set_percentCompleted_m6447_MethodInfo, __this, ___percentCompleted);
		__this->___m_Completed_0 = ___completed;
		__this->___m_Hidden_1 = ___hidden;
		__this->___m_LastReportedDate_2 = ___lastReportedDate;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
 void Achievement__ctor_m6441 (Achievement_t1096 * __this, String_t* ___id, double ___percent, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		VirtActionInvoker1< String_t* >::Invoke(&Achievement_set_id_m6445_MethodInfo, __this, ___id);
		VirtActionInvoker1< double >::Invoke(&Achievement_set_percentCompleted_m6447_MethodInfo, __this, ___percent);
		__this->___m_Hidden_1 = 0;
		__this->___m_Completed_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&DateTime_t674_il2cpp_TypeInfo));
		__this->___m_LastReportedDate_2 = (((DateTime_t674_StaticFields*)InitializedTypeInfo(&DateTime_t674_il2cpp_TypeInfo)->static_fields)->___MinValue_3);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern MethodInfo Achievement__ctor_m6442_MethodInfo;
 void Achievement__ctor_m6442 (Achievement_t1096 * __this, MethodInfo* method){
	{
		Achievement__ctor_m6441(__this, (String_t*) &_stringLiteral415, (0.0), /*hidden argument*/&Achievement__ctor_m6441_MethodInfo);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern MethodInfo Achievement_ToString_m6443_MethodInfo;
 String_t* Achievement_ToString_m6443 (Achievement_t1096 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t130* L_0 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), ((int32_t)9)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Achievement_get_id_m6444_MethodInfo, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t130* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_3 = L_2;
		double L_4 = (double)VirtFuncInvoker0< double >::Invoke(&Achievement_get_percentCompleted_m6446_MethodInfo, __this);
		double L_5 = L_4;
		Object_t * L_6 = Box(InitializedTypeInfo(&Double_t853_il2cpp_TypeInfo), &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2)) = (Object_t *)L_6;
		ObjectU5BU5D_t130* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_8 = L_7;
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(&Achievement_get_completed_m6448_MethodInfo, __this);
		bool L_10 = L_9;
		Object_t * L_11 = Box(InitializedTypeInfo(&Boolean_t122_il2cpp_TypeInfo), &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 4)) = (Object_t *)L_11;
		ObjectU5BU5D_t130* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_13 = L_12;
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(&Achievement_get_hidden_m6449_MethodInfo, __this);
		bool L_15 = L_14;
		Object_t * L_16 = Box(InitializedTypeInfo(&Boolean_t122_il2cpp_TypeInfo), &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6)) = (Object_t *)L_16;
		ObjectU5BU5D_t130* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_18 = L_17;
		DateTime_t674  L_19 = (DateTime_t674 )VirtFuncInvoker0< DateTime_t674  >::Invoke(&Achievement_get_lastReportedDate_m6450_MethodInfo, __this);
		DateTime_t674  L_20 = L_19;
		Object_t * L_21 = Box(InitializedTypeInfo(&DateTime_t674_il2cpp_TypeInfo), &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 8)) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_22 = String_Concat_m516(NULL /*static, unused*/, L_18, /*hidden argument*/&String_Concat_m516_MethodInfo);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
 String_t* Achievement_get_id_m6444 (Achievement_t1096 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
 void Achievement_set_id_m6445 (Achievement_t1096 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___U3CidU3Ek__BackingField_3 = ___value;
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
 double Achievement_get_percentCompleted_m6446 (Achievement_t1096 * __this, MethodInfo* method){
	{
		double L_0 = (__this->___U3CpercentCompletedU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
 void Achievement_set_percentCompleted_m6447 (Achievement_t1096 * __this, double ___value, MethodInfo* method){
	{
		__this->___U3CpercentCompletedU3Ek__BackingField_4 = ___value;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
 bool Achievement_get_completed_m6448 (Achievement_t1096 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_Completed_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
 bool Achievement_get_hidden_m6449 (Achievement_t1096 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_Hidden_1);
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
 DateTime_t674  Achievement_get_lastReportedDate_m6450 (Achievement_t1096 * __this, MethodInfo* method){
	{
		DateTime_t674  L_0 = (__this->___m_LastReportedDate_2);
		return L_0;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo Achievement_t1096____m_Completed_0_FieldInfo = 
{
	"m_Completed"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &Achievement_t1096_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1096, ___m_Completed_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo Achievement_t1096____m_Hidden_1_FieldInfo = 
{
	"m_Hidden"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &Achievement_t1096_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1096, ___m_Hidden_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DateTime_t674_0_0_1;
FieldInfo Achievement_t1096____m_LastReportedDate_2_FieldInfo = 
{
	"m_LastReportedDate"/* name */
	, &DateTime_t674_0_0_1/* type */
	, &Achievement_t1096_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1096, ___m_LastReportedDate_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_U3CidU3Ek__BackingField;
FieldInfo Achievement_t1096____U3CidU3Ek__BackingField_3_FieldInfo = 
{
	"<id>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &Achievement_t1096_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1096, ___U3CidU3Ek__BackingField_3)/* data */
	, &Achievement_t1096__CustomAttributeCache_U3CidU3Ek__BackingField/* custom_attributes_cache */

};
extern Il2CppType Double_t853_0_0_1;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_U3CpercentCompletedU3Ek__BackingField;
FieldInfo Achievement_t1096____U3CpercentCompletedU3Ek__BackingField_4_FieldInfo = 
{
	"<percentCompleted>k__BackingField"/* name */
	, &Double_t853_0_0_1/* type */
	, &Achievement_t1096_il2cpp_TypeInfo/* parent */
	, offsetof(Achievement_t1096, ___U3CpercentCompletedU3Ek__BackingField_4)/* data */
	, &Achievement_t1096__CustomAttributeCache_U3CpercentCompletedU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* Achievement_t1096_FieldInfos[] =
{
	&Achievement_t1096____m_Completed_0_FieldInfo,
	&Achievement_t1096____m_Hidden_1_FieldInfo,
	&Achievement_t1096____m_LastReportedDate_2_FieldInfo,
	&Achievement_t1096____U3CidU3Ek__BackingField_3_FieldInfo,
	&Achievement_t1096____U3CpercentCompletedU3Ek__BackingField_4_FieldInfo,
	NULL
};
static PropertyInfo Achievement_t1096____id_PropertyInfo = 
{
	&Achievement_t1096_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Achievement_get_id_m6444_MethodInfo/* get */
	, &Achievement_set_id_m6445_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Achievement_t1096____percentCompleted_PropertyInfo = 
{
	&Achievement_t1096_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &Achievement_get_percentCompleted_m6446_MethodInfo/* get */
	, &Achievement_set_percentCompleted_m6447_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Achievement_t1096____completed_PropertyInfo = 
{
	&Achievement_t1096_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &Achievement_get_completed_m6448_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Achievement_t1096____hidden_PropertyInfo = 
{
	&Achievement_t1096_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &Achievement_get_hidden_m6449_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Achievement_t1096____lastReportedDate_PropertyInfo = 
{
	&Achievement_t1096_il2cpp_TypeInfo/* parent */
	, "lastReportedDate"/* name */
	, &Achievement_get_lastReportedDate_m6450_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Achievement_t1096_PropertyInfos[] =
{
	&Achievement_t1096____id_PropertyInfo,
	&Achievement_t1096____percentCompleted_PropertyInfo,
	&Achievement_t1096____completed_PropertyInfo,
	&Achievement_t1096____hidden_PropertyInfo,
	&Achievement_t1096____lastReportedDate_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t853_0_0_0;
extern Il2CppType Double_t853_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType DateTime_t674_0_0_0;
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo Achievement_t1096_Achievement__ctor_m6440_ParameterInfos[] = 
{
	{"id", 0, 134219286, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"percentCompleted", 1, 134219287, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
	{"completed", 2, 134219288, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"hidden", 3, 134219289, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"lastReportedDate", 4, 134219290, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Double_t853_SByte_t125_SByte_t125_DateTime_t674 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
MethodInfo Achievement__ctor_m6440_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6440/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Double_t853_SByte_t125_SByte_t125_DateTime_t674/* invoker_method */
	, Achievement_t1096_Achievement__ctor_m6440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo Achievement_t1096_Achievement__ctor_m6441_ParameterInfos[] = 
{
	{"id", 0, 134219291, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"percent", 1, 134219292, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Double_t853 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
MethodInfo Achievement__ctor_m6441_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6441/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Double_t853/* invoker_method */
	, Achievement_t1096_Achievement__ctor_m6441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
MethodInfo Achievement__ctor_m6442_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6442/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
MethodInfo Achievement_ToString_m6443_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Achievement_ToString_m6443/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_get_id_m6444;
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
MethodInfo Achievement_get_id_m6444_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Achievement_get_id_m6444/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &Achievement_t1096__CustomAttributeCache_Achievement_get_id_m6444/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Achievement_t1096_Achievement_set_id_m6445_ParameterInfos[] = 
{
	{"value", 0, 134219293, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_set_id_m6445;
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
MethodInfo Achievement_set_id_m6445_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Achievement_set_id_m6445/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Achievement_t1096_Achievement_set_id_m6445_ParameterInfos/* parameters */
	, &Achievement_t1096__CustomAttributeCache_Achievement_set_id_m6445/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Double_t853_0_0_0;
extern void* RuntimeInvoker_Double_t853 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_get_percentCompleted_m6446;
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
MethodInfo Achievement_get_percentCompleted_m6446_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, (methodPointerType)&Achievement_get_percentCompleted_m6446/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Double_t853_0_0_0/* return_type */
	, RuntimeInvoker_Double_t853/* invoker_method */
	, NULL/* parameters */
	, &Achievement_t1096__CustomAttributeCache_Achievement_get_percentCompleted_m6446/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo Achievement_t1096_Achievement_set_percentCompleted_m6447_ParameterInfos[] = 
{
	{"value", 0, 134219294, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Double_t853 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_set_percentCompleted_m6447;
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
MethodInfo Achievement_set_percentCompleted_m6447_MethodInfo = 
{
	"set_percentCompleted"/* name */
	, (methodPointerType)&Achievement_set_percentCompleted_m6447/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Double_t853/* invoker_method */
	, Achievement_t1096_Achievement_set_percentCompleted_m6447_ParameterInfos/* parameters */
	, &Achievement_t1096__CustomAttributeCache_Achievement_set_percentCompleted_m6447/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
MethodInfo Achievement_get_completed_m6448_MethodInfo = 
{
	"get_completed"/* name */
	, (methodPointerType)&Achievement_get_completed_m6448/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
MethodInfo Achievement_get_hidden_m6449_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&Achievement_get_hidden_m6449/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DateTime_t674_0_0_0;
extern void* RuntimeInvoker_DateTime_t674 (MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
MethodInfo Achievement_get_lastReportedDate_m6450_MethodInfo = 
{
	"get_lastReportedDate"/* name */
	, (methodPointerType)&Achievement_get_lastReportedDate_m6450/* method */
	, &Achievement_t1096_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t674_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t674/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Achievement_t1096_MethodInfos[] =
{
	&Achievement__ctor_m6440_MethodInfo,
	&Achievement__ctor_m6441_MethodInfo,
	&Achievement__ctor_m6442_MethodInfo,
	&Achievement_ToString_m6443_MethodInfo,
	&Achievement_get_id_m6444_MethodInfo,
	&Achievement_set_id_m6445_MethodInfo,
	&Achievement_get_percentCompleted_m6446_MethodInfo,
	&Achievement_set_percentCompleted_m6447_MethodInfo,
	&Achievement_get_completed_m6448_MethodInfo,
	&Achievement_get_hidden_m6449_MethodInfo,
	&Achievement_get_lastReportedDate_m6450_MethodInfo,
	NULL
};
static MethodInfo* Achievement_t1096_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Achievement_ToString_m6443_MethodInfo,
	&Achievement_get_id_m6444_MethodInfo,
	&Achievement_set_id_m6445_MethodInfo,
	&Achievement_get_percentCompleted_m6446_MethodInfo,
	&Achievement_set_percentCompleted_m6447_MethodInfo,
	&Achievement_get_completed_m6448_MethodInfo,
	&Achievement_get_hidden_m6449_MethodInfo,
	&Achievement_get_lastReportedDate_m6450_MethodInfo,
};
extern TypeInfo IAchievement_t978_il2cpp_TypeInfo;
static TypeInfo* Achievement_t1096_InterfacesTypeInfos[] = 
{
	&IAchievement_t978_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Achievement_t1096_InterfacesOffsets[] = 
{
	{ &IAchievement_t978_il2cpp_TypeInfo, 4},
};
extern TypeInfo CompilerGeneratedAttribute_t202_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern MethodInfo CompilerGeneratedAttribute__ctor_m701_MethodInfo;
void Achievement_t1096_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Achievement_t1096_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Achievement_t1096_CustomAttributesCacheGenerator_Achievement_get_id_m6444(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Achievement_t1096_CustomAttributesCacheGenerator_Achievement_set_id_m6445(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Achievement_t1096_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m6446(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Achievement_t1096_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m6447(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache Achievement_t1096__CustomAttributeCache_U3CidU3Ek__BackingField = {
1,
NULL,
&Achievement_t1096_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField
};
CustomAttributesCache Achievement_t1096__CustomAttributeCache_U3CpercentCompletedU3Ek__BackingField = {
1,
NULL,
&Achievement_t1096_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField
};
CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_get_id_m6444 = {
1,
NULL,
&Achievement_t1096_CustomAttributesCacheGenerator_Achievement_get_id_m6444
};
CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_set_id_m6445 = {
1,
NULL,
&Achievement_t1096_CustomAttributesCacheGenerator_Achievement_set_id_m6445
};
CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_get_percentCompleted_m6446 = {
1,
NULL,
&Achievement_t1096_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m6446
};
CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_set_percentCompleted_m6447 = {
1,
NULL,
&Achievement_t1096_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m6447
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Achievement_t1096_0_0_0;
extern Il2CppType Achievement_t1096_1_0_0;
struct Achievement_t1096;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_U3CidU3Ek__BackingField;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_U3CpercentCompletedU3Ek__BackingField;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_get_id_m6444;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_set_id_m6445;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_get_percentCompleted_m6446;
extern CustomAttributesCache Achievement_t1096__CustomAttributeCache_Achievement_set_percentCompleted_m6447;
TypeInfo Achievement_t1096_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Achievement_t1096_MethodInfos/* methods */
	, Achievement_t1096_PropertyInfos/* properties */
	, Achievement_t1096_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Achievement_t1096_il2cpp_TypeInfo/* element_class */
	, Achievement_t1096_InterfacesTypeInfos/* implemented_interfaces */
	, Achievement_t1096_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Achievement_t1096_il2cpp_TypeInfo/* cast_class */
	, &Achievement_t1096_0_0_0/* byval_arg */
	, &Achievement_t1096_1_0_0/* this_arg */
	, Achievement_t1096_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t1096)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo AchievementDescription_t1095_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"

extern MethodInfo AchievementDescription_set_id_m6455_MethodInfo;
extern MethodInfo AchievementDescription_get_id_m6454_MethodInfo;
extern MethodInfo AchievementDescription_get_title_m6456_MethodInfo;
extern MethodInfo AchievementDescription_get_achievedDescription_m6457_MethodInfo;
extern MethodInfo AchievementDescription_get_unachievedDescription_m6458_MethodInfo;
extern MethodInfo AchievementDescription_get_points_m6460_MethodInfo;
extern MethodInfo AchievementDescription_get_hidden_m6459_MethodInfo;


// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern MethodInfo AchievementDescription__ctor_m6451_MethodInfo;
 void AchievementDescription__ctor_m6451 (AchievementDescription_t1095 * __this, String_t* ___id, String_t* ___title, Texture2D_t196 * ___image, String_t* ___achievedDescription, String_t* ___unachievedDescription, bool ___hidden, int32_t ___points, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		VirtActionInvoker1< String_t* >::Invoke(&AchievementDescription_set_id_m6455_MethodInfo, __this, ___id);
		__this->___m_Title_0 = ___title;
		__this->___m_Image_1 = ___image;
		__this->___m_AchievedDescription_2 = ___achievedDescription;
		__this->___m_UnachievedDescription_3 = ___unachievedDescription;
		__this->___m_Hidden_4 = ___hidden;
		__this->___m_Points_5 = ___points;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern MethodInfo AchievementDescription_ToString_m6452_MethodInfo;
 String_t* AchievementDescription_ToString_m6452 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t130* L_0 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), ((int32_t)11)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&AchievementDescription_get_id_m6454_MethodInfo, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t130* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&AchievementDescription_get_title_m6456_MethodInfo, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2)) = (Object_t *)L_4;
		ObjectU5BU5D_t130* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_6 = L_5;
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&AchievementDescription_get_achievedDescription_m6457_MethodInfo, __this);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4)) = (Object_t *)L_7;
		ObjectU5BU5D_t130* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 5)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_9 = L_8;
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&AchievementDescription_get_unachievedDescription_m6458_MethodInfo, __this);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 6)) = (Object_t *)L_10;
		ObjectU5BU5D_t130* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 7)) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_12 = L_11;
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&AchievementDescription_get_points_m6460_MethodInfo, __this);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 8)) = (Object_t *)L_15;
		ObjectU5BU5D_t130* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, (String_t*) &_stringLiteral49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, ((int32_t)9))) = (Object_t *)(String_t*) &_stringLiteral49;
		ObjectU5BU5D_t130* L_17 = L_16;
		bool L_18 = (bool)VirtFuncInvoker0< bool >::Invoke(&AchievementDescription_get_hidden_m6459_MethodInfo, __this);
		bool L_19 = L_18;
		Object_t * L_20 = Box(InitializedTypeInfo(&Boolean_t122_il2cpp_TypeInfo), &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)10))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_21 = String_Concat_m516(NULL /*static, unused*/, L_17, /*hidden argument*/&String_Concat_m516_MethodInfo);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern MethodInfo AchievementDescription_SetImage_m6453_MethodInfo;
 void AchievementDescription_SetImage_m6453 (AchievementDescription_t1095 * __this, Texture2D_t196 * ___image, MethodInfo* method){
	{
		__this->___m_Image_1 = ___image;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
 String_t* AchievementDescription_get_id_m6454 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
 void AchievementDescription_set_id_m6455 (AchievementDescription_t1095 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___U3CidU3Ek__BackingField_6 = ___value;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
 String_t* AchievementDescription_get_title_m6456 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_Title_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
 String_t* AchievementDescription_get_achievedDescription_m6457 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_AchievedDescription_2);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
 String_t* AchievementDescription_get_unachievedDescription_m6458 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_UnachievedDescription_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
 bool AchievementDescription_get_hidden_m6459 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_Hidden_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
 int32_t AchievementDescription_get_points_m6460 (AchievementDescription_t1095 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_Points_5);
		return L_0;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1095____m_Title_0_FieldInfo = 
{
	"m_Title"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___m_Title_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Texture2D_t196_0_0_1;
FieldInfo AchievementDescription_t1095____m_Image_1_FieldInfo = 
{
	"m_Image"/* name */
	, &Texture2D_t196_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___m_Image_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1095____m_AchievedDescription_2_FieldInfo = 
{
	"m_AchievedDescription"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___m_AchievedDescription_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo AchievementDescription_t1095____m_UnachievedDescription_3_FieldInfo = 
{
	"m_UnachievedDescription"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___m_UnachievedDescription_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo AchievementDescription_t1095____m_Hidden_4_FieldInfo = 
{
	"m_Hidden"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___m_Hidden_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo AchievementDescription_t1095____m_Points_5_FieldInfo = 
{
	"m_Points"/* name */
	, &Int32_t123_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___m_Points_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_U3CidU3Ek__BackingField;
FieldInfo AchievementDescription_t1095____U3CidU3Ek__BackingField_6_FieldInfo = 
{
	"<id>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, offsetof(AchievementDescription_t1095, ___U3CidU3Ek__BackingField_6)/* data */
	, &AchievementDescription_t1095__CustomAttributeCache_U3CidU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* AchievementDescription_t1095_FieldInfos[] =
{
	&AchievementDescription_t1095____m_Title_0_FieldInfo,
	&AchievementDescription_t1095____m_Image_1_FieldInfo,
	&AchievementDescription_t1095____m_AchievedDescription_2_FieldInfo,
	&AchievementDescription_t1095____m_UnachievedDescription_3_FieldInfo,
	&AchievementDescription_t1095____m_Hidden_4_FieldInfo,
	&AchievementDescription_t1095____m_Points_5_FieldInfo,
	&AchievementDescription_t1095____U3CidU3Ek__BackingField_6_FieldInfo,
	NULL
};
static PropertyInfo AchievementDescription_t1095____id_PropertyInfo = 
{
	&AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &AchievementDescription_get_id_m6454_MethodInfo/* get */
	, &AchievementDescription_set_id_m6455_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo AchievementDescription_t1095____title_PropertyInfo = 
{
	&AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, "title"/* name */
	, &AchievementDescription_get_title_m6456_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo AchievementDescription_t1095____achievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, "achievedDescription"/* name */
	, &AchievementDescription_get_achievedDescription_m6457_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo AchievementDescription_t1095____unachievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, "unachievedDescription"/* name */
	, &AchievementDescription_get_unachievedDescription_m6458_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo AchievementDescription_t1095____hidden_PropertyInfo = 
{
	&AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &AchievementDescription_get_hidden_m6459_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo AchievementDescription_t1095____points_PropertyInfo = 
{
	&AchievementDescription_t1095_il2cpp_TypeInfo/* parent */
	, "points"/* name */
	, &AchievementDescription_get_points_m6460_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* AchievementDescription_t1095_PropertyInfos[] =
{
	&AchievementDescription_t1095____id_PropertyInfo,
	&AchievementDescription_t1095____title_PropertyInfo,
	&AchievementDescription_t1095____achievedDescription_PropertyInfo,
	&AchievementDescription_t1095____unachievedDescription_PropertyInfo,
	&AchievementDescription_t1095____hidden_PropertyInfo,
	&AchievementDescription_t1095____points_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Texture2D_t196_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo AchievementDescription_t1095_AchievementDescription__ctor_m6451_ParameterInfos[] = 
{
	{"id", 0, 134219295, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"title", 1, 134219296, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"image", 2, 134219297, &EmptyCustomAttributesCache, &Texture2D_t196_0_0_0},
	{"achievedDescription", 3, 134219298, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"unachievedDescription", 4, 134219299, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"hidden", 5, 134219300, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"points", 6, 134219301, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t125_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
MethodInfo AchievementDescription__ctor_m6451_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AchievementDescription__ctor_m6451/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t125_Int32_t123/* invoker_method */
	, AchievementDescription_t1095_AchievementDescription__ctor_m6451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
MethodInfo AchievementDescription_ToString_m6452_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AchievementDescription_ToString_m6452/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Texture2D_t196_0_0_0;
static ParameterInfo AchievementDescription_t1095_AchievementDescription_SetImage_m6453_ParameterInfos[] = 
{
	{"image", 0, 134219302, &EmptyCustomAttributesCache, &Texture2D_t196_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
MethodInfo AchievementDescription_SetImage_m6453_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&AchievementDescription_SetImage_m6453/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, AchievementDescription_t1095_AchievementDescription_SetImage_m6453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_get_id_m6454;
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
MethodInfo AchievementDescription_get_id_m6454_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&AchievementDescription_get_id_m6454/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_get_id_m6454/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo AchievementDescription_t1095_AchievementDescription_set_id_m6455_ParameterInfos[] = 
{
	{"value", 0, 134219303, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_set_id_m6455;
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
MethodInfo AchievementDescription_set_id_m6455_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&AchievementDescription_set_id_m6455/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, AchievementDescription_t1095_AchievementDescription_set_id_m6455_ParameterInfos/* parameters */
	, &AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_set_id_m6455/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
MethodInfo AchievementDescription_get_title_m6456_MethodInfo = 
{
	"get_title"/* name */
	, (methodPointerType)&AchievementDescription_get_title_m6456/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
MethodInfo AchievementDescription_get_achievedDescription_m6457_MethodInfo = 
{
	"get_achievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_achievedDescription_m6457/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
MethodInfo AchievementDescription_get_unachievedDescription_m6458_MethodInfo = 
{
	"get_unachievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_unachievedDescription_m6458/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
MethodInfo AchievementDescription_get_hidden_m6459_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&AchievementDescription_get_hidden_m6459/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
MethodInfo AchievementDescription_get_points_m6460_MethodInfo = 
{
	"get_points"/* name */
	, (methodPointerType)&AchievementDescription_get_points_m6460/* method */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AchievementDescription_t1095_MethodInfos[] =
{
	&AchievementDescription__ctor_m6451_MethodInfo,
	&AchievementDescription_ToString_m6452_MethodInfo,
	&AchievementDescription_SetImage_m6453_MethodInfo,
	&AchievementDescription_get_id_m6454_MethodInfo,
	&AchievementDescription_set_id_m6455_MethodInfo,
	&AchievementDescription_get_title_m6456_MethodInfo,
	&AchievementDescription_get_achievedDescription_m6457_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m6458_MethodInfo,
	&AchievementDescription_get_hidden_m6459_MethodInfo,
	&AchievementDescription_get_points_m6460_MethodInfo,
	NULL
};
static MethodInfo* AchievementDescription_t1095_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&AchievementDescription_ToString_m6452_MethodInfo,
	&AchievementDescription_get_id_m6454_MethodInfo,
	&AchievementDescription_set_id_m6455_MethodInfo,
	&AchievementDescription_get_title_m6456_MethodInfo,
	&AchievementDescription_get_achievedDescription_m6457_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m6458_MethodInfo,
	&AchievementDescription_get_hidden_m6459_MethodInfo,
	&AchievementDescription_get_points_m6460_MethodInfo,
};
extern TypeInfo IAchievementDescription_t1157_il2cpp_TypeInfo;
static TypeInfo* AchievementDescription_t1095_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t1157_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t1095_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t1157_il2cpp_TypeInfo, 4},
};
void AchievementDescription_t1095_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void AchievementDescription_t1095_CustomAttributesCacheGenerator_AchievementDescription_get_id_m6454(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void AchievementDescription_t1095_CustomAttributesCacheGenerator_AchievementDescription_set_id_m6455(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_U3CidU3Ek__BackingField = {
1,
NULL,
&AchievementDescription_t1095_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField
};
CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_get_id_m6454 = {
1,
NULL,
&AchievementDescription_t1095_CustomAttributesCacheGenerator_AchievementDescription_get_id_m6454
};
CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_set_id_m6455 = {
1,
NULL,
&AchievementDescription_t1095_CustomAttributesCacheGenerator_AchievementDescription_set_id_m6455
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType AchievementDescription_t1095_0_0_0;
extern Il2CppType AchievementDescription_t1095_1_0_0;
struct AchievementDescription_t1095;
extern CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_U3CidU3Ek__BackingField;
extern CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_get_id_m6454;
extern CustomAttributesCache AchievementDescription_t1095__CustomAttributeCache_AchievementDescription_set_id_m6455;
TypeInfo AchievementDescription_t1095_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescription_t1095_MethodInfos/* methods */
	, AchievementDescription_t1095_PropertyInfos/* properties */
	, AchievementDescription_t1095_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* element_class */
	, AchievementDescription_t1095_InterfacesTypeInfos/* implemented_interfaces */
	, AchievementDescription_t1095_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &AchievementDescription_t1095_il2cpp_TypeInfo/* cast_class */
	, &AchievementDescription_t1095_0_0_0/* byval_arg */
	, &AchievementDescription_t1095_1_0_0/* this_arg */
	, AchievementDescription_t1095_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t1095)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Score_t1097_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"

// System.Int64
#include "mscorlib_System_Int64.h"
extern TypeInfo Int64_t1163_il2cpp_TypeInfo;
extern MethodInfo DateTime_get_Now_m4978_MethodInfo;
extern MethodInfo Score__ctor_m6462_MethodInfo;
extern MethodInfo Score_set_leaderboardID_m6465_MethodInfo;
extern MethodInfo Score_set_value_m6467_MethodInfo;
extern MethodInfo Score_get_value_m6466_MethodInfo;
extern MethodInfo Score_get_leaderboardID_m6464_MethodInfo;


// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern MethodInfo Score__ctor_m6461_MethodInfo;
 void Score__ctor_m6461 (Score_t1097 * __this, String_t* ___leaderboardID, int64_t ___value, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&DateTime_t674_il2cpp_TypeInfo));
		DateTime_t674  L_0 = DateTime_get_Now_m4978(NULL /*static, unused*/, /*hidden argument*/&DateTime_get_Now_m4978_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		Score__ctor_m6462(__this, ___leaderboardID, ___value, (String_t*) &_stringLiteral324, L_0, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), (-1), /*hidden argument*/&Score__ctor_m6462_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
 void Score__ctor_m6462 (Score_t1097 * __this, String_t* ___leaderboardID, int64_t ___value, String_t* ___userID, DateTime_t674  ___date, String_t* ___formattedValue, int32_t ___rank, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		VirtActionInvoker1< String_t* >::Invoke(&Score_set_leaderboardID_m6465_MethodInfo, __this, ___leaderboardID);
		VirtActionInvoker1< int64_t >::Invoke(&Score_set_value_m6467_MethodInfo, __this, ___value);
		__this->___m_UserID_2 = ___userID;
		__this->___m_Date_0 = ___date;
		__this->___m_FormattedValue_1 = ___formattedValue;
		__this->___m_Rank_3 = ___rank;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern MethodInfo Score_ToString_m6463_MethodInfo;
 String_t* Score_ToString_m6463 (Score_t1097 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t130* L_0 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), ((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral416);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)(String_t*) &_stringLiteral416;
		ObjectU5BU5D_t130* L_1 = L_0;
		int32_t L_2 = (__this->___m_Rank_3);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1)) = (Object_t *)L_4;
		ObjectU5BU5D_t130* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, (String_t*) &_stringLiteral417);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2)) = (Object_t *)(String_t*) &_stringLiteral417;
		ObjectU5BU5D_t130* L_6 = L_5;
		int64_t L_7 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(&Score_get_value_m6466_MethodInfo, __this);
		int64_t L_8 = L_7;
		Object_t * L_9 = Box(InitializedTypeInfo(&Int64_t1163_il2cpp_TypeInfo), &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3)) = (Object_t *)L_9;
		ObjectU5BU5D_t130* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, (String_t*) &_stringLiteral418);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4)) = (Object_t *)(String_t*) &_stringLiteral418;
		ObjectU5BU5D_t130* L_11 = L_10;
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Score_get_leaderboardID_m6464_MethodInfo, __this);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5)) = (Object_t *)L_12;
		ObjectU5BU5D_t130* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, (String_t*) &_stringLiteral419);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6)) = (Object_t *)(String_t*) &_stringLiteral419;
		ObjectU5BU5D_t130* L_14 = L_13;
		String_t* L_15 = (__this->___m_UserID_2);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 7)) = (Object_t *)L_15;
		ObjectU5BU5D_t130* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, (String_t*) &_stringLiteral420);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 8)) = (Object_t *)(String_t*) &_stringLiteral420;
		ObjectU5BU5D_t130* L_17 = L_16;
		DateTime_t674  L_18 = (__this->___m_Date_0);
		DateTime_t674  L_19 = L_18;
		Object_t * L_20 = Box(InitializedTypeInfo(&DateTime_t674_il2cpp_TypeInfo), &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)9))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_21 = String_Concat_m516(NULL /*static, unused*/, L_17, /*hidden argument*/&String_Concat_m516_MethodInfo);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
 String_t* Score_get_leaderboardID_m6464 (Score_t1097 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___U3CleaderboardIDU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
 void Score_set_leaderboardID_m6465 (Score_t1097 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___U3CleaderboardIDU3Ek__BackingField_4 = ___value;
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
 int64_t Score_get_value_m6466 (Score_t1097 * __this, MethodInfo* method){
	{
		int64_t L_0 = (__this->___U3CvalueU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
 void Score_set_value_m6467 (Score_t1097 * __this, int64_t ___value, MethodInfo* method){
	{
		__this->___U3CvalueU3Ek__BackingField_5 = ___value;
		return;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern Il2CppType DateTime_t674_0_0_1;
FieldInfo Score_t1097____m_Date_0_FieldInfo = 
{
	"m_Date"/* name */
	, &DateTime_t674_0_0_1/* type */
	, &Score_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1097, ___m_Date_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Score_t1097____m_FormattedValue_1_FieldInfo = 
{
	"m_FormattedValue"/* name */
	, &String_t_0_0_1/* type */
	, &Score_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1097, ___m_FormattedValue_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Score_t1097____m_UserID_2_FieldInfo = 
{
	"m_UserID"/* name */
	, &String_t_0_0_1/* type */
	, &Score_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1097, ___m_UserID_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Score_t1097____m_Rank_3_FieldInfo = 
{
	"m_Rank"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Score_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1097, ___m_Rank_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_U3CleaderboardIDU3Ek__BackingField;
FieldInfo Score_t1097____U3CleaderboardIDU3Ek__BackingField_4_FieldInfo = 
{
	"<leaderboardID>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &Score_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1097, ___U3CleaderboardIDU3Ek__BackingField_4)/* data */
	, &Score_t1097__CustomAttributeCache_U3CleaderboardIDU3Ek__BackingField/* custom_attributes_cache */

};
extern Il2CppType Int64_t1163_0_0_1;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_U3CvalueU3Ek__BackingField;
FieldInfo Score_t1097____U3CvalueU3Ek__BackingField_5_FieldInfo = 
{
	"<value>k__BackingField"/* name */
	, &Int64_t1163_0_0_1/* type */
	, &Score_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(Score_t1097, ___U3CvalueU3Ek__BackingField_5)/* data */
	, &Score_t1097__CustomAttributeCache_U3CvalueU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* Score_t1097_FieldInfos[] =
{
	&Score_t1097____m_Date_0_FieldInfo,
	&Score_t1097____m_FormattedValue_1_FieldInfo,
	&Score_t1097____m_UserID_2_FieldInfo,
	&Score_t1097____m_Rank_3_FieldInfo,
	&Score_t1097____U3CleaderboardIDU3Ek__BackingField_4_FieldInfo,
	&Score_t1097____U3CvalueU3Ek__BackingField_5_FieldInfo,
	NULL
};
static PropertyInfo Score_t1097____leaderboardID_PropertyInfo = 
{
	&Score_t1097_il2cpp_TypeInfo/* parent */
	, "leaderboardID"/* name */
	, &Score_get_leaderboardID_m6464_MethodInfo/* get */
	, &Score_set_leaderboardID_m6465_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Score_t1097____value_PropertyInfo = 
{
	&Score_t1097_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Score_get_value_m6466_MethodInfo/* get */
	, &Score_set_value_m6467_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Score_t1097_PropertyInfos[] =
{
	&Score_t1097____leaderboardID_PropertyInfo,
	&Score_t1097____value_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int64_t1163_0_0_0;
extern Il2CppType Int64_t1163_0_0_0;
static ParameterInfo Score_t1097_Score__ctor_m6461_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219304, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"value", 1, 134219305, &EmptyCustomAttributesCache, &Int64_t1163_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int64_t1163 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
MethodInfo Score__ctor_m6461_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m6461/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int64_t1163/* invoker_method */
	, Score_t1097_Score__ctor_m6461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int64_t1163_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType DateTime_t674_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Score_t1097_Score__ctor_m6462_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219306, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"value", 1, 134219307, &EmptyCustomAttributesCache, &Int64_t1163_0_0_0},
	{"userID", 2, 134219308, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"date", 3, 134219309, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
	{"formattedValue", 4, 134219310, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"rank", 5, 134219311, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int64_t1163_Object_t_DateTime_t674_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
MethodInfo Score__ctor_m6462_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m6462/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int64_t1163_Object_t_DateTime_t674_Object_t_Int32_t123/* invoker_method */
	, Score_t1097_Score__ctor_m6462_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
MethodInfo Score_ToString_m6463_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Score_ToString_m6463/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_get_leaderboardID_m6464;
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
MethodInfo Score_get_leaderboardID_m6464_MethodInfo = 
{
	"get_leaderboardID"/* name */
	, (methodPointerType)&Score_get_leaderboardID_m6464/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &Score_t1097__CustomAttributeCache_Score_get_leaderboardID_m6464/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Score_t1097_Score_set_leaderboardID_m6465_ParameterInfos[] = 
{
	{"value", 0, 134219312, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_set_leaderboardID_m6465;
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
MethodInfo Score_set_leaderboardID_m6465_MethodInfo = 
{
	"set_leaderboardID"/* name */
	, (methodPointerType)&Score_set_leaderboardID_m6465/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Score_t1097_Score_set_leaderboardID_m6465_ParameterInfos/* parameters */
	, &Score_t1097__CustomAttributeCache_Score_set_leaderboardID_m6465/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t1163_0_0_0;
extern void* RuntimeInvoker_Int64_t1163 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_get_value_m6466;
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
MethodInfo Score_get_value_m6466_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Score_get_value_m6466/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1163_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1163/* invoker_method */
	, NULL/* parameters */
	, &Score_t1097__CustomAttributeCache_Score_get_value_m6466/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t1163_0_0_0;
static ParameterInfo Score_t1097_Score_set_value_m6467_ParameterInfos[] = 
{
	{"value", 0, 134219313, &EmptyCustomAttributesCache, &Int64_t1163_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int64_t1163 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_set_value_m6467;
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
MethodInfo Score_set_value_m6467_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Score_set_value_m6467/* method */
	, &Score_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int64_t1163/* invoker_method */
	, Score_t1097_Score_set_value_m6467_ParameterInfos/* parameters */
	, &Score_t1097__CustomAttributeCache_Score_set_value_m6467/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Score_t1097_MethodInfos[] =
{
	&Score__ctor_m6461_MethodInfo,
	&Score__ctor_m6462_MethodInfo,
	&Score_ToString_m6463_MethodInfo,
	&Score_get_leaderboardID_m6464_MethodInfo,
	&Score_set_leaderboardID_m6465_MethodInfo,
	&Score_get_value_m6466_MethodInfo,
	&Score_set_value_m6467_MethodInfo,
	NULL
};
static MethodInfo* Score_t1097_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Score_ToString_m6463_MethodInfo,
	&Score_get_leaderboardID_m6464_MethodInfo,
	&Score_set_leaderboardID_m6465_MethodInfo,
	&Score_get_value_m6466_MethodInfo,
	&Score_set_value_m6467_MethodInfo,
};
extern TypeInfo IScore_t1105_il2cpp_TypeInfo;
static TypeInfo* Score_t1097_InterfacesTypeInfos[] = 
{
	&IScore_t1105_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Score_t1097_InterfacesOffsets[] = 
{
	{ &IScore_t1105_il2cpp_TypeInfo, 4},
};
void Score_t1097_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Score_t1097_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Score_t1097_CustomAttributesCacheGenerator_Score_get_leaderboardID_m6464(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Score_t1097_CustomAttributesCacheGenerator_Score_set_leaderboardID_m6465(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Score_t1097_CustomAttributesCacheGenerator_Score_get_value_m6466(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Score_t1097_CustomAttributesCacheGenerator_Score_set_value_m6467(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache Score_t1097__CustomAttributeCache_U3CleaderboardIDU3Ek__BackingField = {
1,
NULL,
&Score_t1097_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField
};
CustomAttributesCache Score_t1097__CustomAttributeCache_U3CvalueU3Ek__BackingField = {
1,
NULL,
&Score_t1097_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField
};
CustomAttributesCache Score_t1097__CustomAttributeCache_Score_get_leaderboardID_m6464 = {
1,
NULL,
&Score_t1097_CustomAttributesCacheGenerator_Score_get_leaderboardID_m6464
};
CustomAttributesCache Score_t1097__CustomAttributeCache_Score_set_leaderboardID_m6465 = {
1,
NULL,
&Score_t1097_CustomAttributesCacheGenerator_Score_set_leaderboardID_m6465
};
CustomAttributesCache Score_t1097__CustomAttributeCache_Score_get_value_m6466 = {
1,
NULL,
&Score_t1097_CustomAttributesCacheGenerator_Score_get_value_m6466
};
CustomAttributesCache Score_t1097__CustomAttributeCache_Score_set_value_m6467 = {
1,
NULL,
&Score_t1097_CustomAttributesCacheGenerator_Score_set_value_m6467
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Score_t1097_0_0_0;
extern Il2CppType Score_t1097_1_0_0;
struct Score_t1097;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_U3CleaderboardIDU3Ek__BackingField;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_U3CvalueU3Ek__BackingField;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_get_leaderboardID_m6464;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_set_leaderboardID_m6465;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_get_value_m6466;
extern CustomAttributesCache Score_t1097__CustomAttributeCache_Score_set_value_m6467;
TypeInfo Score_t1097_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Score_t1097_MethodInfos/* methods */
	, Score_t1097_PropertyInfos/* properties */
	, Score_t1097_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Score_t1097_il2cpp_TypeInfo/* element_class */
	, Score_t1097_InterfacesTypeInfos/* implemented_interfaces */
	, Score_t1097_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Score_t1097_il2cpp_TypeInfo/* cast_class */
	, &Score_t1097_0_0_0/* byval_arg */
	, &Score_t1097_1_0_0/* this_arg */
	, Score_t1097_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t1097)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Leaderboard_t981_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"

// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
extern TypeInfo Range_t1107_il2cpp_TypeInfo;
extern TypeInfo UserScope_t1112_il2cpp_TypeInfo;
extern TypeInfo TimeScope_t1113_il2cpp_TypeInfo;
extern TypeInfo ScoreU5BU5D_t1160_il2cpp_TypeInfo;
extern TypeInfo StringU5BU5D_t862_il2cpp_TypeInfo;
extern TypeInfo UInt32_t1166_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
extern MethodInfo Leaderboard_set_id_m6476_MethodInfo;
extern MethodInfo Range__ctor_m6489_MethodInfo;
extern MethodInfo Leaderboard_set_range_m6480_MethodInfo;
extern MethodInfo Leaderboard_set_userScope_m6478_MethodInfo;
extern MethodInfo Leaderboard_set_timeScope_m6482_MethodInfo;
extern MethodInfo Leaderboard_get_id_m6475_MethodInfo;
extern MethodInfo Leaderboard_get_range_m6479_MethodInfo;
extern MethodInfo Leaderboard_get_userScope_m6477_MethodInfo;
extern MethodInfo Leaderboard_get_timeScope_m6481_MethodInfo;


// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern MethodInfo Leaderboard__ctor_m6468_MethodInfo;
 void Leaderboard__ctor_m6468 (Leaderboard_t981 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		VirtActionInvoker1< String_t* >::Invoke(&Leaderboard_set_id_m6476_MethodInfo, __this, (String_t*) &_stringLiteral421);
		Range_t1107  L_0 = {0};
		Range__ctor_m6489(&L_0, 1, ((int32_t)10), /*hidden argument*/&Range__ctor_m6489_MethodInfo);
		VirtActionInvoker1< Range_t1107  >::Invoke(&Leaderboard_set_range_m6480_MethodInfo, __this, L_0);
		VirtActionInvoker1< int32_t >::Invoke(&Leaderboard_set_userScope_m6478_MethodInfo, __this, 0);
		VirtActionInvoker1< int32_t >::Invoke(&Leaderboard_set_timeScope_m6482_MethodInfo, __this, 2);
		__this->___m_Loading_0 = 0;
		Score_t1097 * L_1 = (Score_t1097 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Score_t1097_il2cpp_TypeInfo));
		Score__ctor_m6461(L_1, (String_t*) &_stringLiteral421, (((int64_t)0)), /*hidden argument*/&Score__ctor_m6461_MethodInfo);
		__this->___m_LocalUserScore_1 = L_1;
		__this->___m_MaxRange_2 = 0;
		__this->___m_Scores_3 = (IScoreU5BU5D_t1106*)((ScoreU5BU5D_t1160*)SZArrayNew(InitializedTypeInfo(&ScoreU5BU5D_t1160_il2cpp_TypeInfo), 0));
		__this->___m_Title_4 = (String_t*) &_stringLiteral421;
		__this->___m_UserIDs_5 = ((StringU5BU5D_t862*)SZArrayNew(InitializedTypeInfo(&StringU5BU5D_t862_il2cpp_TypeInfo), 0));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern MethodInfo Leaderboard_ToString_m6469_MethodInfo;
 String_t* Leaderboard_ToString_m6469 (Leaderboard_t981 * __this, MethodInfo* method){
	Range_t1107  V_0 = {0};
	Range_t1107  V_1 = {0};
	{
		ObjectU5BU5D_t130* L_0 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), ((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral422);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)(String_t*) &_stringLiteral422;
		ObjectU5BU5D_t130* L_1 = L_0;
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Leaderboard_get_id_m6475_MethodInfo, __this);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1)) = (Object_t *)L_2;
		ObjectU5BU5D_t130* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, (String_t*) &_stringLiteral423);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2)) = (Object_t *)(String_t*) &_stringLiteral423;
		ObjectU5BU5D_t130* L_4 = L_3;
		String_t* L_5 = (__this->___m_Title_4);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3)) = (Object_t *)L_5;
		ObjectU5BU5D_t130* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral424);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4)) = (Object_t *)(String_t*) &_stringLiteral424;
		ObjectU5BU5D_t130* L_7 = L_6;
		bool L_8 = (__this->___m_Loading_0);
		bool L_9 = L_8;
		Object_t * L_10 = Box(InitializedTypeInfo(&Boolean_t122_il2cpp_TypeInfo), &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 5)) = (Object_t *)L_10;
		ObjectU5BU5D_t130* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, (String_t*) &_stringLiteral425);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6)) = (Object_t *)(String_t*) &_stringLiteral425;
		ObjectU5BU5D_t130* L_12 = L_11;
		Range_t1107  L_13 = (Range_t1107 )VirtFuncInvoker0< Range_t1107  >::Invoke(&Leaderboard_get_range_m6479_MethodInfo, __this);
		V_0 = L_13;
		NullCheck((&V_0));
		int32_t L_14 = ((&V_0)->___from_0);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 7)) = (Object_t *)L_16;
		ObjectU5BU5D_t130* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, (String_t*) &_stringLiteral21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 8)) = (Object_t *)(String_t*) &_stringLiteral21;
		ObjectU5BU5D_t130* L_18 = L_17;
		Range_t1107  L_19 = (Range_t1107 )VirtFuncInvoker0< Range_t1107  >::Invoke(&Leaderboard_get_range_m6479_MethodInfo, __this);
		V_1 = L_19;
		NullCheck((&V_1));
		int32_t L_20 = ((&V_1)->___count_1);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, ((int32_t)9))) = (Object_t *)L_22;
		ObjectU5BU5D_t130* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, (String_t*) &_stringLiteral426);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, ((int32_t)10))) = (Object_t *)(String_t*) &_stringLiteral426;
		ObjectU5BU5D_t130* L_24 = L_23;
		uint32_t L_25 = (__this->___m_MaxRange_2);
		uint32_t L_26 = L_25;
		Object_t * L_27 = Box(InitializedTypeInfo(&UInt32_t1166_il2cpp_TypeInfo), &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, ((int32_t)11))) = (Object_t *)L_27;
		ObjectU5BU5D_t130* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, (String_t*) &_stringLiteral427);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, ((int32_t)12))) = (Object_t *)(String_t*) &_stringLiteral427;
		ObjectU5BU5D_t130* L_29 = L_28;
		IScoreU5BU5D_t1106* L_30 = (__this->___m_Scores_3);
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)(((Array_t *)L_30)->max_length)));
		Object_t * L_32 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, ((int32_t)13))) = (Object_t *)L_32;
		ObjectU5BU5D_t130* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, (String_t*) &_stringLiteral428);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, ((int32_t)14))) = (Object_t *)(String_t*) &_stringLiteral428;
		ObjectU5BU5D_t130* L_34 = L_33;
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Leaderboard_get_userScope_m6477_MethodInfo, __this);
		int32_t L_36 = L_35;
		Object_t * L_37 = Box(InitializedTypeInfo(&UserScope_t1112_il2cpp_TypeInfo), &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_34, ((int32_t)15))) = (Object_t *)L_37;
		ObjectU5BU5D_t130* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, (String_t*) &_stringLiteral429);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, ((int32_t)16))) = (Object_t *)(String_t*) &_stringLiteral429;
		ObjectU5BU5D_t130* L_39 = L_38;
		int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Leaderboard_get_timeScope_m6481_MethodInfo, __this);
		int32_t L_41 = L_40;
		Object_t * L_42 = Box(InitializedTypeInfo(&TimeScope_t1113_il2cpp_TypeInfo), &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_39, ((int32_t)17))) = (Object_t *)L_42;
		ObjectU5BU5D_t130* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, (String_t*) &_stringLiteral430);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, ((int32_t)18))) = (Object_t *)(String_t*) &_stringLiteral430;
		ObjectU5BU5D_t130* L_44 = L_43;
		StringU5BU5D_t862* L_45 = (__this->___m_UserIDs_5);
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)(((Array_t *)L_45)->max_length)));
		Object_t * L_47 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)19))) = (Object_t *)L_47;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_48 = String_Concat_m516(NULL /*static, unused*/, L_44, /*hidden argument*/&String_Concat_m516_MethodInfo);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern MethodInfo Leaderboard_SetLocalUserScore_m6470_MethodInfo;
 void Leaderboard_SetLocalUserScore_m6470 (Leaderboard_t981 * __this, Object_t * ___score, MethodInfo* method){
	{
		__this->___m_LocalUserScore_1 = ___score;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern MethodInfo Leaderboard_SetMaxRange_m6471_MethodInfo;
 void Leaderboard_SetMaxRange_m6471 (Leaderboard_t981 * __this, uint32_t ___maxRange, MethodInfo* method){
	{
		__this->___m_MaxRange_2 = ___maxRange;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern MethodInfo Leaderboard_SetScores_m6472_MethodInfo;
 void Leaderboard_SetScores_m6472 (Leaderboard_t981 * __this, IScoreU5BU5D_t1106* ___scores, MethodInfo* method){
	{
		__this->___m_Scores_3 = ___scores;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern MethodInfo Leaderboard_SetTitle_m6473_MethodInfo;
 void Leaderboard_SetTitle_m6473 (Leaderboard_t981 * __this, String_t* ___title, MethodInfo* method){
	{
		__this->___m_Title_4 = ___title;
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern MethodInfo Leaderboard_GetUserFilter_m6474_MethodInfo;
 StringU5BU5D_t862* Leaderboard_GetUserFilter_m6474 (Leaderboard_t981 * __this, MethodInfo* method){
	{
		StringU5BU5D_t862* L_0 = (__this->___m_UserIDs_5);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
 String_t* Leaderboard_get_id_m6475 (Leaderboard_t981 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
 void Leaderboard_set_id_m6476 (Leaderboard_t981 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___U3CidU3Ek__BackingField_6 = ___value;
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
 int32_t Leaderboard_get_userScope_m6477 (Leaderboard_t981 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___U3CuserScopeU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
 void Leaderboard_set_userScope_m6478 (Leaderboard_t981 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___U3CuserScopeU3Ek__BackingField_7 = ___value;
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
 Range_t1107  Leaderboard_get_range_m6479 (Leaderboard_t981 * __this, MethodInfo* method){
	{
		Range_t1107  L_0 = (__this->___U3CrangeU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
 void Leaderboard_set_range_m6480 (Leaderboard_t981 * __this, Range_t1107  ___value, MethodInfo* method){
	{
		__this->___U3CrangeU3Ek__BackingField_8 = ___value;
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
 int32_t Leaderboard_get_timeScope_m6481 (Leaderboard_t981 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___U3CtimeScopeU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
 void Leaderboard_set_timeScope_m6482 (Leaderboard_t981 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___U3CtimeScopeU3Ek__BackingField_9 = ___value;
		return;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo Leaderboard_t981____m_Loading_0_FieldInfo = 
{
	"m_Loading"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___m_Loading_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IScore_t1105_0_0_1;
FieldInfo Leaderboard_t981____m_LocalUserScore_1_FieldInfo = 
{
	"m_LocalUserScore"/* name */
	, &IScore_t1105_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___m_LocalUserScore_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UInt32_t1166_0_0_1;
FieldInfo Leaderboard_t981____m_MaxRange_2_FieldInfo = 
{
	"m_MaxRange"/* name */
	, &UInt32_t1166_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___m_MaxRange_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IScoreU5BU5D_t1106_0_0_1;
FieldInfo Leaderboard_t981____m_Scores_3_FieldInfo = 
{
	"m_Scores"/* name */
	, &IScoreU5BU5D_t1106_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___m_Scores_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo Leaderboard_t981____m_Title_4_FieldInfo = 
{
	"m_Title"/* name */
	, &String_t_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___m_Title_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t862_0_0_1;
FieldInfo Leaderboard_t981____m_UserIDs_5_FieldInfo = 
{
	"m_UserIDs"/* name */
	, &StringU5BU5D_t862_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___m_UserIDs_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CidU3Ek__BackingField;
FieldInfo Leaderboard_t981____U3CidU3Ek__BackingField_6_FieldInfo = 
{
	"<id>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___U3CidU3Ek__BackingField_6)/* data */
	, &Leaderboard_t981__CustomAttributeCache_U3CidU3Ek__BackingField/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1112_0_0_1;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CuserScopeU3Ek__BackingField;
FieldInfo Leaderboard_t981____U3CuserScopeU3Ek__BackingField_7_FieldInfo = 
{
	"<userScope>k__BackingField"/* name */
	, &UserScope_t1112_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___U3CuserScopeU3Ek__BackingField_7)/* data */
	, &Leaderboard_t981__CustomAttributeCache_U3CuserScopeU3Ek__BackingField/* custom_attributes_cache */

};
extern Il2CppType Range_t1107_0_0_1;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CrangeU3Ek__BackingField;
FieldInfo Leaderboard_t981____U3CrangeU3Ek__BackingField_8_FieldInfo = 
{
	"<range>k__BackingField"/* name */
	, &Range_t1107_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___U3CrangeU3Ek__BackingField_8)/* data */
	, &Leaderboard_t981__CustomAttributeCache_U3CrangeU3Ek__BackingField/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1113_0_0_1;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CtimeScopeU3Ek__BackingField;
FieldInfo Leaderboard_t981____U3CtimeScopeU3Ek__BackingField_9_FieldInfo = 
{
	"<timeScope>k__BackingField"/* name */
	, &TimeScope_t1113_0_0_1/* type */
	, &Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, offsetof(Leaderboard_t981, ___U3CtimeScopeU3Ek__BackingField_9)/* data */
	, &Leaderboard_t981__CustomAttributeCache_U3CtimeScopeU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* Leaderboard_t981_FieldInfos[] =
{
	&Leaderboard_t981____m_Loading_0_FieldInfo,
	&Leaderboard_t981____m_LocalUserScore_1_FieldInfo,
	&Leaderboard_t981____m_MaxRange_2_FieldInfo,
	&Leaderboard_t981____m_Scores_3_FieldInfo,
	&Leaderboard_t981____m_Title_4_FieldInfo,
	&Leaderboard_t981____m_UserIDs_5_FieldInfo,
	&Leaderboard_t981____U3CidU3Ek__BackingField_6_FieldInfo,
	&Leaderboard_t981____U3CuserScopeU3Ek__BackingField_7_FieldInfo,
	&Leaderboard_t981____U3CrangeU3Ek__BackingField_8_FieldInfo,
	&Leaderboard_t981____U3CtimeScopeU3Ek__BackingField_9_FieldInfo,
	NULL
};
static PropertyInfo Leaderboard_t981____id_PropertyInfo = 
{
	&Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Leaderboard_get_id_m6475_MethodInfo/* get */
	, &Leaderboard_set_id_m6476_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Leaderboard_t981____userScope_PropertyInfo = 
{
	&Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &Leaderboard_get_userScope_m6477_MethodInfo/* get */
	, &Leaderboard_set_userScope_m6478_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Leaderboard_t981____range_PropertyInfo = 
{
	&Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &Leaderboard_get_range_m6479_MethodInfo/* get */
	, &Leaderboard_set_range_m6480_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Leaderboard_t981____timeScope_PropertyInfo = 
{
	&Leaderboard_t981_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &Leaderboard_get_timeScope_m6481_MethodInfo/* get */
	, &Leaderboard_set_timeScope_m6482_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Leaderboard_t981_PropertyInfos[] =
{
	&Leaderboard_t981____id_PropertyInfo,
	&Leaderboard_t981____userScope_PropertyInfo,
	&Leaderboard_t981____range_PropertyInfo,
	&Leaderboard_t981____timeScope_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
MethodInfo Leaderboard__ctor_m6468_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Leaderboard__ctor_m6468/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
MethodInfo Leaderboard_ToString_m6469_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Leaderboard_ToString_m6469/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IScore_t1105_0_0_0;
extern Il2CppType IScore_t1105_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_SetLocalUserScore_m6470_ParameterInfos[] = 
{
	{"score", 0, 134219314, &EmptyCustomAttributesCache, &IScore_t1105_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
MethodInfo Leaderboard_SetLocalUserScore_m6470_MethodInfo = 
{
	"SetLocalUserScore"/* name */
	, (methodPointerType)&Leaderboard_SetLocalUserScore_m6470/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Leaderboard_t981_Leaderboard_SetLocalUserScore_m6470_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UInt32_t1166_0_0_0;
extern Il2CppType UInt32_t1166_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_SetMaxRange_m6471_ParameterInfos[] = 
{
	{"maxRange", 0, 134219315, &EmptyCustomAttributesCache, &UInt32_t1166_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
MethodInfo Leaderboard_SetMaxRange_m6471_MethodInfo = 
{
	"SetMaxRange"/* name */
	, (methodPointerType)&Leaderboard_SetMaxRange_m6471/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Leaderboard_t981_Leaderboard_SetMaxRange_m6471_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IScoreU5BU5D_t1106_0_0_0;
extern Il2CppType IScoreU5BU5D_t1106_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_SetScores_m6472_ParameterInfos[] = 
{
	{"scores", 0, 134219316, &EmptyCustomAttributesCache, &IScoreU5BU5D_t1106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
MethodInfo Leaderboard_SetScores_m6472_MethodInfo = 
{
	"SetScores"/* name */
	, (methodPointerType)&Leaderboard_SetScores_m6472/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Leaderboard_t981_Leaderboard_SetScores_m6472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_SetTitle_m6473_ParameterInfos[] = 
{
	{"title", 0, 134219317, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
MethodInfo Leaderboard_SetTitle_m6473_MethodInfo = 
{
	"SetTitle"/* name */
	, (methodPointerType)&Leaderboard_SetTitle_m6473/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Leaderboard_t981_Leaderboard_SetTitle_m6473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StringU5BU5D_t862_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
MethodInfo Leaderboard_GetUserFilter_m6474_MethodInfo = 
{
	"GetUserFilter"/* name */
	, (methodPointerType)&Leaderboard_GetUserFilter_m6474/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t862_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_id_m6475;
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
MethodInfo Leaderboard_get_id_m6475_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Leaderboard_get_id_m6475/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_get_id_m6475/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_set_id_m6476_ParameterInfos[] = 
{
	{"value", 0, 134219318, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_id_m6476;
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
MethodInfo Leaderboard_set_id_m6476_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Leaderboard_set_id_m6476/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Leaderboard_t981_Leaderboard_set_id_m6476_ParameterInfos/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_set_id_m6476/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1112_0_0_0;
extern void* RuntimeInvoker_UserScope_t1112 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_userScope_m6477;
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
MethodInfo Leaderboard_get_userScope_m6477_MethodInfo = 
{
	"get_userScope"/* name */
	, (methodPointerType)&Leaderboard_get_userScope_m6477/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1112_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1112/* invoker_method */
	, NULL/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_get_userScope_m6477/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1112_0_0_0;
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_set_userScope_m6478_ParameterInfos[] = 
{
	{"value", 0, 134219319, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_userScope_m6478;
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
MethodInfo Leaderboard_set_userScope_m6478_MethodInfo = 
{
	"set_userScope"/* name */
	, (methodPointerType)&Leaderboard_set_userScope_m6478/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Leaderboard_t981_Leaderboard_set_userScope_m6478_ParameterInfos/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_set_userScope_m6478/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1107_0_0_0;
extern void* RuntimeInvoker_Range_t1107 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_range_m6479;
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
MethodInfo Leaderboard_get_range_m6479_MethodInfo = 
{
	"get_range"/* name */
	, (methodPointerType)&Leaderboard_get_range_m6479/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1107_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1107/* invoker_method */
	, NULL/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_get_range_m6479/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1107_0_0_0;
extern Il2CppType Range_t1107_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_set_range_m6480_ParameterInfos[] = 
{
	{"value", 0, 134219320, &EmptyCustomAttributesCache, &Range_t1107_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Range_t1107 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_range_m6480;
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
MethodInfo Leaderboard_set_range_m6480_MethodInfo = 
{
	"set_range"/* name */
	, (methodPointerType)&Leaderboard_set_range_m6480/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Range_t1107/* invoker_method */
	, Leaderboard_t981_Leaderboard_set_range_m6480_ParameterInfos/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_set_range_m6480/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1113_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1113 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_timeScope_m6481;
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
MethodInfo Leaderboard_get_timeScope_m6481_MethodInfo = 
{
	"get_timeScope"/* name */
	, (methodPointerType)&Leaderboard_get_timeScope_m6481/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1113_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1113/* invoker_method */
	, NULL/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_get_timeScope_m6481/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1113_0_0_0;
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo Leaderboard_t981_Leaderboard_set_timeScope_m6482_ParameterInfos[] = 
{
	{"value", 0, 134219321, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_timeScope_m6482;
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
MethodInfo Leaderboard_set_timeScope_m6482_MethodInfo = 
{
	"set_timeScope"/* name */
	, (methodPointerType)&Leaderboard_set_timeScope_m6482/* method */
	, &Leaderboard_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Leaderboard_t981_Leaderboard_set_timeScope_m6482_ParameterInfos/* parameters */
	, &Leaderboard_t981__CustomAttributeCache_Leaderboard_set_timeScope_m6482/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Leaderboard_t981_MethodInfos[] =
{
	&Leaderboard__ctor_m6468_MethodInfo,
	&Leaderboard_ToString_m6469_MethodInfo,
	&Leaderboard_SetLocalUserScore_m6470_MethodInfo,
	&Leaderboard_SetMaxRange_m6471_MethodInfo,
	&Leaderboard_SetScores_m6472_MethodInfo,
	&Leaderboard_SetTitle_m6473_MethodInfo,
	&Leaderboard_GetUserFilter_m6474_MethodInfo,
	&Leaderboard_get_id_m6475_MethodInfo,
	&Leaderboard_set_id_m6476_MethodInfo,
	&Leaderboard_get_userScope_m6477_MethodInfo,
	&Leaderboard_set_userScope_m6478_MethodInfo,
	&Leaderboard_get_range_m6479_MethodInfo,
	&Leaderboard_set_range_m6480_MethodInfo,
	&Leaderboard_get_timeScope_m6481_MethodInfo,
	&Leaderboard_set_timeScope_m6482_MethodInfo,
	NULL
};
static MethodInfo* Leaderboard_t981_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Leaderboard_ToString_m6469_MethodInfo,
	&Leaderboard_get_id_m6475_MethodInfo,
	&Leaderboard_get_userScope_m6477_MethodInfo,
	&Leaderboard_get_range_m6479_MethodInfo,
	&Leaderboard_get_timeScope_m6481_MethodInfo,
	&Leaderboard_set_id_m6476_MethodInfo,
	&Leaderboard_set_userScope_m6478_MethodInfo,
	&Leaderboard_set_range_m6480_MethodInfo,
	&Leaderboard_set_timeScope_m6482_MethodInfo,
};
extern TypeInfo ILeaderboard_t977_il2cpp_TypeInfo;
static TypeInfo* Leaderboard_t981_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t977_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Leaderboard_t981_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t977_il2cpp_TypeInfo, 4},
};
void Leaderboard_t981_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_id_m6475(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_id_m6476(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m6477(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m6478(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_range_m6479(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_range_m6480(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m6481(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m6482(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CidU3Ek__BackingField = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CuserScopeU3Ek__BackingField = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CrangeU3Ek__BackingField = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CtimeScopeU3Ek__BackingField = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_id_m6475 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_id_m6475
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_id_m6476 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_id_m6476
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_userScope_m6477 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m6477
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_userScope_m6478 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m6478
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_range_m6479 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_range_m6479
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_range_m6480 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_range_m6480
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_timeScope_m6481 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m6481
};
CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_timeScope_m6482 = {
1,
NULL,
&Leaderboard_t981_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m6482
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Leaderboard_t981_0_0_0;
extern Il2CppType Leaderboard_t981_1_0_0;
struct Leaderboard_t981;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CidU3Ek__BackingField;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CuserScopeU3Ek__BackingField;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CrangeU3Ek__BackingField;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_U3CtimeScopeU3Ek__BackingField;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_id_m6475;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_id_m6476;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_userScope_m6477;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_userScope_m6478;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_range_m6479;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_range_m6480;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_get_timeScope_m6481;
extern CustomAttributesCache Leaderboard_t981__CustomAttributeCache_Leaderboard_set_timeScope_m6482;
TypeInfo Leaderboard_t981_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Leaderboard_t981_MethodInfos/* methods */
	, Leaderboard_t981_PropertyInfos/* properties */
	, Leaderboard_t981_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Leaderboard_t981_il2cpp_TypeInfo/* element_class */
	, Leaderboard_t981_InterfacesTypeInfos/* implemented_interfaces */
	, Leaderboard_t981_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Leaderboard_t981_il2cpp_TypeInfo/* cast_class */
	, &Leaderboard_t981_0_0_0/* byval_arg */
	, &Leaderboard_t981_1_0_0/* this_arg */
	, Leaderboard_t981_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t981)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo HitInfo_t1108_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"

// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern MethodInfo GameObject_SendMessage_m6196_MethodInfo;
extern MethodInfo Object_op_Equality_m524_MethodInfo;
extern MethodInfo Object_op_Inequality_m489_MethodInfo;


// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern MethodInfo HitInfo_SendMessage_m6483_MethodInfo;
 void HitInfo_SendMessage_m6483 (HitInfo_t1108 * __this, String_t* ___name, MethodInfo* method){
	{
		GameObject_t29 * L_0 = (__this->___target_0);
		NullCheck(L_0);
		GameObject_SendMessage_m6196(L_0, ___name, NULL, 1, /*hidden argument*/&GameObject_SendMessage_m6196_MethodInfo);
		return;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern MethodInfo HitInfo_Compare_m6484_MethodInfo;
 bool HitInfo_Compare_m6484 (Object_t * __this/* static, unused */, HitInfo_t1108  ___lhs, HitInfo_t1108  ___rhs, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		NullCheck((&___lhs));
		GameObject_t29 * L_0 = ((&___lhs)->___target_0);
		NullCheck((&___rhs));
		GameObject_t29 * L_1 = ((&___rhs)->___target_0);
		bool L_2 = Object_op_Equality_m524(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		NullCheck((&___lhs));
		Camera_t168 * L_3 = ((&___lhs)->___camera_1);
		NullCheck((&___rhs));
		Camera_t168 * L_4 = ((&___rhs)->___camera_1);
		bool L_5 = Object_op_Equality_m524(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern MethodInfo HitInfo_op_Implicit_m6485_MethodInfo;
 bool HitInfo_op_Implicit_m6485 (Object_t * __this/* static, unused */, HitInfo_t1108  ___exists, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		NullCheck((&___exists));
		GameObject_t29 * L_0 = ((&___exists)->___target_0);
		bool L_1 = Object_op_Inequality_m489(NULL /*static, unused*/, L_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((&___exists));
		Camera_t168 * L_2 = ((&___exists)->___camera_1);
		bool L_3 = Object_op_Inequality_m489(NULL /*static, unused*/, L_2, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern Il2CppType GameObject_t29_0_0_6;
FieldInfo HitInfo_t1108____target_0_FieldInfo = 
{
	"target"/* name */
	, &GameObject_t29_0_0_6/* type */
	, &HitInfo_t1108_il2cpp_TypeInfo/* parent */
	, offsetof(HitInfo_t1108, ___target_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t168_0_0_6;
FieldInfo HitInfo_t1108____camera_1_FieldInfo = 
{
	"camera"/* name */
	, &Camera_t168_0_0_6/* type */
	, &HitInfo_t1108_il2cpp_TypeInfo/* parent */
	, offsetof(HitInfo_t1108, ___camera_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* HitInfo_t1108_FieldInfos[] =
{
	&HitInfo_t1108____target_0_FieldInfo,
	&HitInfo_t1108____camera_1_FieldInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo HitInfo_t1108_HitInfo_SendMessage_m6483_ParameterInfos[] = 
{
	{"name", 0, 134219326, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
MethodInfo HitInfo_SendMessage_m6483_MethodInfo = 
{
	"SendMessage"/* name */
	, (methodPointerType)&HitInfo_SendMessage_m6483/* method */
	, &HitInfo_t1108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, HitInfo_t1108_HitInfo_SendMessage_m6483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HitInfo_t1108_0_0_0;
extern Il2CppType HitInfo_t1108_0_0_0;
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo HitInfo_t1108_HitInfo_Compare_m6484_ParameterInfos[] = 
{
	{"lhs", 0, 134219327, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
	{"rhs", 1, 134219328, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_HitInfo_t1108_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
MethodInfo HitInfo_Compare_m6484_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&HitInfo_Compare_m6484/* method */
	, &HitInfo_t1108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_HitInfo_t1108_HitInfo_t1108/* invoker_method */
	, HitInfo_t1108_HitInfo_Compare_m6484_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo HitInfo_t1108_HitInfo_op_Implicit_m6485_ParameterInfos[] = 
{
	{"exists", 0, 134219329, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
MethodInfo HitInfo_op_Implicit_m6485_MethodInfo = 
{
	"op_Implicit"/* name */
	, (methodPointerType)&HitInfo_op_Implicit_m6485/* method */
	, &HitInfo_t1108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_HitInfo_t1108/* invoker_method */
	, HitInfo_t1108_HitInfo_op_Implicit_m6485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HitInfo_t1108_MethodInfos[] =
{
	&HitInfo_SendMessage_m6483_MethodInfo,
	&HitInfo_Compare_m6484_MethodInfo,
	&HitInfo_op_Implicit_m6485_MethodInfo,
	NULL
};
static MethodInfo* HitInfo_t1108_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType HitInfo_t1108_1_0_0;
extern TypeInfo SendMouseEvents_t1110_il2cpp_TypeInfo;
TypeInfo HitInfo_t1108_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, HitInfo_t1108_MethodInfos/* methods */
	, NULL/* properties */
	, HitInfo_t1108_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* nested_in */
	, &HitInfo_t1108_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, HitInfo_t1108_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &HitInfo_t1108_il2cpp_TypeInfo/* cast_class */
	, &HitInfo_t1108_0_0_0/* byval_arg */
	, &HitInfo_t1108_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t1108)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"

// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayer.h"
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElement.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
extern TypeInfo HitInfoU5BU5D_t1109_il2cpp_TypeInfo;
extern TypeInfo Input_t187_il2cpp_TypeInfo;
extern TypeInfo CameraU5BU5D_t172_il2cpp_TypeInfo;
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
extern TypeInfo Vector3_t73_il2cpp_TypeInfo;
extern TypeInfo Mathf_t179_il2cpp_TypeInfo;
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern MethodInfo Input_get_mousePosition_m633_MethodInfo;
extern MethodInfo Camera_get_allCamerasCount_m6128_MethodInfo;
extern MethodInfo Camera_GetAllCameras_m6129_MethodInfo;
extern MethodInfo Camera_get_targetTexture_m5442_MethodInfo;
extern MethodInfo Camera_get_pixelRect_m4457_MethodInfo;
extern MethodInfo Rect_Contains_m5981_MethodInfo;
extern MethodInfo Component_GetComponent_TisGUILayer_t991_m6682_MethodInfo;
extern MethodInfo Object_op_Implicit_m257_MethodInfo;
extern MethodInfo GUILayer_HitTest_m5723_MethodInfo;
extern MethodInfo Component_get_gameObject_m419_MethodInfo;
extern MethodInfo Camera_get_eventMask_m6116_MethodInfo;
extern MethodInfo Camera_ScreenPointToRay_m853_MethodInfo;
extern MethodInfo Ray_get_direction_m2207_MethodInfo;
extern MethodInfo Mathf_Approximately_m2186_MethodInfo;
extern MethodInfo Camera_get_farClipPlane_m2204_MethodInfo;
extern MethodInfo Camera_get_nearClipPlane_m2205_MethodInfo;
extern MethodInfo Mathf_Abs_m699_MethodInfo;
extern MethodInfo Camera_get_cullingMask_m2217_MethodInfo;
extern MethodInfo Camera_RaycastTry_m6133_MethodInfo;
extern MethodInfo Camera_get_clearFlags_m6123_MethodInfo;
extern MethodInfo Camera_RaycastTry2D_m6135_MethodInfo;
extern MethodInfo SendMouseEvents_SendEvents_m6488_MethodInfo;
extern MethodInfo Input_GetMouseButtonDown_m2164_MethodInfo;
extern MethodInfo Input_GetMouseButton_m632_MethodInfo;
struct Component_t128;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.CastHelper`1<UnityEngine.GUILayer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_36.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
struct Component_t128;
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
 Object_t * Component_GetComponent_TisObject_t_m280_gshared (Component_t128 * __this, MethodInfo* method);
#define Component_GetComponent_TisObject_t_m280(__this, method) (Object_t *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t991_m6682(__this, method) (GUILayer_t991 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)


// System.Void UnityEngine.SendMouseEvents::.cctor()
extern MethodInfo SendMouseEvents__cctor_m6486_MethodInfo;
 void SendMouseEvents__cctor_m6486 (Object_t * __this/* static, unused */, MethodInfo* method){
	HitInfo_t1108  V_0 = {0};
	HitInfo_t1108  V_1 = {0};
	HitInfo_t1108  V_2 = {0};
	HitInfo_t1108  V_3 = {0};
	HitInfo_t1108  V_4 = {0};
	HitInfo_t1108  V_5 = {0};
	HitInfo_t1108  V_6 = {0};
	HitInfo_t1108  V_7 = {0};
	HitInfo_t1108  V_8 = {0};
	{
		HitInfoU5BU5D_t1109* L_0 = ((HitInfoU5BU5D_t1109*)SZArrayNew(InitializedTypeInfo(&HitInfoU5BU5D_t1109_il2cpp_TypeInfo), 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_0));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_0, 0)) = V_0;
		HitInfoU5BU5D_t1109* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_1));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_1, 1)) = V_1;
		HitInfoU5BU5D_t1109* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_2));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_2, 2)) = V_2;
		((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3 = L_2;
		HitInfoU5BU5D_t1109* L_3 = ((HitInfoU5BU5D_t1109*)SZArrayNew(InitializedTypeInfo(&HitInfoU5BU5D_t1109_il2cpp_TypeInfo), 3));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_3));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_3, 0)) = V_3;
		HitInfoU5BU5D_t1109* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_4));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_4, 1)) = V_4;
		HitInfoU5BU5D_t1109* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_5));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_5, 2)) = V_5;
		((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4 = L_5;
		HitInfoU5BU5D_t1109* L_6 = ((HitInfoU5BU5D_t1109*)SZArrayNew(InitializedTypeInfo(&HitInfoU5BU5D_t1109_il2cpp_TypeInfo), 3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_6));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_6, 0)) = V_6;
		HitInfoU5BU5D_t1109* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_7));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_7, 1)) = V_7;
		HitInfoU5BU5D_t1109* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_8));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema(L_8, 2)) = V_8;
		((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5 = L_8;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
extern MethodInfo SendMouseEvents_DoSendMouseEvents_m6487_MethodInfo;
 void SendMouseEvents_DoSendMouseEvents_m6487 (Object_t * __this/* static, unused */, int32_t ___mouseUsed, int32_t ___skipRTCameras, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t168 * V_3 = {0};
	CameraU5BU5D_t172* V_4 = {0};
	int32_t V_5 = 0;
	Rect_t103  V_6 = {0};
	GUILayer_t991 * V_7 = {0};
	GUIElement_t990 * V_8 = {0};
	Ray_t229  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t29 * V_12 = {0};
	GameObject_t29 * V_13 = {0};
	int32_t V_14 = 0;
	HitInfo_t1108  V_15 = {0};
	Vector3_t73  V_16 = {0};
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		Vector3_t73  L_0 = Input_get_mousePosition_m633(NULL /*static, unused*/, /*hidden argument*/&Input_get_mousePosition_m633_MethodInfo);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m6128(NULL /*static, unused*/, /*hidden argument*/&Camera_get_allCamerasCount_m6128_MethodInfo);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		if (!(((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_Cameras_6))
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_Cameras_6));
		if ((((int32_t)(((int32_t)(((Array_t *)(((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_Cameras_6))->max_length)))) == ((int32_t)V_1)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_Cameras_6 = ((CameraU5BU5D_t172*)SZArrayNew(InitializedTypeInfo(&CameraU5BU5D_t172_il2cpp_TypeInfo), V_1));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		Camera_GetAllCameras_m6129(NULL /*static, unused*/, (((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_Cameras_6), /*hidden argument*/&Camera_GetAllCameras_m6129_MethodInfo);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), V_2);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_15));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), V_2)) = V_15;
		V_2 = ((int32_t)(V_2+1));
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((Array_t *)(((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5))->max_length))))))
		{
			goto IL_0040;
		}
	}
	{
		if (___mouseUsed)
		{
			goto IL_02bf;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		V_4 = (((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_Cameras_6);
		V_5 = 0;
		goto IL_02b4;
	}

IL_0080:
	{
		NullCheck(V_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
		int32_t L_2 = V_5;
		V_3 = (*(Camera_t168 **)(Camera_t168 **)SZArrayLdElema(V_4, L_2));
		bool L_3 = Object_op_Equality_m524(NULL /*static, unused*/, V_3, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (L_3)
		{
			goto IL_00a9;
		}
	}
	{
		if (!___skipRTCameras)
		{
			goto IL_00ae;
		}
	}
	{
		NullCheck(V_3);
		RenderTexture_t786 * L_4 = Camera_get_targetTexture_m5442(V_3, /*hidden argument*/&Camera_get_targetTexture_m5442_MethodInfo);
		bool L_5 = Object_op_Inequality_m489(NULL /*static, unused*/, L_4, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_5)
		{
			goto IL_00ae;
		}
	}

IL_00a9:
	{
		goto IL_02ae;
	}

IL_00ae:
	{
		NullCheck(V_3);
		Rect_t103  L_6 = Camera_get_pixelRect_m4457(V_3, /*hidden argument*/&Camera_get_pixelRect_m4457_MethodInfo);
		V_6 = L_6;
		bool L_7 = Rect_Contains_m5981((&V_6), V_0, /*hidden argument*/&Rect_Contains_m5981_MethodInfo);
		if (L_7)
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_02ae;
	}

IL_00c8:
	{
		NullCheck(V_3);
		GUILayer_t991 * L_8 = Component_GetComponent_TisGUILayer_t991_m6682(V_3, /*hidden argument*/&Component_GetComponent_TisGUILayer_t991_m6682_MethodInfo);
		V_7 = L_8;
		bool L_9 = Object_op_Implicit_m257(NULL /*static, unused*/, V_7, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_9)
		{
			goto IL_0141;
		}
	}
	{
		NullCheck(V_7);
		GUIElement_t990 * L_10 = GUILayer_HitTest_m5723(V_7, V_0, /*hidden argument*/&GUILayer_HitTest_m5723_MethodInfo);
		V_8 = L_10;
		bool L_11 = Object_op_Implicit_m257(NULL /*static, unused*/, V_8, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_11)
		{
			goto IL_011f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0);
		NullCheck(V_8);
		GameObject_t29 * L_12 = Component_get_gameObject_m419(V_8, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0))->___target_0 = L_12;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0))->___camera_1 = V_3;
		goto IL_0141;
	}

IL_011f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0))->___target_0 = (GameObject_t29 *)NULL;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 0))->___camera_1 = (Camera_t168 *)NULL;
	}

IL_0141:
	{
		NullCheck(V_3);
		int32_t L_13 = Camera_get_eventMask_m6116(V_3, /*hidden argument*/&Camera_get_eventMask_m6116_MethodInfo);
		if (L_13)
		{
			goto IL_0151;
		}
	}
	{
		goto IL_02ae;
	}

IL_0151:
	{
		NullCheck(V_3);
		Ray_t229  L_14 = Camera_ScreenPointToRay_m853(V_3, V_0, /*hidden argument*/&Camera_ScreenPointToRay_m853_MethodInfo);
		V_9 = L_14;
		Vector3_t73  L_15 = Ray_get_direction_m2207((&V_9), /*hidden argument*/&Ray_get_direction_m2207_MethodInfo);
		V_16 = L_15;
		NullCheck((&V_16));
		float L_16 = ((&V_16)->___z_3);
		V_10 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		bool L_17 = Mathf_Approximately_m2186(NULL /*static, unused*/, (0.0f), V_10, /*hidden argument*/&Mathf_Approximately_m2186_MethodInfo);
		if (!L_17)
		{
			goto IL_0187;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_019c;
	}

IL_0187:
	{
		NullCheck(V_3);
		float L_18 = Camera_get_farClipPlane_m2204(V_3, /*hidden argument*/&Camera_get_farClipPlane_m2204_MethodInfo);
		NullCheck(V_3);
		float L_19 = Camera_get_nearClipPlane_m2205(V_3, /*hidden argument*/&Camera_get_nearClipPlane_m2205_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_20 = fabsf(((float)((float)((float)(L_18-L_19))/(float)V_10)));
		G_B23_0 = L_20;
	}

IL_019c:
	{
		V_11 = G_B23_0;
		NullCheck(V_3);
		int32_t L_21 = Camera_get_cullingMask_m2217(V_3, /*hidden argument*/&Camera_get_cullingMask_m2217_MethodInfo);
		NullCheck(V_3);
		int32_t L_22 = Camera_get_eventMask_m6116(V_3, /*hidden argument*/&Camera_get_eventMask_m6116_MethodInfo);
		NullCheck(V_3);
		GameObject_t29 * L_23 = Camera_RaycastTry_m6133(V_3, V_9, V_11, ((int32_t)((int32_t)L_21&(int32_t)L_22)), /*hidden argument*/&Camera_RaycastTry_m6133_MethodInfo);
		V_12 = L_23;
		bool L_24 = Object_op_Inequality_m489(NULL /*static, unused*/, V_12, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_24)
		{
			goto IL_01ec;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1))->___target_0 = V_12;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1))->___camera_1 = V_3;
		goto IL_0226;
	}

IL_01ec:
	{
		NullCheck(V_3);
		int32_t L_25 = Camera_get_clearFlags_m6123(V_3, /*hidden argument*/&Camera_get_clearFlags_m6123_MethodInfo);
		if ((((int32_t)L_25) == ((int32_t)1)))
		{
			goto IL_0204;
		}
	}
	{
		NullCheck(V_3);
		int32_t L_26 = Camera_get_clearFlags_m6123(V_3, /*hidden argument*/&Camera_get_clearFlags_m6123_MethodInfo);
		if ((((uint32_t)L_26) != ((uint32_t)2)))
		{
			goto IL_0226;
		}
	}

IL_0204:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1))->___target_0 = (GameObject_t29 *)NULL;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 1))->___camera_1 = (Camera_t168 *)NULL;
	}

IL_0226:
	{
		NullCheck(V_3);
		int32_t L_27 = Camera_get_cullingMask_m2217(V_3, /*hidden argument*/&Camera_get_cullingMask_m2217_MethodInfo);
		NullCheck(V_3);
		int32_t L_28 = Camera_get_eventMask_m6116(V_3, /*hidden argument*/&Camera_get_eventMask_m6116_MethodInfo);
		NullCheck(V_3);
		GameObject_t29 * L_29 = Camera_RaycastTry2D_m6135(V_3, V_9, V_11, ((int32_t)((int32_t)L_27&(int32_t)L_28)), /*hidden argument*/&Camera_RaycastTry2D_m6135_MethodInfo);
		V_13 = L_29;
		bool L_30 = Object_op_Inequality_m489(NULL /*static, unused*/, V_13, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_30)
		{
			goto IL_0274;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2))->___target_0 = V_13;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2))->___camera_1 = V_3;
		goto IL_02ae;
	}

IL_0274:
	{
		NullCheck(V_3);
		int32_t L_31 = Camera_get_clearFlags_m6123(V_3, /*hidden argument*/&Camera_get_clearFlags_m6123_MethodInfo);
		if ((((int32_t)L_31) == ((int32_t)1)))
		{
			goto IL_028c;
		}
	}
	{
		NullCheck(V_3);
		int32_t L_32 = Camera_get_clearFlags_m6123(V_3, /*hidden argument*/&Camera_get_clearFlags_m6123_MethodInfo);
		if ((((uint32_t)L_32) != ((uint32_t)2)))
		{
			goto IL_02ae;
		}
	}

IL_028c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2))->___target_0 = (GameObject_t29 *)NULL;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2);
		NullCheck(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2)));
		((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), 2))->___camera_1 = (Camera_t168 *)NULL;
	}

IL_02ae:
	{
		V_5 = ((int32_t)(V_5+1));
	}

IL_02b4:
	{
		NullCheck(V_4);
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
		{
			goto IL_0080;
		}
	}

IL_02bf:
	{
		V_14 = 0;
		goto IL_02e5;
	}

IL_02c7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), V_14);
		SendMouseEvents_SendEvents_m6488(NULL /*static, unused*/, V_14, (*(HitInfo_t1108 *)((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5), V_14))), /*hidden argument*/&SendMouseEvents_SendEvents_m6488_MethodInfo);
		V_14 = ((int32_t)(V_14+1));
	}

IL_02e5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5));
		if ((((int32_t)V_14) < ((int32_t)(((int32_t)(((Array_t *)(((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_CurrentHit_5))->max_length))))))
		{
			goto IL_02c7;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
 void SendMouseEvents_SendEvents_m6488 (Object_t * __this/* static, unused */, int32_t ___i, HitInfo_t1108  ___hit, MethodInfo* method){
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t1108  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		bool L_0 = Input_GetMouseButtonDown_m2164(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetMouseButtonDown_m2164_MethodInfo);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m632(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetMouseButton_m632_MethodInfo);
		V_1 = L_1;
		if (!V_0)
		{
			goto IL_004a;
		}
	}
	{
		bool L_2 = HitInfo_op_Implicit_m6485(NULL /*static, unused*/, ___hit, /*hidden argument*/&HitInfo_op_Implicit_m6485_MethodInfo);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i)) = ___hit;
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		HitInfo_SendMessage_m6483(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i)), (String_t*) &_stringLiteral431, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		if (V_1)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		bool L_3 = HitInfo_op_Implicit_m6485(NULL /*static, unused*/, (*(HitInfo_t1108 *)((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i))), /*hidden argument*/&HitInfo_op_Implicit_m6485_MethodInfo);
		if (!L_3)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		bool L_4 = HitInfo_Compare_m6484(NULL /*static, unused*/, ___hit, (*(HitInfo_t1108 *)((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i))), /*hidden argument*/&HitInfo_Compare_m6484_MethodInfo);
		if (!L_4)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		HitInfo_SendMessage_m6483(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i)), (String_t*) &_stringLiteral432, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		HitInfo_SendMessage_m6483(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i)), (String_t*) &_stringLiteral433, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		Initobj (&HitInfo_t1108_il2cpp_TypeInfo, (&V_2));
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i)) = V_2;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		bool L_5 = HitInfo_op_Implicit_m6485(NULL /*static, unused*/, (*(HitInfo_t1108 *)((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i))), /*hidden argument*/&HitInfo_op_Implicit_m6485_MethodInfo);
		if (!L_5)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i);
		HitInfo_SendMessage_m6483(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_MouseDownHit_4), ___i)), (String_t*) &_stringLiteral434, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i);
		bool L_6 = HitInfo_Compare_m6484(NULL /*static, unused*/, ___hit, (*(HitInfo_t1108 *)((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i))), /*hidden argument*/&HitInfo_Compare_m6484_MethodInfo);
		if (!L_6)
		{
			goto IL_0133;
		}
	}
	{
		bool L_7 = HitInfo_op_Implicit_m6485(NULL /*static, unused*/, ___hit, /*hidden argument*/&HitInfo_op_Implicit_m6485_MethodInfo);
		if (!L_7)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m6483((&___hit), (String_t*) &_stringLiteral435, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i);
		bool L_8 = HitInfo_op_Implicit_m6485(NULL /*static, unused*/, (*(HitInfo_t1108 *)((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i))), /*hidden argument*/&HitInfo_op_Implicit_m6485_MethodInfo);
		if (!L_8)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i);
		HitInfo_SendMessage_m6483(((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i)), (String_t*) &_stringLiteral436, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
	}

IL_0162:
	{
		bool L_9 = HitInfo_op_Implicit_m6485(NULL /*static, unused*/, ___hit, /*hidden argument*/&HitInfo_op_Implicit_m6485_MethodInfo);
		if (!L_9)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m6483((&___hit), (String_t*) &_stringLiteral437, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
		HitInfo_SendMessage_m6483((&___hit), (String_t*) &_stringLiteral435, /*hidden argument*/&HitInfo_SendMessage_m6483_MethodInfo);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo));
		NullCheck((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3));
		IL2CPP_ARRAY_BOUNDS_CHECK((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i);
		*((HitInfo_t1108 *)(HitInfo_t1108 *)SZArrayLdElema((((SendMouseEvents_t1110_StaticFields*)InitializedTypeInfo(&SendMouseEvents_t1110_il2cpp_TypeInfo)->static_fields)->___m_LastHit_3), ___i)) = ___hit;
		return;
	}
}
// Metadata Definition UnityEngine.SendMouseEvents
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo SendMouseEvents_t1110____m_HitIndexGUI_0_FieldInfo = 
{
	"m_HitIndexGUI"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo SendMouseEvents_t1110____m_HitIndexPhysics3D_1_FieldInfo = 
{
	"m_HitIndexPhysics3D"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo SendMouseEvents_t1110____m_HitIndexPhysics2D_2_FieldInfo = 
{
	"m_HitIndexPhysics2D"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HitInfoU5BU5D_t1109_0_0_49;
FieldInfo SendMouseEvents_t1110____m_LastHit_3_FieldInfo = 
{
	"m_LastHit"/* name */
	, &HitInfoU5BU5D_t1109_0_0_49/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1110_StaticFields, ___m_LastHit_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HitInfoU5BU5D_t1109_0_0_49;
FieldInfo SendMouseEvents_t1110____m_MouseDownHit_4_FieldInfo = 
{
	"m_MouseDownHit"/* name */
	, &HitInfoU5BU5D_t1109_0_0_49/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1110_StaticFields, ___m_MouseDownHit_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HitInfoU5BU5D_t1109_0_0_49;
FieldInfo SendMouseEvents_t1110____m_CurrentHit_5_FieldInfo = 
{
	"m_CurrentHit"/* name */
	, &HitInfoU5BU5D_t1109_0_0_49/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1110_StaticFields, ___m_CurrentHit_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType CameraU5BU5D_t172_0_0_17;
FieldInfo SendMouseEvents_t1110____m_Cameras_6_FieldInfo = 
{
	"m_Cameras"/* name */
	, &CameraU5BU5D_t172_0_0_17/* type */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* parent */
	, offsetof(SendMouseEvents_t1110_StaticFields, ___m_Cameras_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SendMouseEvents_t1110_FieldInfos[] =
{
	&SendMouseEvents_t1110____m_HitIndexGUI_0_FieldInfo,
	&SendMouseEvents_t1110____m_HitIndexPhysics3D_1_FieldInfo,
	&SendMouseEvents_t1110____m_HitIndexPhysics2D_2_FieldInfo,
	&SendMouseEvents_t1110____m_LastHit_3_FieldInfo,
	&SendMouseEvents_t1110____m_MouseDownHit_4_FieldInfo,
	&SendMouseEvents_t1110____m_CurrentHit_5_FieldInfo,
	&SendMouseEvents_t1110____m_Cameras_6_FieldInfo,
	NULL
};
static const int32_t SendMouseEvents_t1110____m_HitIndexGUI_0_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry SendMouseEvents_t1110____m_HitIndexGUI_0_DefaultValue = 
{
	&SendMouseEvents_t1110____m_HitIndexGUI_0_FieldInfo/* field */
	, { (char*)&SendMouseEvents_t1110____m_HitIndexGUI_0_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t SendMouseEvents_t1110____m_HitIndexPhysics3D_1_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry SendMouseEvents_t1110____m_HitIndexPhysics3D_1_DefaultValue = 
{
	&SendMouseEvents_t1110____m_HitIndexPhysics3D_1_FieldInfo/* field */
	, { (char*)&SendMouseEvents_t1110____m_HitIndexPhysics3D_1_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t SendMouseEvents_t1110____m_HitIndexPhysics2D_2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry SendMouseEvents_t1110____m_HitIndexPhysics2D_2_DefaultValue = 
{
	&SendMouseEvents_t1110____m_HitIndexPhysics2D_2_FieldInfo/* field */
	, { (char*)&SendMouseEvents_t1110____m_HitIndexPhysics2D_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* SendMouseEvents_t1110_FieldDefaultValues[] = 
{
	&SendMouseEvents_t1110____m_HitIndexGUI_0_DefaultValue,
	&SendMouseEvents_t1110____m_HitIndexPhysics3D_1_DefaultValue,
	&SendMouseEvents_t1110____m_HitIndexPhysics2D_2_DefaultValue,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::.cctor()
MethodInfo SendMouseEvents__cctor_m6486_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SendMouseEvents__cctor_m6486/* method */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo SendMouseEvents_t1110_SendMouseEvents_DoSendMouseEvents_m6487_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134219322, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"skipRTCameras", 1, 134219323, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
MethodInfo SendMouseEvents_DoSendMouseEvents_m6487_MethodInfo = 
{
	"DoSendMouseEvents"/* name */
	, (methodPointerType)&SendMouseEvents_DoSendMouseEvents_m6487/* method */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, SendMouseEvents_t1110_SendMouseEvents_DoSendMouseEvents_m6487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo SendMouseEvents_t1110_SendMouseEvents_SendEvents_m6488_ParameterInfos[] = 
{
	{"i", 0, 134219324, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"hit", 1, 134219325, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
MethodInfo SendMouseEvents_SendEvents_m6488_MethodInfo = 
{
	"SendEvents"/* name */
	, (methodPointerType)&SendMouseEvents_SendEvents_m6488/* method */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_HitInfo_t1108/* invoker_method */
	, SendMouseEvents_t1110_SendMouseEvents_SendEvents_m6488_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SendMouseEvents_t1110_MethodInfos[] =
{
	&SendMouseEvents__cctor_m6486_MethodInfo,
	&SendMouseEvents_DoSendMouseEvents_m6487_MethodInfo,
	&SendMouseEvents_SendEvents_m6488_MethodInfo,
	NULL
};
extern TypeInfo HitInfo_t1108_il2cpp_TypeInfo;
static TypeInfo* SendMouseEvents_t1110_il2cpp_TypeInfo__nestedTypes[2] =
{
	&HitInfo_t1108_il2cpp_TypeInfo,
	NULL
};
extern MethodInfo Object_ToString_m322_MethodInfo;
static MethodInfo* SendMouseEvents_t1110_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SendMouseEvents_t1110_0_0_0;
extern Il2CppType SendMouseEvents_t1110_1_0_0;
struct SendMouseEvents_t1110;
TypeInfo SendMouseEvents_t1110_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, SendMouseEvents_t1110_MethodInfos/* methods */
	, NULL/* properties */
	, SendMouseEvents_t1110_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, SendMouseEvents_t1110_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SendMouseEvents_t1110_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SendMouseEvents_t1110_il2cpp_TypeInfo/* cast_class */
	, &SendMouseEvents_t1110_0_0_0/* byval_arg */
	, &SendMouseEvents_t1110_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, SendMouseEvents_t1110_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t1110)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t1110_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ISocialPlatform_t1165_il2cpp_TypeInfo;

// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"


// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern Il2CppType ILocalUser_t972_0_0_0;
extern Il2CppType ILocalUser_t972_0_0_0;
extern Il2CppType Action_1_t802_0_0_0;
extern Il2CppType Action_1_t802_0_0_0;
static ParameterInfo ISocialPlatform_t1165_ISocialPlatform_Authenticate_m6683_ParameterInfos[] = 
{
	{"user", 0, 134219330, &EmptyCustomAttributesCache, &ILocalUser_t972_0_0_0},
	{"callback", 1, 134219331, &EmptyCustomAttributesCache, &Action_1_t802_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
MethodInfo ISocialPlatform_Authenticate_m6683_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1165_ISocialPlatform_Authenticate_m6683_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ILocalUser_t972_0_0_0;
extern Il2CppType Action_1_t802_0_0_0;
static ParameterInfo ISocialPlatform_t1165_ISocialPlatform_LoadFriends_m6684_ParameterInfos[] = 
{
	{"user", 0, 134219332, &EmptyCustomAttributesCache, &ILocalUser_t972_0_0_0},
	{"callback", 1, 134219333, &EmptyCustomAttributesCache, &Action_1_t802_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
MethodInfo ISocialPlatform_LoadFriends_m6684_MethodInfo = 
{
	"LoadFriends"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1165_ISocialPlatform_LoadFriends_m6684_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ISocialPlatform_t1165_MethodInfos[] =
{
	&ISocialPlatform_Authenticate_m6683_MethodInfo,
	&ISocialPlatform_LoadFriends_m6684_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ISocialPlatform_t1165_0_0_0;
extern Il2CppType ISocialPlatform_t1165_1_0_0;
struct ISocialPlatform_t1165;
TypeInfo ISocialPlatform_t1165_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ISocialPlatform_t1165_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ISocialPlatform_t1165_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ISocialPlatform_t1165_il2cpp_TypeInfo/* cast_class */
	, &ISocialPlatform_t1165_0_0_0/* byval_arg */
	, &ISocialPlatform_t1165_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern MethodInfo ILocalUser_get_authenticated_m6592_MethodInfo;
static PropertyInfo ILocalUser_t972____authenticated_PropertyInfo = 
{
	&ILocalUser_t972_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &ILocalUser_get_authenticated_m6592_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ILocalUser_t972_PropertyInfos[] =
{
	&ILocalUser_t972____authenticated_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
MethodInfo ILocalUser_get_authenticated_m6592_MethodInfo = 
{
	"get_authenticated"/* name */
	, NULL/* method */
	, &ILocalUser_t972_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILocalUser_t972_MethodInfos[] =
{
	&ILocalUser_get_authenticated_m6592_MethodInfo,
	NULL
};
static TypeInfo* ILocalUser_t972_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1162_il2cpp_TypeInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ILocalUser_t972_1_0_0;
struct ILocalUser_t972;
TypeInfo ILocalUser_t972_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILocalUser_t972_MethodInfos/* methods */
	, ILocalUser_t972_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ILocalUser_t972_il2cpp_TypeInfo/* element_class */
	, ILocalUser_t972_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ILocalUser_t972_il2cpp_TypeInfo/* cast_class */
	, &ILocalUser_t972_0_0_0/* byval_arg */
	, &ILocalUser_t972_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"



// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo UserState_t1111____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &UserState_t1111_il2cpp_TypeInfo/* parent */
	, offsetof(UserState_t1111, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1111_0_0_32854;
FieldInfo UserState_t1111____Online_2_FieldInfo = 
{
	"Online"/* name */
	, &UserState_t1111_0_0_32854/* type */
	, &UserState_t1111_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1111_0_0_32854;
FieldInfo UserState_t1111____OnlineAndAway_3_FieldInfo = 
{
	"OnlineAndAway"/* name */
	, &UserState_t1111_0_0_32854/* type */
	, &UserState_t1111_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1111_0_0_32854;
FieldInfo UserState_t1111____OnlineAndBusy_4_FieldInfo = 
{
	"OnlineAndBusy"/* name */
	, &UserState_t1111_0_0_32854/* type */
	, &UserState_t1111_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1111_0_0_32854;
FieldInfo UserState_t1111____Offline_5_FieldInfo = 
{
	"Offline"/* name */
	, &UserState_t1111_0_0_32854/* type */
	, &UserState_t1111_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1111_0_0_32854;
FieldInfo UserState_t1111____Playing_6_FieldInfo = 
{
	"Playing"/* name */
	, &UserState_t1111_0_0_32854/* type */
	, &UserState_t1111_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserState_t1111_FieldInfos[] =
{
	&UserState_t1111____value___1_FieldInfo,
	&UserState_t1111____Online_2_FieldInfo,
	&UserState_t1111____OnlineAndAway_3_FieldInfo,
	&UserState_t1111____OnlineAndBusy_4_FieldInfo,
	&UserState_t1111____Offline_5_FieldInfo,
	&UserState_t1111____Playing_6_FieldInfo,
	NULL
};
static const int32_t UserState_t1111____Online_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UserState_t1111____Online_2_DefaultValue = 
{
	&UserState_t1111____Online_2_FieldInfo/* field */
	, { (char*)&UserState_t1111____Online_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UserState_t1111____OnlineAndAway_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UserState_t1111____OnlineAndAway_3_DefaultValue = 
{
	&UserState_t1111____OnlineAndAway_3_FieldInfo/* field */
	, { (char*)&UserState_t1111____OnlineAndAway_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UserState_t1111____OnlineAndBusy_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UserState_t1111____OnlineAndBusy_4_DefaultValue = 
{
	&UserState_t1111____OnlineAndBusy_4_FieldInfo/* field */
	, { (char*)&UserState_t1111____OnlineAndBusy_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UserState_t1111____Offline_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry UserState_t1111____Offline_5_DefaultValue = 
{
	&UserState_t1111____Offline_5_FieldInfo/* field */
	, { (char*)&UserState_t1111____Offline_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UserState_t1111____Playing_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry UserState_t1111____Playing_6_DefaultValue = 
{
	&UserState_t1111____Playing_6_FieldInfo/* field */
	, { (char*)&UserState_t1111____Playing_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserState_t1111_FieldDefaultValues[] = 
{
	&UserState_t1111____Online_2_DefaultValue,
	&UserState_t1111____OnlineAndAway_3_DefaultValue,
	&UserState_t1111____OnlineAndBusy_4_DefaultValue,
	&UserState_t1111____Offline_5_DefaultValue,
	&UserState_t1111____Playing_6_DefaultValue,
	NULL
};
static MethodInfo* UserState_t1111_MethodInfos[] =
{
	NULL
};
static MethodInfo* UserState_t1111_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair UserState_t1111_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserState_t1111_1_0_0;
TypeInfo UserState_t1111_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t1111_MethodInfos/* methods */
	, NULL/* properties */
	, UserState_t1111_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserState_t1111_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &UserState_t1111_0_0_0/* byval_arg */
	, &UserState_t1111_1_0_0/* this_arg */
	, UserState_t1111_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserState_t1111_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t1111)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
static MethodInfo* IUserProfile_t1162_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IUserProfile_t1162_0_0_0;
extern Il2CppType IUserProfile_t1162_1_0_0;
struct IUserProfile_t1162;
TypeInfo IUserProfile_t1162_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t1162_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IUserProfile_t1162_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IUserProfile_t1162_il2cpp_TypeInfo/* cast_class */
	, &IUserProfile_t1162_0_0_0/* byval_arg */
	, &IUserProfile_t1162_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
static MethodInfo* IAchievement_t978_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievement_t978_0_0_0;
extern Il2CppType IAchievement_t978_1_0_0;
struct IAchievement_t978;
TypeInfo IAchievement_t978_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t978_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IAchievement_t978_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IAchievement_t978_il2cpp_TypeInfo/* cast_class */
	, &IAchievement_t978_0_0_0/* byval_arg */
	, &IAchievement_t978_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
static MethodInfo* IAchievementDescription_t1157_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievementDescription_t1157_0_0_0;
extern Il2CppType IAchievementDescription_t1157_1_0_0;
struct IAchievementDescription_t1157;
TypeInfo IAchievementDescription_t1157_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t1157_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IAchievementDescription_t1157_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IAchievementDescription_t1157_il2cpp_TypeInfo/* cast_class */
	, &IAchievementDescription_t1157_0_0_0/* byval_arg */
	, &IAchievementDescription_t1157_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.SocialPlatforms.IScore
static MethodInfo* IScore_t1105_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IScore_t1105_1_0_0;
struct IScore_t1105;
TypeInfo IScore_t1105_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t1105_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IScore_t1105_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IScore_t1105_il2cpp_TypeInfo/* cast_class */
	, &IScore_t1105_0_0_0/* byval_arg */
	, &IScore_t1105_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"



// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo UserScope_t1112____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &UserScope_t1112_il2cpp_TypeInfo/* parent */
	, offsetof(UserScope_t1112, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1112_0_0_32854;
FieldInfo UserScope_t1112____Global_2_FieldInfo = 
{
	"Global"/* name */
	, &UserScope_t1112_0_0_32854/* type */
	, &UserScope_t1112_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1112_0_0_32854;
FieldInfo UserScope_t1112____FriendsOnly_3_FieldInfo = 
{
	"FriendsOnly"/* name */
	, &UserScope_t1112_0_0_32854/* type */
	, &UserScope_t1112_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserScope_t1112_FieldInfos[] =
{
	&UserScope_t1112____value___1_FieldInfo,
	&UserScope_t1112____Global_2_FieldInfo,
	&UserScope_t1112____FriendsOnly_3_FieldInfo,
	NULL
};
static const int32_t UserScope_t1112____Global_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UserScope_t1112____Global_2_DefaultValue = 
{
	&UserScope_t1112____Global_2_FieldInfo/* field */
	, { (char*)&UserScope_t1112____Global_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UserScope_t1112____FriendsOnly_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UserScope_t1112____FriendsOnly_3_DefaultValue = 
{
	&UserScope_t1112____FriendsOnly_3_FieldInfo/* field */
	, { (char*)&UserScope_t1112____FriendsOnly_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserScope_t1112_FieldDefaultValues[] = 
{
	&UserScope_t1112____Global_2_DefaultValue,
	&UserScope_t1112____FriendsOnly_3_DefaultValue,
	NULL
};
static MethodInfo* UserScope_t1112_MethodInfos[] =
{
	NULL
};
static MethodInfo* UserScope_t1112_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair UserScope_t1112_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserScope_t1112_1_0_0;
TypeInfo UserScope_t1112_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t1112_MethodInfos/* methods */
	, NULL/* properties */
	, UserScope_t1112_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserScope_t1112_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &UserScope_t1112_0_0_0/* byval_arg */
	, &UserScope_t1112_1_0_0/* this_arg */
	, UserScope_t1112_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserScope_t1112_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t1112)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"



// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo TimeScope_t1113____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &TimeScope_t1113_il2cpp_TypeInfo/* parent */
	, offsetof(TimeScope_t1113, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1113_0_0_32854;
FieldInfo TimeScope_t1113____Today_2_FieldInfo = 
{
	"Today"/* name */
	, &TimeScope_t1113_0_0_32854/* type */
	, &TimeScope_t1113_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1113_0_0_32854;
FieldInfo TimeScope_t1113____Week_3_FieldInfo = 
{
	"Week"/* name */
	, &TimeScope_t1113_0_0_32854/* type */
	, &TimeScope_t1113_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1113_0_0_32854;
FieldInfo TimeScope_t1113____AllTime_4_FieldInfo = 
{
	"AllTime"/* name */
	, &TimeScope_t1113_0_0_32854/* type */
	, &TimeScope_t1113_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TimeScope_t1113_FieldInfos[] =
{
	&TimeScope_t1113____value___1_FieldInfo,
	&TimeScope_t1113____Today_2_FieldInfo,
	&TimeScope_t1113____Week_3_FieldInfo,
	&TimeScope_t1113____AllTime_4_FieldInfo,
	NULL
};
static const int32_t TimeScope_t1113____Today_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TimeScope_t1113____Today_2_DefaultValue = 
{
	&TimeScope_t1113____Today_2_FieldInfo/* field */
	, { (char*)&TimeScope_t1113____Today_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TimeScope_t1113____Week_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TimeScope_t1113____Week_3_DefaultValue = 
{
	&TimeScope_t1113____Week_3_FieldInfo/* field */
	, { (char*)&TimeScope_t1113____Week_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TimeScope_t1113____AllTime_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TimeScope_t1113____AllTime_4_DefaultValue = 
{
	&TimeScope_t1113____AllTime_4_FieldInfo/* field */
	, { (char*)&TimeScope_t1113____AllTime_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TimeScope_t1113_FieldDefaultValues[] = 
{
	&TimeScope_t1113____Today_2_DefaultValue,
	&TimeScope_t1113____Week_3_DefaultValue,
	&TimeScope_t1113____AllTime_4_DefaultValue,
	NULL
};
static MethodInfo* TimeScope_t1113_MethodInfos[] =
{
	NULL
};
static MethodInfo* TimeScope_t1113_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair TimeScope_t1113_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TimeScope_t1113_1_0_0;
TypeInfo TimeScope_t1113_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t1113_MethodInfos/* methods */
	, NULL/* properties */
	, TimeScope_t1113_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TimeScope_t1113_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &TimeScope_t1113_0_0_0/* byval_arg */
	, &TimeScope_t1113_1_0_0/* this_arg */
	, TimeScope_t1113_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TimeScope_t1113_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t1113)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
 void Range__ctor_m6489 (Range_t1107 * __this, int32_t ___fromValue, int32_t ___valueCount, MethodInfo* method){
	{
		__this->___from_0 = ___fromValue;
		__this->___count_1 = ___valueCount;
		return;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern Il2CppType Int32_t123_0_0_6;
FieldInfo Range_t1107____from_0_FieldInfo = 
{
	"from"/* name */
	, &Int32_t123_0_0_6/* type */
	, &Range_t1107_il2cpp_TypeInfo/* parent */
	, offsetof(Range_t1107, ___from_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo Range_t1107____count_1_FieldInfo = 
{
	"count"/* name */
	, &Int32_t123_0_0_6/* type */
	, &Range_t1107_il2cpp_TypeInfo/* parent */
	, offsetof(Range_t1107, ___count_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Range_t1107_FieldInfos[] =
{
	&Range_t1107____from_0_FieldInfo,
	&Range_t1107____count_1_FieldInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Range_t1107_Range__ctor_m6489_ParameterInfos[] = 
{
	{"fromValue", 0, 134219334, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"valueCount", 1, 134219335, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
MethodInfo Range__ctor_m6489_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m6489/* method */
	, &Range_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Range_t1107_Range__ctor_m6489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Range_t1107_MethodInfos[] =
{
	&Range__ctor_m6489_MethodInfo,
	NULL
};
static MethodInfo* Range_t1107_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Range_t1107_1_0_0;
TypeInfo Range_t1107_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t1107_MethodInfos/* methods */
	, NULL/* properties */
	, Range_t1107_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Range_t1107_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Range_t1107_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Range_t1107_il2cpp_TypeInfo/* cast_class */
	, &Range_t1107_0_0_0/* byval_arg */
	, &Range_t1107_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t1107)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof(Range_t1107 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, true/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern MethodInfo ILeaderboard_get_id_m6585_MethodInfo;
static PropertyInfo ILeaderboard_t977____id_PropertyInfo = 
{
	&ILeaderboard_t977_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m6585_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_userScope_m6588_MethodInfo;
static PropertyInfo ILeaderboard_t977____userScope_PropertyInfo = 
{
	&ILeaderboard_t977_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m6588_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_range_m6587_MethodInfo;
static PropertyInfo ILeaderboard_t977____range_PropertyInfo = 
{
	&ILeaderboard_t977_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m6587_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_timeScope_m6586_MethodInfo;
static PropertyInfo ILeaderboard_t977____timeScope_PropertyInfo = 
{
	&ILeaderboard_t977_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m6586_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ILeaderboard_t977_PropertyInfos[] =
{
	&ILeaderboard_t977____id_PropertyInfo,
	&ILeaderboard_t977____userScope_PropertyInfo,
	&ILeaderboard_t977____range_PropertyInfo,
	&ILeaderboard_t977____timeScope_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
MethodInfo ILeaderboard_get_id_m6585_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t977_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1112_0_0_0;
extern void* RuntimeInvoker_UserScope_t1112 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
MethodInfo ILeaderboard_get_userScope_m6588_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t977_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1112_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1112/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1107_0_0_0;
extern void* RuntimeInvoker_Range_t1107 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
MethodInfo ILeaderboard_get_range_m6587_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t977_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1107_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1107/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1113_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1113 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
MethodInfo ILeaderboard_get_timeScope_m6586_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t977_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1113_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1113/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILeaderboard_t977_MethodInfos[] =
{
	&ILeaderboard_get_id_m6585_MethodInfo,
	&ILeaderboard_get_userScope_m6588_MethodInfo,
	&ILeaderboard_get_range_m6587_MethodInfo,
	&ILeaderboard_get_timeScope_m6586_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ILeaderboard_t977_0_0_0;
extern Il2CppType ILeaderboard_t977_1_0_0;
struct ILeaderboard_t977;
TypeInfo ILeaderboard_t977_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t977_MethodInfos/* methods */
	, ILeaderboard_t977_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ILeaderboard_t977_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ILeaderboard_t977_il2cpp_TypeInfo/* cast_class */
	, &ILeaderboard_t977_0_0_0/* byval_arg */
	, &ILeaderboard_t977_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PropertyAttribute_t1114_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
extern MethodInfo Attribute__ctor_m4415_MethodInfo;


// System.Void UnityEngine.PropertyAttribute::.ctor()
extern MethodInfo PropertyAttribute__ctor_m6490_MethodInfo;
 void PropertyAttribute__ctor_m6490 (PropertyAttribute_t1114 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.PropertyAttribute
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
MethodInfo PropertyAttribute__ctor_m6490_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m6490/* method */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PropertyAttribute_t1114_MethodInfos[] =
{
	&PropertyAttribute__ctor_m6490_MethodInfo,
	NULL
};
extern MethodInfo Attribute_Equals_m4416_MethodInfo;
extern MethodInfo Attribute_GetHashCode_m4417_MethodInfo;
static MethodInfo* PropertyAttribute_t1114_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern TypeInfo _Attribute_t819_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair PropertyAttribute_t1114_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
extern TypeInfo AttributeUsageAttribute_t1213_il2cpp_TypeInfo;
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern MethodInfo AttributeUsageAttribute__ctor_m6676_MethodInfo;
extern MethodInfo AttributeUsageAttribute_set_Inherited_m6677_MethodInfo;
extern MethodInfo AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo;
void PropertyAttribute_t1114_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 256, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, true, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PropertyAttribute_t1114__CustomAttributeCache = {
1,
NULL,
&PropertyAttribute_t1114_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PropertyAttribute_t1114_0_0_0;
extern Il2CppType PropertyAttribute_t1114_1_0_0;
extern TypeInfo Attribute_t145_il2cpp_TypeInfo;
struct PropertyAttribute_t1114;
extern CustomAttributesCache PropertyAttribute_t1114__CustomAttributeCache;
TypeInfo PropertyAttribute_t1114_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t1114_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PropertyAttribute_t1114_VTable/* vtable */
	, &PropertyAttribute_t1114__CustomAttributeCache/* custom_attributes_cache */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* cast_class */
	, &PropertyAttribute_t1114_0_0_0/* byval_arg */
	, &PropertyAttribute_t1114_1_0_0/* this_arg */
	, PropertyAttribute_t1114_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t1114)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TooltipAttribute_t544_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"



// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern MethodInfo TooltipAttribute__ctor_m2599_MethodInfo;
 void TooltipAttribute__ctor_m2599 (TooltipAttribute_t544 * __this, String_t* ___tooltip, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6490(__this, /*hidden argument*/&PropertyAttribute__ctor_m6490_MethodInfo);
		__this->___tooltip_0 = ___tooltip;
		return;
	}
}
// Metadata Definition UnityEngine.TooltipAttribute
extern Il2CppType String_t_0_0_38;
FieldInfo TooltipAttribute_t544____tooltip_0_FieldInfo = 
{
	"tooltip"/* name */
	, &String_t_0_0_38/* type */
	, &TooltipAttribute_t544_il2cpp_TypeInfo/* parent */
	, offsetof(TooltipAttribute_t544, ___tooltip_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TooltipAttribute_t544_FieldInfos[] =
{
	&TooltipAttribute_t544____tooltip_0_FieldInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TooltipAttribute_t544_TooltipAttribute__ctor_m2599_ParameterInfos[] = 
{
	{"tooltip", 0, 134219336, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
MethodInfo TooltipAttribute__ctor_m2599_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m2599/* method */
	, &TooltipAttribute_t544_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TooltipAttribute_t544_TooltipAttribute__ctor_m2599_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TooltipAttribute_t544_MethodInfos[] =
{
	&TooltipAttribute__ctor_m2599_MethodInfo,
	NULL
};
static MethodInfo* TooltipAttribute_t544_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t544_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void TooltipAttribute_t544_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 256, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, true, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TooltipAttribute_t544__CustomAttributeCache = {
1,
NULL,
&TooltipAttribute_t544_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TooltipAttribute_t544_0_0_0;
extern Il2CppType TooltipAttribute_t544_1_0_0;
struct TooltipAttribute_t544;
extern CustomAttributesCache TooltipAttribute_t544__CustomAttributeCache;
TypeInfo TooltipAttribute_t544_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t544_MethodInfos/* methods */
	, NULL/* properties */
	, TooltipAttribute_t544_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TooltipAttribute_t544_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TooltipAttribute_t544_VTable/* vtable */
	, &TooltipAttribute_t544__CustomAttributeCache/* custom_attributes_cache */
	, &TooltipAttribute_t544_il2cpp_TypeInfo/* cast_class */
	, &TooltipAttribute_t544_0_0_0/* byval_arg */
	, &TooltipAttribute_t544_1_0_0/* this_arg */
	, TooltipAttribute_t544_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t544)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SpaceAttribute_t540_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"



// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern MethodInfo SpaceAttribute__ctor_m2548_MethodInfo;
 void SpaceAttribute__ctor_m2548 (SpaceAttribute_t540 * __this, float ___height, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6490(__this, /*hidden argument*/&PropertyAttribute__ctor_m6490_MethodInfo);
		__this->___height_0 = ___height;
		return;
	}
}
// Metadata Definition UnityEngine.SpaceAttribute
extern Il2CppType Single_t170_0_0_38;
FieldInfo SpaceAttribute_t540____height_0_FieldInfo = 
{
	"height"/* name */
	, &Single_t170_0_0_38/* type */
	, &SpaceAttribute_t540_il2cpp_TypeInfo/* parent */
	, offsetof(SpaceAttribute_t540, ___height_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SpaceAttribute_t540_FieldInfos[] =
{
	&SpaceAttribute_t540____height_0_FieldInfo,
	NULL
};
extern Il2CppType Single_t170_0_0_0;
extern Il2CppType Single_t170_0_0_0;
static ParameterInfo SpaceAttribute_t540_SpaceAttribute__ctor_m2548_ParameterInfos[] = 
{
	{"height", 0, 134219337, &EmptyCustomAttributesCache, &Single_t170_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Single_t170 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
MethodInfo SpaceAttribute__ctor_m2548_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m2548/* method */
	, &SpaceAttribute_t540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Single_t170/* invoker_method */
	, SpaceAttribute_t540_SpaceAttribute__ctor_m2548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SpaceAttribute_t540_MethodInfos[] =
{
	&SpaceAttribute__ctor_m2548_MethodInfo,
	NULL
};
static MethodInfo* SpaceAttribute_t540_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t540_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void SpaceAttribute_t540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 256, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, true, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, true, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SpaceAttribute_t540__CustomAttributeCache = {
1,
NULL,
&SpaceAttribute_t540_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SpaceAttribute_t540_0_0_0;
extern Il2CppType SpaceAttribute_t540_1_0_0;
struct SpaceAttribute_t540;
extern CustomAttributesCache SpaceAttribute_t540__CustomAttributeCache;
TypeInfo SpaceAttribute_t540_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t540_MethodInfos/* methods */
	, NULL/* properties */
	, SpaceAttribute_t540_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SpaceAttribute_t540_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SpaceAttribute_t540_VTable/* vtable */
	, &SpaceAttribute_t540__CustomAttributeCache/* custom_attributes_cache */
	, &SpaceAttribute_t540_il2cpp_TypeInfo/* cast_class */
	, &SpaceAttribute_t540_0_0_0/* byval_arg */
	, &SpaceAttribute_t540_1_0_0/* this_arg */
	, SpaceAttribute_t540_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t540)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RangeAttribute_t501_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"



// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern MethodInfo RangeAttribute__ctor_m2275_MethodInfo;
 void RangeAttribute__ctor_m2275 (RangeAttribute_t501 * __this, float ___min, float ___max, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6490(__this, /*hidden argument*/&PropertyAttribute__ctor_m6490_MethodInfo);
		__this->___min_0 = ___min;
		__this->___max_1 = ___max;
		return;
	}
}
// Metadata Definition UnityEngine.RangeAttribute
extern Il2CppType Single_t170_0_0_38;
FieldInfo RangeAttribute_t501____min_0_FieldInfo = 
{
	"min"/* name */
	, &Single_t170_0_0_38/* type */
	, &RangeAttribute_t501_il2cpp_TypeInfo/* parent */
	, offsetof(RangeAttribute_t501, ___min_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_38;
FieldInfo RangeAttribute_t501____max_1_FieldInfo = 
{
	"max"/* name */
	, &Single_t170_0_0_38/* type */
	, &RangeAttribute_t501_il2cpp_TypeInfo/* parent */
	, offsetof(RangeAttribute_t501, ___max_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* RangeAttribute_t501_FieldInfos[] =
{
	&RangeAttribute_t501____min_0_FieldInfo,
	&RangeAttribute_t501____max_1_FieldInfo,
	NULL
};
extern Il2CppType Single_t170_0_0_0;
extern Il2CppType Single_t170_0_0_0;
static ParameterInfo RangeAttribute_t501_RangeAttribute__ctor_m2275_ParameterInfos[] = 
{
	{"min", 0, 134219338, &EmptyCustomAttributesCache, &Single_t170_0_0_0},
	{"max", 1, 134219339, &EmptyCustomAttributesCache, &Single_t170_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Single_t170_Single_t170 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
MethodInfo RangeAttribute__ctor_m2275_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m2275/* method */
	, &RangeAttribute_t501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Single_t170_Single_t170/* invoker_method */
	, RangeAttribute_t501_RangeAttribute__ctor_m2275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RangeAttribute_t501_MethodInfos[] =
{
	&RangeAttribute__ctor_m2275_MethodInfo,
	NULL
};
static MethodInfo* RangeAttribute_t501_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t501_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void RangeAttribute_t501_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 256, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, true, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache RangeAttribute_t501__CustomAttributeCache = {
1,
NULL,
&RangeAttribute_t501_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RangeAttribute_t501_0_0_0;
extern Il2CppType RangeAttribute_t501_1_0_0;
struct RangeAttribute_t501;
extern CustomAttributesCache RangeAttribute_t501__CustomAttributeCache;
TypeInfo RangeAttribute_t501_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t501_MethodInfos/* methods */
	, NULL/* properties */
	, RangeAttribute_t501_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RangeAttribute_t501_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RangeAttribute_t501_VTable/* vtable */
	, &RangeAttribute_t501__CustomAttributeCache/* custom_attributes_cache */
	, &RangeAttribute_t501_il2cpp_TypeInfo/* cast_class */
	, &RangeAttribute_t501_0_0_0/* byval_arg */
	, &RangeAttribute_t501_1_0_0/* this_arg */
	, RangeAttribute_t501_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t501)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextAreaAttribute_t550_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"



// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern MethodInfo TextAreaAttribute__ctor_m2628_MethodInfo;
 void TextAreaAttribute__ctor_m2628 (TextAreaAttribute_t550 * __this, int32_t ___minLines, int32_t ___maxLines, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6490(__this, /*hidden argument*/&PropertyAttribute__ctor_m6490_MethodInfo);
		__this->___minLines_0 = ___minLines;
		__this->___maxLines_1 = ___maxLines;
		return;
	}
}
// Metadata Definition UnityEngine.TextAreaAttribute
extern Il2CppType Int32_t123_0_0_38;
FieldInfo TextAreaAttribute_t550____minLines_0_FieldInfo = 
{
	"minLines"/* name */
	, &Int32_t123_0_0_38/* type */
	, &TextAreaAttribute_t550_il2cpp_TypeInfo/* parent */
	, offsetof(TextAreaAttribute_t550, ___minLines_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_38;
FieldInfo TextAreaAttribute_t550____maxLines_1_FieldInfo = 
{
	"maxLines"/* name */
	, &Int32_t123_0_0_38/* type */
	, &TextAreaAttribute_t550_il2cpp_TypeInfo/* parent */
	, offsetof(TextAreaAttribute_t550, ___maxLines_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextAreaAttribute_t550_FieldInfos[] =
{
	&TextAreaAttribute_t550____minLines_0_FieldInfo,
	&TextAreaAttribute_t550____maxLines_1_FieldInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo TextAreaAttribute_t550_TextAreaAttribute__ctor_m2628_ParameterInfos[] = 
{
	{"minLines", 0, 134219340, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"maxLines", 1, 134219341, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
MethodInfo TextAreaAttribute__ctor_m2628_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m2628/* method */
	, &TextAreaAttribute_t550_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, TextAreaAttribute_t550_TextAreaAttribute__ctor_m2628_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextAreaAttribute_t550_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m2628_MethodInfo,
	NULL
};
static MethodInfo* TextAreaAttribute_t550_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t550_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void TextAreaAttribute_t550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 256, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, true, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TextAreaAttribute_t550__CustomAttributeCache = {
1,
NULL,
&TextAreaAttribute_t550_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextAreaAttribute_t550_0_0_0;
extern Il2CppType TextAreaAttribute_t550_1_0_0;
struct TextAreaAttribute_t550;
extern CustomAttributesCache TextAreaAttribute_t550__CustomAttributeCache;
TypeInfo TextAreaAttribute_t550_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t550_MethodInfos/* methods */
	, NULL/* properties */
	, TextAreaAttribute_t550_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1114_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextAreaAttribute_t550_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextAreaAttribute_t550_VTable/* vtable */
	, &TextAreaAttribute_t550__CustomAttributeCache/* custom_attributes_cache */
	, &TextAreaAttribute_t550_il2cpp_TypeInfo/* cast_class */
	, &TextAreaAttribute_t550_0_0_0/* byval_arg */
	, &TextAreaAttribute_t550_1_0_0/* this_arg */
	, TextAreaAttribute_t550_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t550)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SelectionBaseAttribute_t542_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"



// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern MethodInfo SelectionBaseAttribute__ctor_m2572_MethodInfo;
 void SelectionBaseAttribute__ctor_m2572 (SelectionBaseAttribute_t542 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
MethodInfo SelectionBaseAttribute__ctor_m2572_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m2572/* method */
	, &SelectionBaseAttribute_t542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SelectionBaseAttribute_t542_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m2572_MethodInfo,
	NULL
};
static MethodInfo* SelectionBaseAttribute_t542_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t542_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void SelectionBaseAttribute_t542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 4, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, true, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SelectionBaseAttribute_t542__CustomAttributeCache = {
1,
NULL,
&SelectionBaseAttribute_t542_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
extern Il2CppType SelectionBaseAttribute_t542_1_0_0;
struct SelectionBaseAttribute_t542;
extern CustomAttributesCache SelectionBaseAttribute_t542__CustomAttributeCache;
TypeInfo SelectionBaseAttribute_t542_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t542_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SelectionBaseAttribute_t542_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SelectionBaseAttribute_t542_VTable/* vtable */
	, &SelectionBaseAttribute_t542__CustomAttributeCache/* custom_attributes_cache */
	, &SelectionBaseAttribute_t542_il2cpp_TypeInfo/* cast_class */
	, &SelectionBaseAttribute_t542_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t542_1_0_0/* this_arg */
	, SelectionBaseAttribute_t542_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t542)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SliderState_t1115_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"



// System.Void UnityEngine.SliderState::.ctor()
extern MethodInfo SliderState__ctor_m6491_MethodInfo;
 void SliderState__ctor_m6491 (SliderState_t1115 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.SliderState
extern Il2CppType Single_t170_0_0_6;
FieldInfo SliderState_t1115____dragStartPos_0_FieldInfo = 
{
	"dragStartPos"/* name */
	, &Single_t170_0_0_6/* type */
	, &SliderState_t1115_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1115, ___dragStartPos_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo SliderState_t1115____dragStartValue_1_FieldInfo = 
{
	"dragStartValue"/* name */
	, &Single_t170_0_0_6/* type */
	, &SliderState_t1115_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1115, ___dragStartValue_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo SliderState_t1115____isDragging_2_FieldInfo = 
{
	"isDragging"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &SliderState_t1115_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1115, ___isDragging_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SliderState_t1115_FieldInfos[] =
{
	&SliderState_t1115____dragStartPos_0_FieldInfo,
	&SliderState_t1115____dragStartValue_1_FieldInfo,
	&SliderState_t1115____isDragging_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
MethodInfo SliderState__ctor_m6491_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m6491/* method */
	, &SliderState_t1115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SliderState_t1115_MethodInfos[] =
{
	&SliderState__ctor_m6491_MethodInfo,
	NULL
};
static MethodInfo* SliderState_t1115_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SliderState_t1115_0_0_0;
extern Il2CppType SliderState_t1115_1_0_0;
struct SliderState_t1115;
TypeInfo SliderState_t1115_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t1115_MethodInfos/* methods */
	, NULL/* properties */
	, SliderState_t1115_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SliderState_t1115_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SliderState_t1115_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SliderState_t1115_il2cpp_TypeInfo/* cast_class */
	, &SliderState_t1115_0_0_0/* byval_arg */
	, &SliderState_t1115_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t1115)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo StackTraceUtility_t1116_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"

// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTrace.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
extern TypeInfo StackTrace_t1117_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
extern TypeInfo Exception_t151_il2cpp_TypeInfo;
extern TypeInfo StringBuilder_t466_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo MemberInfo_t144_il2cpp_TypeInfo;
extern TypeInfo CharU5BU5D_t378_il2cpp_TypeInfo;
extern TypeInfo Char_t371_il2cpp_TypeInfo;
extern TypeInfo StackFrame_t1219_il2cpp_TypeInfo;
extern TypeInfo MethodBase_t1220_il2cpp_TypeInfo;
extern TypeInfo ParameterInfoU5BU5D_t1221_il2cpp_TypeInfo;
extern TypeInfo ParameterInfo_t1222_il2cpp_TypeInfo;
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
extern MethodInfo StackTrace__ctor_m6685_MethodInfo;
extern MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m6500_MethodInfo;
extern MethodInfo String_ToString_m6686_MethodInfo;
extern MethodInfo String_StartsWith_m6687_MethodInfo;
extern MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m6498_MethodInfo;
extern MethodInfo String_Concat_m287_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo Exception_get_StackTrace_m6613_MethodInfo;
extern MethodInfo String_get_Length_m2431_MethodInfo;
extern MethodInfo StringBuilder__ctor_m4769_MethodInfo;
extern MethodInfo Exception_GetType_m6614_MethodInfo;
extern MethodInfo MemberInfo_get_Name_m6688_MethodInfo;
extern MethodInfo Exception_get_Message_m673_MethodInfo;
extern MethodInfo String_Trim_m6689_MethodInfo;
extern MethodInfo String_Concat_m269_MethodInfo;
extern MethodInfo Exception_get_InnerException_m6611_MethodInfo;
extern MethodInfo String_Concat_m4744_MethodInfo;
extern MethodInfo StringBuilder_Append_m6690_MethodInfo;
extern MethodInfo StringBuilder_ToString_m2080_MethodInfo;
extern MethodInfo String_Split_m5524_MethodInfo;
extern MethodInfo String_get_Chars_m2456_MethodInfo;
extern MethodInfo StackTraceUtility_IsSystemStacktraceType_m6496_MethodInfo;
extern MethodInfo String_IndexOf_m6691_MethodInfo;
extern MethodInfo String_Substring_m2457_MethodInfo;
extern MethodInfo String_EndsWith_m4709_MethodInfo;
extern MethodInfo String_Remove_m2479_MethodInfo;
extern MethodInfo String_IndexOf_m6692_MethodInfo;
extern MethodInfo String_Replace_m6693_MethodInfo;
extern MethodInfo String_Replace_m6694_MethodInfo;
extern MethodInfo String_LastIndexOf_m6695_MethodInfo;
extern MethodInfo String_Insert_m2481_MethodInfo;
extern MethodInfo StackTrace_GetFrame_m6696_MethodInfo;
extern MethodInfo StackFrame_GetMethod_m6697_MethodInfo;
extern MethodInfo MemberInfo_get_DeclaringType_m6698_MethodInfo;
extern MethodInfo Type_get_Namespace_m6699_MethodInfo;
extern MethodInfo MethodBase_GetParameters_m6700_MethodInfo;
extern MethodInfo ParameterInfo_get_ParameterType_m6701_MethodInfo;
extern MethodInfo StackFrame_GetFileName_m6702_MethodInfo;
extern MethodInfo String_op_Equality_m562_MethodInfo;
extern MethodInfo StackFrame_GetFileLineNumber_m6703_MethodInfo;
extern MethodInfo Int32_ToString_m6704_MethodInfo;
extern MethodInfo StackTrace_get_FrameCount_m6705_MethodInfo;


// System.Void UnityEngine.StackTraceUtility::.ctor()
extern MethodInfo StackTraceUtility__ctor_m6492_MethodInfo;
 void StackTraceUtility__ctor_m6492 (StackTraceUtility_t1116 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern MethodInfo StackTraceUtility__cctor_m6493_MethodInfo;
 void StackTraceUtility__cctor_m6493 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern MethodInfo StackTraceUtility_SetProjectFolder_m6494_MethodInfo;
 void StackTraceUtility_SetProjectFolder_m6494 (Object_t * __this/* static, unused */, String_t* ___folder, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0 = ___folder;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern MethodInfo StackTraceUtility_ExtractStackTrace_m6495_MethodInfo;
 String_t* StackTraceUtility_ExtractStackTrace_m6495 (Object_t * __this/* static, unused */, MethodInfo* method){
	StackTrace_t1117 * V_0 = {0};
	String_t* V_1 = {0};
	{
		StackTrace_t1117 * L_0 = (StackTrace_t1117 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StackTrace_t1117_il2cpp_TypeInfo));
		StackTrace__ctor_m6685(L_0, 1, 1, /*hidden argument*/&StackTrace__ctor_m6685_MethodInfo);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		String_t* L_1 = StackTraceUtility_ExtractFormattedStackTrace_m6500(NULL /*static, unused*/, V_0, /*hidden argument*/&StackTraceUtility_ExtractFormattedStackTrace_m6500_MethodInfo);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&String_ToString_m6686_MethodInfo, L_1);
		V_1 = L_2;
		return V_1;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
 bool StackTraceUtility_IsSystemStacktraceType_m6496 (Object_t * __this/* static, unused */, Object_t * ___name, MethodInfo* method){
	String_t* V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		V_0 = ((String_t*)Castclass(___name, (&String_t_il2cpp_TypeInfo)));
		NullCheck(V_0);
		bool L_0 = String_StartsWith_m6687(V_0, (String_t*) &_stringLiteral438, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (L_0)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_1 = String_StartsWith_m6687(V_0, (String_t*) &_stringLiteral439, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (L_1)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_2 = String_StartsWith_m6687(V_0, (String_t*) &_stringLiteral440, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_3 = String_StartsWith_m6687(V_0, (String_t*) &_stringLiteral441, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (L_3)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_4 = String_StartsWith_m6687(V_0, (String_t*) &_stringLiteral442, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_5 = String_StartsWith_m6687(V_0, (String_t*) &_stringLiteral443, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		G_B7_0 = ((int32_t)(L_5));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern MethodInfo StackTraceUtility_ExtractStringFromException_m6497_MethodInfo;
 String_t* StackTraceUtility_ExtractStringFromException_m6497 (Object_t * __this/* static, unused */, Object_t * ___exception, MethodInfo* method){
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		V_0 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		V_1 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		StackTraceUtility_ExtractStringFromExceptionInternal_m6498(NULL /*static, unused*/, ___exception, (&V_0), (&V_1), /*hidden argument*/&StackTraceUtility_ExtractStringFromExceptionInternal_m6498_MethodInfo);
		String_t* L_0 = String_Concat_m287(NULL /*static, unused*/, V_0, (String_t*) &_stringLiteral347, V_1, /*hidden argument*/&String_Concat_m287_MethodInfo);
		return L_0;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
 void StackTraceUtility_ExtractStringFromExceptionInternal_m6498 (Object_t * __this/* static, unused */, Object_t * ___exceptiono, String_t** ___message, String_t** ___stackTrace, MethodInfo* method){
	Exception_t151 * V_0 = {0};
	StringBuilder_t466 * V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	StackTrace_t1117 * V_5 = {0};
	int32_t G_B7_0 = 0;
	{
		if (___exceptiono)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t551 * L_0 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_0, (String_t*) &_stringLiteral444, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		V_0 = ((Exception_t151 *)IsInst(___exceptiono, InitializedTypeInfo(&Exception_t151_il2cpp_TypeInfo)));
		if (V_0)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t551 * L_1 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_1, (String_t*) &_stringLiteral445, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0029:
	{
		NullCheck(V_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6613_MethodInfo, V_0);
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		NullCheck(V_0);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6613_MethodInfo, V_0);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2431(L_3, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		G_B7_0 = ((int32_t)((int32_t)L_4*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t466 * L_5 = (StringBuilder_t466 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StringBuilder_t466_il2cpp_TypeInfo));
		StringBuilder__ctor_m4769(L_5, G_B7_0, /*hidden argument*/&StringBuilder__ctor_m4769_MethodInfo);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		*((Object_t **)(___message)) = (Object_t *)(((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		V_2 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		goto IL_00ff;
	}

IL_0063:
	{
		NullCheck(V_2);
		int32_t L_6 = String_get_Length_m2431(V_2, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		if (L_6)
		{
			goto IL_007a;
		}
	}
	{
		NullCheck(V_0);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6613_MethodInfo, V_0);
		V_2 = L_7;
		goto IL_008c;
	}

IL_007a:
	{
		NullCheck(V_0);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6613_MethodInfo, V_0);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_9 = String_Concat_m287(NULL /*static, unused*/, L_8, (String_t*) &_stringLiteral347, V_2, /*hidden argument*/&String_Concat_m287_MethodInfo);
		V_2 = L_9;
	}

IL_008c:
	{
		NullCheck(V_0);
		Type_t * L_10 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&Exception_GetType_m6614_MethodInfo, V_0);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6688_MethodInfo, L_10);
		V_3 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		V_4 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		NullCheck(V_0);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_Message_m673_MethodInfo, V_0);
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		NullCheck(V_0);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_Message_m673_MethodInfo, V_0);
		V_4 = L_13;
	}

IL_00b2:
	{
		NullCheck(V_4);
		String_t* L_14 = String_Trim_m6689(V_4, /*hidden argument*/&String_Trim_m6689_MethodInfo);
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2431(L_14, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		if (!L_15)
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_16 = String_Concat_m269(NULL /*static, unused*/, V_3, (String_t*) &_stringLiteral446, /*hidden argument*/&String_Concat_m269_MethodInfo);
		V_3 = L_16;
		String_t* L_17 = String_Concat_m269(NULL /*static, unused*/, V_3, V_4, /*hidden argument*/&String_Concat_m269_MethodInfo);
		V_3 = L_17;
	}

IL_00d8:
	{
		*((Object_t **)(___message)) = (Object_t *)V_3;
		NullCheck(V_0);
		Exception_t151 * L_18 = (Exception_t151 *)VirtFuncInvoker0< Exception_t151 * >::Invoke(&Exception_get_InnerException_m6611_MethodInfo, V_0);
		if (!L_18)
		{
			goto IL_00f8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_19 = String_Concat_m4744(NULL /*static, unused*/, (String_t*) &_stringLiteral447, V_3, (String_t*) &_stringLiteral347, V_2, /*hidden argument*/&String_Concat_m4744_MethodInfo);
		V_2 = L_19;
	}

IL_00f8:
	{
		NullCheck(V_0);
		Exception_t151 * L_20 = (Exception_t151 *)VirtFuncInvoker0< Exception_t151 * >::Invoke(&Exception_get_InnerException_m6611_MethodInfo, V_0);
		V_0 = L_20;
	}

IL_00ff:
	{
		if (V_0)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_21 = String_Concat_m269(NULL /*static, unused*/, V_2, (String_t*) &_stringLiteral347, /*hidden argument*/&String_Concat_m269_MethodInfo);
		NullCheck(V_1);
		StringBuilder_Append_m6690(V_1, L_21, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		StackTrace_t1117 * L_22 = (StackTrace_t1117 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StackTrace_t1117_il2cpp_TypeInfo));
		StackTrace__ctor_m6685(L_22, 1, 1, /*hidden argument*/&StackTrace__ctor_m6685_MethodInfo);
		V_5 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		String_t* L_23 = StackTraceUtility_ExtractFormattedStackTrace_m6500(NULL /*static, unused*/, V_5, /*hidden argument*/&StackTraceUtility_ExtractFormattedStackTrace_m6500_MethodInfo);
		NullCheck(V_1);
		StringBuilder_Append_m6690(V_1, L_23, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_1);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StringBuilder_ToString_m2080_MethodInfo, V_1);
		*((Object_t **)(___stackTrace)) = (Object_t *)L_24;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern MethodInfo StackTraceUtility_PostprocessStacktrace_m6499_MethodInfo;
 String_t* StackTraceUtility_PostprocessStacktrace_m6499 (Object_t * __this/* static, unused */, String_t* ___oldString, bool ___stripEngineInternalInformation, MethodInfo* method){
	StringU5BU5D_t862* V_0 = {0};
	StringBuilder_t466 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		if (___oldString)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		return (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
	}

IL_000c:
	{
		CharU5BU5D_t378* L_0 = ((CharU5BU5D_t378*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t378_il2cpp_TypeInfo), 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0)) = (uint16_t)((int32_t)10);
		NullCheck(___oldString);
		StringU5BU5D_t862* L_1 = String_Split_m5524(___oldString, L_0, /*hidden argument*/&String_Split_m5524_MethodInfo);
		V_0 = L_1;
		NullCheck(___oldString);
		int32_t L_2 = String_get_Length_m2431(___oldString, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		StringBuilder_t466 * L_3 = (StringBuilder_t466 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StringBuilder_t466_il2cpp_TypeInfo));
		StringBuilder__ctor_m4769(L_3, L_2, /*hidden argument*/&StringBuilder__ctor_m4769_MethodInfo);
		V_1 = L_3;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_2);
		int32_t L_4 = V_2;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(V_0, L_4)));
		String_t* L_5 = String_Trim_m6689((*(String_t**)(String_t**)SZArrayLdElema(V_0, L_4)), /*hidden argument*/&String_Trim_m6689_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_2);
		ArrayElementTypeCheck (V_0, L_5);
		*((String_t**)(String_t**)SZArrayLdElema(V_0, V_2)) = (String_t*)L_5;
		V_2 = ((int32_t)(V_2+1));
	}

IL_0040:
	{
		NullCheck(V_0);
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_3);
		int32_t L_6 = V_3;
		V_4 = (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_6));
		NullCheck(V_4);
		int32_t L_7 = String_get_Length_m2431(V_4, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		if (!L_7)
		{
			goto IL_0070;
		}
	}
	{
		NullCheck(V_4);
		uint16_t L_8 = String_get_Chars_m2456(V_4, 0, /*hidden argument*/&String_get_Chars_m2456_MethodInfo);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)10))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		NullCheck(V_4);
		bool L_9 = String_StartsWith_m6687(V_4, (String_t*) &_stringLiteral448, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (!L_9)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		if (!___stripEngineInternalInformation)
		{
			goto IL_00a7;
		}
	}
	{
		NullCheck(V_4);
		bool L_10 = String_StartsWith_m6687(V_4, (String_t*) &_stringLiteral449, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		if (!___stripEngineInternalInformation)
		{
			goto IL_00fa;
		}
	}
	{
		NullCheck(V_0);
		if ((((int32_t)V_3) >= ((int32_t)((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		bool L_11 = StackTraceUtility_IsSystemStacktraceType_m6496(NULL /*static, unused*/, V_4, /*hidden argument*/&StackTraceUtility_IsSystemStacktraceType_m6496_MethodInfo);
		if (!L_11)
		{
			goto IL_00fa;
		}
	}
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ((int32_t)(V_3+1)));
		int32_t L_12 = ((int32_t)(V_3+1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		bool L_13 = StackTraceUtility_IsSystemStacktraceType_m6496(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_12)), /*hidden argument*/&StackTraceUtility_IsSystemStacktraceType_m6496_MethodInfo);
		if (!L_13)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		NullCheck(V_4);
		int32_t L_14 = String_IndexOf_m6691(V_4, (String_t*) &_stringLiteral450, /*hidden argument*/&String_IndexOf_m6691_MethodInfo);
		V_5 = L_14;
		if ((((int32_t)V_5) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_15 = String_Substring_m2457(V_4, 0, V_5, /*hidden argument*/&String_Substring_m2457_MethodInfo);
		V_4 = L_15;
	}

IL_00fa:
	{
		NullCheck(V_4);
		int32_t L_16 = String_IndexOf_m6691(V_4, (String_t*) &_stringLiteral451, /*hidden argument*/&String_IndexOf_m6691_MethodInfo);
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		NullCheck(V_4);
		int32_t L_17 = String_IndexOf_m6691(V_4, (String_t*) &_stringLiteral452, /*hidden argument*/&String_IndexOf_m6691_MethodInfo);
		if ((((int32_t)L_17) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		NullCheck(V_4);
		int32_t L_18 = String_IndexOf_m6691(V_4, (String_t*) &_stringLiteral453, /*hidden argument*/&String_IndexOf_m6691_MethodInfo);
		if ((((int32_t)L_18) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		if (!___stripEngineInternalInformation)
		{
			goto IL_016c;
		}
	}
	{
		NullCheck(V_4);
		bool L_19 = String_StartsWith_m6687(V_4, (String_t*) &_stringLiteral454, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (!L_19)
		{
			goto IL_016c;
		}
	}
	{
		NullCheck(V_4);
		bool L_20 = String_EndsWith_m4709(V_4, (String_t*) &_stringLiteral455, /*hidden argument*/&String_EndsWith_m4709_MethodInfo);
		if (!L_20)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		NullCheck(V_4);
		bool L_21 = String_StartsWith_m6687(V_4, (String_t*) &_stringLiteral456, /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (!L_21)
		{
			goto IL_0188;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_22 = String_Remove_m2479(V_4, 0, 3, /*hidden argument*/&String_Remove_m2479_MethodInfo);
		V_4 = L_22;
	}

IL_0188:
	{
		NullCheck(V_4);
		int32_t L_23 = String_IndexOf_m6691(V_4, (String_t*) &_stringLiteral457, /*hidden argument*/&String_IndexOf_m6691_MethodInfo);
		V_6 = L_23;
		V_7 = (-1);
		if ((((int32_t)V_6) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		NullCheck(V_4);
		int32_t L_24 = String_IndexOf_m6692(V_4, (String_t*) &_stringLiteral455, V_6, /*hidden argument*/&String_IndexOf_m6692_MethodInfo);
		V_7 = L_24;
	}

IL_01b1:
	{
		if ((((int32_t)V_6) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_6)))
		{
			goto IL_01d4;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_25 = String_Remove_m2479(V_4, V_6, ((int32_t)(((int32_t)(V_7-V_6))+1)), /*hidden argument*/&String_Remove_m2479_MethodInfo);
		V_4 = L_25;
	}

IL_01d4:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		NullCheck(V_4);
		String_t* L_26 = String_Replace_m6693(V_4, (String_t*) &_stringLiteral458, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m6693_MethodInfo);
		V_4 = L_26;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		NullCheck(V_4);
		String_t* L_27 = String_Replace_m6693(V_4, (((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m6693_MethodInfo);
		V_4 = L_27;
		NullCheck(V_4);
		String_t* L_28 = String_Replace_m6694(V_4, ((int32_t)92), ((int32_t)47), /*hidden argument*/&String_Replace_m6694_MethodInfo);
		V_4 = L_28;
		NullCheck(V_4);
		int32_t L_29 = String_LastIndexOf_m6695(V_4, (String_t*) &_stringLiteral459, /*hidden argument*/&String_LastIndexOf_m6695_MethodInfo);
		V_8 = L_29;
		if ((((int32_t)V_8) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_30 = String_Remove_m2479(V_4, V_8, 5, /*hidden argument*/&String_Remove_m2479_MethodInfo);
		V_4 = L_30;
		NullCheck(V_4);
		String_t* L_31 = String_Insert_m2481(V_4, V_8, (String_t*) &_stringLiteral460, /*hidden argument*/&String_Insert_m2481_MethodInfo);
		V_4 = L_31;
		NullCheck(V_4);
		int32_t L_32 = String_get_Length_m2431(V_4, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		NullCheck(V_4);
		String_t* L_33 = String_Insert_m2481(V_4, L_32, (String_t*) &_stringLiteral132, /*hidden argument*/&String_Insert_m2481_MethodInfo);
		V_4 = L_33;
	}

IL_024e:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_34 = String_Concat_m269(NULL /*static, unused*/, V_4, (String_t*) &_stringLiteral347, /*hidden argument*/&String_Concat_m269_MethodInfo);
		NullCheck(V_1);
		StringBuilder_Append_m6690(V_1, L_34, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
	}

IL_0261:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0265:
	{
		NullCheck(V_0);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		NullCheck(V_1);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StringBuilder_ToString_m2080_MethodInfo, V_1);
		return L_35;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
 String_t* StackTraceUtility_ExtractFormattedStackTrace_m6500 (Object_t * __this/* static, unused */, StackTrace_t1117 * ___stackTrace, MethodInfo* method){
	StringBuilder_t466 * V_0 = {0};
	int32_t V_1 = 0;
	StackFrame_t1219 * V_2 = {0};
	MethodBase_t1220 * V_3 = {0};
	Type_t * V_4 = {0};
	String_t* V_5 = {0};
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t1221* V_7 = {0};
	bool V_8 = false;
	String_t* V_9 = {0};
	int32_t V_10 = 0;
	{
		StringBuilder_t466 * L_0 = (StringBuilder_t466 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StringBuilder_t466_il2cpp_TypeInfo));
		StringBuilder__ctor_m4769(L_0, ((int32_t)255), /*hidden argument*/&StringBuilder__ctor_m4769_MethodInfo);
		V_0 = L_0;
		V_1 = 0;
		goto IL_01c9;
	}

IL_0012:
	{
		NullCheck(___stackTrace);
		StackFrame_t1219 * L_1 = (StackFrame_t1219 *)VirtFuncInvoker1< StackFrame_t1219 *, int32_t >::Invoke(&StackTrace_GetFrame_m6696_MethodInfo, ___stackTrace, V_1);
		V_2 = L_1;
		NullCheck(V_2);
		MethodBase_t1220 * L_2 = (MethodBase_t1220 *)VirtFuncInvoker0< MethodBase_t1220 * >::Invoke(&StackFrame_GetMethod_m6697_MethodInfo, V_2);
		V_3 = L_2;
		if (V_3)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_01c5;
	}

IL_002c:
	{
		NullCheck(V_3);
		Type_t * L_3 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&MemberInfo_get_DeclaringType_m6698_MethodInfo, V_3);
		V_4 = L_3;
		if (V_4)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_01c5;
	}

IL_0040:
	{
		NullCheck(V_4);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_Namespace_m6699_MethodInfo, V_4);
		V_5 = L_4;
		if (!V_5)
		{
			goto IL_0071;
		}
	}
	{
		NullCheck(V_5);
		int32_t L_5 = String_get_Length_m2431(V_5, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, V_5, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral124, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
	}

IL_0071:
	{
		NullCheck(V_4);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6688_MethodInfo, V_4);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, L_6, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral461, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_3);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6688_MethodInfo, V_3);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, L_7, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral462, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		V_6 = 0;
		NullCheck(V_3);
		ParameterInfoU5BU5D_t1221* L_8 = (ParameterInfoU5BU5D_t1221*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1221* >::Invoke(&MethodBase_GetParameters_m6700_MethodInfo, V_3);
		V_7 = L_8;
		V_8 = 1;
		goto IL_00ee;
	}

IL_00b7:
	{
		if (V_8)
		{
			goto IL_00cf;
		}
	}
	{
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral463, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = 0;
	}

IL_00d2:
	{
		NullCheck(V_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_7, V_6);
		int32_t L_9 = V_6;
		NullCheck((*(ParameterInfo_t1222 **)(ParameterInfo_t1222 **)SZArrayLdElema(V_7, L_9)));
		Type_t * L_10 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&ParameterInfo_get_ParameterType_m6701_MethodInfo, (*(ParameterInfo_t1222 **)(ParameterInfo_t1222 **)SZArrayLdElema(V_7, L_9)));
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6688_MethodInfo, L_10);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, L_11, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		V_6 = ((int32_t)(V_6+1));
	}

IL_00ee:
	{
		NullCheck(V_7);
		if ((((int32_t)V_6) < ((int32_t)(((int32_t)(((Array_t *)V_7)->max_length))))))
		{
			goto IL_00b7;
		}
	}
	{
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral132, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_2);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StackFrame_GetFileName_m6702_MethodInfo, V_2);
		V_9 = L_12;
		if (!V_9)
		{
			goto IL_01b9;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6688_MethodInfo, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_14 = String_op_Equality_m562(NULL /*static, unused*/, L_13, (String_t*) &_stringLiteral464, /*hidden argument*/&String_op_Equality_m562_MethodInfo);
		if (!L_14)
		{
			goto IL_0140;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_Namespace_m6699_MethodInfo, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_16 = String_op_Equality_m562(NULL /*static, unused*/, L_15, (String_t*) &_stringLiteral465, /*hidden argument*/&String_op_Equality_m562_MethodInfo);
		if (L_16)
		{
			goto IL_01b9;
		}
	}

IL_0140:
	{
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral460, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		NullCheck(V_9);
		bool L_17 = String_StartsWith_m6687(V_9, (((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), /*hidden argument*/&String_StartsWith_m6687_MethodInfo);
		if (!L_17)
		{
			goto IL_0182;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo));
		NullCheck((((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0));
		int32_t L_18 = String_get_Length_m2431((((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		NullCheck(V_9);
		int32_t L_19 = String_get_Length_m2431(V_9, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		NullCheck((((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0));
		int32_t L_20 = String_get_Length_m2431((((StackTraceUtility_t1116_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1116_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		NullCheck(V_9);
		String_t* L_21 = String_Substring_m2457(V_9, L_18, ((int32_t)(L_19-L_20)), /*hidden argument*/&String_Substring_m2457_MethodInfo);
		V_9 = L_21;
	}

IL_0182:
	{
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, V_9, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral461, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_2);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&StackFrame_GetFileLineNumber_m6703_MethodInfo, V_2);
		V_10 = L_22;
		String_t* L_23 = Int32_ToString_m6704((&V_10), /*hidden argument*/&Int32_ToString_m6704_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, L_23, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral132, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
	}

IL_01b9:
	{
		NullCheck(V_0);
		StringBuilder_Append_m6690(V_0, (String_t*) &_stringLiteral347, /*hidden argument*/&StringBuilder_Append_m6690_MethodInfo);
	}

IL_01c5:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_01c9:
	{
		NullCheck(___stackTrace);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&StackTrace_get_FrameCount_m6705_MethodInfo, ___stackTrace);
		if ((((int32_t)V_1) < ((int32_t)L_24)))
		{
			goto IL_0012;
		}
	}
	{
		NullCheck(V_0);
		String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StringBuilder_ToString_m2080_MethodInfo, V_0);
		return L_25;
	}
}
// Metadata Definition UnityEngine.StackTraceUtility
extern Il2CppType String_t_0_0_17;
FieldInfo StackTraceUtility_t1116____projectFolder_0_FieldInfo = 
{
	"projectFolder"/* name */
	, &String_t_0_0_17/* type */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* parent */
	, offsetof(StackTraceUtility_t1116_StaticFields, ___projectFolder_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* StackTraceUtility_t1116_FieldInfos[] =
{
	&StackTraceUtility_t1116____projectFolder_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
MethodInfo StackTraceUtility__ctor_m6492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m6492/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
MethodInfo StackTraceUtility__cctor_m6493_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m6493/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo StackTraceUtility_t1116_StackTraceUtility_SetProjectFolder_m6494_ParameterInfos[] = 
{
	{"folder", 0, 134219342, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
MethodInfo StackTraceUtility_SetProjectFolder_m6494_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m6494/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, StackTraceUtility_t1116_StackTraceUtility_SetProjectFolder_m6494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6495;
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
MethodInfo StackTraceUtility_ExtractStackTrace_m6495_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m6495/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6495/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StackTraceUtility_t1116_StackTraceUtility_IsSystemStacktraceType_m6496_ParameterInfos[] = 
{
	{"name", 0, 134219343, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
MethodInfo StackTraceUtility_IsSystemStacktraceType_m6496_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m6496/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, StackTraceUtility_t1116_StackTraceUtility_IsSystemStacktraceType_m6496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StackTraceUtility_t1116_StackTraceUtility_ExtractStringFromException_m6497_ParameterInfos[] = 
{
	{"exception", 0, 134219344, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
MethodInfo StackTraceUtility_ExtractStringFromException_m6497_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m6497/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1116_StackTraceUtility_ExtractStringFromException_m6497_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_1_0_2;
extern Il2CppType String_t_1_0_0;
extern Il2CppType String_t_1_0_2;
static ParameterInfo StackTraceUtility_t1116_StackTraceUtility_ExtractStringFromExceptionInternal_m6498_ParameterInfos[] = 
{
	{"exceptiono", 0, 134219345, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"message", 1, 134219346, &EmptyCustomAttributesCache, &String_t_1_0_2},
	{"stackTrace", 2, 134219347, &EmptyCustomAttributesCache, &String_t_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_StringU26_t1223_StringU26_t1223 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6498;
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m6498_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m6498/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_StringU26_t1223_StringU26_t1223/* invoker_method */
	, StackTraceUtility_t1116_StackTraceUtility_ExtractStringFromExceptionInternal_m6498_ParameterInfos/* parameters */
	, &StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6498/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo StackTraceUtility_t1116_StackTraceUtility_PostprocessStacktrace_m6499_ParameterInfos[] = 
{
	{"oldString", 0, 134219348, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134219349, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
MethodInfo StackTraceUtility_PostprocessStacktrace_m6499_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m6499/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t125/* invoker_method */
	, StackTraceUtility_t1116_StackTraceUtility_PostprocessStacktrace_m6499_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StackTrace_t1117_0_0_0;
extern Il2CppType StackTrace_t1117_0_0_0;
static ParameterInfo StackTraceUtility_t1116_StackTraceUtility_ExtractFormattedStackTrace_m6500_ParameterInfos[] = 
{
	{"stackTrace", 0, 134219350, &EmptyCustomAttributesCache, &StackTrace_t1117_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6500;
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m6500_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m6500/* method */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1116_StackTraceUtility_ExtractFormattedStackTrace_m6500_ParameterInfos/* parameters */
	, &StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6500/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StackTraceUtility_t1116_MethodInfos[] =
{
	&StackTraceUtility__ctor_m6492_MethodInfo,
	&StackTraceUtility__cctor_m6493_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m6494_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m6495_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m6496_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m6497_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m6498_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m6499_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m6500_MethodInfo,
	NULL
};
static MethodInfo* StackTraceUtility_t1116_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern TypeInfo SecuritySafeCriticalAttribute_t1196_il2cpp_TypeInfo;
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern MethodInfo SecuritySafeCriticalAttribute__ctor_m6651_MethodInfo;
void StackTraceUtility_t1116_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6495(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1196 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1196 *)il2cpp_codegen_object_new (&SecuritySafeCriticalAttribute_t1196_il2cpp_TypeInfo);
		SecuritySafeCriticalAttribute__ctor_m6651(tmp, &SecuritySafeCriticalAttribute__ctor_m6651_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void StackTraceUtility_t1116_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6498(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1196 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1196 *)il2cpp_codegen_object_new (&SecuritySafeCriticalAttribute_t1196_il2cpp_TypeInfo);
		SecuritySafeCriticalAttribute__ctor_m6651(tmp, &SecuritySafeCriticalAttribute__ctor_m6651_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void StackTraceUtility_t1116_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6500(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1196 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1196 *)il2cpp_codegen_object_new (&SecuritySafeCriticalAttribute_t1196_il2cpp_TypeInfo);
		SecuritySafeCriticalAttribute__ctor_m6651(tmp, &SecuritySafeCriticalAttribute__ctor_m6651_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6495 = {
1,
NULL,
&StackTraceUtility_t1116_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6495
};
CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6498 = {
1,
NULL,
&StackTraceUtility_t1116_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6498
};
CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6500 = {
1,
NULL,
&StackTraceUtility_t1116_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6500
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType StackTraceUtility_t1116_0_0_0;
extern Il2CppType StackTraceUtility_t1116_1_0_0;
struct StackTraceUtility_t1116;
extern CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6495;
extern CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6498;
extern CustomAttributesCache StackTraceUtility_t1116__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6500;
TypeInfo StackTraceUtility_t1116_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t1116_MethodInfos/* methods */
	, NULL/* properties */
	, StackTraceUtility_t1116_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, StackTraceUtility_t1116_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &StackTraceUtility_t1116_il2cpp_TypeInfo/* cast_class */
	, &StackTraceUtility_t1116_0_0_0/* byval_arg */
	, &StackTraceUtility_t1116_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t1116)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t1116_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityException_t520_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"

// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
extern MethodInfo Exception__ctor_m6662_MethodInfo;
extern MethodInfo Exception_set_HResult_m6706_MethodInfo;
extern MethodInfo Exception__ctor_m6707_MethodInfo;
extern MethodInfo Exception__ctor_m6708_MethodInfo;


// System.Void UnityEngine.UnityException::.ctor()
extern MethodInfo UnityException__ctor_m6501_MethodInfo;
 void UnityException__ctor_m6501 (UnityException_t520 * __this, MethodInfo* method){
	{
		Exception__ctor_m6662(__this, (String_t*) &_stringLiteral466, /*hidden argument*/&Exception__ctor_m6662_MethodInfo);
		Exception_set_HResult_m6706(__this, ((int32_t)-2147467261), /*hidden argument*/&Exception_set_HResult_m6706_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern MethodInfo UnityException__ctor_m6502_MethodInfo;
 void UnityException__ctor_m6502 (UnityException_t520 * __this, String_t* ___message, MethodInfo* method){
	{
		Exception__ctor_m6662(__this, ___message, /*hidden argument*/&Exception__ctor_m6662_MethodInfo);
		Exception_set_HResult_m6706(__this, ((int32_t)-2147467261), /*hidden argument*/&Exception_set_HResult_m6706_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern MethodInfo UnityException__ctor_m6503_MethodInfo;
 void UnityException__ctor_m6503 (UnityException_t520 * __this, String_t* ___message, Exception_t151 * ___innerException, MethodInfo* method){
	{
		Exception__ctor_m6707(__this, ___message, ___innerException, /*hidden argument*/&Exception__ctor_m6707_MethodInfo);
		Exception_set_HResult_m6706(__this, ((int32_t)-2147467261), /*hidden argument*/&Exception_set_HResult_m6706_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern MethodInfo UnityException__ctor_m6504_MethodInfo;
 void UnityException__ctor_m6504 (UnityException_t520 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method){
	{
		Exception__ctor_m6708(__this, ___info, ___context, /*hidden argument*/&Exception__ctor_m6708_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.UnityException
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo UnityException_t520____Result_11_FieldInfo = 
{
	"Result"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &UnityException_t520_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo UnityException_t520____unityStackTrace_12_FieldInfo = 
{
	"unityStackTrace"/* name */
	, &String_t_0_0_1/* type */
	, &UnityException_t520_il2cpp_TypeInfo/* parent */
	, offsetof(UnityException_t520, ___unityStackTrace_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityException_t520_FieldInfos[] =
{
	&UnityException_t520____Result_11_FieldInfo,
	&UnityException_t520____unityStackTrace_12_FieldInfo,
	NULL
};
static const int32_t UnityException_t520____Result_11_DefaultValueData = -2147467261;
static Il2CppFieldDefaultValueEntry UnityException_t520____Result_11_DefaultValue = 
{
	&UnityException_t520____Result_11_FieldInfo/* field */
	, { (char*)&UnityException_t520____Result_11_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UnityException_t520_FieldDefaultValues[] = 
{
	&UnityException_t520____Result_11_DefaultValue,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
MethodInfo UnityException__ctor_m6501_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6501/* method */
	, &UnityException_t520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityException_t520_UnityException__ctor_m6502_ParameterInfos[] = 
{
	{"message", 0, 134219351, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
MethodInfo UnityException__ctor_m6502_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6502/* method */
	, &UnityException_t520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityException_t520_UnityException__ctor_m6502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Exception_t151_0_0_0;
extern Il2CppType Exception_t151_0_0_0;
static ParameterInfo UnityException_t520_UnityException__ctor_m6503_ParameterInfos[] = 
{
	{"message", 0, 134219352, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"innerException", 1, 134219353, &EmptyCustomAttributesCache, &Exception_t151_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
MethodInfo UnityException__ctor_m6503_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6503/* method */
	, &UnityException_t520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, UnityException_t520_UnityException__ctor_m6503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
static ParameterInfo UnityException_t520_UnityException__ctor_m6504_ParameterInfos[] = 
{
	{"info", 0, 134219354, &EmptyCustomAttributesCache, &SerializationInfo_t1118_0_0_0},
	{"context", 1, 134219355, &EmptyCustomAttributesCache, &StreamingContext_t1119_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_StreamingContext_t1119 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo UnityException__ctor_m6504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6504/* method */
	, &UnityException_t520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_StreamingContext_t1119/* invoker_method */
	, UnityException_t520_UnityException__ctor_m6504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityException_t520_MethodInfos[] =
{
	&UnityException__ctor_m6501_MethodInfo,
	&UnityException__ctor_m6502_MethodInfo,
	&UnityException__ctor_m6503_MethodInfo,
	&UnityException__ctor_m6504_MethodInfo,
	NULL
};
extern MethodInfo Exception_ToString_m6609_MethodInfo;
extern MethodInfo Exception_GetObjectData_m6610_MethodInfo;
extern MethodInfo Exception_get_Source_m6612_MethodInfo;
static MethodInfo* UnityException_t520_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Exception_ToString_m6609_MethodInfo,
	&Exception_GetObjectData_m6610_MethodInfo,
	&Exception_get_InnerException_m6611_MethodInfo,
	&Exception_get_Message_m673_MethodInfo,
	&Exception_get_Source_m6612_MethodInfo,
	&Exception_get_StackTrace_m6613_MethodInfo,
	&Exception_GetObjectData_m6610_MethodInfo,
	&Exception_GetType_m6614_MethodInfo,
};
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
extern TypeInfo _Exception_t1171_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityException_t520_InterfacesOffsets[] = 
{
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
	{ &_Exception_t1171_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityException_t520_0_0_0;
extern Il2CppType UnityException_t520_1_0_0;
struct UnityException_t520;
TypeInfo UnityException_t520_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t520_MethodInfos/* methods */
	, NULL/* properties */
	, UnityException_t520_FieldInfos/* fields */
	, NULL/* events */
	, &Exception_t151_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityException_t520_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityException_t520_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityException_t520_il2cpp_TypeInfo/* cast_class */
	, &UnityException_t520_0_0_0/* byval_arg */
	, &UnityException_t520_1_0_0/* this_arg */
	, UnityException_t520_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UnityException_t520_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t520)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"



// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern MethodInfo SharedBetweenAnimatorsAttribute__ctor_m6505_MethodInfo;
 void SharedBetweenAnimatorsAttribute__ctor_m6505 (SharedBetweenAnimatorsAttribute_t1120 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
MethodInfo SharedBetweenAnimatorsAttribute__ctor_m6505_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m6505/* method */
	, &SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SharedBetweenAnimatorsAttribute_t1120_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m6505_MethodInfo,
	NULL
};
static MethodInfo* SharedBetweenAnimatorsAttribute_t1120_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t1120_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void SharedBetweenAnimatorsAttribute_t1120_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 4, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SharedBetweenAnimatorsAttribute_t1120__CustomAttributeCache = {
1,
NULL,
&SharedBetweenAnimatorsAttribute_t1120_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_1_0_0;
struct SharedBetweenAnimatorsAttribute_t1120;
extern CustomAttributesCache SharedBetweenAnimatorsAttribute_t1120__CustomAttributeCache;
TypeInfo SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t1120_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SharedBetweenAnimatorsAttribute_t1120_VTable/* vtable */
	, &SharedBetweenAnimatorsAttribute_t1120__CustomAttributeCache/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo/* cast_class */
	, &SharedBetweenAnimatorsAttribute_t1120_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t1120_1_0_0/* this_arg */
	, SharedBetweenAnimatorsAttribute_t1120_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t1120)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo StateMachineBehaviour_t1121_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
extern MethodInfo ScriptableObject__ctor_m5588_MethodInfo;


// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern MethodInfo StateMachineBehaviour__ctor_m6506_MethodInfo;
 void StateMachineBehaviour__ctor_m6506 (StateMachineBehaviour_t1121 * __this, MethodInfo* method){
	{
		ScriptableObject__ctor_m5588(__this, /*hidden argument*/&ScriptableObject__ctor_m5588_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateEnter_m6507_MethodInfo;
 void StateMachineBehaviour_OnStateEnter_m6507 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateUpdate_m6508_MethodInfo;
 void StateMachineBehaviour_OnStateUpdate_m6508 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateExit_m6509_MethodInfo;
 void StateMachineBehaviour_OnStateExit_m6509 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateMove_m6510_MethodInfo;
 void StateMachineBehaviour_OnStateMove_m6510 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateIK_m6511_MethodInfo;
 void StateMachineBehaviour_OnStateIK_m6511 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6512_MethodInfo;
 void StateMachineBehaviour_OnStateMachineEnter_m6512 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateMachineExit_m6513_MethodInfo;
 void StateMachineBehaviour_OnStateMachineExit_m6513 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition UnityEngine.StateMachineBehaviour
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
MethodInfo StateMachineBehaviour__ctor_m6506_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m6506/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType AnimatorStateInfo_t1064_0_0_0;
extern Il2CppType AnimatorStateInfo_t1064_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateEnter_m6507_ParameterInfos[] = 
{
	{"animator", 0, 134219356, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateInfo", 1, 134219357, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1064_0_0_0},
	{"layerIndex", 2, 134219358, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateEnter_m6507_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m6507/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateEnter_m6507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType AnimatorStateInfo_t1064_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateUpdate_m6508_ParameterInfos[] = 
{
	{"animator", 0, 134219359, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateInfo", 1, 134219360, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1064_0_0_0},
	{"layerIndex", 2, 134219361, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateUpdate_m6508_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m6508/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateUpdate_m6508_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType AnimatorStateInfo_t1064_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateExit_m6509_ParameterInfos[] = 
{
	{"animator", 0, 134219362, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateInfo", 1, 134219363, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1064_0_0_0},
	{"layerIndex", 2, 134219364, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateExit_m6509_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m6509/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateExit_m6509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType AnimatorStateInfo_t1064_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateMove_m6510_ParameterInfos[] = 
{
	{"animator", 0, 134219365, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateInfo", 1, 134219366, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1064_0_0_0},
	{"layerIndex", 2, 134219367, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMove_m6510_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m6510/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateMove_m6510_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType AnimatorStateInfo_t1064_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateIK_m6511_ParameterInfos[] = 
{
	{"animator", 0, 134219368, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateInfo", 1, 134219369, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1064_0_0_0},
	{"layerIndex", 2, 134219370, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateIK_m6511_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m6511/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_AnimatorStateInfo_t1064_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateIK_m6511_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateMachineEnter_m6512_ParameterInfos[] = 
{
	{"animator", 0, 134219371, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateMachinePathHash", 1, 134219372, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6512_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m6512/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateMachineEnter_m6512_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateMachineExit_m6513_ParameterInfos[] = 
{
	{"animator", 0, 134219373, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"stateMachinePathHash", 1, 134219374, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMachineExit_m6513_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m6513/* method */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, StateMachineBehaviour_t1121_StateMachineBehaviour_OnStateMachineExit_m6513_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StateMachineBehaviour_t1121_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m6506_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6507_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6508_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6509_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6510_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6511_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6512_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6513_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m197_MethodInfo;
extern MethodInfo Object_GetHashCode_m199_MethodInfo;
extern MethodInfo Object_ToString_m200_MethodInfo;
static MethodInfo* StateMachineBehaviour_t1121_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6507_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6508_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6509_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6510_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6511_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6512_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6513_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
extern Il2CppType StateMachineBehaviour_t1121_1_0_0;
extern TypeInfo ScriptableObject_t962_il2cpp_TypeInfo;
struct StateMachineBehaviour_t1121;
TypeInfo StateMachineBehaviour_t1121_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t1121_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ScriptableObject_t962_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, StateMachineBehaviour_t1121_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &StateMachineBehaviour_t1121_il2cpp_TypeInfo/* cast_class */
	, &StateMachineBehaviour_t1121_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t1121_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t1121)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DblClickSnapping_t1122_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"



// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern Il2CppType Byte_t838_0_0_1542;
FieldInfo DblClickSnapping_t1122____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t838_0_0_1542/* type */
	, &DblClickSnapping_t1122_il2cpp_TypeInfo/* parent */
	, offsetof(DblClickSnapping_t1122, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1122_0_0_32854;
FieldInfo DblClickSnapping_t1122____WORDS_2_FieldInfo = 
{
	"WORDS"/* name */
	, &DblClickSnapping_t1122_0_0_32854/* type */
	, &DblClickSnapping_t1122_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1122_0_0_32854;
FieldInfo DblClickSnapping_t1122____PARAGRAPHS_3_FieldInfo = 
{
	"PARAGRAPHS"/* name */
	, &DblClickSnapping_t1122_0_0_32854/* type */
	, &DblClickSnapping_t1122_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DblClickSnapping_t1122_FieldInfos[] =
{
	&DblClickSnapping_t1122____value___1_FieldInfo,
	&DblClickSnapping_t1122____WORDS_2_FieldInfo,
	&DblClickSnapping_t1122____PARAGRAPHS_3_FieldInfo,
	NULL
};
static const uint8_t DblClickSnapping_t1122____WORDS_2_DefaultValueData = 0;
extern Il2CppType Byte_t838_0_0_0;
static Il2CppFieldDefaultValueEntry DblClickSnapping_t1122____WORDS_2_DefaultValue = 
{
	&DblClickSnapping_t1122____WORDS_2_FieldInfo/* field */
	, { (char*)&DblClickSnapping_t1122____WORDS_2_DefaultValueData, &Byte_t838_0_0_0 }/* value */

};
static const uint8_t DblClickSnapping_t1122____PARAGRAPHS_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry DblClickSnapping_t1122____PARAGRAPHS_3_DefaultValue = 
{
	&DblClickSnapping_t1122____PARAGRAPHS_3_FieldInfo/* field */
	, { (char*)&DblClickSnapping_t1122____PARAGRAPHS_3_DefaultValueData, &Byte_t838_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* DblClickSnapping_t1122_FieldDefaultValues[] = 
{
	&DblClickSnapping_t1122____WORDS_2_DefaultValue,
	&DblClickSnapping_t1122____PARAGRAPHS_3_DefaultValue,
	NULL
};
static MethodInfo* DblClickSnapping_t1122_MethodInfos[] =
{
	NULL
};
static MethodInfo* DblClickSnapping_t1122_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t1122_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType DblClickSnapping_t1122_0_0_0;
extern Il2CppType DblClickSnapping_t1122_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t838_il2cpp_TypeInfo;
extern TypeInfo TextEditor_t528_il2cpp_TypeInfo;
TypeInfo DblClickSnapping_t1122_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t1122_MethodInfos/* methods */
	, NULL/* properties */
	, DblClickSnapping_t1122_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &TextEditor_t528_il2cpp_TypeInfo/* nested_in */
	, &Byte_t838_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DblClickSnapping_t1122_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Byte_t838_il2cpp_TypeInfo/* cast_class */
	, &DblClickSnapping_t1122_0_0_0/* byval_arg */
	, &DblClickSnapping_t1122_1_0_0/* this_arg */
	, DblClickSnapping_t1122_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, DblClickSnapping_t1122_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t1122)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (uint8_t)/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextEditOp_t1123_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"



// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo TextEditOp_t1123____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditOp_t1123, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveLeft_2_FieldInfo = 
{
	"MoveLeft"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveRight_3_FieldInfo = 
{
	"MoveRight"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveUp_4_FieldInfo = 
{
	"MoveUp"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveDown_5_FieldInfo = 
{
	"MoveDown"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveLineStart_6_FieldInfo = 
{
	"MoveLineStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveLineEnd_7_FieldInfo = 
{
	"MoveLineEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveTextStart_8_FieldInfo = 
{
	"MoveTextStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveTextEnd_9_FieldInfo = 
{
	"MoveTextEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MovePageUp_10_FieldInfo = 
{
	"MovePageUp"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MovePageDown_11_FieldInfo = 
{
	"MovePageDown"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveGraphicalLineStart_12_FieldInfo = 
{
	"MoveGraphicalLineStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveGraphicalLineEnd_13_FieldInfo = 
{
	"MoveGraphicalLineEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveWordLeft_14_FieldInfo = 
{
	"MoveWordLeft"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveWordRight_15_FieldInfo = 
{
	"MoveWordRight"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveParagraphForward_16_FieldInfo = 
{
	"MoveParagraphForward"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveParagraphBackward_17_FieldInfo = 
{
	"MoveParagraphBackward"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveToStartOfNextWord_18_FieldInfo = 
{
	"MoveToStartOfNextWord"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____MoveToEndOfPreviousWord_19_FieldInfo = 
{
	"MoveToEndOfPreviousWord"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectLeft_20_FieldInfo = 
{
	"SelectLeft"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectRight_21_FieldInfo = 
{
	"SelectRight"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectUp_22_FieldInfo = 
{
	"SelectUp"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectDown_23_FieldInfo = 
{
	"SelectDown"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectTextStart_24_FieldInfo = 
{
	"SelectTextStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectTextEnd_25_FieldInfo = 
{
	"SelectTextEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectPageUp_26_FieldInfo = 
{
	"SelectPageUp"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectPageDown_27_FieldInfo = 
{
	"SelectPageDown"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_FieldInfo = 
{
	"ExpandSelectGraphicalLineStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_FieldInfo = 
{
	"ExpandSelectGraphicalLineEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectGraphicalLineStart_30_FieldInfo = 
{
	"SelectGraphicalLineStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectGraphicalLineEnd_31_FieldInfo = 
{
	"SelectGraphicalLineEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectWordLeft_32_FieldInfo = 
{
	"SelectWordLeft"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectWordRight_33_FieldInfo = 
{
	"SelectWordRight"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectToEndOfPreviousWord_34_FieldInfo = 
{
	"SelectToEndOfPreviousWord"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectToStartOfNextWord_35_FieldInfo = 
{
	"SelectToStartOfNextWord"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectParagraphBackward_36_FieldInfo = 
{
	"SelectParagraphBackward"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectParagraphForward_37_FieldInfo = 
{
	"SelectParagraphForward"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____Delete_38_FieldInfo = 
{
	"Delete"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____Backspace_39_FieldInfo = 
{
	"Backspace"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____DeleteWordBack_40_FieldInfo = 
{
	"DeleteWordBack"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____DeleteWordForward_41_FieldInfo = 
{
	"DeleteWordForward"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____DeleteLineBack_42_FieldInfo = 
{
	"DeleteLineBack"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____Cut_43_FieldInfo = 
{
	"Cut"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____Copy_44_FieldInfo = 
{
	"Copy"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____Paste_45_FieldInfo = 
{
	"Paste"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectAll_46_FieldInfo = 
{
	"SelectAll"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____SelectNone_47_FieldInfo = 
{
	"SelectNone"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____ScrollStart_48_FieldInfo = 
{
	"ScrollStart"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____ScrollEnd_49_FieldInfo = 
{
	"ScrollEnd"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____ScrollPageUp_50_FieldInfo = 
{
	"ScrollPageUp"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1123_0_0_32854;
FieldInfo TextEditOp_t1123____ScrollPageDown_51_FieldInfo = 
{
	"ScrollPageDown"/* name */
	, &TextEditOp_t1123_0_0_32854/* type */
	, &TextEditOp_t1123_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextEditOp_t1123_FieldInfos[] =
{
	&TextEditOp_t1123____value___1_FieldInfo,
	&TextEditOp_t1123____MoveLeft_2_FieldInfo,
	&TextEditOp_t1123____MoveRight_3_FieldInfo,
	&TextEditOp_t1123____MoveUp_4_FieldInfo,
	&TextEditOp_t1123____MoveDown_5_FieldInfo,
	&TextEditOp_t1123____MoveLineStart_6_FieldInfo,
	&TextEditOp_t1123____MoveLineEnd_7_FieldInfo,
	&TextEditOp_t1123____MoveTextStart_8_FieldInfo,
	&TextEditOp_t1123____MoveTextEnd_9_FieldInfo,
	&TextEditOp_t1123____MovePageUp_10_FieldInfo,
	&TextEditOp_t1123____MovePageDown_11_FieldInfo,
	&TextEditOp_t1123____MoveGraphicalLineStart_12_FieldInfo,
	&TextEditOp_t1123____MoveGraphicalLineEnd_13_FieldInfo,
	&TextEditOp_t1123____MoveWordLeft_14_FieldInfo,
	&TextEditOp_t1123____MoveWordRight_15_FieldInfo,
	&TextEditOp_t1123____MoveParagraphForward_16_FieldInfo,
	&TextEditOp_t1123____MoveParagraphBackward_17_FieldInfo,
	&TextEditOp_t1123____MoveToStartOfNextWord_18_FieldInfo,
	&TextEditOp_t1123____MoveToEndOfPreviousWord_19_FieldInfo,
	&TextEditOp_t1123____SelectLeft_20_FieldInfo,
	&TextEditOp_t1123____SelectRight_21_FieldInfo,
	&TextEditOp_t1123____SelectUp_22_FieldInfo,
	&TextEditOp_t1123____SelectDown_23_FieldInfo,
	&TextEditOp_t1123____SelectTextStart_24_FieldInfo,
	&TextEditOp_t1123____SelectTextEnd_25_FieldInfo,
	&TextEditOp_t1123____SelectPageUp_26_FieldInfo,
	&TextEditOp_t1123____SelectPageDown_27_FieldInfo,
	&TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_FieldInfo,
	&TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_FieldInfo,
	&TextEditOp_t1123____SelectGraphicalLineStart_30_FieldInfo,
	&TextEditOp_t1123____SelectGraphicalLineEnd_31_FieldInfo,
	&TextEditOp_t1123____SelectWordLeft_32_FieldInfo,
	&TextEditOp_t1123____SelectWordRight_33_FieldInfo,
	&TextEditOp_t1123____SelectToEndOfPreviousWord_34_FieldInfo,
	&TextEditOp_t1123____SelectToStartOfNextWord_35_FieldInfo,
	&TextEditOp_t1123____SelectParagraphBackward_36_FieldInfo,
	&TextEditOp_t1123____SelectParagraphForward_37_FieldInfo,
	&TextEditOp_t1123____Delete_38_FieldInfo,
	&TextEditOp_t1123____Backspace_39_FieldInfo,
	&TextEditOp_t1123____DeleteWordBack_40_FieldInfo,
	&TextEditOp_t1123____DeleteWordForward_41_FieldInfo,
	&TextEditOp_t1123____DeleteLineBack_42_FieldInfo,
	&TextEditOp_t1123____Cut_43_FieldInfo,
	&TextEditOp_t1123____Copy_44_FieldInfo,
	&TextEditOp_t1123____Paste_45_FieldInfo,
	&TextEditOp_t1123____SelectAll_46_FieldInfo,
	&TextEditOp_t1123____SelectNone_47_FieldInfo,
	&TextEditOp_t1123____ScrollStart_48_FieldInfo,
	&TextEditOp_t1123____ScrollEnd_49_FieldInfo,
	&TextEditOp_t1123____ScrollPageUp_50_FieldInfo,
	&TextEditOp_t1123____ScrollPageDown_51_FieldInfo,
	NULL
};
static const int32_t TextEditOp_t1123____MoveLeft_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveLeft_2_DefaultValue = 
{
	&TextEditOp_t1123____MoveLeft_2_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveLeft_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveRight_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveRight_3_DefaultValue = 
{
	&TextEditOp_t1123____MoveRight_3_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveRight_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveUp_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveUp_4_DefaultValue = 
{
	&TextEditOp_t1123____MoveUp_4_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveUp_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveDown_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveDown_5_DefaultValue = 
{
	&TextEditOp_t1123____MoveDown_5_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveDown_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveLineStart_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveLineStart_6_DefaultValue = 
{
	&TextEditOp_t1123____MoveLineStart_6_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveLineStart_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveLineEnd_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveLineEnd_7_DefaultValue = 
{
	&TextEditOp_t1123____MoveLineEnd_7_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveLineEnd_7_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveTextStart_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveTextStart_8_DefaultValue = 
{
	&TextEditOp_t1123____MoveTextStart_8_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveTextStart_8_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveTextEnd_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveTextEnd_9_DefaultValue = 
{
	&TextEditOp_t1123____MoveTextEnd_9_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveTextEnd_9_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MovePageUp_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MovePageUp_10_DefaultValue = 
{
	&TextEditOp_t1123____MovePageUp_10_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MovePageUp_10_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MovePageDown_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MovePageDown_11_DefaultValue = 
{
	&TextEditOp_t1123____MovePageDown_11_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MovePageDown_11_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveGraphicalLineStart_12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveGraphicalLineStart_12_DefaultValue = 
{
	&TextEditOp_t1123____MoveGraphicalLineStart_12_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveGraphicalLineStart_12_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveGraphicalLineEnd_13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveGraphicalLineEnd_13_DefaultValue = 
{
	&TextEditOp_t1123____MoveGraphicalLineEnd_13_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveGraphicalLineEnd_13_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveWordLeft_14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveWordLeft_14_DefaultValue = 
{
	&TextEditOp_t1123____MoveWordLeft_14_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveWordLeft_14_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveWordRight_15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveWordRight_15_DefaultValue = 
{
	&TextEditOp_t1123____MoveWordRight_15_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveWordRight_15_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveParagraphForward_16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveParagraphForward_16_DefaultValue = 
{
	&TextEditOp_t1123____MoveParagraphForward_16_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveParagraphForward_16_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveParagraphBackward_17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveParagraphBackward_17_DefaultValue = 
{
	&TextEditOp_t1123____MoveParagraphBackward_17_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveParagraphBackward_17_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveToStartOfNextWord_18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveToStartOfNextWord_18_DefaultValue = 
{
	&TextEditOp_t1123____MoveToStartOfNextWord_18_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveToStartOfNextWord_18_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____MoveToEndOfPreviousWord_19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____MoveToEndOfPreviousWord_19_DefaultValue = 
{
	&TextEditOp_t1123____MoveToEndOfPreviousWord_19_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____MoveToEndOfPreviousWord_19_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectLeft_20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectLeft_20_DefaultValue = 
{
	&TextEditOp_t1123____SelectLeft_20_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectLeft_20_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectRight_21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectRight_21_DefaultValue = 
{
	&TextEditOp_t1123____SelectRight_21_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectRight_21_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectUp_22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectUp_22_DefaultValue = 
{
	&TextEditOp_t1123____SelectUp_22_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectUp_22_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectDown_23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectDown_23_DefaultValue = 
{
	&TextEditOp_t1123____SelectDown_23_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectDown_23_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectTextStart_24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectTextStart_24_DefaultValue = 
{
	&TextEditOp_t1123____SelectTextStart_24_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectTextStart_24_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectTextEnd_25_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectTextEnd_25_DefaultValue = 
{
	&TextEditOp_t1123____SelectTextEnd_25_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectTextEnd_25_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectPageUp_26_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectPageUp_26_DefaultValue = 
{
	&TextEditOp_t1123____SelectPageUp_26_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectPageUp_26_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectPageDown_27_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectPageDown_27_DefaultValue = 
{
	&TextEditOp_t1123____SelectPageDown_27_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectPageDown_27_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_DefaultValue = 
{
	&TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_DefaultValue = 
{
	&TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectGraphicalLineStart_30_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectGraphicalLineStart_30_DefaultValue = 
{
	&TextEditOp_t1123____SelectGraphicalLineStart_30_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectGraphicalLineStart_30_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectGraphicalLineEnd_31_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectGraphicalLineEnd_31_DefaultValue = 
{
	&TextEditOp_t1123____SelectGraphicalLineEnd_31_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectGraphicalLineEnd_31_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectWordLeft_32_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectWordLeft_32_DefaultValue = 
{
	&TextEditOp_t1123____SelectWordLeft_32_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectWordLeft_32_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectWordRight_33_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectWordRight_33_DefaultValue = 
{
	&TextEditOp_t1123____SelectWordRight_33_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectWordRight_33_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectToEndOfPreviousWord_34_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectToEndOfPreviousWord_34_DefaultValue = 
{
	&TextEditOp_t1123____SelectToEndOfPreviousWord_34_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectToEndOfPreviousWord_34_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectToStartOfNextWord_35_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectToStartOfNextWord_35_DefaultValue = 
{
	&TextEditOp_t1123____SelectToStartOfNextWord_35_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectToStartOfNextWord_35_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectParagraphBackward_36_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectParagraphBackward_36_DefaultValue = 
{
	&TextEditOp_t1123____SelectParagraphBackward_36_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectParagraphBackward_36_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectParagraphForward_37_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectParagraphForward_37_DefaultValue = 
{
	&TextEditOp_t1123____SelectParagraphForward_37_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectParagraphForward_37_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____Delete_38_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____Delete_38_DefaultValue = 
{
	&TextEditOp_t1123____Delete_38_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____Delete_38_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____Backspace_39_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____Backspace_39_DefaultValue = 
{
	&TextEditOp_t1123____Backspace_39_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____Backspace_39_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____DeleteWordBack_40_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____DeleteWordBack_40_DefaultValue = 
{
	&TextEditOp_t1123____DeleteWordBack_40_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____DeleteWordBack_40_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____DeleteWordForward_41_DefaultValueData = 39;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____DeleteWordForward_41_DefaultValue = 
{
	&TextEditOp_t1123____DeleteWordForward_41_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____DeleteWordForward_41_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____DeleteLineBack_42_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____DeleteLineBack_42_DefaultValue = 
{
	&TextEditOp_t1123____DeleteLineBack_42_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____DeleteLineBack_42_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____Cut_43_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____Cut_43_DefaultValue = 
{
	&TextEditOp_t1123____Cut_43_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____Cut_43_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____Copy_44_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____Copy_44_DefaultValue = 
{
	&TextEditOp_t1123____Copy_44_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____Copy_44_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____Paste_45_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____Paste_45_DefaultValue = 
{
	&TextEditOp_t1123____Paste_45_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____Paste_45_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectAll_46_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectAll_46_DefaultValue = 
{
	&TextEditOp_t1123____SelectAll_46_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectAll_46_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____SelectNone_47_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____SelectNone_47_DefaultValue = 
{
	&TextEditOp_t1123____SelectNone_47_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____SelectNone_47_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____ScrollStart_48_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____ScrollStart_48_DefaultValue = 
{
	&TextEditOp_t1123____ScrollStart_48_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____ScrollStart_48_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____ScrollEnd_49_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____ScrollEnd_49_DefaultValue = 
{
	&TextEditOp_t1123____ScrollEnd_49_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____ScrollEnd_49_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____ScrollPageUp_50_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____ScrollPageUp_50_DefaultValue = 
{
	&TextEditOp_t1123____ScrollPageUp_50_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____ScrollPageUp_50_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1123____ScrollPageDown_51_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry TextEditOp_t1123____ScrollPageDown_51_DefaultValue = 
{
	&TextEditOp_t1123____ScrollPageDown_51_FieldInfo/* field */
	, { (char*)&TextEditOp_t1123____ScrollPageDown_51_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TextEditOp_t1123_FieldDefaultValues[] = 
{
	&TextEditOp_t1123____MoveLeft_2_DefaultValue,
	&TextEditOp_t1123____MoveRight_3_DefaultValue,
	&TextEditOp_t1123____MoveUp_4_DefaultValue,
	&TextEditOp_t1123____MoveDown_5_DefaultValue,
	&TextEditOp_t1123____MoveLineStart_6_DefaultValue,
	&TextEditOp_t1123____MoveLineEnd_7_DefaultValue,
	&TextEditOp_t1123____MoveTextStart_8_DefaultValue,
	&TextEditOp_t1123____MoveTextEnd_9_DefaultValue,
	&TextEditOp_t1123____MovePageUp_10_DefaultValue,
	&TextEditOp_t1123____MovePageDown_11_DefaultValue,
	&TextEditOp_t1123____MoveGraphicalLineStart_12_DefaultValue,
	&TextEditOp_t1123____MoveGraphicalLineEnd_13_DefaultValue,
	&TextEditOp_t1123____MoveWordLeft_14_DefaultValue,
	&TextEditOp_t1123____MoveWordRight_15_DefaultValue,
	&TextEditOp_t1123____MoveParagraphForward_16_DefaultValue,
	&TextEditOp_t1123____MoveParagraphBackward_17_DefaultValue,
	&TextEditOp_t1123____MoveToStartOfNextWord_18_DefaultValue,
	&TextEditOp_t1123____MoveToEndOfPreviousWord_19_DefaultValue,
	&TextEditOp_t1123____SelectLeft_20_DefaultValue,
	&TextEditOp_t1123____SelectRight_21_DefaultValue,
	&TextEditOp_t1123____SelectUp_22_DefaultValue,
	&TextEditOp_t1123____SelectDown_23_DefaultValue,
	&TextEditOp_t1123____SelectTextStart_24_DefaultValue,
	&TextEditOp_t1123____SelectTextEnd_25_DefaultValue,
	&TextEditOp_t1123____SelectPageUp_26_DefaultValue,
	&TextEditOp_t1123____SelectPageDown_27_DefaultValue,
	&TextEditOp_t1123____ExpandSelectGraphicalLineStart_28_DefaultValue,
	&TextEditOp_t1123____ExpandSelectGraphicalLineEnd_29_DefaultValue,
	&TextEditOp_t1123____SelectGraphicalLineStart_30_DefaultValue,
	&TextEditOp_t1123____SelectGraphicalLineEnd_31_DefaultValue,
	&TextEditOp_t1123____SelectWordLeft_32_DefaultValue,
	&TextEditOp_t1123____SelectWordRight_33_DefaultValue,
	&TextEditOp_t1123____SelectToEndOfPreviousWord_34_DefaultValue,
	&TextEditOp_t1123____SelectToStartOfNextWord_35_DefaultValue,
	&TextEditOp_t1123____SelectParagraphBackward_36_DefaultValue,
	&TextEditOp_t1123____SelectParagraphForward_37_DefaultValue,
	&TextEditOp_t1123____Delete_38_DefaultValue,
	&TextEditOp_t1123____Backspace_39_DefaultValue,
	&TextEditOp_t1123____DeleteWordBack_40_DefaultValue,
	&TextEditOp_t1123____DeleteWordForward_41_DefaultValue,
	&TextEditOp_t1123____DeleteLineBack_42_DefaultValue,
	&TextEditOp_t1123____Cut_43_DefaultValue,
	&TextEditOp_t1123____Copy_44_DefaultValue,
	&TextEditOp_t1123____Paste_45_DefaultValue,
	&TextEditOp_t1123____SelectAll_46_DefaultValue,
	&TextEditOp_t1123____SelectNone_47_DefaultValue,
	&TextEditOp_t1123____ScrollStart_48_DefaultValue,
	&TextEditOp_t1123____ScrollEnd_49_DefaultValue,
	&TextEditOp_t1123____ScrollPageUp_50_DefaultValue,
	&TextEditOp_t1123____ScrollPageDown_51_DefaultValue,
	NULL
};
static MethodInfo* TextEditOp_t1123_MethodInfos[] =
{
	NULL
};
static MethodInfo* TextEditOp_t1123_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair TextEditOp_t1123_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextEditOp_t1123_0_0_0;
extern Il2CppType TextEditOp_t1123_1_0_0;
TypeInfo TextEditOp_t1123_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t1123_MethodInfos/* methods */
	, NULL/* properties */
	, TextEditOp_t1123_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &TextEditor_t528_il2cpp_TypeInfo/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextEditOp_t1123_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &TextEditOp_t1123_0_0_0/* byval_arg */
	, &TextEditOp_t1123_1_0_0/* this_arg */
	, TextEditOp_t1123_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TextEditOp_t1123_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t1123)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"

// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
extern TypeInfo GUIContent_t529_il2cpp_TypeInfo;
extern TypeInfo GUIStyle_t999_il2cpp_TypeInfo;
extern TypeInfo Rect_t103_il2cpp_TypeInfo;
extern TypeInfo Vector2_t99_il2cpp_TypeInfo;
extern TypeInfo GUIUtility_t1012_il2cpp_TypeInfo;
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
extern MethodInfo GUIContent__ctor_m5866_MethodInfo;
extern MethodInfo GUIStyle_get_none_m5914_MethodInfo;
extern MethodInfo Vector2_get_zero_m665_MethodInfo;
extern MethodInfo TextEditor_SelectAll_m6515_MethodInfo;
extern MethodInfo GUIContent_get_text_m2446_MethodInfo;
extern MethodInfo TextEditor_ClearCursorPos_m6514_MethodInfo;
extern MethodInfo GUIContent_set_text_m5868_MethodInfo;
extern MethodInfo TextEditor_DeleteSelection_m6516_MethodInfo;
extern MethodInfo TextEditor_UpdateScrollOffset_m6518_MethodInfo;
extern MethodInfo Rect_get_width_m679_MethodInfo;
extern MethodInfo Rect_get_height_m680_MethodInfo;
extern MethodInfo Rect__ctor_m262_MethodInfo;
extern MethodInfo GUIStyle_GetCursorPixelPosition_m5915_MethodInfo;
extern MethodInfo GUIStyle_get_padding_m5903_MethodInfo;
extern MethodInfo RectOffset_Remove_m5890_MethodInfo;
extern MethodInfo GUIStyle_CalcSize_m5918_MethodInfo;
extern MethodInfo GUIStyle_CalcHeight_m5920_MethodInfo;
extern MethodInfo Vector2__ctor_m636_MethodInfo;
extern MethodInfo RectOffset_get_left_m2669_MethodInfo;
extern MethodInfo GUIStyle_get_lineHeight_m5912_MethodInfo;
extern MethodInfo RectOffset_get_top_m2670_MethodInfo;
extern MethodInfo RectOffset_get_bottom_m5888_MethodInfo;
extern MethodInfo GUIUtility_set_systemCopyBuffer_m5798_MethodInfo;
extern MethodInfo GUIUtility_get_systemCopyBuffer_m5797_MethodInfo;
extern MethodInfo String_op_Inequality_m658_MethodInfo;
extern MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m6519_MethodInfo;
extern MethodInfo TextEditor_ReplaceSelection_m6517_MethodInfo;


// System.Void UnityEngine.TextEditor::.ctor()
extern MethodInfo TextEditor__ctor_m2444_MethodInfo;
 void TextEditor__ctor_m2444 (TextEditor_t528 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIContent_t529_il2cpp_TypeInfo));
		GUIContent_t529 * L_0 = (GUIContent_t529 *)il2cpp_codegen_object_new (InitializedTypeInfo(&GUIContent_t529_il2cpp_TypeInfo));
		GUIContent__ctor_m5866(L_0, /*hidden argument*/&GUIContent__ctor_m5866_MethodInfo);
		__this->___content_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIStyle_t999_il2cpp_TypeInfo));
		GUIStyle_t999 * L_1 = GUIStyle_get_none_m5914(NULL /*static, unused*/, /*hidden argument*/&GUIStyle_get_none_m5914_MethodInfo);
		__this->___style_5 = L_1;
		Vector2_t99  L_2 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		__this->___scrollOffset_11 = L_2;
		__this->___m_iAltCursorPos_19 = (-1);
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
 void TextEditor_ClearCursorPos_m6514 (TextEditor_t528 * __this, MethodInfo* method){
	{
		__this->___hasHorizontalCursorPos_8 = 0;
		__this->___m_iAltCursorPos_19 = (-1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern MethodInfo TextEditor_OnFocus_m2448_MethodInfo;
 void TextEditor_OnFocus_m2448 (TextEditor_t528 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___multiline_7);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->___selectPos_2 = L_1;
		__this->___pos_1 = V_0;
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m6515(__this, /*hidden argument*/&TextEditor_SelectAll_m6515_MethodInfo);
	}

IL_0026:
	{
		__this->___m_HasFocus_10 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
 void TextEditor_SelectAll_m6515 (TextEditor_t528 * __this, MethodInfo* method){
	{
		__this->___pos_1 = 0;
		GUIContent_t529 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m2446(L_0, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2431(L_1, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		__this->___selectPos_2 = L_2;
		TextEditor_ClearCursorPos_m6514(__this, /*hidden argument*/&TextEditor_ClearCursorPos_m6514_MethodInfo);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
 bool TextEditor_DeleteSelection_m6516 (TextEditor_t528 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		GUIContent_t529 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m2446(L_0, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2431(L_1, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		V_0 = L_2;
		int32_t L_3 = (__this->___pos_1);
		if ((((int32_t)L_3) <= ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		__this->___pos_1 = V_0;
	}

IL_0024:
	{
		int32_t L_4 = (__this->___selectPos_2);
		if ((((int32_t)L_4) <= ((int32_t)V_0)))
		{
			goto IL_0037;
		}
	}
	{
		__this->___selectPos_2 = V_0;
	}

IL_0037:
	{
		int32_t L_5 = (__this->___pos_1);
		int32_t L_6 = (__this->___selectPos_2);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		int32_t L_7 = (__this->___pos_1);
		int32_t L_8 = (__this->___selectPos_2);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_00c0;
		}
	}
	{
		GUIContent_t529 * L_9 = (__this->___content_4);
		GUIContent_t529 * L_10 = (__this->___content_4);
		NullCheck(L_10);
		String_t* L_11 = GUIContent_get_text_m2446(L_10, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_12 = (__this->___pos_1);
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m2457(L_11, 0, L_12, /*hidden argument*/&String_Substring_m2457_MethodInfo);
		GUIContent_t529 * L_14 = (__this->___content_4);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m2446(L_14, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_16 = (__this->___selectPos_2);
		GUIContent_t529 * L_17 = (__this->___content_4);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m2446(L_17, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m2431(L_18, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		int32_t L_20 = (__this->___selectPos_2);
		NullCheck(L_15);
		String_t* L_21 = String_Substring_m2457(L_15, L_16, ((int32_t)(L_19-L_20)), /*hidden argument*/&String_Substring_m2457_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_22 = String_Concat_m269(NULL /*static, unused*/, L_13, L_21, /*hidden argument*/&String_Concat_m269_MethodInfo);
		NullCheck(L_9);
		GUIContent_set_text_m5868(L_9, L_22, /*hidden argument*/&GUIContent_set_text_m5868_MethodInfo);
		int32_t L_23 = (__this->___pos_1);
		__this->___selectPos_2 = L_23;
		goto IL_0120;
	}

IL_00c0:
	{
		GUIContent_t529 * L_24 = (__this->___content_4);
		GUIContent_t529 * L_25 = (__this->___content_4);
		NullCheck(L_25);
		String_t* L_26 = GUIContent_get_text_m2446(L_25, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_27 = (__this->___selectPos_2);
		NullCheck(L_26);
		String_t* L_28 = String_Substring_m2457(L_26, 0, L_27, /*hidden argument*/&String_Substring_m2457_MethodInfo);
		GUIContent_t529 * L_29 = (__this->___content_4);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m2446(L_29, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_31 = (__this->___pos_1);
		GUIContent_t529 * L_32 = (__this->___content_4);
		NullCheck(L_32);
		String_t* L_33 = GUIContent_get_text_m2446(L_32, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m2431(L_33, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		int32_t L_35 = (__this->___pos_1);
		NullCheck(L_30);
		String_t* L_36 = String_Substring_m2457(L_30, L_31, ((int32_t)(L_34-L_35)), /*hidden argument*/&String_Substring_m2457_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_37 = String_Concat_m269(NULL /*static, unused*/, L_28, L_36, /*hidden argument*/&String_Concat_m269_MethodInfo);
		NullCheck(L_24);
		GUIContent_set_text_m5868(L_24, L_37, /*hidden argument*/&GUIContent_set_text_m5868_MethodInfo);
		int32_t L_38 = (__this->___selectPos_2);
		__this->___pos_1 = L_38;
	}

IL_0120:
	{
		TextEditor_ClearCursorPos_m6514(__this, /*hidden argument*/&TextEditor_ClearCursorPos_m6514_MethodInfo);
		return 1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
 void TextEditor_ReplaceSelection_m6517 (TextEditor_t528 * __this, String_t* ___replace, MethodInfo* method){
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m6516(__this, /*hidden argument*/&TextEditor_DeleteSelection_m6516_MethodInfo);
		GUIContent_t529 * L_0 = (__this->___content_4);
		GUIContent_t529 * L_1 = (__this->___content_4);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m2446(L_1, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_3 = (__this->___pos_1);
		NullCheck(L_2);
		String_t* L_4 = String_Insert_m2481(L_2, L_3, ___replace, /*hidden argument*/&String_Insert_m2481_MethodInfo);
		NullCheck(L_0);
		GUIContent_set_text_m5868(L_0, L_4, /*hidden argument*/&GUIContent_set_text_m5868_MethodInfo);
		int32_t L_5 = (__this->___pos_1);
		NullCheck(___replace);
		int32_t L_6 = String_get_Length_m2431(___replace, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		int32_t L_7 = ((int32_t)(L_5+L_6));
		V_0 = L_7;
		__this->___pos_1 = L_7;
		__this->___selectPos_2 = V_0;
		TextEditor_ClearCursorPos_m6514(__this, /*hidden argument*/&TextEditor_ClearCursorPos_m6514_MethodInfo);
		TextEditor_UpdateScrollOffset_m6518(__this, /*hidden argument*/&TextEditor_UpdateScrollOffset_m6518_MethodInfo);
		__this->___m_TextHeightPotentiallyChanged_12 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
 void TextEditor_UpdateScrollOffset_m6518 (TextEditor_t528 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	Rect_t103  V_1 = {0};
	Vector2_t99  V_2 = {0};
	Vector2_t99  V_3 = {0};
	Vector2_t99 * G_B17_0 = {0};
	Vector2_t99 * G_B16_0 = {0};
	float G_B18_0 = 0.0f;
	Vector2_t99 * G_B18_1 = {0};
	{
		int32_t L_0 = (__this->___pos_1);
		V_0 = L_0;
		GUIStyle_t999 * L_1 = (__this->___style_5);
		Rect_t103 * L_2 = &(__this->___position_6);
		float L_3 = Rect_get_width_m679(L_2, /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		Rect_t103 * L_4 = &(__this->___position_6);
		float L_5 = Rect_get_height_m680(L_4, /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		Rect_t103  L_6 = {0};
		Rect__ctor_m262(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		GUIContent_t529 * L_7 = (__this->___content_4);
		NullCheck(L_1);
		Vector2_t99  L_8 = GUIStyle_GetCursorPixelPosition_m5915(L_1, L_6, L_7, V_0, /*hidden argument*/&GUIStyle_GetCursorPixelPosition_m5915_MethodInfo);
		__this->___graphicalCursorPos_13 = L_8;
		GUIStyle_t999 * L_9 = (__this->___style_5);
		NullCheck(L_9);
		RectOffset_t439 * L_10 = GUIStyle_get_padding_m5903(L_9, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		Rect_t103  L_11 = (__this->___position_6);
		NullCheck(L_10);
		Rect_t103  L_12 = RectOffset_Remove_m5890(L_10, L_11, /*hidden argument*/&RectOffset_Remove_m5890_MethodInfo);
		V_1 = L_12;
		GUIStyle_t999 * L_13 = (__this->___style_5);
		GUIContent_t529 * L_14 = (__this->___content_4);
		NullCheck(L_13);
		Vector2_t99  L_15 = GUIStyle_CalcSize_m5918(L_13, L_14, /*hidden argument*/&GUIStyle_CalcSize_m5918_MethodInfo);
		V_3 = L_15;
		NullCheck((&V_3));
		float L_16 = ((&V_3)->___x_1);
		GUIStyle_t999 * L_17 = (__this->___style_5);
		GUIContent_t529 * L_18 = (__this->___content_4);
		Rect_t103 * L_19 = &(__this->___position_6);
		float L_20 = Rect_get_width_m679(L_19, /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		NullCheck(L_17);
		float L_21 = GUIStyle_CalcHeight_m5920(L_17, L_18, L_20, /*hidden argument*/&GUIStyle_CalcHeight_m5920_MethodInfo);
		Vector2__ctor_m636((&V_2), L_16, L_21, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		NullCheck((&V_2));
		float L_22 = ((&V_2)->___x_1);
		Rect_t103 * L_23 = &(__this->___position_6);
		float L_24 = Rect_get_width_m679(L_23, /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		if ((((float)L_22) >= ((float)L_24)))
		{
			goto IL_00c3;
		}
	}
	{
		Vector2_t99 * L_25 = &(__this->___scrollOffset_11);
		NullCheck(L_25);
		L_25->___x_1 = (0.0f);
		goto IL_015f;
	}

IL_00c3:
	{
		Vector2_t99 * L_26 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_26);
		float L_27 = (L_26->___x_1);
		Vector2_t99 * L_28 = &(__this->___scrollOffset_11);
		NullCheck(L_28);
		float L_29 = (L_28->___x_1);
		float L_30 = Rect_get_width_m679((&V_1), /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		if ((((float)((float)(L_27+(1.0f)))) <= ((float)((float)(L_29+L_30)))))
		{
			goto IL_010a;
		}
	}
	{
		Vector2_t99 * L_31 = &(__this->___scrollOffset_11);
		Vector2_t99 * L_32 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_32);
		float L_33 = (L_32->___x_1);
		float L_34 = Rect_get_width_m679((&V_1), /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		NullCheck(L_31);
		L_31->___x_1 = ((float)(L_33-L_34));
	}

IL_010a:
	{
		Vector2_t99 * L_35 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_35);
		float L_36 = (L_35->___x_1);
		Vector2_t99 * L_37 = &(__this->___scrollOffset_11);
		NullCheck(L_37);
		float L_38 = (L_37->___x_1);
		GUIStyle_t999 * L_39 = (__this->___style_5);
		NullCheck(L_39);
		RectOffset_t439 * L_40 = GUIStyle_get_padding_m5903(L_39, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_40);
		int32_t L_41 = RectOffset_get_left_m2669(L_40, /*hidden argument*/&RectOffset_get_left_m2669_MethodInfo);
		if ((((float)L_36) >= ((float)((float)(L_38+(((float)L_41)))))))
		{
			goto IL_015f;
		}
	}
	{
		Vector2_t99 * L_42 = &(__this->___scrollOffset_11);
		Vector2_t99 * L_43 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_43);
		float L_44 = (L_43->___x_1);
		GUIStyle_t999 * L_45 = (__this->___style_5);
		NullCheck(L_45);
		RectOffset_t439 * L_46 = GUIStyle_get_padding_m5903(L_45, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_46);
		int32_t L_47 = RectOffset_get_left_m2669(L_46, /*hidden argument*/&RectOffset_get_left_m2669_MethodInfo);
		NullCheck(L_42);
		L_42->___x_1 = ((float)(L_44-(((float)L_47))));
	}

IL_015f:
	{
		NullCheck((&V_2));
		float L_48 = ((&V_2)->___y_2);
		float L_49 = Rect_get_height_m680((&V_1), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		if ((((float)L_48) >= ((float)L_49)))
		{
			goto IL_0187;
		}
	}
	{
		Vector2_t99 * L_50 = &(__this->___scrollOffset_11);
		NullCheck(L_50);
		L_50->___y_2 = (0.0f);
		goto IL_0259;
	}

IL_0187:
	{
		Vector2_t99 * L_51 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_51);
		float L_52 = (L_51->___y_2);
		GUIStyle_t999 * L_53 = (__this->___style_5);
		NullCheck(L_53);
		float L_54 = GUIStyle_get_lineHeight_m5912(L_53, /*hidden argument*/&GUIStyle_get_lineHeight_m5912_MethodInfo);
		Vector2_t99 * L_55 = &(__this->___scrollOffset_11);
		NullCheck(L_55);
		float L_56 = (L_55->___y_2);
		float L_57 = Rect_get_height_m680((&V_1), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		GUIStyle_t999 * L_58 = (__this->___style_5);
		NullCheck(L_58);
		RectOffset_t439 * L_59 = GUIStyle_get_padding_m5903(L_58, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_59);
		int32_t L_60 = RectOffset_get_top_m2670(L_59, /*hidden argument*/&RectOffset_get_top_m2670_MethodInfo);
		if ((((float)((float)(L_52+L_54))) <= ((float)((float)(((float)(L_56+L_57))+(((float)L_60)))))))
		{
			goto IL_0204;
		}
	}
	{
		Vector2_t99 * L_61 = &(__this->___scrollOffset_11);
		Vector2_t99 * L_62 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_62);
		float L_63 = (L_62->___y_2);
		float L_64 = Rect_get_height_m680((&V_1), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		GUIStyle_t999 * L_65 = (__this->___style_5);
		NullCheck(L_65);
		RectOffset_t439 * L_66 = GUIStyle_get_padding_m5903(L_65, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_66);
		int32_t L_67 = RectOffset_get_top_m2670(L_66, /*hidden argument*/&RectOffset_get_top_m2670_MethodInfo);
		GUIStyle_t999 * L_68 = (__this->___style_5);
		NullCheck(L_68);
		float L_69 = GUIStyle_get_lineHeight_m5912(L_68, /*hidden argument*/&GUIStyle_get_lineHeight_m5912_MethodInfo);
		NullCheck(L_61);
		L_61->___y_2 = ((float)(((float)(((float)(L_63-L_64))-(((float)L_67))))+L_69));
	}

IL_0204:
	{
		Vector2_t99 * L_70 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_70);
		float L_71 = (L_70->___y_2);
		Vector2_t99 * L_72 = &(__this->___scrollOffset_11);
		NullCheck(L_72);
		float L_73 = (L_72->___y_2);
		GUIStyle_t999 * L_74 = (__this->___style_5);
		NullCheck(L_74);
		RectOffset_t439 * L_75 = GUIStyle_get_padding_m5903(L_74, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_75);
		int32_t L_76 = RectOffset_get_top_m2670(L_75, /*hidden argument*/&RectOffset_get_top_m2670_MethodInfo);
		if ((((float)L_71) >= ((float)((float)(L_73+(((float)L_76)))))))
		{
			goto IL_0259;
		}
	}
	{
		Vector2_t99 * L_77 = &(__this->___scrollOffset_11);
		Vector2_t99 * L_78 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_78);
		float L_79 = (L_78->___y_2);
		GUIStyle_t999 * L_80 = (__this->___style_5);
		NullCheck(L_80);
		RectOffset_t439 * L_81 = GUIStyle_get_padding_m5903(L_80, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_81);
		int32_t L_82 = RectOffset_get_top_m2670(L_81, /*hidden argument*/&RectOffset_get_top_m2670_MethodInfo);
		NullCheck(L_77);
		L_77->___y_2 = ((float)(L_79-(((float)L_82))));
	}

IL_0259:
	{
		Vector2_t99 * L_83 = &(__this->___scrollOffset_11);
		NullCheck(L_83);
		float L_84 = (L_83->___y_2);
		if ((((float)L_84) <= ((float)(0.0f))))
		{
			goto IL_02cb;
		}
	}
	{
		NullCheck((&V_2));
		float L_85 = ((&V_2)->___y_2);
		Vector2_t99 * L_86 = &(__this->___scrollOffset_11);
		NullCheck(L_86);
		float L_87 = (L_86->___y_2);
		float L_88 = Rect_get_height_m680((&V_1), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		if ((((float)((float)(L_85-L_87))) >= ((float)L_88)))
		{
			goto IL_02cb;
		}
	}
	{
		Vector2_t99 * L_89 = &(__this->___scrollOffset_11);
		NullCheck((&V_2));
		float L_90 = ((&V_2)->___y_2);
		float L_91 = Rect_get_height_m680((&V_1), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		GUIStyle_t999 * L_92 = (__this->___style_5);
		NullCheck(L_92);
		RectOffset_t439 * L_93 = GUIStyle_get_padding_m5903(L_92, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_top_m2670(L_93, /*hidden argument*/&RectOffset_get_top_m2670_MethodInfo);
		GUIStyle_t999 * L_95 = (__this->___style_5);
		NullCheck(L_95);
		RectOffset_t439 * L_96 = GUIStyle_get_padding_m5903(L_95, /*hidden argument*/&GUIStyle_get_padding_m5903_MethodInfo);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_bottom_m5888(L_96, /*hidden argument*/&RectOffset_get_bottom_m5888_MethodInfo);
		NullCheck(L_89);
		L_89->___y_2 = ((float)(((float)(((float)(L_90-L_91))-(((float)L_94))))-(((float)L_97))));
	}

IL_02cb:
	{
		Vector2_t99 * L_98 = &(__this->___scrollOffset_11);
		Vector2_t99 * L_99 = &(__this->___scrollOffset_11);
		NullCheck(L_99);
		float L_100 = (L_99->___y_2);
		G_B16_0 = L_98;
		if ((((float)L_100) >= ((float)(0.0f))))
		{
			G_B17_0 = L_98;
			goto IL_02f0;
		}
	}
	{
		G_B18_0 = (0.0f);
		G_B18_1 = G_B16_0;
		goto IL_02fb;
	}

IL_02f0:
	{
		Vector2_t99 * L_101 = &(__this->___scrollOffset_11);
		NullCheck(L_101);
		float L_102 = (L_101->___y_2);
		G_B18_0 = L_102;
		G_B18_1 = G_B17_0;
	}

IL_02fb:
	{
		NullCheck(G_B18_1);
		G_B18_1->___y_2 = G_B18_0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern MethodInfo TextEditor_Copy_m2449_MethodInfo;
 void TextEditor_Copy_m2449 (TextEditor_t528 * __this, MethodInfo* method){
	String_t* V_0 = {0};
	{
		int32_t L_0 = (__this->___selectPos_2);
		int32_t L_1 = (__this->___pos_1);
		if ((((uint32_t)L_0) != ((uint32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = (__this->___isPasswordField_9);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___pos_1);
		int32_t L_4 = (__this->___selectPos_2);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0058;
		}
	}
	{
		GUIContent_t529 * L_5 = (__this->___content_4);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m2446(L_5, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_7 = (__this->___pos_1);
		int32_t L_8 = (__this->___selectPos_2);
		int32_t L_9 = (__this->___pos_1);
		NullCheck(L_6);
		String_t* L_10 = String_Substring_m2457(L_6, L_7, ((int32_t)(L_8-L_9)), /*hidden argument*/&String_Substring_m2457_MethodInfo);
		V_0 = L_10;
		goto IL_007c;
	}

IL_0058:
	{
		GUIContent_t529 * L_11 = (__this->___content_4);
		NullCheck(L_11);
		String_t* L_12 = GUIContent_get_text_m2446(L_11, /*hidden argument*/&GUIContent_get_text_m2446_MethodInfo);
		int32_t L_13 = (__this->___selectPos_2);
		int32_t L_14 = (__this->___pos_1);
		int32_t L_15 = (__this->___selectPos_2);
		NullCheck(L_12);
		String_t* L_16 = String_Substring_m2457(L_12, L_13, ((int32_t)(L_14-L_15)), /*hidden argument*/&String_Substring_m2457_MethodInfo);
		V_0 = L_16;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIUtility_t1012_il2cpp_TypeInfo));
		GUIUtility_set_systemCopyBuffer_m5798(NULL /*static, unused*/, V_0, /*hidden argument*/&GUIUtility_set_systemCopyBuffer_m5798_MethodInfo);
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
 String_t* TextEditor_ReplaceNewlinesWithSpaces_m6519 (Object_t * __this/* static, unused */, String_t* ___value, MethodInfo* method){
	{
		NullCheck(___value);
		String_t* L_0 = String_Replace_m6693(___value, (String_t*) &_stringLiteral467, (String_t*) &_stringLiteral178, /*hidden argument*/&String_Replace_m6693_MethodInfo);
		___value = L_0;
		NullCheck(___value);
		String_t* L_1 = String_Replace_m6694(___value, ((int32_t)10), ((int32_t)32), /*hidden argument*/&String_Replace_m6694_MethodInfo);
		___value = L_1;
		NullCheck(___value);
		String_t* L_2 = String_Replace_m6694(___value, ((int32_t)13), ((int32_t)32), /*hidden argument*/&String_Replace_m6694_MethodInfo);
		___value = L_2;
		return ___value;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern MethodInfo TextEditor_Paste_m2445_MethodInfo;
 bool TextEditor_Paste_m2445 (TextEditor_t528 * __this, MethodInfo* method){
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIUtility_t1012_il2cpp_TypeInfo));
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m5797(NULL /*static, unused*/, /*hidden argument*/&GUIUtility_get_systemCopyBuffer_m5797_MethodInfo);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_1 = String_op_Inequality_m658(NULL /*static, unused*/, V_0, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_op_Inequality_m658_MethodInfo);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		bool L_2 = (__this->___multiline_7);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = TextEditor_ReplaceNewlinesWithSpaces_m6519(NULL /*static, unused*/, V_0, /*hidden argument*/&TextEditor_ReplaceNewlinesWithSpaces_m6519_MethodInfo);
		V_0 = L_3;
	}

IL_0028:
	{
		TextEditor_ReplaceSelection_m6517(__this, V_0, /*hidden argument*/&TextEditor_ReplaceSelection_m6517_MethodInfo);
		return 1;
	}

IL_0031:
	{
		return 0;
	}
}
// Metadata Definition UnityEngine.TextEditor
extern Il2CppType TouchScreenKeyboard_t377_0_0_6;
FieldInfo TextEditor_t528____keyboardOnScreen_0_FieldInfo = 
{
	"keyboardOnScreen"/* name */
	, &TouchScreenKeyboard_t377_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___keyboardOnScreen_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo TextEditor_t528____pos_1_FieldInfo = 
{
	"pos"/* name */
	, &Int32_t123_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___pos_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo TextEditor_t528____selectPos_2_FieldInfo = 
{
	"selectPos"/* name */
	, &Int32_t123_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___selectPos_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo TextEditor_t528____controlID_3_FieldInfo = 
{
	"controlID"/* name */
	, &Int32_t123_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___controlID_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GUIContent_t529_0_0_6;
FieldInfo TextEditor_t528____content_4_FieldInfo = 
{
	"content"/* name */
	, &GUIContent_t529_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___content_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t999_0_0_6;
FieldInfo TextEditor_t528____style_5_FieldInfo = 
{
	"style"/* name */
	, &GUIStyle_t999_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___style_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rect_t103_0_0_6;
FieldInfo TextEditor_t528____position_6_FieldInfo = 
{
	"position"/* name */
	, &Rect_t103_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___position_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextEditor_t528____multiline_7_FieldInfo = 
{
	"multiline"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___multiline_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextEditor_t528____hasHorizontalCursorPos_8_FieldInfo = 
{
	"hasHorizontalCursorPos"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___hasHorizontalCursorPos_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextEditor_t528____isPasswordField_9_FieldInfo = 
{
	"isPasswordField"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___isPasswordField_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_3;
FieldInfo TextEditor_t528____m_HasFocus_10_FieldInfo = 
{
	"m_HasFocus"/* name */
	, &Boolean_t122_0_0_3/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_HasFocus_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo TextEditor_t528____scrollOffset_11_FieldInfo = 
{
	"scrollOffset"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___scrollOffset_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TextEditor_t528____m_TextHeightPotentiallyChanged_12_FieldInfo = 
{
	"m_TextHeightPotentiallyChanged"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_TextHeightPotentiallyChanged_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo TextEditor_t528____graphicalCursorPos_13_FieldInfo = 
{
	"graphicalCursorPos"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___graphicalCursorPos_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo TextEditor_t528____graphicalSelectCursorPos_14_FieldInfo = 
{
	"graphicalSelectCursorPos"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___graphicalSelectCursorPos_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TextEditor_t528____m_MouseDragSelectsWholeWords_15_FieldInfo = 
{
	"m_MouseDragSelectsWholeWords"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_MouseDragSelectsWholeWords_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo TextEditor_t528____m_DblClickInitPos_16_FieldInfo = 
{
	"m_DblClickInitPos"/* name */
	, &Int32_t123_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_DblClickInitPos_16)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1122_0_0_1;
FieldInfo TextEditor_t528____m_DblClickSnap_17_FieldInfo = 
{
	"m_DblClickSnap"/* name */
	, &DblClickSnapping_t1122_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_DblClickSnap_17)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TextEditor_t528____m_bJustSelected_18_FieldInfo = 
{
	"m_bJustSelected"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_bJustSelected_18)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo TextEditor_t528____m_iAltCursorPos_19_FieldInfo = 
{
	"m_iAltCursorPos"/* name */
	, &Int32_t123_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___m_iAltCursorPos_19)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo TextEditor_t528____oldText_20_FieldInfo = 
{
	"oldText"/* name */
	, &String_t_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___oldText_20)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo TextEditor_t528____oldPos_21_FieldInfo = 
{
	"oldPos"/* name */
	, &Int32_t123_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___oldPos_21)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo TextEditor_t528____oldSelectPos_22_FieldInfo = 
{
	"oldSelectPos"/* name */
	, &Int32_t123_0_0_1/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528, ___oldSelectPos_22)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t1124_0_0_17;
FieldInfo TextEditor_t528____s_Keyactions_23_FieldInfo = 
{
	"s_Keyactions"/* name */
	, &Dictionary_2_t1124_0_0_17/* type */
	, &TextEditor_t528_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t528_StaticFields, ___s_Keyactions_23)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextEditor_t528_FieldInfos[] =
{
	&TextEditor_t528____keyboardOnScreen_0_FieldInfo,
	&TextEditor_t528____pos_1_FieldInfo,
	&TextEditor_t528____selectPos_2_FieldInfo,
	&TextEditor_t528____controlID_3_FieldInfo,
	&TextEditor_t528____content_4_FieldInfo,
	&TextEditor_t528____style_5_FieldInfo,
	&TextEditor_t528____position_6_FieldInfo,
	&TextEditor_t528____multiline_7_FieldInfo,
	&TextEditor_t528____hasHorizontalCursorPos_8_FieldInfo,
	&TextEditor_t528____isPasswordField_9_FieldInfo,
	&TextEditor_t528____m_HasFocus_10_FieldInfo,
	&TextEditor_t528____scrollOffset_11_FieldInfo,
	&TextEditor_t528____m_TextHeightPotentiallyChanged_12_FieldInfo,
	&TextEditor_t528____graphicalCursorPos_13_FieldInfo,
	&TextEditor_t528____graphicalSelectCursorPos_14_FieldInfo,
	&TextEditor_t528____m_MouseDragSelectsWholeWords_15_FieldInfo,
	&TextEditor_t528____m_DblClickInitPos_16_FieldInfo,
	&TextEditor_t528____m_DblClickSnap_17_FieldInfo,
	&TextEditor_t528____m_bJustSelected_18_FieldInfo,
	&TextEditor_t528____m_iAltCursorPos_19_FieldInfo,
	&TextEditor_t528____oldText_20_FieldInfo,
	&TextEditor_t528____oldPos_21_FieldInfo,
	&TextEditor_t528____oldSelectPos_22_FieldInfo,
	&TextEditor_t528____s_Keyactions_23_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
MethodInfo TextEditor__ctor_m2444_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m2444/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
MethodInfo TextEditor_ClearCursorPos_m6514_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m6514/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
MethodInfo TextEditor_OnFocus_m2448_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m2448/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
MethodInfo TextEditor_SelectAll_m6515_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m6515/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
MethodInfo TextEditor_DeleteSelection_m6516_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m6516/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextEditor_t528_TextEditor_ReplaceSelection_m6517_ParameterInfos[] = 
{
	{"replace", 0, 134219375, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
MethodInfo TextEditor_ReplaceSelection_m6517_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m6517/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextEditor_t528_TextEditor_ReplaceSelection_m6517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
MethodInfo TextEditor_UpdateScrollOffset_m6518_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m6518/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
MethodInfo TextEditor_Copy_m2449_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m2449/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextEditor_t528_TextEditor_ReplaceNewlinesWithSpaces_m6519_ParameterInfos[] = 
{
	{"value", 0, 134219376, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m6519_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m6519/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t528_TextEditor_ReplaceNewlinesWithSpaces_m6519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
MethodInfo TextEditor_Paste_m2445_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m2445/* method */
	, &TextEditor_t528_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextEditor_t528_MethodInfos[] =
{
	&TextEditor__ctor_m2444_MethodInfo,
	&TextEditor_ClearCursorPos_m6514_MethodInfo,
	&TextEditor_OnFocus_m2448_MethodInfo,
	&TextEditor_SelectAll_m6515_MethodInfo,
	&TextEditor_DeleteSelection_m6516_MethodInfo,
	&TextEditor_ReplaceSelection_m6517_MethodInfo,
	&TextEditor_UpdateScrollOffset_m6518_MethodInfo,
	&TextEditor_Copy_m2449_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m6519_MethodInfo,
	&TextEditor_Paste_m2445_MethodInfo,
	NULL
};
extern TypeInfo DblClickSnapping_t1122_il2cpp_TypeInfo;
extern TypeInfo TextEditOp_t1123_il2cpp_TypeInfo;
static TypeInfo* TextEditor_t528_il2cpp_TypeInfo__nestedTypes[3] =
{
	&DblClickSnapping_t1122_il2cpp_TypeInfo,
	&TextEditOp_t1123_il2cpp_TypeInfo,
	NULL
};
static MethodInfo* TextEditor_t528_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextEditor_t528_0_0_0;
extern Il2CppType TextEditor_t528_1_0_0;
struct TextEditor_t528;
TypeInfo TextEditor_t528_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t528_MethodInfos/* methods */
	, NULL/* properties */
	, TextEditor_t528_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, TextEditor_t528_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &TextEditor_t528_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextEditor_t528_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextEditor_t528_il2cpp_TypeInfo/* cast_class */
	, &TextEditor_t528_0_0_0/* byval_arg */
	, &TextEditor_t528_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t528)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t528_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextGenerationSettings_t413_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"

// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
extern TypeInfo Color32_t463_il2cpp_TypeInfo;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// System.ValueType
#include "mscorlib_System_ValueTypeMethodDeclarations.h"
extern MethodInfo Color32_op_Implicit_m2313_MethodInfo;
extern MethodInfo TextGenerationSettings_CompareColors_m6520_MethodInfo;
extern MethodInfo TextGenerationSettings_CompareVector2_m6521_MethodInfo;


// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
 bool TextGenerationSettings_CompareColors_m6520 (TextGenerationSettings_t413 * __this, Color_t66  ___left, Color_t66  ___right, MethodInfo* method){
	Color32_t463  V_0 = {0};
	Color32_t463  V_1 = {0};
	{
		Color32_t463  L_0 = Color32_op_Implicit_m2313(NULL /*static, unused*/, ___left, /*hidden argument*/&Color32_op_Implicit_m2313_MethodInfo);
		V_0 = L_0;
		Color32_t463  L_1 = Color32_op_Implicit_m2313(NULL /*static, unused*/, ___right, /*hidden argument*/&Color32_op_Implicit_m2313_MethodInfo);
		V_1 = L_1;
		Color32_t463  L_2 = V_0;
		Object_t * L_3 = Box(InitializedTypeInfo(&Color32_t463_il2cpp_TypeInfo), &L_2);
		Color32_t463  L_4 = V_1;
		Object_t * L_5 = Box(InitializedTypeInfo(&Color32_t463_il2cpp_TypeInfo), &L_4);
		NullCheck(L_3);
		bool L_6 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&ValueType_Equals_m2145_MethodInfo, L_3, L_5);
		return L_6;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
 bool TextGenerationSettings_CompareVector2_m6521 (TextGenerationSettings_t413 * __this, Vector2_t99  ___left, Vector2_t99  ___right, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		NullCheck((&___left));
		float L_0 = ((&___left)->___x_1);
		NullCheck((&___right));
		float L_1 = ((&___right)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		bool L_2 = Mathf_Approximately_m2186(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/&Mathf_Approximately_m2186_MethodInfo);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		NullCheck((&___left));
		float L_3 = ((&___left)->___y_2);
		NullCheck((&___right));
		float L_4 = ((&___right)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		bool L_5 = Mathf_Approximately_m2186(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/&Mathf_Approximately_m2186_MethodInfo);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern MethodInfo TextGenerationSettings_Equals_m6522_MethodInfo;
 bool TextGenerationSettings_Equals_m6522 (TextGenerationSettings_t413 * __this, TextGenerationSettings_t413  ___other, MethodInfo* method){
	int32_t G_B19_0 = 0;
	{
		Color_t66  L_0 = (__this->___color_1);
		NullCheck((&___other));
		Color_t66  L_1 = ((&___other)->___color_1);
		bool L_2 = TextGenerationSettings_CompareColors_m6520(__this, L_0, L_1, /*hidden argument*/&TextGenerationSettings_CompareColors_m6520_MethodInfo);
		if (!L_2)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_3 = (__this->___fontSize_2);
		NullCheck((&___other));
		int32_t L_4 = ((&___other)->___fontSize_2);
		if ((((uint32_t)L_3) != ((uint32_t)L_4)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_5 = (__this->___resizeTextMinSize_8);
		NullCheck((&___other));
		int32_t L_6 = ((&___other)->___resizeTextMinSize_8);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_7 = (__this->___resizeTextMaxSize_9);
		NullCheck((&___other));
		int32_t L_8 = ((&___other)->___resizeTextMaxSize_9);
		if ((((uint32_t)L_7) != ((uint32_t)L_8)))
		{
			goto IL_015d;
		}
	}
	{
		float L_9 = (__this->___lineSpacing_3);
		NullCheck((&___other));
		float L_10 = ((&___other)->___lineSpacing_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		bool L_11 = Mathf_Approximately_m2186(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/&Mathf_Approximately_m2186_MethodInfo);
		if (!L_11)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_12 = (__this->___fontStyle_5);
		NullCheck((&___other));
		int32_t L_13 = ((&___other)->___fontStyle_5);
		if ((((uint32_t)L_12) != ((uint32_t)L_13)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_14 = (__this->___richText_4);
		NullCheck((&___other));
		bool L_15 = ((&___other)->___richText_4);
		if ((((uint32_t)L_14) != ((uint32_t)L_15)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_16 = (__this->___textAnchor_6);
		NullCheck((&___other));
		int32_t L_17 = ((&___other)->___textAnchor_6);
		if ((((uint32_t)L_16) != ((uint32_t)L_17)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_18 = (__this->___resizeTextForBestFit_7);
		NullCheck((&___other));
		bool L_19 = ((&___other)->___resizeTextForBestFit_7);
		if ((((uint32_t)L_18) != ((uint32_t)L_19)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_20 = (__this->___resizeTextMinSize_8);
		NullCheck((&___other));
		int32_t L_21 = ((&___other)->___resizeTextMinSize_8);
		if ((((uint32_t)L_20) != ((uint32_t)L_21)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_22 = (__this->___resizeTextMaxSize_9);
		NullCheck((&___other));
		int32_t L_23 = ((&___other)->___resizeTextMaxSize_9);
		if ((((uint32_t)L_22) != ((uint32_t)L_23)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_24 = (__this->___resizeTextForBestFit_7);
		NullCheck((&___other));
		bool L_25 = ((&___other)->___resizeTextForBestFit_7);
		if ((((uint32_t)L_24) != ((uint32_t)L_25)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_26 = (__this->___updateBounds_10);
		NullCheck((&___other));
		bool L_27 = ((&___other)->___updateBounds_10);
		if ((((uint32_t)L_26) != ((uint32_t)L_27)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_28 = (__this->___horizontalOverflow_12);
		NullCheck((&___other));
		int32_t L_29 = ((&___other)->___horizontalOverflow_12);
		if ((((uint32_t)L_28) != ((uint32_t)L_29)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_30 = (__this->___verticalOverflow_11);
		NullCheck((&___other));
		int32_t L_31 = ((&___other)->___verticalOverflow_11);
		if ((((uint32_t)L_30) != ((uint32_t)L_31)))
		{
			goto IL_015d;
		}
	}
	{
		Vector2_t99  L_32 = (__this->___generationExtents_13);
		NullCheck((&___other));
		Vector2_t99  L_33 = ((&___other)->___generationExtents_13);
		bool L_34 = TextGenerationSettings_CompareVector2_m6521(__this, L_32, L_33, /*hidden argument*/&TextGenerationSettings_CompareVector2_m6521_MethodInfo);
		if (!L_34)
		{
			goto IL_015d;
		}
	}
	{
		Vector2_t99  L_35 = (__this->___pivot_14);
		NullCheck((&___other));
		Vector2_t99  L_36 = ((&___other)->___pivot_14);
		bool L_37 = TextGenerationSettings_CompareVector2_m6521(__this, L_35, L_36, /*hidden argument*/&TextGenerationSettings_CompareVector2_m6521_MethodInfo);
		if (!L_37)
		{
			goto IL_015d;
		}
	}
	{
		Font_t332 * L_38 = (__this->___font_0);
		NullCheck((&___other));
		Font_t332 * L_39 = ((&___other)->___font_0);
		bool L_40 = Object_op_Equality_m524(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		G_B19_0 = ((int32_t)(L_40));
		goto IL_015e;
	}

IL_015d:
	{
		G_B19_0 = 0;
	}

IL_015e:
	{
		return G_B19_0;
	}
}
// Metadata Definition UnityEngine.TextGenerationSettings
extern Il2CppType Font_t332_0_0_6;
FieldInfo TextGenerationSettings_t413____font_0_FieldInfo = 
{
	"font"/* name */
	, &Font_t332_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___font_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Color_t66_0_0_6;
FieldInfo TextGenerationSettings_t413____color_1_FieldInfo = 
{
	"color"/* name */
	, &Color_t66_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___color_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo TextGenerationSettings_t413____fontSize_2_FieldInfo = 
{
	"fontSize"/* name */
	, &Int32_t123_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___fontSize_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TextGenerationSettings_t413____lineSpacing_3_FieldInfo = 
{
	"lineSpacing"/* name */
	, &Single_t170_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___lineSpacing_3) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextGenerationSettings_t413____richText_4_FieldInfo = 
{
	"richText"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___richText_4) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FontStyle_t502_0_0_6;
FieldInfo TextGenerationSettings_t413____fontStyle_5_FieldInfo = 
{
	"fontStyle"/* name */
	, &FontStyle_t502_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___fontStyle_5) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextAnchor_t503_0_0_6;
FieldInfo TextGenerationSettings_t413____textAnchor_6_FieldInfo = 
{
	"textAnchor"/* name */
	, &TextAnchor_t503_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___textAnchor_6) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextGenerationSettings_t413____resizeTextForBestFit_7_FieldInfo = 
{
	"resizeTextForBestFit"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___resizeTextForBestFit_7) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo TextGenerationSettings_t413____resizeTextMinSize_8_FieldInfo = 
{
	"resizeTextMinSize"/* name */
	, &Int32_t123_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___resizeTextMinSize_8) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo TextGenerationSettings_t413____resizeTextMaxSize_9_FieldInfo = 
{
	"resizeTextMaxSize"/* name */
	, &Int32_t123_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___resizeTextMaxSize_9) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextGenerationSettings_t413____updateBounds_10_FieldInfo = 
{
	"updateBounds"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___updateBounds_10) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType VerticalWrapMode_t505_0_0_6;
FieldInfo TextGenerationSettings_t413____verticalOverflow_11_FieldInfo = 
{
	"verticalOverflow"/* name */
	, &VerticalWrapMode_t505_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___verticalOverflow_11) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HorizontalWrapMode_t504_0_0_6;
FieldInfo TextGenerationSettings_t413____horizontalOverflow_12_FieldInfo = 
{
	"horizontalOverflow"/* name */
	, &HorizontalWrapMode_t504_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___horizontalOverflow_12) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo TextGenerationSettings_t413____generationExtents_13_FieldInfo = 
{
	"generationExtents"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___generationExtents_13) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo TextGenerationSettings_t413____pivot_14_FieldInfo = 
{
	"pivot"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___pivot_14) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TextGenerationSettings_t413____generateOutOfBounds_15_FieldInfo = 
{
	"generateOutOfBounds"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t413, ___generateOutOfBounds_15) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextGenerationSettings_t413_FieldInfos[] =
{
	&TextGenerationSettings_t413____font_0_FieldInfo,
	&TextGenerationSettings_t413____color_1_FieldInfo,
	&TextGenerationSettings_t413____fontSize_2_FieldInfo,
	&TextGenerationSettings_t413____lineSpacing_3_FieldInfo,
	&TextGenerationSettings_t413____richText_4_FieldInfo,
	&TextGenerationSettings_t413____fontStyle_5_FieldInfo,
	&TextGenerationSettings_t413____textAnchor_6_FieldInfo,
	&TextGenerationSettings_t413____resizeTextForBestFit_7_FieldInfo,
	&TextGenerationSettings_t413____resizeTextMinSize_8_FieldInfo,
	&TextGenerationSettings_t413____resizeTextMaxSize_9_FieldInfo,
	&TextGenerationSettings_t413____updateBounds_10_FieldInfo,
	&TextGenerationSettings_t413____verticalOverflow_11_FieldInfo,
	&TextGenerationSettings_t413____horizontalOverflow_12_FieldInfo,
	&TextGenerationSettings_t413____generationExtents_13_FieldInfo,
	&TextGenerationSettings_t413____pivot_14_FieldInfo,
	&TextGenerationSettings_t413____generateOutOfBounds_15_FieldInfo,
	NULL
};
extern Il2CppType Color_t66_0_0_0;
extern Il2CppType Color_t66_0_0_0;
extern Il2CppType Color_t66_0_0_0;
static ParameterInfo TextGenerationSettings_t413_TextGenerationSettings_CompareColors_m6520_ParameterInfos[] = 
{
	{"left", 0, 134219377, &EmptyCustomAttributesCache, &Color_t66_0_0_0},
	{"right", 1, 134219378, &EmptyCustomAttributesCache, &Color_t66_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Color_t66_Color_t66 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
MethodInfo TextGenerationSettings_CompareColors_m6520_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m6520/* method */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Color_t66_Color_t66/* invoker_method */
	, TextGenerationSettings_t413_TextGenerationSettings_CompareColors_m6520_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo TextGenerationSettings_t413_TextGenerationSettings_CompareVector2_m6521_ParameterInfos[] = 
{
	{"left", 0, 134219379, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"right", 1, 134219380, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector2_t99_Vector2_t99 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
MethodInfo TextGenerationSettings_CompareVector2_m6521_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m6521/* method */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector2_t99_Vector2_t99/* invoker_method */
	, TextGenerationSettings_t413_TextGenerationSettings_CompareVector2_m6521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextGenerationSettings_t413_0_0_0;
extern Il2CppType TextGenerationSettings_t413_0_0_0;
static ParameterInfo TextGenerationSettings_t413_TextGenerationSettings_Equals_m6522_ParameterInfos[] = 
{
	{"other", 0, 134219381, &EmptyCustomAttributesCache, &TextGenerationSettings_t413_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_TextGenerationSettings_t413 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
MethodInfo TextGenerationSettings_Equals_m6522_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m6522/* method */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_TextGenerationSettings_t413/* invoker_method */
	, TextGenerationSettings_t413_TextGenerationSettings_Equals_m6522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextGenerationSettings_t413_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m6520_MethodInfo,
	&TextGenerationSettings_CompareVector2_m6521_MethodInfo,
	&TextGenerationSettings_Equals_m6522_MethodInfo,
	NULL
};
static MethodInfo* TextGenerationSettings_t413_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextGenerationSettings_t413_1_0_0;
TypeInfo TextGenerationSettings_t413_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t413_MethodInfos/* methods */
	, NULL/* properties */
	, TextGenerationSettings_t413_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextGenerationSettings_t413_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextGenerationSettings_t413_il2cpp_TypeInfo/* cast_class */
	, &TextGenerationSettings_t413_0_0_0/* byval_arg */
	, &TextGenerationSettings_t413_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t413)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TrackedReference_t1072_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"

extern TypeInfo IntPtr_t121_il2cpp_TypeInfo;
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
extern MethodInfo TrackedReference_op_Equality_m6525_MethodInfo;
extern MethodInfo IntPtr_op_Explicit_m6709_MethodInfo;
extern MethodInfo IntPtr_op_Equality_m4765_MethodInfo;


// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern MethodInfo TrackedReference_Equals_m6523_MethodInfo;
 bool TrackedReference_Equals_m6523 (TrackedReference_t1072 * __this, Object_t * ___o, MethodInfo* method){
	{
		bool L_0 = TrackedReference_op_Equality_m6525(NULL /*static, unused*/, ((TrackedReference_t1072 *)IsInst(___o, InitializedTypeInfo(&TrackedReference_t1072_il2cpp_TypeInfo))), __this, /*hidden argument*/&TrackedReference_op_Equality_m6525_MethodInfo);
		return L_0;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern MethodInfo TrackedReference_GetHashCode_m6524_MethodInfo;
 int32_t TrackedReference_GetHashCode_m6524 (TrackedReference_t1072 * __this, MethodInfo* method){
	{
		IntPtr_t121 L_0 = (__this->___m_Ptr_0);
		int32_t L_1 = IntPtr_op_Explicit_m6709(NULL /*static, unused*/, L_0, /*hidden argument*/&IntPtr_op_Explicit_m6709_MethodInfo);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
 bool TrackedReference_op_Equality_m6525 (Object_t * __this/* static, unused */, TrackedReference_t1072 * ___x, TrackedReference_t1072 * ___y, MethodInfo* method){
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = ___x;
		V_1 = ___y;
		if (V_1)
		{
			goto IL_0012;
		}
	}
	{
		if (V_0)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		if (V_1)
		{
			goto IL_0029;
		}
	}
	{
		NullCheck(___x);
		IntPtr_t121 L_0 = (___x->___m_Ptr_0);
		bool L_1 = IntPtr_op_Equality_m4765(NULL /*static, unused*/, L_0, (((IntPtr_t121_StaticFields*)InitializedTypeInfo(&IntPtr_t121_il2cpp_TypeInfo)->static_fields)->___Zero_1), /*hidden argument*/&IntPtr_op_Equality_m4765_MethodInfo);
		return L_1;
	}

IL_0029:
	{
		if (V_0)
		{
			goto IL_0040;
		}
	}
	{
		NullCheck(___y);
		IntPtr_t121 L_2 = (___y->___m_Ptr_0);
		bool L_3 = IntPtr_op_Equality_m4765(NULL /*static, unused*/, L_2, (((IntPtr_t121_StaticFields*)InitializedTypeInfo(&IntPtr_t121_il2cpp_TypeInfo)->static_fields)->___Zero_1), /*hidden argument*/&IntPtr_op_Equality_m4765_MethodInfo);
		return L_3;
	}

IL_0040:
	{
		NullCheck(___x);
		IntPtr_t121 L_4 = (___x->___m_Ptr_0);
		NullCheck(___y);
		IntPtr_t121 L_5 = (___y->___m_Ptr_0);
		bool L_6 = IntPtr_op_Equality_m4765(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/&IntPtr_op_Equality_m4765_MethodInfo);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
void TrackedReference_t1072_marshal(const TrackedReference_t1072& unmarshaled, TrackedReference_t1072_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.___m_Ptr_0;
}
void TrackedReference_t1072_marshal_back(const TrackedReference_t1072_marshaled& marshaled, TrackedReference_t1072& unmarshaled)
{
	unmarshaled.___m_Ptr_0 = marshaled.___m_Ptr_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
void TrackedReference_t1072_marshal_cleanup(TrackedReference_t1072_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.TrackedReference
extern Il2CppType IntPtr_t121_0_0_3;
FieldInfo TrackedReference_t1072____m_Ptr_0_FieldInfo = 
{
	"m_Ptr"/* name */
	, &IntPtr_t121_0_0_3/* type */
	, &TrackedReference_t1072_il2cpp_TypeInfo/* parent */
	, offsetof(TrackedReference_t1072, ___m_Ptr_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TrackedReference_t1072_FieldInfos[] =
{
	&TrackedReference_t1072____m_Ptr_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo TrackedReference_t1072_TrackedReference_Equals_m6523_ParameterInfos[] = 
{
	{"o", 0, 134219382, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
MethodInfo TrackedReference_Equals_m6523_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m6523/* method */
	, &TrackedReference_t1072_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, TrackedReference_t1072_TrackedReference_Equals_m6523_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
MethodInfo TrackedReference_GetHashCode_m6524_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m6524/* method */
	, &TrackedReference_t1072_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TrackedReference_t1072_0_0_0;
extern Il2CppType TrackedReference_t1072_0_0_0;
extern Il2CppType TrackedReference_t1072_0_0_0;
static ParameterInfo TrackedReference_t1072_TrackedReference_op_Equality_m6525_ParameterInfos[] = 
{
	{"x", 0, 134219383, &EmptyCustomAttributesCache, &TrackedReference_t1072_0_0_0},
	{"y", 1, 134219384, &EmptyCustomAttributesCache, &TrackedReference_t1072_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
MethodInfo TrackedReference_op_Equality_m6525_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m6525/* method */
	, &TrackedReference_t1072_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, TrackedReference_t1072_TrackedReference_op_Equality_m6525_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TrackedReference_t1072_MethodInfos[] =
{
	&TrackedReference_Equals_m6523_MethodInfo,
	&TrackedReference_GetHashCode_m6524_MethodInfo,
	&TrackedReference_op_Equality_m6525_MethodInfo,
	NULL
};
static MethodInfo* TrackedReference_t1072_VTable[] =
{
	&TrackedReference_Equals_m6523_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&TrackedReference_GetHashCode_m6524_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TrackedReference_t1072_1_0_0;
struct TrackedReference_t1072;
TypeInfo TrackedReference_t1072_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t1072_MethodInfos/* methods */
	, NULL/* properties */
	, TrackedReference_t1072_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TrackedReference_t1072_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TrackedReference_t1072_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TrackedReference_t1072_il2cpp_TypeInfo/* cast_class */
	, &TrackedReference_t1072_0_0_0/* byval_arg */
	, &TrackedReference_t1072_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t1072_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t1072_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t1072_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t1072)/* instance_size */
	, 0/* element_size */
	, sizeof(TrackedReference_t1072_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PersistentListenerMode_t1125_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"



// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo PersistentListenerMode_t1125____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentListenerMode_t1125, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____EventDefined_2_FieldInfo = 
{
	"EventDefined"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____Void_3_FieldInfo = 
{
	"Void"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____Object_4_FieldInfo = 
{
	"Object"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____Int_5_FieldInfo = 
{
	"Int"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____Float_6_FieldInfo = 
{
	"Float"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____String_7_FieldInfo = 
{
	"String"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_32854;
FieldInfo PersistentListenerMode_t1125____Bool_8_FieldInfo = 
{
	"Bool"/* name */
	, &PersistentListenerMode_t1125_0_0_32854/* type */
	, &PersistentListenerMode_t1125_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* PersistentListenerMode_t1125_FieldInfos[] =
{
	&PersistentListenerMode_t1125____value___1_FieldInfo,
	&PersistentListenerMode_t1125____EventDefined_2_FieldInfo,
	&PersistentListenerMode_t1125____Void_3_FieldInfo,
	&PersistentListenerMode_t1125____Object_4_FieldInfo,
	&PersistentListenerMode_t1125____Int_5_FieldInfo,
	&PersistentListenerMode_t1125____Float_6_FieldInfo,
	&PersistentListenerMode_t1125____String_7_FieldInfo,
	&PersistentListenerMode_t1125____Bool_8_FieldInfo,
	NULL
};
static const int32_t PersistentListenerMode_t1125____EventDefined_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____EventDefined_2_DefaultValue = 
{
	&PersistentListenerMode_t1125____EventDefined_2_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____EventDefined_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1125____Void_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____Void_3_DefaultValue = 
{
	&PersistentListenerMode_t1125____Void_3_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____Void_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1125____Object_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____Object_4_DefaultValue = 
{
	&PersistentListenerMode_t1125____Object_4_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____Object_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1125____Int_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____Int_5_DefaultValue = 
{
	&PersistentListenerMode_t1125____Int_5_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____Int_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1125____Float_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____Float_6_DefaultValue = 
{
	&PersistentListenerMode_t1125____Float_6_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____Float_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1125____String_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____String_7_DefaultValue = 
{
	&PersistentListenerMode_t1125____String_7_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____String_7_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1125____Bool_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1125____Bool_8_DefaultValue = 
{
	&PersistentListenerMode_t1125____Bool_8_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1125____Bool_8_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* PersistentListenerMode_t1125_FieldDefaultValues[] = 
{
	&PersistentListenerMode_t1125____EventDefined_2_DefaultValue,
	&PersistentListenerMode_t1125____Void_3_DefaultValue,
	&PersistentListenerMode_t1125____Object_4_DefaultValue,
	&PersistentListenerMode_t1125____Int_5_DefaultValue,
	&PersistentListenerMode_t1125____Float_6_DefaultValue,
	&PersistentListenerMode_t1125____String_7_DefaultValue,
	&PersistentListenerMode_t1125____Bool_8_DefaultValue,
	NULL
};
static MethodInfo* PersistentListenerMode_t1125_MethodInfos[] =
{
	NULL
};
static MethodInfo* PersistentListenerMode_t1125_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1125_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentListenerMode_t1125_0_0_0;
extern Il2CppType PersistentListenerMode_t1125_1_0_0;
TypeInfo PersistentListenerMode_t1125_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t1125_MethodInfos/* methods */
	, NULL/* properties */
	, PersistentListenerMode_t1125_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PersistentListenerMode_t1125_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &PersistentListenerMode_t1125_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1125_1_0_0/* this_arg */
	, PersistentListenerMode_t1125_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, PersistentListenerMode_t1125_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1125)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ArgumentCache_t1126_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"



// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern MethodInfo ArgumentCache__ctor_m6526_MethodInfo;
 void ArgumentCache__ctor_m6526 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern MethodInfo ArgumentCache_get_unityObjectArgument_m6527_MethodInfo;
 Object_t120 * ArgumentCache_get_unityObjectArgument_m6527 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		Object_t120 * L_0 = (__this->___m_ObjectArgument_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo;
 String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_ObjectArgumentAssemblyTypeName_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern MethodInfo ArgumentCache_get_intArgument_m6529_MethodInfo;
 int32_t ArgumentCache_get_intArgument_m6529 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_IntArgument_2);
		return L_0;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern MethodInfo ArgumentCache_get_floatArgument_m6530_MethodInfo;
 float ArgumentCache_get_floatArgument_m6530 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		float L_0 = (__this->___m_FloatArgument_3);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern MethodInfo ArgumentCache_get_stringArgument_m6531_MethodInfo;
 String_t* ArgumentCache_get_stringArgument_m6531 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_StringArgument_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern MethodInfo ArgumentCache_get_boolArgument_m6532_MethodInfo;
 bool ArgumentCache_get_boolArgument_m6532 (ArgumentCache_t1126 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_BoolArgument_5);
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.ArgumentCache
extern Il2CppType Object_t120_0_0_1;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgument;
FieldInfo ArgumentCache_t1126____m_ObjectArgument_0_FieldInfo = 
{
	"m_ObjectArgument"/* name */
	, &Object_t120_0_0_1/* type */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1126, ___m_ObjectArgument_0)/* data */
	, &ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgument/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName;
FieldInfo ArgumentCache_t1126____m_ObjectArgumentAssemblyTypeName_1_FieldInfo = 
{
	"m_ObjectArgumentAssemblyTypeName"/* name */
	, &String_t_0_0_1/* type */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1126, ___m_ObjectArgumentAssemblyTypeName_1)/* data */
	, &ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_IntArgument;
FieldInfo ArgumentCache_t1126____m_IntArgument_2_FieldInfo = 
{
	"m_IntArgument"/* name */
	, &Int32_t123_0_0_1/* type */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1126, ___m_IntArgument_2)/* data */
	, &ArgumentCache_t1126__CustomAttributeCache_m_IntArgument/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_FloatArgument;
FieldInfo ArgumentCache_t1126____m_FloatArgument_3_FieldInfo = 
{
	"m_FloatArgument"/* name */
	, &Single_t170_0_0_1/* type */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1126, ___m_FloatArgument_3)/* data */
	, &ArgumentCache_t1126__CustomAttributeCache_m_FloatArgument/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_StringArgument;
FieldInfo ArgumentCache_t1126____m_StringArgument_4_FieldInfo = 
{
	"m_StringArgument"/* name */
	, &String_t_0_0_1/* type */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1126, ___m_StringArgument_4)/* data */
	, &ArgumentCache_t1126__CustomAttributeCache_m_StringArgument/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_BoolArgument;
FieldInfo ArgumentCache_t1126____m_BoolArgument_5_FieldInfo = 
{
	"m_BoolArgument"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1126, ___m_BoolArgument_5)/* data */
	, &ArgumentCache_t1126__CustomAttributeCache_m_BoolArgument/* custom_attributes_cache */

};
static FieldInfo* ArgumentCache_t1126_FieldInfos[] =
{
	&ArgumentCache_t1126____m_ObjectArgument_0_FieldInfo,
	&ArgumentCache_t1126____m_ObjectArgumentAssemblyTypeName_1_FieldInfo,
	&ArgumentCache_t1126____m_IntArgument_2_FieldInfo,
	&ArgumentCache_t1126____m_FloatArgument_3_FieldInfo,
	&ArgumentCache_t1126____m_StringArgument_4_FieldInfo,
	&ArgumentCache_t1126____m_BoolArgument_5_FieldInfo,
	NULL
};
static PropertyInfo ArgumentCache_t1126____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m6527_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1126____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1126____intArgument_PropertyInfo = 
{
	&ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m6529_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1126____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m6530_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1126____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m6531_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1126____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t1126_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m6532_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ArgumentCache_t1126_PropertyInfos[] =
{
	&ArgumentCache_t1126____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t1126____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t1126____intArgument_PropertyInfo,
	&ArgumentCache_t1126____floatArgument_PropertyInfo,
	&ArgumentCache_t1126____stringArgument_PropertyInfo,
	&ArgumentCache_t1126____boolArgument_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
MethodInfo ArgumentCache__ctor_m6526_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m6526/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
MethodInfo ArgumentCache_get_unityObjectArgument_m6527_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m6527/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Object_t120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
MethodInfo ArgumentCache_get_intArgument_m6529_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m6529/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t170_0_0_0;
extern void* RuntimeInvoker_Single_t170 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
MethodInfo ArgumentCache_get_floatArgument_m6530_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m6530/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Single_t170_0_0_0/* return_type */
	, RuntimeInvoker_Single_t170/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
MethodInfo ArgumentCache_get_stringArgument_m6531_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m6531/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
MethodInfo ArgumentCache_get_boolArgument_m6532_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m6532/* method */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ArgumentCache_t1126_MethodInfos[] =
{
	&ArgumentCache__ctor_m6526_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m6527_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo,
	&ArgumentCache_get_intArgument_m6529_MethodInfo,
	&ArgumentCache_get_floatArgument_m6530_MethodInfo,
	&ArgumentCache_get_stringArgument_m6531_MethodInfo,
	&ArgumentCache_get_boolArgument_m6532_MethodInfo,
	NULL
};
static MethodInfo* ArgumentCache_t1126_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern TypeInfo SerializeField_t469_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern MethodInfo SerializeField__ctor_m2084_MethodInfo;
extern TypeInfo FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern MethodInfo FormerlySerializedAsAttribute__ctor_m2083_MethodInfo;
void ArgumentCache_t1126_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1126_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1126_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1126_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1126_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1126_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgument = {
2,
NULL,
&ArgumentCache_t1126_CustomAttributesCacheGenerator_m_ObjectArgument
};
CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName = {
2,
NULL,
&ArgumentCache_t1126_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName
};
CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_IntArgument = {
2,
NULL,
&ArgumentCache_t1126_CustomAttributesCacheGenerator_m_IntArgument
};
CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_FloatArgument = {
2,
NULL,
&ArgumentCache_t1126_CustomAttributesCacheGenerator_m_FloatArgument
};
CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_StringArgument = {
2,
NULL,
&ArgumentCache_t1126_CustomAttributesCacheGenerator_m_StringArgument
};
CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_BoolArgument = {
1,
NULL,
&ArgumentCache_t1126_CustomAttributesCacheGenerator_m_BoolArgument
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ArgumentCache_t1126_0_0_0;
extern Il2CppType ArgumentCache_t1126_1_0_0;
struct ArgumentCache_t1126;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgument;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_IntArgument;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_FloatArgument;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_StringArgument;
extern CustomAttributesCache ArgumentCache_t1126__CustomAttributeCache_m_BoolArgument;
TypeInfo ArgumentCache_t1126_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t1126_MethodInfos/* methods */
	, ArgumentCache_t1126_PropertyInfos/* properties */
	, ArgumentCache_t1126_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ArgumentCache_t1126_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ArgumentCache_t1126_il2cpp_TypeInfo/* cast_class */
	, &ArgumentCache_t1126_0_0_0/* byval_arg */
	, &ArgumentCache_t1126_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1126)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
extern TypeInfo ArgumentNullException_t1224_il2cpp_TypeInfo;
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
extern MethodInfo ArgumentNullException__ctor_m6710_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
extern MethodInfo MethodBase_get_IsStatic_m6712_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;


// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
 void BaseInvokableCall__ctor_m6533 (BaseInvokableCall_t1127 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
 void BaseInvokableCall__ctor_m6534 (BaseInvokableCall_t1127 * __this, Object_t * ___target, MethodInfo_t141 * ___function, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		if (___target)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral468, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0017:
	{
		if (___function)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1224 * L_1 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_1, (String_t*) &_stringLiteral469, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
 bool BaseInvokableCall_AllowInvoke_m6535 (Object_t * __this/* static, unused */, Delegate_t152 * ___delegate, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		NullCheck(___delegate);
		MethodInfo_t141 * L_0 = Delegate_get_Method_m6711(___delegate, /*hidden argument*/&Delegate_get_Method_m6711_MethodInfo);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&MethodBase_get_IsStatic_m6712_MethodInfo, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		NullCheck(___delegate);
		Object_t * L_2 = Delegate_get_Target_m6713(___delegate, /*hidden argument*/&Delegate_get_Target_m6713_MethodInfo);
		G_B3_0 = ((((int32_t)((((Object_t *)L_2) == ((Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6533/* method */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo BaseInvokableCall_t1127_BaseInvokableCall__ctor_m6534_ParameterInfos[] = 
{
	{"target", 0, 134219385, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"function", 1, 134219386, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6534/* method */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1127_BaseInvokableCall__ctor_m6534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo BaseInvokableCall_t1127_BaseInvokableCall_Invoke_m6714_ParameterInfos[] = 
{
	{"args", 0, 134219387, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
MethodInfo BaseInvokableCall_Invoke_m6714_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, BaseInvokableCall_t1127_BaseInvokableCall_Invoke_m6714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo BaseInvokableCall_t1127_BaseInvokableCall_ThrowOnInvalidArg_m6715_ParameterInfos[] = 
{
	{"arg", 0, 134219388, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m6715_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m6715_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull BaseInvokableCall_ThrowOnInvalidArg_m6715_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &BaseInvokableCall_ThrowOnInvalidArg_m6715_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* BaseInvokableCall_ThrowOnInvalidArg_m6715_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m6715_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m6715_MethodInfo;
Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m6715_Il2CppGenericContainer = { { NULL, NULL }, NULL, &BaseInvokableCall_ThrowOnInvalidArg_m6715_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m6715_Il2CppGenericParametersArray };
extern Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m6715_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m6715_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &BaseInvokableCall_ThrowOnInvalidArg_m6715_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_TYPE, &BaseInvokableCall_ThrowOnInvalidArg_m6715_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m6715_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t1127_BaseInvokableCall_ThrowOnInvalidArg_m6715_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1578/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m6715_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m6715_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType Delegate_t152_0_0_0;
extern Il2CppType Delegate_t152_0_0_0;
static ParameterInfo BaseInvokableCall_t1127_BaseInvokableCall_AllowInvoke_m6535_ParameterInfos[] = 
{
	{"delegate", 0, 134219389, &EmptyCustomAttributesCache, &Delegate_t152_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m6535/* method */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, BaseInvokableCall_t1127_BaseInvokableCall_AllowInvoke_m6535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo BaseInvokableCall_t1127_BaseInvokableCall_Find_m6716_ParameterInfos[] = 
{
	{"targetObj", 0, 134219390, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219391, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo BaseInvokableCall_Find_m6716_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1127_BaseInvokableCall_Find_m6716_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BaseInvokableCall_t1127_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m6533_MethodInfo,
	&BaseInvokableCall__ctor_m6534_MethodInfo,
	&BaseInvokableCall_Invoke_m6714_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m6715_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m6535_MethodInfo,
	&BaseInvokableCall_Find_m6716_MethodInfo,
	NULL
};
static MethodInfo* BaseInvokableCall_t1127_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	NULL,
	NULL,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType BaseInvokableCall_t1127_1_0_0;
struct BaseInvokableCall_t1127;
TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t1127_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, BaseInvokableCall_t1127_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* cast_class */
	, &BaseInvokableCall_t1127_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1127_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1127)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InvokableCall_t1128_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"

// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
extern TypeInfo UnityAction_t341_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern Il2CppType UnityAction_t341_0_0_0;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo UnityAction_Invoke_m2300_MethodInfo;


// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern MethodInfo InvokableCall__ctor_m6536_MethodInfo;
 void InvokableCall__ctor_m6536 (InvokableCall_t1128 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method){
	{
		BaseInvokableCall__ctor_m6534(__this, ___target, ___theFunction, /*hidden argument*/&BaseInvokableCall__ctor_m6534_MethodInfo);
		UnityAction_t341 * L_0 = (__this->___Delegate_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&UnityAction_t341_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Delegate_t152 * L_2 = Delegate_CreateDelegate_m330(NULL /*static, unused*/, L_1, ___target, ___theFunction, /*hidden argument*/&Delegate_CreateDelegate_m330_MethodInfo);
		Delegate_t152 * L_3 = Delegate_Combine_m2327(NULL /*static, unused*/, L_0, ((UnityAction_t341 *)IsInst(L_2, InitializedTypeInfo(&UnityAction_t341_il2cpp_TypeInfo))), /*hidden argument*/&Delegate_Combine_m2327_MethodInfo);
		__this->___Delegate_0 = ((UnityAction_t341 *)Castclass(L_3, InitializedTypeInfo(&UnityAction_t341_il2cpp_TypeInfo)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern MethodInfo InvokableCall_Invoke_m6537_MethodInfo;
 void InvokableCall_Invoke_m6537 (InvokableCall_t1128 * __this, ObjectU5BU5D_t130* ___args, MethodInfo* method){
	{
		UnityAction_t341 * L_0 = (__this->___Delegate_0);
		bool L_1 = BaseInvokableCall_AllowInvoke_m6535(NULL /*static, unused*/, L_0, /*hidden argument*/&BaseInvokableCall_AllowInvoke_m6535_MethodInfo);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UnityAction_t341 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(&UnityAction_Invoke_m2300_MethodInfo, L_2);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern MethodInfo InvokableCall_Find_m6538_MethodInfo;
 bool InvokableCall_Find_m6538 (InvokableCall_t1128 * __this, Object_t * ___targetObj, MethodInfo_t141 * ___method, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		UnityAction_t341 * L_0 = (__this->___Delegate_0);
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m6713(L_0, /*hidden argument*/&Delegate_get_Target_m6713_MethodInfo);
		if ((((Object_t *)L_1) != ((Object_t *)___targetObj)))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_t341 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		MethodInfo_t141 * L_3 = Delegate_get_Method_m6711(L_2, /*hidden argument*/&Delegate_get_Method_m6711_MethodInfo);
		G_B3_0 = ((((MethodInfo_t141 *)L_3) == ((MethodInfo_t141 *)___method))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall
extern Il2CppType UnityAction_t341_0_0_1;
FieldInfo InvokableCall_t1128____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_t341_0_0_1/* type */
	, &InvokableCall_t1128_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_t1128, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_t1128_FieldInfos[] =
{
	&InvokableCall_t1128____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_t1128_InvokableCall__ctor_m6536_ParameterInfos[] = 
{
	{"target", 0, 134219392, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219393, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall__ctor_m6536_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m6536/* method */
	, &InvokableCall_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1128_InvokableCall__ctor_m6536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_t1128_InvokableCall_Invoke_m6537_ParameterInfos[] = 
{
	{"args", 0, 134219394, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
MethodInfo InvokableCall_Invoke_m6537_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m6537/* method */
	, &InvokableCall_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_t1128_InvokableCall_Invoke_m6537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_t1128_InvokableCall_Find_m6538_ParameterInfos[] = 
{
	{"targetObj", 0, 134219395, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219396, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_Find_m6538_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m6538/* method */
	, &InvokableCall_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1128_InvokableCall_Find_m6538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_t1128_MethodInfos[] =
{
	&InvokableCall__ctor_m6536_MethodInfo,
	&InvokableCall_Invoke_m6537_MethodInfo,
	&InvokableCall_Find_m6538_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_t1128_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_Invoke_m6537_MethodInfo,
	&InvokableCall_Find_m6538_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_t1128_0_0_0;
extern Il2CppType InvokableCall_t1128_1_0_0;
struct InvokableCall_t1128;
TypeInfo InvokableCall_t1128_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t1128_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_t1128_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_t1128_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_t1128_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_t1128_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_t1128_0_0_0/* byval_arg */
	, &InvokableCall_t1128_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1128)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`1
#include "UnityEngine_UnityEngine_Events_InvokableCall_1.h"
extern Il2CppGenericContainer InvokableCall_1_t1129_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t1129_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_1_t1129_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_1_t1129_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_1_t1129_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t1129_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_1_t1129_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_1_t1129_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_1_t1129_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t1129_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t1129_InvokableCall_1__ctor_m6717_ParameterInfos[] = 
{
	{"target", 0, 134219397, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219398, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m6717_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1129_InvokableCall_1__ctor_m6717_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1227_0_0_0;
extern Il2CppType UnityAction_1_t1227_0_0_0;
static ParameterInfo InvokableCall_1_t1129_InvokableCall_1__ctor_m6718_ParameterInfos[] = 
{
	{"callback", 0, 134219399, &EmptyCustomAttributesCache, &UnityAction_1_t1227_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m6718_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1129_InvokableCall_1__ctor_m6718_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t1129_InvokableCall_1_Invoke_m6719_ParameterInfos[] = 
{
	{"args", 0, 134219400, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m6719_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1129_InvokableCall_1_Invoke_m6719_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t1129_InvokableCall_1_Find_m6720_ParameterInfos[] = 
{
	{"targetObj", 0, 134219401, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219402, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m6720_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1129_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1129_InvokableCall_1_Find_m6720_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_1_t1129_MethodInfos[] =
{
	&InvokableCall_1__ctor_m6717_MethodInfo,
	&InvokableCall_1__ctor_m6718_MethodInfo,
	&InvokableCall_1_Invoke_m6719_MethodInfo,
	&InvokableCall_1_Find_m6720_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_1_t1227_0_0_1;
FieldInfo InvokableCall_1_t1129____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t1227_0_0_1/* type */
	, &InvokableCall_1_t1129_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t1129_FieldInfos[] =
{
	&InvokableCall_1_t1129____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t1129_0_0_0;
extern Il2CppType InvokableCall_1_t1129_1_0_0;
struct InvokableCall_1_t1129;
TypeInfo InvokableCall_1_t1129_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t1129_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t1129_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t1129_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_1_t1129_0_0_0/* byval_arg */
	, &InvokableCall_1_t1129_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_1_t1129_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`2
#include "UnityEngine_UnityEngine_Events_InvokableCall_2.h"
extern Il2CppGenericContainer InvokableCall_2_t1130_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t1130_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_2_t1130_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_2_t1130_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_2_t1130_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_2_t1130_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_2_t1130_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_2_t1130_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t1130_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t1130_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_2_t1130_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_2_t1130_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_2_t1130_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t1130_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_2_t1130_InvokableCall_2__ctor_m6721_ParameterInfos[] = 
{
	{"target", 0, 134219403, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219404, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_2__ctor_m6721_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1130_InvokableCall_2__ctor_m6721_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_2_t1130_InvokableCall_2_Invoke_m6722_ParameterInfos[] = 
{
	{"args", 0, 134219405, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
MethodInfo InvokableCall_2_Invoke_m6722_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1130_InvokableCall_2_Invoke_m6722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_2_t1130_InvokableCall_2_Find_m6723_ParameterInfos[] = 
{
	{"targetObj", 0, 134219406, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219407, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_2_Find_m6723_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1130_InvokableCall_2_Find_m6723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_2_t1130_MethodInfos[] =
{
	&InvokableCall_2__ctor_m6721_MethodInfo,
	&InvokableCall_2_Invoke_m6722_MethodInfo,
	&InvokableCall_2_Find_m6723_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_2_t1230_0_0_1;
FieldInfo InvokableCall_2_t1130____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_2_t1230_0_0_1/* type */
	, &InvokableCall_2_t1130_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_2_t1130_FieldInfos[] =
{
	&InvokableCall_2_t1130____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_2_t1130_0_0_0;
extern Il2CppType InvokableCall_2_t1130_1_0_0;
struct InvokableCall_2_t1130;
TypeInfo InvokableCall_2_t1130_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t1130_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_2_t1130_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_2_t1130_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_2_t1130_0_0_0/* byval_arg */
	, &InvokableCall_2_t1130_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_2_t1130_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`3
#include "UnityEngine_UnityEngine_Events_InvokableCall_3.h"
extern Il2CppGenericContainer InvokableCall_3_t1131_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t1131_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1131_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1131_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_3_t1131_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1131_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1131_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo InvokableCall_3_t1131_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1131_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1131_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_3_t1131_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t1131_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1131_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1131_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_3_t1131_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_3_t1131_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_3_t1131_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t1131_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_3_t1131_InvokableCall_3__ctor_m6724_ParameterInfos[] = 
{
	{"target", 0, 134219408, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219409, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_3__ctor_m6724_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1131_InvokableCall_3__ctor_m6724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_3_t1131_InvokableCall_3_Invoke_m6725_ParameterInfos[] = 
{
	{"args", 0, 134219410, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
MethodInfo InvokableCall_3_Invoke_m6725_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1131_InvokableCall_3_Invoke_m6725_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_3_t1131_InvokableCall_3_Find_m6726_ParameterInfos[] = 
{
	{"targetObj", 0, 134219411, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219412, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_3_Find_m6726_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1131_InvokableCall_3_Find_m6726_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_3_t1131_MethodInfos[] =
{
	&InvokableCall_3__ctor_m6724_MethodInfo,
	&InvokableCall_3_Invoke_m6725_MethodInfo,
	&InvokableCall_3_Find_m6726_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_3_t1234_0_0_1;
FieldInfo InvokableCall_3_t1131____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_3_t1234_0_0_1/* type */
	, &InvokableCall_3_t1131_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_3_t1131_FieldInfos[] =
{
	&InvokableCall_3_t1131____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_3_t1131_0_0_0;
extern Il2CppType InvokableCall_3_t1131_1_0_0;
struct InvokableCall_3_t1131;
TypeInfo InvokableCall_3_t1131_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t1131_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_3_t1131_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_3_t1131_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_3_t1131_0_0_0/* byval_arg */
	, &InvokableCall_3_t1131_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_3_t1131_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`4
#include "UnityEngine_UnityEngine_Events_InvokableCall_4.h"
extern Il2CppGenericContainer InvokableCall_4_t1132_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t1132_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1132_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1132_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1132_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1132_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1132_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1132_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1132_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1132_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1132_gp_T4_3_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1132_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1132_Il2CppGenericContainer, 3}, {NULL, "T4", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_4_t1132_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t1132_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1132_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1132_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1132_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_4_t1132_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_4_t1132_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_4_t1132_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t1132_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_4_t1132_InvokableCall_4__ctor_m6727_ParameterInfos[] = 
{
	{"target", 0, 134219413, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219414, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_4__ctor_m6727_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1132_InvokableCall_4__ctor_m6727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_4_t1132_InvokableCall_4_Invoke_m6728_ParameterInfos[] = 
{
	{"args", 0, 134219415, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
MethodInfo InvokableCall_4_Invoke_m6728_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1132_InvokableCall_4_Invoke_m6728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_4_t1132_InvokableCall_4_Find_m6729_ParameterInfos[] = 
{
	{"targetObj", 0, 134219416, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219417, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_4_Find_m6729_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1132_InvokableCall_4_Find_m6729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_4_t1132_MethodInfos[] =
{
	&InvokableCall_4__ctor_m6727_MethodInfo,
	&InvokableCall_4_Invoke_m6728_MethodInfo,
	&InvokableCall_4_Find_m6729_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_4_t1239_0_0_1;
FieldInfo InvokableCall_4_t1132____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_4_t1239_0_0_1/* type */
	, &InvokableCall_4_t1132_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_4_t1132_FieldInfos[] =
{
	&InvokableCall_4_t1132____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_4_t1132_0_0_0;
extern Il2CppType InvokableCall_4_t1132_1_0_0;
struct InvokableCall_4_t1132;
TypeInfo InvokableCall_4_t1132_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t1132_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_4_t1132_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_4_t1132_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_4_t1132_0_0_0/* byval_arg */
	, &InvokableCall_4_t1132_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_4_t1132_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1.h"
extern Il2CppGenericContainer CachedInvokableCall_1_t1133_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t1133_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull CachedInvokableCall_1_t1133_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &CachedInvokableCall_1_t1133_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* CachedInvokableCall_1_t1133_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t1133_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo CachedInvokableCall_1_t1133_il2cpp_TypeInfo;
Il2CppGenericContainer CachedInvokableCall_1_t1133_Il2CppGenericContainer = { { NULL, NULL }, NULL, &CachedInvokableCall_1_t1133_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t1133_Il2CppGenericParametersArray };
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1133_gp_0_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1133_gp_0_0_0_0;
static ParameterInfo CachedInvokableCall_1_t1133_CachedInvokableCall_1__ctor_m6730_ParameterInfos[] = 
{
	{"target", 0, 134219418, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134219419, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134219420, &EmptyCustomAttributesCache, &CachedInvokableCall_1_t1133_gp_0_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m6730_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1133_CachedInvokableCall_1__ctor_m6730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t1133_CachedInvokableCall_1_Invoke_m6731_ParameterInfos[] = 
{
	{"args", 0, 134219421, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m6731_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1133_CachedInvokableCall_1_Invoke_m6731_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CachedInvokableCall_1_t1133_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m6730_MethodInfo,
	&CachedInvokableCall_1_Invoke_m6731_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t1133____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t1133_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t1133_FieldInfos[] =
{
	&CachedInvokableCall_1_t1133____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t1133_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1133_1_0_0;
struct CachedInvokableCall_1_t1133;
TypeInfo CachedInvokableCall_1_t1133_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t1133_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t1133_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t1133_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &CachedInvokableCall_1_t1133_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1133_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t1133_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEventCallState_t1134_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"



// Metadata Definition UnityEngine.Events.UnityEventCallState
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo UnityEventCallState_t1134____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &UnityEventCallState_t1134_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventCallState_t1134, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1134_0_0_32854;
FieldInfo UnityEventCallState_t1134____Off_2_FieldInfo = 
{
	"Off"/* name */
	, &UnityEventCallState_t1134_0_0_32854/* type */
	, &UnityEventCallState_t1134_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1134_0_0_32854;
FieldInfo UnityEventCallState_t1134____EditorAndRuntime_3_FieldInfo = 
{
	"EditorAndRuntime"/* name */
	, &UnityEventCallState_t1134_0_0_32854/* type */
	, &UnityEventCallState_t1134_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1134_0_0_32854;
FieldInfo UnityEventCallState_t1134____RuntimeOnly_4_FieldInfo = 
{
	"RuntimeOnly"/* name */
	, &UnityEventCallState_t1134_0_0_32854/* type */
	, &UnityEventCallState_t1134_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEventCallState_t1134_FieldInfos[] =
{
	&UnityEventCallState_t1134____value___1_FieldInfo,
	&UnityEventCallState_t1134____Off_2_FieldInfo,
	&UnityEventCallState_t1134____EditorAndRuntime_3_FieldInfo,
	&UnityEventCallState_t1134____RuntimeOnly_4_FieldInfo,
	NULL
};
static const int32_t UnityEventCallState_t1134____Off_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1134____Off_2_DefaultValue = 
{
	&UnityEventCallState_t1134____Off_2_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1134____Off_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UnityEventCallState_t1134____EditorAndRuntime_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1134____EditorAndRuntime_3_DefaultValue = 
{
	&UnityEventCallState_t1134____EditorAndRuntime_3_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1134____EditorAndRuntime_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UnityEventCallState_t1134____RuntimeOnly_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1134____RuntimeOnly_4_DefaultValue = 
{
	&UnityEventCallState_t1134____RuntimeOnly_4_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1134____RuntimeOnly_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UnityEventCallState_t1134_FieldDefaultValues[] = 
{
	&UnityEventCallState_t1134____Off_2_DefaultValue,
	&UnityEventCallState_t1134____EditorAndRuntime_3_DefaultValue,
	&UnityEventCallState_t1134____RuntimeOnly_4_DefaultValue,
	NULL
};
static MethodInfo* UnityEventCallState_t1134_MethodInfos[] =
{
	NULL
};
static MethodInfo* UnityEventCallState_t1134_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t1134_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEventCallState_t1134_0_0_0;
extern Il2CppType UnityEventCallState_t1134_1_0_0;
TypeInfo UnityEventCallState_t1134_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t1134_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEventCallState_t1134_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEventCallState_t1134_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &UnityEventCallState_t1134_0_0_0/* byval_arg */
	, &UnityEventCallState_t1134_1_0_0/* this_arg */
	, UnityEventCallState_t1134_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UnityEventCallState_t1134_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1134)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PersistentCall_t1135_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"

// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0.h"
// UnityEngine.Events.CachedInvokableCall`1<System.String>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2.h"
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
extern TypeInfo UnityEventBase_t1136_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1241_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1242_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1243_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1244_il2cpp_TypeInfo;
extern TypeInfo Object_t120_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t922_il2cpp_TypeInfo;
extern TypeInfo ConstructorInfo_t1245_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_genMethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0MethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.String>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1MethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2MethodDeclarations.h"
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
extern Il2CppType CachedInvokableCall_1_t1133_0_0_0;
extern MethodInfo PersistentCall_get_target_m6540_MethodInfo;
extern MethodInfo PersistentCall_get_methodName_m6541_MethodInfo;
extern MethodInfo String_IsNullOrEmpty_m2485_MethodInfo;
extern MethodInfo UnityEventBase_FindMethod_m6556_MethodInfo;
extern MethodInfo UnityEventBase_GetDelegate_m6732_MethodInfo;
extern MethodInfo PersistentCall_GetObjectCall_m6546_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6733_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6734_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6735_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6736_MethodInfo;
extern MethodInfo Type_GetType_m6737_MethodInfo;
extern MethodInfo Type_MakeGenericType_m6738_MethodInfo;
extern MethodInfo Type_GetConstructor_m6739_MethodInfo;
extern MethodInfo Object_GetType_m323_MethodInfo;
extern MethodInfo Type_IsAssignableFrom_m6740_MethodInfo;
extern MethodInfo ConstructorInfo_Invoke_m6741_MethodInfo;


// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern MethodInfo PersistentCall__ctor_m6539_MethodInfo;
 void PersistentCall__ctor_m6539 (PersistentCall_t1135 * __this, MethodInfo* method){
	{
		ArgumentCache_t1126 * L_0 = (ArgumentCache_t1126 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentCache_t1126_il2cpp_TypeInfo));
		ArgumentCache__ctor_m6526(L_0, /*hidden argument*/&ArgumentCache__ctor_m6526_MethodInfo);
		__this->___m_Arguments_3 = L_0;
		__this->___m_CallState_4 = 2;
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
 Object_t120 * PersistentCall_get_target_m6540 (PersistentCall_t1135 * __this, MethodInfo* method){
	{
		Object_t120 * L_0 = (__this->___m_Target_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
 String_t* PersistentCall_get_methodName_m6541 (PersistentCall_t1135 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_MethodName_1);
		return L_0;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern MethodInfo PersistentCall_get_mode_m6542_MethodInfo;
 int32_t PersistentCall_get_mode_m6542 (PersistentCall_t1135 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_Mode_2);
		return L_0;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern MethodInfo PersistentCall_get_arguments_m6543_MethodInfo;
 ArgumentCache_t1126 * PersistentCall_get_arguments_m6543 (PersistentCall_t1135 * __this, MethodInfo* method){
	{
		ArgumentCache_t1126 * L_0 = (__this->___m_Arguments_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern MethodInfo PersistentCall_IsValid_m6544_MethodInfo;
 bool PersistentCall_IsValid_m6544 (PersistentCall_t1135 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		Object_t120 * L_0 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		bool L_1 = Object_op_Inequality_m489(NULL /*static, unused*/, L_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m6541(__this, /*hidden argument*/&PersistentCall_get_methodName_m6541_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_3 = String_IsNullOrEmpty_m2485(NULL /*static, unused*/, L_2, /*hidden argument*/&String_IsNullOrEmpty_m2485_MethodInfo);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern MethodInfo PersistentCall_GetRuntimeCall_m6545_MethodInfo;
 BaseInvokableCall_t1127 * PersistentCall_GetRuntimeCall_m6545 (PersistentCall_t1135 * __this, UnityEventBase_t1136 * ___theEvent, MethodInfo* method){
	MethodInfo_t141 * V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = (__this->___m_CallState_4);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		if (___theEvent)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (BaseInvokableCall_t1127 *)NULL;
	}

IL_0013:
	{
		NullCheck(___theEvent);
		MethodInfo_t141 * L_1 = UnityEventBase_FindMethod_m6556(___theEvent, __this, /*hidden argument*/&UnityEventBase_FindMethod_m6556_MethodInfo);
		V_0 = L_1;
		if (V_0)
		{
			goto IL_0023;
		}
	}
	{
		return (BaseInvokableCall_t1127 *)NULL;
	}

IL_0023:
	{
		int32_t L_2 = (__this->___m_Mode_2);
		V_1 = L_2;
		if (V_1 == 0)
		{
			goto IL_0051;
		}
		if (V_1 == 1)
		{
			goto IL_00d2;
		}
		if (V_1 == 2)
		{
			goto IL_005f;
		}
		if (V_1 == 3)
		{
			goto IL_008a;
		}
		if (V_1 == 4)
		{
			goto IL_0072;
		}
		if (V_1 == 5)
		{
			goto IL_00a2;
		}
		if (V_1 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		Object_t120 * L_3 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		NullCheck(___theEvent);
		BaseInvokableCall_t1127 * L_4 = (BaseInvokableCall_t1127 *)VirtFuncInvoker2< BaseInvokableCall_t1127 *, Object_t *, MethodInfo_t141 * >::Invoke(&UnityEventBase_GetDelegate_m6732_MethodInfo, ___theEvent, L_3, V_0);
		return L_4;
	}

IL_005f:
	{
		Object_t120 * L_5 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		ArgumentCache_t1126 * L_6 = (__this->___m_Arguments_3);
		BaseInvokableCall_t1127 * L_7 = PersistentCall_GetObjectCall_m6546(NULL /*static, unused*/, L_5, V_0, L_6, /*hidden argument*/&PersistentCall_GetObjectCall_m6546_MethodInfo);
		return L_7;
	}

IL_0072:
	{
		Object_t120 * L_8 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		ArgumentCache_t1126 * L_9 = (__this->___m_Arguments_3);
		NullCheck(L_9);
		float L_10 = ArgumentCache_get_floatArgument_m6530(L_9, /*hidden argument*/&ArgumentCache_get_floatArgument_m6530_MethodInfo);
		CachedInvokableCall_1_t1241 * L_11 = (CachedInvokableCall_1_t1241 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1241_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6733(L_11, L_8, V_0, L_10, /*hidden argument*/&CachedInvokableCall_1__ctor_m6733_MethodInfo);
		return L_11;
	}

IL_008a:
	{
		Object_t120 * L_12 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		ArgumentCache_t1126 * L_13 = (__this->___m_Arguments_3);
		NullCheck(L_13);
		int32_t L_14 = ArgumentCache_get_intArgument_m6529(L_13, /*hidden argument*/&ArgumentCache_get_intArgument_m6529_MethodInfo);
		CachedInvokableCall_1_t1242 * L_15 = (CachedInvokableCall_1_t1242 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1242_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6734(L_15, L_12, V_0, L_14, /*hidden argument*/&CachedInvokableCall_1__ctor_m6734_MethodInfo);
		return L_15;
	}

IL_00a2:
	{
		Object_t120 * L_16 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		ArgumentCache_t1126 * L_17 = (__this->___m_Arguments_3);
		NullCheck(L_17);
		String_t* L_18 = ArgumentCache_get_stringArgument_m6531(L_17, /*hidden argument*/&ArgumentCache_get_stringArgument_m6531_MethodInfo);
		CachedInvokableCall_1_t1243 * L_19 = (CachedInvokableCall_1_t1243 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1243_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6735(L_19, L_16, V_0, L_18, /*hidden argument*/&CachedInvokableCall_1__ctor_m6735_MethodInfo);
		return L_19;
	}

IL_00ba:
	{
		Object_t120 * L_20 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		ArgumentCache_t1126 * L_21 = (__this->___m_Arguments_3);
		NullCheck(L_21);
		bool L_22 = ArgumentCache_get_boolArgument_m6532(L_21, /*hidden argument*/&ArgumentCache_get_boolArgument_m6532_MethodInfo);
		CachedInvokableCall_1_t1244 * L_23 = (CachedInvokableCall_1_t1244 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1244_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6736(L_23, L_20, V_0, L_22, /*hidden argument*/&CachedInvokableCall_1__ctor_m6736_MethodInfo);
		return L_23;
	}

IL_00d2:
	{
		Object_t120 * L_24 = PersistentCall_get_target_m6540(__this, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		InvokableCall_t1128 * L_25 = (InvokableCall_t1128 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvokableCall_t1128_il2cpp_TypeInfo));
		InvokableCall__ctor_m6536(L_25, L_24, V_0, /*hidden argument*/&InvokableCall__ctor_m6536_MethodInfo);
		return L_25;
	}

IL_00df:
	{
		return (BaseInvokableCall_t1127 *)NULL;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
 BaseInvokableCall_t1127 * PersistentCall_GetObjectCall_m6546 (Object_t * __this/* static, unused */, Object_t120 * ___target, MethodInfo_t141 * ___method, ArgumentCache_t1126 * ___arguments, MethodInfo* method){
	Type_t * V_0 = {0};
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	ConstructorInfo_t1245 * V_3 = {0};
	Object_t120 * V_4 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t120_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		V_0 = L_0;
		NullCheck(___arguments);
		String_t* L_1 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528(___arguments, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_2 = String_IsNullOrEmpty_m2485(NULL /*static, unused*/, L_1, /*hidden argument*/&String_IsNullOrEmpty_m2485_MethodInfo);
		if (L_2)
		{
			goto IL_0039;
		}
	}
	{
		NullCheck(___arguments);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528(___arguments, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_4 = Type_GetType_m6737(NULL /*static, unused*/, L_3, 0, /*hidden argument*/&Type_GetType_m6737_MethodInfo);
		Type_t * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t120_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		G_B3_0 = L_6;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&CachedInvokableCall_1_t1133_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		V_1 = L_7;
		TypeU5BU5D_t922* L_8 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 1));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, V_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_8, 0)) = (Type_t *)V_0;
		NullCheck(V_1);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t922* >::Invoke(&Type_MakeGenericType_m6738_MethodInfo, V_1, L_8);
		V_2 = L_9;
		TypeU5BU5D_t922* L_10 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 3));
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t120_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_10, 0)) = (Type_t *)L_11;
		TypeU5BU5D_t922* L_12 = L_10;
		Type_t * L_13 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MethodInfo_t141_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_13);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_12, 1)) = (Type_t *)L_13;
		TypeU5BU5D_t922* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, V_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_14, 2)) = (Type_t *)V_0;
		NullCheck(V_2);
		ConstructorInfo_t1245 * L_15 = (ConstructorInfo_t1245 *)VirtFuncInvoker1< ConstructorInfo_t1245 *, TypeU5BU5D_t922* >::Invoke(&Type_GetConstructor_m6739_MethodInfo, V_2, L_14);
		V_3 = L_15;
		NullCheck(___arguments);
		Object_t120 * L_16 = ArgumentCache_get_unityObjectArgument_m6527(___arguments, /*hidden argument*/&ArgumentCache_get_unityObjectArgument_m6527_MethodInfo);
		V_4 = L_16;
		bool L_17 = Object_op_Inequality_m489(NULL /*static, unused*/, V_4, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_17)
		{
			goto IL_00aa;
		}
	}
	{
		NullCheck(V_4);
		Type_t * L_18 = Object_GetType_m323(V_4, /*hidden argument*/&Object_GetType_m323_MethodInfo);
		NullCheck(V_0);
		bool L_19 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(&Type_IsAssignableFrom_m6740_MethodInfo, V_0, L_18);
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (Object_t120 *)NULL;
	}

IL_00aa:
	{
		ObjectU5BU5D_t130* L_20 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 3));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, ___target);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 0)) = (Object_t *)___target;
		ObjectU5BU5D_t130* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, ___method);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 1)) = (Object_t *)___method;
		ObjectU5BU5D_t130* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 2);
		ArrayElementTypeCheck (L_22, V_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 2)) = (Object_t *)V_4;
		NullCheck(V_3);
		Object_t * L_23 = ConstructorInfo_Invoke_m6741(V_3, L_22, /*hidden argument*/&ConstructorInfo_Invoke_m6741_MethodInfo);
		return ((BaseInvokableCall_t1127 *)IsInst(L_23, InitializedTypeInfo(&BaseInvokableCall_t1127_il2cpp_TypeInfo)));
	}
}
// Metadata Definition UnityEngine.Events.PersistentCall
extern Il2CppType Object_t120_0_0_1;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Target;
FieldInfo PersistentCall_t1135____m_Target_0_FieldInfo = 
{
	"m_Target"/* name */
	, &Object_t120_0_0_1/* type */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1135, ___m_Target_0)/* data */
	, &PersistentCall_t1135__CustomAttributeCache_m_Target/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_MethodName;
FieldInfo PersistentCall_t1135____m_MethodName_1_FieldInfo = 
{
	"m_MethodName"/* name */
	, &String_t_0_0_1/* type */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1135, ___m_MethodName_1)/* data */
	, &PersistentCall_t1135__CustomAttributeCache_m_MethodName/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_1;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Mode;
FieldInfo PersistentCall_t1135____m_Mode_2_FieldInfo = 
{
	"m_Mode"/* name */
	, &PersistentListenerMode_t1125_0_0_1/* type */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1135, ___m_Mode_2)/* data */
	, &PersistentCall_t1135__CustomAttributeCache_m_Mode/* custom_attributes_cache */

};
extern Il2CppType ArgumentCache_t1126_0_0_1;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Arguments;
FieldInfo PersistentCall_t1135____m_Arguments_3_FieldInfo = 
{
	"m_Arguments"/* name */
	, &ArgumentCache_t1126_0_0_1/* type */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1135, ___m_Arguments_3)/* data */
	, &PersistentCall_t1135__CustomAttributeCache_m_Arguments/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1134_0_0_1;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_CallState;
FieldInfo PersistentCall_t1135____m_CallState_4_FieldInfo = 
{
	"m_CallState"/* name */
	, &UnityEventCallState_t1134_0_0_1/* type */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1135, ___m_CallState_4)/* data */
	, &PersistentCall_t1135__CustomAttributeCache_m_CallState/* custom_attributes_cache */

};
static FieldInfo* PersistentCall_t1135_FieldInfos[] =
{
	&PersistentCall_t1135____m_Target_0_FieldInfo,
	&PersistentCall_t1135____m_MethodName_1_FieldInfo,
	&PersistentCall_t1135____m_Mode_2_FieldInfo,
	&PersistentCall_t1135____m_Arguments_3_FieldInfo,
	&PersistentCall_t1135____m_CallState_4_FieldInfo,
	NULL
};
static PropertyInfo PersistentCall_t1135____target_PropertyInfo = 
{
	&PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m6540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo PersistentCall_t1135____methodName_PropertyInfo = 
{
	&PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m6541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo PersistentCall_t1135____mode_PropertyInfo = 
{
	&PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m6542_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo PersistentCall_t1135____arguments_PropertyInfo = 
{
	&PersistentCall_t1135_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m6543_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* PersistentCall_t1135_PropertyInfos[] =
{
	&PersistentCall_t1135____target_PropertyInfo,
	&PersistentCall_t1135____methodName_PropertyInfo,
	&PersistentCall_t1135____mode_PropertyInfo,
	&PersistentCall_t1135____arguments_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
MethodInfo PersistentCall__ctor_m6539_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m6539/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
MethodInfo PersistentCall_get_target_m6540_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m6540/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &Object_t120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
MethodInfo PersistentCall_get_methodName_m6541_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m6541/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PersistentListenerMode_t1125_0_0_0;
extern void* RuntimeInvoker_PersistentListenerMode_t1125 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
MethodInfo PersistentCall_get_mode_m6542_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m6542/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t1125_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t1125/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ArgumentCache_t1126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
MethodInfo PersistentCall_get_arguments_m6543_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m6543/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t1126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
MethodInfo PersistentCall_IsValid_m6544_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m6544/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityEventBase_t1136_0_0_0;
extern Il2CppType UnityEventBase_t1136_0_0_0;
static ParameterInfo PersistentCall_t1135_PersistentCall_GetRuntimeCall_m6545_ParameterInfos[] = 
{
	{"theEvent", 0, 134219422, &EmptyCustomAttributesCache, &UnityEventBase_t1136_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
MethodInfo PersistentCall_GetRuntimeCall_m6545_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m6545/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1135_PersistentCall_GetRuntimeCall_m6545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ArgumentCache_t1126_0_0_0;
static ParameterInfo PersistentCall_t1135_PersistentCall_GetObjectCall_m6546_ParameterInfos[] = 
{
	{"target", 0, 134219423, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"method", 1, 134219424, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"arguments", 2, 134219425, &EmptyCustomAttributesCache, &ArgumentCache_t1126_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
MethodInfo PersistentCall_GetObjectCall_m6546_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m6546/* method */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1135_PersistentCall_GetObjectCall_m6546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PersistentCall_t1135_MethodInfos[] =
{
	&PersistentCall__ctor_m6539_MethodInfo,
	&PersistentCall_get_target_m6540_MethodInfo,
	&PersistentCall_get_methodName_m6541_MethodInfo,
	&PersistentCall_get_mode_m6542_MethodInfo,
	&PersistentCall_get_arguments_m6543_MethodInfo,
	&PersistentCall_IsValid_m6544_MethodInfo,
	&PersistentCall_GetRuntimeCall_m6545_MethodInfo,
	&PersistentCall_GetObjectCall_m6546_MethodInfo,
	NULL
};
static MethodInfo* PersistentCall_t1135_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
void PersistentCall_t1135_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("instance"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1135_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("methodName"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1135_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("mode"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1135_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("arguments"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1135_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("enabled"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Target = {
2,
NULL,
&PersistentCall_t1135_CustomAttributesCacheGenerator_m_Target
};
CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_MethodName = {
2,
NULL,
&PersistentCall_t1135_CustomAttributesCacheGenerator_m_MethodName
};
CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Mode = {
2,
NULL,
&PersistentCall_t1135_CustomAttributesCacheGenerator_m_Mode
};
CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Arguments = {
2,
NULL,
&PersistentCall_t1135_CustomAttributesCacheGenerator_m_Arguments
};
CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_CallState = {
3,
NULL,
&PersistentCall_t1135_CustomAttributesCacheGenerator_m_CallState
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentCall_t1135_0_0_0;
extern Il2CppType PersistentCall_t1135_1_0_0;
struct PersistentCall_t1135;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Target;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_MethodName;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Mode;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_Arguments;
extern CustomAttributesCache PersistentCall_t1135__CustomAttributeCache_m_CallState;
TypeInfo PersistentCall_t1135_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t1135_MethodInfos/* methods */
	, PersistentCall_t1135_PropertyInfos/* properties */
	, PersistentCall_t1135_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PersistentCall_t1135_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PersistentCall_t1135_il2cpp_TypeInfo/* cast_class */
	, &PersistentCall_t1135_0_0_0/* byval_arg */
	, &PersistentCall_t1135_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1135)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PersistentCallGroup_t1138_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_53.h"
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"
extern TypeInfo List_1_t1137_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1246_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_53MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern MethodInfo List_1__ctor_m6742_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m6743_MethodInfo;
extern MethodInfo Enumerator_get_Current_m6744_MethodInfo;
extern MethodInfo InvokableCallList_AddPersistentInvokableCall_m6550_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m6745_MethodInfo;
extern MethodInfo IDisposable_Dispose_m333_MethodInfo;


// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern MethodInfo PersistentCallGroup__ctor_m6547_MethodInfo;
 void PersistentCallGroup__ctor_m6547 (PersistentCallGroup_t1138 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t1137_il2cpp_TypeInfo));
		List_1_t1137 * L_0 = (List_1_t1137 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1137_il2cpp_TypeInfo));
		List_1__ctor_m6742(L_0, /*hidden argument*/&List_1__ctor_m6742_MethodInfo);
		__this->___m_Calls_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern MethodInfo PersistentCallGroup_Initialize_m6548_MethodInfo;
 void PersistentCallGroup_Initialize_m6548 (PersistentCallGroup_t1138 * __this, InvokableCallList_t1139 * ___invokableList, UnityEventBase_t1136 * ___unityEventBase, MethodInfo* method){
	PersistentCall_t1135 * V_0 = {0};
	Enumerator_t1246  V_1 = {0};
	BaseInvokableCall_t1127 * V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		List_1_t1137 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		Enumerator_t1246  L_1 = List_1_GetEnumerator_m6743(L_0, /*hidden argument*/&List_1_GetEnumerator_m6743_MethodInfo);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			PersistentCall_t1135 * L_2 = Enumerator_get_Current_m6744((&V_1), /*hidden argument*/&Enumerator_get_Current_m6744_MethodInfo);
			V_0 = L_2;
			NullCheck(V_0);
			bool L_3 = PersistentCall_IsValid_m6544(V_0, /*hidden argument*/&PersistentCall_IsValid_m6544_MethodInfo);
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			NullCheck(V_0);
			BaseInvokableCall_t1127 * L_4 = PersistentCall_GetRuntimeCall_m6545(V_0, ___unityEventBase, /*hidden argument*/&PersistentCall_GetRuntimeCall_m6545_MethodInfo);
			V_2 = L_4;
			if (!V_2)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			NullCheck(___invokableList);
			InvokableCallList_AddPersistentInvokableCall_m6550(___invokableList, V_2, /*hidden argument*/&InvokableCallList_AddPersistentInvokableCall_m6550_MethodInfo);
		}

IL_003e:
		{
			bool L_5 = Enumerator_MoveNext_m6745((&V_1), /*hidden argument*/&Enumerator_MoveNext_m6745_MethodInfo);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			// IL_004a: leave IL_005b
			leaveInstructions[0] = 0x5B; // 1
			THROW_SENTINEL(IL_005b);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_004f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_004f;
	}

IL_004f:
	{ // begin finally (depth: 1)
		Enumerator_t1246  L_6 = V_1;
		Object_t * L_7 = Box(InitializedTypeInfo(&Enumerator_t1246_il2cpp_TypeInfo), &L_6);
		NullCheck(L_7);
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, L_7);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x5B:
				goto IL_005b;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_005b:
	{
		return;
	}
}
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern Il2CppType List_1_t1137_0_0_1;
extern CustomAttributesCache PersistentCallGroup_t1138__CustomAttributeCache_m_Calls;
FieldInfo PersistentCallGroup_t1138____m_Calls_0_FieldInfo = 
{
	"m_Calls"/* name */
	, &List_1_t1137_0_0_1/* type */
	, &PersistentCallGroup_t1138_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCallGroup_t1138, ___m_Calls_0)/* data */
	, &PersistentCallGroup_t1138__CustomAttributeCache_m_Calls/* custom_attributes_cache */

};
static FieldInfo* PersistentCallGroup_t1138_FieldInfos[] =
{
	&PersistentCallGroup_t1138____m_Calls_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
MethodInfo PersistentCallGroup__ctor_m6547_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m6547/* method */
	, &PersistentCallGroup_t1138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType InvokableCallList_t1139_0_0_0;
extern Il2CppType InvokableCallList_t1139_0_0_0;
extern Il2CppType UnityEventBase_t1136_0_0_0;
static ParameterInfo PersistentCallGroup_t1138_PersistentCallGroup_Initialize_m6548_ParameterInfos[] = 
{
	{"invokableList", 0, 134219426, &EmptyCustomAttributesCache, &InvokableCallList_t1139_0_0_0},
	{"unityEventBase", 1, 134219427, &EmptyCustomAttributesCache, &UnityEventBase_t1136_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
MethodInfo PersistentCallGroup_Initialize_m6548_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m6548/* method */
	, &PersistentCallGroup_t1138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t1138_PersistentCallGroup_Initialize_m6548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PersistentCallGroup_t1138_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m6547_MethodInfo,
	&PersistentCallGroup_Initialize_m6548_MethodInfo,
	NULL
};
static MethodInfo* PersistentCallGroup_t1138_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
void PersistentCallGroup_t1138_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PersistentCallGroup_t1138__CustomAttributeCache_m_Calls = {
2,
NULL,
&PersistentCallGroup_t1138_CustomAttributesCacheGenerator_m_Calls
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentCallGroup_t1138_0_0_0;
extern Il2CppType PersistentCallGroup_t1138_1_0_0;
struct PersistentCallGroup_t1138;
extern CustomAttributesCache PersistentCallGroup_t1138__CustomAttributeCache_m_Calls;
TypeInfo PersistentCallGroup_t1138_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t1138_MethodInfos/* methods */
	, NULL/* properties */
	, PersistentCallGroup_t1138_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PersistentCallGroup_t1138_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PersistentCallGroup_t1138_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PersistentCallGroup_t1138_il2cpp_TypeInfo/* cast_class */
	, &PersistentCallGroup_t1138_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1138_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1138)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InvokableCallList_t1139_il2cpp_TypeInfo;

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_54.h"
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Predicate_1_gen_3.h"
extern TypeInfo List_1_t1140_il2cpp_TypeInfo;
extern TypeInfo Predicate_1_t1247_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_54MethodDeclarations.h"
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
extern MethodInfo List_1__ctor_m6746_MethodInfo;
extern MethodInfo List_1_Add_m6747_MethodInfo;
extern MethodInfo List_1_get_Item_m6748_MethodInfo;
extern MethodInfo BaseInvokableCall_Find_m6716_MethodInfo;
extern MethodInfo List_1_get_Count_m6749_MethodInfo;
extern MethodInfo List_1_Contains_m6750_MethodInfo;
extern MethodInfo Predicate_1__ctor_m6751_MethodInfo;
extern MethodInfo List_1_RemoveAll_m6752_MethodInfo;
extern MethodInfo List_1_Clear_m6753_MethodInfo;
extern MethodInfo List_1_AddRange_m6754_MethodInfo;
extern MethodInfo BaseInvokableCall_Invoke_m6714_MethodInfo;


// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern MethodInfo InvokableCallList__ctor_m6549_MethodInfo;
 void InvokableCallList__ctor_m6549 (InvokableCallList_t1139 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t1140_il2cpp_TypeInfo));
		List_1_t1140 * L_0 = (List_1_t1140 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1140_il2cpp_TypeInfo));
		List_1__ctor_m6746(L_0, /*hidden argument*/&List_1__ctor_m6746_MethodInfo);
		__this->___m_PersistentCalls_0 = L_0;
		List_1_t1140 * L_1 = (List_1_t1140 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1140_il2cpp_TypeInfo));
		List_1__ctor_m6746(L_1, /*hidden argument*/&List_1__ctor_m6746_MethodInfo);
		__this->___m_RuntimeCalls_1 = L_1;
		List_1_t1140 * L_2 = (List_1_t1140 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1140_il2cpp_TypeInfo));
		List_1__ctor_m6746(L_2, /*hidden argument*/&List_1__ctor_m6746_MethodInfo);
		__this->___m_ExecutingCalls_2 = L_2;
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
 void InvokableCallList_AddPersistentInvokableCall_m6550 (InvokableCallList_t1139 * __this, BaseInvokableCall_t1127 * ___call, MethodInfo* method){
	{
		List_1_t1140 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t1127 * >::Invoke(&List_1_Add_m6747_MethodInfo, L_0, ___call);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern MethodInfo InvokableCallList_AddListener_m6551_MethodInfo;
 void InvokableCallList_AddListener_m6551 (InvokableCallList_t1139 * __this, BaseInvokableCall_t1127 * ___call, MethodInfo* method){
	{
		List_1_t1140 * L_0 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t1127 * >::Invoke(&List_1_Add_m6747_MethodInfo, L_0, ___call);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern MethodInfo InvokableCallList_RemoveListener_m6552_MethodInfo;
 void InvokableCallList_RemoveListener_m6552 (InvokableCallList_t1139 * __this, Object_t * ___targetObj, MethodInfo_t141 * ___method, MethodInfo* method){
	List_1_t1140 * V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t1140_il2cpp_TypeInfo));
		List_1_t1140 * L_0 = (List_1_t1140 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1140_il2cpp_TypeInfo));
		List_1__ctor_m6746(L_0, /*hidden argument*/&List_1__ctor_m6746_MethodInfo);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		List_1_t1140 * L_1 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_1);
		BaseInvokableCall_t1127 * L_2 = (BaseInvokableCall_t1127 *)VirtFuncInvoker1< BaseInvokableCall_t1127 *, int32_t >::Invoke(&List_1_get_Item_m6748_MethodInfo, L_1, V_1);
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker2< bool, Object_t *, MethodInfo_t141 * >::Invoke(&BaseInvokableCall_Find_m6716_MethodInfo, L_2, ___targetObj, ___method);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t1140 * L_4 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_4);
		BaseInvokableCall_t1127 * L_5 = (BaseInvokableCall_t1127 *)VirtFuncInvoker1< BaseInvokableCall_t1127 *, int32_t >::Invoke(&List_1_get_Item_m6748_MethodInfo, L_4, V_1);
		NullCheck(V_0);
		VirtActionInvoker1< BaseInvokableCall_t1127 * >::Invoke(&List_1_Add_m6747_MethodInfo, V_0, L_5);
	}

IL_0037:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_003b:
	{
		List_1_t1140 * L_6 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&List_1_get_Count_m6749_MethodInfo, L_6);
		if ((((int32_t)V_1) < ((int32_t)L_7)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1140 * L_8 = (__this->___m_RuntimeCalls_1);
		List_1_t1140 * L_9 = V_0;
		IntPtr_t121 L_10 = { GetVirtualMethodInfo(L_9, &List_1_Contains_m6750_MethodInfo) };
		Predicate_1_t1247 * L_11 = (Predicate_1_t1247 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Predicate_1_t1247_il2cpp_TypeInfo));
		Predicate_1__ctor_m6751(L_11, L_9, L_10, /*hidden argument*/&Predicate_1__ctor_m6751_MethodInfo);
		NullCheck(L_8);
		List_1_RemoveAll_m6752(L_8, L_11, /*hidden argument*/&List_1_RemoveAll_m6752_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern MethodInfo InvokableCallList_ClearPersistent_m6553_MethodInfo;
 void InvokableCallList_ClearPersistent_m6553 (InvokableCallList_t1139 * __this, MethodInfo* method){
	{
		List_1_t1140 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(&List_1_Clear_m6753_MethodInfo, L_0);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern MethodInfo InvokableCallList_Invoke_m6554_MethodInfo;
 void InvokableCallList_Invoke_m6554 (InvokableCallList_t1139 * __this, ObjectU5BU5D_t130* ___parameters, MethodInfo* method){
	int32_t V_0 = 0;
	{
		List_1_t1140 * L_0 = (__this->___m_ExecutingCalls_2);
		List_1_t1140 * L_1 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		List_1_AddRange_m6754(L_0, L_1, /*hidden argument*/&List_1_AddRange_m6754_MethodInfo);
		List_1_t1140 * L_2 = (__this->___m_ExecutingCalls_2);
		List_1_t1140 * L_3 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_2);
		List_1_AddRange_m6754(L_2, L_3, /*hidden argument*/&List_1_AddRange_m6754_MethodInfo);
		V_0 = 0;
		goto IL_003f;
	}

IL_0029:
	{
		List_1_t1140 * L_4 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_4);
		BaseInvokableCall_t1127 * L_5 = (BaseInvokableCall_t1127 *)VirtFuncInvoker1< BaseInvokableCall_t1127 *, int32_t >::Invoke(&List_1_get_Item_m6748_MethodInfo, L_4, V_0);
		NullCheck(L_5);
		VirtActionInvoker1< ObjectU5BU5D_t130* >::Invoke(&BaseInvokableCall_Invoke_m6714_MethodInfo, L_5, ___parameters);
		V_0 = ((int32_t)(V_0+1));
	}

IL_003f:
	{
		List_1_t1140 * L_6 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&List_1_get_Count_m6749_MethodInfo, L_6);
		if ((((int32_t)V_0) < ((int32_t)L_7)))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1140 * L_8 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(&List_1_Clear_m6753_MethodInfo, L_8);
		return;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCallList
extern Il2CppType List_1_t1140_0_0_33;
FieldInfo InvokableCallList_t1139____m_PersistentCalls_0_FieldInfo = 
{
	"m_PersistentCalls"/* name */
	, &List_1_t1140_0_0_33/* type */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1139, ___m_PersistentCalls_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t1140_0_0_33;
FieldInfo InvokableCallList_t1139____m_RuntimeCalls_1_FieldInfo = 
{
	"m_RuntimeCalls"/* name */
	, &List_1_t1140_0_0_33/* type */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1139, ___m_RuntimeCalls_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t1140_0_0_33;
FieldInfo InvokableCallList_t1139____m_ExecutingCalls_2_FieldInfo = 
{
	"m_ExecutingCalls"/* name */
	, &List_1_t1140_0_0_33/* type */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1139, ___m_ExecutingCalls_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCallList_t1139_FieldInfos[] =
{
	&InvokableCallList_t1139____m_PersistentCalls_0_FieldInfo,
	&InvokableCallList_t1139____m_RuntimeCalls_1_FieldInfo,
	&InvokableCallList_t1139____m_ExecutingCalls_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
MethodInfo InvokableCallList__ctor_m6549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m6549/* method */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo InvokableCallList_t1139_InvokableCallList_AddPersistentInvokableCall_m6550_ParameterInfos[] = 
{
	{"call", 0, 134219428, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
MethodInfo InvokableCallList_AddPersistentInvokableCall_m6550_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m6550/* method */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCallList_t1139_InvokableCallList_AddPersistentInvokableCall_m6550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo InvokableCallList_t1139_InvokableCallList_AddListener_m6551_ParameterInfos[] = 
{
	{"call", 0, 134219429, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
MethodInfo InvokableCallList_AddListener_m6551_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m6551/* method */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCallList_t1139_InvokableCallList_AddListener_m6551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCallList_t1139_InvokableCallList_RemoveListener_m6552_ParameterInfos[] = 
{
	{"targetObj", 0, 134219430, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219431, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCallList_RemoveListener_m6552_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m6552/* method */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t1139_InvokableCallList_RemoveListener_m6552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
MethodInfo InvokableCallList_ClearPersistent_m6553_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m6553/* method */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCallList_t1139_InvokableCallList_Invoke_m6554_ParameterInfos[] = 
{
	{"parameters", 0, 134219432, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
MethodInfo InvokableCallList_Invoke_m6554_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m6554/* method */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCallList_t1139_InvokableCallList_Invoke_m6554_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCallList_t1139_MethodInfos[] =
{
	&InvokableCallList__ctor_m6549_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m6550_MethodInfo,
	&InvokableCallList_AddListener_m6551_MethodInfo,
	&InvokableCallList_RemoveListener_m6552_MethodInfo,
	&InvokableCallList_ClearPersistent_m6553_MethodInfo,
	&InvokableCallList_Invoke_m6554_MethodInfo,
	NULL
};
static MethodInfo* InvokableCallList_t1139_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCallList_t1139_1_0_0;
struct InvokableCallList_t1139;
TypeInfo InvokableCallList_t1139_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t1139_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCallList_t1139_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCallList_t1139_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCallList_t1139_il2cpp_TypeInfo/* cast_class */
	, &InvokableCallList_t1139_0_0_0/* byval_arg */
	, &InvokableCallList_t1139_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1139)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
extern TypeInfo Single_t170_il2cpp_TypeInfo;
extern TypeInfo BindingFlags_t142_il2cpp_TypeInfo;
extern TypeInfo Binder_t1215_il2cpp_TypeInfo;
extern TypeInfo ParameterModifierU5BU5D_t1216_il2cpp_TypeInfo;
extern TypeInfo ParameterModifier_t1217_il2cpp_TypeInfo;
extern MethodInfo Type_get_AssemblyQualifiedName_m6755_MethodInfo;
extern MethodInfo UnityEventBase_DirtyPersistentCalls_m6558_MethodInfo;
extern MethodInfo UnityEventBase_FindMethod_m6557_MethodInfo;
extern MethodInfo UnityEventBase_FindMethod_Impl_m6756_MethodInfo;
extern MethodInfo UnityEventBase_GetValidMethodInfo_m6563_MethodInfo;
extern MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m6559_MethodInfo;
extern MethodInfo Type_get_FullName_m6757_MethodInfo;
extern MethodInfo Type_GetMethod_m6758_MethodInfo;
extern MethodInfo Type_get_IsPrimitive_m6759_MethodInfo;
extern MethodInfo Type_get_BaseType_m6670_MethodInfo;


// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern MethodInfo UnityEventBase__ctor_m6555_MethodInfo;
 void UnityEventBase__ctor_m6555 (UnityEventBase_t1136 * __this, MethodInfo* method){
	{
		__this->___m_CallsDirty_3 = 1;
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		InvokableCallList_t1139 * L_0 = (InvokableCallList_t1139 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvokableCallList_t1139_il2cpp_TypeInfo));
		InvokableCallList__ctor_m6549(L_0, /*hidden argument*/&InvokableCallList__ctor_m6549_MethodInfo);
		__this->___m_Calls_0 = L_0;
		PersistentCallGroup_t1138 * L_1 = (PersistentCallGroup_t1138 *)il2cpp_codegen_object_new (InitializedTypeInfo(&PersistentCallGroup_t1138_il2cpp_TypeInfo));
		PersistentCallGroup__ctor_m6547(L_1, /*hidden argument*/&PersistentCallGroup__ctor_m6547_MethodInfo);
		__this->___m_PersistentCalls_1 = L_1;
		Type_t * L_2 = Object_GetType_m323(__this, /*hidden argument*/&Object_GetType_m323_MethodInfo);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_AssemblyQualifiedName_m6755_MethodInfo, L_2);
		__this->___m_TypeName_2 = L_3;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo;
 void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087 (UnityEventBase_t1136 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo;
 void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088 (UnityEventBase_t1136 * __this, MethodInfo* method){
	{
		UnityEventBase_DirtyPersistentCalls_m6558(__this, /*hidden argument*/&UnityEventBase_DirtyPersistentCalls_m6558_MethodInfo);
		Type_t * L_0 = Object_GetType_m323(__this, /*hidden argument*/&Object_GetType_m323_MethodInfo);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_AssemblyQualifiedName_m6755_MethodInfo, L_0);
		__this->___m_TypeName_2 = L_1;
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
 MethodInfo_t141 * UnityEventBase_FindMethod_m6556 (UnityEventBase_t1136 * __this, PersistentCall_t1135 * ___call, MethodInfo* method){
	Type_t * V_0 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t120_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		V_0 = L_0;
		NullCheck(___call);
		ArgumentCache_t1126 * L_1 = PersistentCall_get_arguments_m6543(___call, /*hidden argument*/&PersistentCall_get_arguments_m6543_MethodInfo);
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528(L_1, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_3 = String_IsNullOrEmpty_m2485(NULL /*static, unused*/, L_2, /*hidden argument*/&String_IsNullOrEmpty_m2485_MethodInfo);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		NullCheck(___call);
		ArgumentCache_t1126 * L_4 = PersistentCall_get_arguments_m6543(___call, /*hidden argument*/&PersistentCall_get_arguments_m6543_MethodInfo);
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528(L_4, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetType_m6737(NULL /*static, unused*/, L_5, 0, /*hidden argument*/&Type_GetType_m6737_MethodInfo);
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_8 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t120_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		G_B3_0 = L_8;
	}

IL_0042:
	{
		V_0 = G_B3_0;
	}

IL_0043:
	{
		NullCheck(___call);
		String_t* L_9 = PersistentCall_get_methodName_m6541(___call, /*hidden argument*/&PersistentCall_get_methodName_m6541_MethodInfo);
		NullCheck(___call);
		Object_t120 * L_10 = PersistentCall_get_target_m6540(___call, /*hidden argument*/&PersistentCall_get_target_m6540_MethodInfo);
		NullCheck(___call);
		int32_t L_11 = PersistentCall_get_mode_m6542(___call, /*hidden argument*/&PersistentCall_get_mode_m6542_MethodInfo);
		MethodInfo_t141 * L_12 = UnityEventBase_FindMethod_m6557(__this, L_9, L_10, L_11, V_0, /*hidden argument*/&UnityEventBase_FindMethod_m6557_MethodInfo);
		return L_12;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
 MethodInfo_t141 * UnityEventBase_FindMethod_m6557 (UnityEventBase_t1136 * __this, String_t* ___name, Object_t * ___listener, int32_t ___mode, Type_t * ___argumentType, MethodInfo* method){
	int32_t V_0 = {0};
	Type_t * G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t922* G_B10_2 = {0};
	TypeU5BU5D_t922* G_B10_3 = {0};
	String_t* G_B10_4 = {0};
	Object_t * G_B10_5 = {0};
	Type_t * G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t922* G_B9_2 = {0};
	TypeU5BU5D_t922* G_B9_3 = {0};
	String_t* G_B9_4 = {0};
	Object_t * G_B9_5 = {0};
	{
		V_0 = ___mode;
		if (V_0 == 0)
		{
			goto IL_0029;
		}
		if (V_0 == 1)
		{
			goto IL_0032;
		}
		if (V_0 == 2)
		{
			goto IL_00ac;
		}
		if (V_0 == 3)
		{
			goto IL_005b;
		}
		if (V_0 == 4)
		{
			goto IL_0040;
		}
		if (V_0 == 5)
		{
			goto IL_0091;
		}
		if (V_0 == 6)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_00d0;
	}

IL_0029:
	{
		MethodInfo_t141 * L_0 = (MethodInfo_t141 *)VirtFuncInvoker2< MethodInfo_t141 *, String_t*, Object_t * >::Invoke(&UnityEventBase_FindMethod_Impl_m6756_MethodInfo, __this, ___name, ___listener);
		return L_0;
	}

IL_0032:
	{
		MethodInfo_t141 * L_1 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___listener, ___name, ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 0)), /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_1;
	}

IL_0040:
	{
		TypeU5BU5D_t922* L_2 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Single_t170_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 0)) = (Type_t *)L_3;
		MethodInfo_t141 * L_4 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___listener, ___name, L_2, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_4;
	}

IL_005b:
	{
		TypeU5BU5D_t922* L_5 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Int32_t123_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_5, 0)) = (Type_t *)L_6;
		MethodInfo_t141 * L_7 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___listener, ___name, L_5, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_7;
	}

IL_0076:
	{
		TypeU5BU5D_t922* L_8 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Boolean_t122_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_8, 0)) = (Type_t *)L_9;
		MethodInfo_t141 * L_10 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___listener, ___name, L_8, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_10;
	}

IL_0091:
	{
		TypeU5BU5D_t922* L_11 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_12 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&String_t_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0)) = (Type_t *)L_12;
		MethodInfo_t141 * L_13 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___listener, ___name, L_11, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_13;
	}

IL_00ac:
	{
		TypeU5BU5D_t922* L_14 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 1));
		Type_t * L_15 = ___argumentType;
		G_B9_0 = L_15;
		G_B9_1 = 0;
		G_B9_2 = L_14;
		G_B9_3 = L_14;
		G_B9_4 = ___name;
		G_B9_5 = ___listener;
		if (L_15)
		{
			G_B10_0 = L_15;
			G_B10_1 = 0;
			G_B10_2 = L_14;
			G_B10_3 = L_14;
			G_B10_4 = ___name;
			G_B10_5 = ___listener;
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_16 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t120_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		G_B10_0 = L_16;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00c9:
	{
		NullCheck(G_B10_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B10_2, G_B10_1);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(G_B10_2, G_B10_1)) = (Type_t *)G_B10_0;
		MethodInfo_t141 * L_17 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_17;
	}

IL_00d0:
	{
		return (MethodInfo_t141 *)NULL;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
 void UnityEventBase_DirtyPersistentCalls_m6558 (UnityEventBase_t1136 * __this, MethodInfo* method){
	{
		InvokableCallList_t1139 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m6553(L_0, /*hidden argument*/&InvokableCallList_ClearPersistent_m6553_MethodInfo);
		__this->___m_CallsDirty_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
 void UnityEventBase_RebuildPersistentCallsIfNeeded_m6559 (UnityEventBase_t1136 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_CallsDirty_3);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		PersistentCallGroup_t1138 * L_1 = (__this->___m_PersistentCalls_1);
		InvokableCallList_t1139 * L_2 = (__this->___m_Calls_0);
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m6548(L_1, L_2, __this, /*hidden argument*/&PersistentCallGroup_Initialize_m6548_MethodInfo);
		__this->___m_CallsDirty_3 = 0;
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern MethodInfo UnityEventBase_AddCall_m6560_MethodInfo;
 void UnityEventBase_AddCall_m6560 (UnityEventBase_t1136 * __this, BaseInvokableCall_t1127 * ___call, MethodInfo* method){
	{
		InvokableCallList_t1139 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_AddListener_m6551(L_0, ___call, /*hidden argument*/&InvokableCallList_AddListener_m6551_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEventBase_RemoveListener_m6561_MethodInfo;
 void UnityEventBase_RemoveListener_m6561 (UnityEventBase_t1136 * __this, Object_t * ___targetObj, MethodInfo_t141 * ___method, MethodInfo* method){
	{
		InvokableCallList_t1139 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m6552(L_0, ___targetObj, ___method, /*hidden argument*/&InvokableCallList_RemoveListener_m6552_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern MethodInfo UnityEventBase_Invoke_m6562_MethodInfo;
 void UnityEventBase_Invoke_m6562 (UnityEventBase_t1136 * __this, ObjectU5BU5D_t130* ___parameters, MethodInfo* method){
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m6559(__this, /*hidden argument*/&UnityEventBase_RebuildPersistentCallsIfNeeded_m6559_MethodInfo);
		InvokableCallList_t1139 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_Invoke_m6554(L_0, ___parameters, /*hidden argument*/&InvokableCallList_Invoke_m6554_MethodInfo);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern MethodInfo UnityEventBase_ToString_m2086_MethodInfo;
 String_t* UnityEventBase_ToString_m2086 (UnityEventBase_t1136 * __this, MethodInfo* method){
	{
		String_t* L_0 = Object_ToString_m322(__this, /*hidden argument*/&Object_ToString_m322_MethodInfo);
		Type_t * L_1 = Object_GetType_m323(__this, /*hidden argument*/&Object_GetType_m323_MethodInfo);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_FullName_m6757_MethodInfo, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_3 = String_Concat_m287(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral178, L_2, /*hidden argument*/&String_Concat_m287_MethodInfo);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
 MethodInfo_t141 * UnityEventBase_GetValidMethodInfo_m6563 (Object_t * __this/* static, unused */, Object_t * ___obj, String_t* ___functionName, TypeU5BU5D_t922* ___argumentTypes, MethodInfo* method){
	Type_t * V_0 = {0};
	MethodInfo_t141 * V_1 = {0};
	ParameterInfoU5BU5D_t1221* V_2 = {0};
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t1222 * V_5 = {0};
	ParameterInfoU5BU5D_t1221* V_6 = {0};
	int32_t V_7 = 0;
	Type_t * V_8 = {0};
	Type_t * V_9 = {0};
	{
		NullCheck(___obj);
		Type_t * L_0 = Object_GetType_m323(___obj, /*hidden argument*/&Object_GetType_m323_MethodInfo);
		V_0 = L_0;
		goto IL_008e;
	}

IL_000c:
	{
		NullCheck(V_0);
		MethodInfo_t141 * L_1 = (MethodInfo_t141 *)VirtFuncInvoker5< MethodInfo_t141 *, String_t*, int32_t, Binder_t1215 *, TypeU5BU5D_t922*, ParameterModifierU5BU5D_t1216* >::Invoke(&Type_GetMethod_m6758_MethodInfo, V_0, ___functionName, ((int32_t)52), (Binder_t1215 *)NULL, ___argumentTypes, (ParameterModifierU5BU5D_t1216*)(ParameterModifierU5BU5D_t1216*)NULL);
		V_1 = L_1;
		if (!V_1)
		{
			goto IL_0087;
		}
	}
	{
		NullCheck(V_1);
		ParameterInfoU5BU5D_t1221* L_2 = (ParameterInfoU5BU5D_t1221*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1221* >::Invoke(&MethodBase_GetParameters_m6700_MethodInfo, V_1);
		V_2 = L_2;
		V_3 = 1;
		V_4 = 0;
		V_6 = V_2;
		V_7 = 0;
		goto IL_0074;
	}

IL_0036:
	{
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, V_7);
		int32_t L_3 = V_7;
		V_5 = (*(ParameterInfo_t1222 **)(ParameterInfo_t1222 **)SZArrayLdElema(V_6, L_3));
		NullCheck(___argumentTypes);
		IL2CPP_ARRAY_BOUNDS_CHECK(___argumentTypes, V_4);
		int32_t L_4 = V_4;
		V_8 = (*(Type_t **)(Type_t **)SZArrayLdElema(___argumentTypes, L_4));
		NullCheck(V_5);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&ParameterInfo_get_ParameterType_m6701_MethodInfo, V_5);
		V_9 = L_5;
		NullCheck(V_8);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&Type_get_IsPrimitive_m6759_MethodInfo, V_8);
		NullCheck(V_9);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(&Type_get_IsPrimitive_m6759_MethodInfo, V_9);
		V_3 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		if (V_3)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_007f;
	}

IL_0068:
	{
		V_4 = ((int32_t)(V_4+1));
		V_7 = ((int32_t)(V_7+1));
	}

IL_0074:
	{
		NullCheck(V_6);
		if ((((int32_t)V_7) < ((int32_t)(((int32_t)(((Array_t *)V_6)->max_length))))))
		{
			goto IL_0036;
		}
	}

IL_007f:
	{
		if (!V_3)
		{
			goto IL_0087;
		}
	}
	{
		return V_1;
	}

IL_0087:
	{
		NullCheck(V_0);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&Type_get_BaseType_m6670_MethodInfo, V_0);
		V_0 = L_8;
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Object_t_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)V_0) == ((Type_t *)L_9)))
		{
			goto IL_00a4;
		}
	}
	{
		if (V_0)
		{
			goto IL_000c;
		}
	}

IL_00a4:
	{
		return (MethodInfo_t141 *)NULL;
	}
}
// Metadata Definition UnityEngine.Events.UnityEventBase
extern Il2CppType InvokableCallList_t1139_0_0_1;
FieldInfo UnityEventBase_t1136____m_Calls_0_FieldInfo = 
{
	"m_Calls"/* name */
	, &InvokableCallList_t1139_0_0_1/* type */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1136, ___m_Calls_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentCallGroup_t1138_0_0_1;
extern CustomAttributesCache UnityEventBase_t1136__CustomAttributeCache_m_PersistentCalls;
FieldInfo UnityEventBase_t1136____m_PersistentCalls_1_FieldInfo = 
{
	"m_PersistentCalls"/* name */
	, &PersistentCallGroup_t1138_0_0_1/* type */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1136, ___m_PersistentCalls_1)/* data */
	, &UnityEventBase_t1136__CustomAttributeCache_m_PersistentCalls/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache UnityEventBase_t1136__CustomAttributeCache_m_TypeName;
FieldInfo UnityEventBase_t1136____m_TypeName_2_FieldInfo = 
{
	"m_TypeName"/* name */
	, &String_t_0_0_1/* type */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1136, ___m_TypeName_2)/* data */
	, &UnityEventBase_t1136__CustomAttributeCache_m_TypeName/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo UnityEventBase_t1136____m_CallsDirty_3_FieldInfo = 
{
	"m_CallsDirty"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1136, ___m_CallsDirty_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEventBase_t1136_FieldInfos[] =
{
	&UnityEventBase_t1136____m_Calls_0_FieldInfo,
	&UnityEventBase_t1136____m_PersistentCalls_1_FieldInfo,
	&UnityEventBase_t1136____m_TypeName_2_FieldInfo,
	&UnityEventBase_t1136____m_CallsDirty_3_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
MethodInfo UnityEventBase__ctor_m6555_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m6555/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_FindMethod_Impl_m6756_ParameterInfos[] = 
{
	{"name", 0, 134219433, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219434, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEventBase_FindMethod_Impl_m6756_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_FindMethod_Impl_m6756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_GetDelegate_m6732_ParameterInfos[] = 
{
	{"target", 0, 134219435, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219436, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEventBase_GetDelegate_m6732_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_GetDelegate_m6732_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PersistentCall_t1135_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_FindMethod_m6556_ParameterInfos[] = 
{
	{"call", 0, 134219437, &EmptyCustomAttributesCache, &PersistentCall_t1135_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
MethodInfo UnityEventBase_FindMethod_m6556_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6556/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_FindMethod_m6556_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType PersistentListenerMode_t1125_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_FindMethod_m6557_ParameterInfos[] = 
{
	{"name", 0, 134219438, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"listener", 1, 134219439, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"mode", 2, 134219440, &EmptyCustomAttributesCache, &PersistentListenerMode_t1125_0_0_0},
	{"argumentType", 3, 134219441, &EmptyCustomAttributesCache, &Type_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
MethodInfo UnityEventBase_FindMethod_m6557_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6557/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t123_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_FindMethod_m6557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
MethodInfo UnityEventBase_DirtyPersistentCalls_m6558_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m6558/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m6559_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m6559/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_AddCall_m6560_ParameterInfos[] = 
{
	{"call", 0, 134219442, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
MethodInfo UnityEventBase_AddCall_m6560_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m6560/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_AddCall_m6560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_RemoveListener_m6561_ParameterInfos[] = 
{
	{"targetObj", 0, 134219443, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219444, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEventBase_RemoveListener_m6561_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m6561/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_RemoveListener_m6561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_Invoke_m6562_ParameterInfos[] = 
{
	{"parameters", 0, 134219445, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
MethodInfo UnityEventBase_Invoke_m6562_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m6562/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_Invoke_m6562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
MethodInfo UnityEventBase_ToString_m2086_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m2086/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType TypeU5BU5D_t922_0_0_0;
extern Il2CppType TypeU5BU5D_t922_0_0_0;
static ParameterInfo UnityEventBase_t1136_UnityEventBase_GetValidMethodInfo_m6563_ParameterInfos[] = 
{
	{"obj", 0, 134219446, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"functionName", 1, 134219447, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"argumentTypes", 2, 134219448, &EmptyCustomAttributesCache, &TypeU5BU5D_t922_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
MethodInfo UnityEventBase_GetValidMethodInfo_m6563_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m6563/* method */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1136_UnityEventBase_GetValidMethodInfo_m6563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEventBase_t1136_MethodInfos[] =
{
	&UnityEventBase__ctor_m6555_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m6756_MethodInfo,
	&UnityEventBase_GetDelegate_m6732_MethodInfo,
	&UnityEventBase_FindMethod_m6556_MethodInfo,
	&UnityEventBase_FindMethod_m6557_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m6558_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m6559_MethodInfo,
	&UnityEventBase_AddCall_m6560_MethodInfo,
	&UnityEventBase_RemoveListener_m6561_MethodInfo,
	&UnityEventBase_Invoke_m6562_MethodInfo,
	&UnityEventBase_ToString_m2086_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo,
	NULL
};
static MethodInfo* UnityEventBase_t1136_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UnityEventBase_ToString_m2086_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo,
	NULL,
	NULL,
};
extern TypeInfo ISerializationCallbackReceiver_t470_il2cpp_TypeInfo;
static TypeInfo* UnityEventBase_t1136_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t470_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1136_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t470_il2cpp_TypeInfo, 4},
};
void UnityEventBase_t1136_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t468 * tmp;
		tmp = (FormerlySerializedAsAttribute_t468 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m2083(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), &FormerlySerializedAsAttribute__ctor_m2083_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void UnityEventBase_t1136_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache UnityEventBase_t1136__CustomAttributeCache_m_PersistentCalls = {
2,
NULL,
&UnityEventBase_t1136_CustomAttributesCacheGenerator_m_PersistentCalls
};
CustomAttributesCache UnityEventBase_t1136__CustomAttributeCache_m_TypeName = {
1,
NULL,
&UnityEventBase_t1136_CustomAttributesCacheGenerator_m_TypeName
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEventBase_t1136_1_0_0;
struct UnityEventBase_t1136;
extern CustomAttributesCache UnityEventBase_t1136__CustomAttributeCache_m_PersistentCalls;
extern CustomAttributesCache UnityEventBase_t1136__CustomAttributeCache_m_TypeName;
TypeInfo UnityEventBase_t1136_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t1136_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEventBase_t1136_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* element_class */
	, UnityEventBase_t1136_InterfacesTypeInfos/* implemented_interfaces */
	, UnityEventBase_t1136_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* cast_class */
	, &UnityEventBase_t1136_0_0_0/* byval_arg */
	, &UnityEventBase_t1136_1_0_0/* this_arg */
	, UnityEventBase_t1136_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1136)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_t321_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"



// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern MethodInfo UnityEvent__ctor_m2248_MethodInfo;
 void UnityEvent__ctor_m2248 (UnityEvent_t321 * __this, MethodInfo* method){
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 0));
		UnityEventBase__ctor_m6555(__this, /*hidden argument*/&UnityEventBase__ctor_m6555_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_FindMethod_Impl_m2249_MethodInfo;
 MethodInfo_t141 * UnityEvent_FindMethod_Impl_m2249 (UnityEvent_t321 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method){
	{
		MethodInfo_t141 * L_0 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___targetObj, ___name, ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 0)), /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_GetDelegate_m2250_MethodInfo;
 BaseInvokableCall_t1127 * UnityEvent_GetDelegate_m2250 (UnityEvent_t321 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method){
	{
		InvokableCall_t1128 * L_0 = (InvokableCall_t1128 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvokableCall_t1128_il2cpp_TypeInfo));
		InvokableCall__ctor_m6536(L_0, ___target, ___theFunction, /*hidden argument*/&InvokableCall__ctor_m6536_MethodInfo);
		return L_0;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern MethodInfo UnityEvent_Invoke_m2253_MethodInfo;
 void UnityEvent_Invoke_m2253 (UnityEvent_t321 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t130* L_0 = (__this->___m_InvokeArray_4);
		UnityEventBase_Invoke_m6562(__this, L_0, /*hidden argument*/&UnityEventBase_Invoke_m6562_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_t321____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_t321_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_t321, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_t321_FieldInfos[] =
{
	&UnityEvent_t321____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
MethodInfo UnityEvent__ctor_m2248_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m2248/* method */
	, &UnityEvent_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_t321_UnityEvent_FindMethod_Impl_m2249_ParameterInfos[] = 
{
	{"name", 0, 134219449, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219450, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_FindMethod_Impl_m2249_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m2249/* method */
	, &UnityEvent_t321_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t321_UnityEvent_FindMethod_Impl_m2249_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_t321_UnityEvent_GetDelegate_m2250_ParameterInfos[] = 
{
	{"target", 0, 134219451, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219452, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_GetDelegate_m2250_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m2250/* method */
	, &UnityEvent_t321_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t321_UnityEvent_GetDelegate_m2250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
MethodInfo UnityEvent_Invoke_m2253_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m2253/* method */
	, &UnityEvent_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_t321_MethodInfos[] =
{
	&UnityEvent__ctor_m2248_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2249_MethodInfo,
	&UnityEvent_GetDelegate_m2250_MethodInfo,
	&UnityEvent_Invoke_m2253_MethodInfo,
	NULL
};
static MethodInfo* UnityEvent_t321_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UnityEventBase_ToString_m2086_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2249_MethodInfo,
	&UnityEvent_GetDelegate_m2250_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEvent_t321_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t470_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_t321_0_0_0;
extern Il2CppType UnityEvent_t321_1_0_0;
struct UnityEvent_t321;
TypeInfo UnityEvent_t321_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t321_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_t321_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_t321_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_t321_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_t321_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_t321_0_0_0/* byval_arg */
	, &UnityEvent_t321_1_0_0/* this_arg */
	, UnityEvent_t321_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t321)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`1
#include "UnityEngine_UnityEngine_Events_UnityEvent_1.h"
extern Il2CppGenericContainer UnityEvent_1_t1141_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t1141_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_1_t1141_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_1_t1141_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_1_t1141_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t1141_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_1_t1141_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_1_t1141_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_1_t1141_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t1141_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
MethodInfo UnityEvent_1__ctor_m6760_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1249_0_0_0;
extern Il2CppType UnityAction_1_t1249_0_0_0;
static ParameterInfo UnityEvent_1_t1141_UnityEvent_1_AddListener_m6761_ParameterInfos[] = 
{
	{"call", 0, 134219453, &EmptyCustomAttributesCache, &UnityAction_1_t1249_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_AddListener_m6761_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1141_UnityEvent_1_AddListener_m6761_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1249_0_0_0;
static ParameterInfo UnityEvent_1_t1141_UnityEvent_1_RemoveListener_m6762_ParameterInfos[] = 
{
	{"call", 0, 134219454, &EmptyCustomAttributesCache, &UnityAction_1_t1249_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_RemoveListener_m6762_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1141_UnityEvent_1_RemoveListener_m6762_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_1_t1141_UnityEvent_1_FindMethod_Impl_m6763_ParameterInfos[] = 
{
	{"name", 0, 134219455, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219456, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_1_FindMethod_Impl_m6763_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1141_UnityEvent_1_FindMethod_Impl_m6763_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_1_t1141_UnityEvent_1_GetDelegate_m6764_ParameterInfos[] = 
{
	{"target", 0, 134219457, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219458, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_1_GetDelegate_m6764_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1141_UnityEvent_1_GetDelegate_m6764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1249_0_0_0;
static ParameterInfo UnityEvent_1_t1141_UnityEvent_1_GetDelegate_m6765_ParameterInfos[] = 
{
	{"action", 0, 134219459, &EmptyCustomAttributesCache, &UnityAction_1_t1249_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_GetDelegate_m6765_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1141_UnityEvent_1_GetDelegate_m6765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityEvent_1_t1141_gp_0_0_0_0;
extern Il2CppType UnityEvent_1_t1141_gp_0_0_0_0;
static ParameterInfo UnityEvent_1_t1141_UnityEvent_1_Invoke_m6766_ParameterInfos[] = 
{
	{"arg0", 0, 134219460, &EmptyCustomAttributesCache, &UnityEvent_1_t1141_gp_0_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
MethodInfo UnityEvent_1_Invoke_m6766_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1141_UnityEvent_1_Invoke_m6766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_1_t1141_MethodInfos[] =
{
	&UnityEvent_1__ctor_m6760_MethodInfo,
	&UnityEvent_1_AddListener_m6761_MethodInfo,
	&UnityEvent_1_RemoveListener_m6762_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m6763_MethodInfo,
	&UnityEvent_1_GetDelegate_m6764_MethodInfo,
	&UnityEvent_1_GetDelegate_m6765_MethodInfo,
	&UnityEvent_1_Invoke_m6766_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_1_t1141____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_1_t1141_FieldInfos[] =
{
	&UnityEvent_1_t1141____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_1_t1141_0_0_0;
extern Il2CppType UnityEvent_1_t1141_1_0_0;
struct UnityEvent_1_t1141;
TypeInfo UnityEvent_1_t1141_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t1141_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_1_t1141_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_1_t1141_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_1_t1141_0_0_0/* byval_arg */
	, &UnityEvent_1_t1141_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_1_t1141_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`2
#include "UnityEngine_UnityEngine_Events_UnityEvent_2.h"
extern Il2CppGenericContainer UnityEvent_2_t1142_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t1142_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_2_t1142_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_2_t1142_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_2_t1142_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_2_t1142_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_2_t1142_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_2_t1142_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t1142_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t1142_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_2_t1142_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_2_t1142_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_2_t1142_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t1142_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
MethodInfo UnityEvent_2__ctor_m6767_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1142_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_2_t1142_UnityEvent_2_FindMethod_Impl_m6768_ParameterInfos[] = 
{
	{"name", 0, 134219461, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219462, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_2_FindMethod_Impl_m6768_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1142_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1142_UnityEvent_2_FindMethod_Impl_m6768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_2_t1142_UnityEvent_2_GetDelegate_m6769_ParameterInfos[] = 
{
	{"target", 0, 134219463, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219464, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_2_GetDelegate_m6769_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1142_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1142_UnityEvent_2_GetDelegate_m6769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_2_t1142_MethodInfos[] =
{
	&UnityEvent_2__ctor_m6767_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m6768_MethodInfo,
	&UnityEvent_2_GetDelegate_m6769_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_2_t1142____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_2_t1142_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_2_t1142_FieldInfos[] =
{
	&UnityEvent_2_t1142____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_2_t1142_0_0_0;
extern Il2CppType UnityEvent_2_t1142_1_0_0;
struct UnityEvent_2_t1142;
TypeInfo UnityEvent_2_t1142_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t1142_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_2_t1142_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_2_t1142_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_2_t1142_0_0_0/* byval_arg */
	, &UnityEvent_2_t1142_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_2_t1142_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`3
#include "UnityEngine_UnityEngine_Events_UnityEvent_3.h"
extern Il2CppGenericContainer UnityEvent_3_t1143_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t1143_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1143_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1143_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_3_t1143_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1143_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1143_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityEvent_3_t1143_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1143_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1143_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_3_t1143_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t1143_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1143_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1143_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_3_t1143_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_3_t1143_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_3_t1143_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t1143_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
MethodInfo UnityEvent_3__ctor_m6770_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_3_t1143_UnityEvent_3_FindMethod_Impl_m6771_ParameterInfos[] = 
{
	{"name", 0, 134219465, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219466, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_3_FindMethod_Impl_m6771_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1143_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1143_UnityEvent_3_FindMethod_Impl_m6771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_3_t1143_UnityEvent_3_GetDelegate_m6772_ParameterInfos[] = 
{
	{"target", 0, 134219467, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219468, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_3_GetDelegate_m6772_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1143_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1143_UnityEvent_3_GetDelegate_m6772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_3_t1143_MethodInfos[] =
{
	&UnityEvent_3__ctor_m6770_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m6771_MethodInfo,
	&UnityEvent_3_GetDelegate_m6772_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_3_t1143____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_3_t1143_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_3_t1143_FieldInfos[] =
{
	&UnityEvent_3_t1143____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_3_t1143_0_0_0;
extern Il2CppType UnityEvent_3_t1143_1_0_0;
struct UnityEvent_3_t1143;
TypeInfo UnityEvent_3_t1143_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t1143_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_3_t1143_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_3_t1143_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_3_t1143_0_0_0/* byval_arg */
	, &UnityEvent_3_t1143_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_3_t1143_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`4
#include "UnityEngine_UnityEngine_Events_UnityEvent_4.h"
extern Il2CppGenericContainer UnityEvent_4_t1144_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t1144_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1144_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1144_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1144_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1144_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1144_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1144_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1144_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1144_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1144_gp_T3_3_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1144_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1144_Il2CppGenericContainer, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_4_t1144_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t1144_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1144_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1144_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1144_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_4_t1144_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_4_t1144_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_4_t1144_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t1144_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
MethodInfo UnityEvent_4__ctor_m6773_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1144_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_4_t1144_UnityEvent_4_FindMethod_Impl_m6774_ParameterInfos[] = 
{
	{"name", 0, 134219469, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219470, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_4_FindMethod_Impl_m6774_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1144_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1144_UnityEvent_4_FindMethod_Impl_m6774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_4_t1144_UnityEvent_4_GetDelegate_m6775_ParameterInfos[] = 
{
	{"target", 0, 134219471, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219472, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_4_GetDelegate_m6775_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1144_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1144_UnityEvent_4_GetDelegate_m6775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_4_t1144_MethodInfos[] =
{
	&UnityEvent_4__ctor_m6773_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m6774_MethodInfo,
	&UnityEvent_4_GetDelegate_m6775_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_4_t1144____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_4_t1144_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_4_t1144_FieldInfos[] =
{
	&UnityEvent_4_t1144____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_4_t1144_0_0_0;
extern Il2CppType UnityEvent_4_t1144_1_0_0;
struct UnityEvent_4_t1144;
TypeInfo UnityEvent_4_t1144_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t1144_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_4_t1144_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_4_t1144_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_4_t1144_0_0_0/* byval_arg */
	, &UnityEvent_4_t1144_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_4_t1144_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern MethodInfo MonoBehaviour__ctor_m254_MethodInfo;


// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern MethodInfo UserAuthorizationDialog__ctor_m6564_MethodInfo;
 void UserAuthorizationDialog__ctor_m6564 (UserAuthorizationDialog_t1145 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern MethodInfo UserAuthorizationDialog_Start_m6565_MethodInfo;
 void UserAuthorizationDialog_Start_m6565 (UserAuthorizationDialog_t1145 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern MethodInfo UserAuthorizationDialog_OnGUI_m6566_MethodInfo;
 void UserAuthorizationDialog_OnGUI_m6566 (UserAuthorizationDialog_t1145 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m6567_MethodInfo;
 void UserAuthorizationDialog_DoUserAuthorizationDialog_m6567 (UserAuthorizationDialog_t1145 * __this, int32_t ___windowID, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo UserAuthorizationDialog_t1145____width_2_FieldInfo = 
{
	"width"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo UserAuthorizationDialog_t1145____height_3_FieldInfo = 
{
	"height"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rect_t103_0_0_1;
FieldInfo UserAuthorizationDialog_t1145____windowRect_4_FieldInfo = 
{
	"windowRect"/* name */
	, &Rect_t103_0_0_1/* type */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* parent */
	, offsetof(UserAuthorizationDialog_t1145, ___windowRect_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Texture_t107_0_0_1;
FieldInfo UserAuthorizationDialog_t1145____warningIcon_5_FieldInfo = 
{
	"warningIcon"/* name */
	, &Texture_t107_0_0_1/* type */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* parent */
	, offsetof(UserAuthorizationDialog_t1145, ___warningIcon_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserAuthorizationDialog_t1145_FieldInfos[] =
{
	&UserAuthorizationDialog_t1145____width_2_FieldInfo,
	&UserAuthorizationDialog_t1145____height_3_FieldInfo,
	&UserAuthorizationDialog_t1145____windowRect_4_FieldInfo,
	&UserAuthorizationDialog_t1145____warningIcon_5_FieldInfo,
	NULL
};
static const int32_t UserAuthorizationDialog_t1145____width_2_DefaultValueData = 385;
static Il2CppFieldDefaultValueEntry UserAuthorizationDialog_t1145____width_2_DefaultValue = 
{
	&UserAuthorizationDialog_t1145____width_2_FieldInfo/* field */
	, { (char*)&UserAuthorizationDialog_t1145____width_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t UserAuthorizationDialog_t1145____height_3_DefaultValueData = 155;
static Il2CppFieldDefaultValueEntry UserAuthorizationDialog_t1145____height_3_DefaultValue = 
{
	&UserAuthorizationDialog_t1145____height_3_FieldInfo/* field */
	, { (char*)&UserAuthorizationDialog_t1145____height_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserAuthorizationDialog_t1145_FieldDefaultValues[] = 
{
	&UserAuthorizationDialog_t1145____width_2_DefaultValue,
	&UserAuthorizationDialog_t1145____height_3_DefaultValue,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
MethodInfo UserAuthorizationDialog__ctor_m6564_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m6564/* method */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
MethodInfo UserAuthorizationDialog_Start_m6565_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m6565/* method */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
MethodInfo UserAuthorizationDialog_OnGUI_m6566_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m6566/* method */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo UserAuthorizationDialog_t1145_UserAuthorizationDialog_DoUserAuthorizationDialog_m6567_ParameterInfos[] = 
{
	{"windowID", 0, 134219473, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m6567_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m6567/* method */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, UserAuthorizationDialog_t1145_UserAuthorizationDialog_DoUserAuthorizationDialog_m6567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserAuthorizationDialog_t1145_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m6564_MethodInfo,
	&UserAuthorizationDialog_Start_m6565_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m6566_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m6567_MethodInfo,
	NULL
};
static MethodInfo* UserAuthorizationDialog_t1145_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
};
extern TypeInfo AddComponentMenu_t467_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern MethodInfo AddComponentMenu__ctor_m2082_MethodInfo;
void UserAuthorizationDialog_t1145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t467 * tmp;
		tmp = (AddComponentMenu_t467 *)il2cpp_codegen_object_new (&AddComponentMenu_t467_il2cpp_TypeInfo);
		AddComponentMenu__ctor_m2082(tmp, il2cpp_codegen_string_new_wrapper(""), &AddComponentMenu__ctor_m2082_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache UserAuthorizationDialog_t1145__CustomAttributeCache = {
1,
NULL,
&UserAuthorizationDialog_t1145_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1145_1_0_0;
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
struct UserAuthorizationDialog_t1145;
extern CustomAttributesCache UserAuthorizationDialog_t1145__CustomAttributeCache;
TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t1145_MethodInfos/* methods */
	, NULL/* properties */
	, UserAuthorizationDialog_t1145_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserAuthorizationDialog_t1145_VTable/* vtable */
	, &UserAuthorizationDialog_t1145__CustomAttributeCache/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* cast_class */
	, &UserAuthorizationDialog_t1145_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1145_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserAuthorizationDialog_t1145_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1145)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DefaultValueAttribute_t1146_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_Attribute.h"
extern MethodInfo DefaultValueAttribute_get_Value_m6569_MethodInfo;


// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern MethodInfo DefaultValueAttribute__ctor_m6568_MethodInfo;
 void DefaultValueAttribute__ctor_m6568 (DefaultValueAttribute_t1146 * __this, String_t* ___value, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		__this->___DefaultValue_0 = ___value;
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
 Object_t * DefaultValueAttribute_get_Value_m6569 (DefaultValueAttribute_t1146 * __this, MethodInfo* method){
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern MethodInfo DefaultValueAttribute_Equals_m6570_MethodInfo;
 bool DefaultValueAttribute_Equals_m6570 (DefaultValueAttribute_t1146 * __this, Object_t * ___obj, MethodInfo* method){
	DefaultValueAttribute_t1146 * V_0 = {0};
	{
		V_0 = ((DefaultValueAttribute_t1146 *)IsInst(___obj, InitializedTypeInfo(&DefaultValueAttribute_t1146_il2cpp_TypeInfo)));
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_1 = DefaultValueAttribute_get_Value_m6569(V_0, /*hidden argument*/&DefaultValueAttribute_get_Value_m6569_MethodInfo);
		return ((((Object_t *)L_1) == ((Object_t *)NULL))? 1 : 0);
	}

IL_0024:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(V_0);
		Object_t * L_3 = DefaultValueAttribute_get_Value_m6569(V_0, /*hidden argument*/&DefaultValueAttribute_get_Value_m6569_MethodInfo);
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, L_2, L_3);
		return L_4;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern MethodInfo DefaultValueAttribute_GetHashCode_m6571_MethodInfo;
 int32_t DefaultValueAttribute_GetHashCode_m6571 (DefaultValueAttribute_t1146 * __this, MethodInfo* method){
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m4417(__this, /*hidden argument*/&Attribute_GetHashCode_m4417_MethodInfo);
		return L_1;
	}

IL_0012:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Object_GetHashCode_m321_MethodInfo, L_2);
		return L_3;
	}
}
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern Il2CppType Object_t_0_0_1;
FieldInfo DefaultValueAttribute_t1146____DefaultValue_0_FieldInfo = 
{
	"DefaultValue"/* name */
	, &Object_t_0_0_1/* type */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultValueAttribute_t1146, ___DefaultValue_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DefaultValueAttribute_t1146_FieldInfos[] =
{
	&DefaultValueAttribute_t1146____DefaultValue_0_FieldInfo,
	NULL
};
static PropertyInfo DefaultValueAttribute_t1146____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t1146_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m6569_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* DefaultValueAttribute_t1146_PropertyInfos[] =
{
	&DefaultValueAttribute_t1146____Value_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo DefaultValueAttribute_t1146_DefaultValueAttribute__ctor_m6568_ParameterInfos[] = 
{
	{"value", 0, 134219474, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
MethodInfo DefaultValueAttribute__ctor_m6568_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m6568/* method */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, DefaultValueAttribute_t1146_DefaultValueAttribute__ctor_m6568_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
MethodInfo DefaultValueAttribute_get_Value_m6569_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m6569/* method */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo DefaultValueAttribute_t1146_DefaultValueAttribute_Equals_m6570_ParameterInfos[] = 
{
	{"obj", 0, 134219475, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
MethodInfo DefaultValueAttribute_Equals_m6570_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m6570/* method */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, DefaultValueAttribute_t1146_DefaultValueAttribute_Equals_m6570_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
MethodInfo DefaultValueAttribute_GetHashCode_m6571_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m6571/* method */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultValueAttribute_t1146_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m6568_MethodInfo,
	&DefaultValueAttribute_get_Value_m6569_MethodInfo,
	&DefaultValueAttribute_Equals_m6570_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6571_MethodInfo,
	NULL
};
static MethodInfo* DefaultValueAttribute_t1146_VTable[] =
{
	&DefaultValueAttribute_Equals_m6570_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6571_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1146_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void DefaultValueAttribute_t1146_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 18432, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache DefaultValueAttribute_t1146__CustomAttributeCache = {
1,
NULL,
&DefaultValueAttribute_t1146_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
extern Il2CppType DefaultValueAttribute_t1146_1_0_0;
struct DefaultValueAttribute_t1146;
extern CustomAttributesCache DefaultValueAttribute_t1146__CustomAttributeCache;
TypeInfo DefaultValueAttribute_t1146_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t1146_MethodInfos/* methods */
	, DefaultValueAttribute_t1146_PropertyInfos/* properties */
	, DefaultValueAttribute_t1146_FieldInfos/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultValueAttribute_t1146_VTable/* vtable */
	, &DefaultValueAttribute_t1146__CustomAttributeCache/* custom_attributes_cache */
	, &DefaultValueAttribute_t1146_il2cpp_TypeInfo/* cast_class */
	, &DefaultValueAttribute_t1146_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1146_1_0_0/* this_arg */
	, DefaultValueAttribute_t1146_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1146)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"



// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern MethodInfo ExcludeFromDocsAttribute__ctor_m6572_MethodInfo;
 void ExcludeFromDocsAttribute__ctor_m6572 (ExcludeFromDocsAttribute_t1147 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
MethodInfo ExcludeFromDocsAttribute__ctor_m6572_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m6572/* method */
	, &ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExcludeFromDocsAttribute_t1147_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m6572_MethodInfo,
	NULL
};
static MethodInfo* ExcludeFromDocsAttribute_t1147_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1147_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void ExcludeFromDocsAttribute_t1147_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 64, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache ExcludeFromDocsAttribute_t1147__CustomAttributeCache = {
1,
NULL,
&ExcludeFromDocsAttribute_t1147_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1147_1_0_0;
struct ExcludeFromDocsAttribute_t1147;
extern CustomAttributesCache ExcludeFromDocsAttribute_t1147__CustomAttributeCache;
TypeInfo ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t1147_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ExcludeFromDocsAttribute_t1147_VTable/* vtable */
	, &ExcludeFromDocsAttribute_t1147__CustomAttributeCache/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo/* cast_class */
	, &ExcludeFromDocsAttribute_t1147_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1147_1_0_0/* this_arg */
	, ExcludeFromDocsAttribute_t1147_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1147)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
 void FormerlySerializedAsAttribute__ctor_m2083 (FormerlySerializedAsAttribute_t468 * __this, String_t* ___oldName, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		__this->___m_oldName_0 = ___oldName;
		return;
	}
}
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern Il2CppType String_t_0_0_1;
FieldInfo FormerlySerializedAsAttribute_t468____m_oldName_0_FieldInfo = 
{
	"m_oldName"/* name */
	, &String_t_0_0_1/* type */
	, &FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo/* parent */
	, offsetof(FormerlySerializedAsAttribute_t468, ___m_oldName_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* FormerlySerializedAsAttribute_t468_FieldInfos[] =
{
	&FormerlySerializedAsAttribute_t468____m_oldName_0_FieldInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo FormerlySerializedAsAttribute_t468_FormerlySerializedAsAttribute__ctor_m2083_ParameterInfos[] = 
{
	{"oldName", 0, 134219476, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
MethodInfo FormerlySerializedAsAttribute__ctor_m2083_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m2083/* method */
	, &FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t468_FormerlySerializedAsAttribute__ctor_m2083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FormerlySerializedAsAttribute_t468_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m2083_MethodInfo,
	NULL
};
static MethodInfo* FormerlySerializedAsAttribute_t468_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t468_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void FormerlySerializedAsAttribute_t468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 256, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, true, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6677(tmp, false, &AttributeUsageAttribute_set_Inherited_m6677_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache FormerlySerializedAsAttribute_t468__CustomAttributeCache = {
1,
NULL,
&FormerlySerializedAsAttribute_t468_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t468_1_0_0;
struct FormerlySerializedAsAttribute_t468;
extern CustomAttributesCache FormerlySerializedAsAttribute_t468__CustomAttributeCache;
TypeInfo FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t468_MethodInfos/* methods */
	, NULL/* properties */
	, FormerlySerializedAsAttribute_t468_FieldInfos/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, FormerlySerializedAsAttribute_t468_VTable/* vtable */
	, &FormerlySerializedAsAttribute_t468__CustomAttributeCache/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo/* cast_class */
	, &FormerlySerializedAsAttribute_t468_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t468_1_0_0/* this_arg */
	, FormerlySerializedAsAttribute_t468_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t468)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TypeInferenceRules_t1148_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"



// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo TypeInferenceRules_t1148____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &TypeInferenceRules_t1148_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInferenceRules_t1148, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_32854;
FieldInfo TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_FieldInfo = 
{
	"TypeReferencedByFirstArgument"/* name */
	, &TypeInferenceRules_t1148_0_0_32854/* type */
	, &TypeInferenceRules_t1148_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_32854;
FieldInfo TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_FieldInfo = 
{
	"TypeReferencedBySecondArgument"/* name */
	, &TypeInferenceRules_t1148_0_0_32854/* type */
	, &TypeInferenceRules_t1148_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_32854;
FieldInfo TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo = 
{
	"ArrayOfTypeReferencedByFirstArgument"/* name */
	, &TypeInferenceRules_t1148_0_0_32854/* type */
	, &TypeInferenceRules_t1148_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_32854;
FieldInfo TypeInferenceRules_t1148____TypeOfFirstArgument_5_FieldInfo = 
{
	"TypeOfFirstArgument"/* name */
	, &TypeInferenceRules_t1148_0_0_32854/* type */
	, &TypeInferenceRules_t1148_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TypeInferenceRules_t1148_FieldInfos[] =
{
	&TypeInferenceRules_t1148____value___1_FieldInfo,
	&TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_FieldInfo,
	&TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_FieldInfo,
	&TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo,
	&TypeInferenceRules_t1148____TypeOfFirstArgument_5_FieldInfo,
	NULL
};
static const int32_t TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_DefaultValue = 
{
	&TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_DefaultValue = 
{
	&TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_DefaultValue = 
{
	&TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1148____TypeOfFirstArgument_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1148____TypeOfFirstArgument_5_DefaultValue = 
{
	&TypeInferenceRules_t1148____TypeOfFirstArgument_5_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1148____TypeOfFirstArgument_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TypeInferenceRules_t1148_FieldDefaultValues[] = 
{
	&TypeInferenceRules_t1148____TypeReferencedByFirstArgument_2_DefaultValue,
	&TypeInferenceRules_t1148____TypeReferencedBySecondArgument_3_DefaultValue,
	&TypeInferenceRules_t1148____ArrayOfTypeReferencedByFirstArgument_4_DefaultValue,
	&TypeInferenceRules_t1148____TypeOfFirstArgument_5_DefaultValue,
	NULL
};
static MethodInfo* TypeInferenceRules_t1148_MethodInfos[] =
{
	NULL
};
static MethodInfo* TypeInferenceRules_t1148_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1148_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
extern Il2CppType TypeInferenceRules_t1148_1_0_0;
TypeInfo TypeInferenceRules_t1148_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t1148_MethodInfos/* methods */
	, NULL/* properties */
	, TypeInferenceRules_t1148_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TypeInferenceRules_t1148_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &TypeInferenceRules_t1148_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1148_1_0_0/* this_arg */
	, TypeInferenceRules_t1148_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TypeInferenceRules_t1148_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1148)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"

// System.Enum
#include "mscorlib_System_Enum.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
extern MethodInfo TypeInferenceRuleAttribute__ctor_m6574_MethodInfo;


// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern MethodInfo TypeInferenceRuleAttribute__ctor_m6573_MethodInfo;
 void TypeInferenceRuleAttribute__ctor_m6573 (TypeInferenceRuleAttribute_t1149 * __this, int32_t ___rule, MethodInfo* method){
	{
		int32_t L_0 = ___rule;
		Object_t * L_1 = Box(InitializedTypeInfo(&TypeInferenceRules_t1148_il2cpp_TypeInfo), &L_0);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Enum_ToString_m589_MethodInfo, L_1);
		TypeInferenceRuleAttribute__ctor_m6574(__this, L_2, /*hidden argument*/&TypeInferenceRuleAttribute__ctor_m6574_MethodInfo);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
 void TypeInferenceRuleAttribute__ctor_m6574 (TypeInferenceRuleAttribute_t1149 * __this, String_t* ___rule, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		__this->____rule_0 = ___rule;
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern MethodInfo TypeInferenceRuleAttribute_ToString_m6575_MethodInfo;
 String_t* TypeInferenceRuleAttribute_ToString_m6575 (TypeInferenceRuleAttribute_t1149 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->____rule_0);
		return L_0;
	}
}
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern Il2CppType String_t_0_0_33;
FieldInfo TypeInferenceRuleAttribute_t1149_____rule_0_FieldInfo = 
{
	"_rule"/* name */
	, &String_t_0_0_33/* type */
	, &TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInferenceRuleAttribute_t1149, ____rule_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TypeInferenceRuleAttribute_t1149_FieldInfos[] =
{
	&TypeInferenceRuleAttribute_t1149_____rule_0_FieldInfo,
	NULL
};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo TypeInferenceRuleAttribute_t1149_TypeInferenceRuleAttribute__ctor_m6573_ParameterInfos[] = 
{
	{"rule", 0, 134219477, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
MethodInfo TypeInferenceRuleAttribute__ctor_m6573_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6573/* method */
	, &TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, TypeInferenceRuleAttribute_t1149_TypeInferenceRuleAttribute__ctor_m6573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TypeInferenceRuleAttribute_t1149_TypeInferenceRuleAttribute__ctor_m6574_ParameterInfos[] = 
{
	{"rule", 0, 134219478, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
MethodInfo TypeInferenceRuleAttribute__ctor_m6574_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6574/* method */
	, &TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t1149_TypeInferenceRuleAttribute__ctor_m6574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
MethodInfo TypeInferenceRuleAttribute_ToString_m6575_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m6575/* method */
	, &TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TypeInferenceRuleAttribute_t1149_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m6573_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m6574_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6575_MethodInfo,
	NULL
};
static MethodInfo* TypeInferenceRuleAttribute_t1149_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6575_MethodInfo,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1149_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
void TypeInferenceRuleAttribute_t1149_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 64, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TypeInferenceRuleAttribute_t1149__CustomAttributeCache = {
1,
NULL,
&TypeInferenceRuleAttribute_t1149_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1149_1_0_0;
struct TypeInferenceRuleAttribute_t1149;
extern CustomAttributesCache TypeInferenceRuleAttribute_t1149__CustomAttributeCache;
TypeInfo TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t1149_MethodInfos/* methods */
	, NULL/* properties */
	, TypeInferenceRuleAttribute_t1149_FieldInfos/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TypeInferenceRuleAttribute_t1149_VTable/* vtable */
	, &TypeInferenceRuleAttribute_t1149__CustomAttributeCache/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* cast_class */
	, &TypeInferenceRuleAttribute_t1149_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1149_1_0_0/* this_arg */
	, TypeInferenceRuleAttribute_t1149_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1149)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GenericStack_t998_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"

// System.Collections.Stack
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
extern MethodInfo Stack__ctor_m6776_MethodInfo;


// System.Void UnityEngineInternal.GenericStack::.ctor()
extern MethodInfo GenericStack__ctor_m6576_MethodInfo;
 void GenericStack__ctor_m6576 (GenericStack_t998 * __this, MethodInfo* method){
	{
		Stack__ctor_m6776(__this, /*hidden argument*/&Stack__ctor_m6776_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngineInternal.GenericStack
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
MethodInfo GenericStack__ctor_m6576_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m6576/* method */
	, &GenericStack_t998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GenericStack_t998_MethodInfos[] =
{
	&GenericStack__ctor_m6576_MethodInfo,
	NULL
};
extern MethodInfo Stack_GetEnumerator_m6777_MethodInfo;
extern MethodInfo Stack_get_Count_m6778_MethodInfo;
extern MethodInfo Stack_get_IsSynchronized_m6779_MethodInfo;
extern MethodInfo Stack_get_SyncRoot_m6780_MethodInfo;
extern MethodInfo Stack_CopyTo_m6781_MethodInfo;
extern MethodInfo Stack_Clear_m6602_MethodInfo;
extern MethodInfo Stack_Peek_m6782_MethodInfo;
extern MethodInfo Stack_Pop_m6783_MethodInfo;
extern MethodInfo Stack_Push_m6598_MethodInfo;
static MethodInfo* GenericStack_t998_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Stack_GetEnumerator_m6777_MethodInfo,
	&Stack_get_Count_m6778_MethodInfo,
	&Stack_get_IsSynchronized_m6779_MethodInfo,
	&Stack_get_SyncRoot_m6780_MethodInfo,
	&Stack_CopyTo_m6781_MethodInfo,
	&Stack_get_Count_m6778_MethodInfo,
	&Stack_get_IsSynchronized_m6779_MethodInfo,
	&Stack_get_SyncRoot_m6780_MethodInfo,
	&Stack_Clear_m6602_MethodInfo,
	&Stack_CopyTo_m6781_MethodInfo,
	&Stack_GetEnumerator_m6777_MethodInfo,
	&Stack_Peek_m6782_MethodInfo,
	&Stack_Pop_m6783_MethodInfo,
	&Stack_Push_m6598_MethodInfo,
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1259_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair GenericStack_t998_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1179_il2cpp_TypeInfo, 4},
	{ &ICloneable_t525_il2cpp_TypeInfo, 5},
	{ &ICollection_t1259_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GenericStack_t998_0_0_0;
extern Il2CppType GenericStack_t998_1_0_0;
extern TypeInfo Stack_t1150_il2cpp_TypeInfo;
struct GenericStack_t998;
TypeInfo GenericStack_t998_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t998_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Stack_t1150_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GenericStack_t998_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GenericStack_t998_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GenericStack_t998_il2cpp_TypeInfo/* cast_class */
	, &GenericStack_t998_0_0_0/* byval_arg */
	, &GenericStack_t998_1_0_0/* this_arg */
	, GenericStack_t998_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t998)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern MethodInfo UnityAction__ctor_m2452_MethodInfo;
 void UnityAction__ctor_m2452 (UnityAction_t341 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method){
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
 void UnityAction_Invoke_m2300 (UnityAction_t341 * __this, MethodInfo* method){
	if(__this->___prev_9 != NULL)
	{
		UnityAction_Invoke_m2300((UnityAction_t341 *)__this->___prev_9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
	((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
}
void pinvoke_delegate_wrapper_UnityAction_t341(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern MethodInfo UnityAction_BeginInvoke_m6577_MethodInfo;
 Object_t * UnityAction_BeginInvoke_m6577 (UnityAction_t341 * __this, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method){
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern MethodInfo UnityAction_EndInvoke_m6578_MethodInfo;
 void UnityAction_EndInvoke_m6578 (UnityAction_t341 * __this, Object_t * ___result, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_t341_UnityAction__ctor_m2452_ParameterInfos[] = 
{
	{"object", 0, 134219479, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219480, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction__ctor_m2452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m2452/* method */
	, &UnityAction_t341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_t341_UnityAction__ctor_m2452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
MethodInfo UnityAction_Invoke_m2300_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m2300/* method */
	, &UnityAction_t341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_t341_UnityAction_BeginInvoke_m6577_ParameterInfos[] = 
{
	{"callback", 0, 134219481, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 1, 134219482, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
MethodInfo UnityAction_BeginInvoke_m6577_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m6577/* method */
	, &UnityAction_t341_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t341_UnityAction_BeginInvoke_m6577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_t341_UnityAction_EndInvoke_m6578_ParameterInfos[] = 
{
	{"result", 0, 134219483, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_EndInvoke_m6578_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m6578/* method */
	, &UnityAction_t341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_t341_UnityAction_EndInvoke_m6578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_t341_MethodInfos[] =
{
	&UnityAction__ctor_m2452_MethodInfo,
	&UnityAction_Invoke_m2300_MethodInfo,
	&UnityAction_BeginInvoke_m6577_MethodInfo,
	&UnityAction_EndInvoke_m6578_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
static MethodInfo* UnityAction_t341_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_Invoke_m2300_MethodInfo,
	&UnityAction_BeginInvoke_m6577_MethodInfo,
	&UnityAction_EndInvoke_m6578_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_t341_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_t341_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_t341;
TypeInfo UnityAction_t341_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t341_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_t341_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_t341_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_t341_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_t341_0_0_0/* byval_arg */
	, &UnityAction_t341_1_0_0/* this_arg */
	, UnityAction_t341_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t341/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t341)/* instance_size */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`1
#include "UnityEngine_UnityEngine_Events_UnityAction_1.h"
extern Il2CppGenericContainer UnityAction_1_t1151_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t1151_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_1_t1151_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_1_t1151_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_1_t1151_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t1151_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_1_t1151_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_1_t1151_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_1_t1151_il2cpp_TypeInfo, 1, 0, UnityAction_1_t1151_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t1151_UnityAction_1__ctor_m6784_ParameterInfos[] = 
{
	{"object", 0, 134219484, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219485, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m6784_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1151_UnityAction_1__ctor_m6784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1151_gp_0_0_0_0;
extern Il2CppType UnityAction_1_t1151_gp_0_0_0_0;
static ParameterInfo UnityAction_1_t1151_UnityAction_1_Invoke_m6785_ParameterInfos[] = 
{
	{"arg0", 0, 134219486, &EmptyCustomAttributesCache, &UnityAction_1_t1151_gp_0_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m6785_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1151_UnityAction_1_Invoke_m6785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1151_gp_0_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t1151_UnityAction_1_BeginInvoke_m6786_ParameterInfos[] = 
{
	{"arg0", 0, 134219487, &EmptyCustomAttributesCache, &UnityAction_1_t1151_gp_0_0_0_0},
	{"callback", 1, 134219488, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134219489, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m6786_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1151_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1151_UnityAction_1_BeginInvoke_m6786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t1151_UnityAction_1_EndInvoke_m6787_ParameterInfos[] = 
{
	{"result", 0, 134219490, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m6787_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1151_UnityAction_1_EndInvoke_m6787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_1_t1151_MethodInfos[] =
{
	&UnityAction_1__ctor_m6784_MethodInfo,
	&UnityAction_1_Invoke_m6785_MethodInfo,
	&UnityAction_1_BeginInvoke_m6786_MethodInfo,
	&UnityAction_1_EndInvoke_m6787_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t1151_0_0_0;
extern Il2CppType UnityAction_1_t1151_1_0_0;
struct UnityAction_1_t1151;
TypeInfo UnityAction_1_t1151_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t1151_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t1151_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_1_t1151_0_0_0/* byval_arg */
	, &UnityAction_1_t1151_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_1_t1151_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`2
#include "UnityEngine_UnityEngine_Events_UnityAction_2.h"
extern Il2CppGenericContainer UnityAction_2_t1152_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t1152_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_2_t1152_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_2_t1152_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_2_t1152_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_2_t1152_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_2_t1152_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_2_t1152_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t1152_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t1152_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_2_t1152_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_2_t1152_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_2_t1152_il2cpp_TypeInfo, 2, 0, UnityAction_2_t1152_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_2_t1152_UnityAction_2__ctor_m6788_ParameterInfos[] = 
{
	{"object", 0, 134219491, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219492, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_2__ctor_m6788_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t1152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1152_UnityAction_2__ctor_m6788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_2_t1152_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1152_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1152_gp_1_0_0_0;
extern Il2CppType UnityAction_2_t1152_gp_1_0_0_0;
static ParameterInfo UnityAction_2_t1152_UnityAction_2_Invoke_m6789_ParameterInfos[] = 
{
	{"arg0", 0, 134219493, &EmptyCustomAttributesCache, &UnityAction_2_t1152_gp_0_0_0_0},
	{"arg1", 1, 134219494, &EmptyCustomAttributesCache, &UnityAction_2_t1152_gp_1_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
MethodInfo UnityAction_2_Invoke_m6789_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1152_UnityAction_2_Invoke_m6789_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_2_t1152_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1152_gp_1_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_2_t1152_UnityAction_2_BeginInvoke_m6790_ParameterInfos[] = 
{
	{"arg0", 0, 134219495, &EmptyCustomAttributesCache, &UnityAction_2_t1152_gp_0_0_0_0},
	{"arg1", 1, 134219496, &EmptyCustomAttributesCache, &UnityAction_2_t1152_gp_1_0_0_0},
	{"callback", 2, 134219497, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 3, 134219498, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
MethodInfo UnityAction_2_BeginInvoke_m6790_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1152_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1152_UnityAction_2_BeginInvoke_m6790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_2_t1152_UnityAction_2_EndInvoke_m6791_ParameterInfos[] = 
{
	{"result", 0, 134219499, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_2_EndInvoke_m6791_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1152_UnityAction_2_EndInvoke_m6791_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_2_t1152_MethodInfos[] =
{
	&UnityAction_2__ctor_m6788_MethodInfo,
	&UnityAction_2_Invoke_m6789_MethodInfo,
	&UnityAction_2_BeginInvoke_m6790_MethodInfo,
	&UnityAction_2_EndInvoke_m6791_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_2_t1152_0_0_0;
extern Il2CppType UnityAction_2_t1152_1_0_0;
struct UnityAction_2_t1152;
TypeInfo UnityAction_2_t1152_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t1152_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_2_t1152_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_2_t1152_0_0_0/* byval_arg */
	, &UnityAction_2_t1152_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_2_t1152_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`3
#include "UnityEngine_UnityEngine_Events_UnityAction_3.h"
extern Il2CppGenericContainer UnityAction_3_t1153_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t1153_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1153_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1153_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_3_t1153_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1153_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1153_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityAction_3_t1153_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1153_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1153_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_3_t1153_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t1153_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1153_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1153_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_3_t1153_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_3_t1153_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_3_t1153_il2cpp_TypeInfo, 3, 0, UnityAction_3_t1153_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_3_t1153_UnityAction_3__ctor_m6792_ParameterInfos[] = 
{
	{"object", 0, 134219500, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219501, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_3__ctor_m6792_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t1153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1153_UnityAction_3__ctor_m6792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_3_t1153_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_2_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_2_0_0_0;
static ParameterInfo UnityAction_3_t1153_UnityAction_3_Invoke_m6793_ParameterInfos[] = 
{
	{"arg0", 0, 134219502, &EmptyCustomAttributesCache, &UnityAction_3_t1153_gp_0_0_0_0},
	{"arg1", 1, 134219503, &EmptyCustomAttributesCache, &UnityAction_3_t1153_gp_1_0_0_0},
	{"arg2", 2, 134219504, &EmptyCustomAttributesCache, &UnityAction_3_t1153_gp_2_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
MethodInfo UnityAction_3_Invoke_m6793_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1153_UnityAction_3_Invoke_m6793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_3_t1153_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1153_gp_2_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_3_t1153_UnityAction_3_BeginInvoke_m6794_ParameterInfos[] = 
{
	{"arg0", 0, 134219505, &EmptyCustomAttributesCache, &UnityAction_3_t1153_gp_0_0_0_0},
	{"arg1", 1, 134219506, &EmptyCustomAttributesCache, &UnityAction_3_t1153_gp_1_0_0_0},
	{"arg2", 2, 134219507, &EmptyCustomAttributesCache, &UnityAction_3_t1153_gp_2_0_0_0},
	{"callback", 3, 134219508, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 4, 134219509, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
MethodInfo UnityAction_3_BeginInvoke_m6794_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1153_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1153_UnityAction_3_BeginInvoke_m6794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_3_t1153_UnityAction_3_EndInvoke_m6795_ParameterInfos[] = 
{
	{"result", 0, 134219510, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_3_EndInvoke_m6795_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1153_UnityAction_3_EndInvoke_m6795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_3_t1153_MethodInfos[] =
{
	&UnityAction_3__ctor_m6792_MethodInfo,
	&UnityAction_3_Invoke_m6793_MethodInfo,
	&UnityAction_3_BeginInvoke_m6794_MethodInfo,
	&UnityAction_3_EndInvoke_m6795_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_3_t1153_0_0_0;
extern Il2CppType UnityAction_3_t1153_1_0_0;
struct UnityAction_3_t1153;
TypeInfo UnityAction_3_t1153_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t1153_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_3_t1153_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_3_t1153_0_0_0/* byval_arg */
	, &UnityAction_3_t1153_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_3_t1153_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`4
#include "UnityEngine_UnityEngine_Events_UnityAction_4.h"
extern Il2CppGenericContainer UnityAction_4_t1154_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t1154_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1154_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1154_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1154_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1154_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1154_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1154_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1154_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1154_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1154_gp_T3_3_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1154_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1154_Il2CppGenericContainer, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_4_t1154_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t1154_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1154_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1154_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1154_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_4_t1154_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_4_t1154_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_4_t1154_il2cpp_TypeInfo, 4, 0, UnityAction_4_t1154_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_4_t1154_UnityAction_4__ctor_m6796_ParameterInfos[] = 
{
	{"object", 0, 134219511, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219512, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_4__ctor_m6796_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1154_UnityAction_4__ctor_m6796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_4_t1154_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_3_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_3_0_0_0;
static ParameterInfo UnityAction_4_t1154_UnityAction_4_Invoke_m6797_ParameterInfos[] = 
{
	{"arg0", 0, 134219513, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_0_0_0_0},
	{"arg1", 1, 134219514, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_1_0_0_0},
	{"arg2", 2, 134219515, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_2_0_0_0},
	{"arg3", 3, 134219516, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_3_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
MethodInfo UnityAction_4_Invoke_m6797_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1154_UnityAction_4_Invoke_m6797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_4_t1154_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1154_gp_3_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_4_t1154_UnityAction_4_BeginInvoke_m6798_ParameterInfos[] = 
{
	{"arg0", 0, 134219517, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_0_0_0_0},
	{"arg1", 1, 134219518, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_1_0_0_0},
	{"arg2", 2, 134219519, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_2_0_0_0},
	{"arg3", 3, 134219520, &EmptyCustomAttributesCache, &UnityAction_4_t1154_gp_3_0_0_0},
	{"callback", 4, 134219521, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 5, 134219522, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
MethodInfo UnityAction_4_BeginInvoke_m6798_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1154_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1154_UnityAction_4_BeginInvoke_m6798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_4_t1154_UnityAction_4_EndInvoke_m6799_ParameterInfos[] = 
{
	{"result", 0, 134219523, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_4_EndInvoke_m6799_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1154_UnityAction_4_EndInvoke_m6799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_4_t1154_MethodInfos[] =
{
	&UnityAction_4__ctor_m6796_MethodInfo,
	&UnityAction_4_Invoke_m6797_MethodInfo,
	&UnityAction_4_BeginInvoke_m6798_MethodInfo,
	&UnityAction_4_EndInvoke_m6799_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_4_t1154_0_0_0;
extern Il2CppType UnityAction_4_t1154_1_0_0;
struct UnityAction_4_t1154;
TypeInfo UnityAction_4_t1154_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t1154_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_4_t1154_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_4_t1154_0_0_0/* byval_arg */
	, &UnityAction_4_t1154_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_4_t1154_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
