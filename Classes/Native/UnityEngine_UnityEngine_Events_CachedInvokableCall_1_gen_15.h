﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_11.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>
struct CachedInvokableCall_1_t2875  : public InvokableCall_1_t2876
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
