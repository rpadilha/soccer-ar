﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t2308;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t2239  : public MulticastDelegate_t373
{
};
