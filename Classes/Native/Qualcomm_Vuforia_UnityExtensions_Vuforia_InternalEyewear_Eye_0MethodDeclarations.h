﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewear/EyewearCalibrationReading
struct EyewearCalibrationReading_t591;
struct EyewearCalibrationReading_t591_marshaled;

void EyewearCalibrationReading_t591_marshal(const EyewearCalibrationReading_t591& unmarshaled, EyewearCalibrationReading_t591_marshaled& marshaled);
void EyewearCalibrationReading_t591_marshal_back(const EyewearCalibrationReading_t591_marshaled& marshaled, EyewearCalibrationReading_t591& unmarshaled);
void EyewearCalibrationReading_t591_marshal_cleanup(EyewearCalibrationReading_t591_marshaled& marshaled);
