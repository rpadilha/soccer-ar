﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA512
struct SHA512_t2172;

// System.Void System.Security.Cryptography.SHA512::.ctor()
 void SHA512__ctor_m12271 (SHA512_t2172 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
