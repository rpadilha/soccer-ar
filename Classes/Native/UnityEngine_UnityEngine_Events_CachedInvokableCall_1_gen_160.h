﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_162.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>
struct CachedInvokableCall_1_t4796  : public InvokableCall_1_t4797
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
