﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.WordFilterMode>
struct InternalEnumerator_1_t4602;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WordFilterMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27839 (InternalEnumerator_1_t4602 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordFilterMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27840 (InternalEnumerator_1_t4602 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordFilterMode>::Dispose()
 void InternalEnumerator_1_Dispose_m27841 (InternalEnumerator_1_t4602 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordFilterMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27842 (InternalEnumerator_1_t4602 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.WordFilterMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m27843 (InternalEnumerator_1_t4602 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
