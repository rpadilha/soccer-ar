﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t1348;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.Protocol.Tls.ClientSessionCache
struct ClientSessionCache_t1656  : public Object_t
{
};
struct ClientSessionCache_t1656_StaticFields{
	// System.Collections.Hashtable Mono.Security.Protocol.Tls.ClientSessionCache::cache
	Hashtable_t1348 * ___cache_0;
	// System.Object Mono.Security.Protocol.Tls.ClientSessionCache::locker
	Object_t * ___locker_1;
};
