﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.CameraDevice/CameraDirection
struct CameraDirection_t601 
{
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;
};
