﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t63;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t648;

// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
 bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
 void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397 (WebCamAbstractBehaviour_t63 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
 String_t* WebCamAbstractBehaviour_get_DeviceName_m4398 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
 void WebCamAbstractBehaviour_set_DeviceName_m4399 (WebCamAbstractBehaviour_t63 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
 bool WebCamAbstractBehaviour_get_FlipHorizontally_m4400 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
 void WebCamAbstractBehaviour_set_FlipHorizontally_m4401 (WebCamAbstractBehaviour_t63 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
 bool WebCamAbstractBehaviour_get_TurnOffWebCam_m4402 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
 void WebCamAbstractBehaviour_set_TurnOffWebCam_m4403 (WebCamAbstractBehaviour_t63 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
 bool WebCamAbstractBehaviour_get_IsPlaying_m4404 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
 bool WebCamAbstractBehaviour_IsWebCamUsed_m4405 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
 WebCamImpl_t648 * WebCamAbstractBehaviour_get_ImplementationClass_m4406 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
 void WebCamAbstractBehaviour_InitCamera_m4407 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
 bool WebCamAbstractBehaviour_CheckNativePluginSupport_m4408 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
 void WebCamAbstractBehaviour_OnLevelWasLoaded_m4409 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
 void WebCamAbstractBehaviour_OnDestroy_m4410 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
 void WebCamAbstractBehaviour_Update_m4411 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
 int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
 void WebCamAbstractBehaviour__ctor_m514 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
