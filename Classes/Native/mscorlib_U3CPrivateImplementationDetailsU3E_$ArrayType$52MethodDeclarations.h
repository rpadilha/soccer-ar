﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$52
struct $ArrayType$52_t2334;
struct $ArrayType$52_t2334_marshaled;

void $ArrayType$52_t2334_marshal(const $ArrayType$52_t2334& unmarshaled, $ArrayType$52_t2334_marshaled& marshaled);
void $ArrayType$52_t2334_marshal_back(const $ArrayType$52_t2334_marshaled& marshaled, $ArrayType$52_t2334& unmarshaled);
void $ArrayType$52_t2334_marshal_cleanup($ArrayType$52_t2334_marshaled& marshaled);
