﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t63;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.WebCamAbstractBehaviour>
struct UnityAction_1_t4598  : public MulticastDelegate_t373
{
};
