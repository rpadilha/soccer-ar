﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.VirtualButton/Sensitivity>
struct InternalEnumerator_1_t4436;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButton/Sensitivity>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26626 (InternalEnumerator_1_t4436 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButton/Sensitivity>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26627 (InternalEnumerator_1_t4436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButton/Sensitivity>::Dispose()
 void InternalEnumerator_1_Dispose_m26628 (InternalEnumerator_1_t4436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButton/Sensitivity>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26629 (InternalEnumerator_1_t4436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButton/Sensitivity>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26630 (InternalEnumerator_1_t4436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
