﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderExceptionFallback
struct DecoderExceptionFallback_t2197;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2195;
// System.Object
struct Object_t;

// System.Void System.Text.DecoderExceptionFallback::.ctor()
 void DecoderExceptionFallback__ctor_m12404 (DecoderExceptionFallback_t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderExceptionFallback::CreateFallbackBuffer()
 DecoderFallbackBuffer_t2195 * DecoderExceptionFallback_CreateFallbackBuffer_m12405 (DecoderExceptionFallback_t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderExceptionFallback::Equals(System.Object)
 bool DecoderExceptionFallback_Equals_m12406 (DecoderExceptionFallback_t2197 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderExceptionFallback::GetHashCode()
 int32_t DecoderExceptionFallback_GetHashCode_m12407 (DecoderExceptionFallback_t2197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
