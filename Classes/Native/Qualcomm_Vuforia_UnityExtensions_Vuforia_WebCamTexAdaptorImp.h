﻿#pragma once
#include <stdint.h>
// UnityEngine.WebCamTexture
struct WebCamTexture_t728;
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t729  : public WebCamTexAdaptor_t672
{
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptorImpl::mWebCamTexture
	WebCamTexture_t728 * ___mWebCamTexture_0;
};
