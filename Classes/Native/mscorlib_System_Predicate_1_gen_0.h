﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t418;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t420  : public MulticastDelegate_t373
{
};
