﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.Assertion
struct Assertion_t1495;
// System.Text.RegularExpressions.Syntax.Expression
struct Expression_t1496;

// System.Void System.Text.RegularExpressions.Syntax.Assertion::.ctor()
 void Assertion__ctor_m7631 (Assertion_t1495 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_TrueExpression()
 Expression_t1496 * Assertion_get_TrueExpression_m7632 (Assertion_t1495 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_TrueExpression(System.Text.RegularExpressions.Syntax.Expression)
 void Assertion_set_TrueExpression_m7633 (Assertion_t1495 * __this, Expression_t1496 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_FalseExpression()
 Expression_t1496 * Assertion_get_FalseExpression_m7634 (Assertion_t1495 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_FalseExpression(System.Text.RegularExpressions.Syntax.Expression)
 void Assertion_set_FalseExpression_m7635 (Assertion_t1495 * __this, Expression_t1496 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.Assertion::GetWidth(System.Int32&,System.Int32&)
 void Assertion_GetWidth_m7636 (Assertion_t1495 * __this, int32_t* ___min, int32_t* ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
