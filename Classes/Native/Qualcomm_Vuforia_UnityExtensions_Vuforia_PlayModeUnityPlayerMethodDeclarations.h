﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t161;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
 void PlayModeUnityPlayer_LoadNativeLibraries_m2877 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
 void PlayModeUnityPlayer_InitializePlatform_m2878 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
 void PlayModeUnityPlayer_InitializeSurface_m2879 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
 int32_t PlayModeUnityPlayer_Start_m2880 (PlayModeUnityPlayer_t161 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Update()
 void PlayModeUnityPlayer_Update_m2881 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
 void PlayModeUnityPlayer_Dispose_m2882 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
 void PlayModeUnityPlayer_OnPause_m2883 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
 void PlayModeUnityPlayer_OnResume_m2884 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
 void PlayModeUnityPlayer_OnDestroy_m2885 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
 void PlayModeUnityPlayer__ctor_m417 (PlayModeUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
