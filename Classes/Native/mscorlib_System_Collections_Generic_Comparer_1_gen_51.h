﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Rigidbody2D>
struct Comparer_1_t4840;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Rigidbody2D>
struct Comparer_1_t4840  : public Object_t
{
};
struct Comparer_1_t4840_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Rigidbody2D>::_default
	Comparer_1_t4840 * ____default_0;
};
