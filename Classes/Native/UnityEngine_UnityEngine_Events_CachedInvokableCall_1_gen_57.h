﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_53.h"
// UnityEngine.Events.CachedInvokableCall`1<Jump_Button>
struct CachedInvokableCall_1_t3110  : public InvokableCall_1_t3111
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Jump_Button>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
