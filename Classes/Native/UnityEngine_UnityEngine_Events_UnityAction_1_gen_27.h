﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t49;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
struct UnityAction_1_t2930  : public MulticastDelegate_t373
{
};
