﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>
struct InternalEnumerator_1_t3672;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m19971 (InternalEnumerator_1_t3672 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19972 (InternalEnumerator_1_t3672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>::Dispose()
 void InternalEnumerator_1_Dispose_m19973 (InternalEnumerator_1_t3672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m19974 (InternalEnumerator_1_t3672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m19975 (InternalEnumerator_1_t3672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
