﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARUnity/InitError>
struct InternalEnumerator_1_t4482;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARUnity/InitError>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26937 (InternalEnumerator_1_t4482 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARUnity/InitError>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26938 (InternalEnumerator_1_t4482 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARUnity/InitError>::Dispose()
 void InternalEnumerator_1_Dispose_m26939 (InternalEnumerator_1_t4482 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARUnity/InitError>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26940 (InternalEnumerator_1_t4482 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARUnity/InitError>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26941 (InternalEnumerator_1_t4482 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
