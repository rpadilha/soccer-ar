﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1636;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.KeyedHashAlgorithm::.ctor()
 void KeyedHashAlgorithm__ctor_m9048 (KeyedHashAlgorithm_t1636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Finalize()
 void KeyedHashAlgorithm_Finalize_m9049 (KeyedHashAlgorithm_t1636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::get_Key()
 ByteU5BU5D_t653* KeyedHashAlgorithm_get_Key_m12112 (KeyedHashAlgorithm_t1636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::set_Key(System.Byte[])
 void KeyedHashAlgorithm_set_Key_m12113 (KeyedHashAlgorithm_t1636 * __this, ByteU5BU5D_t653* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Dispose(System.Boolean)
 void KeyedHashAlgorithm_Dispose_m9050 (KeyedHashAlgorithm_t1636 * __this, bool ___disposing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::ZeroizeKey()
 void KeyedHashAlgorithm_ZeroizeKey_m12114 (KeyedHashAlgorithm_t1636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
