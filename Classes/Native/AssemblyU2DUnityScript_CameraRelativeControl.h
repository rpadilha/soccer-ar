﻿#pragma once
#include <stdint.h>
// Joystick
struct Joystick_t208;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.CharacterController
struct CharacterController_t209;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// CameraRelativeControl
struct CameraRelativeControl_t210  : public MonoBehaviour_t10
{
	// Joystick CameraRelativeControl::moveJoystick
	Joystick_t208 * ___moveJoystick_2;
	// Joystick CameraRelativeControl::rotateJoystick
	Joystick_t208 * ___rotateJoystick_3;
	// UnityEngine.Transform CameraRelativeControl::cameraPivot
	Transform_t74 * ___cameraPivot_4;
	// UnityEngine.Transform CameraRelativeControl::cameraTransform
	Transform_t74 * ___cameraTransform_5;
	// System.Single CameraRelativeControl::speed
	float ___speed_6;
	// System.Single CameraRelativeControl::jumpSpeed
	float ___jumpSpeed_7;
	// System.Single CameraRelativeControl::inAirMultiplier
	float ___inAirMultiplier_8;
	// UnityEngine.Vector2 CameraRelativeControl::rotationSpeed
	Vector2_t99  ___rotationSpeed_9;
	// UnityEngine.Transform CameraRelativeControl::thisTransform
	Transform_t74 * ___thisTransform_10;
	// UnityEngine.CharacterController CameraRelativeControl::character
	CharacterController_t209 * ___character_11;
	// UnityEngine.Vector3 CameraRelativeControl::velocity
	Vector3_t73  ___velocity_12;
	// System.Boolean CameraRelativeControl::canJump
	bool ___canJump_13;
};
