﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewear
struct InternalEyewear_t623;
// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t587;
// Vuforia.InternalEyewearUserCalibrator
struct InternalEyewearUserCalibrator_t589;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"

// Vuforia.InternalEyewear Vuforia.InternalEyewear::get_Instance()
 InternalEyewear_t623 * InternalEyewear_get_Instance_m2907 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewear::IsSupportedDeviceDetected()
 bool InternalEyewear_IsSupportedDeviceDetected_m2908 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewear::IsSeeThru()
 bool InternalEyewear_IsSeeThru_m2909 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.InternalEyewear::GetScreenOrientation()
 int32_t InternalEyewear_GetScreenOrientation_m2910 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewear::IsStereoCapable()
 bool InternalEyewear_IsStereoCapable_m2911 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewear::IsStereoEnabled()
 bool InternalEyewear_IsStereoEnabled_m2912 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewear::IsStereoGLOnly()
 bool InternalEyewear_IsStereoGLOnly_m2913 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewear::SetStereo(System.Boolean)
 bool InternalEyewear_SetStereo_m2914 (InternalEyewear_t623 * __this, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.InternalEyewear::GetDefaultSceneScale()
 float InternalEyewear_GetDefaultSceneScale_m2915 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::getProfileManager()
 InternalEyewearCalibrationProfileManager_t587 * InternalEyewear_getProfileManager_m2916 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::getCalibrator()
 InternalEyewearUserCalibrator_t589 * InternalEyewear_getCalibrator_m2917 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.InternalEyewear::GetProjectionMatrix(Vuforia.InternalEyewear/EyeID,UnityEngine.ScreenOrientation)
 Matrix4x4_t176  InternalEyewear_GetProjectionMatrix_m2918 (InternalEyewear_t623 * __this, int32_t ___eyeID, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.InternalEyewear::GetProjectionMatrix(Vuforia.InternalEyewear/EyeID,System.Int32,UnityEngine.ScreenOrientation)
 Matrix4x4_t176  InternalEyewear_GetProjectionMatrix_m2919 (InternalEyewear_t623 * __this, int32_t ___eyeID, int32_t ___profileID, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.InternalEyewear::.ctor()
 void InternalEyewear__ctor_m2920 (InternalEyewear_t623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.InternalEyewear::.cctor()
 void InternalEyewear__cctor_m2921 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
