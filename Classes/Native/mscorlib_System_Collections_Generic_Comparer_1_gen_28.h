﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.DataSet>
struct Comparer_1_t4041;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.DataSet>
struct Comparer_1_t4041  : public Object_t
{
};
struct Comparer_1_t4041_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.DataSet>::_default
	Comparer_1_t4041 * ____default_0;
};
