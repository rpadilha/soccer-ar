﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializeField
struct SerializeField_t469;

// System.Void UnityEngine.SerializeField::.ctor()
 void SerializeField__ctor_m2084 (SerializeField_t469 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
