﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Quaternion
struct Quaternion_t108;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
 void Quaternion__ctor_m5965 (Quaternion_t108 * __this, float ___x, float ___y, float ___z, float ___w, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
 Quaternion_t108  Quaternion_get_identity_m717 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
 float Quaternion_Dot_m5966 (Object_t * __this/* static, unused */, Quaternion_t108  ___a, Quaternion_t108  ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
 Quaternion_t108  Quaternion_AngleAxis_m839 (Object_t * __this/* static, unused */, float ___angle, Vector3_t73  ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&)
 Quaternion_t108  Quaternion_INTERNAL_CALL_AngleAxis_m5967 (Object_t * __this/* static, unused */, float ___angle, Vector3_t73 * ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
 void Quaternion_ToAngleAxis_m4646 (Quaternion_t108 * __this, float* ___angle, Vector3_t73 * ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
 Quaternion_t108  Quaternion_LookRotation_m711 (Object_t * __this/* static, unused */, Vector3_t73  ___forward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
 Quaternion_t108  Quaternion_INTERNAL_CALL_LookRotation_m5968 (Object_t * __this/* static, unused */, Vector3_t73 * ___forward, Vector3_t73 * ___upwards, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
 Quaternion_t108  Quaternion_Inverse_m2589 (Object_t * __this/* static, unused */, Quaternion_t108  ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
 Quaternion_t108  Quaternion_INTERNAL_CALL_Inverse_m5969 (Object_t * __this/* static, unused */, Quaternion_t108 * ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
 String_t* Quaternion_ToString_m5970 (Quaternion_t108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
 float Quaternion_Angle_m842 (Object_t * __this/* static, unused */, Quaternion_t108  ___a, Quaternion_t108  ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
 Vector3_t73  Quaternion_get_eulerAngles_m840 (Quaternion_t108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
 Quaternion_t108  Quaternion_Euler_m722 (Object_t * __this/* static, unused */, float ___x, float ___y, float ___z, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
 Quaternion_t108  Quaternion_Euler_m5971 (Object_t * __this/* static, unused */, Vector3_t73  ___euler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
 Vector3_t73  Quaternion_Internal_ToEulerRad_m5972 (Object_t * __this/* static, unused */, Quaternion_t108  ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
 Vector3_t73  Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5973 (Object_t * __this/* static, unused */, Quaternion_t108 * ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
 Quaternion_t108  Quaternion_Internal_FromEulerRad_m5974 (Object_t * __this/* static, unused */, Vector3_t73  ___euler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
 Quaternion_t108  Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5975 (Object_t * __this/* static, unused */, Vector3_t73 * ___euler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
 void Quaternion_Internal_ToAxisAngleRad_m5976 (Object_t * __this/* static, unused */, Quaternion_t108  ___q, Vector3_t73 * ___axis, float* ___angle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
 void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m5977 (Object_t * __this/* static, unused */, Quaternion_t108 * ___q, Vector3_t73 * ___axis, float* ___angle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
 int32_t Quaternion_GetHashCode_m5978 (Quaternion_t108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
 bool Quaternion_Equals_m5979 (Quaternion_t108 * __this, Object_t * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
 Quaternion_t108  Quaternion_op_Multiply_m705 (Object_t * __this/* static, unused */, Quaternion_t108  ___lhs, Quaternion_t108  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
 Vector3_t73  Quaternion_op_Multiply_m2347 (Object_t * __this/* static, unused */, Quaternion_t108  ___rotation, Vector3_t73  ___point, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
 bool Quaternion_op_Inequality_m2500 (Object_t * __this/* static, unused */, Quaternion_t108  ___lhs, Quaternion_t108  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
