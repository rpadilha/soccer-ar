﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Vuforia.BackgroundPlaneBehaviour[]
// Vuforia.BackgroundPlaneBehaviour[]
struct BackgroundPlaneBehaviourU5BU5D_t5358  : public Array_t
{
};
// Vuforia.CloudRecoBehaviour[]
// Vuforia.CloudRecoBehaviour[]
struct CloudRecoBehaviourU5BU5D_t5359  : public Array_t
{
};
// Vuforia.CylinderTargetBehaviour[]
// Vuforia.CylinderTargetBehaviour[]
struct CylinderTargetBehaviourU5BU5D_t5360  : public Array_t
{
};
// Vuforia.DataSetLoadBehaviour[]
// Vuforia.DataSetLoadBehaviour[]
struct DataSetLoadBehaviourU5BU5D_t5361  : public Array_t
{
};
// Vuforia.DefaultInitializationErrorHandler[]
// Vuforia.DefaultInitializationErrorHandler[]
struct DefaultInitializationErrorHandlerU5BU5D_t5362  : public Array_t
{
};
// Vuforia.DefaultSmartTerrainEventHandler[]
// Vuforia.DefaultSmartTerrainEventHandler[]
struct DefaultSmartTerrainEventHandlerU5BU5D_t5363  : public Array_t
{
};
// Vuforia.DefaultTrackableEventHandler[]
// Vuforia.DefaultTrackableEventHandler[]
struct DefaultTrackableEventHandlerU5BU5D_t5364  : public Array_t
{
};
// Vuforia.GLErrorHandler[]
// Vuforia.GLErrorHandler[]
struct GLErrorHandlerU5BU5D_t5365  : public Array_t
{
};
struct GLErrorHandlerU5BU5D_t5365_StaticFields{
};
// Vuforia.HideExcessAreaBehaviour[]
// Vuforia.HideExcessAreaBehaviour[]
struct HideExcessAreaBehaviourU5BU5D_t5366  : public Array_t
{
};
// Vuforia.ImageTargetBehaviour[]
// Vuforia.ImageTargetBehaviour[]
struct ImageTargetBehaviourU5BU5D_t5367  : public Array_t
{
};
// Vuforia.ComponentFactoryStarterBehaviour[]
// Vuforia.ComponentFactoryStarterBehaviour[]
struct ComponentFactoryStarterBehaviourU5BU5D_t5368  : public Array_t
{
};
// Vuforia.KeepAliveBehaviour[]
// Vuforia.KeepAliveBehaviour[]
struct KeepAliveBehaviourU5BU5D_t5369  : public Array_t
{
};
// Vuforia.MarkerBehaviour[]
// Vuforia.MarkerBehaviour[]
struct MarkerBehaviourU5BU5D_t5370  : public Array_t
{
};
// Vuforia.MaskOutBehaviour[]
// Vuforia.MaskOutBehaviour[]
struct MaskOutBehaviourU5BU5D_t5371  : public Array_t
{
};
// Vuforia.MultiTargetBehaviour[]
// Vuforia.MultiTargetBehaviour[]
struct MultiTargetBehaviourU5BU5D_t5372  : public Array_t
{
};
// Vuforia.ObjectTargetBehaviour[]
// Vuforia.ObjectTargetBehaviour[]
struct ObjectTargetBehaviourU5BU5D_t5373  : public Array_t
{
};
// Vuforia.PropBehaviour[]
// Vuforia.PropBehaviour[]
struct PropBehaviourU5BU5D_t5374  : public Array_t
{
};
// Vuforia.QCARBehaviour[]
// Vuforia.QCARBehaviour[]
struct QCARBehaviourU5BU5D_t5375  : public Array_t
{
};
// Vuforia.ReconstructionBehaviour[]
// Vuforia.ReconstructionBehaviour[]
struct ReconstructionBehaviourU5BU5D_t5376  : public Array_t
{
};
// Vuforia.ReconstructionFromTargetBehaviour[]
// Vuforia.ReconstructionFromTargetBehaviour[]
struct ReconstructionFromTargetBehaviourU5BU5D_t5377  : public Array_t
{
};
// Vuforia.SmartTerrainTrackerBehaviour[]
// Vuforia.SmartTerrainTrackerBehaviour[]
struct SmartTerrainTrackerBehaviourU5BU5D_t5378  : public Array_t
{
};
// Vuforia.SurfaceBehaviour[]
// Vuforia.SurfaceBehaviour[]
struct SurfaceBehaviourU5BU5D_t5379  : public Array_t
{
};
// Vuforia.TextRecoBehaviour[]
// Vuforia.TextRecoBehaviour[]
struct TextRecoBehaviourU5BU5D_t5380  : public Array_t
{
};
// Vuforia.TurnOffBehaviour[]
// Vuforia.TurnOffBehaviour[]
struct TurnOffBehaviourU5BU5D_t5381  : public Array_t
{
};
// Vuforia.TurnOffWordBehaviour[]
// Vuforia.TurnOffWordBehaviour[]
struct TurnOffWordBehaviourU5BU5D_t5382  : public Array_t
{
};
// Vuforia.UserDefinedTargetBuildingBehaviour[]
// Vuforia.UserDefinedTargetBuildingBehaviour[]
struct UserDefinedTargetBuildingBehaviourU5BU5D_t5383  : public Array_t
{
};
// Vuforia.VideoBackgroundBehaviour[]
// Vuforia.VideoBackgroundBehaviour[]
struct VideoBackgroundBehaviourU5BU5D_t5384  : public Array_t
{
};
// Vuforia.VideoTextureRenderer[]
// Vuforia.VideoTextureRenderer[]
struct VideoTextureRendererU5BU5D_t5385  : public Array_t
{
};
// Vuforia.VirtualButtonBehaviour[]
// Vuforia.VirtualButtonBehaviour[]
struct VirtualButtonBehaviourU5BU5D_t5386  : public Array_t
{
};
// Vuforia.WebCamBehaviour[]
// Vuforia.WebCamBehaviour[]
struct WebCamBehaviourU5BU5D_t5387  : public Array_t
{
};
// Vuforia.WireframeBehaviour[]
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t177  : public Array_t
{
};
// Vuforia.WireframeTrackableEventHandler[]
// Vuforia.WireframeTrackableEventHandler[]
struct WireframeTrackableEventHandlerU5BU5D_t5388  : public Array_t
{
};
// Vuforia.WordBehaviour[]
// Vuforia.WordBehaviour[]
struct WordBehaviourU5BU5D_t5389  : public Array_t
{
};
// Area_Script[]
// Area_Script[]
struct Area_ScriptU5BU5D_t5390  : public Array_t
{
};
// Banda_Script[]
// Banda_Script[]
struct Banda_ScriptU5BU5D_t5391  : public Array_t
{
};
// Sphere[]
// Sphere[]
struct SphereU5BU5D_t5392  : public Array_t
{
};
// Camera_Script[]
// Camera_Script[]
struct Camera_ScriptU5BU5D_t5393  : public Array_t
{
};
// Corner_Script[]
// Corner_Script[]
struct Corner_ScriptU5BU5D_t5394  : public Array_t
{
};
// GoalKeeperJump[]
// GoalKeeperJump[]
struct GoalKeeperJumpU5BU5D_t5395  : public Array_t
{
};
// GoalKeeperJump_Down[]
// GoalKeeperJump_Down[]
struct GoalKeeperJump_DownU5BU5D_t5396  : public Array_t
{
};
// GoalKeeper_Script[]
// GoalKeeper_Script[]
struct GoalKeeper_ScriptU5BU5D_t5397  : public Array_t
{
};
// GoalKeeper_Script/GoalKeeper_State[]
// GoalKeeper_Script/GoalKeeper_State[]
struct GoalKeeper_StateU5BU5D_t5398  : public Array_t
{
};
// Goal_Script[]
// Goal_Script[]
struct Goal_ScriptU5BU5D_t5399  : public Array_t
{
};
// ChooseTeam[]
// ChooseTeam[]
struct ChooseTeamU5BU5D_t5400  : public Array_t
{
};
// ShieldMenu[]
// ShieldMenu[]
struct ShieldMenuU5BU5D_t87  : public Array_t
{
};
// ScoreHUD[]
// ScoreHUD[]
struct ScoreHUDU5BU5D_t5401  : public Array_t
{
};
// InGameState_Script[]
// InGameState_Script[]
struct InGameState_ScriptU5BU5D_t5402  : public Array_t
{
};
// ScorerTimeHUD[]
// ScorerTimeHUD[]
struct ScorerTimeHUDU5BU5D_t5403  : public Array_t
{
};
// SetShield[]
// SetShield[]
struct SetShieldU5BU5D_t5404  : public Array_t
{
};
// InGameState_Script/InGameState[]
// InGameState_Script/InGameState[]
struct InGameStateU5BU5D_t5405  : public Array_t
{
};
// Joystick_Script[]
// Joystick_Script[]
struct Joystick_ScriptU5BU5D_t3069  : public Array_t
{
};
struct Joystick_ScriptU5BU5D_t3069_StaticFields{
};
// Pass_Script[]
// Pass_Script[]
struct Pass_ScriptU5BU5D_t5406  : public Array_t
{
};
// Player_Script[]
// Player_Script[]
struct Player_ScriptU5BU5D_t5407  : public Array_t
{
};
// Player_Script/TypePlayer[]
// Player_Script/TypePlayer[]
struct TypePlayerU5BU5D_t5408  : public Array_t
{
};
// Player_Script/Player_State[]
// Player_Script/Player_State[]
struct Player_StateU5BU5D_t5409  : public Array_t
{
};
// Shoot_Script[]
// Shoot_Script[]
struct Shoot_ScriptU5BU5D_t5410  : public Array_t
{
};
// Sombra_Script[]
// Sombra_Script[]
struct Sombra_ScriptU5BU5D_t5411  : public Array_t
{
};
