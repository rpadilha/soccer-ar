﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t25;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.ComponentFactoryStarterBehaviour>
struct UnityAction_1_t2825  : public MulticastDelegate_t373
{
};
