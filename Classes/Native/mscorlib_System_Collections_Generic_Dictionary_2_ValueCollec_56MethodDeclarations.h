﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>
struct Enumerator_t4746;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1019;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m28799 (Enumerator_t4746 * __this, Dictionary_2_t1019 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28800 (Enumerator_t4746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>::Dispose()
 void Enumerator_Dispose_m28801 (Enumerator_t4746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>::MoveNext()
 bool Enumerator_MoveNext_m28802 (Enumerator_t4746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Int32>::get_Current()
 int32_t Enumerator_get_Current_m28803 (Enumerator_t4746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
