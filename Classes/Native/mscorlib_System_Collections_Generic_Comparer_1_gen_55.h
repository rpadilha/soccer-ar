﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>
struct Comparer_1_t5022;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>
struct Comparer_1_t5022  : public Object_t
{
};
struct Comparer_1_t5022_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>::_default
	Comparer_1_t5022 * ____default_0;
};
