﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
struct InternalEnumerator_1_t3599;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.Image/OriginHorizontal
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizontal.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m19499 (InternalEnumerator_1_t3599 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19500 (InternalEnumerator_1_t3599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::Dispose()
 void InternalEnumerator_1_Dispose_m19501 (InternalEnumerator_1_t3599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m19502 (InternalEnumerator_1_t3599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m19503 (InternalEnumerator_1_t3599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
