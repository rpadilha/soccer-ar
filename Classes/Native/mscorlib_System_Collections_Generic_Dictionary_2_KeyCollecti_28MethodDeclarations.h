﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>
struct KeyCollection_t4113;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t726;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t3891;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Type[]
struct TypeU5BU5D_t922;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void KeyCollection__ctor_m23557 (KeyCollection_t4113 * __this, Dictionary_2_t726 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m23558 (KeyCollection_t4113 * __this, Type_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Clear()
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m23559 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m23560 (KeyCollection_t4113 * __this, Type_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m23561 (KeyCollection_t4113 * __this, Type_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
 Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m23562 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void KeyCollection_System_Collections_ICollection_CopyTo_m23563 (KeyCollection_t4113 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m23564 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m23565 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
 bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m23566 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
 Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m23567 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::CopyTo(TKey[],System.Int32)
 void KeyCollection_CopyTo_m23568 (KeyCollection_t4113 * __this, TypeU5BU5D_t922* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::GetEnumerator()
 Enumerator_t4123  KeyCollection_GetEnumerator_m23569 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::get_Count()
 int32_t KeyCollection_get_Count_m23570 (KeyCollection_t4113 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
