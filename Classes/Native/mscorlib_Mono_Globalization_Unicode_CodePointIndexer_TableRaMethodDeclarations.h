﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.CodePointIndexer/TableRange
struct TableRange_t1781;

// System.Void Mono.Globalization.Unicode.CodePointIndexer/TableRange::.ctor(System.Int32,System.Int32,System.Int32)
 void TableRange__ctor_m9937 (TableRange_t1781 * __this, int32_t ___start, int32_t ___end, int32_t ___indexStart, MethodInfo* method) IL2CPP_METHOD_ATTR;
