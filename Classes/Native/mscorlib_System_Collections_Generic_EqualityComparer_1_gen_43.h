﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ILoadLevelEventHandler>
struct EqualityComparer_1_t4221;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ILoadLevelEventHandler>
struct EqualityComparer_1_t4221  : public Object_t
{
};
struct EqualityComparer_1_t4221_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ILoadLevelEventHandler>::_default
	EqualityComparer_1_t4221 * ____default_0;
};
