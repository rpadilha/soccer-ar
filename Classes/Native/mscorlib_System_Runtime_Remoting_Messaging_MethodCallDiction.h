﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t862;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct MethodCallDictionary_t2070  : public MethodDictionary_t2064
{
};
struct MethodCallDictionary_t2070_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallDictionary::InternalKeys
	StringU5BU5D_t862* ___InternalKeys_6;
};
