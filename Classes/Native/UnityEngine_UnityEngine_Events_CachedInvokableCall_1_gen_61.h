﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Joystick>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_57.h"
// UnityEngine.Events.CachedInvokableCall`1<Joystick>
struct CachedInvokableCall_1_t3129  : public InvokableCall_1_t3130
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Joystick>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
