﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/UpdateState
struct UpdateState_t775 
{
	// System.Int32 Vuforia.TargetFinder/UpdateState::value__
	int32_t ___value___1;
};
