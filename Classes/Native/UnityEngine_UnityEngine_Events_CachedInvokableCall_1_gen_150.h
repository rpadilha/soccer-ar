﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_152.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>
struct CachedInvokableCall_1_t4754  : public InvokableCall_1_t4755
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
