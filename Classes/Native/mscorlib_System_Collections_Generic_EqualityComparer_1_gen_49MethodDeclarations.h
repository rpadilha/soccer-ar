﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>
struct EqualityComparer_1_t4310;
// System.Object
struct Object_t;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t43;

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::.ctor()
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#define EqualityComparer_1__ctor_m25375(__this, method) (void)EqualityComparer_1__ctor_m14630_gshared((EqualityComparer_1_t2840 *)__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::.cctor()
#define EqualityComparer_1__cctor_m25376(__this/* static, unused */, method) (void)EqualityComparer_1__cctor_m14631_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25377(__this, ___obj, method) (int32_t)EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14632_gshared((EqualityComparer_1_t2840 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25378(__this, ___x, ___y, method) (bool)EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14633_gshared((EqualityComparer_1_t2840 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::get_Default()
#define EqualityComparer_1_get_Default_m25379(__this/* static, unused */, method) (EqualityComparer_1_t4310 *)EqualityComparer_1_get_Default_m14634_gshared((Object_t *)__this/* static, unused */, method)
