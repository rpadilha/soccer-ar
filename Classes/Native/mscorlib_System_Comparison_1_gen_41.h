﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t16;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Surface>
struct Comparison_1_t4323  : public MulticastDelegate_t373
{
};
