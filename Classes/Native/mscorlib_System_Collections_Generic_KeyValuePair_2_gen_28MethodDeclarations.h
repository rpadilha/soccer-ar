﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct KeyValuePair_2_t5058;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m30640 (KeyValuePair_2_t5058 * __this, String_t* ___key, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
 String_t* KeyValuePair_2_get_Key_m30641 (KeyValuePair_2_t5058 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m30642 (KeyValuePair_2_t5058 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
 bool KeyValuePair_2_get_Value_m30643 (KeyValuePair_2_t5058 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m30644 (KeyValuePair_2_t5058 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
 String_t* KeyValuePair_2_ToString_m30645 (KeyValuePair_2_t5058 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
