﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t653;
// Mono.Security.Protocol.Tls.CipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuite.h"
// Mono.Security.Protocol.Tls.TlsCipherSuite
struct TlsCipherSuite_t1645  : public CipherSuite_t1643
{
	// System.Byte[] Mono.Security.Protocol.Tls.TlsCipherSuite::header
	ByteU5BU5D_t653* ___header_21;
};
