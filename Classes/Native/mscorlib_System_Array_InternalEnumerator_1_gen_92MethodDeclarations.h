﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
struct InternalEnumerator_1_t2950;
// System.Object
struct Object_t;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t54;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15164(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m15166(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15167(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m15168(__this, method) (TurnOffWordBehaviour_t54 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
