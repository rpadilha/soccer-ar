﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Graphic>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_79.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Graphic>
struct CachedInvokableCall_1_t3501  : public InvokableCall_1_t3502
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Graphic>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
