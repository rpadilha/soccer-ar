﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>
struct Transform_1_t4292;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t15;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m25231 (Transform_1_t4292 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::Invoke(TKey,TValue)
 Object_t * Transform_1_Invoke_m25232 (Transform_1_t4292 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m25233 (Transform_1_t4292 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::EndInvoke(System.IAsyncResult)
 Object_t * Transform_1_EndInvoke_m25234 (Transform_1_t4292 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
