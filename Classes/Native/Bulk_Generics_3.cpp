﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Array/InternalEnumerator`1<System.Double>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InternalEnumerator_1_t2857_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Double>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Array
#include "mscorlib_System_Array.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo Double_t853_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14717_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDouble_t853_m32783_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
 double Array_InternalArray__get_Item_TisDouble_t853_m32783 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m14713_MethodInfo;
 void InternalEnumerator_1__ctor_m14713 (InternalEnumerator_1_t2857 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714 (InternalEnumerator_1_t2857 * __this, MethodInfo* method){
	{
		double L_0 = InternalEnumerator_1_get_Current_m14717(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m14717_MethodInfo);
		double L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Double_t853_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m14715_MethodInfo;
 void InternalEnumerator_1_Dispose_m14715 (InternalEnumerator_1_t2857 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m14716_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m14716 (InternalEnumerator_1_t2857 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
 double InternalEnumerator_1_get_Current_m14717 (InternalEnumerator_1_t2857 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		double L_8 = Array_InternalArray__get_Item_TisDouble_t853_m32783(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDouble_t853_m32783_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Double>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2857____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2857, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2857____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2857, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2857_FieldInfos[] =
{
	&InternalEnumerator_1_t2857____array_0_FieldInfo,
	&InternalEnumerator_1_t2857____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2857____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2857_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2857____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2857_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14717_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2857_PropertyInfos[] =
{
	&InternalEnumerator_1_t2857____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2857____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2857_InternalEnumerator_1__ctor_m14713_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14713_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14713_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14713/* method */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2857_InternalEnumerator_1__ctor_m14713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14713_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714/* method */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14715_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14715_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14715/* method */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14715_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14716_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14716_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14716/* method */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14716_GenericMethod/* genericMethod */

};
extern Il2CppType Double_t853_0_0_0;
extern void* RuntimeInvoker_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14717_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14717_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14717/* method */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* declaring_type */
	, &Double_t853_0_0_0/* return_type */
	, RuntimeInvoker_Double_t853/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14717_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2857_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14713_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_MethodInfo,
	&InternalEnumerator_1_Dispose_m14715_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14716_MethodInfo,
	&InternalEnumerator_1_get_Current_m14717_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2857_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14716_MethodInfo,
	&InternalEnumerator_1_Dispose_m14715_MethodInfo,
	&InternalEnumerator_1_get_Current_m14717_MethodInfo,
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t6017_il2cpp_TypeInfo;
static TypeInfo* InternalEnumerator_1_t2857_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6017_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2857_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6017_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2857_0_0_0;
extern Il2CppType InternalEnumerator_1_t2857_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2857_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2857_MethodInfos/* methods */
	, InternalEnumerator_1_t2857_PropertyInfos/* properties */
	, InternalEnumerator_1_t2857_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2857_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2857_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2857_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2857_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2857_1_0_0/* this_arg */
	, InternalEnumerator_1_t2857_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2857_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2857)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7656_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Double>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Double>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Double>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Double>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Double>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Double>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Double>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Double>
extern MethodInfo ICollection_1_get_Count_m43032_MethodInfo;
static PropertyInfo ICollection_1_t7656____Count_PropertyInfo = 
{
	&ICollection_1_t7656_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43032_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43033_MethodInfo;
static PropertyInfo ICollection_1_t7656____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7656_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43033_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7656_PropertyInfos[] =
{
	&ICollection_1_t7656____Count_PropertyInfo,
	&ICollection_1_t7656____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43032_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Double>::get_Count()
MethodInfo ICollection_1_get_Count_m43032_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43032_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43033_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Double>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43033_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43033_GenericMethod/* genericMethod */

};
extern Il2CppType Double_t853_0_0_0;
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo ICollection_1_t7656_ICollection_1_Add_m43034_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43034_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Double>::Add(T)
MethodInfo ICollection_1_Add_m43034_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Double_t853/* invoker_method */
	, ICollection_1_t7656_ICollection_1_Add_m43034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43034_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43035_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Double>::Clear()
MethodInfo ICollection_1_Clear_m43035_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43035_GenericMethod/* genericMethod */

};
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo ICollection_1_t7656_ICollection_1_Contains_m43036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43036_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Double>::Contains(T)
MethodInfo ICollection_1_Contains_m43036_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Double_t853/* invoker_method */
	, ICollection_1_t7656_ICollection_1_Contains_m43036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43036_GenericMethod/* genericMethod */

};
extern Il2CppType DoubleU5BU5D_t1752_0_0_0;
extern Il2CppType DoubleU5BU5D_t1752_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7656_ICollection_1_CopyTo_m43037_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DoubleU5BU5D_t1752_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43037_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Double>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43037_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7656_ICollection_1_CopyTo_m43037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43037_GenericMethod/* genericMethod */

};
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo ICollection_1_t7656_ICollection_1_Remove_m43038_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43038_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Double>::Remove(T)
MethodInfo ICollection_1_Remove_m43038_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Double_t853/* invoker_method */
	, ICollection_1_t7656_ICollection_1_Remove_m43038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43038_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7656_MethodInfos[] =
{
	&ICollection_1_get_Count_m43032_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43033_MethodInfo,
	&ICollection_1_Add_m43034_MethodInfo,
	&ICollection_1_Clear_m43035_MethodInfo,
	&ICollection_1_Contains_m43036_MethodInfo,
	&ICollection_1_CopyTo_m43037_MethodInfo,
	&ICollection_1_Remove_m43038_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7658_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7656_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7658_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7656_0_0_0;
extern Il2CppType ICollection_1_t7656_1_0_0;
struct ICollection_1_t7656;
extern Il2CppGenericClass ICollection_1_t7656_GenericClass;
TypeInfo ICollection_1_t7656_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7656_MethodInfos/* methods */
	, ICollection_1_t7656_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7656_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7656_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7656_0_0_0/* byval_arg */
	, &ICollection_1_t7656_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7656_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Double>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Double>
extern Il2CppType IEnumerator_1_t6017_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43039_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Double>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43039_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7658_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6017_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43039_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7658_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43039_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7658_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7658_0_0_0;
extern Il2CppType IEnumerable_1_t7658_1_0_0;
struct IEnumerable_1_t7658;
extern Il2CppGenericClass IEnumerable_1_t7658_GenericClass;
TypeInfo IEnumerable_1_t7658_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7658_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7658_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7658_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7658_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7658_0_0_0/* byval_arg */
	, &IEnumerable_1_t7658_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7658_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7657_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Double>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Double>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Double>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Double>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Double>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Double>
extern MethodInfo IList_1_get_Item_m43040_MethodInfo;
extern MethodInfo IList_1_set_Item_m43041_MethodInfo;
static PropertyInfo IList_1_t7657____Item_PropertyInfo = 
{
	&IList_1_t7657_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43040_MethodInfo/* get */
	, &IList_1_set_Item_m43041_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7657_PropertyInfos[] =
{
	&IList_1_t7657____Item_PropertyInfo,
	NULL
};
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo IList_1_t7657_IList_1_IndexOf_m43042_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43042_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Double>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43042_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7657_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Double_t853/* invoker_method */
	, IList_1_t7657_IList_1_IndexOf_m43042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43042_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo IList_1_t7657_IList_1_Insert_m43043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43043_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Double>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43043_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Double_t853/* invoker_method */
	, IList_1_t7657_IList_1_Insert_m43043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43043_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7657_IList_1_RemoveAt_m43044_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43044_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Double>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43044_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7657_IList_1_RemoveAt_m43044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43044_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7657_IList_1_get_Item_m43040_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Double_t853_0_0_0;
extern void* RuntimeInvoker_Double_t853_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43040_GenericMethod;
// T System.Collections.Generic.IList`1<System.Double>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43040_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7657_il2cpp_TypeInfo/* declaring_type */
	, &Double_t853_0_0_0/* return_type */
	, RuntimeInvoker_Double_t853_Int32_t123/* invoker_method */
	, IList_1_t7657_IList_1_get_Item_m43040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43040_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo IList_1_t7657_IList_1_set_Item_m43041_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43041_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Double>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43041_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Double_t853/* invoker_method */
	, IList_1_t7657_IList_1_set_Item_m43041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43041_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7657_MethodInfos[] =
{
	&IList_1_IndexOf_m43042_MethodInfo,
	&IList_1_Insert_m43043_MethodInfo,
	&IList_1_RemoveAt_m43044_MethodInfo,
	&IList_1_get_Item_m43040_MethodInfo,
	&IList_1_set_Item_m43041_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7657_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7656_il2cpp_TypeInfo,
	&IEnumerable_1_t7658_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7657_0_0_0;
extern Il2CppType IList_1_t7657_1_0_0;
struct IList_1_t7657;
extern Il2CppGenericClass IList_1_t7657_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7657_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7657_MethodInfos/* methods */
	, IList_1_t7657_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7657_il2cpp_TypeInfo/* element_class */
	, IList_1_t7657_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7657_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7657_0_0_0/* byval_arg */
	, &IList_1_t7657_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7657_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7659_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>
extern MethodInfo ICollection_1_get_Count_m43045_MethodInfo;
static PropertyInfo ICollection_1_t7659____Count_PropertyInfo = 
{
	&ICollection_1_t7659_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43046_MethodInfo;
static PropertyInfo ICollection_1_t7659____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7659_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43046_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7659_PropertyInfos[] =
{
	&ICollection_1_t7659____Count_PropertyInfo,
	&ICollection_1_t7659____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43045_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::get_Count()
MethodInfo ICollection_1_get_Count_m43045_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43045_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43046_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43046_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43046_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2385_0_0_0;
extern Il2CppType IComparable_1_t2385_0_0_0;
static ParameterInfo ICollection_1_t7659_ICollection_1_Add_m43047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2385_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43047_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Add(T)
MethodInfo ICollection_1_Add_m43047_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7659_ICollection_1_Add_m43047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43047_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43048_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Clear()
MethodInfo ICollection_1_Clear_m43048_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43048_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2385_0_0_0;
static ParameterInfo ICollection_1_t7659_ICollection_1_Contains_m43049_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2385_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43049_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Contains(T)
MethodInfo ICollection_1_Contains_m43049_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7659_ICollection_1_Contains_m43049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43049_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5422_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5422_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7659_ICollection_1_CopyTo_m43050_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5422_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43050_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43050_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7659_ICollection_1_CopyTo_m43050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43050_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2385_0_0_0;
static ParameterInfo ICollection_1_t7659_ICollection_1_Remove_m43051_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2385_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43051_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Double>>::Remove(T)
MethodInfo ICollection_1_Remove_m43051_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7659_ICollection_1_Remove_m43051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43051_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7659_MethodInfos[] =
{
	&ICollection_1_get_Count_m43045_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43046_MethodInfo,
	&ICollection_1_Add_m43047_MethodInfo,
	&ICollection_1_Clear_m43048_MethodInfo,
	&ICollection_1_Contains_m43049_MethodInfo,
	&ICollection_1_CopyTo_m43050_MethodInfo,
	&ICollection_1_Remove_m43051_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7661_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7659_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7661_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7659_0_0_0;
extern Il2CppType ICollection_1_t7659_1_0_0;
struct ICollection_1_t7659;
extern Il2CppGenericClass ICollection_1_t7659_GenericClass;
TypeInfo ICollection_1_t7659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7659_MethodInfos/* methods */
	, ICollection_1_t7659_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7659_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7659_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7659_0_0_0/* byval_arg */
	, &ICollection_1_t7659_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7659_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Double>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Double>>
extern Il2CppType IEnumerator_1_t6019_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43052_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Double>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43052_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7661_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6019_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43052_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7661_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43052_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7661_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7661_0_0_0;
extern Il2CppType IEnumerable_1_t7661_1_0_0;
struct IEnumerable_1_t7661;
extern Il2CppGenericClass IEnumerable_1_t7661_GenericClass;
TypeInfo IEnumerable_1_t7661_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7661_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7661_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7661_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7661_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7661_0_0_0/* byval_arg */
	, &IEnumerable_1_t7661_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7661_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6019_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Double>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Double>>
extern MethodInfo IEnumerator_1_get_Current_m43053_MethodInfo;
static PropertyInfo IEnumerator_1_t6019____Current_PropertyInfo = 
{
	&IEnumerator_1_t6019_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43053_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6019_PropertyInfos[] =
{
	&IEnumerator_1_t6019____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2385_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43053_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Double>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43053_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6019_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2385_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43053_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6019_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43053_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6019_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6019_0_0_0;
extern Il2CppType IEnumerator_1_t6019_1_0_0;
struct IEnumerator_1_t6019;
extern Il2CppGenericClass IEnumerator_1_t6019_GenericClass;
TypeInfo IEnumerator_1_t6019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6019_MethodInfos/* methods */
	, IEnumerator_1_t6019_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6019_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6019_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6019_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6019_0_0_0/* byval_arg */
	, &IEnumerator_1_t6019_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6019_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2385_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Double>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Double>
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo IComparable_1_t2385_IComparable_1_CompareTo_m43054_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m43054_GenericMethod;
// System.Int32 System.IComparable`1<System.Double>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m43054_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2385_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Double_t853/* invoker_method */
	, IComparable_1_t2385_IComparable_1_CompareTo_m43054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m43054_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2385_MethodInfos[] =
{
	&IComparable_1_CompareTo_m43054_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2385_1_0_0;
struct IComparable_1_t2385;
extern Il2CppGenericClass IComparable_1_t2385_GenericClass;
TypeInfo IComparable_1_t2385_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2385_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2385_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2385_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2385_0_0_0/* byval_arg */
	, &IComparable_1_t2385_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2385_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_48.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2858_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_48MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14722_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2385_m32794_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Double>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Double>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2385_m32794(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2858____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2858, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2858____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2858, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2858_FieldInfos[] =
{
	&InternalEnumerator_1_t2858____array_0_FieldInfo,
	&InternalEnumerator_1_t2858____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2858____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2858_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2858____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2858_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14722_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2858_PropertyInfos[] =
{
	&InternalEnumerator_1_t2858____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2858____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2858_InternalEnumerator_1__ctor_m14718_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14718_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14718_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2858_InternalEnumerator_1__ctor_m14718_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14718_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14720_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14720_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14720_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14721_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14721_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14721_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2385_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14722_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Double>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14722_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2385_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14722_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2858_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14718_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_MethodInfo,
	&InternalEnumerator_1_Dispose_m14720_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14721_MethodInfo,
	&InternalEnumerator_1_get_Current_m14722_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14721_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14720_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2858_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14719_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14721_MethodInfo,
	&InternalEnumerator_1_Dispose_m14720_MethodInfo,
	&InternalEnumerator_1_get_Current_m14722_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2858_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6019_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2858_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6019_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2385_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2858_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14722_MethodInfo/* Method Usage */,
	&IComparable_1_t2385_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2385_m32794_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2858_0_0_0;
extern Il2CppType InternalEnumerator_1_t2858_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2858_GenericClass;
TypeInfo InternalEnumerator_1_t2858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2858_MethodInfos/* methods */
	, InternalEnumerator_1_t2858_PropertyInfos/* properties */
	, InternalEnumerator_1_t2858_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2858_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2858_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2858_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2858_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2858_1_0_0/* this_arg */
	, InternalEnumerator_1_t2858_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2858_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2858_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2858)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7660_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>
extern MethodInfo IList_1_get_Item_m43055_MethodInfo;
extern MethodInfo IList_1_set_Item_m43056_MethodInfo;
static PropertyInfo IList_1_t7660____Item_PropertyInfo = 
{
	&IList_1_t7660_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43055_MethodInfo/* get */
	, &IList_1_set_Item_m43056_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7660_PropertyInfos[] =
{
	&IList_1_t7660____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2385_0_0_0;
static ParameterInfo IList_1_t7660_IList_1_IndexOf_m43057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2385_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43057_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43057_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7660_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7660_IList_1_IndexOf_m43057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43057_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2385_0_0_0;
static ParameterInfo IList_1_t7660_IList_1_Insert_m43058_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2385_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43058_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43058_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7660_IList_1_Insert_m43058_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43058_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7660_IList_1_RemoveAt_m43059_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43059_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43059_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7660_IList_1_RemoveAt_m43059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43059_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7660_IList_1_get_Item_m43055_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IComparable_1_t2385_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43055_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43055_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7660_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2385_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7660_IList_1_get_Item_m43055_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43055_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2385_0_0_0;
static ParameterInfo IList_1_t7660_IList_1_set_Item_m43056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2385_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43056_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Double>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43056_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7660_IList_1_set_Item_m43056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43056_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7660_MethodInfos[] =
{
	&IList_1_IndexOf_m43057_MethodInfo,
	&IList_1_Insert_m43058_MethodInfo,
	&IList_1_RemoveAt_m43059_MethodInfo,
	&IList_1_get_Item_m43055_MethodInfo,
	&IList_1_set_Item_m43056_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7660_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7659_il2cpp_TypeInfo,
	&IEnumerable_1_t7661_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7660_0_0_0;
extern Il2CppType IList_1_t7660_1_0_0;
struct IList_1_t7660;
extern Il2CppGenericClass IList_1_t7660_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7660_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7660_MethodInfos/* methods */
	, IList_1_t7660_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7660_il2cpp_TypeInfo/* element_class */
	, IList_1_t7660_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7660_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7660_0_0_0/* byval_arg */
	, &IList_1_t7660_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7660_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7662_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>
extern MethodInfo ICollection_1_get_Count_m43060_MethodInfo;
static PropertyInfo ICollection_1_t7662____Count_PropertyInfo = 
{
	&ICollection_1_t7662_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43060_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43061_MethodInfo;
static PropertyInfo ICollection_1_t7662____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7662_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43061_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7662_PropertyInfos[] =
{
	&ICollection_1_t7662____Count_PropertyInfo,
	&ICollection_1_t7662____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43060_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::get_Count()
MethodInfo ICollection_1_get_Count_m43060_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43060_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43061_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43061_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43061_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2386_0_0_0;
extern Il2CppType IEquatable_1_t2386_0_0_0;
static ParameterInfo ICollection_1_t7662_ICollection_1_Add_m43062_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2386_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43062_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Add(T)
MethodInfo ICollection_1_Add_m43062_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7662_ICollection_1_Add_m43062_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43062_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43063_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Clear()
MethodInfo ICollection_1_Clear_m43063_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43063_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2386_0_0_0;
static ParameterInfo ICollection_1_t7662_ICollection_1_Contains_m43064_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2386_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43064_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Contains(T)
MethodInfo ICollection_1_Contains_m43064_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7662_ICollection_1_Contains_m43064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43064_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5423_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5423_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7662_ICollection_1_CopyTo_m43065_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5423_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43065_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43065_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7662_ICollection_1_CopyTo_m43065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43065_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2386_0_0_0;
static ParameterInfo ICollection_1_t7662_ICollection_1_Remove_m43066_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2386_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43066_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Double>>::Remove(T)
MethodInfo ICollection_1_Remove_m43066_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7662_ICollection_1_Remove_m43066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43066_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7662_MethodInfos[] =
{
	&ICollection_1_get_Count_m43060_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43061_MethodInfo,
	&ICollection_1_Add_m43062_MethodInfo,
	&ICollection_1_Clear_m43063_MethodInfo,
	&ICollection_1_Contains_m43064_MethodInfo,
	&ICollection_1_CopyTo_m43065_MethodInfo,
	&ICollection_1_Remove_m43066_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7664_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7662_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7664_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7662_0_0_0;
extern Il2CppType ICollection_1_t7662_1_0_0;
struct ICollection_1_t7662;
extern Il2CppGenericClass ICollection_1_t7662_GenericClass;
TypeInfo ICollection_1_t7662_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7662_MethodInfos/* methods */
	, ICollection_1_t7662_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7662_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7662_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7662_0_0_0/* byval_arg */
	, &ICollection_1_t7662_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7662_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Double>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Double>>
extern Il2CppType IEnumerator_1_t6021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43067_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Double>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43067_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7664_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43067_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7664_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43067_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7664_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7664_0_0_0;
extern Il2CppType IEnumerable_1_t7664_1_0_0;
struct IEnumerable_1_t7664;
extern Il2CppGenericClass IEnumerable_1_t7664_GenericClass;
TypeInfo IEnumerable_1_t7664_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7664_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7664_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7664_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7664_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7664_0_0_0/* byval_arg */
	, &IEnumerable_1_t7664_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7664_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6021_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Double>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Double>>
extern MethodInfo IEnumerator_1_get_Current_m43068_MethodInfo;
static PropertyInfo IEnumerator_1_t6021____Current_PropertyInfo = 
{
	&IEnumerator_1_t6021_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43068_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6021_PropertyInfos[] =
{
	&IEnumerator_1_t6021____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2386_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43068_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Double>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43068_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6021_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2386_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43068_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6021_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43068_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6021_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6021_0_0_0;
extern Il2CppType IEnumerator_1_t6021_1_0_0;
struct IEnumerator_1_t6021;
extern Il2CppGenericClass IEnumerator_1_t6021_GenericClass;
TypeInfo IEnumerator_1_t6021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6021_MethodInfos/* methods */
	, IEnumerator_1_t6021_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6021_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6021_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6021_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6021_0_0_0/* byval_arg */
	, &IEnumerator_1_t6021_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6021_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2386_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Double>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Double>
extern Il2CppType Double_t853_0_0_0;
static ParameterInfo IEquatable_1_t2386_IEquatable_1_Equals_m43069_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Double_t853_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Double_t853 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m43069_GenericMethod;
// System.Boolean System.IEquatable`1<System.Double>::Equals(T)
MethodInfo IEquatable_1_Equals_m43069_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2386_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Double_t853/* invoker_method */
	, IEquatable_1_t2386_IEquatable_1_Equals_m43069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m43069_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2386_MethodInfos[] =
{
	&IEquatable_1_Equals_m43069_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2386_1_0_0;
struct IEquatable_1_t2386;
extern Il2CppGenericClass IEquatable_1_t2386_GenericClass;
TypeInfo IEquatable_1_t2386_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2386_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2386_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2386_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2386_0_0_0/* byval_arg */
	, &IEquatable_1_t2386_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2386_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_49.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2859_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_49MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14727_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2386_m32805_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Double>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Double>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2386_m32805(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2859____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2859, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2859____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2859, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2859_FieldInfos[] =
{
	&InternalEnumerator_1_t2859____array_0_FieldInfo,
	&InternalEnumerator_1_t2859____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2859____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2859_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2859____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2859_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14727_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2859_PropertyInfos[] =
{
	&InternalEnumerator_1_t2859____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2859____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2859_InternalEnumerator_1__ctor_m14723_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14723_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14723_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2859_InternalEnumerator_1__ctor_m14723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14723_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14725_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14725_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14725_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14726_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14726_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14726_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2386_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14727_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Double>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14727_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2386_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14727_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2859_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14723_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_MethodInfo,
	&InternalEnumerator_1_Dispose_m14725_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14726_MethodInfo,
	&InternalEnumerator_1_get_Current_m14727_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14726_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14725_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2859_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14724_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14726_MethodInfo,
	&InternalEnumerator_1_Dispose_m14725_MethodInfo,
	&InternalEnumerator_1_get_Current_m14727_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2859_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6021_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2859_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6021_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2386_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2859_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14727_MethodInfo/* Method Usage */,
	&IEquatable_1_t2386_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2386_m32805_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2859_0_0_0;
extern Il2CppType InternalEnumerator_1_t2859_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2859_GenericClass;
TypeInfo InternalEnumerator_1_t2859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2859_MethodInfos/* methods */
	, InternalEnumerator_1_t2859_PropertyInfos/* properties */
	, InternalEnumerator_1_t2859_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2859_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2859_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2859_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2859_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2859_1_0_0/* this_arg */
	, InternalEnumerator_1_t2859_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2859_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2859_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2859)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7663_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>
extern MethodInfo IList_1_get_Item_m43070_MethodInfo;
extern MethodInfo IList_1_set_Item_m43071_MethodInfo;
static PropertyInfo IList_1_t7663____Item_PropertyInfo = 
{
	&IList_1_t7663_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43070_MethodInfo/* get */
	, &IList_1_set_Item_m43071_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7663_PropertyInfos[] =
{
	&IList_1_t7663____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2386_0_0_0;
static ParameterInfo IList_1_t7663_IList_1_IndexOf_m43072_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2386_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43072_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43072_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7663_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7663_IList_1_IndexOf_m43072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43072_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEquatable_1_t2386_0_0_0;
static ParameterInfo IList_1_t7663_IList_1_Insert_m43073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2386_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43073_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43073_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7663_IList_1_Insert_m43073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43073_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7663_IList_1_RemoveAt_m43074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43074_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43074_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7663_IList_1_RemoveAt_m43074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43074_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7663_IList_1_get_Item_m43070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEquatable_1_t2386_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43070_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43070_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7663_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2386_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7663_IList_1_get_Item_m43070_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43070_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEquatable_1_t2386_0_0_0;
static ParameterInfo IList_1_t7663_IList_1_set_Item_m43071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2386_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43071_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Double>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43071_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7663_IList_1_set_Item_m43071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43071_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7663_MethodInfos[] =
{
	&IList_1_IndexOf_m43072_MethodInfo,
	&IList_1_Insert_m43073_MethodInfo,
	&IList_1_RemoveAt_m43074_MethodInfo,
	&IList_1_get_Item_m43070_MethodInfo,
	&IList_1_set_Item_m43071_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7663_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7662_il2cpp_TypeInfo,
	&IEnumerable_1_t7664_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7663_0_0_0;
extern Il2CppType IList_1_t7663_1_0_0;
struct IList_1_t7663;
extern Il2CppGenericClass IList_1_t7663_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7663_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7663_MethodInfos/* methods */
	, IList_1_t7663_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7663_il2cpp_TypeInfo/* element_class */
	, IList_1_t7663_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7663_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7663_0_0_0/* byval_arg */
	, &IList_1_t7663_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7663_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t1742_il2cpp_TypeInfo;

// System.Char
#include "mscorlib_System_Char.h"


// T System.Collections.Generic.IEnumerator`1<System.Char>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Char>
extern MethodInfo IEnumerator_1_get_Current_m43075_MethodInfo;
static PropertyInfo IEnumerator_1_t1742____Current_PropertyInfo = 
{
	&IEnumerator_1_t1742_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43075_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t1742_PropertyInfos[] =
{
	&IEnumerator_1_t1742____Current_PropertyInfo,
	NULL
};
extern Il2CppType Char_t371_0_0_0;
extern void* RuntimeInvoker_Char_t371 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43075_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Char>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43075_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t1742_il2cpp_TypeInfo/* declaring_type */
	, &Char_t371_0_0_0/* return_type */
	, RuntimeInvoker_Char_t371/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43075_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t1742_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43075_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t1742_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t1742_0_0_0;
extern Il2CppType IEnumerator_1_t1742_1_0_0;
struct IEnumerator_1_t1742;
extern Il2CppGenericClass IEnumerator_1_t1742_GenericClass;
TypeInfo IEnumerator_1_t1742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t1742_MethodInfos/* methods */
	, IEnumerator_1_t1742_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t1742_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t1742_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t1742_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t1742_0_0_0/* byval_arg */
	, &IEnumerator_1_t1742_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t1742_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Char>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_50.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2860_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Char>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_50MethodDeclarations.h"

extern TypeInfo Char_t371_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14732_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisChar_t371_m32816_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
 uint16_t Array_InternalArray__get_Item_TisChar_t371_m32816 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m14728_MethodInfo;
 void InternalEnumerator_1__ctor_m14728 (InternalEnumerator_1_t2860 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729 (InternalEnumerator_1_t2860 * __this, MethodInfo* method){
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m14732(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m14732_MethodInfo);
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Char_t371_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m14730_MethodInfo;
 void InternalEnumerator_1_Dispose_m14730 (InternalEnumerator_1_t2860 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m14731_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m14731 (InternalEnumerator_1_t2860 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
 uint16_t InternalEnumerator_1_get_Current_m14732 (InternalEnumerator_1_t2860 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint16_t L_8 = Array_InternalArray__get_Item_TisChar_t371_m32816(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisChar_t371_m32816_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Char>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2860____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2860, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2860____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2860, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2860_FieldInfos[] =
{
	&InternalEnumerator_1_t2860____array_0_FieldInfo,
	&InternalEnumerator_1_t2860____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2860____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2860_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2860____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2860_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2860_PropertyInfos[] =
{
	&InternalEnumerator_1_t2860____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2860____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2860_InternalEnumerator_1__ctor_m14728_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14728_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14728_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14728/* method */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2860_InternalEnumerator_1__ctor_m14728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14728_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729/* method */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14730_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14730_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14730/* method */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14730_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14731_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14731_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14731/* method */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14731_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t371_0_0_0;
extern void* RuntimeInvoker_Char_t371 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14732_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14732_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14732/* method */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* declaring_type */
	, &Char_t371_0_0_0/* return_type */
	, RuntimeInvoker_Char_t371/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14732_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2860_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14728_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_MethodInfo,
	&InternalEnumerator_1_Dispose_m14730_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14731_MethodInfo,
	&InternalEnumerator_1_get_Current_m14732_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2860_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14729_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14731_MethodInfo,
	&InternalEnumerator_1_Dispose_m14730_MethodInfo,
	&InternalEnumerator_1_get_Current_m14732_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2860_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t1742_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2860_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t1742_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2860_0_0_0;
extern Il2CppType InternalEnumerator_1_t2860_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2860_GenericClass;
TypeInfo InternalEnumerator_1_t2860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2860_MethodInfos/* methods */
	, InternalEnumerator_1_t2860_PropertyInfos/* properties */
	, InternalEnumerator_1_t2860_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2860_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2860_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2860_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2860_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2860_1_0_0/* this_arg */
	, InternalEnumerator_1_t2860_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2860_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2860)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7665_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Char>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Char>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Char>
extern MethodInfo ICollection_1_get_Count_m43076_MethodInfo;
static PropertyInfo ICollection_1_t7665____Count_PropertyInfo = 
{
	&ICollection_1_t7665_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43076_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43077_MethodInfo;
static PropertyInfo ICollection_1_t7665____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7665_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7665_PropertyInfos[] =
{
	&ICollection_1_t7665____Count_PropertyInfo,
	&ICollection_1_t7665____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43076_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Char>::get_Count()
MethodInfo ICollection_1_get_Count_m43076_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43076_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43077_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43077_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43077_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t371_0_0_0;
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo ICollection_1_t7665_ICollection_1_Add_m43078_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43078_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Add(T)
MethodInfo ICollection_1_Add_m43078_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int16_t524/* invoker_method */
	, ICollection_1_t7665_ICollection_1_Add_m43078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43078_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43079_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Clear()
MethodInfo ICollection_1_Clear_m43079_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43079_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo ICollection_1_t7665_ICollection_1_Contains_m43080_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43080_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Contains(T)
MethodInfo ICollection_1_Contains_m43080_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int16_t524/* invoker_method */
	, ICollection_1_t7665_ICollection_1_Contains_m43080_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43080_GenericMethod/* genericMethod */

};
extern Il2CppType CharU5BU5D_t378_0_0_0;
extern Il2CppType CharU5BU5D_t378_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7665_ICollection_1_CopyTo_m43081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CharU5BU5D_t378_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43081_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Char>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43081_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7665_ICollection_1_CopyTo_m43081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43081_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo ICollection_1_t7665_ICollection_1_Remove_m43082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43082_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Remove(T)
MethodInfo ICollection_1_Remove_m43082_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int16_t524/* invoker_method */
	, ICollection_1_t7665_ICollection_1_Remove_m43082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43082_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7665_MethodInfos[] =
{
	&ICollection_1_get_Count_m43076_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43077_MethodInfo,
	&ICollection_1_Add_m43078_MethodInfo,
	&ICollection_1_Clear_m43079_MethodInfo,
	&ICollection_1_Contains_m43080_MethodInfo,
	&ICollection_1_CopyTo_m43081_MethodInfo,
	&ICollection_1_Remove_m43082_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t2378_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7665_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t2378_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7665_0_0_0;
extern Il2CppType ICollection_1_t7665_1_0_0;
struct ICollection_1_t7665;
extern Il2CppGenericClass ICollection_1_t7665_GenericClass;
TypeInfo ICollection_1_t7665_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7665_MethodInfos/* methods */
	, ICollection_1_t7665_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7665_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7665_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7665_0_0_0/* byval_arg */
	, &ICollection_1_t7665_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7665_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Char>
extern Il2CppType IEnumerator_1_t1742_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43083_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43083_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t2378_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1742_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43083_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t2378_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43083_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t2378_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t2378_0_0_0;
extern Il2CppType IEnumerable_1_t2378_1_0_0;
struct IEnumerable_1_t2378;
extern Il2CppGenericClass IEnumerable_1_t2378_GenericClass;
TypeInfo IEnumerable_1_t2378_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t2378_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t2378_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t2378_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t2378_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t2378_0_0_0/* byval_arg */
	, &IEnumerable_1_t2378_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t2378_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7666_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Char>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Char>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Char>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Char>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Char>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Char>
extern MethodInfo IList_1_get_Item_m43084_MethodInfo;
extern MethodInfo IList_1_set_Item_m43085_MethodInfo;
static PropertyInfo IList_1_t7666____Item_PropertyInfo = 
{
	&IList_1_t7666_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43084_MethodInfo/* get */
	, &IList_1_set_Item_m43085_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7666_PropertyInfos[] =
{
	&IList_1_t7666____Item_PropertyInfo,
	NULL
};
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo IList_1_t7666_IList_1_IndexOf_m43086_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43086_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Char>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43086_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7666_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int16_t524/* invoker_method */
	, IList_1_t7666_IList_1_IndexOf_m43086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43086_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo IList_1_t7666_IList_1_Insert_m43087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43087_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Char>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43087_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int16_t524/* invoker_method */
	, IList_1_t7666_IList_1_Insert_m43087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43087_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7666_IList_1_RemoveAt_m43088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43088_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Char>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43088_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7666_IList_1_RemoveAt_m43088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43088_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7666_IList_1_get_Item_m43084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Char_t371_0_0_0;
extern void* RuntimeInvoker_Char_t371_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43084_GenericMethod;
// T System.Collections.Generic.IList`1<System.Char>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43084_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7666_il2cpp_TypeInfo/* declaring_type */
	, &Char_t371_0_0_0/* return_type */
	, RuntimeInvoker_Char_t371_Int32_t123/* invoker_method */
	, IList_1_t7666_IList_1_get_Item_m43084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43084_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo IList_1_t7666_IList_1_set_Item_m43085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43085_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Char>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43085_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int16_t524/* invoker_method */
	, IList_1_t7666_IList_1_set_Item_m43085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43085_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7666_MethodInfos[] =
{
	&IList_1_IndexOf_m43086_MethodInfo,
	&IList_1_Insert_m43087_MethodInfo,
	&IList_1_RemoveAt_m43088_MethodInfo,
	&IList_1_get_Item_m43084_MethodInfo,
	&IList_1_set_Item_m43085_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7666_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7665_il2cpp_TypeInfo,
	&IEnumerable_1_t2378_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7666_0_0_0;
extern Il2CppType IList_1_t7666_1_0_0;
struct IList_1_t7666;
extern Il2CppGenericClass IList_1_t7666_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7666_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7666_MethodInfos/* methods */
	, IList_1_t7666_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7666_il2cpp_TypeInfo/* element_class */
	, IList_1_t7666_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7666_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7666_0_0_0/* byval_arg */
	, &IList_1_t7666_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7666_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7667_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>
extern MethodInfo ICollection_1_get_Count_m43089_MethodInfo;
static PropertyInfo ICollection_1_t7667____Count_PropertyInfo = 
{
	&ICollection_1_t7667_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43089_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43090_MethodInfo;
static PropertyInfo ICollection_1_t7667____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7667_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43090_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7667_PropertyInfos[] =
{
	&ICollection_1_t7667____Count_PropertyInfo,
	&ICollection_1_t7667____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43089_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_Count()
MethodInfo ICollection_1_get_Count_m43089_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43089_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43090_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43090_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43090_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2371_0_0_0;
extern Il2CppType IComparable_1_t2371_0_0_0;
static ParameterInfo ICollection_1_t7667_ICollection_1_Add_m43091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2371_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43091_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Add(T)
MethodInfo ICollection_1_Add_m43091_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7667_ICollection_1_Add_m43091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43091_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43092_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Clear()
MethodInfo ICollection_1_Clear_m43092_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43092_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2371_0_0_0;
static ParameterInfo ICollection_1_t7667_ICollection_1_Contains_m43093_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2371_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43093_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Contains(T)
MethodInfo ICollection_1_Contains_m43093_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7667_ICollection_1_Contains_m43093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43093_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5424_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5424_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7667_ICollection_1_CopyTo_m43094_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5424_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43094_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43094_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7667_ICollection_1_CopyTo_m43094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43094_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2371_0_0_0;
static ParameterInfo ICollection_1_t7667_ICollection_1_Remove_m43095_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2371_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43095_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Remove(T)
MethodInfo ICollection_1_Remove_m43095_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7667_ICollection_1_Remove_m43095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43095_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7667_MethodInfos[] =
{
	&ICollection_1_get_Count_m43089_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43090_MethodInfo,
	&ICollection_1_Add_m43091_MethodInfo,
	&ICollection_1_Clear_m43092_MethodInfo,
	&ICollection_1_Contains_m43093_MethodInfo,
	&ICollection_1_CopyTo_m43094_MethodInfo,
	&ICollection_1_Remove_m43095_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7669_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7667_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7669_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7667_0_0_0;
extern Il2CppType ICollection_1_t7667_1_0_0;
struct ICollection_1_t7667;
extern Il2CppGenericClass ICollection_1_t7667_GenericClass;
TypeInfo ICollection_1_t7667_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7667_MethodInfos/* methods */
	, ICollection_1_t7667_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7667_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7667_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7667_0_0_0/* byval_arg */
	, &ICollection_1_t7667_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7667_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>
extern Il2CppType IEnumerator_1_t6023_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43096_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43096_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7669_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43096_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7669_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43096_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7669_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7669_0_0_0;
extern Il2CppType IEnumerable_1_t7669_1_0_0;
struct IEnumerable_1_t7669;
extern Il2CppGenericClass IEnumerable_1_t7669_GenericClass;
TypeInfo IEnumerable_1_t7669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7669_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7669_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7669_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7669_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7669_0_0_0/* byval_arg */
	, &IEnumerable_1_t7669_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7669_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6023_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>
extern MethodInfo IEnumerator_1_get_Current_m43097_MethodInfo;
static PropertyInfo IEnumerator_1_t6023____Current_PropertyInfo = 
{
	&IEnumerator_1_t6023_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43097_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6023_PropertyInfos[] =
{
	&IEnumerator_1_t6023____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2371_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43097_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43097_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6023_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2371_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43097_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6023_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43097_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6023_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6023_0_0_0;
extern Il2CppType IEnumerator_1_t6023_1_0_0;
struct IEnumerator_1_t6023;
extern Il2CppGenericClass IEnumerator_1_t6023_GenericClass;
TypeInfo IEnumerator_1_t6023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6023_MethodInfos/* methods */
	, IEnumerator_1_t6023_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6023_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6023_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6023_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6023_0_0_0/* byval_arg */
	, &IEnumerator_1_t6023_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6023_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2371_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Char>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Char>
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo IComparable_1_t2371_IComparable_1_CompareTo_m43098_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m43098_GenericMethod;
// System.Int32 System.IComparable`1<System.Char>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m43098_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2371_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int16_t524/* invoker_method */
	, IComparable_1_t2371_IComparable_1_CompareTo_m43098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m43098_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2371_MethodInfos[] =
{
	&IComparable_1_CompareTo_m43098_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2371_1_0_0;
struct IComparable_1_t2371;
extern Il2CppGenericClass IComparable_1_t2371_GenericClass;
TypeInfo IComparable_1_t2371_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2371_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2371_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2371_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2371_0_0_0/* byval_arg */
	, &IComparable_1_t2371_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2371_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_51.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2861_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_51MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14737_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2371_m32827_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Char>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Char>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2371_m32827(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2861____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2861, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2861____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2861, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2861_FieldInfos[] =
{
	&InternalEnumerator_1_t2861____array_0_FieldInfo,
	&InternalEnumerator_1_t2861____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2861____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2861_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2861____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2861_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14737_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2861_PropertyInfos[] =
{
	&InternalEnumerator_1_t2861____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2861____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2861_InternalEnumerator_1__ctor_m14733_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14733_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14733_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2861_InternalEnumerator_1__ctor_m14733_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14733_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14735_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14735_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14735_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14736_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14736_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14736_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2371_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14737_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14737_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2371_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14737_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2861_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14733_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_MethodInfo,
	&InternalEnumerator_1_Dispose_m14735_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14736_MethodInfo,
	&InternalEnumerator_1_get_Current_m14737_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14736_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14735_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2861_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14734_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14736_MethodInfo,
	&InternalEnumerator_1_Dispose_m14735_MethodInfo,
	&InternalEnumerator_1_get_Current_m14737_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2861_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6023_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2861_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6023_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2371_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2861_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14737_MethodInfo/* Method Usage */,
	&IComparable_1_t2371_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2371_m32827_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2861_0_0_0;
extern Il2CppType InternalEnumerator_1_t2861_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2861_GenericClass;
TypeInfo InternalEnumerator_1_t2861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2861_MethodInfos/* methods */
	, InternalEnumerator_1_t2861_PropertyInfos/* properties */
	, InternalEnumerator_1_t2861_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2861_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2861_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2861_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2861_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2861_1_0_0/* this_arg */
	, InternalEnumerator_1_t2861_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2861_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2861_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2861)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7668_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>
extern MethodInfo IList_1_get_Item_m43099_MethodInfo;
extern MethodInfo IList_1_set_Item_m43100_MethodInfo;
static PropertyInfo IList_1_t7668____Item_PropertyInfo = 
{
	&IList_1_t7668_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43099_MethodInfo/* get */
	, &IList_1_set_Item_m43100_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7668_PropertyInfos[] =
{
	&IList_1_t7668____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2371_0_0_0;
static ParameterInfo IList_1_t7668_IList_1_IndexOf_m43101_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2371_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43101_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43101_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7668_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7668_IList_1_IndexOf_m43101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43101_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2371_0_0_0;
static ParameterInfo IList_1_t7668_IList_1_Insert_m43102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2371_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43102_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43102_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7668_IList_1_Insert_m43102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43102_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7668_IList_1_RemoveAt_m43103_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43103_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43103_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7668_IList_1_RemoveAt_m43103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43103_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7668_IList_1_get_Item_m43099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IComparable_1_t2371_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43099_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43099_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7668_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2371_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7668_IList_1_get_Item_m43099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43099_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2371_0_0_0;
static ParameterInfo IList_1_t7668_IList_1_set_Item_m43100_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2371_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43100_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43100_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7668_IList_1_set_Item_m43100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43100_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7668_MethodInfos[] =
{
	&IList_1_IndexOf_m43101_MethodInfo,
	&IList_1_Insert_m43102_MethodInfo,
	&IList_1_RemoveAt_m43103_MethodInfo,
	&IList_1_get_Item_m43099_MethodInfo,
	&IList_1_set_Item_m43100_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7668_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7667_il2cpp_TypeInfo,
	&IEnumerable_1_t7669_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7668_0_0_0;
extern Il2CppType IList_1_t7668_1_0_0;
struct IList_1_t7668;
extern Il2CppGenericClass IList_1_t7668_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7668_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7668_MethodInfos/* methods */
	, IList_1_t7668_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7668_il2cpp_TypeInfo/* element_class */
	, IList_1_t7668_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7668_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7668_0_0_0/* byval_arg */
	, &IList_1_t7668_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7668_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7670_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>
extern MethodInfo ICollection_1_get_Count_m43104_MethodInfo;
static PropertyInfo ICollection_1_t7670____Count_PropertyInfo = 
{
	&ICollection_1_t7670_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43104_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43105_MethodInfo;
static PropertyInfo ICollection_1_t7670____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7670_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43105_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7670_PropertyInfos[] =
{
	&ICollection_1_t7670____Count_PropertyInfo,
	&ICollection_1_t7670____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43104_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_Count()
MethodInfo ICollection_1_get_Count_m43104_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43104_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43105_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43105_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43105_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2372_0_0_0;
extern Il2CppType IEquatable_1_t2372_0_0_0;
static ParameterInfo ICollection_1_t7670_ICollection_1_Add_m43106_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2372_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43106_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Add(T)
MethodInfo ICollection_1_Add_m43106_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7670_ICollection_1_Add_m43106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43106_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43107_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Clear()
MethodInfo ICollection_1_Clear_m43107_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43107_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2372_0_0_0;
static ParameterInfo ICollection_1_t7670_ICollection_1_Contains_m43108_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2372_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43108_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Contains(T)
MethodInfo ICollection_1_Contains_m43108_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7670_ICollection_1_Contains_m43108_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43108_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5425_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5425_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7670_ICollection_1_CopyTo_m43109_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5425_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43109_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43109_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7670_ICollection_1_CopyTo_m43109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43109_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2372_0_0_0;
static ParameterInfo ICollection_1_t7670_ICollection_1_Remove_m43110_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2372_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43110_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Remove(T)
MethodInfo ICollection_1_Remove_m43110_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7670_ICollection_1_Remove_m43110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43110_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7670_MethodInfos[] =
{
	&ICollection_1_get_Count_m43104_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43105_MethodInfo,
	&ICollection_1_Add_m43106_MethodInfo,
	&ICollection_1_Clear_m43107_MethodInfo,
	&ICollection_1_Contains_m43108_MethodInfo,
	&ICollection_1_CopyTo_m43109_MethodInfo,
	&ICollection_1_Remove_m43110_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7672_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7670_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7672_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7670_0_0_0;
extern Il2CppType ICollection_1_t7670_1_0_0;
struct ICollection_1_t7670;
extern Il2CppGenericClass ICollection_1_t7670_GenericClass;
TypeInfo ICollection_1_t7670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7670_MethodInfos/* methods */
	, ICollection_1_t7670_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7670_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7670_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7670_0_0_0/* byval_arg */
	, &ICollection_1_t7670_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7670_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>
extern Il2CppType IEnumerator_1_t6025_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43111_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43111_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7672_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6025_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43111_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7672_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43111_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7672_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7672_0_0_0;
extern Il2CppType IEnumerable_1_t7672_1_0_0;
struct IEnumerable_1_t7672;
extern Il2CppGenericClass IEnumerable_1_t7672_GenericClass;
TypeInfo IEnumerable_1_t7672_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7672_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7672_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7672_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7672_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7672_0_0_0/* byval_arg */
	, &IEnumerable_1_t7672_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7672_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6025_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>
extern MethodInfo IEnumerator_1_get_Current_m43112_MethodInfo;
static PropertyInfo IEnumerator_1_t6025____Current_PropertyInfo = 
{
	&IEnumerator_1_t6025_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43112_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6025_PropertyInfos[] =
{
	&IEnumerator_1_t6025____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2372_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43112_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43112_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6025_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2372_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43112_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6025_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43112_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6025_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6025_0_0_0;
extern Il2CppType IEnumerator_1_t6025_1_0_0;
struct IEnumerator_1_t6025;
extern Il2CppGenericClass IEnumerator_1_t6025_GenericClass;
TypeInfo IEnumerator_1_t6025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6025_MethodInfos/* methods */
	, IEnumerator_1_t6025_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6025_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6025_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6025_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6025_0_0_0/* byval_arg */
	, &IEnumerator_1_t6025_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6025_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2372_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Char>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Char>
extern Il2CppType Char_t371_0_0_0;
static ParameterInfo IEquatable_1_t2372_IEquatable_1_Equals_m43113_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Char_t371_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m43113_GenericMethod;
// System.Boolean System.IEquatable`1<System.Char>::Equals(T)
MethodInfo IEquatable_1_Equals_m43113_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2372_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int16_t524/* invoker_method */
	, IEquatable_1_t2372_IEquatable_1_Equals_m43113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m43113_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2372_MethodInfos[] =
{
	&IEquatable_1_Equals_m43113_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2372_1_0_0;
struct IEquatable_1_t2372;
extern Il2CppGenericClass IEquatable_1_t2372_GenericClass;
TypeInfo IEquatable_1_t2372_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2372_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2372_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2372_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2372_0_0_0/* byval_arg */
	, &IEquatable_1_t2372_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2372_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_52.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2862_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_52MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14742_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2372_m32838_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Char>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Char>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2372_m32838(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2862____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2862, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2862____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2862, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2862_FieldInfos[] =
{
	&InternalEnumerator_1_t2862____array_0_FieldInfo,
	&InternalEnumerator_1_t2862____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2862____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2862_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2862____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2862_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14742_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2862_PropertyInfos[] =
{
	&InternalEnumerator_1_t2862____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2862____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2862_InternalEnumerator_1__ctor_m14738_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14738_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14738_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2862_InternalEnumerator_1__ctor_m14738_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14738_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14740_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14740_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14740_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14741_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14741_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14741_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2372_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14742_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14742_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2372_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14742_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2862_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14738_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_MethodInfo,
	&InternalEnumerator_1_Dispose_m14740_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14741_MethodInfo,
	&InternalEnumerator_1_get_Current_m14742_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14741_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14740_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2862_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14739_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14741_MethodInfo,
	&InternalEnumerator_1_Dispose_m14740_MethodInfo,
	&InternalEnumerator_1_get_Current_m14742_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2862_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6025_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2862_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6025_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2372_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2862_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14742_MethodInfo/* Method Usage */,
	&IEquatable_1_t2372_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2372_m32838_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2862_0_0_0;
extern Il2CppType InternalEnumerator_1_t2862_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2862_GenericClass;
TypeInfo InternalEnumerator_1_t2862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2862_MethodInfos/* methods */
	, InternalEnumerator_1_t2862_PropertyInfos/* properties */
	, InternalEnumerator_1_t2862_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2862_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2862_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2862_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2862_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2862_1_0_0/* this_arg */
	, InternalEnumerator_1_t2862_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2862_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2862_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2862)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7671_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>
extern MethodInfo IList_1_get_Item_m43114_MethodInfo;
extern MethodInfo IList_1_set_Item_m43115_MethodInfo;
static PropertyInfo IList_1_t7671____Item_PropertyInfo = 
{
	&IList_1_t7671_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43114_MethodInfo/* get */
	, &IList_1_set_Item_m43115_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7671_PropertyInfos[] =
{
	&IList_1_t7671____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2372_0_0_0;
static ParameterInfo IList_1_t7671_IList_1_IndexOf_m43116_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2372_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43116_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43116_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7671_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7671_IList_1_IndexOf_m43116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43116_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEquatable_1_t2372_0_0_0;
static ParameterInfo IList_1_t7671_IList_1_Insert_m43117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2372_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43117_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43117_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7671_IList_1_Insert_m43117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43117_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7671_IList_1_RemoveAt_m43118_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43118_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43118_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7671_IList_1_RemoveAt_m43118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43118_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7671_IList_1_get_Item_m43114_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEquatable_1_t2372_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43114_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43114_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7671_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2372_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7671_IList_1_get_Item_m43114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43114_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEquatable_1_t2372_0_0_0;
static ParameterInfo IList_1_t7671_IList_1_set_Item_m43115_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2372_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43115_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43115_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7671_IList_1_set_Item_m43115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43115_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7671_MethodInfos[] =
{
	&IList_1_IndexOf_m43116_MethodInfo,
	&IList_1_Insert_m43117_MethodInfo,
	&IList_1_RemoveAt_m43118_MethodInfo,
	&IList_1_get_Item_m43114_MethodInfo,
	&IList_1_set_Item_m43115_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7671_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7670_il2cpp_TypeInfo,
	&IEnumerable_1_t7672_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7671_0_0_0;
extern Il2CppType IList_1_t7671_1_0_0;
struct IList_1_t7671;
extern Il2CppGenericClass IList_1_t7671_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7671_MethodInfos/* methods */
	, IList_1_t7671_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7671_il2cpp_TypeInfo/* element_class */
	, IList_1_t7671_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7671_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7671_0_0_0/* byval_arg */
	, &IList_1_t7671_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7671_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Comparison_1_t2833_il2cpp_TypeInfo;
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Comparison_1__ctor_m14743_MethodInfo;
 void Comparison_1__ctor_m14743_gshared (Comparison_1_t2833 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
extern MethodInfo Comparison_1_Invoke_m14744_MethodInfo;
 int32_t Comparison_1_Invoke_m14744_gshared (Comparison_1_t2833 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Comparison_1_Invoke_m14744((Comparison_1_t2833 *)__this->___prev_9,___x, ___y, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, Object_t * ___y, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Comparison`1<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern MethodInfo Comparison_1_BeginInvoke_m14745_MethodInfo;
 Object_t * Comparison_1_BeginInvoke_m14745_gshared (Comparison_1_t2833 * __this, Object_t * ___x, Object_t * ___y, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___x;
	__d_args[1] = ___y;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Int32 System.Comparison`1<System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo Comparison_1_EndInvoke_m14746_MethodInfo;
 int32_t Comparison_1_EndInvoke_m14746_gshared (Comparison_1_t2833 * __this, Object_t * ___result, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Comparison_1_t2833_Comparison_1__ctor_m14743_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1__ctor_m14743_GenericMethod;
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo Comparison_1__ctor_m14743_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparison_1__ctor_m14743_gshared/* method */
	, &Comparison_1_t2833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Comparison_1_t2833_Comparison_1__ctor_m14743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1__ctor_m14743_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparison_1_t2833_Comparison_1_Invoke_m14744_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_Invoke_m14744_GenericMethod;
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
MethodInfo Comparison_1_Invoke_m14744_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Comparison_1_Invoke_m14744_gshared/* method */
	, &Comparison_1_t2833_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparison_1_t2833_Comparison_1_Invoke_m14744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_Invoke_m14744_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparison_1_t2833_Comparison_1_BeginInvoke_m14745_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_BeginInvoke_m14745_GenericMethod;
// System.IAsyncResult System.Comparison`1<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
MethodInfo Comparison_1_BeginInvoke_m14745_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Comparison_1_BeginInvoke_m14745_gshared/* method */
	, &Comparison_1_t2833_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Comparison_1_t2833_Comparison_1_BeginInvoke_m14745_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_BeginInvoke_m14745_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Comparison_1_t2833_Comparison_1_EndInvoke_m14746_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_EndInvoke_m14746_GenericMethod;
// System.Int32 System.Comparison`1<System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo Comparison_1_EndInvoke_m14746_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Comparison_1_EndInvoke_m14746_gshared/* method */
	, &Comparison_1_t2833_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Comparison_1_t2833_Comparison_1_EndInvoke_m14746_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_EndInvoke_m14746_GenericMethod/* genericMethod */

};
static MethodInfo* Comparison_1_t2833_MethodInfos[] =
{
	&Comparison_1__ctor_m14743_MethodInfo,
	&Comparison_1_Invoke_m14744_MethodInfo,
	&Comparison_1_BeginInvoke_m14745_MethodInfo,
	&Comparison_1_EndInvoke_m14746_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
static MethodInfo* Comparison_1_t2833_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Comparison_1_Invoke_m14744_MethodInfo,
	&Comparison_1_BeginInvoke_m14745_MethodInfo,
	&Comparison_1_EndInvoke_m14746_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair Comparison_1_t2833_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparison_1_t2833_0_0_0;
extern Il2CppType Comparison_1_t2833_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct Comparison_1_t2833;
extern Il2CppGenericClass Comparison_1_t2833_GenericClass;
TypeInfo Comparison_1_t2833_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t2833_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparison_1_t2833_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Comparison_1_t2833_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparison_1_t2833_il2cpp_TypeInfo/* cast_class */
	, &Comparison_1_t2833_0_0_0/* byval_arg */
	, &Comparison_1_t2833_1_0_0/* this_arg */
	, Comparison_1_t2833_InterfacesOffsets/* interface_offsets */
	, &Comparison_1_t2833_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparison_1_t2833)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t2828_il2cpp_TypeInfo;

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>
extern MethodInfo ICollection_1_get_Count_m42842_MethodInfo;
static PropertyInfo ICollection_1_t2828____Count_PropertyInfo = 
{
	&ICollection_1_t2828_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42842_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43119_MethodInfo;
static PropertyInfo ICollection_1_t2828____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t2828_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43119_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t2828_PropertyInfos[] =
{
	&ICollection_1_t2828____Count_PropertyInfo,
	&ICollection_1_t2828____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42842_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m42842_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42842_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43119_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43119_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43119_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ICollection_1_t2828_ICollection_1_Add_m43120_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43120_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Add(T)
MethodInfo ICollection_1_Add_m43120_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t2828_ICollection_1_Add_m43120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43120_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43121_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Clear()
MethodInfo ICollection_1_Clear_m43121_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43121_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ICollection_1_t2828_ICollection_1_Contains_m43122_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43122_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m43122_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t2828_ICollection_1_Contains_m43122_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43122_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfoU5BU5D_t140_0_0_0;
extern Il2CppType MethodInfoU5BU5D_t140_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t2828_ICollection_1_CopyTo_m42843_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfoU5BU5D_t140_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42843_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42843_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t2828_ICollection_1_CopyTo_m42843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42843_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ICollection_1_t2828_ICollection_1_Remove_m43123_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43123_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m43123_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t2828_ICollection_1_Remove_m43123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43123_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t2828_MethodInfos[] =
{
	&ICollection_1_get_Count_m42842_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43119_MethodInfo,
	&ICollection_1_Add_m43120_MethodInfo,
	&ICollection_1_Clear_m43121_MethodInfo,
	&ICollection_1_Contains_m43122_MethodInfo,
	&ICollection_1_CopyTo_m42843_MethodInfo,
	&ICollection_1_Remove_m43123_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t2826_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t2828_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t2826_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t2828_0_0_0;
extern Il2CppType ICollection_1_t2828_1_0_0;
struct ICollection_1_t2828;
extern Il2CppGenericClass ICollection_1_t2828_GenericClass;
TypeInfo ICollection_1_t2828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t2828_MethodInfos/* methods */
	, ICollection_1_t2828_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t2828_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t2828_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t2828_0_0_0/* byval_arg */
	, &ICollection_1_t2828_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t2828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
extern Il2CppType IEnumerator_1_t2827_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42844_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42844_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t2826_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t2827_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42844_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t2826_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42844_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t2826_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t2826_0_0_0;
extern Il2CppType IEnumerable_1_t2826_1_0_0;
struct IEnumerable_1_t2826;
extern Il2CppGenericClass IEnumerable_1_t2826_GenericClass;
TypeInfo IEnumerable_1_t2826_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t2826_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t2826_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t2826_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t2826_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t2826_0_0_0/* byval_arg */
	, &IEnumerable_1_t2826_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t2826_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t2827_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>
extern MethodInfo IEnumerator_1_get_Current_m42845_MethodInfo;
static PropertyInfo IEnumerator_1_t2827____Current_PropertyInfo = 
{
	&IEnumerator_1_t2827_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t2827_PropertyInfos[] =
{
	&IEnumerator_1_t2827____Current_PropertyInfo,
	NULL
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42845_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42845_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t2827_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42845_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t2827_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42845_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t2827_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t2827_0_0_0;
extern Il2CppType IEnumerator_1_t2827_1_0_0;
struct IEnumerator_1_t2827;
extern Il2CppGenericClass IEnumerator_1_t2827_GenericClass;
TypeInfo IEnumerator_1_t2827_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t2827_MethodInfos/* methods */
	, IEnumerator_1_t2827_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t2827_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t2827_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t2827_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t2827_0_0_0/* byval_arg */
	, &IEnumerator_1_t2827_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t2827_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_53.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2863_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_53MethodDeclarations.h"

extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14751_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMethodInfo_t141_m32856_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodInfo>(System.Int32)
#define Array_InternalArray__get_Item_TisMethodInfo_t141_m32856(__this, p0, method) (MethodInfo_t141 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2863____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2863, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2863____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2863, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2863_FieldInfos[] =
{
	&InternalEnumerator_1_t2863____array_0_FieldInfo,
	&InternalEnumerator_1_t2863____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2863____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2863_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2863____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2863_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14751_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2863_PropertyInfos[] =
{
	&InternalEnumerator_1_t2863____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2863____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2863_InternalEnumerator_1__ctor_m14747_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14747_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14747_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2863_InternalEnumerator_1__ctor_m14747_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14747_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14749_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14749_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14749_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14750_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14750_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14750_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14751_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.MethodInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14751_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14751_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2863_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14747_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_MethodInfo,
	&InternalEnumerator_1_Dispose_m14749_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14750_MethodInfo,
	&InternalEnumerator_1_get_Current_m14751_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14750_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14749_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2863_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14748_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14750_MethodInfo,
	&InternalEnumerator_1_Dispose_m14749_MethodInfo,
	&InternalEnumerator_1_get_Current_m14751_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2863_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t2827_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2863_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t2827_il2cpp_TypeInfo, 7},
};
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2863_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14751_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMethodInfo_t141_m32856_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2863_0_0_0;
extern Il2CppType InternalEnumerator_1_t2863_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2863_GenericClass;
TypeInfo InternalEnumerator_1_t2863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2863_MethodInfos/* methods */
	, InternalEnumerator_1_t2863_PropertyInfos/* properties */
	, InternalEnumerator_1_t2863_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2863_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2863_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2863_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2863_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2863_1_0_0/* this_arg */
	, InternalEnumerator_1_t2863_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2863_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2863_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2863)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t2867_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodInfo>
extern MethodInfo IList_1_get_Item_m43124_MethodInfo;
extern MethodInfo IList_1_set_Item_m43125_MethodInfo;
static PropertyInfo IList_1_t2867____Item_PropertyInfo = 
{
	&IList_1_t2867_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43124_MethodInfo/* get */
	, &IList_1_set_Item_m43125_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t2867_PropertyInfos[] =
{
	&IList_1_t2867____Item_PropertyInfo,
	NULL
};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IList_1_t2867_IList_1_IndexOf_m43126_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43126_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43126_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t2867_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t2867_IList_1_IndexOf_m43126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43126_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IList_1_t2867_IList_1_Insert_m43127_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43127_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43127_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t2867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t2867_IList_1_Insert_m43127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43127_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t2867_IList_1_RemoveAt_m43128_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43128_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43128_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t2867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t2867_IList_1_RemoveAt_m43128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43128_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t2867_IList_1_get_Item_m43124_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43124_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43124_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t2867_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t2867_IList_1_get_Item_m43124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43124_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IList_1_t2867_IList_1_set_Item_m43125_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43125_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43125_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t2867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t2867_IList_1_set_Item_m43125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43125_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t2867_MethodInfos[] =
{
	&IList_1_IndexOf_m43126_MethodInfo,
	&IList_1_Insert_m43127_MethodInfo,
	&IList_1_RemoveAt_m43128_MethodInfo,
	&IList_1_get_Item_m43124_MethodInfo,
	&IList_1_set_Item_m43125_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t2867_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t2828_il2cpp_TypeInfo,
	&IEnumerable_1_t2826_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t2867_0_0_0;
extern Il2CppType IList_1_t2867_1_0_0;
struct IList_1_t2867;
extern Il2CppGenericClass IList_1_t2867_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t2867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t2867_MethodInfos/* methods */
	, IList_1_t2867_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t2867_il2cpp_TypeInfo/* element_class */
	, IList_1_t2867_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t2867_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t2867_0_0_0/* byval_arg */
	, &IList_1_t2867_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t2867_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7673_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>
extern MethodInfo ICollection_1_get_Count_m43129_MethodInfo;
static PropertyInfo ICollection_1_t7673____Count_PropertyInfo = 
{
	&ICollection_1_t7673_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43129_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43130_MethodInfo;
static PropertyInfo ICollection_1_t7673____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7673_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43130_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7673_PropertyInfos[] =
{
	&ICollection_1_t7673____Count_PropertyInfo,
	&ICollection_1_t7673____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43129_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m43129_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43129_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43130_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43130_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43130_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodInfo_t2656_0_0_0;
extern Il2CppType _MethodInfo_t2656_0_0_0;
static ParameterInfo ICollection_1_t7673_ICollection_1_Add_m43131_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodInfo_t2656_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43131_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Add(T)
MethodInfo ICollection_1_Add_m43131_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7673_ICollection_1_Add_m43131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43131_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43132_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Clear()
MethodInfo ICollection_1_Clear_m43132_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43132_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodInfo_t2656_0_0_0;
static ParameterInfo ICollection_1_t7673_ICollection_1_Contains_m43133_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodInfo_t2656_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43133_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m43133_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7673_ICollection_1_Contains_m43133_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43133_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodInfoU5BU5D_t5426_0_0_0;
extern Il2CppType _MethodInfoU5BU5D_t5426_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7673_ICollection_1_CopyTo_m43134_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_MethodInfoU5BU5D_t5426_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43134_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43134_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7673_ICollection_1_CopyTo_m43134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43134_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodInfo_t2656_0_0_0;
static ParameterInfo ICollection_1_t7673_ICollection_1_Remove_m43135_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodInfo_t2656_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43135_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m43135_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7673_ICollection_1_Remove_m43135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43135_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7673_MethodInfos[] =
{
	&ICollection_1_get_Count_m43129_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43130_MethodInfo,
	&ICollection_1_Add_m43131_MethodInfo,
	&ICollection_1_Clear_m43132_MethodInfo,
	&ICollection_1_Contains_m43133_MethodInfo,
	&ICollection_1_CopyTo_m43134_MethodInfo,
	&ICollection_1_Remove_m43135_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7675_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7673_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7675_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7673_0_0_0;
extern Il2CppType ICollection_1_t7673_1_0_0;
struct ICollection_1_t7673;
extern Il2CppGenericClass ICollection_1_t7673_GenericClass;
TypeInfo ICollection_1_t7673_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7673_MethodInfos/* methods */
	, ICollection_1_t7673_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7673_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7673_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7673_0_0_0/* byval_arg */
	, &ICollection_1_t7673_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7673_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodInfo>
extern Il2CppType IEnumerator_1_t6029_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43136_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43136_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7675_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43136_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7675_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43136_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7675_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7675_0_0_0;
extern Il2CppType IEnumerable_1_t7675_1_0_0;
struct IEnumerable_1_t7675;
extern Il2CppGenericClass IEnumerable_1_t7675_GenericClass;
TypeInfo IEnumerable_1_t7675_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7675_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7675_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7675_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7675_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7675_0_0_0/* byval_arg */
	, &IEnumerable_1_t7675_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7675_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6029_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodInfo>
extern MethodInfo IEnumerator_1_get_Current_m43137_MethodInfo;
static PropertyInfo IEnumerator_1_t6029____Current_PropertyInfo = 
{
	&IEnumerator_1_t6029_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43137_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6029_PropertyInfos[] =
{
	&IEnumerator_1_t6029____Current_PropertyInfo,
	NULL
};
extern Il2CppType _MethodInfo_t2656_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43137_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43137_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6029_il2cpp_TypeInfo/* declaring_type */
	, &_MethodInfo_t2656_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43137_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6029_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43137_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6029_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6029_0_0_0;
extern Il2CppType IEnumerator_1_t6029_1_0_0;
struct IEnumerator_1_t6029;
extern Il2CppGenericClass IEnumerator_1_t6029_GenericClass;
TypeInfo IEnumerator_1_t6029_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6029_MethodInfos/* methods */
	, IEnumerator_1_t6029_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6029_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6029_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6029_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6029_0_0_0/* byval_arg */
	, &IEnumerator_1_t6029_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6029_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2864_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54MethodDeclarations.h"

extern TypeInfo _MethodInfo_t2656_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14756_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_MethodInfo_t2656_m32867_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._MethodInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._MethodInfo>(System.Int32)
#define Array_InternalArray__get_Item_Tis_MethodInfo_t2656_m32867(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2864____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2864, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2864____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2864, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2864_FieldInfos[] =
{
	&InternalEnumerator_1_t2864____array_0_FieldInfo,
	&InternalEnumerator_1_t2864____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2864____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2864_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2864____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2864_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14756_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2864_PropertyInfos[] =
{
	&InternalEnumerator_1_t2864____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2864____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2864_InternalEnumerator_1__ctor_m14752_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14752_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14752_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2864_InternalEnumerator_1__ctor_m14752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14752_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14754_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14754_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14754_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14755_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14755_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14755_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodInfo_t2656_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14756_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14756_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* declaring_type */
	, &_MethodInfo_t2656_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14756_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2864_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14752_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_MethodInfo,
	&InternalEnumerator_1_Dispose_m14754_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14755_MethodInfo,
	&InternalEnumerator_1_get_Current_m14756_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14755_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14754_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2864_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14753_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14755_MethodInfo,
	&InternalEnumerator_1_Dispose_m14754_MethodInfo,
	&InternalEnumerator_1_get_Current_m14756_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2864_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6029_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2864_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6029_il2cpp_TypeInfo, 7},
};
extern TypeInfo _MethodInfo_t2656_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2864_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14756_MethodInfo/* Method Usage */,
	&_MethodInfo_t2656_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_MethodInfo_t2656_m32867_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2864_0_0_0;
extern Il2CppType InternalEnumerator_1_t2864_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2864_GenericClass;
TypeInfo InternalEnumerator_1_t2864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2864_MethodInfos/* methods */
	, InternalEnumerator_1_t2864_PropertyInfos/* properties */
	, InternalEnumerator_1_t2864_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2864_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2864_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2864_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2864_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2864_1_0_0/* this_arg */
	, InternalEnumerator_1_t2864_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2864_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2864_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2864)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7674_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>
extern MethodInfo IList_1_get_Item_m43138_MethodInfo;
extern MethodInfo IList_1_set_Item_m43139_MethodInfo;
static PropertyInfo IList_1_t7674____Item_PropertyInfo = 
{
	&IList_1_t7674_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43138_MethodInfo/* get */
	, &IList_1_set_Item_m43139_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7674_PropertyInfos[] =
{
	&IList_1_t7674____Item_PropertyInfo,
	NULL
};
extern Il2CppType _MethodInfo_t2656_0_0_0;
static ParameterInfo IList_1_t7674_IList_1_IndexOf_m43140_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodInfo_t2656_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43140_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43140_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7674_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7674_IList_1_IndexOf_m43140_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43140_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _MethodInfo_t2656_0_0_0;
static ParameterInfo IList_1_t7674_IList_1_Insert_m43141_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_MethodInfo_t2656_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43141_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43141_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7674_IList_1_Insert_m43141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43141_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7674_IList_1_RemoveAt_m43142_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43142_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43142_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7674_IList_1_RemoveAt_m43142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43142_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7674_IList_1_get_Item_m43138_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType _MethodInfo_t2656_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43138_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43138_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7674_il2cpp_TypeInfo/* declaring_type */
	, &_MethodInfo_t2656_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7674_IList_1_get_Item_m43138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43138_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _MethodInfo_t2656_0_0_0;
static ParameterInfo IList_1_t7674_IList_1_set_Item_m43139_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_MethodInfo_t2656_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43139_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43139_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7674_IList_1_set_Item_m43139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43139_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7674_MethodInfos[] =
{
	&IList_1_IndexOf_m43140_MethodInfo,
	&IList_1_Insert_m43141_MethodInfo,
	&IList_1_RemoveAt_m43142_MethodInfo,
	&IList_1_get_Item_m43138_MethodInfo,
	&IList_1_set_Item_m43139_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7674_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7673_il2cpp_TypeInfo,
	&IEnumerable_1_t7675_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7674_0_0_0;
extern Il2CppType IList_1_t7674_1_0_0;
struct IList_1_t7674;
extern Il2CppGenericClass IList_1_t7674_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7674_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7674_MethodInfos/* methods */
	, IList_1_t7674_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7674_il2cpp_TypeInfo/* element_class */
	, IList_1_t7674_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7674_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7674_0_0_0/* byval_arg */
	, &IList_1_t7674_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7674_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7676_il2cpp_TypeInfo;

// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>
extern MethodInfo ICollection_1_get_Count_m43143_MethodInfo;
static PropertyInfo ICollection_1_t7676____Count_PropertyInfo = 
{
	&ICollection_1_t7676_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43144_MethodInfo;
static PropertyInfo ICollection_1_t7676____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7676_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43144_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7676_PropertyInfos[] =
{
	&ICollection_1_t7676____Count_PropertyInfo,
	&ICollection_1_t7676____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43143_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::get_Count()
MethodInfo ICollection_1_get_Count_m43143_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43143_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43144_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43144_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43144_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBase_t1220_0_0_0;
extern Il2CppType MethodBase_t1220_0_0_0;
static ParameterInfo ICollection_1_t7676_ICollection_1_Add_m43145_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBase_t1220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43145_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Add(T)
MethodInfo ICollection_1_Add_m43145_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7676_ICollection_1_Add_m43145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43145_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43146_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Clear()
MethodInfo ICollection_1_Clear_m43146_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43146_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBase_t1220_0_0_0;
static ParameterInfo ICollection_1_t7676_ICollection_1_Contains_m43147_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBase_t1220_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43147_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Contains(T)
MethodInfo ICollection_1_Contains_m43147_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7676_ICollection_1_Contains_m43147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43147_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBaseU5BU5D_t1975_0_0_0;
extern Il2CppType MethodBaseU5BU5D_t1975_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7676_ICollection_1_CopyTo_m43148_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodBaseU5BU5D_t1975_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43148_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43148_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7676_ICollection_1_CopyTo_m43148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43148_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBase_t1220_0_0_0;
static ParameterInfo ICollection_1_t7676_ICollection_1_Remove_m43149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBase_t1220_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43149_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodBase>::Remove(T)
MethodInfo ICollection_1_Remove_m43149_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7676_ICollection_1_Remove_m43149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43149_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7676_MethodInfos[] =
{
	&ICollection_1_get_Count_m43143_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43144_MethodInfo,
	&ICollection_1_Add_m43145_MethodInfo,
	&ICollection_1_Clear_m43146_MethodInfo,
	&ICollection_1_Contains_m43147_MethodInfo,
	&ICollection_1_CopyTo_m43148_MethodInfo,
	&ICollection_1_Remove_m43149_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7678_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7676_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7678_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7676_0_0_0;
extern Il2CppType ICollection_1_t7676_1_0_0;
struct ICollection_1_t7676;
extern Il2CppGenericClass ICollection_1_t7676_GenericClass;
TypeInfo ICollection_1_t7676_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7676_MethodInfos/* methods */
	, ICollection_1_t7676_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7676_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7676_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7676_0_0_0/* byval_arg */
	, &ICollection_1_t7676_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7676_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodBase>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodBase>
extern Il2CppType IEnumerator_1_t6030_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43150_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodBase>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43150_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7678_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6030_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43150_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7678_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43150_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7678_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7678_0_0_0;
extern Il2CppType IEnumerable_1_t7678_1_0_0;
struct IEnumerable_1_t7678;
extern Il2CppGenericClass IEnumerable_1_t7678_GenericClass;
TypeInfo IEnumerable_1_t7678_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7678_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7678_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7678_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7678_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7678_0_0_0/* byval_arg */
	, &IEnumerable_1_t7678_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7678_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6030_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodBase>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodBase>
extern MethodInfo IEnumerator_1_get_Current_m43151_MethodInfo;
static PropertyInfo IEnumerator_1_t6030____Current_PropertyInfo = 
{
	&IEnumerator_1_t6030_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43151_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6030_PropertyInfos[] =
{
	&IEnumerator_1_t6030____Current_PropertyInfo,
	NULL
};
extern Il2CppType MethodBase_t1220_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43151_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodBase>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43151_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6030_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43151_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6030_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43151_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6030_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6030_0_0_0;
extern Il2CppType IEnumerator_1_t6030_1_0_0;
struct IEnumerator_1_t6030;
extern Il2CppGenericClass IEnumerator_1_t6030_GenericClass;
TypeInfo IEnumerator_1_t6030_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6030_MethodInfos/* methods */
	, IEnumerator_1_t6030_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6030_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6030_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6030_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6030_0_0_0/* byval_arg */
	, &IEnumerator_1_t6030_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6030_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.MethodBase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_55.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2865_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.MethodBase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_55MethodDeclarations.h"

extern TypeInfo MethodBase_t1220_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14761_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMethodBase_t1220_m32878_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodBase>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodBase>(System.Int32)
#define Array_InternalArray__get_Item_TisMethodBase_t1220_m32878(__this, p0, method) (MethodBase_t1220 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodBase>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2865____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2865, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2865____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2865, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2865_FieldInfos[] =
{
	&InternalEnumerator_1_t2865____array_0_FieldInfo,
	&InternalEnumerator_1_t2865____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2865____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2865_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2865____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2865_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2865_PropertyInfos[] =
{
	&InternalEnumerator_1_t2865____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2865____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2865_InternalEnumerator_1__ctor_m14757_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14757_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14757_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2865_InternalEnumerator_1__ctor_m14757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14757_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14759_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14759_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14759_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14760_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14760_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14760_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBase_t1220_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14761_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.MethodBase>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14761_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14761_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2865_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14757_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_MethodInfo,
	&InternalEnumerator_1_Dispose_m14759_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14760_MethodInfo,
	&InternalEnumerator_1_get_Current_m14761_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14760_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14759_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2865_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14758_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14760_MethodInfo,
	&InternalEnumerator_1_Dispose_m14759_MethodInfo,
	&InternalEnumerator_1_get_Current_m14761_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2865_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6030_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2865_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6030_il2cpp_TypeInfo, 7},
};
extern TypeInfo MethodBase_t1220_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2865_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14761_MethodInfo/* Method Usage */,
	&MethodBase_t1220_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMethodBase_t1220_m32878_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2865_0_0_0;
extern Il2CppType InternalEnumerator_1_t2865_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2865_GenericClass;
TypeInfo InternalEnumerator_1_t2865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2865_MethodInfos/* methods */
	, InternalEnumerator_1_t2865_PropertyInfos/* properties */
	, InternalEnumerator_1_t2865_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2865_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2865_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2865_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2865_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2865_1_0_0/* this_arg */
	, InternalEnumerator_1_t2865_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2865_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2865_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2865)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7677_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodBase>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodBase>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodBase>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.MethodBase>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodBase>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodBase>
extern MethodInfo IList_1_get_Item_m43152_MethodInfo;
extern MethodInfo IList_1_set_Item_m43153_MethodInfo;
static PropertyInfo IList_1_t7677____Item_PropertyInfo = 
{
	&IList_1_t7677_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43152_MethodInfo/* get */
	, &IList_1_set_Item_m43153_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7677_PropertyInfos[] =
{
	&IList_1_t7677____Item_PropertyInfo,
	NULL
};
extern Il2CppType MethodBase_t1220_0_0_0;
static ParameterInfo IList_1_t7677_IList_1_IndexOf_m43154_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBase_t1220_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43154_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodBase>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43154_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7677_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7677_IList_1_IndexOf_m43154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43154_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodBase_t1220_0_0_0;
static ParameterInfo IList_1_t7677_IList_1_Insert_m43155_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodBase_t1220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43155_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodBase>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43155_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7677_IList_1_Insert_m43155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43155_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7677_IList_1_RemoveAt_m43156_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43156_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodBase>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43156_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7677_IList_1_RemoveAt_m43156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43156_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7677_IList_1_get_Item_m43152_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodBase_t1220_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43152_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.MethodBase>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43152_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7677_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1220_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7677_IList_1_get_Item_m43152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43152_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodBase_t1220_0_0_0;
static ParameterInfo IList_1_t7677_IList_1_set_Item_m43153_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodBase_t1220_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43153_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodBase>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43153_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7677_IList_1_set_Item_m43153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43153_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7677_MethodInfos[] =
{
	&IList_1_IndexOf_m43154_MethodInfo,
	&IList_1_Insert_m43155_MethodInfo,
	&IList_1_RemoveAt_m43156_MethodInfo,
	&IList_1_get_Item_m43152_MethodInfo,
	&IList_1_set_Item_m43153_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7677_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7676_il2cpp_TypeInfo,
	&IEnumerable_1_t7678_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7677_0_0_0;
extern Il2CppType IList_1_t7677_1_0_0;
struct IList_1_t7677;
extern Il2CppGenericClass IList_1_t7677_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7677_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7677_MethodInfos/* methods */
	, IList_1_t7677_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7677_il2cpp_TypeInfo/* element_class */
	, IList_1_t7677_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7677_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7677_0_0_0/* byval_arg */
	, &IList_1_t7677_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7677_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7679_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>
extern MethodInfo ICollection_1_get_Count_m43157_MethodInfo;
static PropertyInfo ICollection_1_t7679____Count_PropertyInfo = 
{
	&ICollection_1_t7679_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43157_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43158_MethodInfo;
static PropertyInfo ICollection_1_t7679____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7679_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43158_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7679_PropertyInfos[] =
{
	&ICollection_1_t7679____Count_PropertyInfo,
	&ICollection_1_t7679____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43157_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::get_Count()
MethodInfo ICollection_1_get_Count_m43157_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43157_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43158_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43158_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43158_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBase_t2651_0_0_0;
extern Il2CppType _MethodBase_t2651_0_0_0;
static ParameterInfo ICollection_1_t7679_ICollection_1_Add_m43159_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBase_t2651_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43159_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Add(T)
MethodInfo ICollection_1_Add_m43159_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7679_ICollection_1_Add_m43159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43159_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43160_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Clear()
MethodInfo ICollection_1_Clear_m43160_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43160_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBase_t2651_0_0_0;
static ParameterInfo ICollection_1_t7679_ICollection_1_Contains_m43161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBase_t2651_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43161_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Contains(T)
MethodInfo ICollection_1_Contains_m43161_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7679_ICollection_1_Contains_m43161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43161_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBaseU5BU5D_t5427_0_0_0;
extern Il2CppType _MethodBaseU5BU5D_t5427_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7679_ICollection_1_CopyTo_m43162_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBaseU5BU5D_t5427_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43162_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43162_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7679_ICollection_1_CopyTo_m43162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43162_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBase_t2651_0_0_0;
static ParameterInfo ICollection_1_t7679_ICollection_1_Remove_m43163_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBase_t2651_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43163_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBase>::Remove(T)
MethodInfo ICollection_1_Remove_m43163_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7679_ICollection_1_Remove_m43163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43163_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7679_MethodInfos[] =
{
	&ICollection_1_get_Count_m43157_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43158_MethodInfo,
	&ICollection_1_Add_m43159_MethodInfo,
	&ICollection_1_Clear_m43160_MethodInfo,
	&ICollection_1_Contains_m43161_MethodInfo,
	&ICollection_1_CopyTo_m43162_MethodInfo,
	&ICollection_1_Remove_m43163_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7681_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7679_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7681_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7679_0_0_0;
extern Il2CppType ICollection_1_t7679_1_0_0;
struct ICollection_1_t7679;
extern Il2CppGenericClass ICollection_1_t7679_GenericClass;
TypeInfo ICollection_1_t7679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7679_MethodInfos/* methods */
	, ICollection_1_t7679_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7679_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7679_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7679_0_0_0/* byval_arg */
	, &ICollection_1_t7679_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7679_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBase>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBase>
extern Il2CppType IEnumerator_1_t6032_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43164_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBase>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43164_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7681_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6032_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43164_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7681_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43164_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7681_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7681_0_0_0;
extern Il2CppType IEnumerable_1_t7681_1_0_0;
struct IEnumerable_1_t7681;
extern Il2CppGenericClass IEnumerable_1_t7681_GenericClass;
TypeInfo IEnumerable_1_t7681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7681_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7681_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7681_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7681_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7681_0_0_0/* byval_arg */
	, &IEnumerable_1_t7681_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7681_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6032_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBase>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBase>
extern MethodInfo IEnumerator_1_get_Current_m43165_MethodInfo;
static PropertyInfo IEnumerator_1_t6032____Current_PropertyInfo = 
{
	&IEnumerator_1_t6032_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43165_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6032_PropertyInfos[] =
{
	&IEnumerator_1_t6032____Current_PropertyInfo,
	NULL
};
extern Il2CppType _MethodBase_t2651_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43165_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBase>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43165_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6032_il2cpp_TypeInfo/* declaring_type */
	, &_MethodBase_t2651_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43165_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6032_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43165_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6032_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6032_0_0_0;
extern Il2CppType IEnumerator_1_t6032_1_0_0;
struct IEnumerator_1_t6032;
extern Il2CppGenericClass IEnumerator_1_t6032_GenericClass;
TypeInfo IEnumerator_1_t6032_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6032_MethodInfos/* methods */
	, IEnumerator_1_t6032_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6032_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6032_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6032_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6032_0_0_0/* byval_arg */
	, &IEnumerator_1_t6032_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6032_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_56.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2866_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_56MethodDeclarations.h"

extern TypeInfo _MethodBase_t2651_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14766_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_MethodBase_t2651_m32889_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._MethodBase>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._MethodBase>(System.Int32)
#define Array_InternalArray__get_Item_Tis_MethodBase_t2651_m32889(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2866____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2866, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2866____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2866, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2866_FieldInfos[] =
{
	&InternalEnumerator_1_t2866____array_0_FieldInfo,
	&InternalEnumerator_1_t2866____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2866____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2866_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2866____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2866_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2866_PropertyInfos[] =
{
	&InternalEnumerator_1_t2866____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2866____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2866_InternalEnumerator_1__ctor_m14762_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14762_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14762_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2866_InternalEnumerator_1__ctor_m14762_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14762_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14764_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14764_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14764_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14765_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14765_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14765_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBase_t2651_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14766_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBase>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14766_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* declaring_type */
	, &_MethodBase_t2651_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14766_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2866_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14762_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_MethodInfo,
	&InternalEnumerator_1_Dispose_m14764_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14765_MethodInfo,
	&InternalEnumerator_1_get_Current_m14766_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14765_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14764_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2866_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14763_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14765_MethodInfo,
	&InternalEnumerator_1_Dispose_m14764_MethodInfo,
	&InternalEnumerator_1_get_Current_m14766_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2866_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6032_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2866_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6032_il2cpp_TypeInfo, 7},
};
extern TypeInfo _MethodBase_t2651_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2866_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14766_MethodInfo/* Method Usage */,
	&_MethodBase_t2651_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_MethodBase_t2651_m32889_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2866_0_0_0;
extern Il2CppType InternalEnumerator_1_t2866_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2866_GenericClass;
TypeInfo InternalEnumerator_1_t2866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2866_MethodInfos/* methods */
	, InternalEnumerator_1_t2866_PropertyInfos/* properties */
	, InternalEnumerator_1_t2866_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2866_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2866_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2866_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2866_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2866_1_0_0/* this_arg */
	, InternalEnumerator_1_t2866_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2866_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2866_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2866)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7680_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>
extern MethodInfo IList_1_get_Item_m43166_MethodInfo;
extern MethodInfo IList_1_set_Item_m43167_MethodInfo;
static PropertyInfo IList_1_t7680____Item_PropertyInfo = 
{
	&IList_1_t7680_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43166_MethodInfo/* get */
	, &IList_1_set_Item_m43167_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7680_PropertyInfos[] =
{
	&IList_1_t7680____Item_PropertyInfo,
	NULL
};
extern Il2CppType _MethodBase_t2651_0_0_0;
static ParameterInfo IList_1_t7680_IList_1_IndexOf_m43168_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBase_t2651_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43168_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43168_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7680_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7680_IList_1_IndexOf_m43168_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43168_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _MethodBase_t2651_0_0_0;
static ParameterInfo IList_1_t7680_IList_1_Insert_m43169_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_MethodBase_t2651_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43169_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43169_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7680_IList_1_Insert_m43169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43169_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7680_IList_1_RemoveAt_m43170_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43170_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43170_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7680_IList_1_RemoveAt_m43170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43170_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7680_IList_1_get_Item_m43166_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType _MethodBase_t2651_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43166_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43166_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7680_il2cpp_TypeInfo/* declaring_type */
	, &_MethodBase_t2651_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7680_IList_1_get_Item_m43166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43166_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _MethodBase_t2651_0_0_0;
static ParameterInfo IList_1_t7680_IList_1_set_Item_m43167_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_MethodBase_t2651_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43167_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBase>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43167_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7680_IList_1_set_Item_m43167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43167_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7680_MethodInfos[] =
{
	&IList_1_IndexOf_m43168_MethodInfo,
	&IList_1_Insert_m43169_MethodInfo,
	&IList_1_RemoveAt_m43170_MethodInfo,
	&IList_1_get_Item_m43166_MethodInfo,
	&IList_1_set_Item_m43167_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7680_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7679_il2cpp_TypeInfo,
	&IEnumerable_1_t7681_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7680_0_0_0;
extern Il2CppType IList_1_t7680_1_0_0;
struct IList_1_t7680;
extern Il2CppGenericClass IList_1_t7680_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7680_MethodInfos/* methods */
	, IList_1_t7680_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7680_il2cpp_TypeInfo/* element_class */
	, IList_1_t7680_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7680_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7680_0_0_0/* byval_arg */
	, &IList_1_t7680_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7680_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Enumerator_t143_il2cpp_TypeInfo;
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"

// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
extern TypeInfo List_1_t150_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo ObjectDisposedException_t1718_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern MethodInfo Enumerator_VerifyState_m14770_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7835_MethodInfo;
extern MethodInfo Object_GetType_m323_MethodInfo;
extern MethodInfo Type_get_FullName_m6757_MethodInfo;
extern MethodInfo ObjectDisposedException__ctor_m9005_MethodInfo;


// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::.ctor(System.Collections.Generic.List`1<T>)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::Dispose()
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::VerifyState()
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::MoveNext()
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::get_Current()
// Metadata Definition System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
extern Il2CppType List_1_t150_0_0_1;
FieldInfo Enumerator_t143____l_0_FieldInfo = 
{
	"l"/* name */
	, &List_1_t150_0_0_1/* type */
	, &Enumerator_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t143, ___l_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Enumerator_t143____next_1_FieldInfo = 
{
	"next"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Enumerator_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t143, ___next_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Enumerator_t143____ver_2_FieldInfo = 
{
	"ver"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Enumerator_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t143, ___ver_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType MethodInfo_t141_0_0_1;
FieldInfo Enumerator_t143____current_3_FieldInfo = 
{
	"current"/* name */
	, &MethodInfo_t141_0_0_1/* type */
	, &Enumerator_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t143, ___current_3) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t143_FieldInfos[] =
{
	&Enumerator_t143____l_0_FieldInfo,
	&Enumerator_t143____next_1_FieldInfo,
	&Enumerator_t143____ver_2_FieldInfo,
	&Enumerator_t143____current_3_FieldInfo,
	NULL
};
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m14768_MethodInfo;
static PropertyInfo Enumerator_t143____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t143_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m14768_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Enumerator_get_Current_m328_MethodInfo;
static PropertyInfo Enumerator_t143____Current_PropertyInfo = 
{
	&Enumerator_t143_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t143_PropertyInfos[] =
{
	&Enumerator_t143____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t143____Current_PropertyInfo,
	NULL
};
extern Il2CppType List_1_t150_0_0_0;
extern Il2CppType List_1_t150_0_0_0;
static ParameterInfo Enumerator_t143_Enumerator__ctor_m14767_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t150_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator__ctor_m14767_GenericMethod;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::.ctor(System.Collections.Generic.List`1<T>)
MethodInfo Enumerator__ctor_m14767_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m14558_gshared/* method */
	, &Enumerator_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Enumerator_t143_Enumerator__ctor_m14767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator__ctor_m14767_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_System_Collections_IEnumerator_get_Current_m14768_GenericMethod;
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m14768_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared/* method */
	, &Enumerator_t143_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m14768_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_Dispose_m14769_GenericMethod;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::Dispose()
MethodInfo Enumerator_Dispose_m14769_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&Enumerator_Dispose_m14560_gshared/* method */
	, &Enumerator_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_Dispose_m14769_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_VerifyState_m14770_GenericMethod;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::VerifyState()
MethodInfo Enumerator_VerifyState_m14770_MethodInfo = 
{
	"VerifyState"/* name */
	, (methodPointerType)&Enumerator_VerifyState_m14561_gshared/* method */
	, &Enumerator_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_VerifyState_m14770_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_MoveNext_m332_GenericMethod;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::MoveNext()
MethodInfo Enumerator_MoveNext_m332_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m14562_gshared/* method */
	, &Enumerator_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_MoveNext_m332_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_get_Current_m328_GenericMethod;
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::get_Current()
MethodInfo Enumerator_get_Current_m328_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m14563_gshared/* method */
	, &Enumerator_t143_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_get_Current_m328_GenericMethod/* genericMethod */

};
static MethodInfo* Enumerator_t143_MethodInfos[] =
{
	&Enumerator__ctor_m14767_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m14768_MethodInfo,
	&Enumerator_Dispose_m14769_MethodInfo,
	&Enumerator_VerifyState_m14770_MethodInfo,
	&Enumerator_MoveNext_m332_MethodInfo,
	&Enumerator_get_Current_m328_MethodInfo,
	NULL
};
extern MethodInfo Enumerator_MoveNext_m332_MethodInfo;
extern MethodInfo Enumerator_Dispose_m14769_MethodInfo;
static MethodInfo* Enumerator_t143_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m14768_MethodInfo,
	&Enumerator_MoveNext_m332_MethodInfo,
	&Enumerator_Dispose_m14769_MethodInfo,
	&Enumerator_get_Current_m328_MethodInfo,
};
static TypeInfo* Enumerator_t143_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t2827_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Enumerator_t143_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t2827_il2cpp_TypeInfo, 7},
};
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t143_il2cpp_TypeInfo;
static Il2CppRGCTXData Enumerator_t143_RGCTXData[3] = 
{
	&Enumerator_VerifyState_m14770_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&Enumerator_t143_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Enumerator_t143_0_0_0;
extern Il2CppType Enumerator_t143_1_0_0;
extern Il2CppGenericClass Enumerator_t143_GenericClass;
extern TypeInfo List_1_t1871_il2cpp_TypeInfo;
TypeInfo Enumerator_t143_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t143_MethodInfos/* methods */
	, Enumerator_t143_PropertyInfos/* properties */
	, Enumerator_t143_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &List_1_t1871_il2cpp_TypeInfo/* nested_in */
	, &Enumerator_t143_il2cpp_TypeInfo/* element_class */
	, Enumerator_t143_InterfacesTypeInfos/* implemented_interfaces */
	, Enumerator_t143_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Enumerator_t143_il2cpp_TypeInfo/* cast_class */
	, &Enumerator_t143_0_0_0/* byval_arg */
	, &Enumerator_t143_1_0_0/* this_arg */
	, Enumerator_t143_InterfacesOffsets/* interface_offsets */
	, &Enumerator_t143_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Enumerator_t143_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t143)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ReadOnlyCollection_1_t2829_il2cpp_TypeInfo;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_1MethodDeclarations.h"

// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern TypeInfo NotSupportedException_t498_il2cpp_TypeInfo;
extern TypeInfo ArgumentNullException_t1224_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1259_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo MethodInfoU5BU5D_t140_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_0MethodDeclarations.h"
extern MethodInfo ReadOnlyCollection_1_get_Item_m14800_MethodInfo;
extern MethodInfo NotSupportedException__ctor_m2252_MethodInfo;
extern MethodInfo Object__ctor_m312_MethodInfo;
extern MethodInfo ArgumentNullException__ctor_m6710_MethodInfo;
extern MethodInfo ICollection_CopyTo_m7824_MethodInfo;
extern MethodInfo IEnumerable_GetEnumerator_m8011_MethodInfo;
extern MethodInfo Collection_1_IsValidItem_m14832_MethodInfo;
extern MethodInfo ICollection_1_Contains_m43122_MethodInfo;
extern MethodInfo IList_1_IndexOf_m43126_MethodInfo;
extern MethodInfo ICollection_1_CopyTo_m42843_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m42844_MethodInfo;


// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.Add(T)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.Clear()
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IEnumerable.GetEnumerator()
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Add(System.Object)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Clear()
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Contains(System.Object)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.IndexOf(System.Object)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Remove(System.Object)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.RemoveAt(System.Int32)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_SyncRoot()
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsFixedSize()
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsReadOnly()
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_Item(System.Int32)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::Contains(T)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::CopyTo(T[],System.Int32)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::GetEnumerator()
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::IndexOf(T)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::get_Count()
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::get_Item(System.Int32)
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>
extern Il2CppType IList_1_t2867_0_0_1;
FieldInfo ReadOnlyCollection_1_t2829____list_0_FieldInfo = 
{
	"list"/* name */
	, &IList_1_t2867_0_0_1/* type */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, offsetof(ReadOnlyCollection_1_t2829, ___list_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ReadOnlyCollection_1_t2829_FieldInfos[] =
{
	&ReadOnlyCollection_1_t2829____list_0_FieldInfo,
	NULL
};
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IList<T>.Item"/* name */
	, &ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_MethodInfo/* get */
	, &ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsFixedSize"/* name */
	, &ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsReadOnly"/* name */
	, &ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____System_Collections_IList_Item_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.Item"/* name */
	, &ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_MethodInfo/* get */
	, &ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ReadOnlyCollection_1_get_Count_m14799_MethodInfo;
static PropertyInfo ReadOnlyCollection_1_t2829____Count_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ReadOnlyCollection_1_get_Count_m14799_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ReadOnlyCollection_1_t2829____Item_PropertyInfo = 
{
	&ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ReadOnlyCollection_1_get_Item_m14800_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ReadOnlyCollection_1_t2829_PropertyInfos[] =
{
	&ReadOnlyCollection_1_t2829____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&ReadOnlyCollection_1_t2829____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&ReadOnlyCollection_1_t2829____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&ReadOnlyCollection_1_t2829____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&ReadOnlyCollection_1_t2829____System_Collections_IList_IsFixedSize_PropertyInfo,
	&ReadOnlyCollection_1_t2829____System_Collections_IList_IsReadOnly_PropertyInfo,
	&ReadOnlyCollection_1_t2829____System_Collections_IList_Item_PropertyInfo,
	&ReadOnlyCollection_1_t2829____Count_PropertyInfo,
	&ReadOnlyCollection_1_t2829____Item_PropertyInfo,
	NULL
};
extern Il2CppType IList_1_t2867_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1__ctor_m14771_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t2867_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1__ctor_m14771_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::.ctor(System.Collections.Generic.IList`1<T>)
MethodInfo ReadOnlyCollection_1__ctor_m14771_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReadOnlyCollection_1__ctor_m14564_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1__ctor_m14771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1__ctor_m14771_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.Add(T)
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14565_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14773_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.Clear()
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14773_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Clear"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14566_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14773_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_MethodInfo = 
{
	"System.Collections.Generic.IList<T>.Insert"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14567_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Remove"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14568_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_MethodInfo = 
{
	"System.Collections.Generic.IList<T>.RemoveAt"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14569_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_GenericMethod;
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_MethodInfo = 
{
	"System.Collections.Generic.IList<T>.get_Item"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14570_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_MethodInfo = 
{
	"System.Collections.Generic.IList<T>.set_Item"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14571_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14572_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14573_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14781_GenericMethod;
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14781_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14574_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14781_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Add_m14782_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_Add_m14782_GenericMethod;
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Add(System.Object)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_Add_m14782_MethodInfo = 
{
	"System.Collections.IList.Add"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m14575_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Add_m14782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_Add_m14782_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_Clear_m14783_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Clear()
MethodInfo ReadOnlyCollection_1_System_Collections_IList_Clear_m14783_MethodInfo = 
{
	"System.Collections.IList.Clear"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m14576_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_Clear_m14783_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Contains(System.Object)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_MethodInfo = 
{
	"System.Collections.IList.Contains"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m14577_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_GenericMethod;
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.IndexOf(System.Object)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_MethodInfo = 
{
	"System.Collections.IList.IndexOf"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14578_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_MethodInfo = 
{
	"System.Collections.IList.Insert"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m14579_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.Remove(System.Object)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_MethodInfo = 
{
	"System.Collections.IList.Remove"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m14580_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.RemoveAt(System.Int32)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_MethodInfo = 
{
	"System.Collections.IList.RemoveAt"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14581_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14582_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_GenericMethod;
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_SyncRoot()
MethodInfo ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14583_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsFixedSize()
MethodInfo ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_MethodInfo = 
{
	"System.Collections.IList.get_IsFixedSize"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14584_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsReadOnly()
MethodInfo ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_MethodInfo = 
{
	"System.Collections.IList.get_IsReadOnly"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14585_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_GenericMethod;
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_Item(System.Int32)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_MethodInfo = 
{
	"System.Collections.IList.get_Item"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m14586_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
MethodInfo ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_MethodInfo = 
{
	"System.Collections.IList.set_Item"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m14587_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_Contains_m14795_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_Contains_m14795_GenericMethod;
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::Contains(T)
MethodInfo ReadOnlyCollection_1_Contains_m14795_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_Contains_m14588_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_Contains_m14795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_Contains_m14795_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfoU5BU5D_t140_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_CopyTo_m14796_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfoU5BU5D_t140_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_CopyTo_m14796_GenericMethod;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::CopyTo(T[],System.Int32)
MethodInfo ReadOnlyCollection_1_CopyTo_m14796_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_CopyTo_m14589_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_CopyTo_m14796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_CopyTo_m14796_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t2827_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_GetEnumerator_m14797_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::GetEnumerator()
MethodInfo ReadOnlyCollection_1_GetEnumerator_m14797_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m14590_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t2827_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_GetEnumerator_m14797_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_IndexOf_m14798_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_IndexOf_m14798_GenericMethod;
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::IndexOf(T)
MethodInfo ReadOnlyCollection_1_IndexOf_m14798_MethodInfo = 
{
	"IndexOf"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_IndexOf_m14591_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_IndexOf_m14798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_IndexOf_m14798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_get_Count_m14799_GenericMethod;
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::get_Count()
MethodInfo ReadOnlyCollection_1_get_Count_m14799_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_get_Count_m14592_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_get_Count_m14799_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_get_Item_m14800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ReadOnlyCollection_1_get_Item_m14800_GenericMethod;
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.MethodInfo>::get_Item(System.Int32)
MethodInfo ReadOnlyCollection_1_get_Item_m14800_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&ReadOnlyCollection_1_get_Item_m14593_gshared/* method */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, ReadOnlyCollection_1_t2829_ReadOnlyCollection_1_get_Item_m14800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ReadOnlyCollection_1_get_Item_m14800_GenericMethod/* genericMethod */

};
static MethodInfo* ReadOnlyCollection_1_t2829_MethodInfos[] =
{
	&ReadOnlyCollection_1__ctor_m14771_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14773_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14781_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Add_m14782_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Clear_m14783_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_MethodInfo,
	&ReadOnlyCollection_1_Contains_m14795_MethodInfo,
	&ReadOnlyCollection_1_CopyTo_m14796_MethodInfo,
	&ReadOnlyCollection_1_GetEnumerator_m14797_MethodInfo,
	&ReadOnlyCollection_1_IndexOf_m14798_MethodInfo,
	&ReadOnlyCollection_1_get_Count_m14799_MethodInfo,
	&ReadOnlyCollection_1_get_Item_m14800_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14781_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_Add_m14782_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_Clear_m14783_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14773_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_Contains_m14795_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_CopyTo_m14796_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_IndexOf_m14798_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_MethodInfo;
extern MethodInfo ReadOnlyCollection_1_GetEnumerator_m14797_MethodInfo;
static MethodInfo* ReadOnlyCollection_1_t2829_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14781_MethodInfo,
	&ReadOnlyCollection_1_get_Count_m14799_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14789_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14790_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14780_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14791_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14792_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_get_Item_m14793_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_set_Item_m14794_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Add_m14782_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Clear_m14783_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Contains_m14784_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14785_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Insert_m14786_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_Remove_m14787_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14788_MethodInfo,
	&ReadOnlyCollection_1_get_Count_m14799_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14779_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14772_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14773_MethodInfo,
	&ReadOnlyCollection_1_Contains_m14795_MethodInfo,
	&ReadOnlyCollection_1_CopyTo_m14796_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14775_MethodInfo,
	&ReadOnlyCollection_1_IndexOf_m14798_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14774_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14776_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14777_MethodInfo,
	&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14778_MethodInfo,
	&ReadOnlyCollection_1_GetEnumerator_m14797_MethodInfo,
	&ReadOnlyCollection_1_get_Item_m14800_MethodInfo,
};
extern TypeInfo IList_t1488_il2cpp_TypeInfo;
static TypeInfo* ReadOnlyCollection_1_t2829_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_t1259_il2cpp_TypeInfo,
	&IList_t1488_il2cpp_TypeInfo,
	&ICollection_1_t2828_il2cpp_TypeInfo,
	&IList_1_t2867_il2cpp_TypeInfo,
	&IEnumerable_1_t2826_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ReadOnlyCollection_1_t2829_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1179_il2cpp_TypeInfo, 4},
	{ &ICollection_t1259_il2cpp_TypeInfo, 5},
	{ &IList_t1488_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t2828_il2cpp_TypeInfo, 20},
	{ &IList_1_t2867_il2cpp_TypeInfo, 27},
	{ &IEnumerable_1_t2826_il2cpp_TypeInfo, 32},
};
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
static Il2CppRGCTXData ReadOnlyCollection_1_t2829_RGCTXData[9] = 
{
	&ReadOnlyCollection_1_get_Item_m14800_MethodInfo/* Method Usage */,
	&Collection_1_IsValidItem_m14832_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&ICollection_1_Contains_m43122_MethodInfo/* Method Usage */,
	&IList_1_IndexOf_m43126_MethodInfo/* Method Usage */,
	&IList_1_get_Item_m43124_MethodInfo/* Method Usage */,
	&ICollection_1_CopyTo_m42843_MethodInfo/* Method Usage */,
	&IEnumerable_1_GetEnumerator_m42844_MethodInfo/* Method Usage */,
	&ICollection_1_get_Count_m42842_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ReadOnlyCollection_1_t2829_0_0_0;
extern Il2CppType ReadOnlyCollection_1_t2829_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct ReadOnlyCollection_1_t2829;
extern Il2CppGenericClass ReadOnlyCollection_1_t2829_GenericClass;
extern CustomAttributesCache ReadOnlyCollection_1_t1873__CustomAttributeCache;
TypeInfo ReadOnlyCollection_1_t2829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadOnlyCollection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, ReadOnlyCollection_1_t2829_MethodInfos/* methods */
	, ReadOnlyCollection_1_t2829_PropertyInfos/* properties */
	, ReadOnlyCollection_1_t2829_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* element_class */
	, ReadOnlyCollection_1_t2829_InterfacesTypeInfos/* implemented_interfaces */
	, ReadOnlyCollection_1_t2829_VTable/* vtable */
	, &ReadOnlyCollection_1_t1873__CustomAttributeCache/* custom_attributes_cache */
	, &ReadOnlyCollection_1_t2829_il2cpp_TypeInfo/* cast_class */
	, &ReadOnlyCollection_1_t2829_0_0_0/* byval_arg */
	, &ReadOnlyCollection_1_t2829_1_0_0/* this_arg */
	, ReadOnlyCollection_1_t2829_InterfacesOffsets/* interface_offsets */
	, &ReadOnlyCollection_1_t2829_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, ReadOnlyCollection_1_t2829_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReadOnlyCollection_1_t2829)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 30/* method_count */
	, 9/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Collection_1_t2868_il2cpp_TypeInfo;

// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern MethodInfo Collection_1_IsSynchronized_m14835_MethodInfo;
extern MethodInfo Collection_1_IsFixedSize_m14836_MethodInfo;
extern MethodInfo Collection_1_ConvertItem_m14833_MethodInfo;
extern MethodInfo Collection_1_SetItem_m14831_MethodInfo;
extern MethodInfo List_1__ctor_m14460_MethodInfo;
extern MethodInfo ICollection_get_SyncRoot_m13663_MethodInfo;
extern MethodInfo Collection_1_InsertItem_m14824_MethodInfo;
extern MethodInfo Collection_1_CheckWritable_m14834_MethodInfo;
extern MethodInfo Collection_1_IndexOf_m14822_MethodInfo;
extern MethodInfo Collection_1_RemoveItem_m14827_MethodInfo;
extern MethodInfo Collection_1_ClearItems_m14818_MethodInfo;
extern MethodInfo ICollection_1_Clear_m43121_MethodInfo;
extern MethodInfo IList_1_Insert_m43127_MethodInfo;
extern MethodInfo IList_1_RemoveAt_m43128_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Type_get_IsValueType_m9853_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo ICollection_get_IsSynchronized_m13662_MethodInfo;
extern MethodInfo IList_get_IsFixedSize_m13664_MethodInfo;


// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::.ctor()
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IEnumerable.GetEnumerator()
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Add(System.Object)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Contains(System.Object)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.IndexOf(System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Remove(System.Object)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_SyncRoot()
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsFixedSize()
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsReadOnly()
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_Item(System.Int32)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Add(T)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Clear()
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::ClearItems()
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Contains(T)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::CopyTo(T[],System.Int32)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::GetEnumerator()
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IndexOf(T)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Insert(System.Int32,T)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::InsertItem(System.Int32,T)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Remove(T)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::RemoveAt(System.Int32)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::RemoveItem(System.Int32)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::get_Count()
// T System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::get_Item(System.Int32)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::set_Item(System.Int32,T)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::SetItem(System.Int32,T)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IsValidItem(System.Object)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::ConvertItem(System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
// Metadata Definition System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>
extern Il2CppType IList_1_t2867_0_0_1;
FieldInfo Collection_1_t2868____list_0_FieldInfo = 
{
	"list"/* name */
	, &IList_1_t2867_0_0_1/* type */
	, &Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, offsetof(Collection_1_t2868, ___list_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo Collection_1_t2868____syncRoot_1_FieldInfo = 
{
	"syncRoot"/* name */
	, &Object_t_0_0_1/* type */
	, &Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, offsetof(Collection_1_t2868, ___syncRoot_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Collection_1_t2868_FieldInfos[] =
{
	&Collection_1_t2868____list_0_FieldInfo,
	&Collection_1_t2868____syncRoot_1_FieldInfo,
	NULL
};
extern MethodInfo Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_MethodInfo;
static PropertyInfo Collection_1_t2868____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_MethodInfo;
static PropertyInfo Collection_1_t2868____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_MethodInfo;
static PropertyInfo Collection_1_t2868____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_IList_get_IsFixedSize_m14812_MethodInfo;
static PropertyInfo Collection_1_t2868____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsFixedSize"/* name */
	, &Collection_1_System_Collections_IList_get_IsFixedSize_m14812_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_IList_get_IsReadOnly_m14813_MethodInfo;
static PropertyInfo Collection_1_t2868____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsReadOnly"/* name */
	, &Collection_1_System_Collections_IList_get_IsReadOnly_m14813_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_IList_get_Item_m14814_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_set_Item_m14815_MethodInfo;
static PropertyInfo Collection_1_t2868____System_Collections_IList_Item_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.Item"/* name */
	, &Collection_1_System_Collections_IList_get_Item_m14814_MethodInfo/* get */
	, &Collection_1_System_Collections_IList_set_Item_m14815_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_get_Count_m14828_MethodInfo;
static PropertyInfo Collection_1_t2868____Count_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &Collection_1_get_Count_m14828_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_get_Item_m14829_MethodInfo;
extern MethodInfo Collection_1_set_Item_m14830_MethodInfo;
static PropertyInfo Collection_1_t2868____Item_PropertyInfo = 
{
	&Collection_1_t2868_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &Collection_1_get_Item_m14829_MethodInfo/* get */
	, &Collection_1_set_Item_m14830_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Collection_1_t2868_PropertyInfos[] =
{
	&Collection_1_t2868____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&Collection_1_t2868____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&Collection_1_t2868____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&Collection_1_t2868____System_Collections_IList_IsFixedSize_PropertyInfo,
	&Collection_1_t2868____System_Collections_IList_IsReadOnly_PropertyInfo,
	&Collection_1_t2868____System_Collections_IList_Item_PropertyInfo,
	&Collection_1_t2868____Count_PropertyInfo,
	&Collection_1_t2868____Item_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1__ctor_m14801_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::.ctor()
MethodInfo Collection_1__ctor_m14801_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Collection_1__ctor_m14594_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1__ctor_m14801_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14595_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_ICollection_CopyTo_m14803_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_ICollection_CopyTo_m14803_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo Collection_1_System_Collections_ICollection_CopyTo_m14803_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m14596_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_ICollection_CopyTo_m14803_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_ICollection_CopyTo_m14803_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IEnumerable_GetEnumerator_m14804_GenericMethod;
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo Collection_1_System_Collections_IEnumerable_GetEnumerator_m14804_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m14597_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IEnumerable_GetEnumerator_m14804_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_Add_m14805_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Add_m14805_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Add(System.Object)
MethodInfo Collection_1_System_Collections_IList_Add_m14805_MethodInfo = 
{
	"System.Collections.IList.Add"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Add_m14598_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_Add_m14805_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Add_m14805_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_Contains_m14806_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Contains_m14806_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Contains(System.Object)
MethodInfo Collection_1_System_Collections_IList_Contains_m14806_MethodInfo = 
{
	"System.Collections.IList.Contains"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Contains_m14599_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_Contains_m14806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Contains_m14806_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_IndexOf_m14807_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_IndexOf_m14807_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.IndexOf(System.Object)
MethodInfo Collection_1_System_Collections_IList_IndexOf_m14807_MethodInfo = 
{
	"System.Collections.IList.IndexOf"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m14600_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_IndexOf_m14807_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_IndexOf_m14807_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_Insert_m14808_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Insert_m14808_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
MethodInfo Collection_1_System_Collections_IList_Insert_m14808_MethodInfo = 
{
	"System.Collections.IList.Insert"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Insert_m14601_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_Insert_m14808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Insert_m14808_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_Remove_m14809_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Remove_m14809_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.Remove(System.Object)
MethodInfo Collection_1_System_Collections_IList_Remove_m14809_MethodInfo = 
{
	"System.Collections.IList.Remove"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Remove_m14602_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_Remove_m14809_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Remove_m14809_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m14603_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_GenericMethod;
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.ICollection.get_SyncRoot()
MethodInfo Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m14604_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_get_IsFixedSize_m14812_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsFixedSize()
MethodInfo Collection_1_System_Collections_IList_get_IsFixedSize_m14812_MethodInfo = 
{
	"System.Collections.IList.get_IsFixedSize"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m14605_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_get_IsFixedSize_m14812_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_get_IsReadOnly_m14813_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_IsReadOnly()
MethodInfo Collection_1_System_Collections_IList_get_IsReadOnly_m14813_MethodInfo = 
{
	"System.Collections.IList.get_IsReadOnly"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m14606_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_get_IsReadOnly_m14813_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_get_Item_m14814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_get_Item_m14814_GenericMethod;
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.get_Item(System.Int32)
MethodInfo Collection_1_System_Collections_IList_get_Item_m14814_MethodInfo = 
{
	"System.Collections.IList.get_Item"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_get_Item_m14607_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_get_Item_m14814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_get_Item_m14814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_System_Collections_IList_set_Item_m14815_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_set_Item_m14815_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
MethodInfo Collection_1_System_Collections_IList_set_Item_m14815_MethodInfo = 
{
	"System.Collections.IList.set_Item"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_set_Item_m14608_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_System_Collections_IList_set_Item_m14815_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_set_Item_m14815_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_Add_m14816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Add_m14816_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Add(T)
MethodInfo Collection_1_Add_m14816_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&Collection_1_Add_m14609_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_Add_m14816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Add_m14816_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Clear_m14817_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Clear()
MethodInfo Collection_1_Clear_m14817_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&Collection_1_Clear_m14610_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Clear_m14817_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_ClearItems_m14818_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::ClearItems()
MethodInfo Collection_1_ClearItems_m14818_MethodInfo = 
{
	"ClearItems"/* name */
	, (methodPointerType)&Collection_1_ClearItems_m14611_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_ClearItems_m14818_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_Contains_m14819_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Contains_m14819_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Contains(T)
MethodInfo Collection_1_Contains_m14819_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Collection_1_Contains_m14612_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_Contains_m14819_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Contains_m14819_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfoU5BU5D_t140_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_CopyTo_m14820_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfoU5BU5D_t140_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_CopyTo_m14820_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::CopyTo(T[],System.Int32)
MethodInfo Collection_1_CopyTo_m14820_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&Collection_1_CopyTo_m14613_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t2868_Collection_1_CopyTo_m14820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_CopyTo_m14820_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t2827_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_GetEnumerator_m14821_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::GetEnumerator()
MethodInfo Collection_1_GetEnumerator_m14821_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&Collection_1_GetEnumerator_m14614_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t2827_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_GetEnumerator_m14821_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_IndexOf_m14822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IndexOf_m14822_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IndexOf(T)
MethodInfo Collection_1_IndexOf_m14822_MethodInfo = 
{
	"IndexOf"/* name */
	, (methodPointerType)&Collection_1_IndexOf_m14615_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_IndexOf_m14822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IndexOf_m14822_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_Insert_m14823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Insert_m14823_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Insert(System.Int32,T)
MethodInfo Collection_1_Insert_m14823_MethodInfo = 
{
	"Insert"/* name */
	, (methodPointerType)&Collection_1_Insert_m14616_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_Insert_m14823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Insert_m14823_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_InsertItem_m14824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_InsertItem_m14824_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::InsertItem(System.Int32,T)
MethodInfo Collection_1_InsertItem_m14824_MethodInfo = 
{
	"InsertItem"/* name */
	, (methodPointerType)&Collection_1_InsertItem_m14617_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_InsertItem_m14824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_InsertItem_m14824_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_Remove_m14825_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Remove_m14825_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::Remove(T)
MethodInfo Collection_1_Remove_m14825_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&Collection_1_Remove_m14618_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_Remove_m14825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Remove_m14825_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_RemoveAt_m14826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_RemoveAt_m14826_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::RemoveAt(System.Int32)
MethodInfo Collection_1_RemoveAt_m14826_MethodInfo = 
{
	"RemoveAt"/* name */
	, (methodPointerType)&Collection_1_RemoveAt_m14619_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Collection_1_t2868_Collection_1_RemoveAt_m14826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_RemoveAt_m14826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_RemoveItem_m14827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_RemoveItem_m14827_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::RemoveItem(System.Int32)
MethodInfo Collection_1_RemoveItem_m14827_MethodInfo = 
{
	"RemoveItem"/* name */
	, (methodPointerType)&Collection_1_RemoveItem_m14620_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Collection_1_t2868_Collection_1_RemoveItem_m14827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_RemoveItem_m14827_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_get_Count_m14828_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::get_Count()
MethodInfo Collection_1_get_Count_m14828_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&Collection_1_get_Count_m14621_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_get_Count_m14828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_get_Item_m14829_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_get_Item_m14829_GenericMethod;
// T System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::get_Item(System.Int32)
MethodInfo Collection_1_get_Item_m14829_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&Collection_1_get_Item_m14622_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t2868_Collection_1_get_Item_m14829_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_get_Item_m14829_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_set_Item_m14830_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_set_Item_m14830_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::set_Item(System.Int32,T)
MethodInfo Collection_1_set_Item_m14830_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&Collection_1_set_Item_m14623_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_set_Item_m14830_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_set_Item_m14830_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_SetItem_m14831_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_SetItem_m14831_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::SetItem(System.Int32,T)
MethodInfo Collection_1_SetItem_m14831_MethodInfo = 
{
	"SetItem"/* name */
	, (methodPointerType)&Collection_1_SetItem_m14624_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_SetItem_m14831_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_SetItem_m14831_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_IsValidItem_m14832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IsValidItem_m14832_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IsValidItem(System.Object)
MethodInfo Collection_1_IsValidItem_m14832_MethodInfo = 
{
	"IsValidItem"/* name */
	, (methodPointerType)&Collection_1_IsValidItem_m14625_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_IsValidItem_m14832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IsValidItem_m14832_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_ConvertItem_m14833_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_ConvertItem_m14833_GenericMethod;
// T System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::ConvertItem(System.Object)
MethodInfo Collection_1_ConvertItem_m14833_MethodInfo = 
{
	"ConvertItem"/* name */
	, (methodPointerType)&Collection_1_ConvertItem_m14626_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_ConvertItem_m14833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_ConvertItem_m14833_GenericMethod/* genericMethod */

};
extern Il2CppType IList_1_t2867_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_CheckWritable_m14834_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t2867_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_CheckWritable_m14834_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
MethodInfo Collection_1_CheckWritable_m14834_MethodInfo = 
{
	"CheckWritable"/* name */
	, (methodPointerType)&Collection_1_CheckWritable_m14627_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_CheckWritable_m14834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_CheckWritable_m14834_GenericMethod/* genericMethod */

};
extern Il2CppType IList_1_t2867_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_IsSynchronized_m14835_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t2867_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IsSynchronized_m14835_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
MethodInfo Collection_1_IsSynchronized_m14835_MethodInfo = 
{
	"IsSynchronized"/* name */
	, (methodPointerType)&Collection_1_IsSynchronized_m14628_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_IsSynchronized_m14835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IsSynchronized_m14835_GenericMethod/* genericMethod */

};
extern Il2CppType IList_1_t2867_0_0_0;
static ParameterInfo Collection_1_t2868_Collection_1_IsFixedSize_m14836_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t2867_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IsFixedSize_m14836_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.MethodInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
MethodInfo Collection_1_IsFixedSize_m14836_MethodInfo = 
{
	"IsFixedSize"/* name */
	, (methodPointerType)&Collection_1_IsFixedSize_m14629_gshared/* method */
	, &Collection_1_t2868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t2868_Collection_1_IsFixedSize_m14836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IsFixedSize_m14836_GenericMethod/* genericMethod */

};
static MethodInfo* Collection_1_t2868_MethodInfos[] =
{
	&Collection_1__ctor_m14801_MethodInfo,
	&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_MethodInfo,
	&Collection_1_System_Collections_ICollection_CopyTo_m14803_MethodInfo,
	&Collection_1_System_Collections_IEnumerable_GetEnumerator_m14804_MethodInfo,
	&Collection_1_System_Collections_IList_Add_m14805_MethodInfo,
	&Collection_1_System_Collections_IList_Contains_m14806_MethodInfo,
	&Collection_1_System_Collections_IList_IndexOf_m14807_MethodInfo,
	&Collection_1_System_Collections_IList_Insert_m14808_MethodInfo,
	&Collection_1_System_Collections_IList_Remove_m14809_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsFixedSize_m14812_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsReadOnly_m14813_MethodInfo,
	&Collection_1_System_Collections_IList_get_Item_m14814_MethodInfo,
	&Collection_1_System_Collections_IList_set_Item_m14815_MethodInfo,
	&Collection_1_Add_m14816_MethodInfo,
	&Collection_1_Clear_m14817_MethodInfo,
	&Collection_1_ClearItems_m14818_MethodInfo,
	&Collection_1_Contains_m14819_MethodInfo,
	&Collection_1_CopyTo_m14820_MethodInfo,
	&Collection_1_GetEnumerator_m14821_MethodInfo,
	&Collection_1_IndexOf_m14822_MethodInfo,
	&Collection_1_Insert_m14823_MethodInfo,
	&Collection_1_InsertItem_m14824_MethodInfo,
	&Collection_1_Remove_m14825_MethodInfo,
	&Collection_1_RemoveAt_m14826_MethodInfo,
	&Collection_1_RemoveItem_m14827_MethodInfo,
	&Collection_1_get_Count_m14828_MethodInfo,
	&Collection_1_get_Item_m14829_MethodInfo,
	&Collection_1_set_Item_m14830_MethodInfo,
	&Collection_1_SetItem_m14831_MethodInfo,
	&Collection_1_IsValidItem_m14832_MethodInfo,
	&Collection_1_ConvertItem_m14833_MethodInfo,
	&Collection_1_CheckWritable_m14834_MethodInfo,
	&Collection_1_IsSynchronized_m14835_MethodInfo,
	&Collection_1_IsFixedSize_m14836_MethodInfo,
	NULL
};
extern MethodInfo Collection_1_System_Collections_IEnumerable_GetEnumerator_m14804_MethodInfo;
extern MethodInfo Collection_1_System_Collections_ICollection_CopyTo_m14803_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Add_m14805_MethodInfo;
extern MethodInfo Collection_1_Clear_m14817_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Contains_m14806_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_IndexOf_m14807_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Insert_m14808_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Remove_m14809_MethodInfo;
extern MethodInfo Collection_1_RemoveAt_m14826_MethodInfo;
extern MethodInfo Collection_1_Add_m14816_MethodInfo;
extern MethodInfo Collection_1_Contains_m14819_MethodInfo;
extern MethodInfo Collection_1_CopyTo_m14820_MethodInfo;
extern MethodInfo Collection_1_Remove_m14825_MethodInfo;
extern MethodInfo Collection_1_Insert_m14823_MethodInfo;
extern MethodInfo Collection_1_GetEnumerator_m14821_MethodInfo;
static MethodInfo* Collection_1_t2868_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Collection_1_System_Collections_IEnumerable_GetEnumerator_m14804_MethodInfo,
	&Collection_1_get_Count_m14828_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_IsSynchronized_m14810_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_SyncRoot_m14811_MethodInfo,
	&Collection_1_System_Collections_ICollection_CopyTo_m14803_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsFixedSize_m14812_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsReadOnly_m14813_MethodInfo,
	&Collection_1_System_Collections_IList_get_Item_m14814_MethodInfo,
	&Collection_1_System_Collections_IList_set_Item_m14815_MethodInfo,
	&Collection_1_System_Collections_IList_Add_m14805_MethodInfo,
	&Collection_1_Clear_m14817_MethodInfo,
	&Collection_1_System_Collections_IList_Contains_m14806_MethodInfo,
	&Collection_1_System_Collections_IList_IndexOf_m14807_MethodInfo,
	&Collection_1_System_Collections_IList_Insert_m14808_MethodInfo,
	&Collection_1_System_Collections_IList_Remove_m14809_MethodInfo,
	&Collection_1_RemoveAt_m14826_MethodInfo,
	&Collection_1_get_Count_m14828_MethodInfo,
	&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14802_MethodInfo,
	&Collection_1_Add_m14816_MethodInfo,
	&Collection_1_Clear_m14817_MethodInfo,
	&Collection_1_Contains_m14819_MethodInfo,
	&Collection_1_CopyTo_m14820_MethodInfo,
	&Collection_1_Remove_m14825_MethodInfo,
	&Collection_1_IndexOf_m14822_MethodInfo,
	&Collection_1_Insert_m14823_MethodInfo,
	&Collection_1_RemoveAt_m14826_MethodInfo,
	&Collection_1_get_Item_m14829_MethodInfo,
	&Collection_1_set_Item_m14830_MethodInfo,
	&Collection_1_GetEnumerator_m14821_MethodInfo,
	&Collection_1_ClearItems_m14818_MethodInfo,
	&Collection_1_InsertItem_m14824_MethodInfo,
	&Collection_1_RemoveItem_m14827_MethodInfo,
	&Collection_1_SetItem_m14831_MethodInfo,
};
static TypeInfo* Collection_1_t2868_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_t1259_il2cpp_TypeInfo,
	&IList_t1488_il2cpp_TypeInfo,
	&ICollection_1_t2828_il2cpp_TypeInfo,
	&IList_1_t2867_il2cpp_TypeInfo,
	&IEnumerable_1_t2826_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Collection_1_t2868_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1179_il2cpp_TypeInfo, 4},
	{ &ICollection_t1259_il2cpp_TypeInfo, 5},
	{ &IList_t1488_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t2828_il2cpp_TypeInfo, 20},
	{ &IList_1_t2867_il2cpp_TypeInfo, 27},
	{ &IEnumerable_1_t2826_il2cpp_TypeInfo, 32},
};
extern TypeInfo List_1_t150_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
static Il2CppRGCTXData Collection_1_t2868_RGCTXData[25] = 
{
	&List_1_t150_il2cpp_TypeInfo/* Class Usage */,
	&List_1__ctor_m14460_MethodInfo/* Method Usage */,
	&ICollection_1_get_IsReadOnly_m43119_MethodInfo/* Method Usage */,
	&IEnumerable_1_GetEnumerator_m42844_MethodInfo/* Method Usage */,
	&ICollection_1_get_Count_m42842_MethodInfo/* Method Usage */,
	&Collection_1_ConvertItem_m14833_MethodInfo/* Method Usage */,
	&Collection_1_InsertItem_m14824_MethodInfo/* Method Usage */,
	&Collection_1_IsValidItem_m14832_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&ICollection_1_Contains_m43122_MethodInfo/* Method Usage */,
	&IList_1_IndexOf_m43126_MethodInfo/* Method Usage */,
	&Collection_1_CheckWritable_m14834_MethodInfo/* Method Usage */,
	&Collection_1_IndexOf_m14822_MethodInfo/* Method Usage */,
	&Collection_1_RemoveItem_m14827_MethodInfo/* Method Usage */,
	&Collection_1_IsSynchronized_m14835_MethodInfo/* Method Usage */,
	&Collection_1_IsFixedSize_m14836_MethodInfo/* Method Usage */,
	&IList_1_get_Item_m43124_MethodInfo/* Method Usage */,
	&Collection_1_SetItem_m14831_MethodInfo/* Method Usage */,
	&Collection_1_ClearItems_m14818_MethodInfo/* Method Usage */,
	&ICollection_1_Clear_m43121_MethodInfo/* Method Usage */,
	&ICollection_1_CopyTo_m42843_MethodInfo/* Method Usage */,
	&IList_1_Insert_m43127_MethodInfo/* Method Usage */,
	&IList_1_RemoveAt_m43128_MethodInfo/* Method Usage */,
	&IList_1_set_Item_m43125_MethodInfo/* Method Usage */,
	&MethodInfo_t141_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Collection_1_t2868_0_0_0;
extern Il2CppType Collection_1_t2868_1_0_0;
struct Collection_1_t2868;
extern Il2CppGenericClass Collection_1_t2868_GenericClass;
extern CustomAttributesCache Collection_1_t1872__CustomAttributeCache;
TypeInfo Collection_1_t2868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, Collection_1_t2868_MethodInfos/* methods */
	, Collection_1_t2868_PropertyInfos/* properties */
	, Collection_1_t2868_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Collection_1_t2868_il2cpp_TypeInfo/* element_class */
	, Collection_1_t2868_InterfacesTypeInfos/* implemented_interfaces */
	, Collection_1_t2868_VTable/* vtable */
	, &Collection_1_t1872__CustomAttributeCache/* custom_attributes_cache */
	, &Collection_1_t2868_il2cpp_TypeInfo/* cast_class */
	, &Collection_1_t2868_0_0_0/* byval_arg */
	, &Collection_1_t2868_1_0_0/* this_arg */
	, Collection_1_t2868_InterfacesOffsets/* interface_offsets */
	, &Collection_1_t2868_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Collection_1_t2868_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Collection_1_t2868)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 36/* method_count */
	, 8/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 37/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo EqualityComparer_1_t2869_il2cpp_TypeInfo;
// System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_0MethodDeclarations.h"

// System.Collections.Generic.GenericEqualityComparer`1
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer_.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_1.h"
extern TypeInfo IEquatable_1_t9709_il2cpp_TypeInfo;
extern TypeInfo GenericEqualityComparer_1_t1867_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t922_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t2870_il2cpp_TypeInfo;
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_1MethodDeclarations.h"
extern Il2CppType IEquatable_1_t9709_0_0_0;
extern Il2CppType GenericEqualityComparer_1_t1867_0_0_0;
extern MethodInfo Type_IsAssignableFrom_m6740_MethodInfo;
extern MethodInfo Type_MakeGenericType_m6738_MethodInfo;
extern MethodInfo Activator_CreateInstance_m12682_MethodInfo;
extern MethodInfo DefaultComparer__ctor_m14842_MethodInfo;
extern MethodInfo EqualityComparer_1_GetHashCode_m43171_MethodInfo;
extern MethodInfo EqualityComparer_1_Equals_m32901_MethodInfo;


// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::.ctor()
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::.cctor()
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::get_Default()
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>
extern Il2CppType EqualityComparer_1_t2869_0_0_49;
FieldInfo EqualityComparer_1_t2869_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &EqualityComparer_1_t2869_0_0_49/* type */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* parent */
	, offsetof(EqualityComparer_1_t2869_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* EqualityComparer_1_t2869_FieldInfos[] =
{
	&EqualityComparer_1_t2869_____default_0_FieldInfo,
	NULL
};
extern MethodInfo EqualityComparer_1_get_Default_m14841_MethodInfo;
static PropertyInfo EqualityComparer_1_t2869____Default_PropertyInfo = 
{
	&EqualityComparer_1_t2869_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &EqualityComparer_1_get_Default_m14841_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* EqualityComparer_1_t2869_PropertyInfos[] =
{
	&EqualityComparer_1_t2869____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__ctor_m14837_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::.ctor()
MethodInfo EqualityComparer_1__ctor_m14837_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EqualityComparer_1__ctor_m14630_gshared/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__ctor_m14837_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__cctor_m14838_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::.cctor()
MethodInfo EqualityComparer_1__cctor_m14838_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EqualityComparer_1__cctor_m14631_gshared/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__cctor_m14838_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t2869_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_MethodInfo = 
{
	"System.Collections.IEqualityComparer.GetHashCode"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14632_gshared/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, EqualityComparer_1_t2869_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t2869_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_MethodInfo = 
{
	"System.Collections.IEqualityComparer.Equals"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14633_gshared/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, EqualityComparer_1_t2869_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo EqualityComparer_1_t2869_EqualityComparer_1_GetHashCode_m43171_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_GetHashCode_m43171_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::GetHashCode(T)
MethodInfo EqualityComparer_1_GetHashCode_m43171_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, EqualityComparer_1_t2869_EqualityComparer_1_GetHashCode_m43171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_GetHashCode_m43171_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo EqualityComparer_1_t2869_EqualityComparer_1_Equals_m32901_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_Equals_m32901_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::Equals(T,T)
MethodInfo EqualityComparer_1_Equals_m32901_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, EqualityComparer_1_t2869_EqualityComparer_1_Equals_m32901_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_Equals_m32901_GenericMethod/* genericMethod */

};
extern Il2CppType EqualityComparer_1_t2869_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m14841_GenericMethod;
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Reflection.MethodInfo>::get_Default()
MethodInfo EqualityComparer_1_get_Default_m14841_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&EqualityComparer_1_get_Default_m14634_gshared/* method */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* declaring_type */
	, &EqualityComparer_1_t2869_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_get_Default_m14841_GenericMethod/* genericMethod */

};
static MethodInfo* EqualityComparer_1_t2869_MethodInfos[] =
{
	&EqualityComparer_1__ctor_m14837_MethodInfo,
	&EqualityComparer_1__cctor_m14838_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_MethodInfo,
	&EqualityComparer_1_GetHashCode_m43171_MethodInfo,
	&EqualityComparer_1_Equals_m32901_MethodInfo,
	&EqualityComparer_1_get_Default_m14841_MethodInfo,
	NULL
};
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_MethodInfo;
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_MethodInfo;
static MethodInfo* EqualityComparer_1_t2869_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&EqualityComparer_1_Equals_m32901_MethodInfo,
	&EqualityComparer_1_GetHashCode_m43171_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_MethodInfo,
	NULL,
	NULL,
};
extern TypeInfo IEqualityComparer_1_t9710_il2cpp_TypeInfo;
extern TypeInfo IEqualityComparer_t1363_il2cpp_TypeInfo;
static TypeInfo* EqualityComparer_1_t2869_InterfacesTypeInfos[] = 
{
	&IEqualityComparer_1_t9710_il2cpp_TypeInfo,
	&IEqualityComparer_t1363_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair EqualityComparer_1_t2869_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t9710_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern TypeInfo EqualityComparer_1_t2869_il2cpp_TypeInfo;
extern TypeInfo EqualityComparer_1_t2869_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t2870_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
static Il2CppRGCTXData EqualityComparer_1_t2869_RGCTXData[9] = 
{
	&IEquatable_1_t9709_0_0_0/* Type Usage */,
	&MethodInfo_t141_0_0_0/* Type Usage */,
	&EqualityComparer_1_t2869_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_t2869_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t2870_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m14842_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_GetHashCode_m43171_MethodInfo/* Method Usage */,
	&EqualityComparer_1_Equals_m32901_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType EqualityComparer_1_t2869_0_0_0;
extern Il2CppType EqualityComparer_1_t2869_1_0_0;
struct EqualityComparer_1_t2869;
extern Il2CppGenericClass EqualityComparer_1_t2869_GenericClass;
TypeInfo EqualityComparer_1_t2869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, EqualityComparer_1_t2869_MethodInfos/* methods */
	, EqualityComparer_1_t2869_PropertyInfos/* properties */
	, EqualityComparer_1_t2869_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* element_class */
	, EqualityComparer_1_t2869_InterfacesTypeInfos/* implemented_interfaces */
	, EqualityComparer_1_t2869_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* cast_class */
	, &EqualityComparer_1_t2869_0_0_0/* byval_arg */
	, &EqualityComparer_1_t2869_1_0_0/* this_arg */
	, EqualityComparer_1_t2869_InterfacesOffsets/* interface_offsets */
	, &EqualityComparer_1_t2869_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, EqualityComparer_1_t2869_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EqualityComparer_1_t2869)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EqualityComparer_1_t2869_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Reflection.MethodInfo>::Equals(T,T)
// System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Reflection.MethodInfo>::GetHashCode(T)
// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Reflection.MethodInfo>
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IEqualityComparer_1_t9710_IEqualityComparer_1_Equals_m43172_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_Equals_m43172_GenericMethod;
// System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Reflection.MethodInfo>::Equals(T,T)
MethodInfo IEqualityComparer_1_Equals_m43172_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t9710_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, IEqualityComparer_1_t9710_IEqualityComparer_1_Equals_m43172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_Equals_m43172_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IEqualityComparer_1_t9710_IEqualityComparer_1_GetHashCode_m43173_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_GetHashCode_m43173_GenericMethod;
// System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Reflection.MethodInfo>::GetHashCode(T)
MethodInfo IEqualityComparer_1_GetHashCode_m43173_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t9710_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IEqualityComparer_1_t9710_IEqualityComparer_1_GetHashCode_m43173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_GetHashCode_m43173_GenericMethod/* genericMethod */

};
static MethodInfo* IEqualityComparer_1_t9710_MethodInfos[] =
{
	&IEqualityComparer_1_Equals_m43172_MethodInfo,
	&IEqualityComparer_1_GetHashCode_m43173_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEqualityComparer_1_t9710_0_0_0;
extern Il2CppType IEqualityComparer_1_t9710_1_0_0;
struct IEqualityComparer_1_t9710;
extern Il2CppGenericClass IEqualityComparer_1_t9710_GenericClass;
TypeInfo IEqualityComparer_1_t9710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEqualityComparer_1_t9710_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEqualityComparer_1_t9710_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEqualityComparer_1_t9710_il2cpp_TypeInfo/* cast_class */
	, &IEqualityComparer_1_t9710_0_0_0/* byval_arg */
	, &IEqualityComparer_1_t9710_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEqualityComparer_1_t9710_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.IEquatable`1<System.Reflection.MethodInfo>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Reflection.MethodInfo>
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IEquatable_1_t9709_IEquatable_1_Equals_m43174_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m43174_GenericMethod;
// System.Boolean System.IEquatable`1<System.Reflection.MethodInfo>::Equals(T)
MethodInfo IEquatable_1_Equals_m43174_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t9709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, IEquatable_1_t9709_IEquatable_1_Equals_m43174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m43174_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t9709_MethodInfos[] =
{
	&IEquatable_1_Equals_m43174_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t9709_1_0_0;
struct IEquatable_1_t9709;
extern Il2CppGenericClass IEquatable_1_t9709_GenericClass;
TypeInfo IEquatable_1_t9709_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t9709_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t9709_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t9709_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t9709_0_0_0/* byval_arg */
	, &IEquatable_1_t9709_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t9709_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo EqualityComparer_1__ctor_m14837_MethodInfo;


// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>::.ctor()
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>::Equals(T,T)
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m14842_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>::.ctor()
MethodInfo DefaultComparer__ctor_m14842_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m14665_gshared/* method */
	, &DefaultComparer_t2870_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m14842_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo DefaultComparer_t2870_DefaultComparer_GetHashCode_m14843_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_GetHashCode_m14843_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>::GetHashCode(T)
MethodInfo DefaultComparer_GetHashCode_m14843_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultComparer_GetHashCode_m14666_gshared/* method */
	, &DefaultComparer_t2870_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, DefaultComparer_t2870_DefaultComparer_GetHashCode_m14843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_GetHashCode_m14843_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo DefaultComparer_t2870_DefaultComparer_Equals_m14844_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Equals_m14844_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.MethodInfo>::Equals(T,T)
MethodInfo DefaultComparer_Equals_m14844_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultComparer_Equals_m14667_gshared/* method */
	, &DefaultComparer_t2870_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, DefaultComparer_t2870_DefaultComparer_Equals_m14844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Equals_m14844_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t2870_MethodInfos[] =
{
	&DefaultComparer__ctor_m14842_MethodInfo,
	&DefaultComparer_GetHashCode_m14843_MethodInfo,
	&DefaultComparer_Equals_m14844_MethodInfo,
	NULL
};
extern MethodInfo DefaultComparer_Equals_m14844_MethodInfo;
extern MethodInfo DefaultComparer_GetHashCode_m14843_MethodInfo;
static MethodInfo* DefaultComparer_t2870_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Equals_m14844_MethodInfo,
	&DefaultComparer_GetHashCode_m14843_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14840_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14839_MethodInfo,
	&DefaultComparer_GetHashCode_m14843_MethodInfo,
	&DefaultComparer_Equals_m14844_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t2870_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t9710_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern TypeInfo EqualityComparer_1_t2869_il2cpp_TypeInfo;
extern TypeInfo EqualityComparer_1_t2869_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t2870_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
static Il2CppRGCTXData DefaultComparer_t2870_RGCTXData[11] = 
{
	&IEquatable_1_t9709_0_0_0/* Type Usage */,
	&MethodInfo_t141_0_0_0/* Type Usage */,
	&EqualityComparer_1_t2869_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_t2869_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t2870_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m14842_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_GetHashCode_m43171_MethodInfo/* Method Usage */,
	&EqualityComparer_1_Equals_m32901_MethodInfo/* Method Usage */,
	&EqualityComparer_1__ctor_m14837_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t2870_0_0_0;
extern Il2CppType DefaultComparer_t2870_1_0_0;
struct DefaultComparer_t2870;
extern Il2CppGenericClass DefaultComparer_t2870_GenericClass;
extern TypeInfo EqualityComparer_1_t1866_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t2870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t2870_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &EqualityComparer_1_t2869_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &EqualityComparer_1_t1866_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t2870_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t2870_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t2870_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t2870_0_0_0/* byval_arg */
	, &DefaultComparer_t2870_1_0_0/* this_arg */
	, DefaultComparer_t2870_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t2870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, DefaultComparer_t2870_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t2870)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Predicate`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Predicate_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Predicate_1_t2830_il2cpp_TypeInfo;
// System.Predicate`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Predicate_1_gen_5MethodDeclarations.h"



// System.Void System.Predicate`1<System.Reflection.MethodInfo>::.ctor(System.Object,System.IntPtr)
// System.Boolean System.Predicate`1<System.Reflection.MethodInfo>::Invoke(T)
// System.IAsyncResult System.Predicate`1<System.Reflection.MethodInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.Boolean System.Predicate`1<System.Reflection.MethodInfo>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Predicate`1<System.Reflection.MethodInfo>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Predicate_1_t2830_Predicate_1__ctor_m14845_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1__ctor_m14845_GenericMethod;
// System.Void System.Predicate`1<System.Reflection.MethodInfo>::.ctor(System.Object,System.IntPtr)
MethodInfo Predicate_1__ctor_m14845_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Predicate_1__ctor_m14668_gshared/* method */
	, &Predicate_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Predicate_1_t2830_Predicate_1__ctor_m14845_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1__ctor_m14845_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Predicate_1_t2830_Predicate_1_Invoke_m14846_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1_Invoke_m14846_GenericMethod;
// System.Boolean System.Predicate`1<System.Reflection.MethodInfo>::Invoke(T)
MethodInfo Predicate_1_Invoke_m14846_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Predicate_1_Invoke_m14669_gshared/* method */
	, &Predicate_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Predicate_1_t2830_Predicate_1_Invoke_m14846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1_Invoke_m14846_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Predicate_1_t2830_Predicate_1_BeginInvoke_m14847_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1_BeginInvoke_m14847_GenericMethod;
// System.IAsyncResult System.Predicate`1<System.Reflection.MethodInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Predicate_1_BeginInvoke_m14847_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Predicate_1_BeginInvoke_m14670_gshared/* method */
	, &Predicate_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Predicate_1_t2830_Predicate_1_BeginInvoke_m14847_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1_BeginInvoke_m14847_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Predicate_1_t2830_Predicate_1_EndInvoke_m14848_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1_EndInvoke_m14848_GenericMethod;
// System.Boolean System.Predicate`1<System.Reflection.MethodInfo>::EndInvoke(System.IAsyncResult)
MethodInfo Predicate_1_EndInvoke_m14848_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Predicate_1_EndInvoke_m14671_gshared/* method */
	, &Predicate_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Predicate_1_t2830_Predicate_1_EndInvoke_m14848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1_EndInvoke_m14848_GenericMethod/* genericMethod */

};
static MethodInfo* Predicate_1_t2830_MethodInfos[] =
{
	&Predicate_1__ctor_m14845_MethodInfo,
	&Predicate_1_Invoke_m14846_MethodInfo,
	&Predicate_1_BeginInvoke_m14847_MethodInfo,
	&Predicate_1_EndInvoke_m14848_MethodInfo,
	NULL
};
extern MethodInfo Predicate_1_Invoke_m14846_MethodInfo;
extern MethodInfo Predicate_1_BeginInvoke_m14847_MethodInfo;
extern MethodInfo Predicate_1_EndInvoke_m14848_MethodInfo;
static MethodInfo* Predicate_1_t2830_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Predicate_1_Invoke_m14846_MethodInfo,
	&Predicate_1_BeginInvoke_m14847_MethodInfo,
	&Predicate_1_EndInvoke_m14848_MethodInfo,
};
static Il2CppInterfaceOffsetPair Predicate_1_t2830_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Predicate_1_t2830_0_0_0;
extern Il2CppType Predicate_1_t2830_1_0_0;
struct Predicate_1_t2830;
extern Il2CppGenericClass Predicate_1_t2830_GenericClass;
TypeInfo Predicate_1_t2830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, Predicate_1_t2830_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Predicate_1_t2830_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Predicate_1_t2830_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Predicate_1_t2830_il2cpp_TypeInfo/* cast_class */
	, &Predicate_1_t2830_0_0_0/* byval_arg */
	, &Predicate_1_t2830_1_0_0/* this_arg */
	, Predicate_1_t2830_InterfacesOffsets/* interface_offsets */
	, &Predicate_1_t2830_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Predicate_1_t2830)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Comparer_1_t2871_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_0MethodDeclarations.h"

// System.Collections.Generic.GenericComparer`1
#include "mscorlib_System_Collections_Generic_GenericComparer_1.h"
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_1.h"
extern TypeInfo IComparable_1_t6035_il2cpp_TypeInfo;
extern TypeInfo GenericComparer_1_t1855_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t2872_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_1MethodDeclarations.h"
extern Il2CppType IComparable_1_t6035_0_0_0;
extern Il2CppType GenericComparer_1_t1855_0_0_0;
extern MethodInfo DefaultComparer__ctor_m14853_MethodInfo;
extern MethodInfo Comparer_1_Compare_m43175_MethodInfo;
extern MethodInfo ArgumentException__ctor_m12706_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::.ctor()
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::.cctor()
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::get_Default()
// Metadata Definition System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>
extern Il2CppType Comparer_1_t2871_0_0_49;
FieldInfo Comparer_1_t2871_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &Comparer_1_t2871_0_0_49/* type */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* parent */
	, offsetof(Comparer_1_t2871_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Comparer_1_t2871_FieldInfos[] =
{
	&Comparer_1_t2871_____default_0_FieldInfo,
	NULL
};
extern MethodInfo Comparer_1_get_Default_m14852_MethodInfo;
static PropertyInfo Comparer_1_t2871____Default_PropertyInfo = 
{
	&Comparer_1_t2871_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &Comparer_1_get_Default_m14852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Comparer_1_t2871_PropertyInfos[] =
{
	&Comparer_1_t2871____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__ctor_m14849_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::.ctor()
MethodInfo Comparer_1__ctor_m14849_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparer_1__ctor_m14672_gshared/* method */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__ctor_m14849_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__cctor_m14850_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::.cctor()
MethodInfo Comparer_1__cctor_m14850_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Comparer_1__cctor_m14673_gshared/* method */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__cctor_m14850_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparer_1_t2871_Comparer_1_System_Collections_IComparer_Compare_m14851_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_System_Collections_IComparer_Compare_m14851_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
MethodInfo Comparer_1_System_Collections_IComparer_Compare_m14851_MethodInfo = 
{
	"System.Collections.IComparer.Compare"/* name */
	, (methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m14674_gshared/* method */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparer_1_t2871_Comparer_1_System_Collections_IComparer_Compare_m14851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_System_Collections_IComparer_Compare_m14851_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Comparer_1_t2871_Comparer_1_Compare_m43175_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_Compare_m43175_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::Compare(T,T)
MethodInfo Comparer_1_Compare_m43175_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparer_1_t2871_Comparer_1_Compare_m43175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_Compare_m43175_GenericMethod/* genericMethod */

};
extern Il2CppType Comparer_1_t2871_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_get_Default_m14852_GenericMethod;
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::get_Default()
MethodInfo Comparer_1_get_Default_m14852_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&Comparer_1_get_Default_m14675_gshared/* method */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* declaring_type */
	, &Comparer_1_t2871_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_get_Default_m14852_GenericMethod/* genericMethod */

};
static MethodInfo* Comparer_1_t2871_MethodInfos[] =
{
	&Comparer_1__ctor_m14849_MethodInfo,
	&Comparer_1__cctor_m14850_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m14851_MethodInfo,
	&Comparer_1_Compare_m43175_MethodInfo,
	&Comparer_1_get_Default_m14852_MethodInfo,
	NULL
};
extern MethodInfo Comparer_1_System_Collections_IComparer_Compare_m14851_MethodInfo;
static MethodInfo* Comparer_1_t2871_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Comparer_1_Compare_m43175_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m14851_MethodInfo,
	NULL,
};
extern TypeInfo IComparer_1_t6034_il2cpp_TypeInfo;
extern TypeInfo IComparer_t1356_il2cpp_TypeInfo;
static TypeInfo* Comparer_1_t2871_InterfacesTypeInfos[] = 
{
	&IComparer_1_t6034_il2cpp_TypeInfo,
	&IComparer_t1356_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Comparer_1_t2871_InterfacesOffsets[] = 
{
	{ &IComparer_1_t6034_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern TypeInfo Comparer_1_t2871_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t2871_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t2872_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
static Il2CppRGCTXData Comparer_1_t2871_RGCTXData[8] = 
{
	&IComparable_1_t6035_0_0_0/* Type Usage */,
	&MethodInfo_t141_0_0_0/* Type Usage */,
	&Comparer_1_t2871_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_t2871_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t2872_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m14853_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_Compare_m43175_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparer_1_t2871_0_0_0;
extern Il2CppType Comparer_1_t2871_1_0_0;
struct Comparer_1_t2871;
extern Il2CppGenericClass Comparer_1_t2871_GenericClass;
TypeInfo Comparer_1_t2871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, Comparer_1_t2871_MethodInfos/* methods */
	, Comparer_1_t2871_PropertyInfos/* properties */
	, Comparer_1_t2871_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* element_class */
	, Comparer_1_t2871_InterfacesTypeInfos/* implemented_interfaces */
	, Comparer_1_t2871_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* cast_class */
	, &Comparer_1_t2871_0_0_0/* byval_arg */
	, &Comparer_1_t2871_1_0_0/* this_arg */
	, Comparer_1_t2871_InterfacesOffsets/* interface_offsets */
	, &Comparer_1_t2871_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Comparer_1_t2871_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparer_1_t2871)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Comparer_1_t2871_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IComparer`1<System.Reflection.MethodInfo>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<System.Reflection.MethodInfo>
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IComparer_1_t6034_IComparer_1_Compare_m32909_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m32909_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<System.Reflection.MethodInfo>::Compare(T,T)
MethodInfo IComparer_1_Compare_m32909_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t6034_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, IComparer_1_t6034_IComparer_1_Compare_m32909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m32909_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t6034_MethodInfos[] =
{
	&IComparer_1_Compare_m32909_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t6034_0_0_0;
extern Il2CppType IComparer_1_t6034_1_0_0;
struct IComparer_1_t6034;
extern Il2CppGenericClass IComparer_1_t6034_GenericClass;
TypeInfo IComparer_1_t6034_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t6034_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t6034_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t6034_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t6034_0_0_0/* byval_arg */
	, &IComparer_1_t6034_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t6034_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.IComparable`1<System.Reflection.MethodInfo>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Reflection.MethodInfo>
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo IComparable_1_t6035_IComparable_1_CompareTo_m32910_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m32910_GenericMethod;
// System.Int32 System.IComparable`1<System.Reflection.MethodInfo>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m32910_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t6035_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IComparable_1_t6035_IComparable_1_CompareTo_m32910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m32910_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t6035_MethodInfos[] =
{
	&IComparable_1_CompareTo_m32910_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t6035_1_0_0;
struct IComparable_1_t6035;
extern Il2CppGenericClass IComparable_1_t6035_GenericClass;
TypeInfo IComparable_1_t6035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t6035_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t6035_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t6035_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t6035_0_0_0/* byval_arg */
	, &IComparable_1_t6035_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t6035_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
extern MethodInfo Comparer_1__ctor_m14849_MethodInfo;
extern MethodInfo IComparable_1_CompareTo_m32910_MethodInfo;
extern MethodInfo IComparable_CompareTo_m13522_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>::.ctor()
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>::Compare(T,T)
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m14853_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>::.ctor()
MethodInfo DefaultComparer__ctor_m14853_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m14676_gshared/* method */
	, &DefaultComparer_t2872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m14853_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo DefaultComparer_t2872_DefaultComparer_Compare_m14854_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Compare_m14854_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.MethodInfo>::Compare(T,T)
MethodInfo DefaultComparer_Compare_m14854_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&DefaultComparer_Compare_m14677_gshared/* method */
	, &DefaultComparer_t2872_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, DefaultComparer_t2872_DefaultComparer_Compare_m14854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Compare_m14854_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t2872_MethodInfos[] =
{
	&DefaultComparer__ctor_m14853_MethodInfo,
	&DefaultComparer_Compare_m14854_MethodInfo,
	NULL
};
extern MethodInfo DefaultComparer_Compare_m14854_MethodInfo;
static MethodInfo* DefaultComparer_t2872_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Compare_m14854_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m14851_MethodInfo,
	&DefaultComparer_Compare_m14854_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t2872_InterfacesOffsets[] = 
{
	{ &IComparer_1_t6034_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern TypeInfo Comparer_1_t2871_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t2871_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t2872_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t141_il2cpp_TypeInfo;
extern TypeInfo IComparable_1_t6035_il2cpp_TypeInfo;
static Il2CppRGCTXData DefaultComparer_t2872_RGCTXData[12] = 
{
	&IComparable_1_t6035_0_0_0/* Type Usage */,
	&MethodInfo_t141_0_0_0/* Type Usage */,
	&Comparer_1_t2871_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_t2871_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t2872_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m14853_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_Compare_m43175_MethodInfo/* Method Usage */,
	&Comparer_1__ctor_m14849_MethodInfo/* Method Usage */,
	&MethodInfo_t141_il2cpp_TypeInfo/* Class Usage */,
	&IComparable_1_t6035_il2cpp_TypeInfo/* Class Usage */,
	&IComparable_1_CompareTo_m32910_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t2872_0_0_0;
extern Il2CppType DefaultComparer_t2872_1_0_0;
struct DefaultComparer_t2872;
extern Il2CppGenericClass DefaultComparer_t2872_GenericClass;
extern TypeInfo Comparer_1_t1854_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t2872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t2872_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t2871_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Comparer_1_t1854_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t2872_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t2872_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t2872_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t2872_0_0_0/* byval_arg */
	, &DefaultComparer_t2872_1_0_0/* this_arg */
	, DefaultComparer_t2872_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t2872_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, DefaultComparer_t2872_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t2872)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Comparison`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Comparison_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Comparison_1_t2831_il2cpp_TypeInfo;
// System.Comparison`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"



// System.Void System.Comparison`1<System.Reflection.MethodInfo>::.ctor(System.Object,System.IntPtr)
// System.Int32 System.Comparison`1<System.Reflection.MethodInfo>::Invoke(T,T)
// System.IAsyncResult System.Comparison`1<System.Reflection.MethodInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
// System.Int32 System.Comparison`1<System.Reflection.MethodInfo>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Comparison`1<System.Reflection.MethodInfo>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Comparison_1_t2831_Comparison_1__ctor_m14855_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1__ctor_m14855_GenericMethod;
// System.Void System.Comparison`1<System.Reflection.MethodInfo>::.ctor(System.Object,System.IntPtr)
MethodInfo Comparison_1__ctor_m14855_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparison_1__ctor_m14743_gshared/* method */
	, &Comparison_1_t2831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Comparison_1_t2831_Comparison_1__ctor_m14855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1__ctor_m14855_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo Comparison_1_t2831_Comparison_1_Invoke_m14856_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_Invoke_m14856_GenericMethod;
// System.Int32 System.Comparison`1<System.Reflection.MethodInfo>::Invoke(T,T)
MethodInfo Comparison_1_Invoke_m14856_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Comparison_1_Invoke_m14744_gshared/* method */
	, &Comparison_1_t2831_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparison_1_t2831_Comparison_1_Invoke_m14856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_Invoke_m14856_GenericMethod/* genericMethod */

};
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparison_1_t2831_Comparison_1_BeginInvoke_m14857_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_BeginInvoke_m14857_GenericMethod;
// System.IAsyncResult System.Comparison`1<System.Reflection.MethodInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
MethodInfo Comparison_1_BeginInvoke_m14857_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Comparison_1_BeginInvoke_m14745_gshared/* method */
	, &Comparison_1_t2831_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Comparison_1_t2831_Comparison_1_BeginInvoke_m14857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_BeginInvoke_m14857_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Comparison_1_t2831_Comparison_1_EndInvoke_m14858_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_EndInvoke_m14858_GenericMethod;
// System.Int32 System.Comparison`1<System.Reflection.MethodInfo>::EndInvoke(System.IAsyncResult)
MethodInfo Comparison_1_EndInvoke_m14858_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Comparison_1_EndInvoke_m14746_gshared/* method */
	, &Comparison_1_t2831_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Comparison_1_t2831_Comparison_1_EndInvoke_m14858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_EndInvoke_m14858_GenericMethod/* genericMethod */

};
static MethodInfo* Comparison_1_t2831_MethodInfos[] =
{
	&Comparison_1__ctor_m14855_MethodInfo,
	&Comparison_1_Invoke_m14856_MethodInfo,
	&Comparison_1_BeginInvoke_m14857_MethodInfo,
	&Comparison_1_EndInvoke_m14858_MethodInfo,
	NULL
};
extern MethodInfo Comparison_1_Invoke_m14856_MethodInfo;
extern MethodInfo Comparison_1_BeginInvoke_m14857_MethodInfo;
extern MethodInfo Comparison_1_EndInvoke_m14858_MethodInfo;
static MethodInfo* Comparison_1_t2831_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Comparison_1_Invoke_m14856_MethodInfo,
	&Comparison_1_BeginInvoke_m14857_MethodInfo,
	&Comparison_1_EndInvoke_m14858_MethodInfo,
};
static Il2CppInterfaceOffsetPair Comparison_1_t2831_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparison_1_t2831_0_0_0;
extern Il2CppType Comparison_1_t2831_1_0_0;
struct Comparison_1_t2831;
extern Il2CppGenericClass Comparison_1_t2831_GenericClass;
TypeInfo Comparison_1_t2831_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t2831_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparison_1_t2831_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Comparison_1_t2831_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparison_1_t2831_il2cpp_TypeInfo/* cast_class */
	, &Comparison_1_t2831_0_0_0/* byval_arg */
	, &Comparison_1_t2831_1_0_0/* this_arg */
	, Comparison_1_t2831_InterfacesOffsets/* interface_offsets */
	, &Comparison_1_t2831_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparison_1_t2831)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6037_il2cpp_TypeInfo;

// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.KeepAliveBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.KeepAliveBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43176_MethodInfo;
static PropertyInfo IEnumerator_1_t6037____Current_PropertyInfo = 
{
	&IEnumerator_1_t6037_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6037_PropertyInfos[] =
{
	&IEnumerator_1_t6037____Current_PropertyInfo,
	NULL
};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43176_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.KeepAliveBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43176_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6037_il2cpp_TypeInfo/* declaring_type */
	, &KeepAliveBehaviour_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43176_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6037_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43176_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6037_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6037_0_0_0;
extern Il2CppType IEnumerator_1_t6037_1_0_0;
struct IEnumerator_1_t6037;
extern Il2CppGenericClass IEnumerator_1_t6037_GenericClass;
TypeInfo IEnumerator_1_t6037_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6037_MethodInfos/* methods */
	, IEnumerator_1_t6037_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6037_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6037_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6037_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6037_0_0_0/* byval_arg */
	, &IEnumerator_1_t6037_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6037_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_57.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2873_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_57MethodDeclarations.h"

extern TypeInfo KeepAliveBehaviour_t37_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14863_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeepAliveBehaviour_t37_m32915_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.KeepAliveBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.KeepAliveBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisKeepAliveBehaviour_t37_m32915(__this, p0, method) (KeepAliveBehaviour_t37 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2873____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2873, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2873____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2873, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2873_FieldInfos[] =
{
	&InternalEnumerator_1_t2873____array_0_FieldInfo,
	&InternalEnumerator_1_t2873____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2873____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2873_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2873____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2873_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14863_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2873_PropertyInfos[] =
{
	&InternalEnumerator_1_t2873____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2873____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2873_InternalEnumerator_1__ctor_m14859_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14859_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14859_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2873_InternalEnumerator_1__ctor_m14859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14859_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14861_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14861_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14861_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14862_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14862_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14862_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14863_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.KeepAliveBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14863_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* declaring_type */
	, &KeepAliveBehaviour_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14863_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2873_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14859_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_MethodInfo,
	&InternalEnumerator_1_Dispose_m14861_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14862_MethodInfo,
	&InternalEnumerator_1_get_Current_m14863_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14862_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14861_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2873_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14862_MethodInfo,
	&InternalEnumerator_1_Dispose_m14861_MethodInfo,
	&InternalEnumerator_1_get_Current_m14863_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2873_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6037_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2873_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6037_il2cpp_TypeInfo, 7},
};
extern TypeInfo KeepAliveBehaviour_t37_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2873_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14863_MethodInfo/* Method Usage */,
	&KeepAliveBehaviour_t37_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisKeepAliveBehaviour_t37_m32915_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2873_0_0_0;
extern Il2CppType InternalEnumerator_1_t2873_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2873_GenericClass;
TypeInfo InternalEnumerator_1_t2873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2873_MethodInfos/* methods */
	, InternalEnumerator_1_t2873_PropertyInfos/* properties */
	, InternalEnumerator_1_t2873_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2873_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2873_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2873_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2873_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2873_1_0_0/* this_arg */
	, InternalEnumerator_1_t2873_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2873_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2873_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2873)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7682_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>
extern MethodInfo ICollection_1_get_Count_m43177_MethodInfo;
static PropertyInfo ICollection_1_t7682____Count_PropertyInfo = 
{
	&ICollection_1_t7682_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43177_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43178_MethodInfo;
static PropertyInfo ICollection_1_t7682____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7682_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43178_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7682_PropertyInfos[] =
{
	&ICollection_1_t7682____Count_PropertyInfo,
	&ICollection_1_t7682____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43177_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43177_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43177_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43178_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43178_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43178_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo ICollection_1_t7682_ICollection_1_Add_m43179_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43179_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43179_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7682_ICollection_1_Add_m43179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43179_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43180_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43180_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43180_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo ICollection_1_t7682_ICollection_1_Contains_m43181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43181_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43181_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7682_ICollection_1_Contains_m43181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43181_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviourU5BU5D_t5369_0_0_0;
extern Il2CppType KeepAliveBehaviourU5BU5D_t5369_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7682_ICollection_1_CopyTo_m43182_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviourU5BU5D_t5369_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43182_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43182_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7682_ICollection_1_CopyTo_m43182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43182_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo ICollection_1_t7682_ICollection_1_Remove_m43183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43183_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43183_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7682_ICollection_1_Remove_m43183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43183_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7682_MethodInfos[] =
{
	&ICollection_1_get_Count_m43177_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43178_MethodInfo,
	&ICollection_1_Add_m43179_MethodInfo,
	&ICollection_1_Clear_m43180_MethodInfo,
	&ICollection_1_Contains_m43181_MethodInfo,
	&ICollection_1_CopyTo_m43182_MethodInfo,
	&ICollection_1_Remove_m43183_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7684_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7682_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7684_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7682_0_0_0;
extern Il2CppType ICollection_1_t7682_1_0_0;
struct ICollection_1_t7682;
extern Il2CppGenericClass ICollection_1_t7682_GenericClass;
TypeInfo ICollection_1_t7682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7682_MethodInfos/* methods */
	, ICollection_1_t7682_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7682_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7682_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7682_0_0_0/* byval_arg */
	, &ICollection_1_t7682_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7682_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.KeepAliveBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.KeepAliveBehaviour>
extern Il2CppType IEnumerator_1_t6037_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43184_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.KeepAliveBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43184_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7684_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6037_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43184_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7684_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43184_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7684_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7684_0_0_0;
extern Il2CppType IEnumerable_1_t7684_1_0_0;
struct IEnumerable_1_t7684;
extern Il2CppGenericClass IEnumerable_1_t7684_GenericClass;
TypeInfo IEnumerable_1_t7684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7684_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7684_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7684_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7684_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7684_0_0_0/* byval_arg */
	, &IEnumerable_1_t7684_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7684_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7683_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>
extern MethodInfo IList_1_get_Item_m43185_MethodInfo;
extern MethodInfo IList_1_set_Item_m43186_MethodInfo;
static PropertyInfo IList_1_t7683____Item_PropertyInfo = 
{
	&IList_1_t7683_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43185_MethodInfo/* get */
	, &IList_1_set_Item_m43186_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7683_PropertyInfos[] =
{
	&IList_1_t7683____Item_PropertyInfo,
	NULL
};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo IList_1_t7683_IList_1_IndexOf_m43187_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43187_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43187_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7683_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7683_IList_1_IndexOf_m43187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43187_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo IList_1_t7683_IList_1_Insert_m43188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43188_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43188_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7683_IList_1_Insert_m43188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43188_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7683_IList_1_RemoveAt_m43189_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43189_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43189_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7683_IList_1_RemoveAt_m43189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43189_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7683_IList_1_get_Item_m43185_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43185_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43185_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7683_il2cpp_TypeInfo/* declaring_type */
	, &KeepAliveBehaviour_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7683_IList_1_get_Item_m43185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43185_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo IList_1_t7683_IList_1_set_Item_m43186_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43186_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43186_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7683_IList_1_set_Item_m43186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43186_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7683_MethodInfos[] =
{
	&IList_1_IndexOf_m43187_MethodInfo,
	&IList_1_Insert_m43188_MethodInfo,
	&IList_1_RemoveAt_m43189_MethodInfo,
	&IList_1_get_Item_m43185_MethodInfo,
	&IList_1_set_Item_m43186_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7683_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7682_il2cpp_TypeInfo,
	&IEnumerable_1_t7684_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7683_0_0_0;
extern Il2CppType IList_1_t7683_1_0_0;
struct IList_1_t7683;
extern Il2CppGenericClass IList_1_t7683_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7683_MethodInfos/* methods */
	, IList_1_t7683_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7683_il2cpp_TypeInfo/* element_class */
	, IList_1_t7683_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7683_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7683_0_0_0/* byval_arg */
	, &IList_1_t7683_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7683_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7685_il2cpp_TypeInfo;

// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43190_MethodInfo;
static PropertyInfo ICollection_1_t7685____Count_PropertyInfo = 
{
	&ICollection_1_t7685_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43191_MethodInfo;
static PropertyInfo ICollection_1_t7685____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7685_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7685_PropertyInfos[] =
{
	&ICollection_1_t7685____Count_PropertyInfo,
	&ICollection_1_t7685____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43190_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43190_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43190_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43191_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43191_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43191_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
static ParameterInfo ICollection_1_t7685_ICollection_1_Add_m43192_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43192_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43192_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7685_ICollection_1_Add_m43192_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43192_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43193_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43193_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43193_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
static ParameterInfo ICollection_1_t7685_ICollection_1_Contains_m43194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviour_t38_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43194_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43194_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7685_ICollection_1_Contains_m43194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43194_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveAbstractBehaviourU5BU5D_t5632_0_0_0;
extern Il2CppType KeepAliveAbstractBehaviourU5BU5D_t5632_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7685_ICollection_1_CopyTo_m43195_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviourU5BU5D_t5632_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43195_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43195_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7685_ICollection_1_CopyTo_m43195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43195_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
static ParameterInfo ICollection_1_t7685_ICollection_1_Remove_m43196_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviour_t38_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43196_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.KeepAliveAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43196_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7685_ICollection_1_Remove_m43196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43196_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7685_MethodInfos[] =
{
	&ICollection_1_get_Count_m43190_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43191_MethodInfo,
	&ICollection_1_Add_m43192_MethodInfo,
	&ICollection_1_Clear_m43193_MethodInfo,
	&ICollection_1_Contains_m43194_MethodInfo,
	&ICollection_1_CopyTo_m43195_MethodInfo,
	&ICollection_1_Remove_m43196_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7687_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7685_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7687_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7685_0_0_0;
extern Il2CppType ICollection_1_t7685_1_0_0;
struct ICollection_1_t7685;
extern Il2CppGenericClass ICollection_1_t7685_GenericClass;
TypeInfo ICollection_1_t7685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7685_MethodInfos/* methods */
	, ICollection_1_t7685_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7685_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7685_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7685_0_0_0/* byval_arg */
	, &ICollection_1_t7685_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7685_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.KeepAliveAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.KeepAliveAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6039_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43197_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.KeepAliveAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43197_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7687_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6039_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43197_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7687_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43197_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7687_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7687_0_0_0;
extern Il2CppType IEnumerable_1_t7687_1_0_0;
struct IEnumerable_1_t7687;
extern Il2CppGenericClass IEnumerable_1_t7687_GenericClass;
TypeInfo IEnumerable_1_t7687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7687_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7687_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7687_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7687_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7687_0_0_0/* byval_arg */
	, &IEnumerable_1_t7687_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7687_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6039_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43198_MethodInfo;
static PropertyInfo IEnumerator_1_t6039____Current_PropertyInfo = 
{
	&IEnumerator_1_t6039_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43198_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6039_PropertyInfos[] =
{
	&IEnumerator_1_t6039____Current_PropertyInfo,
	NULL
};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43198_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43198_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6039_il2cpp_TypeInfo/* declaring_type */
	, &KeepAliveAbstractBehaviour_t38_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43198_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6039_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43198_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6039_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6039_0_0_0;
extern Il2CppType IEnumerator_1_t6039_1_0_0;
struct IEnumerator_1_t6039;
extern Il2CppGenericClass IEnumerator_1_t6039_GenericClass;
TypeInfo IEnumerator_1_t6039_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6039_MethodInfos/* methods */
	, IEnumerator_1_t6039_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6039_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6039_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6039_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6039_0_0_0/* byval_arg */
	, &IEnumerator_1_t6039_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6039_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_58.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2874_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_58MethodDeclarations.h"

extern TypeInfo KeepAliveAbstractBehaviour_t38_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14868_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeepAliveAbstractBehaviour_t38_m32926_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.KeepAliveAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.KeepAliveAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisKeepAliveAbstractBehaviour_t38_m32926(__this, p0, method) (KeepAliveAbstractBehaviour_t38 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2874____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2874, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2874____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2874, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2874_FieldInfos[] =
{
	&InternalEnumerator_1_t2874____array_0_FieldInfo,
	&InternalEnumerator_1_t2874____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2874____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2874_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2874____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2874_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14868_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2874_PropertyInfos[] =
{
	&InternalEnumerator_1_t2874____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2874____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2874_InternalEnumerator_1__ctor_m14864_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14864_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2874_InternalEnumerator_1__ctor_m14864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14864_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14866_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14866_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14866_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14867_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14867_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14867_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14868_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14868_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* declaring_type */
	, &KeepAliveAbstractBehaviour_t38_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14868_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2874_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14864_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_MethodInfo,
	&InternalEnumerator_1_Dispose_m14866_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14867_MethodInfo,
	&InternalEnumerator_1_get_Current_m14868_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14867_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14866_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2874_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14865_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14867_MethodInfo,
	&InternalEnumerator_1_Dispose_m14866_MethodInfo,
	&InternalEnumerator_1_get_Current_m14868_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2874_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6039_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2874_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6039_il2cpp_TypeInfo, 7},
};
extern TypeInfo KeepAliveAbstractBehaviour_t38_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2874_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14868_MethodInfo/* Method Usage */,
	&KeepAliveAbstractBehaviour_t38_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisKeepAliveAbstractBehaviour_t38_m32926_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2874_0_0_0;
extern Il2CppType InternalEnumerator_1_t2874_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2874_GenericClass;
TypeInfo InternalEnumerator_1_t2874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2874_MethodInfos/* methods */
	, InternalEnumerator_1_t2874_PropertyInfos/* properties */
	, InternalEnumerator_1_t2874_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2874_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2874_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2874_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2874_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2874_1_0_0/* this_arg */
	, InternalEnumerator_1_t2874_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2874_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2874_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2874)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7686_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43199_MethodInfo;
extern MethodInfo IList_1_set_Item_m43200_MethodInfo;
static PropertyInfo IList_1_t7686____Item_PropertyInfo = 
{
	&IList_1_t7686_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43199_MethodInfo/* get */
	, &IList_1_set_Item_m43200_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7686_PropertyInfos[] =
{
	&IList_1_t7686____Item_PropertyInfo,
	NULL
};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
static ParameterInfo IList_1_t7686_IList_1_IndexOf_m43201_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviour_t38_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43201_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43201_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7686_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7686_IList_1_IndexOf_m43201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43201_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
static ParameterInfo IList_1_t7686_IList_1_Insert_m43202_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43202_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43202_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7686_IList_1_Insert_m43202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43202_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7686_IList_1_RemoveAt_m43203_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43203_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43203_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7686_IList_1_RemoveAt_m43203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43203_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7686_IList_1_get_Item_m43199_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43199_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43199_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7686_il2cpp_TypeInfo/* declaring_type */
	, &KeepAliveAbstractBehaviour_t38_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7686_IList_1_get_Item_m43199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43199_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeepAliveAbstractBehaviour_t38_0_0_0;
static ParameterInfo IList_1_t7686_IList_1_set_Item_m43200_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &KeepAliveAbstractBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43200_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.KeepAliveAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43200_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7686_IList_1_set_Item_m43200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43200_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7686_MethodInfos[] =
{
	&IList_1_IndexOf_m43201_MethodInfo,
	&IList_1_Insert_m43202_MethodInfo,
	&IList_1_RemoveAt_m43203_MethodInfo,
	&IList_1_get_Item_m43199_MethodInfo,
	&IList_1_set_Item_m43200_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7686_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7685_il2cpp_TypeInfo,
	&IEnumerable_1_t7687_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7686_0_0_0;
extern Il2CppType IList_1_t7686_1_0_0;
struct IList_1_t7686;
extern Il2CppGenericClass IList_1_t7686_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7686_MethodInfos/* methods */
	, IList_1_t7686_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7686_il2cpp_TypeInfo/* element_class */
	, IList_1_t7686_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7686_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7686_0_0_0/* byval_arg */
	, &IList_1_t7686_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7686_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_15.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2875_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_15MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_11.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2876_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_11MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14871_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14873_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2875____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2875_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2875, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2875_FieldInfos[] =
{
	&CachedInvokableCall_1_t2875____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2875_CachedInvokableCall_1__ctor_m14869_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14869_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14869_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2875_CachedInvokableCall_1__ctor_m14869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14869_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2875_CachedInvokableCall_1_Invoke_m14870_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14870_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14870_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2875_CachedInvokableCall_1_Invoke_m14870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14870_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2875_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14869_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14870_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14870_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14874_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2875_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14870_MethodInfo,
	&InvokableCall_1_Find_m14874_MethodInfo,
};
extern Il2CppType UnityAction_1_t2877_0_0_0;
extern TypeInfo UnityAction_1_t2877_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisKeepAliveBehaviour_t37_m32936_MethodInfo;
extern TypeInfo KeepAliveBehaviour_t37_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14876_MethodInfo;
extern TypeInfo KeepAliveBehaviour_t37_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2875_RGCTXData[8] = 
{
	&UnityAction_1_t2877_0_0_0/* Type Usage */,
	&UnityAction_1_t2877_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisKeepAliveBehaviour_t37_m32936_MethodInfo/* Method Usage */,
	&KeepAliveBehaviour_t37_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14876_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14871_MethodInfo/* Method Usage */,
	&KeepAliveBehaviour_t37_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14873_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2875_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2875_1_0_0;
struct CachedInvokableCall_1_t2875;
extern Il2CppGenericClass CachedInvokableCall_1_t2875_GenericClass;
TypeInfo CachedInvokableCall_1_t2875_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2875_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2875_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2875_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2875_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2875_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2875_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2875_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2875_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2875_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2875)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_18.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
extern TypeInfo UnityAction_1_t2877_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_18MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.KeepAliveBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.KeepAliveBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisKeepAliveBehaviour_t37_m32936(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>
extern Il2CppType UnityAction_1_t2877_0_0_1;
FieldInfo InvokableCall_1_t2876____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2877_0_0_1/* type */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2876, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2876_FieldInfos[] =
{
	&InvokableCall_1_t2876____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2876_InvokableCall_1__ctor_m14871_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14871_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14871_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2876_InvokableCall_1__ctor_m14871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14871_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2877_0_0_0;
static ParameterInfo InvokableCall_1_t2876_InvokableCall_1__ctor_m14872_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2877_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14872_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14872_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2876_InvokableCall_1__ctor_m14872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14872_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2876_InvokableCall_1_Invoke_m14873_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14873_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14873_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2876_InvokableCall_1_Invoke_m14873_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14873_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2876_InvokableCall_1_Find_m14874_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14874_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14874_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2876_InvokableCall_1_Find_m14874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14874_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2876_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14871_MethodInfo,
	&InvokableCall_1__ctor_m14872_MethodInfo,
	&InvokableCall_1_Invoke_m14873_MethodInfo,
	&InvokableCall_1_Find_m14874_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2876_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14873_MethodInfo,
	&InvokableCall_1_Find_m14874_MethodInfo,
};
extern TypeInfo UnityAction_1_t2877_il2cpp_TypeInfo;
extern TypeInfo KeepAliveBehaviour_t37_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2876_RGCTXData[5] = 
{
	&UnityAction_1_t2877_0_0_0/* Type Usage */,
	&UnityAction_1_t2877_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisKeepAliveBehaviour_t37_m32936_MethodInfo/* Method Usage */,
	&KeepAliveBehaviour_t37_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14876_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2876_0_0_0;
extern Il2CppType InvokableCall_1_t2876_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2876;
extern Il2CppGenericClass InvokableCall_1_t2876_GenericClass;
TypeInfo InvokableCall_1_t2876_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2876_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2876_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2876_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2876_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2876_0_0_0/* byval_arg */
	, &InvokableCall_1_t2876_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2876_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2876_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2876)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2877_UnityAction_1__ctor_m14875_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14875_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14875_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2877_UnityAction_1__ctor_m14875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14875_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
static ParameterInfo UnityAction_1_t2877_UnityAction_1_Invoke_m14876_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14876_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14876_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2877_UnityAction_1_Invoke_m14876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14876_GenericMethod/* genericMethod */

};
extern Il2CppType KeepAliveBehaviour_t37_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2877_UnityAction_1_BeginInvoke_m14877_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &KeepAliveBehaviour_t37_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14877_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14877_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2877_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2877_UnityAction_1_BeginInvoke_m14877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14877_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2877_UnityAction_1_EndInvoke_m14878_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14878_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14878_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2877_UnityAction_1_EndInvoke_m14878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14878_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2877_MethodInfos[] =
{
	&UnityAction_1__ctor_m14875_MethodInfo,
	&UnityAction_1_Invoke_m14876_MethodInfo,
	&UnityAction_1_BeginInvoke_m14877_MethodInfo,
	&UnityAction_1_EndInvoke_m14878_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14877_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14878_MethodInfo;
static MethodInfo* UnityAction_1_t2877_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14876_MethodInfo,
	&UnityAction_1_BeginInvoke_m14877_MethodInfo,
	&UnityAction_1_EndInvoke_m14878_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2877_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2877_1_0_0;
struct UnityAction_1_t2877;
extern Il2CppGenericClass UnityAction_1_t2877_GenericClass;
TypeInfo UnityAction_1_t2877_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2877_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2877_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2877_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2877_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2877_0_0_0/* byval_arg */
	, &UnityAction_1_t2877_1_0_0/* this_arg */
	, UnityAction_1_t2877_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2877_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2877)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6041_il2cpp_TypeInfo;

// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.MarkerBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.MarkerBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43204_MethodInfo;
static PropertyInfo IEnumerator_1_t6041____Current_PropertyInfo = 
{
	&IEnumerator_1_t6041_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43204_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6041_PropertyInfos[] =
{
	&IEnumerator_1_t6041____Current_PropertyInfo,
	NULL
};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43204_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.MarkerBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43204_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6041_il2cpp_TypeInfo/* declaring_type */
	, &MarkerBehaviour_t39_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43204_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6041_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43204_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6041_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6041_0_0_0;
extern Il2CppType IEnumerator_1_t6041_1_0_0;
struct IEnumerator_1_t6041;
extern Il2CppGenericClass IEnumerator_1_t6041_GenericClass;
TypeInfo IEnumerator_1_t6041_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6041_MethodInfos/* methods */
	, IEnumerator_1_t6041_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6041_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6041_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6041_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6041_0_0_0/* byval_arg */
	, &IEnumerator_1_t6041_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6041_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2878_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59MethodDeclarations.h"

extern TypeInfo MarkerBehaviour_t39_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14883_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMarkerBehaviour_t39_m32938_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.MarkerBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.MarkerBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMarkerBehaviour_t39_m32938(__this, p0, method) (MarkerBehaviour_t39 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2878____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2878, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2878____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2878, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2878_FieldInfos[] =
{
	&InternalEnumerator_1_t2878____array_0_FieldInfo,
	&InternalEnumerator_1_t2878____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2878____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2878_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2878____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2878_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14883_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2878_PropertyInfos[] =
{
	&InternalEnumerator_1_t2878____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2878____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2878_InternalEnumerator_1__ctor_m14879_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14879_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14879_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2878_InternalEnumerator_1__ctor_m14879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14879_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14881_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14881_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14881_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14882_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14882_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14882_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14883_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.MarkerBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14883_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* declaring_type */
	, &MarkerBehaviour_t39_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14883_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2878_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14879_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_MethodInfo,
	&InternalEnumerator_1_Dispose_m14881_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14882_MethodInfo,
	&InternalEnumerator_1_get_Current_m14883_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14882_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14881_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2878_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14880_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14882_MethodInfo,
	&InternalEnumerator_1_Dispose_m14881_MethodInfo,
	&InternalEnumerator_1_get_Current_m14883_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2878_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6041_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2878_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6041_il2cpp_TypeInfo, 7},
};
extern TypeInfo MarkerBehaviour_t39_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2878_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14883_MethodInfo/* Method Usage */,
	&MarkerBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMarkerBehaviour_t39_m32938_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2878_0_0_0;
extern Il2CppType InternalEnumerator_1_t2878_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2878_GenericClass;
TypeInfo InternalEnumerator_1_t2878_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2878_MethodInfos/* methods */
	, InternalEnumerator_1_t2878_PropertyInfos/* properties */
	, InternalEnumerator_1_t2878_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2878_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2878_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2878_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2878_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2878_1_0_0/* this_arg */
	, InternalEnumerator_1_t2878_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2878_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2878_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2878)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7688_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>
extern MethodInfo ICollection_1_get_Count_m43205_MethodInfo;
static PropertyInfo ICollection_1_t7688____Count_PropertyInfo = 
{
	&ICollection_1_t7688_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43205_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43206_MethodInfo;
static PropertyInfo ICollection_1_t7688____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7688_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43206_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7688_PropertyInfos[] =
{
	&ICollection_1_t7688____Count_PropertyInfo,
	&ICollection_1_t7688____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43205_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43205_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43205_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43206_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43206_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43206_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo ICollection_1_t7688_ICollection_1_Add_m43207_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43207_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43207_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7688_ICollection_1_Add_m43207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43207_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43208_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43208_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43208_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo ICollection_1_t7688_ICollection_1_Contains_m43209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43209_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43209_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7688_ICollection_1_Contains_m43209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43209_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviourU5BU5D_t5370_0_0_0;
extern Il2CppType MarkerBehaviourU5BU5D_t5370_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7688_ICollection_1_CopyTo_m43210_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviourU5BU5D_t5370_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43210_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43210_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7688_ICollection_1_CopyTo_m43210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43210_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo ICollection_1_t7688_ICollection_1_Remove_m43211_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43211_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43211_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7688_ICollection_1_Remove_m43211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43211_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7688_MethodInfos[] =
{
	&ICollection_1_get_Count_m43205_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43206_MethodInfo,
	&ICollection_1_Add_m43207_MethodInfo,
	&ICollection_1_Clear_m43208_MethodInfo,
	&ICollection_1_Contains_m43209_MethodInfo,
	&ICollection_1_CopyTo_m43210_MethodInfo,
	&ICollection_1_Remove_m43211_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7690_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7688_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7690_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7688_0_0_0;
extern Il2CppType ICollection_1_t7688_1_0_0;
struct ICollection_1_t7688;
extern Il2CppGenericClass ICollection_1_t7688_GenericClass;
TypeInfo ICollection_1_t7688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7688_MethodInfos/* methods */
	, ICollection_1_t7688_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7688_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7688_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7688_0_0_0/* byval_arg */
	, &ICollection_1_t7688_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7688_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MarkerBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.MarkerBehaviour>
extern Il2CppType IEnumerator_1_t6041_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43212_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MarkerBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43212_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7690_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6041_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43212_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7690_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43212_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7690_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7690_0_0_0;
extern Il2CppType IEnumerable_1_t7690_1_0_0;
struct IEnumerable_1_t7690;
extern Il2CppGenericClass IEnumerable_1_t7690_GenericClass;
TypeInfo IEnumerable_1_t7690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7690_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7690_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7690_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7690_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7690_0_0_0/* byval_arg */
	, &IEnumerable_1_t7690_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7690_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7689_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>
extern MethodInfo IList_1_get_Item_m43213_MethodInfo;
extern MethodInfo IList_1_set_Item_m43214_MethodInfo;
static PropertyInfo IList_1_t7689____Item_PropertyInfo = 
{
	&IList_1_t7689_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43213_MethodInfo/* get */
	, &IList_1_set_Item_m43214_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7689_PropertyInfos[] =
{
	&IList_1_t7689____Item_PropertyInfo,
	NULL
};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo IList_1_t7689_IList_1_IndexOf_m43215_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43215_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43215_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7689_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7689_IList_1_IndexOf_m43215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43215_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo IList_1_t7689_IList_1_Insert_m43216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43216_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43216_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7689_IList_1_Insert_m43216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43216_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7689_IList_1_RemoveAt_m43217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43217_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43217_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7689_IList_1_RemoveAt_m43217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43217_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7689_IList_1_get_Item_m43213_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43213_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43213_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7689_il2cpp_TypeInfo/* declaring_type */
	, &MarkerBehaviour_t39_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7689_IList_1_get_Item_m43213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43213_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo IList_1_t7689_IList_1_set_Item_m43214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43214_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43214_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7689_IList_1_set_Item_m43214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43214_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7689_MethodInfos[] =
{
	&IList_1_IndexOf_m43215_MethodInfo,
	&IList_1_Insert_m43216_MethodInfo,
	&IList_1_RemoveAt_m43217_MethodInfo,
	&IList_1_get_Item_m43213_MethodInfo,
	&IList_1_set_Item_m43214_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7689_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7688_il2cpp_TypeInfo,
	&IEnumerable_1_t7690_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7689_0_0_0;
extern Il2CppType IList_1_t7689_1_0_0;
struct IList_1_t7689;
extern Il2CppGenericClass IList_1_t7689_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7689_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7689_MethodInfos/* methods */
	, IList_1_t7689_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7689_il2cpp_TypeInfo/* element_class */
	, IList_1_t7689_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7689_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7689_0_0_0/* byval_arg */
	, &IList_1_t7689_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7689_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7691_il2cpp_TypeInfo;

// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43218_MethodInfo;
static PropertyInfo ICollection_1_t7691____Count_PropertyInfo = 
{
	&ICollection_1_t7691_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43218_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43219_MethodInfo;
static PropertyInfo ICollection_1_t7691____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7691_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43219_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7691_PropertyInfos[] =
{
	&ICollection_1_t7691____Count_PropertyInfo,
	&ICollection_1_t7691____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43218_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43218_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43218_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43219_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43219_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43219_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
static ParameterInfo ICollection_1_t7691_ICollection_1_Add_m43220_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43220_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43220_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7691_ICollection_1_Add_m43220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43220_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43221_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43221_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43221_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
static ParameterInfo ICollection_1_t7691_ICollection_1_Contains_m43222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43222_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43222_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7691_ICollection_1_Contains_m43222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43222_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerAbstractBehaviourU5BU5D_t908_0_0_0;
extern Il2CppType MarkerAbstractBehaviourU5BU5D_t908_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7691_ICollection_1_CopyTo_m43223_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviourU5BU5D_t908_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43223_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43223_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7691_ICollection_1_CopyTo_m43223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43223_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
static ParameterInfo ICollection_1_t7691_ICollection_1_Remove_m43224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43224_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MarkerAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43224_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7691_ICollection_1_Remove_m43224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43224_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7691_MethodInfos[] =
{
	&ICollection_1_get_Count_m43218_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43219_MethodInfo,
	&ICollection_1_Add_m43220_MethodInfo,
	&ICollection_1_Clear_m43221_MethodInfo,
	&ICollection_1_Contains_m43222_MethodInfo,
	&ICollection_1_CopyTo_m43223_MethodInfo,
	&ICollection_1_Remove_m43224_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7693_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7691_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7693_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7691_0_0_0;
extern Il2CppType ICollection_1_t7691_1_0_0;
struct ICollection_1_t7691;
extern Il2CppGenericClass ICollection_1_t7691_GenericClass;
TypeInfo ICollection_1_t7691_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7691_MethodInfos/* methods */
	, ICollection_1_t7691_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7691_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7691_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7691_0_0_0/* byval_arg */
	, &ICollection_1_t7691_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7691_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MarkerAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.MarkerAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6043_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43225_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MarkerAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43225_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7693_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6043_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43225_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7693_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43225_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7693_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7693_0_0_0;
extern Il2CppType IEnumerable_1_t7693_1_0_0;
struct IEnumerable_1_t7693;
extern Il2CppGenericClass IEnumerable_1_t7693_GenericClass;
TypeInfo IEnumerable_1_t7693_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7693_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7693_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7693_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7693_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7693_0_0_0/* byval_arg */
	, &IEnumerable_1_t7693_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7693_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6043_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.MarkerAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.MarkerAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43226_MethodInfo;
static PropertyInfo IEnumerator_1_t6043____Current_PropertyInfo = 
{
	&IEnumerator_1_t6043_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43226_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6043_PropertyInfos[] =
{
	&IEnumerator_1_t6043____Current_PropertyInfo,
	NULL
};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43226_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.MarkerAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43226_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6043_il2cpp_TypeInfo/* declaring_type */
	, &MarkerAbstractBehaviour_t32_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43226_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6043_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43226_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6043_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6043_0_0_0;
extern Il2CppType IEnumerator_1_t6043_1_0_0;
struct IEnumerator_1_t6043;
extern Il2CppGenericClass IEnumerator_1_t6043_GenericClass;
TypeInfo IEnumerator_1_t6043_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6043_MethodInfos/* methods */
	, IEnumerator_1_t6043_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6043_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6043_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6043_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6043_0_0_0/* byval_arg */
	, &IEnumerator_1_t6043_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6043_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_60.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2879_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_60MethodDeclarations.h"

extern TypeInfo MarkerAbstractBehaviour_t32_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14888_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMarkerAbstractBehaviour_t32_m32949_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.MarkerAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.MarkerAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMarkerAbstractBehaviour_t32_m32949(__this, p0, method) (MarkerAbstractBehaviour_t32 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2879____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2879, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2879____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2879, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2879_FieldInfos[] =
{
	&InternalEnumerator_1_t2879____array_0_FieldInfo,
	&InternalEnumerator_1_t2879____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2879____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2879_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2879____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2879_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14888_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2879_PropertyInfos[] =
{
	&InternalEnumerator_1_t2879____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2879____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2879_InternalEnumerator_1__ctor_m14884_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14884_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14884_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2879_InternalEnumerator_1__ctor_m14884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14884_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14886_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14886_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14886_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14887_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14887_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14887_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14888_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14888_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* declaring_type */
	, &MarkerAbstractBehaviour_t32_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14888_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2879_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14884_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_MethodInfo,
	&InternalEnumerator_1_Dispose_m14886_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14887_MethodInfo,
	&InternalEnumerator_1_get_Current_m14888_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14887_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14886_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2879_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14885_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14887_MethodInfo,
	&InternalEnumerator_1_Dispose_m14886_MethodInfo,
	&InternalEnumerator_1_get_Current_m14888_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2879_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6043_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2879_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6043_il2cpp_TypeInfo, 7},
};
extern TypeInfo MarkerAbstractBehaviour_t32_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2879_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14888_MethodInfo/* Method Usage */,
	&MarkerAbstractBehaviour_t32_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMarkerAbstractBehaviour_t32_m32949_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2879_0_0_0;
extern Il2CppType InternalEnumerator_1_t2879_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2879_GenericClass;
TypeInfo InternalEnumerator_1_t2879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2879_MethodInfos/* methods */
	, InternalEnumerator_1_t2879_PropertyInfos/* properties */
	, InternalEnumerator_1_t2879_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2879_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2879_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2879_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2879_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2879_1_0_0/* this_arg */
	, InternalEnumerator_1_t2879_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2879_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2879_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2879)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7692_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43227_MethodInfo;
extern MethodInfo IList_1_set_Item_m43228_MethodInfo;
static PropertyInfo IList_1_t7692____Item_PropertyInfo = 
{
	&IList_1_t7692_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43227_MethodInfo/* get */
	, &IList_1_set_Item_m43228_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7692_PropertyInfos[] =
{
	&IList_1_t7692____Item_PropertyInfo,
	NULL
};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
static ParameterInfo IList_1_t7692_IList_1_IndexOf_m43229_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43229_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43229_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7692_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7692_IList_1_IndexOf_m43229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43229_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
static ParameterInfo IList_1_t7692_IList_1_Insert_m43230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43230_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43230_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7692_IList_1_Insert_m43230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43230_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7692_IList_1_RemoveAt_m43231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43231_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43231_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7692_IList_1_RemoveAt_m43231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43231_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7692_IList_1_get_Item_m43227_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43227_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43227_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7692_il2cpp_TypeInfo/* declaring_type */
	, &MarkerAbstractBehaviour_t32_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7692_IList_1_get_Item_m43227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43227_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MarkerAbstractBehaviour_t32_0_0_0;
static ParameterInfo IList_1_t7692_IList_1_set_Item_m43228_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MarkerAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43228_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MarkerAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43228_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7692_IList_1_set_Item_m43228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43228_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7692_MethodInfos[] =
{
	&IList_1_IndexOf_m43229_MethodInfo,
	&IList_1_Insert_m43230_MethodInfo,
	&IList_1_RemoveAt_m43231_MethodInfo,
	&IList_1_get_Item_m43227_MethodInfo,
	&IList_1_set_Item_m43228_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7692_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7691_il2cpp_TypeInfo,
	&IEnumerable_1_t7693_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7692_0_0_0;
extern Il2CppType IList_1_t7692_1_0_0;
struct IList_1_t7692;
extern Il2CppGenericClass IList_1_t7692_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7692_MethodInfos/* methods */
	, IList_1_t7692_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7692_il2cpp_TypeInfo/* element_class */
	, IList_1_t7692_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7692_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7692_0_0_0/* byval_arg */
	, &IList_1_t7692_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7692_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7694_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>
extern MethodInfo ICollection_1_get_Count_m43232_MethodInfo;
static PropertyInfo ICollection_1_t7694____Count_PropertyInfo = 
{
	&ICollection_1_t7694_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43233_MethodInfo;
static PropertyInfo ICollection_1_t7694____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7694_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43233_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7694_PropertyInfos[] =
{
	&ICollection_1_t7694____Count_PropertyInfo,
	&ICollection_1_t7694____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43232_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43232_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43232_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43233_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43233_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43233_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
static ParameterInfo ICollection_1_t7694_ICollection_1_Add_m43234_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviour_t155_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43234_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43234_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7694_ICollection_1_Add_m43234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43234_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43235_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43235_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43235_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
static ParameterInfo ICollection_1_t7694_ICollection_1_Contains_m43236_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviour_t155_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43236_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43236_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7694_ICollection_1_Contains_m43236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43236_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMarkerBehaviourU5BU5D_t5633_0_0_0;
extern Il2CppType IEditorMarkerBehaviourU5BU5D_t5633_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7694_ICollection_1_CopyTo_m43237_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviourU5BU5D_t5633_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43237_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43237_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7694_ICollection_1_CopyTo_m43237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43237_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
static ParameterInfo ICollection_1_t7694_ICollection_1_Remove_m43238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviour_t155_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43238_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMarkerBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43238_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7694_ICollection_1_Remove_m43238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43238_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7694_MethodInfos[] =
{
	&ICollection_1_get_Count_m43232_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43233_MethodInfo,
	&ICollection_1_Add_m43234_MethodInfo,
	&ICollection_1_Clear_m43235_MethodInfo,
	&ICollection_1_Contains_m43236_MethodInfo,
	&ICollection_1_CopyTo_m43237_MethodInfo,
	&ICollection_1_Remove_m43238_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7696_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7694_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7696_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7694_0_0_0;
extern Il2CppType ICollection_1_t7694_1_0_0;
struct ICollection_1_t7694;
extern Il2CppGenericClass ICollection_1_t7694_GenericClass;
TypeInfo ICollection_1_t7694_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7694_MethodInfos/* methods */
	, ICollection_1_t7694_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7694_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7694_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7694_0_0_0/* byval_arg */
	, &ICollection_1_t7694_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7694_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorMarkerBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorMarkerBehaviour>
extern Il2CppType IEnumerator_1_t6045_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43239_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorMarkerBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43239_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7696_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6045_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43239_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7696_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43239_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7696_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7696_0_0_0;
extern Il2CppType IEnumerable_1_t7696_1_0_0;
struct IEnumerable_1_t7696;
extern Il2CppGenericClass IEnumerable_1_t7696_GenericClass;
TypeInfo IEnumerable_1_t7696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7696_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7696_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7696_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7696_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7696_0_0_0/* byval_arg */
	, &IEnumerable_1_t7696_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7696_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6045_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorMarkerBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorMarkerBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43240_MethodInfo;
static PropertyInfo IEnumerator_1_t6045____Current_PropertyInfo = 
{
	&IEnumerator_1_t6045_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43240_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6045_PropertyInfos[] =
{
	&IEnumerator_1_t6045____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43240_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorMarkerBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43240_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6045_il2cpp_TypeInfo/* declaring_type */
	, &IEditorMarkerBehaviour_t155_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43240_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6045_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43240_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6045_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6045_0_0_0;
extern Il2CppType IEnumerator_1_t6045_1_0_0;
struct IEnumerator_1_t6045;
extern Il2CppGenericClass IEnumerator_1_t6045_GenericClass;
TypeInfo IEnumerator_1_t6045_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6045_MethodInfos/* methods */
	, IEnumerator_1_t6045_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6045_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6045_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6045_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6045_0_0_0/* byval_arg */
	, &IEnumerator_1_t6045_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6045_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2880_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61MethodDeclarations.h"

extern TypeInfo IEditorMarkerBehaviour_t155_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14893_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorMarkerBehaviour_t155_m32960_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorMarkerBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorMarkerBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorMarkerBehaviour_t155_m32960(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2880____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2880, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2880____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2880, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2880_FieldInfos[] =
{
	&InternalEnumerator_1_t2880____array_0_FieldInfo,
	&InternalEnumerator_1_t2880____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2880____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2880_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2880____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2880_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14893_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2880_PropertyInfos[] =
{
	&InternalEnumerator_1_t2880____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2880____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2880_InternalEnumerator_1__ctor_m14889_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14889_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14889_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2880_InternalEnumerator_1__ctor_m14889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14889_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14891_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14891_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14891_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14892_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14892_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14892_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14893_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorMarkerBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14893_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* declaring_type */
	, &IEditorMarkerBehaviour_t155_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14893_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2880_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14889_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_MethodInfo,
	&InternalEnumerator_1_Dispose_m14891_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14892_MethodInfo,
	&InternalEnumerator_1_get_Current_m14893_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14892_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14891_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2880_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14890_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14892_MethodInfo,
	&InternalEnumerator_1_Dispose_m14891_MethodInfo,
	&InternalEnumerator_1_get_Current_m14893_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2880_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6045_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2880_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6045_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorMarkerBehaviour_t155_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2880_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14893_MethodInfo/* Method Usage */,
	&IEditorMarkerBehaviour_t155_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorMarkerBehaviour_t155_m32960_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2880_0_0_0;
extern Il2CppType InternalEnumerator_1_t2880_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2880_GenericClass;
TypeInfo InternalEnumerator_1_t2880_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2880_MethodInfos/* methods */
	, InternalEnumerator_1_t2880_PropertyInfos/* properties */
	, InternalEnumerator_1_t2880_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2880_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2880_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2880_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2880_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2880_1_0_0/* this_arg */
	, InternalEnumerator_1_t2880_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2880_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2880_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2880)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7695_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>
extern MethodInfo IList_1_get_Item_m43241_MethodInfo;
extern MethodInfo IList_1_set_Item_m43242_MethodInfo;
static PropertyInfo IList_1_t7695____Item_PropertyInfo = 
{
	&IList_1_t7695_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43241_MethodInfo/* get */
	, &IList_1_set_Item_m43242_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7695_PropertyInfos[] =
{
	&IList_1_t7695____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
static ParameterInfo IList_1_t7695_IList_1_IndexOf_m43243_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviour_t155_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43243_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43243_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7695_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7695_IList_1_IndexOf_m43243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43243_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
static ParameterInfo IList_1_t7695_IList_1_Insert_m43244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviour_t155_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43244_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43244_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7695_IList_1_Insert_m43244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43244_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7695_IList_1_RemoveAt_m43245_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43245_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43245_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7695_IList_1_RemoveAt_m43245_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43245_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7695_IList_1_get_Item_m43241_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43241_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43241_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7695_il2cpp_TypeInfo/* declaring_type */
	, &IEditorMarkerBehaviour_t155_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7695_IList_1_get_Item_m43241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43241_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorMarkerBehaviour_t155_0_0_0;
static ParameterInfo IList_1_t7695_IList_1_set_Item_m43242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorMarkerBehaviour_t155_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43242_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMarkerBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43242_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7695_IList_1_set_Item_m43242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43242_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7695_MethodInfos[] =
{
	&IList_1_IndexOf_m43243_MethodInfo,
	&IList_1_Insert_m43244_MethodInfo,
	&IList_1_RemoveAt_m43245_MethodInfo,
	&IList_1_get_Item_m43241_MethodInfo,
	&IList_1_set_Item_m43242_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7695_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7694_il2cpp_TypeInfo,
	&IEnumerable_1_t7696_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7695_0_0_0;
extern Il2CppType IList_1_t7695_1_0_0;
struct IList_1_t7695;
extern Il2CppGenericClass IList_1_t7695_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7695_MethodInfos/* methods */
	, IList_1_t7695_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7695_il2cpp_TypeInfo/* element_class */
	, IList_1_t7695_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7695_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7695_0_0_0/* byval_arg */
	, &IList_1_t7695_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7695_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_16.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2881_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_16MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_12.h"
extern TypeInfo InvokableCall_1_t2882_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_12MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14896_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14898_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2881____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2881_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2881, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2881_FieldInfos[] =
{
	&CachedInvokableCall_1_t2881____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2881_CachedInvokableCall_1__ctor_m14894_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14894_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14894_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2881_CachedInvokableCall_1__ctor_m14894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14894_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2881_CachedInvokableCall_1_Invoke_m14895_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14895_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14895_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2881_CachedInvokableCall_1_Invoke_m14895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14895_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2881_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14894_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14895_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14895_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14899_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2881_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14895_MethodInfo,
	&InvokableCall_1_Find_m14899_MethodInfo,
};
extern Il2CppType UnityAction_1_t2883_0_0_0;
extern TypeInfo UnityAction_1_t2883_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMarkerBehaviour_t39_m32970_MethodInfo;
extern TypeInfo MarkerBehaviour_t39_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14901_MethodInfo;
extern TypeInfo MarkerBehaviour_t39_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2881_RGCTXData[8] = 
{
	&UnityAction_1_t2883_0_0_0/* Type Usage */,
	&UnityAction_1_t2883_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMarkerBehaviour_t39_m32970_MethodInfo/* Method Usage */,
	&MarkerBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14901_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14896_MethodInfo/* Method Usage */,
	&MarkerBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14898_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2881_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2881_1_0_0;
struct CachedInvokableCall_1_t2881;
extern Il2CppGenericClass CachedInvokableCall_1_t2881_GenericClass;
TypeInfo CachedInvokableCall_1_t2881_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2881_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2881_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2881_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2881_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2881_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2881_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2881_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2881_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2881_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2881)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_19.h"
extern TypeInfo UnityAction_1_t2883_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_19MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.MarkerBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.MarkerBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMarkerBehaviour_t39_m32970(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>
extern Il2CppType UnityAction_1_t2883_0_0_1;
FieldInfo InvokableCall_1_t2882____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2883_0_0_1/* type */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2882, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2882_FieldInfos[] =
{
	&InvokableCall_1_t2882____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2882_InvokableCall_1__ctor_m14896_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14896_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14896_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2882_InvokableCall_1__ctor_m14896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14896_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2883_0_0_0;
static ParameterInfo InvokableCall_1_t2882_InvokableCall_1__ctor_m14897_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2883_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14897_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14897_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2882_InvokableCall_1__ctor_m14897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14897_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2882_InvokableCall_1_Invoke_m14898_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14898_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14898_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2882_InvokableCall_1_Invoke_m14898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14898_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2882_InvokableCall_1_Find_m14899_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14899_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14899_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2882_InvokableCall_1_Find_m14899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14899_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2882_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14896_MethodInfo,
	&InvokableCall_1__ctor_m14897_MethodInfo,
	&InvokableCall_1_Invoke_m14898_MethodInfo,
	&InvokableCall_1_Find_m14899_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2882_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14898_MethodInfo,
	&InvokableCall_1_Find_m14899_MethodInfo,
};
extern TypeInfo UnityAction_1_t2883_il2cpp_TypeInfo;
extern TypeInfo MarkerBehaviour_t39_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2882_RGCTXData[5] = 
{
	&UnityAction_1_t2883_0_0_0/* Type Usage */,
	&UnityAction_1_t2883_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMarkerBehaviour_t39_m32970_MethodInfo/* Method Usage */,
	&MarkerBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14901_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2882_0_0_0;
extern Il2CppType InvokableCall_1_t2882_1_0_0;
struct InvokableCall_1_t2882;
extern Il2CppGenericClass InvokableCall_1_t2882_GenericClass;
TypeInfo InvokableCall_1_t2882_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2882_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2882_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2882_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2882_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2882_0_0_0/* byval_arg */
	, &InvokableCall_1_t2882_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2882_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2882_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2882)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2883_UnityAction_1__ctor_m14900_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14900_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14900_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2883_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2883_UnityAction_1__ctor_m14900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14900_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
static ParameterInfo UnityAction_1_t2883_UnityAction_1_Invoke_m14901_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14901_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14901_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2883_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2883_UnityAction_1_Invoke_m14901_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14901_GenericMethod/* genericMethod */

};
extern Il2CppType MarkerBehaviour_t39_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2883_UnityAction_1_BeginInvoke_m14902_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MarkerBehaviour_t39_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14902_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14902_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2883_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2883_UnityAction_1_BeginInvoke_m14902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14902_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2883_UnityAction_1_EndInvoke_m14903_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14903_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14903_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2883_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2883_UnityAction_1_EndInvoke_m14903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14903_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2883_MethodInfos[] =
{
	&UnityAction_1__ctor_m14900_MethodInfo,
	&UnityAction_1_Invoke_m14901_MethodInfo,
	&UnityAction_1_BeginInvoke_m14902_MethodInfo,
	&UnityAction_1_EndInvoke_m14903_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14902_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14903_MethodInfo;
static MethodInfo* UnityAction_1_t2883_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14901_MethodInfo,
	&UnityAction_1_BeginInvoke_m14902_MethodInfo,
	&UnityAction_1_EndInvoke_m14903_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2883_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2883_1_0_0;
struct UnityAction_1_t2883;
extern Il2CppGenericClass UnityAction_1_t2883_GenericClass;
TypeInfo UnityAction_1_t2883_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2883_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2883_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2883_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2883_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2883_0_0_0/* byval_arg */
	, &UnityAction_1_t2883_1_0_0/* this_arg */
	, UnityAction_1_t2883_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2883_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2883)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6047_il2cpp_TypeInfo;

// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.MaskOutBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.MaskOutBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43246_MethodInfo;
static PropertyInfo IEnumerator_1_t6047____Current_PropertyInfo = 
{
	&IEnumerator_1_t6047_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6047_PropertyInfos[] =
{
	&IEnumerator_1_t6047____Current_PropertyInfo,
	NULL
};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43246_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.MaskOutBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43246_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6047_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutBehaviour_t40_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43246_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6047_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43246_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6047_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6047_0_0_0;
extern Il2CppType IEnumerator_1_t6047_1_0_0;
struct IEnumerator_1_t6047;
extern Il2CppGenericClass IEnumerator_1_t6047_GenericClass;
TypeInfo IEnumerator_1_t6047_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6047_MethodInfos/* methods */
	, IEnumerator_1_t6047_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6047_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6047_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6047_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6047_0_0_0/* byval_arg */
	, &IEnumerator_1_t6047_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6047_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_62.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2884_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_62MethodDeclarations.h"

extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14908_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMaskOutBehaviour_t40_m32972_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.MaskOutBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.MaskOutBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMaskOutBehaviour_t40_m32972(__this, p0, method) (MaskOutBehaviour_t40 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2884____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2884, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2884____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2884, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2884_FieldInfos[] =
{
	&InternalEnumerator_1_t2884____array_0_FieldInfo,
	&InternalEnumerator_1_t2884____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2884____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2884_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2884____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2884_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2884_PropertyInfos[] =
{
	&InternalEnumerator_1_t2884____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2884____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2884_InternalEnumerator_1__ctor_m14904_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14904_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14904_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2884_InternalEnumerator_1__ctor_m14904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14904_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14906_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14906_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14906_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14907_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14907_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14907_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14908_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.MaskOutBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14908_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutBehaviour_t40_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14908_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2884_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14904_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_MethodInfo,
	&InternalEnumerator_1_Dispose_m14906_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14907_MethodInfo,
	&InternalEnumerator_1_get_Current_m14908_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14907_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14906_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2884_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14905_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14907_MethodInfo,
	&InternalEnumerator_1_Dispose_m14906_MethodInfo,
	&InternalEnumerator_1_get_Current_m14908_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2884_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6047_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2884_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6047_il2cpp_TypeInfo, 7},
};
extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2884_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14908_MethodInfo/* Method Usage */,
	&MaskOutBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMaskOutBehaviour_t40_m32972_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2884_0_0_0;
extern Il2CppType InternalEnumerator_1_t2884_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2884_GenericClass;
TypeInfo InternalEnumerator_1_t2884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2884_MethodInfos/* methods */
	, InternalEnumerator_1_t2884_PropertyInfos/* properties */
	, InternalEnumerator_1_t2884_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2884_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2884_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2884_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2884_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2884_1_0_0/* this_arg */
	, InternalEnumerator_1_t2884_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2884_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2884_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2884)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
