﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t833;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t639;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton>
struct IEnumerable_1_t640;
// System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButton>
struct IEnumerator_1_t932;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.ICollection`1<Vuforia.VirtualButton>
struct ICollection_1_t3926;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButton>
struct ReadOnlyCollection_1_t3927;
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3925;
// System.Predicate`1<Vuforia.VirtualButton>
struct Predicate_1_t3928;
// System.Comparison`1<Vuforia.VirtualButton>
struct Comparison_1_t3929;
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_45.h"

// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m4729(__this, method) (void)List_1__ctor_m14461_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21608(__this, ___collection, method) (void)List_1__ctor_m14463_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Int32)
#define List_1__ctor_m21609(__this, ___capacity, method) (void)List_1__ctor_m14465_gshared((List_1_t149 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.cctor()
#define List_1__cctor_m21610(__this/* static, unused */, method) (void)List_1__cctor_m14467_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21611(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14469_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21612(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14471_gshared((List_1_t149 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21613(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14473_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21614(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14475_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21615(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14477_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21616(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14479_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21617(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14481_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21618(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14483_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21619(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14485_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21620(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14487_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21621(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14489_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21622(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14491_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21623(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14493_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21624(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14495_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21625(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14497_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Add(T)
#define List_1_Add_m21626(__this, ___item, method) (void)List_1_Add_m14499_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21627(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14501_gshared((List_1_t149 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21628(__this, ___collection, method) (void)List_1_AddCollection_m14503_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21629(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14505_gshared((List_1_t149 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21630(__this, ___collection, method) (void)List_1_AddRange_m14506_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::AsReadOnly()
#define List_1_AsReadOnly_m21631(__this, method) (ReadOnlyCollection_1_t3927 *)List_1_AsReadOnly_m14508_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Clear()
#define List_1_Clear_m21632(__this, method) (void)List_1_Clear_m14510_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Contains(T)
#define List_1_Contains_m21633(__this, ___item, method) (bool)List_1_Contains_m14512_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21634(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14514_gshared((List_1_t149 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::Find(System.Predicate`1<T>)
#define List_1_Find_m21635(__this, ___match, method) (VirtualButton_t639 *)List_1_Find_m14516_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21636(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14518_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2832 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21637(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14520_gshared((List_1_t149 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2832 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetEnumerator()
 Enumerator_t3930  List_1_GetEnumerator_m21638 (List_1_t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::IndexOf(T)
#define List_1_IndexOf_m21639(__this, ___item, method) (int32_t)List_1_IndexOf_m14522_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21640(__this, ___start, ___delta, method) (void)List_1_Shift_m14524_gshared((List_1_t149 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21641(__this, ___index, method) (void)List_1_CheckIndex_m14526_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Insert(System.Int32,T)
#define List_1_Insert_m21642(__this, ___index, ___item, method) (void)List_1_Insert_m14528_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21643(__this, ___collection, method) (void)List_1_CheckCollection_m14530_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Remove(T)
#define List_1_Remove_m21644(__this, ___item, method) (bool)List_1_Remove_m14532_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21645(__this, ___match, method) (int32_t)List_1_RemoveAll_m14534_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21646(__this, ___index, method) (void)List_1_RemoveAt_m14536_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Reverse()
#define List_1_Reverse_m21647(__this, method) (void)List_1_Reverse_m14538_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort()
#define List_1_Sort_m21648(__this, method) (void)List_1_Sort_m14540_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21649(__this, ___comparison, method) (void)List_1_Sort_m14542_gshared((List_1_t149 *)__this, (Comparison_1_t2833 *)___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::ToArray()
#define List_1_ToArray_m21650(__this, method) (VirtualButtonU5BU5D_t3925*)List_1_ToArray_m14544_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::TrimExcess()
#define List_1_TrimExcess_m21651(__this, method) (void)List_1_TrimExcess_m14546_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Capacity()
#define List_1_get_Capacity_m21652(__this, method) (int32_t)List_1_get_Capacity_m14548_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21653(__this, ___value, method) (void)List_1_set_Capacity_m14550_gshared((List_1_t149 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Count()
#define List_1_get_Count_m21654(__this, method) (int32_t)List_1_get_Count_m14552_gshared((List_1_t149 *)__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Item(System.Int32)
#define List_1_get_Item_m21655(__this, ___index, method) (VirtualButton_t639 *)List_1_get_Item_m14554_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Item(System.Int32,T)
#define List_1_set_Item_m21656(__this, ___index, ___value, method) (void)List_1_set_Item_m14556_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
