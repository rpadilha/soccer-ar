﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t6;
// Vuforia.CylinderTarget
struct CylinderTarget_t615;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t597;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::get_CylinderTarget()
 Object_t * CylinderTargetAbstractBehaviour_get_CylinderTarget_m2939 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_SideLength()
 float CylinderTargetAbstractBehaviour_get_SideLength_m2940 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_TopDiameter()
 float CylinderTargetAbstractBehaviour_get_TopDiameter_m2941 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_BottomDiameter()
 float CylinderTargetAbstractBehaviour_get_BottomDiameter_m2942 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetSideLength(System.Single)
 bool CylinderTargetAbstractBehaviour_SetSideLength_m2943 (CylinderTargetAbstractBehaviour_t6 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetTopDiameter(System.Single)
 bool CylinderTargetAbstractBehaviour_SetTopDiameter_m2944 (CylinderTargetAbstractBehaviour_t6 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetBottomDiameter(System.Single)
 bool CylinderTargetAbstractBehaviour_SetBottomDiameter_m2945 (CylinderTargetAbstractBehaviour_t6 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::OnFrameIndexUpdate(System.Int32)
 void CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m221 (CylinderTargetAbstractBehaviour_t6 * __this, int32_t ___newFrameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::CorrectScaleImpl()
 bool CylinderTargetAbstractBehaviour_CorrectScaleImpl_m223 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::InternalUnregisterTrackable()
 void CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m222 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m249 (CylinderTargetAbstractBehaviour_t6 * __this, Vector3_t73 * ___boundsMin, Vector3_t73 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m250 (CylinderTargetAbstractBehaviour_t6 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::GetScale()
 float CylinderTargetAbstractBehaviour_GetScale_m2946 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetScale(System.Single)
 bool CylinderTargetAbstractBehaviour_SetScale_m2947 (CylinderTargetAbstractBehaviour_t6 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ApplyScale(System.Single)
 void CylinderTargetAbstractBehaviour_ApplyScale_m2948 (CylinderTargetAbstractBehaviour_t6 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.InitializeCylinderTarget(Vuforia.CylinderTarget)
 void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m251 (CylinderTargetAbstractBehaviour_t6 * __this, Object_t * ___cylinderTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.SetAspectRatio(System.Single,System.Single)
 void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m252 (CylinderTargetAbstractBehaviour_t6 * __this, float ___topRatio, float ___bottomRatio, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::.ctor()
 void CylinderTargetAbstractBehaviour__ctor_m203 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m215 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m216 (CylinderTargetAbstractBehaviour_t6 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m217 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m218 (CylinderTargetAbstractBehaviour_t6 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
