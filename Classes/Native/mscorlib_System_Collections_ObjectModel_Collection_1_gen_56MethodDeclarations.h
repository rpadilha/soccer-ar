﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>
struct Collection_1_t5318;
// System.Object
struct Object_t;
// System.Security.Policy.StrongName
struct StrongName_t2186;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t5310;
// System.Collections.Generic.IEnumerator`1<System.Security.Policy.StrongName>
struct IEnumerator_1_t5312;
// System.Collections.Generic.IList`1<System.Security.Policy.StrongName>
struct IList_1_t2182;

// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m32007(__this, method) (void)Collection_1__ctor_m14594_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m32008(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14595_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m32009(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14596_gshared((Collection_1_t2839 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m32010(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14597_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m32011(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14598_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m32012(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14599_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m32013(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14600_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m32014(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14601_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m32015(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14602_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m32016(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14603_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m32017(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14604_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m32018(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14605_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m32019(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14606_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m32020(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14607_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m32021(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14608_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::Add(T)
#define Collection_1_Add_m32022(__this, ___item, method) (void)Collection_1_Add_m14609_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::Clear()
#define Collection_1_Clear_m32023(__this, method) (void)Collection_1_Clear_m14610_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::ClearItems()
#define Collection_1_ClearItems_m32024(__this, method) (void)Collection_1_ClearItems_m14611_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::Contains(T)
#define Collection_1_Contains_m32025(__this, ___item, method) (bool)Collection_1_Contains_m14612_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m32026(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14613_gshared((Collection_1_t2839 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::GetEnumerator()
#define Collection_1_GetEnumerator_m32027(__this, method) (Object_t*)Collection_1_GetEnumerator_m14614_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::IndexOf(T)
#define Collection_1_IndexOf_m32028(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14615_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::Insert(System.Int32,T)
#define Collection_1_Insert_m32029(__this, ___index, ___item, method) (void)Collection_1_Insert_m14616_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m32030(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14617_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::Remove(T)
#define Collection_1_Remove_m32031(__this, ___item, method) (bool)Collection_1_Remove_m14618_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m32032(__this, ___index, method) (void)Collection_1_RemoveAt_m14619_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m32033(__this, ___index, method) (void)Collection_1_RemoveItem_m14620_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::get_Count()
#define Collection_1_get_Count_m32034(__this, method) (int32_t)Collection_1_get_Count_m14621_gshared((Collection_1_t2839 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::get_Item(System.Int32)
#define Collection_1_get_Item_m32035(__this, ___index, method) (StrongName_t2186 *)Collection_1_get_Item_m14622_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m32036(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14623_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m32037(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14624_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m32038(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14625_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m32039(__this/* static, unused */, ___item, method) (StrongName_t2186 *)Collection_1_ConvertItem_m14626_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m32040(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14627_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m32041(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14628_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Security.Policy.StrongName>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m32042(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14629_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
