﻿#pragma once
#include <stdint.h>
// Vuforia.DataSetImpl
struct DataSetImpl_t610;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.DataSetImpl>
struct Predicate_1_t4021  : public MulticastDelegate_t373
{
};
