﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>
struct ReadOnlyCollection_1_t3991;
// Vuforia.Trackable
struct Trackable_t594;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.Trackable>
struct IList_1_t3994;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.Trackable[]
struct TrackableU5BU5D_t3973;
// System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>
struct IEnumerator_1_t846;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m22228(__this, ___list, method) (void)ReadOnlyCollection_1__ctor_m14564_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t*)___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22229(__this, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14565_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22230(__this, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14566_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22231(__this, ___index, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14567_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22232(__this, ___item, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14568_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22233(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14569_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22234(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14570_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22235(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14571_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22236(__this, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14572_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22237(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14573_gshared((ReadOnlyCollection_1_t2836 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22238(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14574_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m22239(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_Add_m14575_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m22240(__this, method) (void)ReadOnlyCollection_1_System_Collections_IList_Clear_m14576_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m22241(__this, ___value, method) (bool)ReadOnlyCollection_1_System_Collections_IList_Contains_m14577_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22242(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14578_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m22243(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Insert_m14579_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m22244(__this, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Remove_m14580_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22245(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14581_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22246(__this, method) (bool)ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14582_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22247(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14583_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22248(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14584_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22249(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14585_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m22250(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IList_get_Item_m14586_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m22251(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_set_Item_m14587_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::Contains(T)
#define ReadOnlyCollection_1_Contains_m22252(__this, ___value, method) (bool)ReadOnlyCollection_1_Contains_m14588_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m22253(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_CopyTo_m14589_gshared((ReadOnlyCollection_1_t2836 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m22254(__this, method) (Object_t*)ReadOnlyCollection_1_GetEnumerator_m14590_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m22255(__this, ___value, method) (int32_t)ReadOnlyCollection_1_IndexOf_m14591_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::get_Count()
#define ReadOnlyCollection_1_get_Count_m22256(__this, method) (int32_t)ReadOnlyCollection_1_get_Count_m14592_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m22257(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_get_Item_m14593_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
