﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
struct DictionaryNodeEnumerator_t1354;
// System.Object
struct Object_t;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t1353;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1349;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::.ctor(System.Collections.Specialized.ListDictionary)
 void DictionaryNodeEnumerator__ctor_m6914 (DictionaryNodeEnumerator_t1354 * __this, ListDictionary_t1349 * ___dict, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::FailFast()
 void DictionaryNodeEnumerator_FailFast_m6915 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::MoveNext()
 bool DictionaryNodeEnumerator_MoveNext_m6916 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::Reset()
 void DictionaryNodeEnumerator_Reset_m6917 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Current()
 Object_t * DictionaryNodeEnumerator_get_Current_m6918 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_DictionaryNode()
 DictionaryNode_t1353 * DictionaryNodeEnumerator_get_DictionaryNode_m6919 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Entry()
 DictionaryEntry_t1355  DictionaryNodeEnumerator_get_Entry_m6920 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Key()
 Object_t * DictionaryNodeEnumerator_get_Key_m6921 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Value()
 Object_t * DictionaryNodeEnumerator_get_Value_m6922 (DictionaryNodeEnumerator_t1354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
