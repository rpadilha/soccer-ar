﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>
struct InternalEnumerator_1_t3310;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m17333 (InternalEnumerator_1_t3310 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17334 (InternalEnumerator_1_t3310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>::Dispose()
 void InternalEnumerator_1_Dispose_m17335 (InternalEnumerator_1_t3310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m17336 (InternalEnumerator_1_t3310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.MoveDirection>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m17337 (InternalEnumerator_1_t3310 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
