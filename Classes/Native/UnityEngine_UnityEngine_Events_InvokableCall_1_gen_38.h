﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>
struct UnityAction_1_t3025;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>
struct InvokableCall_1_t3024  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::Delegate
	UnityAction_1_t3025 * ___Delegate_0;
};
