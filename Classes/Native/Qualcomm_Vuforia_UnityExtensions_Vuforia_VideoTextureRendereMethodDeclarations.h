﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t60;

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
 void VideoTextureRendererAbstractBehaviour_Awake_m4378 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
 void VideoTextureRendererAbstractBehaviour_Start_m4379 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
 void VideoTextureRendererAbstractBehaviour_Update_m4380 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
 void VideoTextureRendererAbstractBehaviour_OnDestroy_m4381 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
 void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
 void VideoTextureRendererAbstractBehaviour__ctor_m492 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
