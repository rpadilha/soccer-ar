﻿#pragma once
#include <stdint.h>
// Vuforia.Image
struct Image_t604;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct KeyValuePair_2_t3946 
{
	// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::value
	Image_t604 * ___value_1;
};
