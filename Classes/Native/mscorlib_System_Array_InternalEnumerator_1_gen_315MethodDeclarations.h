﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyeID>
struct InternalEnumerator_1_t3883;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyeID>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21271 (InternalEnumerator_1_t3883 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyeID>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21272 (InternalEnumerator_1_t3883 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyeID>::Dispose()
 void InternalEnumerator_1_Dispose_m21273 (InternalEnumerator_1_t3883 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyeID>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21274 (InternalEnumerator_1_t3883 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyeID>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21275 (InternalEnumerator_1_t3883 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
