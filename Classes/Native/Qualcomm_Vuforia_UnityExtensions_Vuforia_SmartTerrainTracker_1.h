﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t715;
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"
// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t721  : public SmartTerrainTracker_t720
{
	// System.Single Vuforia.SmartTerrainTrackerImpl::mScaleToMillimeter
	float ___mScaleToMillimeter_1;
	// Vuforia.SmartTerrainBuilderImpl Vuforia.SmartTerrainTrackerImpl::mSmartTerrainBuilder
	SmartTerrainBuilderImpl_t715 * ___mSmartTerrainBuilder_2;
};
