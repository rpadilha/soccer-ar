﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIStyleState
struct GUIStyleState_t1017;
// UnityEngine.GUIStyle
struct GUIStyle_t999;
// UnityEngine.Texture2D
struct Texture2D_t196;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.GUIStyleState::.ctor()
 void GUIStyleState__ctor_m5871 (GUIStyleState_t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
 void GUIStyleState__ctor_m5872 (GUIStyleState_t1017 * __this, GUIStyle_t999 * ___sourceStyle, IntPtr_t121 ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::Finalize()
 void GUIStyleState_Finalize_m5873 (GUIStyleState_t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::Init()
 void GUIStyleState_Init_m5874 (GUIStyleState_t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::Cleanup()
 void GUIStyleState_Cleanup_m5875 (GUIStyleState_t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
 Texture2D_t196 * GUIStyleState_GetBackgroundInternal_m5876 (GUIStyleState_t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
 void GUIStyleState_INTERNAL_set_textColor_m5877 (GUIStyleState_t1017 * __this, Color_t66 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
 void GUIStyleState_set_textColor_m5878 (GUIStyleState_t1017 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
