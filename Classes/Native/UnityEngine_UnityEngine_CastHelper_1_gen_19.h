﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t237;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.EventSystems.EventSystem>
struct CastHelper_1_t3319 
{
	// T UnityEngine.CastHelper`1<UnityEngine.EventSystems.EventSystem>::t
	EventSystem_t237 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.EventSystems.EventSystem>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
