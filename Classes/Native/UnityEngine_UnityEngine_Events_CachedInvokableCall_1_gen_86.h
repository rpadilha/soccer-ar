﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_85.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>
struct CachedInvokableCall_1_t3621  : public InvokableCall_1_t3622
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
