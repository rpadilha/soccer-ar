﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Joystick_Script>
struct EqualityComparer_1_t3078;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Joystick_Script>
struct EqualityComparer_1_t3078  : public Object_t
{
};
struct EqualityComparer_1_t3078_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Joystick_Script>::_default
	EqualityComparer_1_t3078 * ____default_0;
};
