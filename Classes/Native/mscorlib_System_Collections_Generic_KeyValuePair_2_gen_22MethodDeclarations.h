﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyValuePair_2_t4443;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m26679 (KeyValuePair_2_t4443 * __this, String_t* ___key, ProfileData_t791  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
 String_t* KeyValuePair_2_get_Key_m26680 (KeyValuePair_2_t4443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m26681 (KeyValuePair_2_t4443 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
 ProfileData_t791  KeyValuePair_2_get_Value_m26682 (KeyValuePair_2_t4443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m26683 (KeyValuePair_2_t4443 * __this, ProfileData_t791  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
 String_t* KeyValuePair_2_ToString_m26684 (KeyValuePair_2_t4443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
