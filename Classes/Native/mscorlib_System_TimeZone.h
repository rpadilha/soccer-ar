﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t2303;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct TimeZone_t2303  : public Object_t
{
};
struct TimeZone_t2303_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t2303 * ___currentTimeZone_0;
};
