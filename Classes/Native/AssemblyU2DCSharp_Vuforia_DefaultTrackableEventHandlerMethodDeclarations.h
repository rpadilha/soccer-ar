﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t18;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"

// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
 void DefaultTrackableEventHandler__ctor_m18 (DefaultTrackableEventHandler_t18 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
 void DefaultTrackableEventHandler_Start_m19 (DefaultTrackableEventHandler_t18 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
 void DefaultTrackableEventHandler_OnTrackableStateChanged_m20 (DefaultTrackableEventHandler_t18 * __this, int32_t ___previousStatus, int32_t ___newStatus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
 void DefaultTrackableEventHandler_OnTrackingFound_m21 (DefaultTrackableEventHandler_t18 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
 void DefaultTrackableEventHandler_OnTrackingLost_m22 (DefaultTrackableEventHandler_t18 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
