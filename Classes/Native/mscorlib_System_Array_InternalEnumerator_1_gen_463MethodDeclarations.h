﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.QueueMode>
struct InternalEnumerator_1_t4864;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.QueueMode
#include "UnityEngine_UnityEngine_QueueMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29351 (InternalEnumerator_1_t4864 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352 (InternalEnumerator_1_t4864 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::Dispose()
 void InternalEnumerator_1_Dispose_m29353 (InternalEnumerator_1_t4864 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29354 (InternalEnumerator_1_t4864 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29355 (InternalEnumerator_1_t4864 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
