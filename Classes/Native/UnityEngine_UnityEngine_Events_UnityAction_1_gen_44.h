﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// GoalKeeperJump
struct GoalKeeperJump_t78;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<GoalKeeperJump>
struct UnityAction_1_t3019  : public MulticastDelegate_t373
{
};
