﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
struct InternalEnumerator_1_t5046;
// System.Object
struct Object_t;
// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_t1182;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m30557(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m30559(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m30560(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m30561(__this, method) (EditorBrowsableAttribute_t1182 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
