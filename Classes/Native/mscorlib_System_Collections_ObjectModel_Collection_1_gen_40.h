﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Surface>
struct IList_1_t4324;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.Surface>
struct Collection_1_t4325  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Surface>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Surface>::syncRoot
	Object_t * ___syncRoot_1;
};
