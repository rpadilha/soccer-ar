﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>
struct InternalEnumerator_1_t5159;
// System.Object
struct Object_t;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_t205;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31179(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31180(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m31181(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31182(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.RuntimeCompatibilityAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m31183(__this, method) (RuntimeCompatibilityAttribute_t205 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
