﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t451;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t452;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t454;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2727(__this, ___actionOnGet, ___actionOnRelease, method) (void)ObjectPool_1__ctor_m16391_gshared((ObjectPool_1_t3208 *)__this, (UnityAction_1_t2763 *)___actionOnGet, (UnityAction_1_t2763 *)___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
#define ObjectPool_1_get_countAll_m20736(__this, method) (int32_t)ObjectPool_1_get_countAll_m16393_gshared((ObjectPool_1_t3208 *)__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m20737(__this, ___value, method) (void)ObjectPool_1_set_countAll_m16395_gshared((ObjectPool_1_t3208 *)__this, (int32_t)___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
#define ObjectPool_1_get_countActive_m20738(__this, method) (int32_t)ObjectPool_1_get_countActive_m16397_gshared((ObjectPool_1_t3208 *)__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m20739(__this, method) (int32_t)ObjectPool_1_get_countInactive_m16399_gshared((ObjectPool_1_t3208 *)__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
#define ObjectPool_1_Get_m2728(__this, method) (List_1_t454 *)ObjectPool_1_Get_m16401_gshared((ObjectPool_1_t3208 *)__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
#define ObjectPool_1_Release_m2729(__this, ___element, method) (void)ObjectPool_1_Release_m16403_gshared((ObjectPool_1_t3208 *)__this, (Object_t *)___element, method)
