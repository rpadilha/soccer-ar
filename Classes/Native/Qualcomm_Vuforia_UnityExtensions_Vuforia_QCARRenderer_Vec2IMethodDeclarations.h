﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRenderer/Vec2I
struct Vec2I_t675;

// System.Void Vuforia.QCARRenderer/Vec2I::.ctor(System.Int32,System.Int32)
 void Vec2I__ctor_m3174 (Vec2I_t675 * __this, int32_t ___v1, int32_t ___v2, MethodInfo* method) IL2CPP_METHOD_ATTR;
