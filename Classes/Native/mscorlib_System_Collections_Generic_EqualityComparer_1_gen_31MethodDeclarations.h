﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct EqualityComparer_1_t3959;
// System.Object
struct Object_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
 void EqualityComparer_1__ctor_m21893 (EqualityComparer_1_t3959 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
 void EqualityComparer_1__cctor_m21894 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
 int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21895 (EqualityComparer_1_t3959 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
 bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21896 (EqualityComparer_1_t3959 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::get_Default()
 EqualityComparer_1_t3959 * EqualityComparer_1_get_Default_m21897 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
