﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
struct UnityAction_1_t2913;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
struct InvokableCall_1_t2912  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Delegate
	UnityAction_1_t2913 * ___Delegate_0;
};
