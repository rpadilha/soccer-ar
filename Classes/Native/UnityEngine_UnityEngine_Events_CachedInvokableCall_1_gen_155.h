﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_157.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
struct CachedInvokableCall_1_t4776  : public InvokableCall_1_t4777
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
