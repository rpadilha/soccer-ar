﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$1024
struct $ArrayType$1024_t2331;
struct $ArrayType$1024_t2331_marshaled;

void $ArrayType$1024_t2331_marshal(const $ArrayType$1024_t2331& unmarshaled, $ArrayType$1024_t2331_marshaled& marshaled);
void $ArrayType$1024_t2331_marshal_back(const $ArrayType$1024_t2331_marshaled& marshaled, $ArrayType$1024_t2331& unmarshaled);
void $ArrayType$1024_t2331_marshal_cleanup($ArrayType$1024_t2331_marshaled& marshaled);
