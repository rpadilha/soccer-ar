﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ParamArrayAttribute
struct ParamArrayAttribute_t538;

// System.Void System.ParamArrayAttribute::.ctor()
 void ParamArrayAttribute__ctor_m2528 (ParamArrayAttribute_t538 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
