﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinderImpl
struct TargetFinderImpl_t783;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t777;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// UnityEngine.GameObject
struct GameObject_t29;
// System.Collections.Generic.IEnumerable`1<Vuforia.ImageTarget>
struct IEnumerable_1_t778;
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void Vuforia.TargetFinderImpl::.ctor()
 void TargetFinderImpl__ctor_m4182 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::Finalize()
 void TargetFinderImpl_Finalize_m4183 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::StartInit(System.String,System.String)
 bool TargetFinderImpl_StartInit_m4184 (TargetFinderImpl_t783 * __this, String_t* ___userAuth, String_t* ___secretAuth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder/InitState Vuforia.TargetFinderImpl::GetInitState()
 int32_t TargetFinderImpl_GetInitState_m4185 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::Deinit()
 bool TargetFinderImpl_Deinit_m4186 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::StartRecognition()
 bool TargetFinderImpl_StartRecognition_m4187 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::Stop()
 bool TargetFinderImpl_Stop_m4188 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::SetUIScanlineColor(UnityEngine.Color)
 void TargetFinderImpl_SetUIScanlineColor_m4189 (TargetFinderImpl_t783 * __this, Color_t66  ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::SetUIPointColor(UnityEngine.Color)
 void TargetFinderImpl_SetUIPointColor_m4190 (TargetFinderImpl_t783 * __this, Color_t66  ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::IsRequesting()
 bool TargetFinderImpl_IsRequesting_m4191 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder/UpdateState Vuforia.TargetFinderImpl::Update()
 int32_t TargetFinderImpl_Update_m4192 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinderImpl::GetResults()
 Object_t* TargetFinderImpl_GetResults_m4193 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.TargetFinderImpl::EnableTracking(Vuforia.TargetFinder/TargetSearchResult,System.String)
 ImageTargetAbstractBehaviour_t23 * TargetFinderImpl_EnableTracking_m4194 (TargetFinderImpl_t783 * __this, TargetSearchResult_t776  ___result, String_t* ___gameObjectName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.TargetFinderImpl::EnableTracking(Vuforia.TargetFinder/TargetSearchResult,UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t23 * TargetFinderImpl_EnableTracking_m4195 (TargetFinderImpl_t783 * __this, TargetSearchResult_t776  ___result, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::ClearTrackables(System.Boolean)
 void TargetFinderImpl_ClearTrackables_m4196 (TargetFinderImpl_t783 * __this, bool ___destroyGameObjects, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.ImageTarget> Vuforia.TargetFinderImpl::GetImageTargets()
 Object_t* TargetFinderImpl_GetImageTargets_m4197 (TargetFinderImpl_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
