﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t4565  : public Array_t
{
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>[]
// System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>[]
struct LinkU5BU5D_t4559  : public Array_t
{
};
// System.Runtime.CompilerServices.ExtensionAttribute[]
// System.Runtime.CompilerServices.ExtensionAttribute[]
struct ExtensionAttributeU5BU5D_t5882  : public Array_t
{
};
// System.MonoTODOAttribute[]
// System.MonoTODOAttribute[]
struct MonoTODOAttributeU5BU5D_t5883  : public Array_t
{
};
