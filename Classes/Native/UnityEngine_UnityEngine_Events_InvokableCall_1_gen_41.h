﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<ChooseTeam>
struct UnityAction_1_t3039;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<ChooseTeam>
struct InvokableCall_1_t3038  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<ChooseTeam>::Delegate
	UnityAction_1_t3039 * ___Delegate_0;
};
