﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>
struct Collection_1_t4402;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t4394;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t827;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t4401;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
 void Collection_1__ctor_m26284 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26285 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void Collection_1_System_Collections_ICollection_CopyTo_m26286 (Collection_1_t4402 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m26287 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
 int32_t Collection_1_System_Collections_IList_Add_m26288 (Collection_1_t4402 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
 bool Collection_1_System_Collections_IList_Contains_m26289 (Collection_1_t4402 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
 int32_t Collection_1_System_Collections_IList_IndexOf_m26290 (Collection_1_t4402 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
 void Collection_1_System_Collections_IList_Insert_m26291 (Collection_1_t4402 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
 void Collection_1_System_Collections_IList_Remove_m26292 (Collection_1_t4402 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
 bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m26293 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
 Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m26294 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
 bool Collection_1_System_Collections_IList_get_IsFixedSize_m26295 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
 bool Collection_1_System_Collections_IList_get_IsReadOnly_m26296 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
 Object_t * Collection_1_System_Collections_IList_get_Item_m26297 (Collection_1_t4402 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
 void Collection_1_System_Collections_IList_set_Item_m26298 (Collection_1_t4402 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
 void Collection_1_Add_m26299 (Collection_1_t4402 * __this, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
 void Collection_1_Clear_m26300 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ClearItems()
 void Collection_1_ClearItems_m26301 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
 bool Collection_1_Contains_m26302 (Collection_1_t4402 * __this, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
 void Collection_1_CopyTo_m26303 (Collection_1_t4402 * __this, TargetSearchResultU5BU5D_t4394* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
 Object_t* Collection_1_GetEnumerator_m26304 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
 int32_t Collection_1_IndexOf_m26305 (Collection_1_t4402 * __this, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
 void Collection_1_Insert_m26306 (Collection_1_t4402 * __this, int32_t ___index, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::InsertItem(System.Int32,T)
 void Collection_1_InsertItem_m26307 (Collection_1_t4402 * __this, int32_t ___index, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
 bool Collection_1_Remove_m26308 (Collection_1_t4402 * __this, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
 void Collection_1_RemoveAt_m26309 (Collection_1_t4402 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveItem(System.Int32)
 void Collection_1_RemoveItem_m26310 (Collection_1_t4402 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
 int32_t Collection_1_get_Count_m26311 (Collection_1_t4402 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
 TargetSearchResult_t776  Collection_1_get_Item_m26312 (Collection_1_t4402 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
 void Collection_1_set_Item_m26313 (Collection_1_t4402 * __this, int32_t ___index, TargetSearchResult_t776  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::SetItem(System.Int32,T)
 void Collection_1_SetItem_m26314 (Collection_1_t4402 * __this, int32_t ___index, TargetSearchResult_t776  ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsValidItem(System.Object)
 bool Collection_1_IsValidItem_m26315 (Object_t * __this/* static, unused */, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ConvertItem(System.Object)
 TargetSearchResult_t776  Collection_1_ConvertItem_m26316 (Object_t * __this/* static, unused */, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
 void Collection_1_CheckWritable_m26317 (Object_t * __this/* static, unused */, Object_t* ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
 bool Collection_1_IsSynchronized_m26318 (Object_t * __this/* static, unused */, Object_t* ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
 bool Collection_1_IsFixedSize_m26319 (Object_t * __this/* static, unused */, Object_t* ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
