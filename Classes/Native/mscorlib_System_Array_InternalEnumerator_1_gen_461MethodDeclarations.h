﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
struct InternalEnumerator_1_t4862;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29341 (InternalEnumerator_1_t4862 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342 (InternalEnumerator_1_t4862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
 void InternalEnumerator_1_Dispose_m29343 (InternalEnumerator_1_t4862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29344 (InternalEnumerator_1_t4862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
 Keyframe_t1066  InternalEnumerator_1_get_Current_m29345 (InternalEnumerator_1_t4862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
