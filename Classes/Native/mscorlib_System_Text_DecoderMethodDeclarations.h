﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.Decoder
struct Decoder_t1915;
// System.Text.DecoderFallback
struct DecoderFallback_t2196;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2195;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.Char[]
struct CharU5BU5D_t378;

// System.Void System.Text.Decoder::.ctor()
 void Decoder__ctor_m12401 (Decoder_t1915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::set_Fallback(System.Text.DecoderFallback)
 void Decoder_set_Fallback_m12402 (Decoder_t1915 * __this, DecoderFallback_t2196 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.Decoder::get_FallbackBuffer()
 DecoderFallbackBuffer_t2195 * Decoder_get_FallbackBuffer_m12403 (Decoder_t1915 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
