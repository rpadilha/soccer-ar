﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHAConstants
struct SHAConstants_t2174;

// System.Void System.Security.Cryptography.SHAConstants::.cctor()
 void SHAConstants__cctor_m12290 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
