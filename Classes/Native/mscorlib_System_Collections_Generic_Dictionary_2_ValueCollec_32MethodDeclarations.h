﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t3457;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3452;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_33.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ValueCollection__ctor_m18389_gshared (ValueCollection_t3457 * __this, Dictionary_2_t3452 * ___dictionary, MethodInfo* method);
#define ValueCollection__ctor_m18389(__this, ___dictionary, method) (void)ValueCollection__ctor_m18389_gshared((ValueCollection_t3457 *)__this, (Dictionary_2_t3452 *)___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
 void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18390_gshared (ValueCollection_t3457 * __this, Object_t * ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18390(__this, ___item, method) (void)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18390_gshared((ValueCollection_t3457 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
 void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18391_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18391(__this, method) (void)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18391_gshared((ValueCollection_t3457 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
 bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18392_gshared (ValueCollection_t3457 * __this, Object_t * ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18392(__this, ___item, method) (bool)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18392_gshared((ValueCollection_t3457 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
 bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18393_gshared (ValueCollection_t3457 * __this, Object_t * ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18393(__this, ___item, method) (bool)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18393_gshared((ValueCollection_t3457 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
 Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18394_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18394(__this, method) (Object_t*)ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18394_gshared((ValueCollection_t3457 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void ValueCollection_System_Collections_ICollection_CopyTo_m18395_gshared (ValueCollection_t3457 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m18395(__this, ___array, ___index, method) (void)ValueCollection_System_Collections_ICollection_CopyTo_m18395_gshared((ValueCollection_t3457 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18396_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18396(__this, method) (Object_t *)ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18396_gshared((ValueCollection_t3457 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
 bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18397_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18397(__this, method) (bool)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18397_gshared((ValueCollection_t3457 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18398_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18398(__this, method) (bool)ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18398_gshared((ValueCollection_t3457 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m18399_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m18399(__this, method) (Object_t *)ValueCollection_System_Collections_ICollection_get_SyncRoot_m18399_gshared((ValueCollection_t3457 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
 void ValueCollection_CopyTo_m18400_gshared (ValueCollection_t3457 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method);
#define ValueCollection_CopyTo_m18400(__this, ___array, ___index, method) (void)ValueCollection_CopyTo_m18400_gshared((ValueCollection_t3457 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::GetEnumerator()
 Enumerator_t3464  ValueCollection_GetEnumerator_m18401 (ValueCollection_t3457 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::get_Count()
 int32_t ValueCollection_get_Count_m18402_gshared (ValueCollection_t3457 * __this, MethodInfo* method);
#define ValueCollection_get_Count_m18402(__this, method) (int32_t)ValueCollection_get_Count_m18402_gshared((ValueCollection_t3457 *)__this, method)
