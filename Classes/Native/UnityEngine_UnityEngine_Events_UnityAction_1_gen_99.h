﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t3703  : public MulticastDelegate_t373
{
};
