﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACMD5
struct HMACMD5_t2149;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.HMACMD5::.ctor()
 void HMACMD5__ctor_m12091 (HMACMD5_t2149 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
 void HMACMD5__ctor_m12092 (HMACMD5_t2149 * __this, ByteU5BU5D_t653* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
