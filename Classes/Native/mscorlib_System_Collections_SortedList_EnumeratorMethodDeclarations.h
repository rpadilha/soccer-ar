﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.SortedList/Enumerator
struct Enumerator_t1891;
// System.Object
struct Object_t;
// System.Collections.SortedList
struct SortedList_t1555;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorMode.h"

// System.Void System.Collections.SortedList/Enumerator::.ctor(System.Collections.SortedList,System.Collections.SortedList/EnumeratorMode)
 void Enumerator__ctor_m10586 (Enumerator_t1891 * __this, SortedList_t1555 * ___host, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/Enumerator::.cctor()
 void Enumerator__cctor_m10587 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/Enumerator::Reset()
 void Enumerator_Reset_m10588 (Enumerator_t1891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m10589 (Enumerator_t1891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.SortedList/Enumerator::get_Entry()
 DictionaryEntry_t1355  Enumerator_get_Entry_m10590 (Enumerator_t1891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/Enumerator::get_Key()
 Object_t * Enumerator_get_Key_m10591 (Enumerator_t1891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/Enumerator::get_Value()
 Object_t * Enumerator_get_Value_m10592 (Enumerator_t1891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m10593 (Enumerator_t1891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
