﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AppDomain
struct AppDomain_t2233;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1556;
// System.Security.Policy.Evidence
struct Evidence_t1970;
// System.Runtime.Remoting.Contexts.Context
struct Context_t2043;

// System.String System.AppDomain::getFriendlyName()
 String_t* AppDomain_getFriendlyName_m12689 (AppDomain_t2233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::getCurDomain()
 AppDomain_t2233 * AppDomain_getCurDomain_m12690 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
 AppDomain_t2233 * AppDomain_get_CurrentDomain_m12691 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::LoadAssembly(System.String,System.Security.Policy.Evidence,System.Boolean)
 Assembly_t1556 * AppDomain_LoadAssembly_m12692 (AppDomain_t2233 * __this, String_t* ___assemblyRef, Evidence_t1970 * ___securityEvidence, bool ___refOnly, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String)
 Assembly_t1556 * AppDomain_Load_m12693 (AppDomain_t2233 * __this, String_t* ___assemblyString, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String,System.Security.Policy.Evidence,System.Boolean)
 Assembly_t1556 * AppDomain_Load_m12694 (AppDomain_t2233 * __this, String_t* ___assemblyString, Evidence_t1970 * ___assemblySecurity, bool ___refonly, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalGetContext()
 Context_t2043 * AppDomain_InternalGetContext_m12695 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalGetDefaultContext()
 Context_t2043 * AppDomain_InternalGetDefaultContext_m12696 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::InternalGetProcessGuid(System.String)
 String_t* AppDomain_InternalGetProcessGuid_m12697 (Object_t * __this/* static, unused */, String_t* ___newguid, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::GetProcessGuid()
 String_t* AppDomain_GetProcessGuid_m12698 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::ToString()
 String_t* AppDomain_ToString_m12699 (AppDomain_t2233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
