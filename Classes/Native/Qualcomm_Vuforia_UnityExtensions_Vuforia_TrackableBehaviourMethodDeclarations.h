﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// Vuforia.Trackable
struct Trackable_t594;
// System.String
struct String_t;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t134;
// UnityEngine.Renderer
struct Renderer_t129;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::get_CurrentStatus()
 int32_t TrackableBehaviour_get_CurrentStatus_m2799 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable()
 Object_t * TrackableBehaviour_get_Trackable_m207 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
 String_t* TrackableBehaviour_get_TrackableName_m204 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
 void TrackableBehaviour_RegisterTrackableEventHandler_m282 (TrackableBehaviour_t17 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::UnregisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
 bool TrackableBehaviour_UnregisterTrackableEventHandler_m2800 (TrackableBehaviour_t17 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status)
 void TrackableBehaviour_OnTrackerUpdate_m354 (TrackableBehaviour_t17 * __this, int32_t ___newStatus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::OnFrameIndexUpdate(System.Int32)
 void TrackableBehaviour_OnFrameIndexUpdate_m296 (TrackableBehaviour_t17 * __this, int32_t ___newFrameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::InternalUnregisterTrackable()
// System.Void Vuforia.TrackableBehaviour::Start()
 void TrackableBehaviour_Start_m2801 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::OnDisable()
 void TrackableBehaviour_OnDisable_m2802 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.CorrectScale()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m205 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::CorrectScaleImpl()
 bool TrackableBehaviour_CorrectScaleImpl_m372 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetNameForTrackable(System.String)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m206 (TrackableBehaviour_t17 * __this, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreviousScale()
 Vector3_t73  TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m208 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreviousScale(UnityEngine.Vector3)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m209 (TrackableBehaviour_t17 * __this, Vector3_t73  ___previousScale, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreserveChildSize()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m210 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreserveChildSize(System.Boolean)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m211 (TrackableBehaviour_t17 * __this, bool ___preserveChildSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_InitializedInEditor()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m212 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetInitializedInEditor(System.Boolean)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m213 (TrackableBehaviour_t17 * __this, bool ___initializedInEditor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.UnregisterTrackable()
 void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m214 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.GetRenderer()
 Renderer_t129 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m219 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::.ctor()
 void TrackableBehaviour__ctor_m2803 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2804 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2805 (TrackableBehaviour_t17 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2806 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2807 (TrackableBehaviour_t17 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
