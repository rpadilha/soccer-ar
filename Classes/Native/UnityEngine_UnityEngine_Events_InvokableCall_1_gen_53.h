﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Jump_Button>
struct UnityAction_1_t3112;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Jump_Button>
struct InvokableCall_1_t3111  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Jump_Button>::Delegate
	UnityAction_1_t3112 * ___Delegate_0;
};
