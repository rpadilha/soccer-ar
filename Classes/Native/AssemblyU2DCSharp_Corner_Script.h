﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;
// Sphere
struct Sphere_t71;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Corner_Script
struct Corner_Script_t76  : public MonoBehaviour_t10
{
	// UnityEngine.Transform Corner_Script::downPosition
	Transform_t74 * ___downPosition_2;
	// UnityEngine.Transform Corner_Script::upPosition
	Transform_t74 * ___upPosition_3;
	// UnityEngine.GameObject Corner_Script::area
	GameObject_t29 * ___area_4;
	// UnityEngine.Transform Corner_Script::point_goalkick
	Transform_t74 * ___point_goalkick_5;
	// UnityEngine.GameObject Corner_Script::goalKeeper
	GameObject_t29 * ___goalKeeper_6;
	// Sphere Corner_Script::sphere
	Sphere_t71 * ___sphere_7;
};
