﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct InternalEnumerator_1_t4400;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26243 (InternalEnumerator_1_t4400 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26244 (InternalEnumerator_1_t4400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
 void InternalEnumerator_1_Dispose_m26245 (InternalEnumerator_1_t4400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26246 (InternalEnumerator_1_t4400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
 TargetSearchResult_t776  InternalEnumerator_1_get_Current_m26247 (InternalEnumerator_1_t4400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
