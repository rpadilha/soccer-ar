﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>
struct InternalEnumerator_1_t3857;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21088 (InternalEnumerator_1_t3857 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21089 (InternalEnumerator_1_t3857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::Dispose()
 void InternalEnumerator_1_Dispose_m21090 (InternalEnumerator_1_t3857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21091 (InternalEnumerator_1_t3857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21092 (InternalEnumerator_1_t3857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
