﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t667;
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32  : public TrackableBehaviour_t17
{
	// System.Int32 Vuforia.MarkerAbstractBehaviour::mMarkerID
	int32_t ___mMarkerID_9;
	// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::mMarker
	Object_t * ___mMarker_10;
};
