﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Area_Script
struct Area_Script_t69;
// UnityEngine.Collider
struct Collider_t70;

// System.Void Area_Script::.ctor()
 void Area_Script__ctor_m106 (Area_Script_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area_Script::Start()
 void Area_Script_Start_m107 (Area_Script_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area_Script::Update()
 void Area_Script_Update_m108 (Area_Script_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area_Script::OnTriggerExit(UnityEngine.Collider)
 void Area_Script_OnTriggerExit_m109 (Area_Script_t69 * __this, Collider_t70 * ___coll, MethodInfo* method) IL2CPP_METHOD_ATTR;
