﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.DefaultCertificatePolicy
struct DefaultCertificatePolicy_t1371;
// System.Net.ServicePoint
struct ServicePoint_t1372;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1373;
// System.Net.WebRequest
struct WebRequest_t1374;

// System.Void System.Net.DefaultCertificatePolicy::.ctor()
 void DefaultCertificatePolicy__ctor_m6983 (DefaultCertificatePolicy_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.DefaultCertificatePolicy::CheckValidationResult(System.Net.ServicePoint,System.Security.Cryptography.X509Certificates.X509Certificate,System.Net.WebRequest,System.Int32)
 bool DefaultCertificatePolicy_CheckValidationResult_m6984 (DefaultCertificatePolicy_t1371 * __this, ServicePoint_t1372 * ___point, X509Certificate_t1373 * ___certificate, WebRequest_t1374 * ___request, int32_t ___certificateProblem, MethodInfo* method) IL2CPP_METHOD_ATTR;
