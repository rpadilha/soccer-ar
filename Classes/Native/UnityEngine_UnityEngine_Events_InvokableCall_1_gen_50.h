﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Shoot_Script>
struct UnityAction_1_t3101;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Shoot_Script>
struct InvokableCall_1_t3100  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Shoot_Script>::Delegate
	UnityAction_1_t3101 * ___Delegate_0;
};
