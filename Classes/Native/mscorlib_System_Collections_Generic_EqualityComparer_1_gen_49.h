﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>
struct EqualityComparer_1_t4310;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>
struct EqualityComparer_1_t4310  : public Object_t
{
};
struct EqualityComparer_1_t4310_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.PropAbstractBehaviour>::_default
	EqualityComparer_1_t4310 * ____default_0;
};
