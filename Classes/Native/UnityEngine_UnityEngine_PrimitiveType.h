﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
// UnityEngine.PrimitiveType
struct PrimitiveType_t956 
{
	// System.Int32 UnityEngine.PrimitiveType::value__
	int32_t ___value___1;
};
