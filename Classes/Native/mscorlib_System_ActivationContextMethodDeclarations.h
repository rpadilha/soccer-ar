﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ActivationContext
struct ActivationContext_t2231;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ActivationContext::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m12678 (ActivationContext_t2231 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Finalize()
 void ActivationContext_Finalize_m12679 (ActivationContext_t2231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose()
 void ActivationContext_Dispose_m12680 (ActivationContext_t2231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose(System.Boolean)
 void ActivationContext_Dispose_m12681 (ActivationContext_t2231 * __this, bool ___disposing, MethodInfo* method) IL2CPP_METHOD_ATTR;
