﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_27.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
struct CachedInvokableCall_1_t2966  : public InvokableCall_1_t2967
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
