﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
struct ValueCollection_t1174;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1016;
// UnityEngine.GUIStyle
struct GUIStyle_t999;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GUIStyle>
struct IEnumerator_1_t4728;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1015;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_25.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32MethodDeclarations.h"
#define ValueCollection__ctor_m28643(__this, ___dictionary, method) (void)ValueCollection__ctor_m18389_gshared((ValueCollection_t3457 *)__this, (Dictionary_2_t3452 *)___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m28644(__this, ___item, method) (void)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18390_gshared((ValueCollection_t3457 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m28645(__this, method) (void)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18391_gshared((ValueCollection_t3457 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m28646(__this, ___item, method) (bool)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18392_gshared((ValueCollection_t3457 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m28647(__this, ___item, method) (bool)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18393_gshared((ValueCollection_t3457 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m28648(__this, method) (Object_t*)ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18394_gshared((ValueCollection_t3457 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m28649(__this, ___array, ___index, method) (void)ValueCollection_System_Collections_ICollection_CopyTo_m18395_gshared((ValueCollection_t3457 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m28650(__this, method) (Object_t *)ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18396_gshared((ValueCollection_t3457 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m28651(__this, method) (bool)ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18397_gshared((ValueCollection_t3457 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m28652(__this, method) (bool)ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18398_gshared((ValueCollection_t3457 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m28653(__this, method) (Object_t *)ValueCollection_System_Collections_ICollection_get_SyncRoot_m18399_gshared((ValueCollection_t3457 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m28654(__this, ___array, ___index, method) (void)ValueCollection_CopyTo_m18400_gshared((ValueCollection_t3457 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::GetEnumerator()
 Enumerator_t1173  ValueCollection_GetEnumerator_m6620 (ValueCollection_t1174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>::get_Count()
#define ValueCollection_get_Count_m28655(__this, method) (int32_t)ValueCollection_get_Count_m18402_gshared((ValueCollection_t3457 *)__this, method)
