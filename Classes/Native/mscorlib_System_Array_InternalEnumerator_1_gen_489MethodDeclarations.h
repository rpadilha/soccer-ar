﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>
struct InternalEnumerator_1_t4941;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29869 (InternalEnumerator_1_t4941 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870 (InternalEnumerator_1_t4941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::Dispose()
 void InternalEnumerator_1_Dispose_m29871 (InternalEnumerator_1_t4941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29872 (InternalEnumerator_1_t4941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29873 (InternalEnumerator_1_t4941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
