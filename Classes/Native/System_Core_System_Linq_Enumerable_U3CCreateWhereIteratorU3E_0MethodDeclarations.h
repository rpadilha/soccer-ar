﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
 void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20349_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20349(__this, method) (void)U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20349_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
 Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20350_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20350(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20350_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20351_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20351(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20351_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20352_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20352(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20352_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
 Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20353_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20353(__this, method) (Object_t*)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20353_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
 bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20354_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20354(__this, method) (bool)U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20354_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
 void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20355_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20355(__this, method) (void)U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20355_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
