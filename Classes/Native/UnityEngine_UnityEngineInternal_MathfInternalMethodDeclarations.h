﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.MathfInternal
struct MathfInternal_t1024;

// System.Void UnityEngineInternal.MathfInternal::.cctor()
 void MathfInternal__cctor_m6044 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
