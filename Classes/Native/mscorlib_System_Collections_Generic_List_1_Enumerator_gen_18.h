﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t800;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t805;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>
struct Enumerator_t935 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::l
	List_1_t800 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::current
	Object_t * ___current_3;
};
