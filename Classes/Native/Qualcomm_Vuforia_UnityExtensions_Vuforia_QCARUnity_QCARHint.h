﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"
// Vuforia.QCARUnity/QCARHint
struct QCARHint_t796 
{
	// System.Int32 Vuforia.QCARUnity/QCARHint::value__
	int32_t ___value___1;
};
