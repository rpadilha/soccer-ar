﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t49;

// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
 void SmartTerrainTrackerBehaviour__ctor_m84 (SmartTerrainTrackerBehaviour_t49 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
