﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t1898;

// System.Void System.Diagnostics.DebuggerStepThroughAttribute::.ctor()
 void DebuggerStepThroughAttribute__ctor_m10622 (DebuggerStepThroughAttribute_t1898 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
