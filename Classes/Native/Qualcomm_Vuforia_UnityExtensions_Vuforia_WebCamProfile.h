﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// Vuforia.WebCamProfile
struct WebCamProfile_t793  : public Object_t
{
	// Vuforia.WebCamProfile/ProfileCollection Vuforia.WebCamProfile::mProfileCollection
	ProfileCollection_t677  ___mProfileCollection_0;
};
