﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t6838_il2cpp_TypeInfo;

// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"

// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventType>
extern MethodInfo IEnumerator_1_get_Current_m48848_MethodInfo;
static PropertyInfo IEnumerator_1_t6838____Current_PropertyInfo = 
{
	&IEnumerator_1_t6838_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48848_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6838_PropertyInfos[] =
{
	&IEnumerator_1_t6838____Current_PropertyInfo,
	NULL
};
extern Il2CppType EventType_t1021_0_0_0;
extern void* RuntimeInvoker_EventType_t1021 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48848_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48848_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6838_il2cpp_TypeInfo/* declaring_type */
	, &EventType_t1021_0_0_0/* return_type */
	, RuntimeInvoker_EventType_t1021/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48848_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6838_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48848_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6838_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6838_0_0_0;
extern Il2CppType IEnumerator_1_t6838_1_0_0;
struct IEnumerator_1_t6838;
extern Il2CppGenericClass IEnumerator_1_t6838_GenericClass;
TypeInfo IEnumerator_1_t6838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6838_MethodInfos/* methods */
	, IEnumerator_1_t6838_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6838_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6838_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6838_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6838_0_0_0/* byval_arg */
	, &IEnumerator_1_t6838_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6838_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_432.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4751_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_432MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo EventType_t1021_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m28831_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEventType_t1021_m38313_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEventType_t1021_m38313 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28827_MethodInfo;
 void InternalEnumerator_1__ctor_m28827 (InternalEnumerator_1_t4751 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828 (InternalEnumerator_1_t4751 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28831(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28831_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EventType_t1021_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28829_MethodInfo;
 void InternalEnumerator_1_Dispose_m28829 (InternalEnumerator_1_t4751 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28830_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28830 (InternalEnumerator_1_t4751 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28831 (InternalEnumerator_1_t4751 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEventType_t1021_m38313(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEventType_t1021_m38313_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4751____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4751, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4751____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4751, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4751_FieldInfos[] =
{
	&InternalEnumerator_1_t4751____array_0_FieldInfo,
	&InternalEnumerator_1_t4751____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4751____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4751_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4751____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4751_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4751_PropertyInfos[] =
{
	&InternalEnumerator_1_t4751____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4751____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4751_InternalEnumerator_1__ctor_m28827_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28827_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28827_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28827/* method */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4751_InternalEnumerator_1__ctor_m28827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28827_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828/* method */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28829_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28829_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28829/* method */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28829_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28830_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28830_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28830/* method */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28830_GenericMethod/* genericMethod */

};
extern Il2CppType EventType_t1021_0_0_0;
extern void* RuntimeInvoker_EventType_t1021 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28831_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28831_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28831/* method */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* declaring_type */
	, &EventType_t1021_0_0_0/* return_type */
	, RuntimeInvoker_EventType_t1021/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28831_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4751_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28827_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_MethodInfo,
	&InternalEnumerator_1_Dispose_m28829_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28830_MethodInfo,
	&InternalEnumerator_1_get_Current_m28831_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4751_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28828_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28830_MethodInfo,
	&InternalEnumerator_1_Dispose_m28829_MethodInfo,
	&InternalEnumerator_1_get_Current_m28831_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4751_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6838_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4751_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6838_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4751_0_0_0;
extern Il2CppType InternalEnumerator_1_t4751_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4751_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4751_MethodInfos/* methods */
	, InternalEnumerator_1_t4751_PropertyInfos/* properties */
	, InternalEnumerator_1_t4751_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4751_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4751_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4751_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4751_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4751_1_0_0/* this_arg */
	, InternalEnumerator_1_t4751_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4751_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4751)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8667_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventType>
extern MethodInfo ICollection_1_get_Count_m48849_MethodInfo;
static PropertyInfo ICollection_1_t8667____Count_PropertyInfo = 
{
	&ICollection_1_t8667_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48849_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48850_MethodInfo;
static PropertyInfo ICollection_1_t8667____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8667_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48850_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8667_PropertyInfos[] =
{
	&ICollection_1_t8667____Count_PropertyInfo,
	&ICollection_1_t8667____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48849_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventType>::get_Count()
MethodInfo ICollection_1_get_Count_m48849_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48849_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48850_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48850_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48850_GenericMethod/* genericMethod */

};
extern Il2CppType EventType_t1021_0_0_0;
extern Il2CppType EventType_t1021_0_0_0;
static ParameterInfo ICollection_1_t8667_ICollection_1_Add_m48851_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventType_t1021_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48851_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Add(T)
MethodInfo ICollection_1_Add_m48851_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8667_ICollection_1_Add_m48851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48851_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48852_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Clear()
MethodInfo ICollection_1_Clear_m48852_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48852_GenericMethod/* genericMethod */

};
extern Il2CppType EventType_t1021_0_0_0;
static ParameterInfo ICollection_1_t8667_ICollection_1_Contains_m48853_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventType_t1021_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48853_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Contains(T)
MethodInfo ICollection_1_Contains_m48853_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8667_ICollection_1_Contains_m48853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48853_GenericMethod/* genericMethod */

};
extern Il2CppType EventTypeU5BU5D_t5703_0_0_0;
extern Il2CppType EventTypeU5BU5D_t5703_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8667_ICollection_1_CopyTo_m48854_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EventTypeU5BU5D_t5703_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48854_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48854_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8667_ICollection_1_CopyTo_m48854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48854_GenericMethod/* genericMethod */

};
extern Il2CppType EventType_t1021_0_0_0;
static ParameterInfo ICollection_1_t8667_ICollection_1_Remove_m48855_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventType_t1021_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48855_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventType>::Remove(T)
MethodInfo ICollection_1_Remove_m48855_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8667_ICollection_1_Remove_m48855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48855_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8667_MethodInfos[] =
{
	&ICollection_1_get_Count_m48849_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48850_MethodInfo,
	&ICollection_1_Add_m48851_MethodInfo,
	&ICollection_1_Clear_m48852_MethodInfo,
	&ICollection_1_Contains_m48853_MethodInfo,
	&ICollection_1_CopyTo_m48854_MethodInfo,
	&ICollection_1_Remove_m48855_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8669_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8667_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8669_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8667_0_0_0;
extern Il2CppType ICollection_1_t8667_1_0_0;
struct ICollection_1_t8667;
extern Il2CppGenericClass ICollection_1_t8667_GenericClass;
TypeInfo ICollection_1_t8667_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8667_MethodInfos/* methods */
	, ICollection_1_t8667_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8667_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8667_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8667_0_0_0/* byval_arg */
	, &ICollection_1_t8667_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8667_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventType>
extern Il2CppType IEnumerator_1_t6838_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48856_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48856_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8669_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6838_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48856_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8669_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48856_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8669_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8669_0_0_0;
extern Il2CppType IEnumerable_1_t8669_1_0_0;
struct IEnumerable_1_t8669;
extern Il2CppGenericClass IEnumerable_1_t8669_GenericClass;
TypeInfo IEnumerable_1_t8669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8669_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8669_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8669_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8669_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8669_0_0_0/* byval_arg */
	, &IEnumerable_1_t8669_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8669_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8668_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventType>
extern MethodInfo IList_1_get_Item_m48857_MethodInfo;
extern MethodInfo IList_1_set_Item_m48858_MethodInfo;
static PropertyInfo IList_1_t8668____Item_PropertyInfo = 
{
	&IList_1_t8668_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48857_MethodInfo/* get */
	, &IList_1_set_Item_m48858_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8668_PropertyInfos[] =
{
	&IList_1_t8668____Item_PropertyInfo,
	NULL
};
extern Il2CppType EventType_t1021_0_0_0;
static ParameterInfo IList_1_t8668_IList_1_IndexOf_m48859_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventType_t1021_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48859_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48859_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8668_IList_1_IndexOf_m48859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48859_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventType_t1021_0_0_0;
static ParameterInfo IList_1_t8668_IList_1_Insert_m48860_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EventType_t1021_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48860_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48860_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8668_IList_1_Insert_m48860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48860_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8668_IList_1_RemoveAt_m48861_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48861_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48861_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8668_IList_1_RemoveAt_m48861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48861_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8668_IList_1_get_Item_m48857_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EventType_t1021_0_0_0;
extern void* RuntimeInvoker_EventType_t1021_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48857_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48857_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &EventType_t1021_0_0_0/* return_type */
	, RuntimeInvoker_EventType_t1021_Int32_t123/* invoker_method */
	, IList_1_t8668_IList_1_get_Item_m48857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48857_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventType_t1021_0_0_0;
static ParameterInfo IList_1_t8668_IList_1_set_Item_m48858_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EventType_t1021_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48858_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48858_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8668_IList_1_set_Item_m48858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48858_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8668_MethodInfos[] =
{
	&IList_1_IndexOf_m48859_MethodInfo,
	&IList_1_Insert_m48860_MethodInfo,
	&IList_1_RemoveAt_m48861_MethodInfo,
	&IList_1_get_Item_m48857_MethodInfo,
	&IList_1_set_Item_m48858_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8668_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8667_il2cpp_TypeInfo,
	&IEnumerable_1_t8669_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8668_0_0_0;
extern Il2CppType IList_1_t8668_1_0_0;
struct IList_1_t8668;
extern Il2CppGenericClass IList_1_t8668_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8668_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8668_MethodInfos/* methods */
	, IList_1_t8668_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8668_il2cpp_TypeInfo/* element_class */
	, IList_1_t8668_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8668_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8668_0_0_0/* byval_arg */
	, &IList_1_t8668_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8668_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6840_il2cpp_TypeInfo;

// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventModifiers>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventModifiers>
extern MethodInfo IEnumerator_1_get_Current_m48862_MethodInfo;
static PropertyInfo IEnumerator_1_t6840____Current_PropertyInfo = 
{
	&IEnumerator_1_t6840_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48862_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6840_PropertyInfos[] =
{
	&IEnumerator_1_t6840____Current_PropertyInfo,
	NULL
};
extern Il2CppType EventModifiers_t1022_0_0_0;
extern void* RuntimeInvoker_EventModifiers_t1022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48862_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventModifiers>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48862_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6840_il2cpp_TypeInfo/* declaring_type */
	, &EventModifiers_t1022_0_0_0/* return_type */
	, RuntimeInvoker_EventModifiers_t1022/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48862_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6840_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48862_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6840_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6840_0_0_0;
extern Il2CppType IEnumerator_1_t6840_1_0_0;
struct IEnumerator_1_t6840;
extern Il2CppGenericClass IEnumerator_1_t6840_GenericClass;
TypeInfo IEnumerator_1_t6840_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6840_MethodInfos/* methods */
	, IEnumerator_1_t6840_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6840_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6840_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6840_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6840_0_0_0/* byval_arg */
	, &IEnumerator_1_t6840_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6840_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_433.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4752_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_433MethodDeclarations.h"

extern TypeInfo EventModifiers_t1022_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28836_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEventModifiers_t1022_m38324_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventModifiers>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventModifiers>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEventModifiers_t1022_m38324 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28832_MethodInfo;
 void InternalEnumerator_1__ctor_m28832 (InternalEnumerator_1_t4752 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833 (InternalEnumerator_1_t4752 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28836(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28836_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EventModifiers_t1022_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28834_MethodInfo;
 void InternalEnumerator_1_Dispose_m28834 (InternalEnumerator_1_t4752 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28835_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28835 (InternalEnumerator_1_t4752 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28836 (InternalEnumerator_1_t4752 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEventModifiers_t1022_m38324(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEventModifiers_t1022_m38324_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4752____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4752, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4752____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4752, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4752_FieldInfos[] =
{
	&InternalEnumerator_1_t4752____array_0_FieldInfo,
	&InternalEnumerator_1_t4752____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4752____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4752_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4752____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4752_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28836_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4752_PropertyInfos[] =
{
	&InternalEnumerator_1_t4752____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4752____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4752_InternalEnumerator_1__ctor_m28832_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28832_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28832_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28832/* method */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4752_InternalEnumerator_1__ctor_m28832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28832_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833/* method */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28834_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28834_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28834/* method */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28834_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28835_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28835_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28835/* method */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28835_GenericMethod/* genericMethod */

};
extern Il2CppType EventModifiers_t1022_0_0_0;
extern void* RuntimeInvoker_EventModifiers_t1022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28836_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventModifiers>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28836_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28836/* method */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* declaring_type */
	, &EventModifiers_t1022_0_0_0/* return_type */
	, RuntimeInvoker_EventModifiers_t1022/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28836_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4752_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28832_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_MethodInfo,
	&InternalEnumerator_1_Dispose_m28834_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28835_MethodInfo,
	&InternalEnumerator_1_get_Current_m28836_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4752_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28833_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28835_MethodInfo,
	&InternalEnumerator_1_Dispose_m28834_MethodInfo,
	&InternalEnumerator_1_get_Current_m28836_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4752_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6840_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4752_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6840_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4752_0_0_0;
extern Il2CppType InternalEnumerator_1_t4752_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4752_GenericClass;
TypeInfo InternalEnumerator_1_t4752_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4752_MethodInfos/* methods */
	, InternalEnumerator_1_t4752_PropertyInfos/* properties */
	, InternalEnumerator_1_t4752_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4752_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4752_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4752_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4752_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4752_1_0_0/* this_arg */
	, InternalEnumerator_1_t4752_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4752_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4752)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8670_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>
extern MethodInfo ICollection_1_get_Count_m48863_MethodInfo;
static PropertyInfo ICollection_1_t8670____Count_PropertyInfo = 
{
	&ICollection_1_t8670_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48863_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48864_MethodInfo;
static PropertyInfo ICollection_1_t8670____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8670_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48864_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8670_PropertyInfos[] =
{
	&ICollection_1_t8670____Count_PropertyInfo,
	&ICollection_1_t8670____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48863_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::get_Count()
MethodInfo ICollection_1_get_Count_m48863_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48863_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48864_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48864_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48864_GenericMethod/* genericMethod */

};
extern Il2CppType EventModifiers_t1022_0_0_0;
extern Il2CppType EventModifiers_t1022_0_0_0;
static ParameterInfo ICollection_1_t8670_ICollection_1_Add_m48865_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventModifiers_t1022_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48865_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Add(T)
MethodInfo ICollection_1_Add_m48865_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8670_ICollection_1_Add_m48865_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48865_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48866_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Clear()
MethodInfo ICollection_1_Clear_m48866_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48866_GenericMethod/* genericMethod */

};
extern Il2CppType EventModifiers_t1022_0_0_0;
static ParameterInfo ICollection_1_t8670_ICollection_1_Contains_m48867_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventModifiers_t1022_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48867_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Contains(T)
MethodInfo ICollection_1_Contains_m48867_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8670_ICollection_1_Contains_m48867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48867_GenericMethod/* genericMethod */

};
extern Il2CppType EventModifiersU5BU5D_t5704_0_0_0;
extern Il2CppType EventModifiersU5BU5D_t5704_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8670_ICollection_1_CopyTo_m48868_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EventModifiersU5BU5D_t5704_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48868_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48868_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8670_ICollection_1_CopyTo_m48868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48868_GenericMethod/* genericMethod */

};
extern Il2CppType EventModifiers_t1022_0_0_0;
static ParameterInfo ICollection_1_t8670_ICollection_1_Remove_m48869_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventModifiers_t1022_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48869_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventModifiers>::Remove(T)
MethodInfo ICollection_1_Remove_m48869_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8670_ICollection_1_Remove_m48869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48869_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8670_MethodInfos[] =
{
	&ICollection_1_get_Count_m48863_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48864_MethodInfo,
	&ICollection_1_Add_m48865_MethodInfo,
	&ICollection_1_Clear_m48866_MethodInfo,
	&ICollection_1_Contains_m48867_MethodInfo,
	&ICollection_1_CopyTo_m48868_MethodInfo,
	&ICollection_1_Remove_m48869_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8672_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8670_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8672_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8670_0_0_0;
extern Il2CppType ICollection_1_t8670_1_0_0;
struct ICollection_1_t8670;
extern Il2CppGenericClass ICollection_1_t8670_GenericClass;
TypeInfo ICollection_1_t8670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8670_MethodInfos/* methods */
	, ICollection_1_t8670_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8670_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8670_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8670_0_0_0/* byval_arg */
	, &ICollection_1_t8670_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8670_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventModifiers>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventModifiers>
extern Il2CppType IEnumerator_1_t6840_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48870_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventModifiers>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48870_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8672_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6840_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48870_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8672_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48870_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8672_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8672_0_0_0;
extern Il2CppType IEnumerable_1_t8672_1_0_0;
struct IEnumerable_1_t8672;
extern Il2CppGenericClass IEnumerable_1_t8672_GenericClass;
TypeInfo IEnumerable_1_t8672_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8672_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8672_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8672_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8672_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8672_0_0_0/* byval_arg */
	, &IEnumerable_1_t8672_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8672_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8671_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventModifiers>
extern MethodInfo IList_1_get_Item_m48871_MethodInfo;
extern MethodInfo IList_1_set_Item_m48872_MethodInfo;
static PropertyInfo IList_1_t8671____Item_PropertyInfo = 
{
	&IList_1_t8671_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48871_MethodInfo/* get */
	, &IList_1_set_Item_m48872_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8671_PropertyInfos[] =
{
	&IList_1_t8671____Item_PropertyInfo,
	NULL
};
extern Il2CppType EventModifiers_t1022_0_0_0;
static ParameterInfo IList_1_t8671_IList_1_IndexOf_m48873_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventModifiers_t1022_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48873_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48873_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8671_IList_1_IndexOf_m48873_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48873_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventModifiers_t1022_0_0_0;
static ParameterInfo IList_1_t8671_IList_1_Insert_m48874_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EventModifiers_t1022_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48874_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48874_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8671_IList_1_Insert_m48874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48874_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8671_IList_1_RemoveAt_m48875_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48875_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48875_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8671_IList_1_RemoveAt_m48875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48875_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8671_IList_1_get_Item_m48871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EventModifiers_t1022_0_0_0;
extern void* RuntimeInvoker_EventModifiers_t1022_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48871_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48871_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &EventModifiers_t1022_0_0_0/* return_type */
	, RuntimeInvoker_EventModifiers_t1022_Int32_t123/* invoker_method */
	, IList_1_t8671_IList_1_get_Item_m48871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48871_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventModifiers_t1022_0_0_0;
static ParameterInfo IList_1_t8671_IList_1_set_Item_m48872_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EventModifiers_t1022_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48872_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventModifiers>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48872_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8671_IList_1_set_Item_m48872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48872_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8671_MethodInfos[] =
{
	&IList_1_IndexOf_m48873_MethodInfo,
	&IList_1_Insert_m48874_MethodInfo,
	&IList_1_RemoveAt_m48875_MethodInfo,
	&IList_1_get_Item_m48871_MethodInfo,
	&IList_1_set_Item_m48872_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8671_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8670_il2cpp_TypeInfo,
	&IEnumerable_1_t8672_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8671_0_0_0;
extern Il2CppType IList_1_t8671_1_0_0;
struct IList_1_t8671;
extern Il2CppGenericClass IList_1_t8671_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8671_MethodInfos/* methods */
	, IList_1_t8671_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8671_il2cpp_TypeInfo/* element_class */
	, IList_1_t8671_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8671_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8671_0_0_0/* byval_arg */
	, &IList_1_t8671_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8671_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6842_il2cpp_TypeInfo;

// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.DrivenTransformProperties>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.DrivenTransformProperties>
extern MethodInfo IEnumerator_1_get_Current_m48876_MethodInfo;
static PropertyInfo IEnumerator_1_t6842____Current_PropertyInfo = 
{
	&IEnumerator_1_t6842_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48876_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6842_PropertyInfos[] =
{
	&IEnumerator_1_t6842____Current_PropertyInfo,
	NULL
};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
extern void* RuntimeInvoker_DrivenTransformProperties_t1025 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48876_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.DrivenTransformProperties>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48876_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6842_il2cpp_TypeInfo/* declaring_type */
	, &DrivenTransformProperties_t1025_0_0_0/* return_type */
	, RuntimeInvoker_DrivenTransformProperties_t1025/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48876_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6842_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48876_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6842_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6842_0_0_0;
extern Il2CppType IEnumerator_1_t6842_1_0_0;
struct IEnumerator_1_t6842;
extern Il2CppGenericClass IEnumerator_1_t6842_GenericClass;
TypeInfo IEnumerator_1_t6842_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6842_MethodInfos/* methods */
	, IEnumerator_1_t6842_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6842_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6842_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6842_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6842_0_0_0/* byval_arg */
	, &IEnumerator_1_t6842_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6842_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_434.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4753_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_434MethodDeclarations.h"

extern TypeInfo DrivenTransformProperties_t1025_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28841_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDrivenTransformProperties_t1025_m38335_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.DrivenTransformProperties>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.DrivenTransformProperties>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDrivenTransformProperties_t1025_m38335 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28837_MethodInfo;
 void InternalEnumerator_1__ctor_m28837 (InternalEnumerator_1_t4753 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838 (InternalEnumerator_1_t4753 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28841(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28841_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&DrivenTransformProperties_t1025_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28839_MethodInfo;
 void InternalEnumerator_1_Dispose_m28839 (InternalEnumerator_1_t4753 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28840_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28840 (InternalEnumerator_1_t4753 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28841 (InternalEnumerator_1_t4753 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisDrivenTransformProperties_t1025_m38335(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDrivenTransformProperties_t1025_m38335_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4753____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4753, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4753____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4753, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4753_FieldInfos[] =
{
	&InternalEnumerator_1_t4753____array_0_FieldInfo,
	&InternalEnumerator_1_t4753____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4753____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4753_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4753____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4753_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28841_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4753_PropertyInfos[] =
{
	&InternalEnumerator_1_t4753____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4753____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4753_InternalEnumerator_1__ctor_m28837_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28837_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28837_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28837/* method */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4753_InternalEnumerator_1__ctor_m28837_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28837_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838/* method */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28839_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28839_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28839/* method */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28839_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28840_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28840_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28840/* method */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28840_GenericMethod/* genericMethod */

};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
extern void* RuntimeInvoker_DrivenTransformProperties_t1025 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28841_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.DrivenTransformProperties>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28841_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28841/* method */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* declaring_type */
	, &DrivenTransformProperties_t1025_0_0_0/* return_type */
	, RuntimeInvoker_DrivenTransformProperties_t1025/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28841_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4753_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28837_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_MethodInfo,
	&InternalEnumerator_1_Dispose_m28839_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28840_MethodInfo,
	&InternalEnumerator_1_get_Current_m28841_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4753_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28838_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28840_MethodInfo,
	&InternalEnumerator_1_Dispose_m28839_MethodInfo,
	&InternalEnumerator_1_get_Current_m28841_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4753_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6842_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4753_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6842_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4753_0_0_0;
extern Il2CppType InternalEnumerator_1_t4753_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4753_GenericClass;
TypeInfo InternalEnumerator_1_t4753_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4753_MethodInfos/* methods */
	, InternalEnumerator_1_t4753_PropertyInfos/* properties */
	, InternalEnumerator_1_t4753_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4753_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4753_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4753_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4753_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4753_1_0_0/* this_arg */
	, InternalEnumerator_1_t4753_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4753_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4753)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8673_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>
extern MethodInfo ICollection_1_get_Count_m48877_MethodInfo;
static PropertyInfo ICollection_1_t8673____Count_PropertyInfo = 
{
	&ICollection_1_t8673_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48877_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48878_MethodInfo;
static PropertyInfo ICollection_1_t8673____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8673_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48878_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8673_PropertyInfos[] =
{
	&ICollection_1_t8673____Count_PropertyInfo,
	&ICollection_1_t8673____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48877_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::get_Count()
MethodInfo ICollection_1_get_Count_m48877_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48877_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48878_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48878_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48878_GenericMethod/* genericMethod */

};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
static ParameterInfo ICollection_1_t8673_ICollection_1_Add_m48879_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DrivenTransformProperties_t1025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48879_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Add(T)
MethodInfo ICollection_1_Add_m48879_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8673_ICollection_1_Add_m48879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48879_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48880_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Clear()
MethodInfo ICollection_1_Clear_m48880_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48880_GenericMethod/* genericMethod */

};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
static ParameterInfo ICollection_1_t8673_ICollection_1_Contains_m48881_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DrivenTransformProperties_t1025_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48881_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Contains(T)
MethodInfo ICollection_1_Contains_m48881_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8673_ICollection_1_Contains_m48881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48881_GenericMethod/* genericMethod */

};
extern Il2CppType DrivenTransformPropertiesU5BU5D_t5705_0_0_0;
extern Il2CppType DrivenTransformPropertiesU5BU5D_t5705_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8673_ICollection_1_CopyTo_m48882_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DrivenTransformPropertiesU5BU5D_t5705_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48882_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48882_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8673_ICollection_1_CopyTo_m48882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48882_GenericMethod/* genericMethod */

};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
static ParameterInfo ICollection_1_t8673_ICollection_1_Remove_m48883_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DrivenTransformProperties_t1025_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48883_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.DrivenTransformProperties>::Remove(T)
MethodInfo ICollection_1_Remove_m48883_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8673_ICollection_1_Remove_m48883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48883_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8673_MethodInfos[] =
{
	&ICollection_1_get_Count_m48877_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48878_MethodInfo,
	&ICollection_1_Add_m48879_MethodInfo,
	&ICollection_1_Clear_m48880_MethodInfo,
	&ICollection_1_Contains_m48881_MethodInfo,
	&ICollection_1_CopyTo_m48882_MethodInfo,
	&ICollection_1_Remove_m48883_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8675_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8673_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8675_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8673_0_0_0;
extern Il2CppType ICollection_1_t8673_1_0_0;
struct ICollection_1_t8673;
extern Il2CppGenericClass ICollection_1_t8673_GenericClass;
TypeInfo ICollection_1_t8673_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8673_MethodInfos/* methods */
	, ICollection_1_t8673_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8673_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8673_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8673_0_0_0/* byval_arg */
	, &ICollection_1_t8673_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8673_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.DrivenTransformProperties>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.DrivenTransformProperties>
extern Il2CppType IEnumerator_1_t6842_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48884_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.DrivenTransformProperties>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48884_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8675_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6842_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48884_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8675_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48884_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8675_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8675_0_0_0;
extern Il2CppType IEnumerable_1_t8675_1_0_0;
struct IEnumerable_1_t8675;
extern Il2CppGenericClass IEnumerable_1_t8675_GenericClass;
TypeInfo IEnumerable_1_t8675_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8675_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8675_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8675_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8675_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8675_0_0_0/* byval_arg */
	, &IEnumerable_1_t8675_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8675_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8674_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>
extern MethodInfo IList_1_get_Item_m48885_MethodInfo;
extern MethodInfo IList_1_set_Item_m48886_MethodInfo;
static PropertyInfo IList_1_t8674____Item_PropertyInfo = 
{
	&IList_1_t8674_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48885_MethodInfo/* get */
	, &IList_1_set_Item_m48886_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8674_PropertyInfos[] =
{
	&IList_1_t8674____Item_PropertyInfo,
	NULL
};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
static ParameterInfo IList_1_t8674_IList_1_IndexOf_m48887_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DrivenTransformProperties_t1025_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48887_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48887_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8674_IList_1_IndexOf_m48887_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48887_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
static ParameterInfo IList_1_t8674_IList_1_Insert_m48888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DrivenTransformProperties_t1025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48888_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48888_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8674_IList_1_Insert_m48888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48888_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8674_IList_1_RemoveAt_m48889_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48889_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48889_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8674_IList_1_RemoveAt_m48889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48889_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8674_IList_1_get_Item_m48885_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
extern void* RuntimeInvoker_DrivenTransformProperties_t1025_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48885_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48885_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &DrivenTransformProperties_t1025_0_0_0/* return_type */
	, RuntimeInvoker_DrivenTransformProperties_t1025_Int32_t123/* invoker_method */
	, IList_1_t8674_IList_1_get_Item_m48885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48885_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DrivenTransformProperties_t1025_0_0_0;
static ParameterInfo IList_1_t8674_IList_1_set_Item_m48886_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DrivenTransformProperties_t1025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48886_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.DrivenTransformProperties>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48886_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8674_IList_1_set_Item_m48886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48886_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8674_MethodInfos[] =
{
	&IList_1_IndexOf_m48887_MethodInfo,
	&IList_1_Insert_m48888_MethodInfo,
	&IList_1_RemoveAt_m48889_MethodInfo,
	&IList_1_get_Item_m48885_MethodInfo,
	&IList_1_set_Item_m48886_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8674_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8673_il2cpp_TypeInfo,
	&IEnumerable_1_t8675_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8674_0_0_0;
extern Il2CppType IList_1_t8674_1_0_0;
struct IList_1_t8674;
extern Il2CppGenericClass IList_1_t8674_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8674_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8674_MethodInfos/* methods */
	, IList_1_t8674_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8674_il2cpp_TypeInfo/* element_class */
	, IList_1_t8674_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8674_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8674_0_0_0/* byval_arg */
	, &IList_1_t8674_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8674_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_150.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4754_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_150MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_152.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo RectTransform_t338_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4755_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_152MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28844_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28846_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4754____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4754_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4754, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4754_FieldInfos[] =
{
	&CachedInvokableCall_1_t4754____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType RectTransform_t338_0_0_0;
extern Il2CppType RectTransform_t338_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4754_CachedInvokableCall_1__ctor_m28842_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &RectTransform_t338_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28842_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28842_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4754_CachedInvokableCall_1__ctor_m28842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28842_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4754_CachedInvokableCall_1_Invoke_m28843_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28843_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RectTransform>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28843_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4754_CachedInvokableCall_1_Invoke_m28843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28843_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4754_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28842_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28843_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m28843_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28847_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4754_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28843_MethodInfo,
	&InvokableCall_1_Find_m28847_MethodInfo,
};
extern Il2CppType UnityAction_1_t4756_0_0_0;
extern TypeInfo UnityAction_1_t4756_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRectTransform_t338_m38345_MethodInfo;
extern TypeInfo RectTransform_t338_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28849_MethodInfo;
extern TypeInfo RectTransform_t338_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4754_RGCTXData[8] = 
{
	&UnityAction_1_t4756_0_0_0/* Type Usage */,
	&UnityAction_1_t4756_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRectTransform_t338_m38345_MethodInfo/* Method Usage */,
	&RectTransform_t338_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28849_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28844_MethodInfo/* Method Usage */,
	&RectTransform_t338_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28846_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4754_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4754_1_0_0;
struct CachedInvokableCall_1_t4754;
extern Il2CppGenericClass CachedInvokableCall_1_t4754_GenericClass;
TypeInfo CachedInvokableCall_1_t4754_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4754_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4754_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4754_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4754_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4754_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4754_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4754_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4754_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4754_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4754)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_157.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t4756_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_157MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.RectTransform>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.RectTransform>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRectTransform_t338_m38345(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>
extern Il2CppType UnityAction_1_t4756_0_0_1;
FieldInfo InvokableCall_1_t4755____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4756_0_0_1/* type */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4755, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4755_FieldInfos[] =
{
	&InvokableCall_1_t4755____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4755_InvokableCall_1__ctor_m28844_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28844_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28844_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4755_InvokableCall_1__ctor_m28844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28844_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4756_0_0_0;
static ParameterInfo InvokableCall_1_t4755_InvokableCall_1__ctor_m28845_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4756_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28845_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28845_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4755_InvokableCall_1__ctor_m28845_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28845_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4755_InvokableCall_1_Invoke_m28846_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28846_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28846_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4755_InvokableCall_1_Invoke_m28846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28846_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4755_InvokableCall_1_Find_m28847_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28847_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.RectTransform>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28847_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4755_InvokableCall_1_Find_m28847_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28847_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4755_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28844_MethodInfo,
	&InvokableCall_1__ctor_m28845_MethodInfo,
	&InvokableCall_1_Invoke_m28846_MethodInfo,
	&InvokableCall_1_Find_m28847_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4755_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28846_MethodInfo,
	&InvokableCall_1_Find_m28847_MethodInfo,
};
extern TypeInfo UnityAction_1_t4756_il2cpp_TypeInfo;
extern TypeInfo RectTransform_t338_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4755_RGCTXData[5] = 
{
	&UnityAction_1_t4756_0_0_0/* Type Usage */,
	&UnityAction_1_t4756_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRectTransform_t338_m38345_MethodInfo/* Method Usage */,
	&RectTransform_t338_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28849_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4755_0_0_0;
extern Il2CppType InvokableCall_1_t4755_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t4755;
extern Il2CppGenericClass InvokableCall_1_t4755_GenericClass;
TypeInfo InvokableCall_1_t4755_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4755_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4755_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4755_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4755_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4755_0_0_0/* byval_arg */
	, &InvokableCall_1_t4755_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4755_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4755_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4755)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4756_UnityAction_1__ctor_m28848_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28848_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28848_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4756_UnityAction_1__ctor_m28848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28848_GenericMethod/* genericMethod */

};
extern Il2CppType RectTransform_t338_0_0_0;
static ParameterInfo UnityAction_1_t4756_UnityAction_1_Invoke_m28849_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RectTransform_t338_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28849_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28849_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4756_UnityAction_1_Invoke_m28849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28849_GenericMethod/* genericMethod */

};
extern Il2CppType RectTransform_t338_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4756_UnityAction_1_BeginInvoke_m28850_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RectTransform_t338_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28850_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28850_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4756_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4756_UnityAction_1_BeginInvoke_m28850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28850_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4756_UnityAction_1_EndInvoke_m28851_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28851_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RectTransform>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28851_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4756_UnityAction_1_EndInvoke_m28851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28851_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4756_MethodInfos[] =
{
	&UnityAction_1__ctor_m28848_MethodInfo,
	&UnityAction_1_Invoke_m28849_MethodInfo,
	&UnityAction_1_BeginInvoke_m28850_MethodInfo,
	&UnityAction_1_EndInvoke_m28851_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m28850_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28851_MethodInfo;
static MethodInfo* UnityAction_1_t4756_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28849_MethodInfo,
	&UnityAction_1_BeginInvoke_m28850_MethodInfo,
	&UnityAction_1_EndInvoke_m28851_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t4756_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4756_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t4756;
extern Il2CppGenericClass UnityAction_1_t4756_GenericClass;
TypeInfo UnityAction_1_t4756_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4756_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4756_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4756_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4756_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4756_0_0_0/* byval_arg */
	, &UnityAction_1_t4756_1_0_0/* this_arg */
	, UnityAction_1_t4756_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4756_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4756)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6844_il2cpp_TypeInfo;

// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Edge>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Edge>
extern MethodInfo IEnumerator_1_get_Current_m48890_MethodInfo;
static PropertyInfo IEnumerator_1_t6844____Current_PropertyInfo = 
{
	&IEnumerator_1_t6844_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48890_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6844_PropertyInfos[] =
{
	&IEnumerator_1_t6844____Current_PropertyInfo,
	NULL
};
extern Il2CppType Edge_t1026_0_0_0;
extern void* RuntimeInvoker_Edge_t1026 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48890_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Edge>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48890_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6844_il2cpp_TypeInfo/* declaring_type */
	, &Edge_t1026_0_0_0/* return_type */
	, RuntimeInvoker_Edge_t1026/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48890_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6844_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48890_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6844_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6844_0_0_0;
extern Il2CppType IEnumerator_1_t6844_1_0_0;
struct IEnumerator_1_t6844;
extern Il2CppGenericClass IEnumerator_1_t6844_GenericClass;
TypeInfo IEnumerator_1_t6844_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6844_MethodInfos/* methods */
	, IEnumerator_1_t6844_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6844_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6844_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6844_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6844_0_0_0/* byval_arg */
	, &IEnumerator_1_t6844_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6844_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_435.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4757_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_435MethodDeclarations.h"

extern TypeInfo Edge_t1026_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28856_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEdge_t1026_m38347_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.RectTransform/Edge>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RectTransform/Edge>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEdge_t1026_m38347 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28852_MethodInfo;
 void InternalEnumerator_1__ctor_m28852 (InternalEnumerator_1_t4757 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853 (InternalEnumerator_1_t4757 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28856(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28856_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Edge_t1026_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28854_MethodInfo;
 void InternalEnumerator_1_Dispose_m28854 (InternalEnumerator_1_t4757 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28855_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28855 (InternalEnumerator_1_t4757 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28856 (InternalEnumerator_1_t4757 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEdge_t1026_m38347(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEdge_t1026_m38347_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4757____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4757, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4757____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4757, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4757_FieldInfos[] =
{
	&InternalEnumerator_1_t4757____array_0_FieldInfo,
	&InternalEnumerator_1_t4757____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4757____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4757_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4757____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4757_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28856_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4757_PropertyInfos[] =
{
	&InternalEnumerator_1_t4757____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4757____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4757_InternalEnumerator_1__ctor_m28852_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28852_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28852_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28852/* method */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4757_InternalEnumerator_1__ctor_m28852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28852_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853/* method */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28854_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28854_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28854/* method */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28854_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28855_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28855_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28855/* method */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28855_GenericMethod/* genericMethod */

};
extern Il2CppType Edge_t1026_0_0_0;
extern void* RuntimeInvoker_Edge_t1026 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28856_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28856_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28856/* method */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* declaring_type */
	, &Edge_t1026_0_0_0/* return_type */
	, RuntimeInvoker_Edge_t1026/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28856_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4757_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28852_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_MethodInfo,
	&InternalEnumerator_1_Dispose_m28854_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28855_MethodInfo,
	&InternalEnumerator_1_get_Current_m28856_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4757_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28855_MethodInfo,
	&InternalEnumerator_1_Dispose_m28854_MethodInfo,
	&InternalEnumerator_1_get_Current_m28856_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4757_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6844_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4757_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6844_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4757_0_0_0;
extern Il2CppType InternalEnumerator_1_t4757_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4757_GenericClass;
TypeInfo InternalEnumerator_1_t4757_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4757_MethodInfos/* methods */
	, InternalEnumerator_1_t4757_PropertyInfos/* properties */
	, InternalEnumerator_1_t4757_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4757_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4757_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4757_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4757_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4757_1_0_0/* this_arg */
	, InternalEnumerator_1_t4757_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4757_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4757)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8676_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>
extern MethodInfo ICollection_1_get_Count_m48891_MethodInfo;
static PropertyInfo ICollection_1_t8676____Count_PropertyInfo = 
{
	&ICollection_1_t8676_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48891_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48892_MethodInfo;
static PropertyInfo ICollection_1_t8676____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8676_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48892_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8676_PropertyInfos[] =
{
	&ICollection_1_t8676____Count_PropertyInfo,
	&ICollection_1_t8676____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48891_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::get_Count()
MethodInfo ICollection_1_get_Count_m48891_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48891_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48892_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48892_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48892_GenericMethod/* genericMethod */

};
extern Il2CppType Edge_t1026_0_0_0;
extern Il2CppType Edge_t1026_0_0_0;
static ParameterInfo ICollection_1_t8676_ICollection_1_Add_m48893_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Edge_t1026_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48893_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Add(T)
MethodInfo ICollection_1_Add_m48893_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8676_ICollection_1_Add_m48893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48893_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48894_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Clear()
MethodInfo ICollection_1_Clear_m48894_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48894_GenericMethod/* genericMethod */

};
extern Il2CppType Edge_t1026_0_0_0;
static ParameterInfo ICollection_1_t8676_ICollection_1_Contains_m48895_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Edge_t1026_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48895_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Contains(T)
MethodInfo ICollection_1_Contains_m48895_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8676_ICollection_1_Contains_m48895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48895_GenericMethod/* genericMethod */

};
extern Il2CppType EdgeU5BU5D_t5706_0_0_0;
extern Il2CppType EdgeU5BU5D_t5706_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8676_ICollection_1_CopyTo_m48896_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EdgeU5BU5D_t5706_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48896_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48896_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8676_ICollection_1_CopyTo_m48896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48896_GenericMethod/* genericMethod */

};
extern Il2CppType Edge_t1026_0_0_0;
static ParameterInfo ICollection_1_t8676_ICollection_1_Remove_m48897_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Edge_t1026_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48897_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Edge>::Remove(T)
MethodInfo ICollection_1_Remove_m48897_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8676_ICollection_1_Remove_m48897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48897_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8676_MethodInfos[] =
{
	&ICollection_1_get_Count_m48891_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48892_MethodInfo,
	&ICollection_1_Add_m48893_MethodInfo,
	&ICollection_1_Clear_m48894_MethodInfo,
	&ICollection_1_Contains_m48895_MethodInfo,
	&ICollection_1_CopyTo_m48896_MethodInfo,
	&ICollection_1_Remove_m48897_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8678_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8676_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8678_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8676_0_0_0;
extern Il2CppType ICollection_1_t8676_1_0_0;
struct ICollection_1_t8676;
extern Il2CppGenericClass ICollection_1_t8676_GenericClass;
TypeInfo ICollection_1_t8676_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8676_MethodInfos/* methods */
	, ICollection_1_t8676_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8676_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8676_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8676_0_0_0/* byval_arg */
	, &ICollection_1_t8676_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8676_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Edge>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Edge>
extern Il2CppType IEnumerator_1_t6844_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48898_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Edge>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48898_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8678_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6844_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48898_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8678_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48898_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8678_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8678_0_0_0;
extern Il2CppType IEnumerable_1_t8678_1_0_0;
struct IEnumerable_1_t8678;
extern Il2CppGenericClass IEnumerable_1_t8678_GenericClass;
TypeInfo IEnumerable_1_t8678_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8678_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8678_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8678_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8678_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8678_0_0_0/* byval_arg */
	, &IEnumerable_1_t8678_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8678_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8677_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>
extern MethodInfo IList_1_get_Item_m48899_MethodInfo;
extern MethodInfo IList_1_set_Item_m48900_MethodInfo;
static PropertyInfo IList_1_t8677____Item_PropertyInfo = 
{
	&IList_1_t8677_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48899_MethodInfo/* get */
	, &IList_1_set_Item_m48900_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8677_PropertyInfos[] =
{
	&IList_1_t8677____Item_PropertyInfo,
	NULL
};
extern Il2CppType Edge_t1026_0_0_0;
static ParameterInfo IList_1_t8677_IList_1_IndexOf_m48901_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Edge_t1026_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48901_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48901_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8677_IList_1_IndexOf_m48901_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48901_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Edge_t1026_0_0_0;
static ParameterInfo IList_1_t8677_IList_1_Insert_m48902_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Edge_t1026_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48902_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48902_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8677_IList_1_Insert_m48902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48902_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8677_IList_1_RemoveAt_m48903_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48903_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48903_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8677_IList_1_RemoveAt_m48903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48903_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8677_IList_1_get_Item_m48899_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Edge_t1026_0_0_0;
extern void* RuntimeInvoker_Edge_t1026_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48899_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48899_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Edge_t1026_0_0_0/* return_type */
	, RuntimeInvoker_Edge_t1026_Int32_t123/* invoker_method */
	, IList_1_t8677_IList_1_get_Item_m48899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48899_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Edge_t1026_0_0_0;
static ParameterInfo IList_1_t8677_IList_1_set_Item_m48900_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Edge_t1026_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48900_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Edge>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48900_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8677_IList_1_set_Item_m48900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48900_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8677_MethodInfos[] =
{
	&IList_1_IndexOf_m48901_MethodInfo,
	&IList_1_Insert_m48902_MethodInfo,
	&IList_1_RemoveAt_m48903_MethodInfo,
	&IList_1_get_Item_m48899_MethodInfo,
	&IList_1_set_Item_m48900_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8677_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8676_il2cpp_TypeInfo,
	&IEnumerable_1_t8678_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8677_0_0_0;
extern Il2CppType IList_1_t8677_1_0_0;
struct IList_1_t8677;
extern Il2CppGenericClass IList_1_t8677_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8677_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8677_MethodInfos/* methods */
	, IList_1_t8677_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8677_il2cpp_TypeInfo/* element_class */
	, IList_1_t8677_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8677_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8677_0_0_0/* byval_arg */
	, &IList_1_t8677_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8677_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6846_il2cpp_TypeInfo;

// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Axis>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Axis>
extern MethodInfo IEnumerator_1_get_Current_m48904_MethodInfo;
static PropertyInfo IEnumerator_1_t6846____Current_PropertyInfo = 
{
	&IEnumerator_1_t6846_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48904_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6846_PropertyInfos[] =
{
	&IEnumerator_1_t6846____Current_PropertyInfo,
	NULL
};
extern Il2CppType Axis_t1027_0_0_0;
extern void* RuntimeInvoker_Axis_t1027 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48904_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform/Axis>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48904_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6846_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t1027_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t1027/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48904_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6846_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48904_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6846_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6846_0_0_0;
extern Il2CppType IEnumerator_1_t6846_1_0_0;
struct IEnumerator_1_t6846;
extern Il2CppGenericClass IEnumerator_1_t6846_GenericClass;
TypeInfo IEnumerator_1_t6846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6846_MethodInfos/* methods */
	, IEnumerator_1_t6846_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6846_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6846_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6846_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6846_0_0_0/* byval_arg */
	, &IEnumerator_1_t6846_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6846_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_436.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4758_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_436MethodDeclarations.h"

extern TypeInfo Axis_t1027_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28861_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAxis_t1027_m38358_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.RectTransform/Axis>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RectTransform/Axis>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAxis_t1027_m38358 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28857_MethodInfo;
 void InternalEnumerator_1__ctor_m28857 (InternalEnumerator_1_t4758 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858 (InternalEnumerator_1_t4758 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28861(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28861_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Axis_t1027_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28859_MethodInfo;
 void InternalEnumerator_1_Dispose_m28859 (InternalEnumerator_1_t4758 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28860_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28860 (InternalEnumerator_1_t4758 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28861 (InternalEnumerator_1_t4758 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAxis_t1027_m38358(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAxis_t1027_m38358_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4758____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4758, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4758____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4758, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4758_FieldInfos[] =
{
	&InternalEnumerator_1_t4758____array_0_FieldInfo,
	&InternalEnumerator_1_t4758____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4758____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4758_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4758____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4758_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28861_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4758_PropertyInfos[] =
{
	&InternalEnumerator_1_t4758____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4758____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4758_InternalEnumerator_1__ctor_m28857_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28857_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28857_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28857/* method */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4758_InternalEnumerator_1__ctor_m28857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28857_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858/* method */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28859_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28859_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28859/* method */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28859_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28860_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28860_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28860/* method */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28860_GenericMethod/* genericMethod */

};
extern Il2CppType Axis_t1027_0_0_0;
extern void* RuntimeInvoker_Axis_t1027 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28861_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28861_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28861/* method */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t1027_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t1027/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28861_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4758_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28857_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_MethodInfo,
	&InternalEnumerator_1_Dispose_m28859_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28860_MethodInfo,
	&InternalEnumerator_1_get_Current_m28861_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4758_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28860_MethodInfo,
	&InternalEnumerator_1_Dispose_m28859_MethodInfo,
	&InternalEnumerator_1_get_Current_m28861_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4758_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6846_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4758_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6846_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4758_0_0_0;
extern Il2CppType InternalEnumerator_1_t4758_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4758_GenericClass;
TypeInfo InternalEnumerator_1_t4758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4758_MethodInfos/* methods */
	, InternalEnumerator_1_t4758_PropertyInfos/* properties */
	, InternalEnumerator_1_t4758_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4758_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4758_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4758_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4758_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4758_1_0_0/* this_arg */
	, InternalEnumerator_1_t4758_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4758_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4758)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8679_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>
extern MethodInfo ICollection_1_get_Count_m48905_MethodInfo;
static PropertyInfo ICollection_1_t8679____Count_PropertyInfo = 
{
	&ICollection_1_t8679_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48905_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48906_MethodInfo;
static PropertyInfo ICollection_1_t8679____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8679_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48906_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8679_PropertyInfos[] =
{
	&ICollection_1_t8679____Count_PropertyInfo,
	&ICollection_1_t8679____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48905_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::get_Count()
MethodInfo ICollection_1_get_Count_m48905_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48905_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48906_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48906_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48906_GenericMethod/* genericMethod */

};
extern Il2CppType Axis_t1027_0_0_0;
extern Il2CppType Axis_t1027_0_0_0;
static ParameterInfo ICollection_1_t8679_ICollection_1_Add_m48907_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Axis_t1027_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48907_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Add(T)
MethodInfo ICollection_1_Add_m48907_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8679_ICollection_1_Add_m48907_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48907_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48908_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Clear()
MethodInfo ICollection_1_Clear_m48908_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48908_GenericMethod/* genericMethod */

};
extern Il2CppType Axis_t1027_0_0_0;
static ParameterInfo ICollection_1_t8679_ICollection_1_Contains_m48909_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Axis_t1027_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48909_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Contains(T)
MethodInfo ICollection_1_Contains_m48909_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8679_ICollection_1_Contains_m48909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48909_GenericMethod/* genericMethod */

};
extern Il2CppType AxisU5BU5D_t5707_0_0_0;
extern Il2CppType AxisU5BU5D_t5707_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8679_ICollection_1_CopyTo_m48910_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AxisU5BU5D_t5707_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48910_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48910_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8679_ICollection_1_CopyTo_m48910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48910_GenericMethod/* genericMethod */

};
extern Il2CppType Axis_t1027_0_0_0;
static ParameterInfo ICollection_1_t8679_ICollection_1_Remove_m48911_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Axis_t1027_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48911_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RectTransform/Axis>::Remove(T)
MethodInfo ICollection_1_Remove_m48911_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8679_ICollection_1_Remove_m48911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48911_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8679_MethodInfos[] =
{
	&ICollection_1_get_Count_m48905_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48906_MethodInfo,
	&ICollection_1_Add_m48907_MethodInfo,
	&ICollection_1_Clear_m48908_MethodInfo,
	&ICollection_1_Contains_m48909_MethodInfo,
	&ICollection_1_CopyTo_m48910_MethodInfo,
	&ICollection_1_Remove_m48911_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8681_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8679_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8681_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8679_0_0_0;
extern Il2CppType ICollection_1_t8679_1_0_0;
struct ICollection_1_t8679;
extern Il2CppGenericClass ICollection_1_t8679_GenericClass;
TypeInfo ICollection_1_t8679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8679_MethodInfos/* methods */
	, ICollection_1_t8679_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8679_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8679_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8679_0_0_0/* byval_arg */
	, &ICollection_1_t8679_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8679_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Axis>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Axis>
extern Il2CppType IEnumerator_1_t6846_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48912_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform/Axis>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48912_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8681_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6846_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48912_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8681_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48912_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8681_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8681_0_0_0;
extern Il2CppType IEnumerable_1_t8681_1_0_0;
struct IEnumerable_1_t8681;
extern Il2CppGenericClass IEnumerable_1_t8681_GenericClass;
TypeInfo IEnumerable_1_t8681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8681_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8681_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8681_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8681_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8681_0_0_0/* byval_arg */
	, &IEnumerable_1_t8681_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8681_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8680_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>
extern MethodInfo IList_1_get_Item_m48913_MethodInfo;
extern MethodInfo IList_1_set_Item_m48914_MethodInfo;
static PropertyInfo IList_1_t8680____Item_PropertyInfo = 
{
	&IList_1_t8680_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48913_MethodInfo/* get */
	, &IList_1_set_Item_m48914_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8680_PropertyInfos[] =
{
	&IList_1_t8680____Item_PropertyInfo,
	NULL
};
extern Il2CppType Axis_t1027_0_0_0;
static ParameterInfo IList_1_t8680_IList_1_IndexOf_m48915_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Axis_t1027_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48915_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48915_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8680_IList_1_IndexOf_m48915_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48915_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Axis_t1027_0_0_0;
static ParameterInfo IList_1_t8680_IList_1_Insert_m48916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Axis_t1027_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48916_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48916_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8680_IList_1_Insert_m48916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48916_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8680_IList_1_RemoveAt_m48917_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48917_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48917_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8680_IList_1_RemoveAt_m48917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48917_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8680_IList_1_get_Item_m48913_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Axis_t1027_0_0_0;
extern void* RuntimeInvoker_Axis_t1027_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48913_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48913_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t1027_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t1027_Int32_t123/* invoker_method */
	, IList_1_t8680_IList_1_get_Item_m48913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48913_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Axis_t1027_0_0_0;
static ParameterInfo IList_1_t8680_IList_1_set_Item_m48914_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Axis_t1027_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48914_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RectTransform/Axis>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48914_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8680_IList_1_set_Item_m48914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48914_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8680_MethodInfos[] =
{
	&IList_1_IndexOf_m48915_MethodInfo,
	&IList_1_Insert_m48916_MethodInfo,
	&IList_1_RemoveAt_m48917_MethodInfo,
	&IList_1_get_Item_m48913_MethodInfo,
	&IList_1_set_Item_m48914_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8680_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8679_il2cpp_TypeInfo,
	&IEnumerable_1_t8681_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8680_0_0_0;
extern Il2CppType IList_1_t8680_1_0_0;
struct IList_1_t8680;
extern Il2CppGenericClass IList_1_t8680_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8680_MethodInfos/* methods */
	, IList_1_t8680_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8680_il2cpp_TypeInfo/* element_class */
	, IList_1_t8680_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8680_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8680_0_0_0/* byval_arg */
	, &IList_1_t8680_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8680_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6848_il2cpp_TypeInfo;

// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SerializePrivateVariables>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SerializePrivateVariables>
extern MethodInfo IEnumerator_1_get_Current_m48918_MethodInfo;
static PropertyInfo IEnumerator_1_t6848____Current_PropertyInfo = 
{
	&IEnumerator_1_t6848_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48918_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6848_PropertyInfos[] =
{
	&IEnumerator_1_t6848____Current_PropertyInfo,
	NULL
};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48918_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SerializePrivateVariables>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48918_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6848_il2cpp_TypeInfo/* declaring_type */
	, &SerializePrivateVariables_t1029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48918_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6848_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48918_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6848_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6848_0_0_0;
extern Il2CppType IEnumerator_1_t6848_1_0_0;
struct IEnumerator_1_t6848;
extern Il2CppGenericClass IEnumerator_1_t6848_GenericClass;
TypeInfo IEnumerator_1_t6848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6848_MethodInfos/* methods */
	, IEnumerator_1_t6848_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6848_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6848_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6848_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6848_0_0_0/* byval_arg */
	, &IEnumerator_1_t6848_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6848_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_437.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4759_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_437MethodDeclarations.h"

extern TypeInfo SerializePrivateVariables_t1029_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28866_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSerializePrivateVariables_t1029_m38369_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SerializePrivateVariables>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SerializePrivateVariables>(System.Int32)
#define Array_InternalArray__get_Item_TisSerializePrivateVariables_t1029_m38369(__this, p0, method) (SerializePrivateVariables_t1029 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4759____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4759, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4759____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4759, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4759_FieldInfos[] =
{
	&InternalEnumerator_1_t4759____array_0_FieldInfo,
	&InternalEnumerator_1_t4759____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4759____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4759_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4759____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4759_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4759_PropertyInfos[] =
{
	&InternalEnumerator_1_t4759____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4759____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4759_InternalEnumerator_1__ctor_m28862_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28862_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28862_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4759_InternalEnumerator_1__ctor_m28862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28862_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28864_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28864_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28864_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28865_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28865_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28865_GenericMethod/* genericMethod */

};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28866_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SerializePrivateVariables>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28866_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* declaring_type */
	, &SerializePrivateVariables_t1029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28866_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4759_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28862_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_MethodInfo,
	&InternalEnumerator_1_Dispose_m28864_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28865_MethodInfo,
	&InternalEnumerator_1_get_Current_m28866_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28865_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28864_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4759_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28863_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28865_MethodInfo,
	&InternalEnumerator_1_Dispose_m28864_MethodInfo,
	&InternalEnumerator_1_get_Current_m28866_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4759_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6848_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4759_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6848_il2cpp_TypeInfo, 7},
};
extern TypeInfo SerializePrivateVariables_t1029_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4759_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28866_MethodInfo/* Method Usage */,
	&SerializePrivateVariables_t1029_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSerializePrivateVariables_t1029_m38369_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4759_0_0_0;
extern Il2CppType InternalEnumerator_1_t4759_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4759_GenericClass;
TypeInfo InternalEnumerator_1_t4759_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4759_MethodInfos/* methods */
	, InternalEnumerator_1_t4759_PropertyInfos/* properties */
	, InternalEnumerator_1_t4759_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4759_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4759_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4759_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4759_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4759_1_0_0/* this_arg */
	, InternalEnumerator_1_t4759_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4759_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4759_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4759)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8682_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>
extern MethodInfo ICollection_1_get_Count_m48919_MethodInfo;
static PropertyInfo ICollection_1_t8682____Count_PropertyInfo = 
{
	&ICollection_1_t8682_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48919_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48920_MethodInfo;
static PropertyInfo ICollection_1_t8682____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8682_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48920_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8682_PropertyInfos[] =
{
	&ICollection_1_t8682____Count_PropertyInfo,
	&ICollection_1_t8682____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48919_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::get_Count()
MethodInfo ICollection_1_get_Count_m48919_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48919_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48920_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48920_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48920_GenericMethod/* genericMethod */

};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
static ParameterInfo ICollection_1_t8682_ICollection_1_Add_m48921_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariables_t1029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48921_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Add(T)
MethodInfo ICollection_1_Add_m48921_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8682_ICollection_1_Add_m48921_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48921_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48922_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Clear()
MethodInfo ICollection_1_Clear_m48922_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48922_GenericMethod/* genericMethod */

};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
static ParameterInfo ICollection_1_t8682_ICollection_1_Contains_m48923_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariables_t1029_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48923_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Contains(T)
MethodInfo ICollection_1_Contains_m48923_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8682_ICollection_1_Contains_m48923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48923_GenericMethod/* genericMethod */

};
extern Il2CppType SerializePrivateVariablesU5BU5D_t5708_0_0_0;
extern Il2CppType SerializePrivateVariablesU5BU5D_t5708_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8682_ICollection_1_CopyTo_m48924_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariablesU5BU5D_t5708_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48924_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48924_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8682_ICollection_1_CopyTo_m48924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48924_GenericMethod/* genericMethod */

};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
static ParameterInfo ICollection_1_t8682_ICollection_1_Remove_m48925_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariables_t1029_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48925_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializePrivateVariables>::Remove(T)
MethodInfo ICollection_1_Remove_m48925_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8682_ICollection_1_Remove_m48925_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48925_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8682_MethodInfos[] =
{
	&ICollection_1_get_Count_m48919_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48920_MethodInfo,
	&ICollection_1_Add_m48921_MethodInfo,
	&ICollection_1_Clear_m48922_MethodInfo,
	&ICollection_1_Contains_m48923_MethodInfo,
	&ICollection_1_CopyTo_m48924_MethodInfo,
	&ICollection_1_Remove_m48925_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8684_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8682_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8684_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8682_0_0_0;
extern Il2CppType ICollection_1_t8682_1_0_0;
struct ICollection_1_t8682;
extern Il2CppGenericClass ICollection_1_t8682_GenericClass;
TypeInfo ICollection_1_t8682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8682_MethodInfos/* methods */
	, ICollection_1_t8682_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8682_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8682_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8682_0_0_0/* byval_arg */
	, &ICollection_1_t8682_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8682_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SerializePrivateVariables>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SerializePrivateVariables>
extern Il2CppType IEnumerator_1_t6848_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48926_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SerializePrivateVariables>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48926_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8684_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6848_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48926_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8684_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48926_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8684_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8684_0_0_0;
extern Il2CppType IEnumerable_1_t8684_1_0_0;
struct IEnumerable_1_t8684;
extern Il2CppGenericClass IEnumerable_1_t8684_GenericClass;
TypeInfo IEnumerable_1_t8684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8684_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8684_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8684_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8684_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8684_0_0_0/* byval_arg */
	, &IEnumerable_1_t8684_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8684_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8683_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>
extern MethodInfo IList_1_get_Item_m48927_MethodInfo;
extern MethodInfo IList_1_set_Item_m48928_MethodInfo;
static PropertyInfo IList_1_t8683____Item_PropertyInfo = 
{
	&IList_1_t8683_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48927_MethodInfo/* get */
	, &IList_1_set_Item_m48928_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8683_PropertyInfos[] =
{
	&IList_1_t8683____Item_PropertyInfo,
	NULL
};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
static ParameterInfo IList_1_t8683_IList_1_IndexOf_m48929_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariables_t1029_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48929_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48929_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8683_IList_1_IndexOf_m48929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48929_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
static ParameterInfo IList_1_t8683_IList_1_Insert_m48930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariables_t1029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48930_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48930_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8683_IList_1_Insert_m48930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48930_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8683_IList_1_RemoveAt_m48931_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48931_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48931_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8683_IList_1_RemoveAt_m48931_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48931_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8683_IList_1_get_Item_m48927_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48927_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48927_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &SerializePrivateVariables_t1029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8683_IList_1_get_Item_m48927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48927_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SerializePrivateVariables_t1029_0_0_0;
static ParameterInfo IList_1_t8683_IList_1_set_Item_m48928_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SerializePrivateVariables_t1029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48928_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializePrivateVariables>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48928_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8683_IList_1_set_Item_m48928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48928_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8683_MethodInfos[] =
{
	&IList_1_IndexOf_m48929_MethodInfo,
	&IList_1_Insert_m48930_MethodInfo,
	&IList_1_RemoveAt_m48931_MethodInfo,
	&IList_1_get_Item_m48927_MethodInfo,
	&IList_1_set_Item_m48928_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8683_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8682_il2cpp_TypeInfo,
	&IEnumerable_1_t8684_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8683_0_0_0;
extern Il2CppType IList_1_t8683_1_0_0;
struct IList_1_t8683;
extern Il2CppGenericClass IList_1_t8683_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8683_MethodInfos/* methods */
	, IList_1_t8683_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8683_il2cpp_TypeInfo/* element_class */
	, IList_1_t8683_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8683_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8683_0_0_0/* byval_arg */
	, &IList_1_t8683_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8683_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6850_il2cpp_TypeInfo;

// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SerializeField>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SerializeField>
extern MethodInfo IEnumerator_1_get_Current_m48932_MethodInfo;
static PropertyInfo IEnumerator_1_t6850____Current_PropertyInfo = 
{
	&IEnumerator_1_t6850_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6850_PropertyInfos[] =
{
	&IEnumerator_1_t6850____Current_PropertyInfo,
	NULL
};
extern Il2CppType SerializeField_t469_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48932_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SerializeField>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48932_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6850_il2cpp_TypeInfo/* declaring_type */
	, &SerializeField_t469_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48932_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6850_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48932_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6850_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6850_0_0_0;
extern Il2CppType IEnumerator_1_t6850_1_0_0;
struct IEnumerator_1_t6850;
extern Il2CppGenericClass IEnumerator_1_t6850_GenericClass;
TypeInfo IEnumerator_1_t6850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6850_MethodInfos/* methods */
	, IEnumerator_1_t6850_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6850_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6850_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6850_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6850_0_0_0/* byval_arg */
	, &IEnumerator_1_t6850_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6850_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SerializeField>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_438.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4760_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SerializeField>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_438MethodDeclarations.h"

extern TypeInfo SerializeField_t469_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28871_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSerializeField_t469_m38380_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SerializeField>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SerializeField>(System.Int32)
#define Array_InternalArray__get_Item_TisSerializeField_t469_m38380(__this, p0, method) (SerializeField_t469 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SerializeField>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4760____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4760, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4760____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4760, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4760_FieldInfos[] =
{
	&InternalEnumerator_1_t4760____array_0_FieldInfo,
	&InternalEnumerator_1_t4760____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4760____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4760_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4760____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4760_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4760_PropertyInfos[] =
{
	&InternalEnumerator_1_t4760____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4760____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4760_InternalEnumerator_1__ctor_m28867_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28867_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28867_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4760_InternalEnumerator_1__ctor_m28867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28867_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28869_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28869_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28869_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28870_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28870_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28870_GenericMethod/* genericMethod */

};
extern Il2CppType SerializeField_t469_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28871_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SerializeField>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28871_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* declaring_type */
	, &SerializeField_t469_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28871_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4760_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28867_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_MethodInfo,
	&InternalEnumerator_1_Dispose_m28869_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28870_MethodInfo,
	&InternalEnumerator_1_get_Current_m28871_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28870_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28869_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4760_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28868_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28870_MethodInfo,
	&InternalEnumerator_1_Dispose_m28869_MethodInfo,
	&InternalEnumerator_1_get_Current_m28871_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4760_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6850_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4760_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6850_il2cpp_TypeInfo, 7},
};
extern TypeInfo SerializeField_t469_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4760_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28871_MethodInfo/* Method Usage */,
	&SerializeField_t469_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSerializeField_t469_m38380_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4760_0_0_0;
extern Il2CppType InternalEnumerator_1_t4760_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4760_GenericClass;
TypeInfo InternalEnumerator_1_t4760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4760_MethodInfos/* methods */
	, InternalEnumerator_1_t4760_PropertyInfos/* properties */
	, InternalEnumerator_1_t4760_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4760_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4760_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4760_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4760_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4760_1_0_0/* this_arg */
	, InternalEnumerator_1_t4760_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4760_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4760_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4760)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8685_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>
extern MethodInfo ICollection_1_get_Count_m48933_MethodInfo;
static PropertyInfo ICollection_1_t8685____Count_PropertyInfo = 
{
	&ICollection_1_t8685_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48933_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48934_MethodInfo;
static PropertyInfo ICollection_1_t8685____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8685_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48934_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8685_PropertyInfos[] =
{
	&ICollection_1_t8685____Count_PropertyInfo,
	&ICollection_1_t8685____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48933_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::get_Count()
MethodInfo ICollection_1_get_Count_m48933_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48933_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48934_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48934_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48934_GenericMethod/* genericMethod */

};
extern Il2CppType SerializeField_t469_0_0_0;
extern Il2CppType SerializeField_t469_0_0_0;
static ParameterInfo ICollection_1_t8685_ICollection_1_Add_m48935_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializeField_t469_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48935_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Add(T)
MethodInfo ICollection_1_Add_m48935_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8685_ICollection_1_Add_m48935_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48935_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48936_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Clear()
MethodInfo ICollection_1_Clear_m48936_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48936_GenericMethod/* genericMethod */

};
extern Il2CppType SerializeField_t469_0_0_0;
static ParameterInfo ICollection_1_t8685_ICollection_1_Contains_m48937_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializeField_t469_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48937_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Contains(T)
MethodInfo ICollection_1_Contains_m48937_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8685_ICollection_1_Contains_m48937_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48937_GenericMethod/* genericMethod */

};
extern Il2CppType SerializeFieldU5BU5D_t5709_0_0_0;
extern Il2CppType SerializeFieldU5BU5D_t5709_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8685_ICollection_1_CopyTo_m48938_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SerializeFieldU5BU5D_t5709_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48938_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48938_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8685_ICollection_1_CopyTo_m48938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48938_GenericMethod/* genericMethod */

};
extern Il2CppType SerializeField_t469_0_0_0;
static ParameterInfo ICollection_1_t8685_ICollection_1_Remove_m48939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializeField_t469_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48939_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SerializeField>::Remove(T)
MethodInfo ICollection_1_Remove_m48939_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8685_ICollection_1_Remove_m48939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48939_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8685_MethodInfos[] =
{
	&ICollection_1_get_Count_m48933_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48934_MethodInfo,
	&ICollection_1_Add_m48935_MethodInfo,
	&ICollection_1_Clear_m48936_MethodInfo,
	&ICollection_1_Contains_m48937_MethodInfo,
	&ICollection_1_CopyTo_m48938_MethodInfo,
	&ICollection_1_Remove_m48939_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8687_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8685_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8687_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8685_0_0_0;
extern Il2CppType ICollection_1_t8685_1_0_0;
struct ICollection_1_t8685;
extern Il2CppGenericClass ICollection_1_t8685_GenericClass;
TypeInfo ICollection_1_t8685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8685_MethodInfos/* methods */
	, ICollection_1_t8685_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8685_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8685_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8685_0_0_0/* byval_arg */
	, &ICollection_1_t8685_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8685_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SerializeField>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SerializeField>
extern Il2CppType IEnumerator_1_t6850_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48940_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SerializeField>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48940_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8687_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6850_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48940_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8687_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48940_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8687_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8687_0_0_0;
extern Il2CppType IEnumerable_1_t8687_1_0_0;
struct IEnumerable_1_t8687;
extern Il2CppGenericClass IEnumerable_1_t8687_GenericClass;
TypeInfo IEnumerable_1_t8687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8687_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8687_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8687_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8687_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8687_0_0_0/* byval_arg */
	, &IEnumerable_1_t8687_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8687_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8686_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SerializeField>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializeField>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializeField>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SerializeField>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializeField>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SerializeField>
extern MethodInfo IList_1_get_Item_m48941_MethodInfo;
extern MethodInfo IList_1_set_Item_m48942_MethodInfo;
static PropertyInfo IList_1_t8686____Item_PropertyInfo = 
{
	&IList_1_t8686_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48941_MethodInfo/* get */
	, &IList_1_set_Item_m48942_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8686_PropertyInfos[] =
{
	&IList_1_t8686____Item_PropertyInfo,
	NULL
};
extern Il2CppType SerializeField_t469_0_0_0;
static ParameterInfo IList_1_t8686_IList_1_IndexOf_m48943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializeField_t469_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48943_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SerializeField>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48943_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8686_IList_1_IndexOf_m48943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48943_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SerializeField_t469_0_0_0;
static ParameterInfo IList_1_t8686_IList_1_Insert_m48944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SerializeField_t469_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48944_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializeField>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48944_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8686_IList_1_Insert_m48944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48944_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8686_IList_1_RemoveAt_m48945_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48945_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializeField>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48945_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8686_IList_1_RemoveAt_m48945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48945_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8686_IList_1_get_Item_m48941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SerializeField_t469_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48941_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SerializeField>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48941_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &SerializeField_t469_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8686_IList_1_get_Item_m48941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48941_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SerializeField_t469_0_0_0;
static ParameterInfo IList_1_t8686_IList_1_set_Item_m48942_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SerializeField_t469_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48942_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SerializeField>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48942_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8686_IList_1_set_Item_m48942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48942_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8686_MethodInfos[] =
{
	&IList_1_IndexOf_m48943_MethodInfo,
	&IList_1_Insert_m48944_MethodInfo,
	&IList_1_RemoveAt_m48945_MethodInfo,
	&IList_1_get_Item_m48941_MethodInfo,
	&IList_1_set_Item_m48942_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8686_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8685_il2cpp_TypeInfo,
	&IEnumerable_1_t8687_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8686_0_0_0;
extern Il2CppType IList_1_t8686_1_0_0;
struct IList_1_t8686;
extern Il2CppGenericClass IList_1_t8686_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8686_MethodInfos/* methods */
	, IList_1_t8686_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8686_il2cpp_TypeInfo/* element_class */
	, IList_1_t8686_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8686_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8686_0_0_0/* byval_arg */
	, &IList_1_t8686_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8686_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6852_il2cpp_TypeInfo;

// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Shader>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Shader>
extern MethodInfo IEnumerator_1_get_Current_m48946_MethodInfo;
static PropertyInfo IEnumerator_1_t6852____Current_PropertyInfo = 
{
	&IEnumerator_1_t6852_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48946_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6852_PropertyInfos[] =
{
	&IEnumerator_1_t6852____Current_PropertyInfo,
	NULL
};
extern Il2CppType Shader_t173_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48946_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Shader>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48946_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6852_il2cpp_TypeInfo/* declaring_type */
	, &Shader_t173_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48946_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6852_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48946_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6852_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6852_0_0_0;
extern Il2CppType IEnumerator_1_t6852_1_0_0;
struct IEnumerator_1_t6852;
extern Il2CppGenericClass IEnumerator_1_t6852_GenericClass;
TypeInfo IEnumerator_1_t6852_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6852_MethodInfos/* methods */
	, IEnumerator_1_t6852_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6852_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6852_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6852_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6852_0_0_0/* byval_arg */
	, &IEnumerator_1_t6852_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6852_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Shader>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_439.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4761_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Shader>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_439MethodDeclarations.h"

extern TypeInfo Shader_t173_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28876_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisShader_t173_m38391_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Shader>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Shader>(System.Int32)
#define Array_InternalArray__get_Item_TisShader_t173_m38391(__this, p0, method) (Shader_t173 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Shader>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Shader>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Shader>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Shader>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Shader>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Shader>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4761____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4761, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4761____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4761, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4761_FieldInfos[] =
{
	&InternalEnumerator_1_t4761____array_0_FieldInfo,
	&InternalEnumerator_1_t4761____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4761____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4761_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4761____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4761_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28876_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4761_PropertyInfos[] =
{
	&InternalEnumerator_1_t4761____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4761____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4761_InternalEnumerator_1__ctor_m28872_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28872_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Shader>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28872_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4761_InternalEnumerator_1__ctor_m28872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28872_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Shader>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28874_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Shader>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28874_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28874_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28875_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Shader>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28875_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28875_GenericMethod/* genericMethod */

};
extern Il2CppType Shader_t173_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28876_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Shader>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28876_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* declaring_type */
	, &Shader_t173_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28876_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4761_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28872_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_MethodInfo,
	&InternalEnumerator_1_Dispose_m28874_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28875_MethodInfo,
	&InternalEnumerator_1_get_Current_m28876_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28875_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28874_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4761_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28873_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28875_MethodInfo,
	&InternalEnumerator_1_Dispose_m28874_MethodInfo,
	&InternalEnumerator_1_get_Current_m28876_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4761_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6852_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4761_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6852_il2cpp_TypeInfo, 7},
};
extern TypeInfo Shader_t173_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4761_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28876_MethodInfo/* Method Usage */,
	&Shader_t173_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisShader_t173_m38391_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4761_0_0_0;
extern Il2CppType InternalEnumerator_1_t4761_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4761_GenericClass;
TypeInfo InternalEnumerator_1_t4761_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4761_MethodInfos/* methods */
	, InternalEnumerator_1_t4761_PropertyInfos/* properties */
	, InternalEnumerator_1_t4761_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4761_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4761_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4761_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4761_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4761_1_0_0/* this_arg */
	, InternalEnumerator_1_t4761_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4761_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4761_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4761)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8688_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Shader>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Shader>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Shader>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Shader>
extern MethodInfo ICollection_1_get_Count_m48947_MethodInfo;
static PropertyInfo ICollection_1_t8688____Count_PropertyInfo = 
{
	&ICollection_1_t8688_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48948_MethodInfo;
static PropertyInfo ICollection_1_t8688____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8688_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48948_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8688_PropertyInfos[] =
{
	&ICollection_1_t8688____Count_PropertyInfo,
	&ICollection_1_t8688____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48947_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Shader>::get_Count()
MethodInfo ICollection_1_get_Count_m48947_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48947_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48948_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Shader>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48948_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48948_GenericMethod/* genericMethod */

};
extern Il2CppType Shader_t173_0_0_0;
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo ICollection_1_t8688_ICollection_1_Add_m48949_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48949_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Add(T)
MethodInfo ICollection_1_Add_m48949_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8688_ICollection_1_Add_m48949_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48949_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48950_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Clear()
MethodInfo ICollection_1_Clear_m48950_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48950_GenericMethod/* genericMethod */

};
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo ICollection_1_t8688_ICollection_1_Contains_m48951_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48951_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Contains(T)
MethodInfo ICollection_1_Contains_m48951_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8688_ICollection_1_Contains_m48951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48951_GenericMethod/* genericMethod */

};
extern Il2CppType ShaderU5BU5D_t5710_0_0_0;
extern Il2CppType ShaderU5BU5D_t5710_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8688_ICollection_1_CopyTo_m48952_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ShaderU5BU5D_t5710_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48952_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Shader>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48952_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8688_ICollection_1_CopyTo_m48952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48952_GenericMethod/* genericMethod */

};
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo ICollection_1_t8688_ICollection_1_Remove_m48953_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48953_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Shader>::Remove(T)
MethodInfo ICollection_1_Remove_m48953_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8688_ICollection_1_Remove_m48953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48953_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8688_MethodInfos[] =
{
	&ICollection_1_get_Count_m48947_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48948_MethodInfo,
	&ICollection_1_Add_m48949_MethodInfo,
	&ICollection_1_Clear_m48950_MethodInfo,
	&ICollection_1_Contains_m48951_MethodInfo,
	&ICollection_1_CopyTo_m48952_MethodInfo,
	&ICollection_1_Remove_m48953_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8690_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8688_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8690_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8688_0_0_0;
extern Il2CppType ICollection_1_t8688_1_0_0;
struct ICollection_1_t8688;
extern Il2CppGenericClass ICollection_1_t8688_GenericClass;
TypeInfo ICollection_1_t8688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8688_MethodInfos/* methods */
	, ICollection_1_t8688_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8688_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8688_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8688_0_0_0/* byval_arg */
	, &ICollection_1_t8688_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8688_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Shader>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Shader>
extern Il2CppType IEnumerator_1_t6852_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48954_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Shader>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48954_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8690_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6852_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48954_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8690_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48954_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8690_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8690_0_0_0;
extern Il2CppType IEnumerable_1_t8690_1_0_0;
struct IEnumerable_1_t8690;
extern Il2CppGenericClass IEnumerable_1_t8690_GenericClass;
TypeInfo IEnumerable_1_t8690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8690_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8690_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8690_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8690_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8690_0_0_0/* byval_arg */
	, &IEnumerable_1_t8690_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8690_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8689_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Shader>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Shader>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Shader>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Shader>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Shader>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Shader>
extern MethodInfo IList_1_get_Item_m48955_MethodInfo;
extern MethodInfo IList_1_set_Item_m48956_MethodInfo;
static PropertyInfo IList_1_t8689____Item_PropertyInfo = 
{
	&IList_1_t8689_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48955_MethodInfo/* get */
	, &IList_1_set_Item_m48956_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8689_PropertyInfos[] =
{
	&IList_1_t8689____Item_PropertyInfo,
	NULL
};
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo IList_1_t8689_IList_1_IndexOf_m48957_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48957_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Shader>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48957_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8689_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8689_IList_1_IndexOf_m48957_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48957_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo IList_1_t8689_IList_1_Insert_m48958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48958_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Shader>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48958_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8689_IList_1_Insert_m48958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48958_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8689_IList_1_RemoveAt_m48959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48959_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Shader>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48959_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8689_IList_1_RemoveAt_m48959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48959_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8689_IList_1_get_Item_m48955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Shader_t173_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48955_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Shader>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48955_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8689_il2cpp_TypeInfo/* declaring_type */
	, &Shader_t173_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8689_IList_1_get_Item_m48955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48955_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo IList_1_t8689_IList_1_set_Item_m48956_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48956_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Shader>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48956_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8689_IList_1_set_Item_m48956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48956_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8689_MethodInfos[] =
{
	&IList_1_IndexOf_m48957_MethodInfo,
	&IList_1_Insert_m48958_MethodInfo,
	&IList_1_RemoveAt_m48959_MethodInfo,
	&IList_1_get_Item_m48955_MethodInfo,
	&IList_1_set_Item_m48956_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8689_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8688_il2cpp_TypeInfo,
	&IEnumerable_1_t8690_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8689_0_0_0;
extern Il2CppType IList_1_t8689_1_0_0;
struct IList_1_t8689;
extern Il2CppGenericClass IList_1_t8689_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8689_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8689_MethodInfos/* methods */
	, IList_1_t8689_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8689_il2cpp_TypeInfo/* element_class */
	, IList_1_t8689_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8689_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8689_0_0_0/* byval_arg */
	, &IList_1_t8689_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8689_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_151.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4762_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_151MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_153.h"
extern TypeInfo InvokableCall_1_t4763_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_153MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28879_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28881_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4762____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4762_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4762, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4762_FieldInfos[] =
{
	&CachedInvokableCall_1_t4762____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4762_CachedInvokableCall_1__ctor_m28877_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28877_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28877_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4762_CachedInvokableCall_1__ctor_m28877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28877_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4762_CachedInvokableCall_1_Invoke_m28878_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28878_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28878_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4762_CachedInvokableCall_1_Invoke_m28878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28878_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4762_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28877_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28878_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28878_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28882_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4762_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28878_MethodInfo,
	&InvokableCall_1_Find_m28882_MethodInfo,
};
extern Il2CppType UnityAction_1_t4764_0_0_0;
extern TypeInfo UnityAction_1_t4764_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisShader_t173_m38401_MethodInfo;
extern TypeInfo Shader_t173_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28884_MethodInfo;
extern TypeInfo Shader_t173_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4762_RGCTXData[8] = 
{
	&UnityAction_1_t4764_0_0_0/* Type Usage */,
	&UnityAction_1_t4764_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisShader_t173_m38401_MethodInfo/* Method Usage */,
	&Shader_t173_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28884_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28879_MethodInfo/* Method Usage */,
	&Shader_t173_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28881_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4762_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4762_1_0_0;
struct CachedInvokableCall_1_t4762;
extern Il2CppGenericClass CachedInvokableCall_1_t4762_GenericClass;
TypeInfo CachedInvokableCall_1_t4762_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4762_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4762_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4762_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4762_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4762_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4762_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4762_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4762_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4762_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4762)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_158.h"
extern TypeInfo UnityAction_1_t4764_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_158MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Shader>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Shader>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisShader_t173_m38401(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>
extern Il2CppType UnityAction_1_t4764_0_0_1;
FieldInfo InvokableCall_1_t4763____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4764_0_0_1/* type */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4763, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4763_FieldInfos[] =
{
	&InvokableCall_1_t4763____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4763_InvokableCall_1__ctor_m28879_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28879_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28879_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4763_InvokableCall_1__ctor_m28879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28879_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4764_0_0_0;
static ParameterInfo InvokableCall_1_t4763_InvokableCall_1__ctor_m28880_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4764_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28880_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28880_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4763_InvokableCall_1__ctor_m28880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28880_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4763_InvokableCall_1_Invoke_m28881_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28881_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28881_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4763_InvokableCall_1_Invoke_m28881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28881_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4763_InvokableCall_1_Find_m28882_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28882_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28882_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4763_InvokableCall_1_Find_m28882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28882_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4763_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28879_MethodInfo,
	&InvokableCall_1__ctor_m28880_MethodInfo,
	&InvokableCall_1_Invoke_m28881_MethodInfo,
	&InvokableCall_1_Find_m28882_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4763_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28881_MethodInfo,
	&InvokableCall_1_Find_m28882_MethodInfo,
};
extern TypeInfo UnityAction_1_t4764_il2cpp_TypeInfo;
extern TypeInfo Shader_t173_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4763_RGCTXData[5] = 
{
	&UnityAction_1_t4764_0_0_0/* Type Usage */,
	&UnityAction_1_t4764_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisShader_t173_m38401_MethodInfo/* Method Usage */,
	&Shader_t173_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28884_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4763_0_0_0;
extern Il2CppType InvokableCall_1_t4763_1_0_0;
struct InvokableCall_1_t4763;
extern Il2CppGenericClass InvokableCall_1_t4763_GenericClass;
TypeInfo InvokableCall_1_t4763_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4763_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4763_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4763_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4763_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4763_0_0_0/* byval_arg */
	, &InvokableCall_1_t4763_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4763_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4763)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Shader>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4764_UnityAction_1__ctor_m28883_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28883_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28883_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4764_UnityAction_1__ctor_m28883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28883_GenericMethod/* genericMethod */

};
extern Il2CppType Shader_t173_0_0_0;
static ParameterInfo UnityAction_1_t4764_UnityAction_1_Invoke_m28884_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28884_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28884_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4764_UnityAction_1_Invoke_m28884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28884_GenericMethod/* genericMethod */

};
extern Il2CppType Shader_t173_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4764_UnityAction_1_BeginInvoke_m28885_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Shader_t173_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28885_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28885_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4764_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4764_UnityAction_1_BeginInvoke_m28885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28885_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4764_UnityAction_1_EndInvoke_m28886_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28886_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Shader>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28886_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4764_UnityAction_1_EndInvoke_m28886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28886_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4764_MethodInfos[] =
{
	&UnityAction_1__ctor_m28883_MethodInfo,
	&UnityAction_1_Invoke_m28884_MethodInfo,
	&UnityAction_1_BeginInvoke_m28885_MethodInfo,
	&UnityAction_1_EndInvoke_m28886_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28885_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28886_MethodInfo;
static MethodInfo* UnityAction_1_t4764_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28884_MethodInfo,
	&UnityAction_1_BeginInvoke_m28885_MethodInfo,
	&UnityAction_1_EndInvoke_m28886_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4764_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4764_1_0_0;
struct UnityAction_1_t4764;
extern Il2CppGenericClass UnityAction_1_t4764_GenericClass;
TypeInfo UnityAction_1_t4764_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4764_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4764_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4764_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4764_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4764_0_0_0/* byval_arg */
	, &UnityAction_1_t4764_1_0_0/* this_arg */
	, UnityAction_1_t4764_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4764_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4764)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_152.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4765_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_152MethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_154.h"
extern TypeInfo Material_t64_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4766_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_154MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28889_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28891_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4765____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4765_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4765, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4765_FieldInfos[] =
{
	&CachedInvokableCall_1_t4765____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Material_t64_0_0_0;
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4765_CachedInvokableCall_1__ctor_m28887_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28887_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28887_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4765_CachedInvokableCall_1__ctor_m28887_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28887_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4765_CachedInvokableCall_1_Invoke_m28888_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28888_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Material>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28888_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4765_CachedInvokableCall_1_Invoke_m28888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28888_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4765_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28887_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28888_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28888_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28892_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4765_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28888_MethodInfo,
	&InvokableCall_1_Find_m28892_MethodInfo,
};
extern Il2CppType UnityAction_1_t4767_0_0_0;
extern TypeInfo UnityAction_1_t4767_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMaterial_t64_m38402_MethodInfo;
extern TypeInfo Material_t64_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28894_MethodInfo;
extern TypeInfo Material_t64_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4765_RGCTXData[8] = 
{
	&UnityAction_1_t4767_0_0_0/* Type Usage */,
	&UnityAction_1_t4767_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMaterial_t64_m38402_MethodInfo/* Method Usage */,
	&Material_t64_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28894_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28889_MethodInfo/* Method Usage */,
	&Material_t64_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28891_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4765_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4765_1_0_0;
struct CachedInvokableCall_1_t4765;
extern Il2CppGenericClass CachedInvokableCall_1_t4765_GenericClass;
TypeInfo CachedInvokableCall_1_t4765_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4765_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4765_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4765_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4765_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4765_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4765_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4765_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4765_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4765)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_159.h"
extern TypeInfo UnityAction_1_t4767_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_159MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Material>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Material>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMaterial_t64_m38402(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Material>
extern Il2CppType UnityAction_1_t4767_0_0_1;
FieldInfo InvokableCall_1_t4766____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4767_0_0_1/* type */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4766, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4766_FieldInfos[] =
{
	&InvokableCall_1_t4766____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4766_InvokableCall_1__ctor_m28889_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28889_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28889_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4766_InvokableCall_1__ctor_m28889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28889_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4767_0_0_0;
static ParameterInfo InvokableCall_1_t4766_InvokableCall_1__ctor_m28890_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4767_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28890_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28890_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4766_InvokableCall_1__ctor_m28890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28890_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4766_InvokableCall_1_Invoke_m28891_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28891_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28891_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4766_InvokableCall_1_Invoke_m28891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28891_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4766_InvokableCall_1_Find_m28892_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28892_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Material>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28892_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4766_InvokableCall_1_Find_m28892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28892_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4766_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28889_MethodInfo,
	&InvokableCall_1__ctor_m28890_MethodInfo,
	&InvokableCall_1_Invoke_m28891_MethodInfo,
	&InvokableCall_1_Find_m28892_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4766_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28891_MethodInfo,
	&InvokableCall_1_Find_m28892_MethodInfo,
};
extern TypeInfo UnityAction_1_t4767_il2cpp_TypeInfo;
extern TypeInfo Material_t64_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4766_RGCTXData[5] = 
{
	&UnityAction_1_t4767_0_0_0/* Type Usage */,
	&UnityAction_1_t4767_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMaterial_t64_m38402_MethodInfo/* Method Usage */,
	&Material_t64_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28894_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4766_0_0_0;
extern Il2CppType InvokableCall_1_t4766_1_0_0;
struct InvokableCall_1_t4766;
extern Il2CppGenericClass InvokableCall_1_t4766_GenericClass;
TypeInfo InvokableCall_1_t4766_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4766_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4766_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4766_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4766_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4766_0_0_0/* byval_arg */
	, &InvokableCall_1_t4766_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4766_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4766_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4766)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Material>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Material>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4767_UnityAction_1__ctor_m28893_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28893_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28893_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4767_UnityAction_1__ctor_m28893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28893_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo UnityAction_1_t4767_UnityAction_1_Invoke_m28894_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28894_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28894_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4767_UnityAction_1_Invoke_m28894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28894_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t64_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4767_UnityAction_1_BeginInvoke_m28895_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28895_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Material>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28895_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4767_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4767_UnityAction_1_BeginInvoke_m28895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28895_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4767_UnityAction_1_EndInvoke_m28896_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28896_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28896_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4767_UnityAction_1_EndInvoke_m28896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28896_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4767_MethodInfos[] =
{
	&UnityAction_1__ctor_m28893_MethodInfo,
	&UnityAction_1_Invoke_m28894_MethodInfo,
	&UnityAction_1_BeginInvoke_m28895_MethodInfo,
	&UnityAction_1_EndInvoke_m28896_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28895_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28896_MethodInfo;
static MethodInfo* UnityAction_1_t4767_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28894_MethodInfo,
	&UnityAction_1_BeginInvoke_m28895_MethodInfo,
	&UnityAction_1_EndInvoke_m28896_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4767_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4767_1_0_0;
struct UnityAction_1_t4767;
extern Il2CppGenericClass UnityAction_1_t4767_GenericClass;
TypeInfo UnityAction_1_t4767_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4767_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4767_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4767_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4767_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4767_0_0_0/* byval_arg */
	, &UnityAction_1_t4767_1_0_0/* this_arg */
	, UnityAction_1_t4767_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4767)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6853_il2cpp_TypeInfo;

// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>
extern MethodInfo IEnumerator_1_get_Current_m48960_MethodInfo;
static PropertyInfo IEnumerator_1_t6853____Current_PropertyInfo = 
{
	&IEnumerator_1_t6853_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6853_PropertyInfos[] =
{
	&IEnumerator_1_t6853____Current_PropertyInfo,
	NULL
};
extern Il2CppType Sprite_t194_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48960_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48960_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6853_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t194_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48960_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6853_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48960_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6853_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6853_0_0_0;
extern Il2CppType IEnumerator_1_t6853_1_0_0;
struct IEnumerator_1_t6853;
extern Il2CppGenericClass IEnumerator_1_t6853_GenericClass;
TypeInfo IEnumerator_1_t6853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6853_MethodInfos/* methods */
	, IEnumerator_1_t6853_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6853_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6853_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6853_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6853_0_0_0/* byval_arg */
	, &IEnumerator_1_t6853_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6853_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Sprite>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_440.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4768_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Sprite>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_440MethodDeclarations.h"

extern TypeInfo Sprite_t194_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28901_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSprite_t194_m38404_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Sprite>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Sprite>(System.Int32)
#define Array_InternalArray__get_Item_TisSprite_t194_m38404(__this, p0, method) (Sprite_t194 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Sprite>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Sprite>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Sprite>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Sprite>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4768____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4768, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4768____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4768, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4768_FieldInfos[] =
{
	&InternalEnumerator_1_t4768____array_0_FieldInfo,
	&InternalEnumerator_1_t4768____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4768____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4768_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4768____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4768_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4768_PropertyInfos[] =
{
	&InternalEnumerator_1_t4768____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4768____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4768_InternalEnumerator_1__ctor_m28897_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28897_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28897_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4768_InternalEnumerator_1__ctor_m28897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28897_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Sprite>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28899_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28899_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28899_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28900_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Sprite>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28900_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28900_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t194_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28901_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Sprite>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28901_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t194_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28901_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4768_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28897_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_MethodInfo,
	&InternalEnumerator_1_Dispose_m28899_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28900_MethodInfo,
	&InternalEnumerator_1_get_Current_m28901_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28900_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28899_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4768_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28898_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28900_MethodInfo,
	&InternalEnumerator_1_Dispose_m28899_MethodInfo,
	&InternalEnumerator_1_get_Current_m28901_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4768_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6853_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4768_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6853_il2cpp_TypeInfo, 7},
};
extern TypeInfo Sprite_t194_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4768_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28901_MethodInfo/* Method Usage */,
	&Sprite_t194_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSprite_t194_m38404_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4768_0_0_0;
extern Il2CppType InternalEnumerator_1_t4768_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4768_GenericClass;
TypeInfo InternalEnumerator_1_t4768_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4768_MethodInfos/* methods */
	, InternalEnumerator_1_t4768_PropertyInfos/* properties */
	, InternalEnumerator_1_t4768_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4768_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4768_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4768_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4768_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4768_1_0_0/* this_arg */
	, InternalEnumerator_1_t4768_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4768_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4768_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4768)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8691_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Sprite>
extern MethodInfo ICollection_1_get_Count_m48961_MethodInfo;
static PropertyInfo ICollection_1_t8691____Count_PropertyInfo = 
{
	&ICollection_1_t8691_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48961_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48962_MethodInfo;
static PropertyInfo ICollection_1_t8691____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8691_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48962_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8691_PropertyInfos[] =
{
	&ICollection_1_t8691____Count_PropertyInfo,
	&ICollection_1_t8691____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48961_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_Count()
MethodInfo ICollection_1_get_Count_m48961_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48961_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48962_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48962_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48962_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t194_0_0_0;
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo ICollection_1_t8691_ICollection_1_Add_m48963_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48963_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Add(T)
MethodInfo ICollection_1_Add_m48963_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8691_ICollection_1_Add_m48963_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48963_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48964_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Clear()
MethodInfo ICollection_1_Clear_m48964_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48964_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo ICollection_1_t8691_ICollection_1_Contains_m48965_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48965_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Contains(T)
MethodInfo ICollection_1_Contains_m48965_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8691_ICollection_1_Contains_m48965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48965_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteU5BU5D_t5711_0_0_0;
extern Il2CppType SpriteU5BU5D_t5711_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8691_ICollection_1_CopyTo_m48966_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SpriteU5BU5D_t5711_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48966_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48966_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8691_ICollection_1_CopyTo_m48966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48966_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo ICollection_1_t8691_ICollection_1_Remove_m48967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48967_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Remove(T)
MethodInfo ICollection_1_Remove_m48967_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8691_ICollection_1_Remove_m48967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48967_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8691_MethodInfos[] =
{
	&ICollection_1_get_Count_m48961_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48962_MethodInfo,
	&ICollection_1_Add_m48963_MethodInfo,
	&ICollection_1_Clear_m48964_MethodInfo,
	&ICollection_1_Contains_m48965_MethodInfo,
	&ICollection_1_CopyTo_m48966_MethodInfo,
	&ICollection_1_Remove_m48967_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8693_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8691_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8693_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8691_0_0_0;
extern Il2CppType ICollection_1_t8691_1_0_0;
struct ICollection_1_t8691;
extern Il2CppGenericClass ICollection_1_t8691_GenericClass;
TypeInfo ICollection_1_t8691_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8691_MethodInfos/* methods */
	, ICollection_1_t8691_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8691_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8691_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8691_0_0_0/* byval_arg */
	, &ICollection_1_t8691_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8691_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>
extern Il2CppType IEnumerator_1_t6853_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48968_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48968_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8693_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6853_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48968_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8693_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48968_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8693_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8693_0_0_0;
extern Il2CppType IEnumerable_1_t8693_1_0_0;
struct IEnumerable_1_t8693;
extern Il2CppGenericClass IEnumerable_1_t8693_GenericClass;
TypeInfo IEnumerable_1_t8693_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8693_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8693_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8693_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8693_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8693_0_0_0/* byval_arg */
	, &IEnumerable_1_t8693_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8693_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8692_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Sprite>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Sprite>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Sprite>
extern MethodInfo IList_1_get_Item_m48969_MethodInfo;
extern MethodInfo IList_1_set_Item_m48970_MethodInfo;
static PropertyInfo IList_1_t8692____Item_PropertyInfo = 
{
	&IList_1_t8692_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48969_MethodInfo/* get */
	, &IList_1_set_Item_m48970_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8692_PropertyInfos[] =
{
	&IList_1_t8692____Item_PropertyInfo,
	NULL
};
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo IList_1_t8692_IList_1_IndexOf_m48971_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48971_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Sprite>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48971_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8692_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8692_IList_1_IndexOf_m48971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48971_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo IList_1_t8692_IList_1_Insert_m48972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48972_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48972_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8692_IList_1_Insert_m48972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48972_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8692_IList_1_RemoveAt_m48973_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48973_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48973_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8692_IList_1_RemoveAt_m48973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48973_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8692_IList_1_get_Item_m48969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Sprite_t194_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48969_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Sprite>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48969_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8692_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t194_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8692_IList_1_get_Item_m48969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48969_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo IList_1_t8692_IList_1_set_Item_m48970_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48970_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48970_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8692_IList_1_set_Item_m48970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48970_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8692_MethodInfos[] =
{
	&IList_1_IndexOf_m48971_MethodInfo,
	&IList_1_Insert_m48972_MethodInfo,
	&IList_1_RemoveAt_m48973_MethodInfo,
	&IList_1_get_Item_m48969_MethodInfo,
	&IList_1_set_Item_m48970_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8692_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8691_il2cpp_TypeInfo,
	&IEnumerable_1_t8693_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8692_0_0_0;
extern Il2CppType IList_1_t8692_1_0_0;
struct IList_1_t8692;
extern Il2CppGenericClass IList_1_t8692_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8692_MethodInfos/* methods */
	, IList_1_t8692_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8692_il2cpp_TypeInfo/* element_class */
	, IList_1_t8692_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8692_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8692_0_0_0/* byval_arg */
	, &IList_1_t8692_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8692_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_153.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4769_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_153MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_155.h"
extern TypeInfo InvokableCall_1_t4770_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_155MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28904_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28906_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4769____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4769_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4769, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4769_FieldInfos[] =
{
	&CachedInvokableCall_1_t4769____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4769_CachedInvokableCall_1__ctor_m28902_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28902_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28902_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4769_CachedInvokableCall_1__ctor_m28902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28902_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4769_CachedInvokableCall_1_Invoke_m28903_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28903_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28903_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4769_CachedInvokableCall_1_Invoke_m28903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28903_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4769_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28902_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28903_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28903_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28907_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4769_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28903_MethodInfo,
	&InvokableCall_1_Find_m28907_MethodInfo,
};
extern Il2CppType UnityAction_1_t4771_0_0_0;
extern TypeInfo UnityAction_1_t4771_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t194_m38414_MethodInfo;
extern TypeInfo Sprite_t194_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28909_MethodInfo;
extern TypeInfo Sprite_t194_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4769_RGCTXData[8] = 
{
	&UnityAction_1_t4771_0_0_0/* Type Usage */,
	&UnityAction_1_t4771_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t194_m38414_MethodInfo/* Method Usage */,
	&Sprite_t194_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28909_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28904_MethodInfo/* Method Usage */,
	&Sprite_t194_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28906_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4769_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4769_1_0_0;
struct CachedInvokableCall_1_t4769;
extern Il2CppGenericClass CachedInvokableCall_1_t4769_GenericClass;
TypeInfo CachedInvokableCall_1_t4769_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4769_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4769_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4769_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4769_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4769_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4769_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4769_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4769_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4769)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_160.h"
extern TypeInfo UnityAction_1_t4771_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_160MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Sprite>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Sprite>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t194_m38414(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
extern Il2CppType UnityAction_1_t4771_0_0_1;
FieldInfo InvokableCall_1_t4770____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4771_0_0_1/* type */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4770, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4770_FieldInfos[] =
{
	&InvokableCall_1_t4770____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4770_InvokableCall_1__ctor_m28904_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28904_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28904_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4770_InvokableCall_1__ctor_m28904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28904_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4771_0_0_0;
static ParameterInfo InvokableCall_1_t4770_InvokableCall_1__ctor_m28905_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4771_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28905_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28905_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4770_InvokableCall_1__ctor_m28905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28905_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4770_InvokableCall_1_Invoke_m28906_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28906_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28906_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4770_InvokableCall_1_Invoke_m28906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28906_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4770_InvokableCall_1_Find_m28907_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28907_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28907_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4770_InvokableCall_1_Find_m28907_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28907_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4770_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28904_MethodInfo,
	&InvokableCall_1__ctor_m28905_MethodInfo,
	&InvokableCall_1_Invoke_m28906_MethodInfo,
	&InvokableCall_1_Find_m28907_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4770_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28906_MethodInfo,
	&InvokableCall_1_Find_m28907_MethodInfo,
};
extern TypeInfo UnityAction_1_t4771_il2cpp_TypeInfo;
extern TypeInfo Sprite_t194_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4770_RGCTXData[5] = 
{
	&UnityAction_1_t4771_0_0_0/* Type Usage */,
	&UnityAction_1_t4771_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t194_m38414_MethodInfo/* Method Usage */,
	&Sprite_t194_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28909_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4770_0_0_0;
extern Il2CppType InvokableCall_1_t4770_1_0_0;
struct InvokableCall_1_t4770;
extern Il2CppGenericClass InvokableCall_1_t4770_GenericClass;
TypeInfo InvokableCall_1_t4770_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4770_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4770_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4770_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4770_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4770_0_0_0/* byval_arg */
	, &InvokableCall_1_t4770_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4770_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4770_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4770)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4771_UnityAction_1__ctor_m28908_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28908_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4771_UnityAction_1__ctor_m28908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28908_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t194_0_0_0;
static ParameterInfo UnityAction_1_t4771_UnityAction_1_Invoke_m28909_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28909_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28909_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4771_UnityAction_1_Invoke_m28909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28909_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t194_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4771_UnityAction_1_BeginInvoke_m28910_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t194_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28910_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28910_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4771_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4771_UnityAction_1_BeginInvoke_m28910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28910_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4771_UnityAction_1_EndInvoke_m28911_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28911_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28911_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4771_UnityAction_1_EndInvoke_m28911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28911_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4771_MethodInfos[] =
{
	&UnityAction_1__ctor_m28908_MethodInfo,
	&UnityAction_1_Invoke_m28909_MethodInfo,
	&UnityAction_1_BeginInvoke_m28910_MethodInfo,
	&UnityAction_1_EndInvoke_m28911_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28910_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28911_MethodInfo;
static MethodInfo* UnityAction_1_t4771_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28909_MethodInfo,
	&UnityAction_1_BeginInvoke_m28910_MethodInfo,
	&UnityAction_1_EndInvoke_m28911_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4771_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4771_1_0_0;
struct UnityAction_1_t4771;
extern Il2CppGenericClass UnityAction_1_t4771_GenericClass;
TypeInfo UnityAction_1_t4771_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4771_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4771_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4771_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4771_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4771_0_0_0/* byval_arg */
	, &UnityAction_1_t4771_1_0_0/* this_arg */
	, UnityAction_1_t4771_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4771)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6855_il2cpp_TypeInfo;

// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRenderer.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>
extern MethodInfo IEnumerator_1_get_Current_m48974_MethodInfo;
static PropertyInfo IEnumerator_1_t6855____Current_PropertyInfo = 
{
	&IEnumerator_1_t6855_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6855_PropertyInfos[] =
{
	&IEnumerator_1_t6855____Current_PropertyInfo,
	NULL
};
extern Il2CppType SpriteRenderer_t193_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48974_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48974_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6855_il2cpp_TypeInfo/* declaring_type */
	, &SpriteRenderer_t193_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48974_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6855_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48974_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6855_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6855_0_0_0;
extern Il2CppType IEnumerator_1_t6855_1_0_0;
struct IEnumerator_1_t6855;
extern Il2CppGenericClass IEnumerator_1_t6855_GenericClass;
TypeInfo IEnumerator_1_t6855_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6855_MethodInfos/* methods */
	, IEnumerator_1_t6855_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6855_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6855_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6855_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6855_0_0_0/* byval_arg */
	, &IEnumerator_1_t6855_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6855_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_441.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4772_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_441MethodDeclarations.h"

extern TypeInfo SpriteRenderer_t193_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28916_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSpriteRenderer_t193_m38416_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SpriteRenderer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SpriteRenderer>(System.Int32)
#define Array_InternalArray__get_Item_TisSpriteRenderer_t193_m38416(__this, p0, method) (SpriteRenderer_t193 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4772____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4772, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4772____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4772, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4772_FieldInfos[] =
{
	&InternalEnumerator_1_t4772____array_0_FieldInfo,
	&InternalEnumerator_1_t4772____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4772____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4772_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4772____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4772_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28916_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4772_PropertyInfos[] =
{
	&InternalEnumerator_1_t4772____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4772____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4772_InternalEnumerator_1__ctor_m28912_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28912_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28912_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4772_InternalEnumerator_1__ctor_m28912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28912_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28914_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28914_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28914_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28915_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28915_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28915_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t193_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28916_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28916_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* declaring_type */
	, &SpriteRenderer_t193_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28916_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4772_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28912_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_MethodInfo,
	&InternalEnumerator_1_Dispose_m28914_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28915_MethodInfo,
	&InternalEnumerator_1_get_Current_m28916_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28915_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28914_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4772_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28913_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28915_MethodInfo,
	&InternalEnumerator_1_Dispose_m28914_MethodInfo,
	&InternalEnumerator_1_get_Current_m28916_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4772_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6855_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4772_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6855_il2cpp_TypeInfo, 7},
};
extern TypeInfo SpriteRenderer_t193_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4772_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28916_MethodInfo/* Method Usage */,
	&SpriteRenderer_t193_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSpriteRenderer_t193_m38416_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4772_0_0_0;
extern Il2CppType InternalEnumerator_1_t4772_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4772_GenericClass;
TypeInfo InternalEnumerator_1_t4772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4772_MethodInfos/* methods */
	, InternalEnumerator_1_t4772_PropertyInfos/* properties */
	, InternalEnumerator_1_t4772_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4772_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4772_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4772_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4772_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4772_1_0_0/* this_arg */
	, InternalEnumerator_1_t4772_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4772_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4772_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4772)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8694_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>
extern MethodInfo ICollection_1_get_Count_m48975_MethodInfo;
static PropertyInfo ICollection_1_t8694____Count_PropertyInfo = 
{
	&ICollection_1_t8694_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48976_MethodInfo;
static PropertyInfo ICollection_1_t8694____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8694_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48976_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8694_PropertyInfos[] =
{
	&ICollection_1_t8694____Count_PropertyInfo,
	&ICollection_1_t8694____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48975_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_Count()
MethodInfo ICollection_1_get_Count_m48975_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48975_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48976_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48976_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48976_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t193_0_0_0;
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo ICollection_1_t8694_ICollection_1_Add_m48977_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48977_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Add(T)
MethodInfo ICollection_1_Add_m48977_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8694_ICollection_1_Add_m48977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48977_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48978_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Clear()
MethodInfo ICollection_1_Clear_m48978_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48978_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo ICollection_1_t8694_ICollection_1_Contains_m48979_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48979_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Contains(T)
MethodInfo ICollection_1_Contains_m48979_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8694_ICollection_1_Contains_m48979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48979_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRendererU5BU5D_t5712_0_0_0;
extern Il2CppType SpriteRendererU5BU5D_t5712_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8694_ICollection_1_CopyTo_m48980_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRendererU5BU5D_t5712_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48980_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48980_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8694_ICollection_1_CopyTo_m48980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48980_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo ICollection_1_t8694_ICollection_1_Remove_m48981_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48981_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Remove(T)
MethodInfo ICollection_1_Remove_m48981_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8694_ICollection_1_Remove_m48981_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48981_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8694_MethodInfos[] =
{
	&ICollection_1_get_Count_m48975_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48976_MethodInfo,
	&ICollection_1_Add_m48977_MethodInfo,
	&ICollection_1_Clear_m48978_MethodInfo,
	&ICollection_1_Contains_m48979_MethodInfo,
	&ICollection_1_CopyTo_m48980_MethodInfo,
	&ICollection_1_Remove_m48981_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8696_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8694_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8696_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8694_0_0_0;
extern Il2CppType ICollection_1_t8694_1_0_0;
struct ICollection_1_t8694;
extern Il2CppGenericClass ICollection_1_t8694_GenericClass;
TypeInfo ICollection_1_t8694_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8694_MethodInfos/* methods */
	, ICollection_1_t8694_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8694_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8694_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8694_0_0_0/* byval_arg */
	, &ICollection_1_t8694_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8694_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>
extern Il2CppType IEnumerator_1_t6855_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48982_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48982_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8696_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6855_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48982_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8696_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48982_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8696_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8696_0_0_0;
extern Il2CppType IEnumerable_1_t8696_1_0_0;
struct IEnumerable_1_t8696;
extern Il2CppGenericClass IEnumerable_1_t8696_GenericClass;
TypeInfo IEnumerable_1_t8696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8696_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8696_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8696_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8696_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8696_0_0_0/* byval_arg */
	, &IEnumerable_1_t8696_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8696_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8695_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>
extern MethodInfo IList_1_get_Item_m48983_MethodInfo;
extern MethodInfo IList_1_set_Item_m48984_MethodInfo;
static PropertyInfo IList_1_t8695____Item_PropertyInfo = 
{
	&IList_1_t8695_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48983_MethodInfo/* get */
	, &IList_1_set_Item_m48984_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8695_PropertyInfos[] =
{
	&IList_1_t8695____Item_PropertyInfo,
	NULL
};
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo IList_1_t8695_IList_1_IndexOf_m48985_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48985_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48985_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8695_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8695_IList_1_IndexOf_m48985_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48985_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo IList_1_t8695_IList_1_Insert_m48986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48986_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48986_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8695_IList_1_Insert_m48986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48986_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8695_IList_1_RemoveAt_m48987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48987_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48987_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8695_IList_1_RemoveAt_m48987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48987_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8695_IList_1_get_Item_m48983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SpriteRenderer_t193_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48983_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48983_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8695_il2cpp_TypeInfo/* declaring_type */
	, &SpriteRenderer_t193_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8695_IList_1_get_Item_m48983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48983_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo IList_1_t8695_IList_1_set_Item_m48984_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48984_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48984_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8695_IList_1_set_Item_m48984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48984_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8695_MethodInfos[] =
{
	&IList_1_IndexOf_m48985_MethodInfo,
	&IList_1_Insert_m48986_MethodInfo,
	&IList_1_RemoveAt_m48987_MethodInfo,
	&IList_1_get_Item_m48983_MethodInfo,
	&IList_1_set_Item_m48984_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8695_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8694_il2cpp_TypeInfo,
	&IEnumerable_1_t8696_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8695_0_0_0;
extern Il2CppType IList_1_t8695_1_0_0;
struct IList_1_t8695;
extern Il2CppGenericClass IList_1_t8695_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8695_MethodInfos/* methods */
	, IList_1_t8695_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8695_il2cpp_TypeInfo/* element_class */
	, IList_1_t8695_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8695_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8695_0_0_0/* byval_arg */
	, &IList_1_t8695_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8695_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_154.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4773_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_154MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_156.h"
extern TypeInfo InvokableCall_1_t4774_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_156MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28919_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28921_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4773____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4773_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4773, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4773_FieldInfos[] =
{
	&CachedInvokableCall_1_t4773____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4773_CachedInvokableCall_1__ctor_m28917_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28917_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28917_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4773_CachedInvokableCall_1__ctor_m28917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28917_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4773_CachedInvokableCall_1_Invoke_m28918_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28918_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28918_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4773_CachedInvokableCall_1_Invoke_m28918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28918_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4773_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28917_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28918_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28918_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28922_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4773_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28918_MethodInfo,
	&InvokableCall_1_Find_m28922_MethodInfo,
};
extern Il2CppType UnityAction_1_t4775_0_0_0;
extern TypeInfo UnityAction_1_t4775_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t193_m38426_MethodInfo;
extern TypeInfo SpriteRenderer_t193_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28924_MethodInfo;
extern TypeInfo SpriteRenderer_t193_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4773_RGCTXData[8] = 
{
	&UnityAction_1_t4775_0_0_0/* Type Usage */,
	&UnityAction_1_t4775_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t193_m38426_MethodInfo/* Method Usage */,
	&SpriteRenderer_t193_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28924_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28919_MethodInfo/* Method Usage */,
	&SpriteRenderer_t193_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28921_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4773_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4773_1_0_0;
struct CachedInvokableCall_1_t4773;
extern Il2CppGenericClass CachedInvokableCall_1_t4773_GenericClass;
TypeInfo CachedInvokableCall_1_t4773_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4773_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4773_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4773_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4773_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4773_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4773_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4773_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4773_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4773)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_161.h"
extern TypeInfo UnityAction_1_t4775_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_161MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.SpriteRenderer>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.SpriteRenderer>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t193_m38426(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
extern Il2CppType UnityAction_1_t4775_0_0_1;
FieldInfo InvokableCall_1_t4774____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4775_0_0_1/* type */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4774, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4774_FieldInfos[] =
{
	&InvokableCall_1_t4774____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4774_InvokableCall_1__ctor_m28919_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28919_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28919_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4774_InvokableCall_1__ctor_m28919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28919_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4775_0_0_0;
static ParameterInfo InvokableCall_1_t4774_InvokableCall_1__ctor_m28920_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4775_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28920_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28920_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4774_InvokableCall_1__ctor_m28920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28920_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4774_InvokableCall_1_Invoke_m28921_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28921_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28921_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4774_InvokableCall_1_Invoke_m28921_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28921_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4774_InvokableCall_1_Find_m28922_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28922_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28922_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4774_InvokableCall_1_Find_m28922_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28922_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4774_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28919_MethodInfo,
	&InvokableCall_1__ctor_m28920_MethodInfo,
	&InvokableCall_1_Invoke_m28921_MethodInfo,
	&InvokableCall_1_Find_m28922_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4774_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28921_MethodInfo,
	&InvokableCall_1_Find_m28922_MethodInfo,
};
extern TypeInfo UnityAction_1_t4775_il2cpp_TypeInfo;
extern TypeInfo SpriteRenderer_t193_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4774_RGCTXData[5] = 
{
	&UnityAction_1_t4775_0_0_0/* Type Usage */,
	&UnityAction_1_t4775_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t193_m38426_MethodInfo/* Method Usage */,
	&SpriteRenderer_t193_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28924_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4774_0_0_0;
extern Il2CppType InvokableCall_1_t4774_1_0_0;
struct InvokableCall_1_t4774;
extern Il2CppGenericClass InvokableCall_1_t4774_GenericClass;
TypeInfo InvokableCall_1_t4774_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4774_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4774_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4774_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4774_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4774_0_0_0/* byval_arg */
	, &InvokableCall_1_t4774_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4774_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4774_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4774)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4775_UnityAction_1__ctor_m28923_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28923_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28923_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4775_UnityAction_1__ctor_m28923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28923_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t193_0_0_0;
static ParameterInfo UnityAction_1_t4775_UnityAction_1_Invoke_m28924_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28924_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28924_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4775_UnityAction_1_Invoke_m28924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28924_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t193_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4775_UnityAction_1_BeginInvoke_m28925_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t193_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28925_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28925_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4775_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4775_UnityAction_1_BeginInvoke_m28925_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28925_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4775_UnityAction_1_EndInvoke_m28926_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28926_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28926_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4775_UnityAction_1_EndInvoke_m28926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28926_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4775_MethodInfos[] =
{
	&UnityAction_1__ctor_m28923_MethodInfo,
	&UnityAction_1_Invoke_m28924_MethodInfo,
	&UnityAction_1_BeginInvoke_m28925_MethodInfo,
	&UnityAction_1_EndInvoke_m28926_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28925_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28926_MethodInfo;
static MethodInfo* UnityAction_1_t4775_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28924_MethodInfo,
	&UnityAction_1_BeginInvoke_m28925_MethodInfo,
	&UnityAction_1_EndInvoke_m28926_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4775_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4775_1_0_0;
struct UnityAction_1_t4775;
extern Il2CppGenericClass UnityAction_1_t4775_GenericClass;
TypeInfo UnityAction_1_t4775_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4775_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4775_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4775_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4775_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4775_0_0_0/* byval_arg */
	, &UnityAction_1_t4775_1_0_0/* this_arg */
	, UnityAction_1_t4775_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4775)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_155.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4776_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_155MethodDeclarations.h"

// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_157.h"
extern TypeInfo Behaviour_t559_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4777_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_157MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28929_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28931_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4776____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4776_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4776, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4776_FieldInfos[] =
{
	&CachedInvokableCall_1_t4776____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Behaviour_t559_0_0_0;
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4776_CachedInvokableCall_1__ctor_m28927_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28927_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28927_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4776_CachedInvokableCall_1__ctor_m28927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28927_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4776_CachedInvokableCall_1_Invoke_m28928_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28928_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28928_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4776_CachedInvokableCall_1_Invoke_m28928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28928_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4776_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28927_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28928_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28928_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28932_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4776_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28928_MethodInfo,
	&InvokableCall_1_Find_m28932_MethodInfo,
};
extern Il2CppType UnityAction_1_t4778_0_0_0;
extern TypeInfo UnityAction_1_t4778_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t559_m38427_MethodInfo;
extern TypeInfo Behaviour_t559_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28934_MethodInfo;
extern TypeInfo Behaviour_t559_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4776_RGCTXData[8] = 
{
	&UnityAction_1_t4778_0_0_0/* Type Usage */,
	&UnityAction_1_t4778_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t559_m38427_MethodInfo/* Method Usage */,
	&Behaviour_t559_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28934_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28929_MethodInfo/* Method Usage */,
	&Behaviour_t559_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28931_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4776_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4776_1_0_0;
struct CachedInvokableCall_1_t4776;
extern Il2CppGenericClass CachedInvokableCall_1_t4776_GenericClass;
TypeInfo CachedInvokableCall_1_t4776_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4776_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4776_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4776_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4776_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4776_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4776_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4776_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4776_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4776_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4776)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_162.h"
extern TypeInfo UnityAction_1_t4778_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_162MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Behaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Behaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t559_m38427(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
extern Il2CppType UnityAction_1_t4778_0_0_1;
FieldInfo InvokableCall_1_t4777____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4778_0_0_1/* type */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4777, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4777_FieldInfos[] =
{
	&InvokableCall_1_t4777____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4777_InvokableCall_1__ctor_m28929_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28929_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28929_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4777_InvokableCall_1__ctor_m28929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28929_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4778_0_0_0;
static ParameterInfo InvokableCall_1_t4777_InvokableCall_1__ctor_m28930_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4778_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28930_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28930_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4777_InvokableCall_1__ctor_m28930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28930_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4777_InvokableCall_1_Invoke_m28931_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28931_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28931_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4777_InvokableCall_1_Invoke_m28931_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28931_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4777_InvokableCall_1_Find_m28932_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28932_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28932_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4777_InvokableCall_1_Find_m28932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28932_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4777_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28929_MethodInfo,
	&InvokableCall_1__ctor_m28930_MethodInfo,
	&InvokableCall_1_Invoke_m28931_MethodInfo,
	&InvokableCall_1_Find_m28932_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4777_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28931_MethodInfo,
	&InvokableCall_1_Find_m28932_MethodInfo,
};
extern TypeInfo UnityAction_1_t4778_il2cpp_TypeInfo;
extern TypeInfo Behaviour_t559_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4777_RGCTXData[5] = 
{
	&UnityAction_1_t4778_0_0_0/* Type Usage */,
	&UnityAction_1_t4778_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t559_m38427_MethodInfo/* Method Usage */,
	&Behaviour_t559_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28934_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4777_0_0_0;
extern Il2CppType InvokableCall_1_t4777_1_0_0;
struct InvokableCall_1_t4777;
extern Il2CppGenericClass InvokableCall_1_t4777_GenericClass;
TypeInfo InvokableCall_1_t4777_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4777_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4777_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4777_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4777_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4777_0_0_0/* byval_arg */
	, &InvokableCall_1_t4777_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4777_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4777)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4778_UnityAction_1__ctor_m28933_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28933_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28933_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4778_UnityAction_1__ctor_m28933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28933_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo UnityAction_1_t4778_UnityAction_1_Invoke_m28934_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28934_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28934_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4778_UnityAction_1_Invoke_m28934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28934_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t559_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4778_UnityAction_1_BeginInvoke_m28935_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28935_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28935_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4778_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4778_UnityAction_1_BeginInvoke_m28935_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28935_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4778_UnityAction_1_EndInvoke_m28936_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28936_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28936_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4778_UnityAction_1_EndInvoke_m28936_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28936_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4778_MethodInfos[] =
{
	&UnityAction_1__ctor_m28933_MethodInfo,
	&UnityAction_1_Invoke_m28934_MethodInfo,
	&UnityAction_1_BeginInvoke_m28935_MethodInfo,
	&UnityAction_1_EndInvoke_m28936_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28935_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28936_MethodInfo;
static MethodInfo* UnityAction_1_t4778_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28934_MethodInfo,
	&UnityAction_1_BeginInvoke_m28935_MethodInfo,
	&UnityAction_1_EndInvoke_m28936_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4778_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4778_1_0_0;
struct UnityAction_1_t4778;
extern Il2CppGenericClass UnityAction_1_t4778_GenericClass;
TypeInfo UnityAction_1_t4778_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4778_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4778_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4778_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4778_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4778_0_0_0/* byval_arg */
	, &UnityAction_1_t4778_1_0_0/* this_arg */
	, UnityAction_1_t4778_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4778_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4778)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_156.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4779_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_156MethodDeclarations.h"

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_158.h"
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4780_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_158MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28939_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28941_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4779____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4779_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4779, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4779_FieldInfos[] =
{
	&CachedInvokableCall_1_t4779____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Camera_t168_0_0_0;
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4779_CachedInvokableCall_1__ctor_m28937_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28937_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28937_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4779_CachedInvokableCall_1__ctor_m28937_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28937_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4779_CachedInvokableCall_1_Invoke_m28938_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28938_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28938_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4779_CachedInvokableCall_1_Invoke_m28938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28938_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4779_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28937_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28938_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28938_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28942_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4779_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28938_MethodInfo,
	&InvokableCall_1_Find_m28942_MethodInfo,
};
extern Il2CppType UnityAction_1_t4781_0_0_0;
extern TypeInfo UnityAction_1_t4781_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t168_m38428_MethodInfo;
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28944_MethodInfo;
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4779_RGCTXData[8] = 
{
	&UnityAction_1_t4781_0_0_0/* Type Usage */,
	&UnityAction_1_t4781_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t168_m38428_MethodInfo/* Method Usage */,
	&Camera_t168_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28944_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28939_MethodInfo/* Method Usage */,
	&Camera_t168_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28941_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4779_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4779_1_0_0;
struct CachedInvokableCall_1_t4779;
extern Il2CppGenericClass CachedInvokableCall_1_t4779_GenericClass;
TypeInfo CachedInvokableCall_1_t4779_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4779_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4779_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4779_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4779_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4779_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4779_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4779_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4779_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4779)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_163.h"
extern TypeInfo UnityAction_1_t4781_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_163MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Camera>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Camera>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t168_m38428(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
extern Il2CppType UnityAction_1_t4781_0_0_1;
FieldInfo InvokableCall_1_t4780____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4781_0_0_1/* type */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4780, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4780_FieldInfos[] =
{
	&InvokableCall_1_t4780____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4780_InvokableCall_1__ctor_m28939_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28939_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28939_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4780_InvokableCall_1__ctor_m28939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28939_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4781_0_0_0;
static ParameterInfo InvokableCall_1_t4780_InvokableCall_1__ctor_m28940_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4781_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28940_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28940_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4780_InvokableCall_1__ctor_m28940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28940_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4780_InvokableCall_1_Invoke_m28941_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28941_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28941_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4780_InvokableCall_1_Invoke_m28941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28941_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4780_InvokableCall_1_Find_m28942_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28942_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28942_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4780_InvokableCall_1_Find_m28942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28942_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4780_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28939_MethodInfo,
	&InvokableCall_1__ctor_m28940_MethodInfo,
	&InvokableCall_1_Invoke_m28941_MethodInfo,
	&InvokableCall_1_Find_m28942_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4780_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28941_MethodInfo,
	&InvokableCall_1_Find_m28942_MethodInfo,
};
extern TypeInfo UnityAction_1_t4781_il2cpp_TypeInfo;
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4780_RGCTXData[5] = 
{
	&UnityAction_1_t4781_0_0_0/* Type Usage */,
	&UnityAction_1_t4781_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t168_m38428_MethodInfo/* Method Usage */,
	&Camera_t168_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28944_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4780_0_0_0;
extern Il2CppType InvokableCall_1_t4780_1_0_0;
struct InvokableCall_1_t4780;
extern Il2CppGenericClass InvokableCall_1_t4780_GenericClass;
TypeInfo InvokableCall_1_t4780_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4780_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4780_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4780_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4780_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4780_0_0_0/* byval_arg */
	, &InvokableCall_1_t4780_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4780_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4780_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4780)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4781_UnityAction_1__ctor_m28943_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28943_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28943_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4781_UnityAction_1__ctor_m28943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28943_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo UnityAction_1_t4781_UnityAction_1_Invoke_m28944_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28944_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28944_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4781_UnityAction_1_Invoke_m28944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28944_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t168_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4781_UnityAction_1_BeginInvoke_m28945_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28945_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28945_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4781_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4781_UnityAction_1_BeginInvoke_m28945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28945_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4781_UnityAction_1_EndInvoke_m28946_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28946_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28946_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4781_UnityAction_1_EndInvoke_m28946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28946_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4781_MethodInfos[] =
{
	&UnityAction_1__ctor_m28943_MethodInfo,
	&UnityAction_1_Invoke_m28944_MethodInfo,
	&UnityAction_1_BeginInvoke_m28945_MethodInfo,
	&UnityAction_1_EndInvoke_m28946_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28945_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28946_MethodInfo;
static MethodInfo* UnityAction_1_t4781_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28944_MethodInfo,
	&UnityAction_1_BeginInvoke_m28945_MethodInfo,
	&UnityAction_1_EndInvoke_m28946_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4781_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4781_1_0_0;
struct UnityAction_1_t4781;
extern Il2CppGenericClass UnityAction_1_t4781_GenericClass;
TypeInfo UnityAction_1_t4781_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4781_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4781_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4781_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4781_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4781_0_0_0/* byval_arg */
	, &UnityAction_1_t4781_1_0_0/* this_arg */
	, UnityAction_1_t4781_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4781_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4781)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6857_il2cpp_TypeInfo;

// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Display>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Display>
extern MethodInfo IEnumerator_1_get_Current_m48988_MethodInfo;
static PropertyInfo IEnumerator_1_t6857____Current_PropertyInfo = 
{
	&IEnumerator_1_t6857_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6857_PropertyInfos[] =
{
	&IEnumerator_1_t6857____Current_PropertyInfo,
	NULL
};
extern Il2CppType Display_t1040_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48988_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Display>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48988_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6857_il2cpp_TypeInfo/* declaring_type */
	, &Display_t1040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48988_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6857_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48988_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6857_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6857_0_0_0;
extern Il2CppType IEnumerator_1_t6857_1_0_0;
struct IEnumerator_1_t6857;
extern Il2CppGenericClass IEnumerator_1_t6857_GenericClass;
TypeInfo IEnumerator_1_t6857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6857_MethodInfos/* methods */
	, IEnumerator_1_t6857_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6857_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6857_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6857_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6857_0_0_0/* byval_arg */
	, &IEnumerator_1_t6857_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6857_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Display>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_442.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4782_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Display>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_442MethodDeclarations.h"

extern TypeInfo Display_t1040_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28951_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDisplay_t1040_m38430_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Display>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Display>(System.Int32)
#define Array_InternalArray__get_Item_TisDisplay_t1040_m38430(__this, p0, method) (Display_t1040 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Display>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Display>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Display>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4782____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4782, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4782____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4782, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4782_FieldInfos[] =
{
	&InternalEnumerator_1_t4782____array_0_FieldInfo,
	&InternalEnumerator_1_t4782____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4782____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4782_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4782____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4782_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28951_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4782_PropertyInfos[] =
{
	&InternalEnumerator_1_t4782____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4782____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4782_InternalEnumerator_1__ctor_m28947_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28947_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28947_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4782_InternalEnumerator_1__ctor_m28947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28947_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28949_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28949_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28949_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28950_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Display>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28950_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28950_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t1040_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28951_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Display>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28951_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* declaring_type */
	, &Display_t1040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28951_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4782_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28947_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_MethodInfo,
	&InternalEnumerator_1_Dispose_m28949_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28950_MethodInfo,
	&InternalEnumerator_1_get_Current_m28951_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28950_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28949_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4782_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28948_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28950_MethodInfo,
	&InternalEnumerator_1_Dispose_m28949_MethodInfo,
	&InternalEnumerator_1_get_Current_m28951_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4782_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6857_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4782_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6857_il2cpp_TypeInfo, 7},
};
extern TypeInfo Display_t1040_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4782_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28951_MethodInfo/* Method Usage */,
	&Display_t1040_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDisplay_t1040_m38430_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4782_0_0_0;
extern Il2CppType InternalEnumerator_1_t4782_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4782_GenericClass;
TypeInfo InternalEnumerator_1_t4782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4782_MethodInfos/* methods */
	, InternalEnumerator_1_t4782_PropertyInfos/* properties */
	, InternalEnumerator_1_t4782_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4782_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4782_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4782_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4782_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4782_1_0_0/* this_arg */
	, InternalEnumerator_1_t4782_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4782_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4782)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8697_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Display>
extern MethodInfo ICollection_1_get_Count_m48989_MethodInfo;
static PropertyInfo ICollection_1_t8697____Count_PropertyInfo = 
{
	&ICollection_1_t8697_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48989_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48990_MethodInfo;
static PropertyInfo ICollection_1_t8697____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8697_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48990_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8697_PropertyInfos[] =
{
	&ICollection_1_t8697____Count_PropertyInfo,
	&ICollection_1_t8697____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48989_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_Count()
MethodInfo ICollection_1_get_Count_m48989_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48989_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48990_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48990_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48990_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t1040_0_0_0;
extern Il2CppType Display_t1040_0_0_0;
static ParameterInfo ICollection_1_t8697_ICollection_1_Add_m48991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t1040_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48991_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Add(T)
MethodInfo ICollection_1_Add_m48991_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8697_ICollection_1_Add_m48991_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48991_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48992_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Clear()
MethodInfo ICollection_1_Clear_m48992_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48992_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t1040_0_0_0;
static ParameterInfo ICollection_1_t8697_ICollection_1_Contains_m48993_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t1040_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48993_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Contains(T)
MethodInfo ICollection_1_Contains_m48993_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8697_ICollection_1_Contains_m48993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48993_GenericMethod/* genericMethod */

};
extern Il2CppType DisplayU5BU5D_t1039_0_0_0;
extern Il2CppType DisplayU5BU5D_t1039_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8697_ICollection_1_CopyTo_m48994_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DisplayU5BU5D_t1039_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48994_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48994_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8697_ICollection_1_CopyTo_m48994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48994_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t1040_0_0_0;
static ParameterInfo ICollection_1_t8697_ICollection_1_Remove_m48995_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t1040_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48995_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Remove(T)
MethodInfo ICollection_1_Remove_m48995_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8697_ICollection_1_Remove_m48995_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48995_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8697_MethodInfos[] =
{
	&ICollection_1_get_Count_m48989_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48990_MethodInfo,
	&ICollection_1_Add_m48991_MethodInfo,
	&ICollection_1_Clear_m48992_MethodInfo,
	&ICollection_1_Contains_m48993_MethodInfo,
	&ICollection_1_CopyTo_m48994_MethodInfo,
	&ICollection_1_Remove_m48995_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8699_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8697_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8699_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8697_0_0_0;
extern Il2CppType ICollection_1_t8697_1_0_0;
struct ICollection_1_t8697;
extern Il2CppGenericClass ICollection_1_t8697_GenericClass;
TypeInfo ICollection_1_t8697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8697_MethodInfos/* methods */
	, ICollection_1_t8697_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8697_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8697_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8697_0_0_0/* byval_arg */
	, &ICollection_1_t8697_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8697_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Display>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Display>
extern Il2CppType IEnumerator_1_t6857_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48996_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Display>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48996_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8699_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6857_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48996_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8699_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48996_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8699_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8699_0_0_0;
extern Il2CppType IEnumerable_1_t8699_1_0_0;
struct IEnumerable_1_t8699;
extern Il2CppGenericClass IEnumerable_1_t8699_GenericClass;
TypeInfo IEnumerable_1_t8699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8699_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8699_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8699_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8699_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8699_0_0_0/* byval_arg */
	, &IEnumerable_1_t8699_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8699_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8698_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Display>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Display>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Display>
extern MethodInfo IList_1_get_Item_m48997_MethodInfo;
extern MethodInfo IList_1_set_Item_m48998_MethodInfo;
static PropertyInfo IList_1_t8698____Item_PropertyInfo = 
{
	&IList_1_t8698_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48997_MethodInfo/* get */
	, &IList_1_set_Item_m48998_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8698_PropertyInfos[] =
{
	&IList_1_t8698____Item_PropertyInfo,
	NULL
};
extern Il2CppType Display_t1040_0_0_0;
static ParameterInfo IList_1_t8698_IList_1_IndexOf_m48999_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t1040_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48999_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Display>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48999_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8698_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8698_IList_1_IndexOf_m48999_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48999_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Display_t1040_0_0_0;
static ParameterInfo IList_1_t8698_IList_1_Insert_m49000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Display_t1040_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49000_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49000_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8698_IList_1_Insert_m49000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49000_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8698_IList_1_RemoveAt_m49001_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49001_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49001_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8698_IList_1_RemoveAt_m49001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49001_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8698_IList_1_get_Item_m48997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Display_t1040_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48997_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Display>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48997_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8698_il2cpp_TypeInfo/* declaring_type */
	, &Display_t1040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8698_IList_1_get_Item_m48997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48997_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Display_t1040_0_0_0;
static ParameterInfo IList_1_t8698_IList_1_set_Item_m48998_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Display_t1040_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48998_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48998_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8698_IList_1_set_Item_m48998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48998_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8698_MethodInfos[] =
{
	&IList_1_IndexOf_m48999_MethodInfo,
	&IList_1_Insert_m49000_MethodInfo,
	&IList_1_RemoveAt_m49001_MethodInfo,
	&IList_1_get_Item_m48997_MethodInfo,
	&IList_1_set_Item_m48998_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8698_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8697_il2cpp_TypeInfo,
	&IEnumerable_1_t8699_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8698_0_0_0;
extern Il2CppType IList_1_t8698_1_0_0;
struct IList_1_t8698;
extern Il2CppGenericClass IList_1_t8698_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8698_MethodInfos/* methods */
	, IList_1_t8698_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8698_il2cpp_TypeInfo/* element_class */
	, IList_1_t8698_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8698_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8698_0_0_0/* byval_arg */
	, &IList_1_t8698_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8698_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6859_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IntPtr>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IntPtr>
extern MethodInfo IEnumerator_1_get_Current_m49002_MethodInfo;
static PropertyInfo IEnumerator_1_t6859____Current_PropertyInfo = 
{
	&IEnumerator_1_t6859_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49002_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6859_PropertyInfos[] =
{
	&IEnumerator_1_t6859____Current_PropertyInfo,
	NULL
};
extern Il2CppType IntPtr_t121_0_0_0;
extern void* RuntimeInvoker_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49002_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IntPtr>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49002_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6859_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t121_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t121/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49002_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6859_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49002_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6859_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6859_0_0_0;
extern Il2CppType IEnumerator_1_t6859_1_0_0;
struct IEnumerator_1_t6859;
extern Il2CppGenericClass IEnumerator_1_t6859_GenericClass;
TypeInfo IEnumerator_1_t6859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6859_MethodInfos/* methods */
	, IEnumerator_1_t6859_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6859_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6859_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6859_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6859_0_0_0/* byval_arg */
	, &IEnumerator_1_t6859_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6859_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IntPtr>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_443.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4783_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IntPtr>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_443MethodDeclarations.h"

extern TypeInfo IntPtr_t121_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28956_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIntPtr_t121_m38441_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
 IntPtr_t121 Array_InternalArray__get_Item_TisIntPtr_t121_m38441 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28952_MethodInfo;
 void InternalEnumerator_1__ctor_m28952 (InternalEnumerator_1_t4783 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953 (InternalEnumerator_1_t4783 * __this, MethodInfo* method){
	{
		IntPtr_t121 L_0 = InternalEnumerator_1_get_Current_m28956(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28956_MethodInfo);
		IntPtr_t121 L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&IntPtr_t121_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28954_MethodInfo;
 void InternalEnumerator_1_Dispose_m28954 (InternalEnumerator_1_t4783 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28955_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28955 (InternalEnumerator_1_t4783 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
 IntPtr_t121 InternalEnumerator_1_get_Current_m28956 (InternalEnumerator_1_t4783 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		IntPtr_t121 L_8 = Array_InternalArray__get_Item_TisIntPtr_t121_m38441(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisIntPtr_t121_m38441_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IntPtr>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4783____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4783, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4783____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4783, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4783_FieldInfos[] =
{
	&InternalEnumerator_1_t4783____array_0_FieldInfo,
	&InternalEnumerator_1_t4783____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4783____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4783_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4783____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4783_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28956_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4783_PropertyInfos[] =
{
	&InternalEnumerator_1_t4783____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4783____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4783_InternalEnumerator_1__ctor_m28952_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28952_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28952_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28952/* method */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4783_InternalEnumerator_1__ctor_m28952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28952_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953/* method */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28954_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28954_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28954/* method */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28954_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28955_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28955_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28955/* method */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28955_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
extern void* RuntimeInvoker_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28956_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28956_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28956/* method */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t121_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t121/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28956_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4783_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28952_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_MethodInfo,
	&InternalEnumerator_1_Dispose_m28954_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28955_MethodInfo,
	&InternalEnumerator_1_get_Current_m28956_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4783_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28953_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28955_MethodInfo,
	&InternalEnumerator_1_Dispose_m28954_MethodInfo,
	&InternalEnumerator_1_get_Current_m28956_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4783_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6859_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4783_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6859_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4783_0_0_0;
extern Il2CppType InternalEnumerator_1_t4783_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4783_GenericClass;
TypeInfo InternalEnumerator_1_t4783_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4783_MethodInfos/* methods */
	, InternalEnumerator_1_t4783_PropertyInfos/* properties */
	, InternalEnumerator_1_t4783_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4783_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4783_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4783_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4783_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4783_1_0_0/* this_arg */
	, InternalEnumerator_1_t4783_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4783_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4783)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8700_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IntPtr>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IntPtr>
extern MethodInfo ICollection_1_get_Count_m49003_MethodInfo;
static PropertyInfo ICollection_1_t8700____Count_PropertyInfo = 
{
	&ICollection_1_t8700_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49003_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49004_MethodInfo;
static PropertyInfo ICollection_1_t8700____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8700_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49004_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8700_PropertyInfos[] =
{
	&ICollection_1_t8700____Count_PropertyInfo,
	&ICollection_1_t8700____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49003_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IntPtr>::get_Count()
MethodInfo ICollection_1_get_Count_m49003_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49003_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49004_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49004_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49004_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo ICollection_1_t8700_ICollection_1_Add_m49005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49005_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Add(T)
MethodInfo ICollection_1_Add_m49005_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_IntPtr_t121/* invoker_method */
	, ICollection_1_t8700_ICollection_1_Add_m49005_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49005_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49006_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Clear()
MethodInfo ICollection_1_Clear_m49006_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49006_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo ICollection_1_t8700_ICollection_1_Contains_m49007_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49007_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Contains(T)
MethodInfo ICollection_1_Contains_m49007_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_IntPtr_t121/* invoker_method */
	, ICollection_1_t8700_ICollection_1_Contains_m49007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49007_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtrU5BU5D_t1041_0_0_0;
extern Il2CppType IntPtrU5BU5D_t1041_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8700_ICollection_1_CopyTo_m49008_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IntPtrU5BU5D_t1041_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49008_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49008_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8700_ICollection_1_CopyTo_m49008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49008_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo ICollection_1_t8700_ICollection_1_Remove_m49009_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49009_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Remove(T)
MethodInfo ICollection_1_Remove_m49009_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_IntPtr_t121/* invoker_method */
	, ICollection_1_t8700_ICollection_1_Remove_m49009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49009_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8700_MethodInfos[] =
{
	&ICollection_1_get_Count_m49003_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49004_MethodInfo,
	&ICollection_1_Add_m49005_MethodInfo,
	&ICollection_1_Clear_m49006_MethodInfo,
	&ICollection_1_Contains_m49007_MethodInfo,
	&ICollection_1_CopyTo_m49008_MethodInfo,
	&ICollection_1_Remove_m49009_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8702_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8700_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8702_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8700_0_0_0;
extern Il2CppType ICollection_1_t8700_1_0_0;
struct ICollection_1_t8700;
extern Il2CppGenericClass ICollection_1_t8700_GenericClass;
TypeInfo ICollection_1_t8700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8700_MethodInfos/* methods */
	, ICollection_1_t8700_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8700_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8700_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8700_0_0_0/* byval_arg */
	, &ICollection_1_t8700_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8700_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IntPtr>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IntPtr>
extern Il2CppType IEnumerator_1_t6859_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49010_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IntPtr>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49010_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8702_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6859_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49010_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8702_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49010_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8702_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8702_0_0_0;
extern Il2CppType IEnumerable_1_t8702_1_0_0;
struct IEnumerable_1_t8702;
extern Il2CppGenericClass IEnumerable_1_t8702_GenericClass;
TypeInfo IEnumerable_1_t8702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8702_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8702_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8702_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8702_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8702_0_0_0/* byval_arg */
	, &IEnumerable_1_t8702_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8701_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IntPtr>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IntPtr>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IntPtr>
extern MethodInfo IList_1_get_Item_m49011_MethodInfo;
extern MethodInfo IList_1_set_Item_m49012_MethodInfo;
static PropertyInfo IList_1_t8701____Item_PropertyInfo = 
{
	&IList_1_t8701_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49011_MethodInfo/* get */
	, &IList_1_set_Item_m49012_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8701_PropertyInfos[] =
{
	&IList_1_t8701____Item_PropertyInfo,
	NULL
};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo IList_1_t8701_IList_1_IndexOf_m49013_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49013_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IntPtr>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49013_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8701_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_IntPtr_t121/* invoker_method */
	, IList_1_t8701_IList_1_IndexOf_m49013_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49013_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo IList_1_t8701_IList_1_Insert_m49014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49014_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49014_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_IntPtr_t121/* invoker_method */
	, IList_1_t8701_IList_1_Insert_m49014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49014_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8701_IList_1_RemoveAt_m49015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49015_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49015_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8701_IList_1_RemoveAt_m49015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49015_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8701_IList_1_get_Item_m49011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IntPtr_t121_0_0_0;
extern void* RuntimeInvoker_IntPtr_t121_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49011_GenericMethod;
// T System.Collections.Generic.IList`1<System.IntPtr>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49011_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8701_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t121_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t121_Int32_t123/* invoker_method */
	, IList_1_t8701_IList_1_get_Item_m49011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49011_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo IList_1_t8701_IList_1_set_Item_m49012_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49012_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49012_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_IntPtr_t121/* invoker_method */
	, IList_1_t8701_IList_1_set_Item_m49012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49012_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8701_MethodInfos[] =
{
	&IList_1_IndexOf_m49013_MethodInfo,
	&IList_1_Insert_m49014_MethodInfo,
	&IList_1_RemoveAt_m49015_MethodInfo,
	&IList_1_get_Item_m49011_MethodInfo,
	&IList_1_set_Item_m49012_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8701_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8700_il2cpp_TypeInfo,
	&IEnumerable_1_t8702_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8701_0_0_0;
extern Il2CppType IList_1_t8701_1_0_0;
struct IList_1_t8701;
extern Il2CppGenericClass IList_1_t8701_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8701_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8701_MethodInfos/* methods */
	, IList_1_t8701_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8701_il2cpp_TypeInfo/* element_class */
	, IList_1_t8701_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8701_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8701_0_0_0/* byval_arg */
	, &IList_1_t8701_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8701_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8703_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo ICollection_1_get_Count_m49016_MethodInfo;
static PropertyInfo ICollection_1_t8703____Count_PropertyInfo = 
{
	&ICollection_1_t8703_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49016_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49017_MethodInfo;
static PropertyInfo ICollection_1_t8703____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8703_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49017_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8703_PropertyInfos[] =
{
	&ICollection_1_t8703____Count_PropertyInfo,
	&ICollection_1_t8703____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49016_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_Count()
MethodInfo ICollection_1_get_Count_m49016_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49016_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49017_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49017_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49017_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t526_0_0_0;
extern Il2CppType ISerializable_t526_0_0_0;
static ParameterInfo ICollection_1_t8703_ICollection_1_Add_m49018_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t526_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49018_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Add(T)
MethodInfo ICollection_1_Add_m49018_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8703_ICollection_1_Add_m49018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49018_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49019_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Clear()
MethodInfo ICollection_1_Clear_m49019_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49019_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t526_0_0_0;
static ParameterInfo ICollection_1_t8703_ICollection_1_Contains_m49020_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t526_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49020_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Contains(T)
MethodInfo ICollection_1_Contains_m49020_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8703_ICollection_1_Contains_m49020_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49020_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializableU5BU5D_t5444_0_0_0;
extern Il2CppType ISerializableU5BU5D_t5444_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8703_ICollection_1_CopyTo_m49021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ISerializableU5BU5D_t5444_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49021_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49021_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8703_ICollection_1_CopyTo_m49021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49021_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t526_0_0_0;
static ParameterInfo ICollection_1_t8703_ICollection_1_Remove_m49022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t526_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49022_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Remove(T)
MethodInfo ICollection_1_Remove_m49022_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8703_ICollection_1_Remove_m49022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49022_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8703_MethodInfos[] =
{
	&ICollection_1_get_Count_m49016_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49017_MethodInfo,
	&ICollection_1_Add_m49018_MethodInfo,
	&ICollection_1_Clear_m49019_MethodInfo,
	&ICollection_1_Contains_m49020_MethodInfo,
	&ICollection_1_CopyTo_m49021_MethodInfo,
	&ICollection_1_Remove_m49022_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8705_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8703_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8705_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8703_0_0_0;
extern Il2CppType ICollection_1_t8703_1_0_0;
struct ICollection_1_t8703;
extern Il2CppGenericClass ICollection_1_t8703_GenericClass;
TypeInfo ICollection_1_t8703_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8703_MethodInfos/* methods */
	, ICollection_1_t8703_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8703_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8703_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8703_0_0_0/* byval_arg */
	, &ICollection_1_t8703_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8703_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>
extern Il2CppType IEnumerator_1_t6861_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49023_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49023_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8705_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6861_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49023_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8705_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49023_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8705_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8705_0_0_0;
extern Il2CppType IEnumerable_1_t8705_1_0_0;
struct IEnumerable_1_t8705;
extern Il2CppGenericClass IEnumerable_1_t8705_GenericClass;
TypeInfo IEnumerable_1_t8705_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8705_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8705_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8705_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8705_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8705_0_0_0/* byval_arg */
	, &IEnumerable_1_t8705_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8705_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6861_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo IEnumerator_1_get_Current_m49024_MethodInfo;
static PropertyInfo IEnumerator_1_t6861____Current_PropertyInfo = 
{
	&IEnumerator_1_t6861_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49024_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6861_PropertyInfos[] =
{
	&IEnumerator_1_t6861____Current_PropertyInfo,
	NULL
};
extern Il2CppType ISerializable_t526_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49024_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49024_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6861_il2cpp_TypeInfo/* declaring_type */
	, &ISerializable_t526_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49024_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6861_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49024_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6861_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6861_0_0_0;
extern Il2CppType IEnumerator_1_t6861_1_0_0;
struct IEnumerator_1_t6861;
extern Il2CppGenericClass IEnumerator_1_t6861_GenericClass;
TypeInfo IEnumerator_1_t6861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6861_MethodInfos/* methods */
	, IEnumerator_1_t6861_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6861_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6861_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6861_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6861_0_0_0/* byval_arg */
	, &IEnumerator_1_t6861_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6861_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_444.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4784_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_444MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m28961_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisISerializable_t526_m38452_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.ISerializable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.ISerializable>(System.Int32)
#define Array_InternalArray__get_Item_TisISerializable_t526_m38452(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4784____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4784, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4784____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4784, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4784_FieldInfos[] =
{
	&InternalEnumerator_1_t4784____array_0_FieldInfo,
	&InternalEnumerator_1_t4784____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4784____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4784_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4784____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4784_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28961_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4784_PropertyInfos[] =
{
	&InternalEnumerator_1_t4784____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4784____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4784_InternalEnumerator_1__ctor_m28957_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28957_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28957_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4784_InternalEnumerator_1__ctor_m28957_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28957_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28959_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28959_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28959_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28960_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28960_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28960_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t526_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28961_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28961_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* declaring_type */
	, &ISerializable_t526_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28961_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4784_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28957_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_MethodInfo,
	&InternalEnumerator_1_Dispose_m28959_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28960_MethodInfo,
	&InternalEnumerator_1_get_Current_m28961_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28960_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28959_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4784_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28958_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28960_MethodInfo,
	&InternalEnumerator_1_Dispose_m28959_MethodInfo,
	&InternalEnumerator_1_get_Current_m28961_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4784_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6861_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4784_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6861_il2cpp_TypeInfo, 7},
};
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4784_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28961_MethodInfo/* Method Usage */,
	&ISerializable_t526_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisISerializable_t526_m38452_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4784_0_0_0;
extern Il2CppType InternalEnumerator_1_t4784_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4784_GenericClass;
TypeInfo InternalEnumerator_1_t4784_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4784_MethodInfos/* methods */
	, InternalEnumerator_1_t4784_PropertyInfos/* properties */
	, InternalEnumerator_1_t4784_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4784_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4784_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4784_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4784_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4784_1_0_0/* this_arg */
	, InternalEnumerator_1_t4784_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4784_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4784_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4784)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8704_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo IList_1_get_Item_m49025_MethodInfo;
extern MethodInfo IList_1_set_Item_m49026_MethodInfo;
static PropertyInfo IList_1_t8704____Item_PropertyInfo = 
{
	&IList_1_t8704_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49025_MethodInfo/* get */
	, &IList_1_set_Item_m49026_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8704_PropertyInfos[] =
{
	&IList_1_t8704____Item_PropertyInfo,
	NULL
};
extern Il2CppType ISerializable_t526_0_0_0;
static ParameterInfo IList_1_t8704_IList_1_IndexOf_m49027_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t526_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49027_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49027_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8704_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8704_IList_1_IndexOf_m49027_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49027_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ISerializable_t526_0_0_0;
static ParameterInfo IList_1_t8704_IList_1_Insert_m49028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ISerializable_t526_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49028_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49028_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8704_IList_1_Insert_m49028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49028_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8704_IList_1_RemoveAt_m49029_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49029_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49029_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8704_IList_1_RemoveAt_m49029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49029_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8704_IList_1_get_Item_m49025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ISerializable_t526_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49025_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49025_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8704_il2cpp_TypeInfo/* declaring_type */
	, &ISerializable_t526_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8704_IList_1_get_Item_m49025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49025_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ISerializable_t526_0_0_0;
static ParameterInfo IList_1_t8704_IList_1_set_Item_m49026_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ISerializable_t526_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49026_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49026_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8704_IList_1_set_Item_m49026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49026_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8704_MethodInfos[] =
{
	&IList_1_IndexOf_m49027_MethodInfo,
	&IList_1_Insert_m49028_MethodInfo,
	&IList_1_RemoveAt_m49029_MethodInfo,
	&IList_1_get_Item_m49025_MethodInfo,
	&IList_1_set_Item_m49026_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8704_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8703_il2cpp_TypeInfo,
	&IEnumerable_1_t8705_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8704_0_0_0;
extern Il2CppType IList_1_t8704_1_0_0;
struct IList_1_t8704;
extern Il2CppGenericClass IList_1_t8704_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8704_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8704_MethodInfos/* methods */
	, IList_1_t8704_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8704_il2cpp_TypeInfo/* element_class */
	, IList_1_t8704_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8704_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8704_0_0_0/* byval_arg */
	, &IList_1_t8704_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8704_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_157.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4785_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_157MethodDeclarations.h"

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_159.h"
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4786_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_159MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28964_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28966_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4785____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4785_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4785, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4785_FieldInfos[] =
{
	&CachedInvokableCall_1_t4785____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MonoBehaviour_t10_0_0_0;
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4785_CachedInvokableCall_1__ctor_m28962_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28962_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28962_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4785_CachedInvokableCall_1__ctor_m28962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28962_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4785_CachedInvokableCall_1_Invoke_m28963_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28963_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28963_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4785_CachedInvokableCall_1_Invoke_m28963_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28963_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4785_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28962_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28963_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28963_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28967_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4785_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28963_MethodInfo,
	&InvokableCall_1_Find_m28967_MethodInfo,
};
extern Il2CppType UnityAction_1_t4787_0_0_0;
extern TypeInfo UnityAction_1_t4787_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t10_m38462_MethodInfo;
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28969_MethodInfo;
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4785_RGCTXData[8] = 
{
	&UnityAction_1_t4787_0_0_0/* Type Usage */,
	&UnityAction_1_t4787_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t10_m38462_MethodInfo/* Method Usage */,
	&MonoBehaviour_t10_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28969_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28964_MethodInfo/* Method Usage */,
	&MonoBehaviour_t10_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28966_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4785_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4785_1_0_0;
struct CachedInvokableCall_1_t4785;
extern Il2CppGenericClass CachedInvokableCall_1_t4785_GenericClass;
TypeInfo CachedInvokableCall_1_t4785_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4785_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4785_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4785_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4785_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4785_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4785_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4785_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4785_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4785_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4785)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_164.h"
extern TypeInfo UnityAction_1_t4787_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_164MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.MonoBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.MonoBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t10_m38462(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
extern Il2CppType UnityAction_1_t4787_0_0_1;
FieldInfo InvokableCall_1_t4786____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4787_0_0_1/* type */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4786, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4786_FieldInfos[] =
{
	&InvokableCall_1_t4786____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4786_InvokableCall_1__ctor_m28964_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28964_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28964_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4786_InvokableCall_1__ctor_m28964_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28964_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4787_0_0_0;
static ParameterInfo InvokableCall_1_t4786_InvokableCall_1__ctor_m28965_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4787_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28965_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28965_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4786_InvokableCall_1__ctor_m28965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28965_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4786_InvokableCall_1_Invoke_m28966_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28966_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28966_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4786_InvokableCall_1_Invoke_m28966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28966_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4786_InvokableCall_1_Find_m28967_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28967_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28967_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4786_InvokableCall_1_Find_m28967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28967_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4786_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28964_MethodInfo,
	&InvokableCall_1__ctor_m28965_MethodInfo,
	&InvokableCall_1_Invoke_m28966_MethodInfo,
	&InvokableCall_1_Find_m28967_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4786_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m28966_MethodInfo,
	&InvokableCall_1_Find_m28967_MethodInfo,
};
extern TypeInfo UnityAction_1_t4787_il2cpp_TypeInfo;
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4786_RGCTXData[5] = 
{
	&UnityAction_1_t4787_0_0_0/* Type Usage */,
	&UnityAction_1_t4787_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t10_m38462_MethodInfo/* Method Usage */,
	&MonoBehaviour_t10_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28969_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4786_0_0_0;
extern Il2CppType InvokableCall_1_t4786_1_0_0;
struct InvokableCall_1_t4786;
extern Il2CppGenericClass InvokableCall_1_t4786_GenericClass;
TypeInfo InvokableCall_1_t4786_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4786_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4786_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4786_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4786_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4786_0_0_0/* byval_arg */
	, &InvokableCall_1_t4786_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4786_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4786_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4786)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4787_UnityAction_1__ctor_m28968_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28968_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28968_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4787_UnityAction_1__ctor_m28968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28968_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo UnityAction_1_t4787_UnityAction_1_Invoke_m28969_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28969_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28969_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4787_UnityAction_1_Invoke_m28969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28969_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t10_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4787_UnityAction_1_BeginInvoke_m28970_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28970_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28970_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4787_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4787_UnityAction_1_BeginInvoke_m28970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28970_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4787_UnityAction_1_EndInvoke_m28971_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28971_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28971_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4787_UnityAction_1_EndInvoke_m28971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28971_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4787_MethodInfos[] =
{
	&UnityAction_1__ctor_m28968_MethodInfo,
	&UnityAction_1_Invoke_m28969_MethodInfo,
	&UnityAction_1_BeginInvoke_m28970_MethodInfo,
	&UnityAction_1_EndInvoke_m28971_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28970_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28971_MethodInfo;
static MethodInfo* UnityAction_1_t4787_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m28969_MethodInfo,
	&UnityAction_1_BeginInvoke_m28970_MethodInfo,
	&UnityAction_1_EndInvoke_m28971_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4787_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4787_1_0_0;
struct UnityAction_1_t4787;
extern Il2CppGenericClass UnityAction_1_t4787_GenericClass;
TypeInfo UnityAction_1_t4787_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4787_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4787_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4787_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4787_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4787_0_0_0/* byval_arg */
	, &UnityAction_1_t4787_1_0_0/* this_arg */
	, UnityAction_1_t4787_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4787_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4787)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6863_il2cpp_TypeInfo;

// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>
extern MethodInfo IEnumerator_1_get_Current_m49030_MethodInfo;
static PropertyInfo IEnumerator_1_t6863____Current_PropertyInfo = 
{
	&IEnumerator_1_t6863_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49030_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6863_PropertyInfos[] =
{
	&IEnumerator_1_t6863____Current_PropertyInfo,
	NULL
};
extern Il2CppType TouchPhase_t1043_0_0_0;
extern void* RuntimeInvoker_TouchPhase_t1043 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49030_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49030_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6863_il2cpp_TypeInfo/* declaring_type */
	, &TouchPhase_t1043_0_0_0/* return_type */
	, RuntimeInvoker_TouchPhase_t1043/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49030_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6863_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49030_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6863_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6863_0_0_0;
extern Il2CppType IEnumerator_1_t6863_1_0_0;
struct IEnumerator_1_t6863;
extern Il2CppGenericClass IEnumerator_1_t6863_GenericClass;
TypeInfo IEnumerator_1_t6863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6863_MethodInfos/* methods */
	, IEnumerator_1_t6863_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6863_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6863_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6863_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6863_0_0_0/* byval_arg */
	, &IEnumerator_1_t6863_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6863_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_445.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4788_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_445MethodDeclarations.h"

extern TypeInfo TouchPhase_t1043_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28976_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTouchPhase_t1043_m38464_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TouchPhase>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TouchPhase>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTouchPhase_t1043_m38464 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28972_MethodInfo;
 void InternalEnumerator_1__ctor_m28972 (InternalEnumerator_1_t4788 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973 (InternalEnumerator_1_t4788 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28976(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28976_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TouchPhase_t1043_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28974_MethodInfo;
 void InternalEnumerator_1_Dispose_m28974 (InternalEnumerator_1_t4788 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28975_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28975 (InternalEnumerator_1_t4788 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28976 (InternalEnumerator_1_t4788 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTouchPhase_t1043_m38464(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTouchPhase_t1043_m38464_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4788____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4788, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4788____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4788, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4788_FieldInfos[] =
{
	&InternalEnumerator_1_t4788____array_0_FieldInfo,
	&InternalEnumerator_1_t4788____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4788____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4788_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4788____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4788_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28976_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4788_PropertyInfos[] =
{
	&InternalEnumerator_1_t4788____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4788____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4788_InternalEnumerator_1__ctor_m28972_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28972_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28972_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28972/* method */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4788_InternalEnumerator_1__ctor_m28972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28972_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973/* method */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28974_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28974_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28974/* method */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28974_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28975_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28975_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28975/* method */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28975_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t1043_0_0_0;
extern void* RuntimeInvoker_TouchPhase_t1043 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28976_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28976_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28976/* method */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* declaring_type */
	, &TouchPhase_t1043_0_0_0/* return_type */
	, RuntimeInvoker_TouchPhase_t1043/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28976_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4788_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28972_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_MethodInfo,
	&InternalEnumerator_1_Dispose_m28974_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28975_MethodInfo,
	&InternalEnumerator_1_get_Current_m28976_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4788_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28973_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28975_MethodInfo,
	&InternalEnumerator_1_Dispose_m28974_MethodInfo,
	&InternalEnumerator_1_get_Current_m28976_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4788_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6863_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4788_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6863_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4788_0_0_0;
extern Il2CppType InternalEnumerator_1_t4788_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4788_GenericClass;
TypeInfo InternalEnumerator_1_t4788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4788_MethodInfos/* methods */
	, InternalEnumerator_1_t4788_PropertyInfos/* properties */
	, InternalEnumerator_1_t4788_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4788_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4788_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4788_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4788_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4788_1_0_0/* this_arg */
	, InternalEnumerator_1_t4788_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4788_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4788)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8706_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>
extern MethodInfo ICollection_1_get_Count_m49031_MethodInfo;
static PropertyInfo ICollection_1_t8706____Count_PropertyInfo = 
{
	&ICollection_1_t8706_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49031_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49032_MethodInfo;
static PropertyInfo ICollection_1_t8706____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8706_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49032_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8706_PropertyInfos[] =
{
	&ICollection_1_t8706____Count_PropertyInfo,
	&ICollection_1_t8706____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49031_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_Count()
MethodInfo ICollection_1_get_Count_m49031_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49031_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49032_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49032_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49032_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t1043_0_0_0;
extern Il2CppType TouchPhase_t1043_0_0_0;
static ParameterInfo ICollection_1_t8706_ICollection_1_Add_m49033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t1043_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49033_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Add(T)
MethodInfo ICollection_1_Add_m49033_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8706_ICollection_1_Add_m49033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49033_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49034_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Clear()
MethodInfo ICollection_1_Clear_m49034_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49034_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t1043_0_0_0;
static ParameterInfo ICollection_1_t8706_ICollection_1_Contains_m49035_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t1043_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49035_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Contains(T)
MethodInfo ICollection_1_Contains_m49035_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8706_ICollection_1_Contains_m49035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49035_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhaseU5BU5D_t5713_0_0_0;
extern Il2CppType TouchPhaseU5BU5D_t5713_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8706_ICollection_1_CopyTo_m49036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhaseU5BU5D_t5713_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49036_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49036_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8706_ICollection_1_CopyTo_m49036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49036_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t1043_0_0_0;
static ParameterInfo ICollection_1_t8706_ICollection_1_Remove_m49037_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t1043_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49037_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Remove(T)
MethodInfo ICollection_1_Remove_m49037_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8706_ICollection_1_Remove_m49037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49037_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8706_MethodInfos[] =
{
	&ICollection_1_get_Count_m49031_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49032_MethodInfo,
	&ICollection_1_Add_m49033_MethodInfo,
	&ICollection_1_Clear_m49034_MethodInfo,
	&ICollection_1_Contains_m49035_MethodInfo,
	&ICollection_1_CopyTo_m49036_MethodInfo,
	&ICollection_1_Remove_m49037_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8708_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8706_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8708_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8706_0_0_0;
extern Il2CppType ICollection_1_t8706_1_0_0;
struct ICollection_1_t8706;
extern Il2CppGenericClass ICollection_1_t8706_GenericClass;
TypeInfo ICollection_1_t8706_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8706_MethodInfos/* methods */
	, ICollection_1_t8706_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8706_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8706_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8706_0_0_0/* byval_arg */
	, &ICollection_1_t8706_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8706_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>
extern Il2CppType IEnumerator_1_t6863_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49038_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49038_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8708_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6863_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49038_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8708_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49038_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8708_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8708_0_0_0;
extern Il2CppType IEnumerable_1_t8708_1_0_0;
struct IEnumerable_1_t8708;
extern Il2CppGenericClass IEnumerable_1_t8708_GenericClass;
TypeInfo IEnumerable_1_t8708_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8708_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8708_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8708_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8708_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8708_0_0_0/* byval_arg */
	, &IEnumerable_1_t8708_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8708_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8707_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TouchPhase>
extern MethodInfo IList_1_get_Item_m49039_MethodInfo;
extern MethodInfo IList_1_set_Item_m49040_MethodInfo;
static PropertyInfo IList_1_t8707____Item_PropertyInfo = 
{
	&IList_1_t8707_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49039_MethodInfo/* get */
	, &IList_1_set_Item_m49040_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8707_PropertyInfos[] =
{
	&IList_1_t8707____Item_PropertyInfo,
	NULL
};
extern Il2CppType TouchPhase_t1043_0_0_0;
static ParameterInfo IList_1_t8707_IList_1_IndexOf_m49041_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t1043_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49041_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49041_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8707_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8707_IList_1_IndexOf_m49041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49041_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TouchPhase_t1043_0_0_0;
static ParameterInfo IList_1_t8707_IList_1_Insert_m49042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t1043_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49042_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49042_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8707_IList_1_Insert_m49042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49042_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8707_IList_1_RemoveAt_m49043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49043_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49043_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8707_IList_1_RemoveAt_m49043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49043_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8707_IList_1_get_Item_m49039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TouchPhase_t1043_0_0_0;
extern void* RuntimeInvoker_TouchPhase_t1043_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49039_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49039_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8707_il2cpp_TypeInfo/* declaring_type */
	, &TouchPhase_t1043_0_0_0/* return_type */
	, RuntimeInvoker_TouchPhase_t1043_Int32_t123/* invoker_method */
	, IList_1_t8707_IList_1_get_Item_m49039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49039_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TouchPhase_t1043_0_0_0;
static ParameterInfo IList_1_t8707_IList_1_set_Item_m49040_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t1043_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49040_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49040_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8707_IList_1_set_Item_m49040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49040_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8707_MethodInfos[] =
{
	&IList_1_IndexOf_m49041_MethodInfo,
	&IList_1_Insert_m49042_MethodInfo,
	&IList_1_RemoveAt_m49043_MethodInfo,
	&IList_1_get_Item_m49039_MethodInfo,
	&IList_1_set_Item_m49040_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8707_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8706_il2cpp_TypeInfo,
	&IEnumerable_1_t8708_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8707_0_0_0;
extern Il2CppType IList_1_t8707_1_0_0;
struct IList_1_t8707;
extern Il2CppGenericClass IList_1_t8707_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8707_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8707_MethodInfos/* methods */
	, IList_1_t8707_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8707_il2cpp_TypeInfo/* element_class */
	, IList_1_t8707_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8707_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8707_0_0_0/* byval_arg */
	, &IList_1_t8707_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8707_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6865_il2cpp_TypeInfo;

// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>
extern MethodInfo IEnumerator_1_get_Current_m49044_MethodInfo;
static PropertyInfo IEnumerator_1_t6865____Current_PropertyInfo = 
{
	&IEnumerator_1_t6865_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49044_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6865_PropertyInfos[] =
{
	&IEnumerator_1_t6865____Current_PropertyInfo,
	NULL
};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
extern void* RuntimeInvoker_IMECompositionMode_t1044 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49044_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49044_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6865_il2cpp_TypeInfo/* declaring_type */
	, &IMECompositionMode_t1044_0_0_0/* return_type */
	, RuntimeInvoker_IMECompositionMode_t1044/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49044_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6865_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49044_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6865_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6865_0_0_0;
extern Il2CppType IEnumerator_1_t6865_1_0_0;
struct IEnumerator_1_t6865;
extern Il2CppGenericClass IEnumerator_1_t6865_GenericClass;
TypeInfo IEnumerator_1_t6865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6865_MethodInfos/* methods */
	, IEnumerator_1_t6865_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6865_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6865_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6865_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6865_0_0_0/* byval_arg */
	, &IEnumerator_1_t6865_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6865_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_446.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4789_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_446MethodDeclarations.h"

extern TypeInfo IMECompositionMode_t1044_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28981_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIMECompositionMode_t1044_m38475_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.IMECompositionMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.IMECompositionMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisIMECompositionMode_t1044_m38475 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28977_MethodInfo;
 void InternalEnumerator_1__ctor_m28977 (InternalEnumerator_1_t4789 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978 (InternalEnumerator_1_t4789 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28981(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28981_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&IMECompositionMode_t1044_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28979_MethodInfo;
 void InternalEnumerator_1_Dispose_m28979 (InternalEnumerator_1_t4789 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28980_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28980 (InternalEnumerator_1_t4789 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28981 (InternalEnumerator_1_t4789 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisIMECompositionMode_t1044_m38475(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisIMECompositionMode_t1044_m38475_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4789____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4789, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4789____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4789, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4789_FieldInfos[] =
{
	&InternalEnumerator_1_t4789____array_0_FieldInfo,
	&InternalEnumerator_1_t4789____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4789____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4789_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4789____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4789_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28981_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4789_PropertyInfos[] =
{
	&InternalEnumerator_1_t4789____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4789____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4789_InternalEnumerator_1__ctor_m28977_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28977_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28977_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28977/* method */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4789_InternalEnumerator_1__ctor_m28977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28977_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978/* method */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28979_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28979_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28979/* method */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28979_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28980_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28980_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28980/* method */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28980_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
extern void* RuntimeInvoker_IMECompositionMode_t1044 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28981_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28981_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28981/* method */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* declaring_type */
	, &IMECompositionMode_t1044_0_0_0/* return_type */
	, RuntimeInvoker_IMECompositionMode_t1044/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28981_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4789_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28977_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_MethodInfo,
	&InternalEnumerator_1_Dispose_m28979_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28980_MethodInfo,
	&InternalEnumerator_1_get_Current_m28981_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4789_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28978_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28980_MethodInfo,
	&InternalEnumerator_1_Dispose_m28979_MethodInfo,
	&InternalEnumerator_1_get_Current_m28981_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4789_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6865_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4789_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6865_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4789_0_0_0;
extern Il2CppType InternalEnumerator_1_t4789_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4789_GenericClass;
TypeInfo InternalEnumerator_1_t4789_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4789_MethodInfos/* methods */
	, InternalEnumerator_1_t4789_PropertyInfos/* properties */
	, InternalEnumerator_1_t4789_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4789_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4789_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4789_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4789_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4789_1_0_0/* this_arg */
	, InternalEnumerator_1_t4789_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4789_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4789)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8709_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>
extern MethodInfo ICollection_1_get_Count_m49045_MethodInfo;
static PropertyInfo ICollection_1_t8709____Count_PropertyInfo = 
{
	&ICollection_1_t8709_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49046_MethodInfo;
static PropertyInfo ICollection_1_t8709____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8709_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49046_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8709_PropertyInfos[] =
{
	&ICollection_1_t8709____Count_PropertyInfo,
	&ICollection_1_t8709____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49045_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49045_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49045_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49046_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49046_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49046_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
extern Il2CppType IMECompositionMode_t1044_0_0_0;
static ParameterInfo ICollection_1_t8709_ICollection_1_Add_m49047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t1044_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49047_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Add(T)
MethodInfo ICollection_1_Add_m49047_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8709_ICollection_1_Add_m49047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49047_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49048_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Clear()
MethodInfo ICollection_1_Clear_m49048_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49048_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
static ParameterInfo ICollection_1_t8709_ICollection_1_Contains_m49049_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t1044_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49049_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49049_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8709_ICollection_1_Contains_m49049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49049_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionModeU5BU5D_t5714_0_0_0;
extern Il2CppType IMECompositionModeU5BU5D_t5714_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8709_ICollection_1_CopyTo_m49050_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionModeU5BU5D_t5714_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49050_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49050_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8709_ICollection_1_CopyTo_m49050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49050_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
static ParameterInfo ICollection_1_t8709_ICollection_1_Remove_m49051_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t1044_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49051_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49051_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8709_ICollection_1_Remove_m49051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49051_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8709_MethodInfos[] =
{
	&ICollection_1_get_Count_m49045_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49046_MethodInfo,
	&ICollection_1_Add_m49047_MethodInfo,
	&ICollection_1_Clear_m49048_MethodInfo,
	&ICollection_1_Contains_m49049_MethodInfo,
	&ICollection_1_CopyTo_m49050_MethodInfo,
	&ICollection_1_Remove_m49051_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8711_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8709_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8711_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8709_0_0_0;
extern Il2CppType ICollection_1_t8709_1_0_0;
struct ICollection_1_t8709;
extern Il2CppGenericClass ICollection_1_t8709_GenericClass;
TypeInfo ICollection_1_t8709_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8709_MethodInfos/* methods */
	, ICollection_1_t8709_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8709_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8709_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8709_0_0_0/* byval_arg */
	, &ICollection_1_t8709_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8709_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>
extern Il2CppType IEnumerator_1_t6865_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49052_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49052_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8711_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6865_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49052_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8711_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49052_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8711_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8711_0_0_0;
extern Il2CppType IEnumerable_1_t8711_1_0_0;
struct IEnumerable_1_t8711;
extern Il2CppGenericClass IEnumerable_1_t8711_GenericClass;
TypeInfo IEnumerable_1_t8711_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8711_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8711_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8711_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8711_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8711_0_0_0/* byval_arg */
	, &IEnumerable_1_t8711_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8711_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8710_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>
extern MethodInfo IList_1_get_Item_m49053_MethodInfo;
extern MethodInfo IList_1_set_Item_m49054_MethodInfo;
static PropertyInfo IList_1_t8710____Item_PropertyInfo = 
{
	&IList_1_t8710_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49053_MethodInfo/* get */
	, &IList_1_set_Item_m49054_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8710_PropertyInfos[] =
{
	&IList_1_t8710____Item_PropertyInfo,
	NULL
};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
static ParameterInfo IList_1_t8710_IList_1_IndexOf_m49055_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t1044_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49055_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49055_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8710_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8710_IList_1_IndexOf_m49055_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49055_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IMECompositionMode_t1044_0_0_0;
static ParameterInfo IList_1_t8710_IList_1_Insert_m49056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t1044_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49056_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49056_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8710_IList_1_Insert_m49056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49056_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8710_IList_1_RemoveAt_m49057_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49057_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49057_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8710_IList_1_RemoveAt_m49057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49057_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8710_IList_1_get_Item_m49053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IMECompositionMode_t1044_0_0_0;
extern void* RuntimeInvoker_IMECompositionMode_t1044_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49053_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49053_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8710_il2cpp_TypeInfo/* declaring_type */
	, &IMECompositionMode_t1044_0_0_0/* return_type */
	, RuntimeInvoker_IMECompositionMode_t1044_Int32_t123/* invoker_method */
	, IList_1_t8710_IList_1_get_Item_m49053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49053_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IMECompositionMode_t1044_0_0_0;
static ParameterInfo IList_1_t8710_IList_1_set_Item_m49054_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t1044_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49054_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49054_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8710_IList_1_set_Item_m49054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49054_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8710_MethodInfos[] =
{
	&IList_1_IndexOf_m49055_MethodInfo,
	&IList_1_Insert_m49056_MethodInfo,
	&IList_1_RemoveAt_m49057_MethodInfo,
	&IList_1_get_Item_m49053_MethodInfo,
	&IList_1_set_Item_m49054_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8710_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8709_il2cpp_TypeInfo,
	&IEnumerable_1_t8711_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8710_0_0_0;
extern Il2CppType IList_1_t8710_1_0_0;
struct IList_1_t8710;
extern Il2CppGenericClass IList_1_t8710_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8710_MethodInfos/* methods */
	, IList_1_t8710_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8710_il2cpp_TypeInfo/* element_class */
	, IList_1_t8710_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8710_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8710_0_0_0/* byval_arg */
	, &IList_1_t8710_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8710_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6867_il2cpp_TypeInfo;

// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>
extern MethodInfo IEnumerator_1_get_Current_m49058_MethodInfo;
static PropertyInfo IEnumerator_1_t6867____Current_PropertyInfo = 
{
	&IEnumerator_1_t6867_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49058_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6867_PropertyInfos[] =
{
	&IEnumerator_1_t6867____Current_PropertyInfo,
	NULL
};
extern Il2CppType HideFlags_t1045_0_0_0;
extern void* RuntimeInvoker_HideFlags_t1045 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49058_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49058_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6867_il2cpp_TypeInfo/* declaring_type */
	, &HideFlags_t1045_0_0_0/* return_type */
	, RuntimeInvoker_HideFlags_t1045/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49058_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6867_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49058_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6867_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6867_0_0_0;
extern Il2CppType IEnumerator_1_t6867_1_0_0;
struct IEnumerator_1_t6867;
extern Il2CppGenericClass IEnumerator_1_t6867_GenericClass;
TypeInfo IEnumerator_1_t6867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6867_MethodInfos/* methods */
	, IEnumerator_1_t6867_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6867_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6867_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6867_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6867_0_0_0/* byval_arg */
	, &IEnumerator_1_t6867_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6867_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
