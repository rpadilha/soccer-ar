﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.Trackable>
struct List_1_t834;
// Vuforia.Trackable
struct Trackable_t594;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
struct Enumerator_t835 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::l
	List_1_t834 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::current
	Object_t * ___current_3;
};
