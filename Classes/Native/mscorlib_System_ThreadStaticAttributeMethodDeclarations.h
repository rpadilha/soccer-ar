﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t2302;

// System.Void System.ThreadStaticAttribute::.ctor()
 void ThreadStaticAttribute__ctor_m13370 (ThreadStaticAttribute_t2302 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
