﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1360;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t1358;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
 void KeysCollection__ctor_m6944 (KeysCollection_t1360 * __this, NameObjectCollectionBase_t1358 * ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void KeysCollection_System_Collections_ICollection_CopyTo_m6945 (KeysCollection_t1360 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_IsSynchronized()
 bool KeysCollection_System_Collections_ICollection_get_IsSynchronized_m6946 (KeysCollection_t1360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_SyncRoot()
 Object_t * KeysCollection_System_Collections_ICollection_get_SyncRoot_m6947 (KeysCollection_t1360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::get_Count()
 int32_t KeysCollection_get_Count_m6948 (KeysCollection_t1360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::GetEnumerator()
 Object_t * KeysCollection_GetEnumerator_m6949 (KeysCollection_t1360 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
