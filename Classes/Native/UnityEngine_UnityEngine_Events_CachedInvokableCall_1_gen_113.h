﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_115.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct CachedInvokableCall_1_t3876  : public InvokableCall_1_t3877
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
