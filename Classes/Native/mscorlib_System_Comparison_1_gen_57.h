﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2186;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Security.Policy.StrongName>
struct Comparison_1_t5316  : public MulticastDelegate_t373
{
};
