﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t1032;
struct CacheIndex_t1032_marshaled;

void CacheIndex_t1032_marshal(const CacheIndex_t1032& unmarshaled, CacheIndex_t1032_marshaled& marshaled);
void CacheIndex_t1032_marshal_back(const CacheIndex_t1032_marshaled& marshaled, CacheIndex_t1032& unmarshaled);
void CacheIndex_t1032_marshal_cleanup(CacheIndex_t1032_marshaled& marshaled);
