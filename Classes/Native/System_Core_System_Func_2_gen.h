﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t418;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t421  : public MulticastDelegate_t373
{
};
