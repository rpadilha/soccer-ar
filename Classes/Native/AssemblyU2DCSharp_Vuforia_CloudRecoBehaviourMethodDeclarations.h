﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t3;

// System.Void Vuforia.CloudRecoBehaviour::.ctor()
 void CloudRecoBehaviour__ctor_m1 (CloudRecoBehaviour_t3 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
