﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>
struct UnityAction_1_t2888;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>
struct InvokableCall_1_t2887  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::Delegate
	UnityAction_1_t2888 * ___Delegate_0;
};
