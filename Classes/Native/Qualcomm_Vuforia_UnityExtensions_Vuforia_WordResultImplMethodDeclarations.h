﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordResultImpl
struct WordResultImpl_t748;
// Vuforia.Word
struct Word_t736;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"

// System.Void Vuforia.WordResultImpl::.ctor(Vuforia.Word)
 void WordResultImpl__ctor_m3286 (WordResultImpl_t748 * __this, Object_t * ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordResultImpl::get_Word()
 Object_t * WordResultImpl_get_Word_m3287 (WordResultImpl_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.WordResultImpl::get_Position()
 Vector3_t73  WordResultImpl_get_Position_m3288 (WordResultImpl_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.WordResultImpl::get_Orientation()
 Quaternion_t108  WordResultImpl_get_Orientation_m3289 (WordResultImpl_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.OrientedBoundingBox Vuforia.WordResultImpl::get_Obb()
 OrientedBoundingBox_t634  WordResultImpl_get_Obb_m3290 (WordResultImpl_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableBehaviour/Status Vuforia.WordResultImpl::get_CurrentStatus()
 int32_t WordResultImpl_get_CurrentStatus_m3291 (WordResultImpl_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordResultImpl::SetPose(UnityEngine.Vector3,UnityEngine.Quaternion)
 void WordResultImpl_SetPose_m3292 (WordResultImpl_t748 * __this, Vector3_t73  ___position, Quaternion_t108  ___orientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordResultImpl::SetObb(Vuforia.OrientedBoundingBox)
 void WordResultImpl_SetObb_m3293 (WordResultImpl_t748 * __this, OrientedBoundingBox_t634  ___obb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordResultImpl::SetStatus(Vuforia.TrackableBehaviour/Status)
 void WordResultImpl_SetStatus_m3294 (WordResultImpl_t748 * __this, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
