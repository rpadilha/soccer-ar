﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
struct InternalEnumerator_1_t4932;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29824 (InternalEnumerator_1_t4932 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29825 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
 void InternalEnumerator_1_Dispose_m29826 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29827 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
 ParameterModifier_t1217  InternalEnumerator_1_get_Current_m29828 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
