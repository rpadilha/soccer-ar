﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>
struct Collection_1_t4026;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t610;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t4016;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSetImpl>
struct IEnumerator_1_t4018;
// System.Collections.Generic.IList`1<Vuforia.DataSetImpl>
struct IList_1_t4025;

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m22546(__this, method) (void)Collection_1__ctor_m14594_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22547(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14595_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m22548(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14596_gshared((Collection_1_t2839 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22549(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14597_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m22550(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14598_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m22551(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14599_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m22552(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14600_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m22553(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14601_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m22554(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14602_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22555(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14603_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22556(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14604_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22557(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14605_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22558(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14606_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m22559(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14607_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m22560(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14608_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::Add(T)
#define Collection_1_Add_m22561(__this, ___item, method) (void)Collection_1_Add_m14609_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::Clear()
#define Collection_1_Clear_m22562(__this, method) (void)Collection_1_Clear_m14610_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::ClearItems()
#define Collection_1_ClearItems_m22563(__this, method) (void)Collection_1_ClearItems_m14611_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::Contains(T)
#define Collection_1_Contains_m22564(__this, ___item, method) (bool)Collection_1_Contains_m14612_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m22565(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14613_gshared((Collection_1_t2839 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::GetEnumerator()
#define Collection_1_GetEnumerator_m22566(__this, method) (Object_t*)Collection_1_GetEnumerator_m14614_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::IndexOf(T)
#define Collection_1_IndexOf_m22567(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14615_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::Insert(System.Int32,T)
#define Collection_1_Insert_m22568(__this, ___index, ___item, method) (void)Collection_1_Insert_m14616_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m22569(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14617_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::Remove(T)
#define Collection_1_Remove_m22570(__this, ___item, method) (bool)Collection_1_Remove_m14618_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m22571(__this, ___index, method) (void)Collection_1_RemoveAt_m14619_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m22572(__this, ___index, method) (void)Collection_1_RemoveItem_m14620_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::get_Count()
#define Collection_1_get_Count_m22573(__this, method) (int32_t)Collection_1_get_Count_m14621_gshared((Collection_1_t2839 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::get_Item(System.Int32)
#define Collection_1_get_Item_m22574(__this, ___index, method) (DataSetImpl_t610 *)Collection_1_get_Item_m14622_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m22575(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14623_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m22576(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14624_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m22577(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14625_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m22578(__this/* static, unused */, ___item, method) (DataSetImpl_t610 *)Collection_1_ConvertItem_m14626_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m22579(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14627_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m22580(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14628_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSetImpl>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m22581(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14629_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
