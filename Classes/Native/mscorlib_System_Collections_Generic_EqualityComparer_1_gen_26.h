﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct EqualityComparer_1_t3851;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct EqualityComparer_1_t3851  : public Object_t
{
};
struct EqualityComparer_1_t3851_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ReconstructionAbstractBehaviour>::_default
	EqualityComparer_1_t3851 * ____default_0;
};
