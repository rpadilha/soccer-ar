﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerTracker
struct MarkerTracker_t666;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32;
// System.String
struct String_t;
// Vuforia.Marker
struct Marker_t667;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker>
struct IEnumerable_1_t668;

// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTracker::CreateMarker(System.Int32,System.String,System.Single)
// System.Boolean Vuforia.MarkerTracker::DestroyMarker(Vuforia.Marker,System.Boolean)
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTracker::GetMarkers()
// Vuforia.Marker Vuforia.MarkerTracker::GetMarkerByMarkerID(System.Int32)
// System.Void Vuforia.MarkerTracker::DestroyAllMarkers(System.Boolean)
// System.Void Vuforia.MarkerTracker::.ctor()
 void MarkerTracker__ctor_m3112 (MarkerTracker_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
