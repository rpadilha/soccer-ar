﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_84.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>
struct CachedInvokableCall_1_t3616  : public InvokableCall_1_t3617
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
