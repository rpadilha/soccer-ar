﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.MeshRenderer>
struct UnityAction_1_t4651;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshRenderer>
struct InvokableCall_1_t4650  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.MeshRenderer>::Delegate
	UnityAction_1_t4651 * ___Delegate_0;
};
