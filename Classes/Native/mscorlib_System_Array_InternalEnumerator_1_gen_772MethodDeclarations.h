﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.DateTimeKind>
struct InternalEnumerator_1_t5335;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTimeKind>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m32122 (InternalEnumerator_1_t5335 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.DateTimeKind>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32123 (InternalEnumerator_1_t5335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.DateTimeKind>::Dispose()
 void InternalEnumerator_1_Dispose_m32124 (InternalEnumerator_1_t5335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.DateTimeKind>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m32125 (InternalEnumerator_1_t5335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.DateTimeKind>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32126 (InternalEnumerator_1_t5335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
