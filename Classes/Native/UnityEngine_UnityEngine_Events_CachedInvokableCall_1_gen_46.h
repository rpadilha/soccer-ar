﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<ScoreHUD>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_42.h"
// UnityEngine.Events.CachedInvokableCall`1<ScoreHUD>
struct CachedInvokableCall_1_t3043  : public InvokableCall_1_t3044
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ScoreHUD>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
