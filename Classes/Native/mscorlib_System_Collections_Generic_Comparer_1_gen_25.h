﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparer_1_t3971;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparer_1_t3971  : public Object_t
{
};
struct Comparer_1_t3971_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::_default
	Comparer_1_t3971 * ____default_0;
};
