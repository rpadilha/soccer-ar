﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>
struct UnityAction_1_t2902;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>
struct InvokableCall_1_t2901  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::Delegate
	UnityAction_1_t2902 * ___Delegate_0;
};
