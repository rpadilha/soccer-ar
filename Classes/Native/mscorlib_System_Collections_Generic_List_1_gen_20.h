﻿#pragma once
#include <stdint.h>
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3862;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t606  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::_items
	ICloudRecoEventHandlerU5BU5D_t3862* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t606_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::EmptyArray
	ICloudRecoEventHandlerU5BU5D_t3862* ___EmptyArray_4;
};
