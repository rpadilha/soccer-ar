﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t653;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
// System.IO.FileStream/WriteDelegate
struct WriteDelegate_t1930  : public MulticastDelegate_t373
{
};
