﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Type>
struct Enumerator_t4929;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1212;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m29809(__this, ___t, method) (void)Enumerator__ctor_m16415_gshared((Enumerator_t3210 *)__this, (Stack_1_t3209 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m29810(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m16416_gshared((Enumerator_t3210 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m29811(__this, method) (void)Enumerator_Dispose_m16417_gshared((Enumerator_t3210 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m29812(__this, method) (bool)Enumerator_MoveNext_m16418_gshared((Enumerator_t3210 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m29813(__this, method) (Type_t *)Enumerator_get_Current_m16419_gshared((Enumerator_t3210 *)__this, method)
