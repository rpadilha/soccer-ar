﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t338;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.RectTransform>
struct Predicate_1_t3754  : public MulticastDelegate_t373
{
};
