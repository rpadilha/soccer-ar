﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ITextRecoEventHandler>
struct IList_1_t4530;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>
struct ReadOnlyCollection_1_t4526  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::list
	Object_t* ___list_0;
};
