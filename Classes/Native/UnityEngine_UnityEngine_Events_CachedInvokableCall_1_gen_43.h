﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_39.h"
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>
struct CachedInvokableCall_1_t3027  : public InvokableCall_1_t3028
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
