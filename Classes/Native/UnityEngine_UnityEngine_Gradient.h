﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Gradient
struct Gradient_t994  : public Object_t
{
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	IntPtr_t121 ___m_Ptr_0;
};
// Native definition for marshalling of: UnityEngine.Gradient
struct Gradient_t994_marshaled
{
	IntPtr_t121 ___m_Ptr_0;
};
