﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CharacterInfo
struct CharacterInfo_t1078;
struct CharacterInfo_t1078_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Int32 UnityEngine.CharacterInfo::get_advance()
 int32_t CharacterInfo_get_advance_m6350 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
 int32_t CharacterInfo_get_glyphWidth_m6351 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
 int32_t CharacterInfo_get_glyphHeight_m6352 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
 int32_t CharacterInfo_get_bearing_m6353 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minY()
 int32_t CharacterInfo_get_minY_m6354 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
 int32_t CharacterInfo_get_maxY_m6355 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minX()
 int32_t CharacterInfo_get_minX_m6356 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
 int32_t CharacterInfo_get_maxX_m6357 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
 Vector2_t99  CharacterInfo_get_uvBottomLeftUnFlipped_m6358 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
 Vector2_t99  CharacterInfo_get_uvBottomRightUnFlipped_m6359 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
 Vector2_t99  CharacterInfo_get_uvTopRightUnFlipped_m6360 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
 Vector2_t99  CharacterInfo_get_uvTopLeftUnFlipped_m6361 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
 Vector2_t99  CharacterInfo_get_uvBottomLeft_m6362 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
 Vector2_t99  CharacterInfo_get_uvBottomRight_m6363 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
 Vector2_t99  CharacterInfo_get_uvTopRight_m6364 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
 Vector2_t99  CharacterInfo_get_uvTopLeft_m6365 (CharacterInfo_t1078 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void CharacterInfo_t1078_marshal(const CharacterInfo_t1078& unmarshaled, CharacterInfo_t1078_marshaled& marshaled);
void CharacterInfo_t1078_marshal_back(const CharacterInfo_t1078_marshaled& marshaled, CharacterInfo_t1078& unmarshaled);
void CharacterInfo_t1078_marshal_cleanup(CharacterInfo_t1078_marshaled& marshaled);
