﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.EventArgs
struct EventArgs_t1620;

// System.Void System.EventArgs::.ctor()
 void EventArgs__ctor_m13098 (EventArgs_t1620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.cctor()
 void EventArgs__cctor_m13099 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
