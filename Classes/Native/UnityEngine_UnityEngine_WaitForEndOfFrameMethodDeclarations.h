﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t539;

// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
 void WaitForEndOfFrame__ctor_m2537 (WaitForEndOfFrame_t539 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
