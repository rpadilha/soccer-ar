﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ICanvasElement>
struct EqualityComparer_1_t3424;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ICanvasElement>
struct EqualityComparer_1_t3424  : public Object_t
{
};
struct EqualityComparer_1_t3424_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ICanvasElement>::_default
	EqualityComparer_1_t3424 * ____default_0;
};
