﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Globalization.DaylightTime
struct DaylightTime_t1909  : public Object_t
{
	// System.DateTime System.Globalization.DaylightTime::m_start
	DateTime_t674  ___m_start_0;
	// System.DateTime System.Globalization.DaylightTime::m_end
	DateTime_t674  ___m_end_1;
	// System.TimeSpan System.Globalization.DaylightTime::m_delta
	TimeSpan_t852  ___m_delta_2;
};
