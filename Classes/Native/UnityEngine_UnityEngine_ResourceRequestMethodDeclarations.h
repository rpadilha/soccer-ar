﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t1028;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
 void ResourceRequest__ctor_m6066 (ResourceRequest_t1028 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
 Object_t120 * ResourceRequest_get_asset_m6067 (ResourceRequest_t1028 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
