﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.TimeSpan>
struct Nullable_1_t2254;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
 void Nullable_1__ctor_m14128 (Nullable_1_t2254 * __this, TimeSpan_t852  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
 bool Nullable_1_get_HasValue_m14129 (Nullable_1_t2254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Nullable`1<System.TimeSpan>::get_Value()
 TimeSpan_t852  Nullable_1_get_Value_m14130 (Nullable_1_t2254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
 bool Nullable_1_Equals_m32144 (Nullable_1_t2254 * __this, Object_t * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
 bool Nullable_1_Equals_m32145 (Nullable_1_t2254 * __this, Nullable_1_t2254  ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
 int32_t Nullable_1_GetHashCode_m32146 (Nullable_1_t2254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.TimeSpan>::ToString()
 String_t* Nullable_1_ToString_m32147 (Nullable_1_t2254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
