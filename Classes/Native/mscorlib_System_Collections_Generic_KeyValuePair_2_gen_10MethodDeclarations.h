﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>
struct KeyValuePair_2_t3977;
// Vuforia.Trackable
struct Trackable_t594;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m22080 (KeyValuePair_2_t3977 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::get_Key()
 int32_t KeyValuePair_2_get_Key_m22081 (KeyValuePair_2_t3977 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m22082 (KeyValuePair_2_t3977 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m22083 (KeyValuePair_2_t3977 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m22084 (KeyValuePair_2_t3977 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::ToString()
 String_t* KeyValuePair_2_ToString_m22085 (KeyValuePair_2_t3977 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
