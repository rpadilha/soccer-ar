﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>
struct PrimeHelper_t4573;

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::.cctor()
// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_PrimeHelper_0MethodDeclarations.h"
#define PrimeHelper__cctor_m27652(__this/* static, unused */, method) (void)PrimeHelper__cctor_m27631_gshared((Object_t *)__this/* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::TestPrime(System.Int32)
#define PrimeHelper_TestPrime_m27653(__this/* static, unused */, ___x, method) (bool)PrimeHelper_TestPrime_m27632_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::CalcPrime(System.Int32)
#define PrimeHelper_CalcPrime_m27654(__this/* static, unused */, ___x, method) (int32_t)PrimeHelper_CalcPrime_m27633_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::ToPrime(System.Int32)
#define PrimeHelper_ToPrime_m27655(__this/* static, unused */, ___x, method) (int32_t)PrimeHelper_ToPrime_m27634_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
