﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t53;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
struct UnityAction_1_t2947  : public MulticastDelegate_t373
{
};
