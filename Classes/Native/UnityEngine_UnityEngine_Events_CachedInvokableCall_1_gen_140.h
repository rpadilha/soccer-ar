﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_142.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Renderer>
struct CachedInvokableCall_1_t4646  : public InvokableCall_1_t4647
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Renderer>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
