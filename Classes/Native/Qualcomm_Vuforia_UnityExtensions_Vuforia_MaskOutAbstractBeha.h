﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t64;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t28  : public MonoBehaviour_t10
{
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t64 * ___maskMaterial_2;
};
