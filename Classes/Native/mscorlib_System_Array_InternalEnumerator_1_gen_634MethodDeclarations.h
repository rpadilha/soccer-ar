﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
struct InternalEnumerator_1_t5178;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31264 (InternalEnumerator_1_t5178 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31265 (InternalEnumerator_1_t5178 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
 void InternalEnumerator_1_Dispose_m31266 (InternalEnumerator_1_t5178 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31267 (InternalEnumerator_1_t5178 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
 Slot_t1889  InternalEnumerator_1_get_Current_m31268 (InternalEnumerator_1_t5178 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
