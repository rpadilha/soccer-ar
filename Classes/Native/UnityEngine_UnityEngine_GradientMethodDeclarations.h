﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t994;
struct Gradient_t994_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
 void Gradient__ctor_m5727 (Gradient_t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
 void Gradient_Init_m5728 (Gradient_t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
 void Gradient_Cleanup_m5729 (Gradient_t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
 void Gradient_Finalize_m5730 (Gradient_t994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Gradient_t994_marshal(const Gradient_t994& unmarshaled, Gradient_t994_marshaled& marshaled);
void Gradient_t994_marshal_back(const Gradient_t994_marshaled& marshaled, Gradient_t994& unmarshaled);
void Gradient_t994_marshal_cleanup(Gradient_t994_marshaled& marshaled);
