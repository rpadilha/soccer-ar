﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARWrapper
struct QCARWrapper_t754;
// Vuforia.IQCARWrapper
struct IQCARWrapper_t753;

// Vuforia.IQCARWrapper Vuforia.QCARWrapper::get_Instance()
 Object_t * QCARWrapper_get_Instance_m4090 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARWrapper::Create()
 void QCARWrapper_Create_m4091 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARWrapper::SetImplementation(Vuforia.IQCARWrapper)
 void QCARWrapper_SetImplementation_m4092 (Object_t * __this/* static, unused */, Object_t * ___implementation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARWrapper::.cctor()
 void QCARWrapper__cctor_m4093 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
