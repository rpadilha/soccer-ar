﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Corner_Script>
struct UnityAction_1_t3015;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Corner_Script>
struct InvokableCall_1_t3014  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Corner_Script>::Delegate
	UnityAction_1_t3015 * ___Delegate_0;
};
