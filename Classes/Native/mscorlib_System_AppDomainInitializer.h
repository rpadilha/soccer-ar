﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t862;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.AppDomainInitializer
struct AppDomainInitializer_t2240  : public MulticastDelegate_t373
{
};
