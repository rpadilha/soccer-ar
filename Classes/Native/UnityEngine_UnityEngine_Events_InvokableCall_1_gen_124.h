﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.PropAbstractBehaviour>
struct UnityAction_1_t4333;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.PropAbstractBehaviour>
struct InvokableCall_1_t4332  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.PropAbstractBehaviour>::Delegate
	UnityAction_1_t4333 * ___Delegate_0;
};
