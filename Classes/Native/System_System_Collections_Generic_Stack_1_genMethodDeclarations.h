﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1212;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t3891;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Type>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m6668(__this, method) (void)Stack_1__ctor_m16404_gshared((Stack_1_t3209 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m29802(__this, method) (bool)Stack_1_System_Collections_ICollection_get_IsSynchronized_m16405_gshared((Stack_1_t3209 *)__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m29803(__this, method) (Object_t *)Stack_1_System_Collections_ICollection_get_SyncRoot_m16406_gshared((Stack_1_t3209 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m29804(__this, ___dest, ___idx, method) (void)Stack_1_System_Collections_ICollection_CopyTo_m16407_gshared((Stack_1_t3209 *)__this, (Array_t *)___dest, (int32_t)___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29805(__this, method) (Object_t*)Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16408_gshared((Stack_1_t3209 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m29806(__this, method) (Object_t *)Stack_1_System_Collections_IEnumerable_GetEnumerator_m16409_gshared((Stack_1_t3209 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Peek()
#define Stack_1_Peek_m29807(__this, method) (Type_t *)Stack_1_Peek_m16410_gshared((Stack_1_t3209 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m6671(__this, method) (Type_t *)Stack_1_Pop_m16411_gshared((Stack_1_t3209 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(T)
#define Stack_1_Push_m6669(__this, ___t, method) (void)Stack_1_Push_m16412_gshared((Stack_1_t3209 *)__this, (Object_t *)___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m6673(__this, method) (int32_t)Stack_1_get_Count_m16413_gshared((Stack_1_t3209 *)__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Type>::GetEnumerator()
 Enumerator_t4929  Stack_1_GetEnumerator_m29808 (Stack_1_t1212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
