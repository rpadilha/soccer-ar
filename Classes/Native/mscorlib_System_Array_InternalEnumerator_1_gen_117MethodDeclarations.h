﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<GoalKeeperJump_Down>
struct InternalEnumerator_1_t3022;
// System.Object
struct Object_t;
// GoalKeeperJump_Down
struct GoalKeeperJump_Down_t79;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15429(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::Dispose()
#define InternalEnumerator_1_Dispose_m15431(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15432(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::get_Current()
#define InternalEnumerator_1_get_Current_m15433(__this, method) (GoalKeeperJump_Down_t79 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
