﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.FixupRecord
struct FixupRecord_t2129;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2125;
// System.Reflection.MemberInfo
struct MemberInfo_t144;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2118;

// System.Void System.Runtime.Serialization.FixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Reflection.MemberInfo,System.Runtime.Serialization.ObjectRecord)
 void FixupRecord__ctor_m11960 (FixupRecord_t2129 * __this, ObjectRecord_t2125 * ___objectToBeFixed, MemberInfo_t144 * ___member, ObjectRecord_t2125 * ___objectRequired, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.FixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
 void FixupRecord_FixupImpl_m11961 (FixupRecord_t2129 * __this, ObjectManager_t2118 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
