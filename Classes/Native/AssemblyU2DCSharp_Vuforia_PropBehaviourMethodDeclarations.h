﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropBehaviour
struct PropBehaviour_t12;

// System.Void Vuforia.PropBehaviour::.ctor()
 void PropBehaviour__ctor_m79 (PropBehaviour_t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
