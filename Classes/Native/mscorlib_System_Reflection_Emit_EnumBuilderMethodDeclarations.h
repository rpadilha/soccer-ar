﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.EnumBuilder
struct EnumBuilder_t1957;
// System.Reflection.Assembly
struct Assembly_t1556;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Reflection.Module
struct Module_t1755;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1245;
// System.Reflection.Binder
struct Binder_t1215;
// System.Type[]
struct TypeU5BU5D_t922;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1216;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1759;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Reflection.EventInfo
struct EventInfo_t1756;
// System.Reflection.FieldInfo
struct FieldInfo_t1757;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t140;
// System.Reflection.PropertyInfo
struct PropertyInfo_t1758;
// System.Object
struct Object_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1218;
// System.String[]
struct StringU5BU5D_t862;
// System.Exception
struct Exception_t151;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Reflection.Assembly System.Reflection.Emit.EnumBuilder::get_Assembly()
 Assembly_t1556 * EnumBuilder_get_Assembly_m11136 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.EnumBuilder::get_AssemblyQualifiedName()
 String_t* EnumBuilder_get_AssemblyQualifiedName_m11137 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EnumBuilder::get_BaseType()
 Type_t * EnumBuilder_get_BaseType_m11138 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EnumBuilder::get_DeclaringType()
 Type_t * EnumBuilder_get_DeclaringType_m11139 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.EnumBuilder::get_FullName()
 String_t* EnumBuilder_get_FullName_m11140 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.EnumBuilder::get_Module()
 Module_t1755 * EnumBuilder_get_Module_m11141 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.EnumBuilder::get_Name()
 String_t* EnumBuilder_get_Name_m11142 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.EnumBuilder::get_Namespace()
 String_t* EnumBuilder_get_Namespace_m11143 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EnumBuilder::get_ReflectedType()
 Type_t * EnumBuilder_get_ReflectedType_m11144 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.Reflection.Emit.EnumBuilder::get_TypeHandle()
 RuntimeTypeHandle_t1754  EnumBuilder_get_TypeHandle_m11145 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EnumBuilder::get_UnderlyingSystemType()
 Type_t * EnumBuilder_get_UnderlyingSystemType_m11146 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.Emit.EnumBuilder::GetAttributeFlagsImpl()
 int32_t EnumBuilder_GetAttributeFlagsImpl_m11147 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.EnumBuilder::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
 ConstructorInfo_t1245 * EnumBuilder_GetConstructorImpl_m11148 (EnumBuilder_t1957 * __this, int32_t ___bindingAttr, Binder_t1215 * ___binder, int32_t ___callConvention, TypeU5BU5D_t922* ___types, ParameterModifierU5BU5D_t1216* ___modifiers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.EnumBuilder::GetConstructors(System.Reflection.BindingFlags)
 ConstructorInfoU5BU5D_t1759* EnumBuilder_GetConstructors_m11149 (EnumBuilder_t1957 * __this, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.EnumBuilder::GetCustomAttributes(System.Boolean)
 ObjectU5BU5D_t130* EnumBuilder_GetCustomAttributes_m11150 (EnumBuilder_t1957 * __this, bool ___inherit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.EnumBuilder::GetCustomAttributes(System.Type,System.Boolean)
 ObjectU5BU5D_t130* EnumBuilder_GetCustomAttributes_m11151 (EnumBuilder_t1957 * __this, Type_t * ___attributeType, bool ___inherit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EnumBuilder::GetElementType()
 Type_t * EnumBuilder_GetElementType_m11152 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.Emit.EnumBuilder::GetEvent(System.String,System.Reflection.BindingFlags)
 EventInfo_t1756 * EnumBuilder_GetEvent_m11153 (EnumBuilder_t1957 * __this, String_t* ___name, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Emit.EnumBuilder::GetField(System.String,System.Reflection.BindingFlags)
 FieldInfo_t1757 * EnumBuilder_GetField_m11154 (EnumBuilder_t1957 * __this, String_t* ___name, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.EnumBuilder::GetInterfaces()
 TypeU5BU5D_t922* EnumBuilder_GetInterfaces_m11155 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.EnumBuilder::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
 MethodInfo_t141 * EnumBuilder_GetMethodImpl_m11156 (EnumBuilder_t1957 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1215 * ___binder, int32_t ___callConvention, TypeU5BU5D_t922* ___types, ParameterModifierU5BU5D_t1216* ___modifiers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.EnumBuilder::GetMethods(System.Reflection.BindingFlags)
 MethodInfoU5BU5D_t140* EnumBuilder_GetMethods_m11157 (EnumBuilder_t1957 * __this, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.Emit.EnumBuilder::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
 PropertyInfo_t1758 * EnumBuilder_GetPropertyImpl_m11158 (EnumBuilder_t1957 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1215 * ___binder, Type_t * ___returnType, TypeU5BU5D_t922* ___types, ParameterModifierU5BU5D_t1216* ___modifiers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::HasElementTypeImpl()
 bool EnumBuilder_HasElementTypeImpl_m11159 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.EnumBuilder::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
 Object_t * EnumBuilder_InvokeMember_m11160 (EnumBuilder_t1957 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1215 * ___binder, Object_t * ___target, ObjectU5BU5D_t130* ___args, ParameterModifierU5BU5D_t1216* ___modifiers, CultureInfo_t1218 * ___culture, StringU5BU5D_t862* ___namedParameters, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::IsArrayImpl()
 bool EnumBuilder_IsArrayImpl_m11161 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::IsByRefImpl()
 bool EnumBuilder_IsByRefImpl_m11162 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::IsPointerImpl()
 bool EnumBuilder_IsPointerImpl_m11163 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::IsPrimitiveImpl()
 bool EnumBuilder_IsPrimitiveImpl_m11164 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::IsValueTypeImpl()
 bool EnumBuilder_IsValueTypeImpl_m11165 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EnumBuilder::IsDefined(System.Type,System.Boolean)
 bool EnumBuilder_IsDefined_m11166 (EnumBuilder_t1957 * __this, Type_t * ___attributeType, bool ___inherit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.EnumBuilder::CreateNotSupportedException()
 Exception_t151 * EnumBuilder_CreateNotSupportedException_m11167 (EnumBuilder_t1957 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
