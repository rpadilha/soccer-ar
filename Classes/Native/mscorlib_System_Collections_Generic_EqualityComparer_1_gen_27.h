﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ICloudRecoEventHandler>
struct EqualityComparer_1_t3872;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ICloudRecoEventHandler>
struct EqualityComparer_1_t3872  : public Object_t
{
};
struct EqualityComparer_1_t3872_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ICloudRecoEventHandler>::_default
	EqualityComparer_1_t3872 * ____default_0;
};
