﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTargetImpl
struct ObjectTargetImpl_t611;
// Vuforia.DataSetImpl
struct DataSetImpl_t610;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t612;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.ObjectTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
 void ObjectTargetImpl__ctor_m2860 (ObjectTargetImpl_t611 * __this, String_t* ___name, int32_t ___id, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize()
 Vector3_t73  ObjectTargetImpl_GetSize_m2861 (ObjectTargetImpl_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetImpl::SetSize(UnityEngine.Vector3)
 void ObjectTargetImpl_SetSize_m2862 (ObjectTargetImpl_t611 * __this, Vector3_t73  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StartExtendedTracking()
 bool ObjectTargetImpl_StartExtendedTracking_m2863 (ObjectTargetImpl_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StopExtendedTracking()
 bool ObjectTargetImpl_StopExtendedTracking_m2864 (ObjectTargetImpl_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::get_DataSet()
 DataSetImpl_t610 * ObjectTargetImpl_get_DataSet_m2865 (ObjectTargetImpl_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
