﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Renderer
struct Renderer_t129;
// UnityEngine.Material
struct Material_t64;
// UnityEngine.Material[]
struct MaterialU5BU5D_t97;

// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
 void Renderer_set_enabled_m285 (Renderer_t129 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
 Material_t64 * Renderer_get_material_m5437 (Renderer_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
 void Renderer_set_material_m640 (Renderer_t129 * __this, Material_t64 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
 void Renderer_set_sharedMaterial_m364 (Renderer_t129 * __this, Material_t64 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
 MaterialU5BU5D_t97* Renderer_get_materials_m363 (Renderer_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
 void Renderer_set_sharedMaterials_m365 (Renderer_t129 * __this, MaterialU5BU5D_t97* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
 int32_t Renderer_get_sortingLayerID_m2214 (Renderer_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
 int32_t Renderer_get_sortingOrder_m2215 (Renderer_t129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
