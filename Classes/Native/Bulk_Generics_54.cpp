﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t7332_il2cpp_TypeInfo;

// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"

// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo IEnumerator_1_get_Current_m52411_MethodInfo;
static PropertyInfo IEnumerator_1_t7332____Current_PropertyInfo = 
{
	&IEnumerator_1_t7332_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52411_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7332_PropertyInfos[] =
{
	&IEnumerator_1_t7332____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1973 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52411_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52411_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1973_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1973/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52411_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7332_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52411_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t7332_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7332_0_0_0;
extern Il2CppType IEnumerator_1_t7332_1_0_0;
struct IEnumerator_1_t7332;
extern Il2CppGenericClass IEnumerator_1_t7332_GenericClass;
TypeInfo IEnumerator_1_t7332_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7332_MethodInfos/* methods */
	, IEnumerator_1_t7332_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7332_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7332_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7332_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7332_0_0_0/* byval_arg */
	, &IEnumerator_1_t7332_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7332_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_682.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5226_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_682MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo AssemblyNameFlags_t1973_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m31508_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyNameFlags_t1973_m41222_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyNameFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyNameFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAssemblyNameFlags_t1973_m41222 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31504_MethodInfo;
 void InternalEnumerator_1__ctor_m31504 (InternalEnumerator_1_t5226 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505 (InternalEnumerator_1_t5226 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31508(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31508_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AssemblyNameFlags_t1973_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31506_MethodInfo;
 void InternalEnumerator_1_Dispose_m31506 (InternalEnumerator_1_t5226 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31507_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31507 (InternalEnumerator_1_t5226 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31508 (InternalEnumerator_1_t5226 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAssemblyNameFlags_t1973_m41222(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAssemblyNameFlags_t1973_m41222_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5226____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5226, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5226____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5226, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5226_FieldInfos[] =
{
	&InternalEnumerator_1_t5226____array_0_FieldInfo,
	&InternalEnumerator_1_t5226____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5226____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5226_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5226____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5226_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5226_PropertyInfos[] =
{
	&InternalEnumerator_1_t5226____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5226____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5226_InternalEnumerator_1__ctor_m31504_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31504_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31504/* method */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5226_InternalEnumerator_1__ctor_m31504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31504_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505/* method */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31506_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31506_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31506/* method */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31506_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31507_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31507_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31507/* method */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31507_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1973 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31508_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31508_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31508/* method */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1973_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1973/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31508_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5226_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31504_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_MethodInfo,
	&InternalEnumerator_1_Dispose_m31506_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31507_MethodInfo,
	&InternalEnumerator_1_get_Current_m31508_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5226_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31507_MethodInfo,
	&InternalEnumerator_1_Dispose_m31506_MethodInfo,
	&InternalEnumerator_1_get_Current_m31508_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5226_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7332_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5226_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7332_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5226_0_0_0;
extern Il2CppType InternalEnumerator_1_t5226_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5226_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5226_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5226_MethodInfos/* methods */
	, InternalEnumerator_1_t5226_PropertyInfos/* properties */
	, InternalEnumerator_1_t5226_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5226_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5226_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5226_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5226_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5226_1_0_0/* this_arg */
	, InternalEnumerator_1_t5226_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5226_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5226)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9408_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo ICollection_1_get_Count_m52412_MethodInfo;
static PropertyInfo ICollection_1_t9408____Count_PropertyInfo = 
{
	&ICollection_1_t9408_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52412_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52413_MethodInfo;
static PropertyInfo ICollection_1_t9408____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9408_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9408_PropertyInfos[] =
{
	&ICollection_1_t9408____Count_PropertyInfo,
	&ICollection_1_t9408____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52412_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m52412_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52412_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52413_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52413_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52413_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
static ParameterInfo ICollection_1_t9408_ICollection_1_Add_m52414_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1973_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52414_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Add(T)
MethodInfo ICollection_1_Add_m52414_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9408_ICollection_1_Add_m52414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52414_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52415_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Clear()
MethodInfo ICollection_1_Clear_m52415_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52415_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
static ParameterInfo ICollection_1_t9408_ICollection_1_Contains_m52416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1973_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52416_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m52416_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9408_ICollection_1_Contains_m52416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52416_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlagsU5BU5D_t5528_0_0_0;
extern Il2CppType AssemblyNameFlagsU5BU5D_t5528_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9408_ICollection_1_CopyTo_m52417_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlagsU5BU5D_t5528_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52417_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52417_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9408_ICollection_1_CopyTo_m52417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52417_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
static ParameterInfo ICollection_1_t9408_ICollection_1_Remove_m52418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1973_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52418_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m52418_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9408_ICollection_1_Remove_m52418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52418_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9408_MethodInfos[] =
{
	&ICollection_1_get_Count_m52412_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52413_MethodInfo,
	&ICollection_1_Add_m52414_MethodInfo,
	&ICollection_1_Clear_m52415_MethodInfo,
	&ICollection_1_Contains_m52416_MethodInfo,
	&ICollection_1_CopyTo_m52417_MethodInfo,
	&ICollection_1_Remove_m52418_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t9410_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9408_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9410_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9408_0_0_0;
extern Il2CppType ICollection_1_t9408_1_0_0;
struct ICollection_1_t9408;
extern Il2CppGenericClass ICollection_1_t9408_GenericClass;
TypeInfo ICollection_1_t9408_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9408_MethodInfos/* methods */
	, ICollection_1_t9408_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9408_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9408_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9408_0_0_0/* byval_arg */
	, &ICollection_1_t9408_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9408_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>
extern Il2CppType IEnumerator_1_t7332_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52419_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52419_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9410_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7332_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52419_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9410_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52419_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9410_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9410_0_0_0;
extern Il2CppType IEnumerable_1_t9410_1_0_0;
struct IEnumerable_1_t9410;
extern Il2CppGenericClass IEnumerable_1_t9410_GenericClass;
TypeInfo IEnumerable_1_t9410_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9410_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9410_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9410_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9410_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9410_0_0_0/* byval_arg */
	, &IEnumerable_1_t9410_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9410_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9409_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo IList_1_get_Item_m52420_MethodInfo;
extern MethodInfo IList_1_set_Item_m52421_MethodInfo;
static PropertyInfo IList_1_t9409____Item_PropertyInfo = 
{
	&IList_1_t9409_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52420_MethodInfo/* get */
	, &IList_1_set_Item_m52421_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9409_PropertyInfos[] =
{
	&IList_1_t9409____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
static ParameterInfo IList_1_t9409_IList_1_IndexOf_m52422_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1973_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52422_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52422_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9409_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9409_IList_1_IndexOf_m52422_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52422_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
static ParameterInfo IList_1_t9409_IList_1_Insert_m52423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1973_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52423_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52423_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9409_IList_1_Insert_m52423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52423_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9409_IList_1_RemoveAt_m52424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52424_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52424_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9409_IList_1_RemoveAt_m52424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52424_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9409_IList_1_get_Item_m52420_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1973_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52420_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52420_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9409_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1973_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1973_Int32_t123/* invoker_method */
	, IList_1_t9409_IList_1_get_Item_m52420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52420_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyNameFlags_t1973_0_0_0;
static ParameterInfo IList_1_t9409_IList_1_set_Item_m52421_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1973_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52421_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52421_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9409_IList_1_set_Item_m52421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52421_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9409_MethodInfos[] =
{
	&IList_1_IndexOf_m52422_MethodInfo,
	&IList_1_Insert_m52423_MethodInfo,
	&IList_1_RemoveAt_m52424_MethodInfo,
	&IList_1_get_Item_m52420_MethodInfo,
	&IList_1_set_Item_m52421_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9409_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9408_il2cpp_TypeInfo,
	&IEnumerable_1_t9410_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9409_0_0_0;
extern Il2CppType IList_1_t9409_1_0_0;
struct IList_1_t9409;
extern Il2CppGenericClass IList_1_t9409_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9409_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9409_MethodInfos/* methods */
	, IList_1_t9409_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9409_il2cpp_TypeInfo/* element_class */
	, IList_1_t9409_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9409_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9409_0_0_0/* byval_arg */
	, &IList_1_t9409_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9409_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7334_il2cpp_TypeInfo;

// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52425_MethodInfo;
static PropertyInfo IEnumerator_1_t7334____Current_PropertyInfo = 
{
	&IEnumerator_1_t7334_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52425_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7334_PropertyInfos[] =
{
	&IEnumerator_1_t7334____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52425_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52425_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7334_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyProductAttribute_t577_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52425_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7334_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52425_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7334_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7334_0_0_0;
extern Il2CppType IEnumerator_1_t7334_1_0_0;
struct IEnumerator_1_t7334;
extern Il2CppGenericClass IEnumerator_1_t7334_GenericClass;
TypeInfo IEnumerator_1_t7334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7334_MethodInfos/* methods */
	, IEnumerator_1_t7334_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7334_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7334_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7334_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7334_0_0_0/* byval_arg */
	, &IEnumerator_1_t7334_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7334_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_683.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5227_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_683MethodDeclarations.h"

extern TypeInfo AssemblyProductAttribute_t577_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31513_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyProductAttribute_t577_m41233_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyProductAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyProductAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyProductAttribute_t577_m41233(__this, p0, method) (AssemblyProductAttribute_t577 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5227____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5227, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5227____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5227, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5227_FieldInfos[] =
{
	&InternalEnumerator_1_t5227____array_0_FieldInfo,
	&InternalEnumerator_1_t5227____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5227____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5227_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5227____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5227_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5227_PropertyInfos[] =
{
	&InternalEnumerator_1_t5227____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5227____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5227_InternalEnumerator_1__ctor_m31509_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31509_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31509_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5227_InternalEnumerator_1__ctor_m31509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31509_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31511_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31511_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31511_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31512_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31512_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31512_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31513_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31513_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyProductAttribute_t577_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31513_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5227_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31509_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_MethodInfo,
	&InternalEnumerator_1_Dispose_m31511_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31512_MethodInfo,
	&InternalEnumerator_1_get_Current_m31513_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31512_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31511_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5227_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31510_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31512_MethodInfo,
	&InternalEnumerator_1_Dispose_m31511_MethodInfo,
	&InternalEnumerator_1_get_Current_m31513_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5227_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7334_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5227_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7334_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyProductAttribute_t577_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5227_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31513_MethodInfo/* Method Usage */,
	&AssemblyProductAttribute_t577_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyProductAttribute_t577_m41233_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5227_0_0_0;
extern Il2CppType InternalEnumerator_1_t5227_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5227_GenericClass;
TypeInfo InternalEnumerator_1_t5227_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5227_MethodInfos/* methods */
	, InternalEnumerator_1_t5227_PropertyInfos/* properties */
	, InternalEnumerator_1_t5227_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5227_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5227_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5227_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5227_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5227_1_0_0/* this_arg */
	, InternalEnumerator_1_t5227_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5227_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5227_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5227)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9411_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo ICollection_1_get_Count_m52426_MethodInfo;
static PropertyInfo ICollection_1_t9411____Count_PropertyInfo = 
{
	&ICollection_1_t9411_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52426_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52427_MethodInfo;
static PropertyInfo ICollection_1_t9411____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9411_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9411_PropertyInfos[] =
{
	&ICollection_1_t9411____Count_PropertyInfo,
	&ICollection_1_t9411____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52426_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52426_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52426_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52427_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52427_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52427_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
static ParameterInfo ICollection_1_t9411_ICollection_1_Add_m52428_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t577_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52428_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52428_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9411_ICollection_1_Add_m52428_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52428_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52429_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52429_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52429_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
static ParameterInfo ICollection_1_t9411_ICollection_1_Contains_m52430_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t577_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52430_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52430_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9411_ICollection_1_Contains_m52430_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52430_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttributeU5BU5D_t5529_0_0_0;
extern Il2CppType AssemblyProductAttributeU5BU5D_t5529_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9411_ICollection_1_CopyTo_m52431_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttributeU5BU5D_t5529_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52431_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52431_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9411_ICollection_1_CopyTo_m52431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52431_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
static ParameterInfo ICollection_1_t9411_ICollection_1_Remove_m52432_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t577_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52432_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52432_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9411_ICollection_1_Remove_m52432_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52432_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9411_MethodInfos[] =
{
	&ICollection_1_get_Count_m52426_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52427_MethodInfo,
	&ICollection_1_Add_m52428_MethodInfo,
	&ICollection_1_Clear_m52429_MethodInfo,
	&ICollection_1_Contains_m52430_MethodInfo,
	&ICollection_1_CopyTo_m52431_MethodInfo,
	&ICollection_1_Remove_m52432_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9413_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9411_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9413_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9411_0_0_0;
extern Il2CppType ICollection_1_t9411_1_0_0;
struct ICollection_1_t9411;
extern Il2CppGenericClass ICollection_1_t9411_GenericClass;
TypeInfo ICollection_1_t9411_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9411_MethodInfos/* methods */
	, ICollection_1_t9411_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9411_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9411_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9411_0_0_0/* byval_arg */
	, &ICollection_1_t9411_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9411_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyProductAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyProductAttribute>
extern Il2CppType IEnumerator_1_t7334_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52433_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyProductAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52433_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9413_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7334_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52433_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9413_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52433_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9413_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9413_0_0_0;
extern Il2CppType IEnumerable_1_t9413_1_0_0;
struct IEnumerable_1_t9413;
extern Il2CppGenericClass IEnumerable_1_t9413_GenericClass;
TypeInfo IEnumerable_1_t9413_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9413_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9413_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9413_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9413_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9413_0_0_0/* byval_arg */
	, &IEnumerable_1_t9413_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9413_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9412_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo IList_1_get_Item_m52434_MethodInfo;
extern MethodInfo IList_1_set_Item_m52435_MethodInfo;
static PropertyInfo IList_1_t9412____Item_PropertyInfo = 
{
	&IList_1_t9412_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52434_MethodInfo/* get */
	, &IList_1_set_Item_m52435_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9412_PropertyInfos[] =
{
	&IList_1_t9412____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
static ParameterInfo IList_1_t9412_IList_1_IndexOf_m52436_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t577_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52436_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52436_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9412_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9412_IList_1_IndexOf_m52436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52436_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
static ParameterInfo IList_1_t9412_IList_1_Insert_m52437_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t577_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52437_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52437_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9412_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9412_IList_1_Insert_m52437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52437_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9412_IList_1_RemoveAt_m52438_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52438_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52438_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9412_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9412_IList_1_RemoveAt_m52438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52438_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9412_IList_1_get_Item_m52434_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52434_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52434_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9412_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyProductAttribute_t577_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9412_IList_1_get_Item_m52434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52434_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyProductAttribute_t577_0_0_0;
static ParameterInfo IList_1_t9412_IList_1_set_Item_m52435_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t577_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52435_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52435_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9412_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9412_IList_1_set_Item_m52435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52435_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9412_MethodInfos[] =
{
	&IList_1_IndexOf_m52436_MethodInfo,
	&IList_1_Insert_m52437_MethodInfo,
	&IList_1_RemoveAt_m52438_MethodInfo,
	&IList_1_get_Item_m52434_MethodInfo,
	&IList_1_set_Item_m52435_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9412_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9411_il2cpp_TypeInfo,
	&IEnumerable_1_t9413_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9412_0_0_0;
extern Il2CppType IList_1_t9412_1_0_0;
struct IList_1_t9412;
extern Il2CppGenericClass IList_1_t9412_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9412_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9412_MethodInfos/* methods */
	, IList_1_t9412_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9412_il2cpp_TypeInfo/* element_class */
	, IList_1_t9412_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9412_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9412_0_0_0/* byval_arg */
	, &IList_1_t9412_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9412_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7336_il2cpp_TypeInfo;

// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTitleAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTitleAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52439_MethodInfo;
static PropertyInfo IEnumerator_1_t7336____Current_PropertyInfo = 
{
	&IEnumerator_1_t7336_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52439_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7336_PropertyInfos[] =
{
	&IEnumerator_1_t7336____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52439_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTitleAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52439_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7336_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyTitleAttribute_t573_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52439_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7336_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52439_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7336_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7336_0_0_0;
extern Il2CppType IEnumerator_1_t7336_1_0_0;
struct IEnumerator_1_t7336;
extern Il2CppGenericClass IEnumerator_1_t7336_GenericClass;
TypeInfo IEnumerator_1_t7336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7336_MethodInfos/* methods */
	, IEnumerator_1_t7336_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7336_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7336_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7336_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7336_0_0_0/* byval_arg */
	, &IEnumerator_1_t7336_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7336_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_684.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5228_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_684MethodDeclarations.h"

extern TypeInfo AssemblyTitleAttribute_t573_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31518_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyTitleAttribute_t573_m41244_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyTitleAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyTitleAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyTitleAttribute_t573_m41244(__this, p0, method) (AssemblyTitleAttribute_t573 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5228____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5228, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5228____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5228, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5228_FieldInfos[] =
{
	&InternalEnumerator_1_t5228____array_0_FieldInfo,
	&InternalEnumerator_1_t5228____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5228____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5228_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5228____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5228_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31518_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5228_PropertyInfos[] =
{
	&InternalEnumerator_1_t5228____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5228____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5228_InternalEnumerator_1__ctor_m31514_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31514_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31514_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5228_InternalEnumerator_1__ctor_m31514_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31514_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31516_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31516_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31516_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31517_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31517_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31517_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31518_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31518_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyTitleAttribute_t573_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31518_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5228_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31514_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_MethodInfo,
	&InternalEnumerator_1_Dispose_m31516_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31517_MethodInfo,
	&InternalEnumerator_1_get_Current_m31518_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31517_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31516_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5228_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31515_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31517_MethodInfo,
	&InternalEnumerator_1_Dispose_m31516_MethodInfo,
	&InternalEnumerator_1_get_Current_m31518_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5228_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7336_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5228_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7336_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyTitleAttribute_t573_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5228_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31518_MethodInfo/* Method Usage */,
	&AssemblyTitleAttribute_t573_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyTitleAttribute_t573_m41244_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5228_0_0_0;
extern Il2CppType InternalEnumerator_1_t5228_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5228_GenericClass;
TypeInfo InternalEnumerator_1_t5228_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5228_MethodInfos/* methods */
	, InternalEnumerator_1_t5228_PropertyInfos/* properties */
	, InternalEnumerator_1_t5228_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5228_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5228_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5228_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5228_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5228_1_0_0/* this_arg */
	, InternalEnumerator_1_t5228_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5228_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5228_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5228)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9414_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>
extern MethodInfo ICollection_1_get_Count_m52440_MethodInfo;
static PropertyInfo ICollection_1_t9414____Count_PropertyInfo = 
{
	&ICollection_1_t9414_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52440_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52441_MethodInfo;
static PropertyInfo ICollection_1_t9414____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9414_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52441_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9414_PropertyInfos[] =
{
	&ICollection_1_t9414____Count_PropertyInfo,
	&ICollection_1_t9414____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52440_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52440_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52440_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52441_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52441_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52441_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
static ParameterInfo ICollection_1_t9414_ICollection_1_Add_m52442_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttribute_t573_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52442_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52442_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9414_ICollection_1_Add_m52442_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52442_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52443_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52443_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52443_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
static ParameterInfo ICollection_1_t9414_ICollection_1_Contains_m52444_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttribute_t573_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52444_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52444_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9414_ICollection_1_Contains_m52444_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52444_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTitleAttributeU5BU5D_t5530_0_0_0;
extern Il2CppType AssemblyTitleAttributeU5BU5D_t5530_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9414_ICollection_1_CopyTo_m52445_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttributeU5BU5D_t5530_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52445_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52445_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9414_ICollection_1_CopyTo_m52445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52445_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
static ParameterInfo ICollection_1_t9414_ICollection_1_Remove_m52446_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttribute_t573_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52446_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52446_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9414_ICollection_1_Remove_m52446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52446_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9414_MethodInfos[] =
{
	&ICollection_1_get_Count_m52440_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52441_MethodInfo,
	&ICollection_1_Add_m52442_MethodInfo,
	&ICollection_1_Clear_m52443_MethodInfo,
	&ICollection_1_Contains_m52444_MethodInfo,
	&ICollection_1_CopyTo_m52445_MethodInfo,
	&ICollection_1_Remove_m52446_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9416_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9414_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9416_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9414_0_0_0;
extern Il2CppType ICollection_1_t9414_1_0_0;
struct ICollection_1_t9414;
extern Il2CppGenericClass ICollection_1_t9414_GenericClass;
TypeInfo ICollection_1_t9414_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9414_MethodInfos/* methods */
	, ICollection_1_t9414_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9414_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9414_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9414_0_0_0/* byval_arg */
	, &ICollection_1_t9414_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9414_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTitleAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTitleAttribute>
extern Il2CppType IEnumerator_1_t7336_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52447_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTitleAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52447_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9416_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7336_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52447_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9416_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52447_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9416_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9416_0_0_0;
extern Il2CppType IEnumerable_1_t9416_1_0_0;
struct IEnumerable_1_t9416;
extern Il2CppGenericClass IEnumerable_1_t9416_GenericClass;
TypeInfo IEnumerable_1_t9416_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9416_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9416_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9416_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9416_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9416_0_0_0/* byval_arg */
	, &IEnumerable_1_t9416_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9416_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9415_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>
extern MethodInfo IList_1_get_Item_m52448_MethodInfo;
extern MethodInfo IList_1_set_Item_m52449_MethodInfo;
static PropertyInfo IList_1_t9415____Item_PropertyInfo = 
{
	&IList_1_t9415_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52448_MethodInfo/* get */
	, &IList_1_set_Item_m52449_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9415_PropertyInfos[] =
{
	&IList_1_t9415____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
static ParameterInfo IList_1_t9415_IList_1_IndexOf_m52450_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttribute_t573_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52450_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52450_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9415_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9415_IList_1_IndexOf_m52450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52450_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
static ParameterInfo IList_1_t9415_IList_1_Insert_m52451_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttribute_t573_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52451_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52451_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9415_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9415_IList_1_Insert_m52451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52451_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9415_IList_1_RemoveAt_m52452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52452_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52452_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9415_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9415_IList_1_RemoveAt_m52452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52452_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9415_IList_1_get_Item_m52448_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52448_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52448_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9415_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyTitleAttribute_t573_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9415_IList_1_get_Item_m52448_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52448_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyTitleAttribute_t573_0_0_0;
static ParameterInfo IList_1_t9415_IList_1_set_Item_m52449_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyTitleAttribute_t573_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52449_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52449_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9415_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9415_IList_1_set_Item_m52449_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52449_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9415_MethodInfos[] =
{
	&IList_1_IndexOf_m52450_MethodInfo,
	&IList_1_Insert_m52451_MethodInfo,
	&IList_1_RemoveAt_m52452_MethodInfo,
	&IList_1_get_Item_m52448_MethodInfo,
	&IList_1_set_Item_m52449_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9415_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9414_il2cpp_TypeInfo,
	&IEnumerable_1_t9416_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9415_0_0_0;
extern Il2CppType IList_1_t9415_1_0_0;
struct IList_1_t9415;
extern Il2CppGenericClass IList_1_t9415_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9415_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9415_MethodInfos/* methods */
	, IList_1_t9415_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9415_il2cpp_TypeInfo/* element_class */
	, IList_1_t9415_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9415_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9415_0_0_0/* byval_arg */
	, &IList_1_t9415_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9415_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7338_il2cpp_TypeInfo;

// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52453_MethodInfo;
static PropertyInfo IEnumerator_1_t7338____Current_PropertyInfo = 
{
	&IEnumerator_1_t7338_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52453_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7338_PropertyInfos[] =
{
	&IEnumerator_1_t7338____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52453_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52453_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyTrademarkAttribute_t582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52453_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7338_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52453_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7338_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7338_0_0_0;
extern Il2CppType IEnumerator_1_t7338_1_0_0;
struct IEnumerator_1_t7338;
extern Il2CppGenericClass IEnumerator_1_t7338_GenericClass;
TypeInfo IEnumerator_1_t7338_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7338_MethodInfos/* methods */
	, IEnumerator_1_t7338_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7338_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7338_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7338_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7338_0_0_0/* byval_arg */
	, &IEnumerator_1_t7338_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7338_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_685.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5229_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_685MethodDeclarations.h"

extern TypeInfo AssemblyTrademarkAttribute_t582_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31523_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyTrademarkAttribute_t582_m41255_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyTrademarkAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyTrademarkAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyTrademarkAttribute_t582_m41255(__this, p0, method) (AssemblyTrademarkAttribute_t582 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5229____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5229, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5229____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5229, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5229_FieldInfos[] =
{
	&InternalEnumerator_1_t5229____array_0_FieldInfo,
	&InternalEnumerator_1_t5229____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5229____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5229_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5229____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5229_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5229_PropertyInfos[] =
{
	&InternalEnumerator_1_t5229____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5229____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5229_InternalEnumerator_1__ctor_m31519_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31519_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31519_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5229_InternalEnumerator_1__ctor_m31519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31519_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31521_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31521_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31521_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31522_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31522_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31522_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31523_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31523_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyTrademarkAttribute_t582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31523_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5229_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31519_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_MethodInfo,
	&InternalEnumerator_1_Dispose_m31521_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31522_MethodInfo,
	&InternalEnumerator_1_get_Current_m31523_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31522_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31521_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5229_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31520_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31522_MethodInfo,
	&InternalEnumerator_1_Dispose_m31521_MethodInfo,
	&InternalEnumerator_1_get_Current_m31523_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5229_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7338_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5229_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7338_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyTrademarkAttribute_t582_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5229_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31523_MethodInfo/* Method Usage */,
	&AssemblyTrademarkAttribute_t582_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyTrademarkAttribute_t582_m41255_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5229_0_0_0;
extern Il2CppType InternalEnumerator_1_t5229_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5229_GenericClass;
TypeInfo InternalEnumerator_1_t5229_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5229_MethodInfos/* methods */
	, InternalEnumerator_1_t5229_PropertyInfos/* properties */
	, InternalEnumerator_1_t5229_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5229_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5229_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5229_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5229_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5229_1_0_0/* this_arg */
	, InternalEnumerator_1_t5229_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5229_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5229_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5229)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9417_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>
extern MethodInfo ICollection_1_get_Count_m52454_MethodInfo;
static PropertyInfo ICollection_1_t9417____Count_PropertyInfo = 
{
	&ICollection_1_t9417_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52454_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52455_MethodInfo;
static PropertyInfo ICollection_1_t9417____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9417_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52455_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9417_PropertyInfos[] =
{
	&ICollection_1_t9417____Count_PropertyInfo,
	&ICollection_1_t9417____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52454_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52454_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52454_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52455_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52455_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52455_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
static ParameterInfo ICollection_1_t9417_ICollection_1_Add_m52456_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttribute_t582_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52456_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52456_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9417_ICollection_1_Add_m52456_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52456_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52457_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52457_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52457_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
static ParameterInfo ICollection_1_t9417_ICollection_1_Contains_m52458_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttribute_t582_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52458_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52458_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9417_ICollection_1_Contains_m52458_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52458_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTrademarkAttributeU5BU5D_t5531_0_0_0;
extern Il2CppType AssemblyTrademarkAttributeU5BU5D_t5531_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9417_ICollection_1_CopyTo_m52459_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttributeU5BU5D_t5531_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52459_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52459_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9417_ICollection_1_CopyTo_m52459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52459_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
static ParameterInfo ICollection_1_t9417_ICollection_1_Remove_m52460_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttribute_t582_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52460_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52460_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9417_ICollection_1_Remove_m52460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52460_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9417_MethodInfos[] =
{
	&ICollection_1_get_Count_m52454_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52455_MethodInfo,
	&ICollection_1_Add_m52456_MethodInfo,
	&ICollection_1_Clear_m52457_MethodInfo,
	&ICollection_1_Contains_m52458_MethodInfo,
	&ICollection_1_CopyTo_m52459_MethodInfo,
	&ICollection_1_Remove_m52460_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9419_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9417_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9419_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9417_0_0_0;
extern Il2CppType ICollection_1_t9417_1_0_0;
struct ICollection_1_t9417;
extern Il2CppGenericClass ICollection_1_t9417_GenericClass;
TypeInfo ICollection_1_t9417_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9417_MethodInfos/* methods */
	, ICollection_1_t9417_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9417_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9417_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9417_0_0_0/* byval_arg */
	, &ICollection_1_t9417_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9417_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTrademarkAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTrademarkAttribute>
extern Il2CppType IEnumerator_1_t7338_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52461_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTrademarkAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52461_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9419_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7338_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52461_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9419_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52461_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9419_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9419_0_0_0;
extern Il2CppType IEnumerable_1_t9419_1_0_0;
struct IEnumerable_1_t9419;
extern Il2CppGenericClass IEnumerable_1_t9419_GenericClass;
TypeInfo IEnumerable_1_t9419_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9419_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9419_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9419_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9419_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9419_0_0_0/* byval_arg */
	, &IEnumerable_1_t9419_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9419_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9418_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>
extern MethodInfo IList_1_get_Item_m52462_MethodInfo;
extern MethodInfo IList_1_set_Item_m52463_MethodInfo;
static PropertyInfo IList_1_t9418____Item_PropertyInfo = 
{
	&IList_1_t9418_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52462_MethodInfo/* get */
	, &IList_1_set_Item_m52463_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9418_PropertyInfos[] =
{
	&IList_1_t9418____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
static ParameterInfo IList_1_t9418_IList_1_IndexOf_m52464_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttribute_t582_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52464_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52464_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9418_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9418_IList_1_IndexOf_m52464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52464_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
static ParameterInfo IList_1_t9418_IList_1_Insert_m52465_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttribute_t582_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52465_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52465_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9418_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9418_IList_1_Insert_m52465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52465_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9418_IList_1_RemoveAt_m52466_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52466_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52466_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9418_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9418_IList_1_RemoveAt_m52466_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52466_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9418_IList_1_get_Item_m52462_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52462_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52462_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9418_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyTrademarkAttribute_t582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9418_IList_1_get_Item_m52462_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52462_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AssemblyTrademarkAttribute_t582_0_0_0;
static ParameterInfo IList_1_t9418_IList_1_set_Item_m52463_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyTrademarkAttribute_t582_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52463_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52463_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9418_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9418_IList_1_set_Item_m52463_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52463_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9418_MethodInfos[] =
{
	&IList_1_IndexOf_m52464_MethodInfo,
	&IList_1_Insert_m52465_MethodInfo,
	&IList_1_RemoveAt_m52466_MethodInfo,
	&IList_1_get_Item_m52462_MethodInfo,
	&IList_1_set_Item_m52463_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9418_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9417_il2cpp_TypeInfo,
	&IEnumerable_1_t9419_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9418_0_0_0;
extern Il2CppType IList_1_t9418_1_0_0;
struct IList_1_t9418;
extern Il2CppGenericClass IList_1_t9418_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9418_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9418_MethodInfos/* methods */
	, IList_1_t9418_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9418_il2cpp_TypeInfo/* element_class */
	, IList_1_t9418_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9418_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9418_0_0_0/* byval_arg */
	, &IList_1_t9418_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9418_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7340_il2cpp_TypeInfo;

// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
extern MethodInfo IEnumerator_1_get_Current_m52467_MethodInfo;
static PropertyInfo IEnumerator_1_t7340____Current_PropertyInfo = 
{
	&IEnumerator_1_t7340_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52467_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7340_PropertyInfos[] =
{
	&IEnumerator_1_t7340____Current_PropertyInfo,
	NULL
};
extern Il2CppType PropertyInfo_t1758_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52467_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52467_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7340_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t1758_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52467_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7340_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52467_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7340_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7340_0_0_0;
extern Il2CppType IEnumerator_1_t7340_1_0_0;
struct IEnumerator_1_t7340;
extern Il2CppGenericClass IEnumerator_1_t7340_GenericClass;
TypeInfo IEnumerator_1_t7340_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7340_MethodInfos/* methods */
	, IEnumerator_1_t7340_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7340_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7340_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7340_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7340_0_0_0/* byval_arg */
	, &IEnumerator_1_t7340_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7340_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_686.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5230_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_686MethodDeclarations.h"

extern TypeInfo PropertyInfo_t1758_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31528_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPropertyInfo_t1758_m41266_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.PropertyInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.PropertyInfo>(System.Int32)
#define Array_InternalArray__get_Item_TisPropertyInfo_t1758_m41266(__this, p0, method) (PropertyInfo_t1758 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5230____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5230, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5230____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5230, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5230_FieldInfos[] =
{
	&InternalEnumerator_1_t5230____array_0_FieldInfo,
	&InternalEnumerator_1_t5230____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5230____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5230_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5230____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5230_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5230_PropertyInfos[] =
{
	&InternalEnumerator_1_t5230____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5230____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5230_InternalEnumerator_1__ctor_m31524_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31524_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31524_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5230_InternalEnumerator_1__ctor_m31524_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31524_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31526_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31526_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31526_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31527_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31527_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31527_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyInfo_t1758_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31528_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31528_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t1758_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31528_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5230_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31524_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_MethodInfo,
	&InternalEnumerator_1_Dispose_m31526_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31527_MethodInfo,
	&InternalEnumerator_1_get_Current_m31528_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31527_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31526_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5230_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31525_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31527_MethodInfo,
	&InternalEnumerator_1_Dispose_m31526_MethodInfo,
	&InternalEnumerator_1_get_Current_m31528_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5230_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7340_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5230_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7340_il2cpp_TypeInfo, 7},
};
extern TypeInfo PropertyInfo_t1758_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5230_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31528_MethodInfo/* Method Usage */,
	&PropertyInfo_t1758_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPropertyInfo_t1758_m41266_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5230_0_0_0;
extern Il2CppType InternalEnumerator_1_t5230_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5230_GenericClass;
TypeInfo InternalEnumerator_1_t5230_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5230_MethodInfos/* methods */
	, InternalEnumerator_1_t5230_PropertyInfos/* properties */
	, InternalEnumerator_1_t5230_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5230_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5230_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5230_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5230_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5230_1_0_0/* this_arg */
	, InternalEnumerator_1_t5230_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5230_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5230_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5230)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9420_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>
extern MethodInfo ICollection_1_get_Count_m52468_MethodInfo;
static PropertyInfo ICollection_1_t9420____Count_PropertyInfo = 
{
	&ICollection_1_t9420_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52468_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52469_MethodInfo;
static PropertyInfo ICollection_1_t9420____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9420_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52469_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9420_PropertyInfos[] =
{
	&ICollection_1_t9420____Count_PropertyInfo,
	&ICollection_1_t9420____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52468_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m52468_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52468_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52469_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52469_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52469_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyInfo_t1758_0_0_0;
extern Il2CppType PropertyInfo_t1758_0_0_0;
static ParameterInfo ICollection_1_t9420_ICollection_1_Add_m52470_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyInfo_t1758_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52470_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Add(T)
MethodInfo ICollection_1_Add_m52470_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9420_ICollection_1_Add_m52470_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52470_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52471_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Clear()
MethodInfo ICollection_1_Clear_m52471_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52471_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyInfo_t1758_0_0_0;
static ParameterInfo ICollection_1_t9420_ICollection_1_Contains_m52472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyInfo_t1758_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52472_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m52472_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9420_ICollection_1_Contains_m52472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52472_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyInfoU5BU5D_t1976_0_0_0;
extern Il2CppType PropertyInfoU5BU5D_t1976_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9420_ICollection_1_CopyTo_m52473_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PropertyInfoU5BU5D_t1976_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52473_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52473_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9420_ICollection_1_CopyTo_m52473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52473_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyInfo_t1758_0_0_0;
static ParameterInfo ICollection_1_t9420_ICollection_1_Remove_m52474_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyInfo_t1758_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52474_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m52474_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9420_ICollection_1_Remove_m52474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52474_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9420_MethodInfos[] =
{
	&ICollection_1_get_Count_m52468_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52469_MethodInfo,
	&ICollection_1_Add_m52470_MethodInfo,
	&ICollection_1_Clear_m52471_MethodInfo,
	&ICollection_1_Contains_m52472_MethodInfo,
	&ICollection_1_CopyTo_m52473_MethodInfo,
	&ICollection_1_Remove_m52474_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9422_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9420_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9422_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9420_0_0_0;
extern Il2CppType ICollection_1_t9420_1_0_0;
struct ICollection_1_t9420;
extern Il2CppGenericClass ICollection_1_t9420_GenericClass;
TypeInfo ICollection_1_t9420_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9420_MethodInfos/* methods */
	, ICollection_1_t9420_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9420_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9420_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9420_0_0_0/* byval_arg */
	, &ICollection_1_t9420_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9420_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
extern Il2CppType IEnumerator_1_t7340_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52475_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52475_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9422_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7340_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52475_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9422_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52475_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9422_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9422_0_0_0;
extern Il2CppType IEnumerable_1_t9422_1_0_0;
struct IEnumerable_1_t9422;
extern Il2CppGenericClass IEnumerable_1_t9422_GenericClass;
TypeInfo IEnumerable_1_t9422_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9422_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9422_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9422_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9422_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9422_0_0_0/* byval_arg */
	, &IEnumerable_1_t9422_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9422_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9421_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>
extern MethodInfo IList_1_get_Item_m52476_MethodInfo;
extern MethodInfo IList_1_set_Item_m52477_MethodInfo;
static PropertyInfo IList_1_t9421____Item_PropertyInfo = 
{
	&IList_1_t9421_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52476_MethodInfo/* get */
	, &IList_1_set_Item_m52477_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9421_PropertyInfos[] =
{
	&IList_1_t9421____Item_PropertyInfo,
	NULL
};
extern Il2CppType PropertyInfo_t1758_0_0_0;
static ParameterInfo IList_1_t9421_IList_1_IndexOf_m52478_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyInfo_t1758_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52478_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52478_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9421_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9421_IList_1_IndexOf_m52478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52478_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropertyInfo_t1758_0_0_0;
static ParameterInfo IList_1_t9421_IList_1_Insert_m52479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PropertyInfo_t1758_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52479_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52479_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9421_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9421_IList_1_Insert_m52479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52479_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9421_IList_1_RemoveAt_m52480_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52480_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52480_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9421_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9421_IList_1_RemoveAt_m52480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52480_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9421_IList_1_get_Item_m52476_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PropertyInfo_t1758_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52476_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52476_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9421_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t1758_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9421_IList_1_get_Item_m52476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52476_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropertyInfo_t1758_0_0_0;
static ParameterInfo IList_1_t9421_IList_1_set_Item_m52477_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PropertyInfo_t1758_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52477_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52477_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9421_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9421_IList_1_set_Item_m52477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52477_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9421_MethodInfos[] =
{
	&IList_1_IndexOf_m52478_MethodInfo,
	&IList_1_Insert_m52479_MethodInfo,
	&IList_1_RemoveAt_m52480_MethodInfo,
	&IList_1_get_Item_m52476_MethodInfo,
	&IList_1_set_Item_m52477_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9421_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9420_il2cpp_TypeInfo,
	&IEnumerable_1_t9422_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9421_0_0_0;
extern Il2CppType IList_1_t9421_1_0_0;
struct IList_1_t9421;
extern Il2CppGenericClass IList_1_t9421_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9421_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9421_MethodInfos/* methods */
	, IList_1_t9421_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9421_il2cpp_TypeInfo/* element_class */
	, IList_1_t9421_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9421_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9421_0_0_0/* byval_arg */
	, &IList_1_t9421_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9421_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9423_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>
extern MethodInfo ICollection_1_get_Count_m52481_MethodInfo;
static PropertyInfo ICollection_1_t9423____Count_PropertyInfo = 
{
	&ICollection_1_t9423_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52481_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52482_MethodInfo;
static PropertyInfo ICollection_1_t9423____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9423_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52482_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9423_PropertyInfos[] =
{
	&ICollection_1_t9423____Count_PropertyInfo,
	&ICollection_1_t9423____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52481_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m52481_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52481_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52482_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52482_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52482_GenericMethod/* genericMethod */

};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
extern Il2CppType _PropertyInfo_t2677_0_0_0;
static ParameterInfo ICollection_1_t9423_ICollection_1_Add_m52483_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_PropertyInfo_t2677_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52483_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Add(T)
MethodInfo ICollection_1_Add_m52483_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9423_ICollection_1_Add_m52483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52483_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52484_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Clear()
MethodInfo ICollection_1_Clear_m52484_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52484_GenericMethod/* genericMethod */

};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
static ParameterInfo ICollection_1_t9423_ICollection_1_Contains_m52485_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_PropertyInfo_t2677_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52485_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m52485_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9423_ICollection_1_Contains_m52485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52485_GenericMethod/* genericMethod */

};
extern Il2CppType _PropertyInfoU5BU5D_t5532_0_0_0;
extern Il2CppType _PropertyInfoU5BU5D_t5532_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9423_ICollection_1_CopyTo_m52486_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_PropertyInfoU5BU5D_t5532_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52486_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52486_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9423_ICollection_1_CopyTo_m52486_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52486_GenericMethod/* genericMethod */

};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
static ParameterInfo ICollection_1_t9423_ICollection_1_Remove_m52487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_PropertyInfo_t2677_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52487_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m52487_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9423_ICollection_1_Remove_m52487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52487_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9423_MethodInfos[] =
{
	&ICollection_1_get_Count_m52481_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52482_MethodInfo,
	&ICollection_1_Add_m52483_MethodInfo,
	&ICollection_1_Clear_m52484_MethodInfo,
	&ICollection_1_Contains_m52485_MethodInfo,
	&ICollection_1_CopyTo_m52486_MethodInfo,
	&ICollection_1_Remove_m52487_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9425_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9423_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9425_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9423_0_0_0;
extern Il2CppType ICollection_1_t9423_1_0_0;
struct ICollection_1_t9423;
extern Il2CppGenericClass ICollection_1_t9423_GenericClass;
TypeInfo ICollection_1_t9423_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9423_MethodInfos/* methods */
	, ICollection_1_t9423_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9423_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9423_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9423_0_0_0/* byval_arg */
	, &ICollection_1_t9423_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9423_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._PropertyInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._PropertyInfo>
extern Il2CppType IEnumerator_1_t7342_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52488_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._PropertyInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52488_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9425_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7342_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52488_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9425_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52488_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9425_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9425_0_0_0;
extern Il2CppType IEnumerable_1_t9425_1_0_0;
struct IEnumerable_1_t9425;
extern Il2CppGenericClass IEnumerable_1_t9425_GenericClass;
TypeInfo IEnumerable_1_t9425_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9425_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9425_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9425_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9425_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9425_0_0_0/* byval_arg */
	, &IEnumerable_1_t9425_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9425_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7342_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._PropertyInfo>
extern MethodInfo IEnumerator_1_get_Current_m52489_MethodInfo;
static PropertyInfo IEnumerator_1_t7342____Current_PropertyInfo = 
{
	&IEnumerator_1_t7342_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52489_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7342_PropertyInfos[] =
{
	&IEnumerator_1_t7342____Current_PropertyInfo,
	NULL
};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52489_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52489_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7342_il2cpp_TypeInfo/* declaring_type */
	, &_PropertyInfo_t2677_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52489_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7342_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52489_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7342_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7342_0_0_0;
extern Il2CppType IEnumerator_1_t7342_1_0_0;
struct IEnumerator_1_t7342;
extern Il2CppGenericClass IEnumerator_1_t7342_GenericClass;
TypeInfo IEnumerator_1_t7342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7342_MethodInfos/* methods */
	, IEnumerator_1_t7342_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7342_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7342_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7342_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7342_0_0_0/* byval_arg */
	, &IEnumerator_1_t7342_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7342_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_687.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5231_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_687MethodDeclarations.h"

extern TypeInfo _PropertyInfo_t2677_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31533_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_PropertyInfo_t2677_m41277_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._PropertyInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._PropertyInfo>(System.Int32)
#define Array_InternalArray__get_Item_Tis_PropertyInfo_t2677_m41277(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5231____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5231, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5231____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5231, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5231_FieldInfos[] =
{
	&InternalEnumerator_1_t5231____array_0_FieldInfo,
	&InternalEnumerator_1_t5231____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5231____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5231_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5231____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5231_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5231_PropertyInfos[] =
{
	&InternalEnumerator_1_t5231____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5231____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5231_InternalEnumerator_1__ctor_m31529_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31529_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31529_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5231_InternalEnumerator_1__ctor_m31529_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31529_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31531_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31531_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31531_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31532_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31532_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31532_GenericMethod/* genericMethod */

};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31533_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31533_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* declaring_type */
	, &_PropertyInfo_t2677_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31533_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5231_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31529_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_MethodInfo,
	&InternalEnumerator_1_Dispose_m31531_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31532_MethodInfo,
	&InternalEnumerator_1_get_Current_m31533_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31532_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31531_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5231_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31530_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31532_MethodInfo,
	&InternalEnumerator_1_Dispose_m31531_MethodInfo,
	&InternalEnumerator_1_get_Current_m31533_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5231_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7342_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5231_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7342_il2cpp_TypeInfo, 7},
};
extern TypeInfo _PropertyInfo_t2677_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5231_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31533_MethodInfo/* Method Usage */,
	&_PropertyInfo_t2677_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_PropertyInfo_t2677_m41277_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5231_0_0_0;
extern Il2CppType InternalEnumerator_1_t5231_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5231_GenericClass;
TypeInfo InternalEnumerator_1_t5231_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5231_MethodInfos/* methods */
	, InternalEnumerator_1_t5231_PropertyInfos/* properties */
	, InternalEnumerator_1_t5231_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5231_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5231_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5231_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5231_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5231_1_0_0/* this_arg */
	, InternalEnumerator_1_t5231_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5231_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5231_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5231)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9424_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>
extern MethodInfo IList_1_get_Item_m52490_MethodInfo;
extern MethodInfo IList_1_set_Item_m52491_MethodInfo;
static PropertyInfo IList_1_t9424____Item_PropertyInfo = 
{
	&IList_1_t9424_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52490_MethodInfo/* get */
	, &IList_1_set_Item_m52491_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9424_PropertyInfos[] =
{
	&IList_1_t9424____Item_PropertyInfo,
	NULL
};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
static ParameterInfo IList_1_t9424_IList_1_IndexOf_m52492_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_PropertyInfo_t2677_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52492_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52492_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9424_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9424_IList_1_IndexOf_m52492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52492_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _PropertyInfo_t2677_0_0_0;
static ParameterInfo IList_1_t9424_IList_1_Insert_m52493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_PropertyInfo_t2677_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52493_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52493_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9424_IList_1_Insert_m52493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52493_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9424_IList_1_RemoveAt_m52494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52494_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52494_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9424_IList_1_RemoveAt_m52494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52494_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9424_IList_1_get_Item_m52490_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType _PropertyInfo_t2677_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52490_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52490_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9424_il2cpp_TypeInfo/* declaring_type */
	, &_PropertyInfo_t2677_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9424_IList_1_get_Item_m52490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52490_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _PropertyInfo_t2677_0_0_0;
static ParameterInfo IList_1_t9424_IList_1_set_Item_m52491_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_PropertyInfo_t2677_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52491_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52491_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9424_IList_1_set_Item_m52491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52491_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9424_MethodInfos[] =
{
	&IList_1_IndexOf_m52492_MethodInfo,
	&IList_1_Insert_m52493_MethodInfo,
	&IList_1_RemoveAt_m52494_MethodInfo,
	&IList_1_get_Item_m52490_MethodInfo,
	&IList_1_set_Item_m52491_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9424_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9423_il2cpp_TypeInfo,
	&IEnumerable_1_t9425_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9424_0_0_0;
extern Il2CppType IList_1_t9424_1_0_0;
struct IList_1_t9424;
extern Il2CppGenericClass IList_1_t9424_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9424_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9424_MethodInfos/* methods */
	, IList_1_t9424_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9424_il2cpp_TypeInfo/* element_class */
	, IList_1_t9424_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9424_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9424_0_0_0/* byval_arg */
	, &IList_1_t9424_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9424_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7344_il2cpp_TypeInfo;

// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.BindingFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.BindingFlags>
extern MethodInfo IEnumerator_1_get_Current_m52495_MethodInfo;
static PropertyInfo IEnumerator_1_t7344____Current_PropertyInfo = 
{
	&IEnumerator_1_t7344_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52495_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7344_PropertyInfos[] =
{
	&IEnumerator_1_t7344____Current_PropertyInfo,
	NULL
};
extern Il2CppType BindingFlags_t142_0_0_0;
extern void* RuntimeInvoker_BindingFlags_t142 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52495_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.BindingFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52495_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &BindingFlags_t142_0_0_0/* return_type */
	, RuntimeInvoker_BindingFlags_t142/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52495_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7344_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52495_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7344_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7344_0_0_0;
extern Il2CppType IEnumerator_1_t7344_1_0_0;
struct IEnumerator_1_t7344;
extern Il2CppGenericClass IEnumerator_1_t7344_GenericClass;
TypeInfo IEnumerator_1_t7344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7344_MethodInfos/* methods */
	, IEnumerator_1_t7344_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7344_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7344_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7344_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7344_0_0_0/* byval_arg */
	, &IEnumerator_1_t7344_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7344_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_688.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5232_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_688MethodDeclarations.h"

extern TypeInfo BindingFlags_t142_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31538_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBindingFlags_t142_m41288_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.BindingFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.BindingFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisBindingFlags_t142_m41288 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31534_MethodInfo;
 void InternalEnumerator_1__ctor_m31534 (InternalEnumerator_1_t5232 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535 (InternalEnumerator_1_t5232 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31538(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31538_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&BindingFlags_t142_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31536_MethodInfo;
 void InternalEnumerator_1_Dispose_m31536 (InternalEnumerator_1_t5232 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31537_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31537 (InternalEnumerator_1_t5232 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31538 (InternalEnumerator_1_t5232 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisBindingFlags_t142_m41288(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisBindingFlags_t142_m41288_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5232____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5232, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5232____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5232, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5232_FieldInfos[] =
{
	&InternalEnumerator_1_t5232____array_0_FieldInfo,
	&InternalEnumerator_1_t5232____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5232____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5232_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5232____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5232_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31538_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5232_PropertyInfos[] =
{
	&InternalEnumerator_1_t5232____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5232____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5232_InternalEnumerator_1__ctor_m31534_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31534_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31534_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31534/* method */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5232_InternalEnumerator_1__ctor_m31534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31534_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535/* method */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31536_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31536_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31536/* method */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31536_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31537_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31537_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31537/* method */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31537_GenericMethod/* genericMethod */

};
extern Il2CppType BindingFlags_t142_0_0_0;
extern void* RuntimeInvoker_BindingFlags_t142 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31538_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31538_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31538/* method */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* declaring_type */
	, &BindingFlags_t142_0_0_0/* return_type */
	, RuntimeInvoker_BindingFlags_t142/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31538_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5232_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31534_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_MethodInfo,
	&InternalEnumerator_1_Dispose_m31536_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31537_MethodInfo,
	&InternalEnumerator_1_get_Current_m31538_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5232_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31535_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31537_MethodInfo,
	&InternalEnumerator_1_Dispose_m31536_MethodInfo,
	&InternalEnumerator_1_get_Current_m31538_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5232_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7344_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5232_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7344_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5232_0_0_0;
extern Il2CppType InternalEnumerator_1_t5232_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5232_GenericClass;
TypeInfo InternalEnumerator_1_t5232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5232_MethodInfos/* methods */
	, InternalEnumerator_1_t5232_PropertyInfos/* properties */
	, InternalEnumerator_1_t5232_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5232_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5232_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5232_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5232_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5232_1_0_0/* this_arg */
	, InternalEnumerator_1_t5232_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5232_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5232)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9426_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>
extern MethodInfo ICollection_1_get_Count_m52496_MethodInfo;
static PropertyInfo ICollection_1_t9426____Count_PropertyInfo = 
{
	&ICollection_1_t9426_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52496_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52497_MethodInfo;
static PropertyInfo ICollection_1_t9426____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9426_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52497_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9426_PropertyInfos[] =
{
	&ICollection_1_t9426____Count_PropertyInfo,
	&ICollection_1_t9426____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52496_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m52496_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52496_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52497_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52497_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52497_GenericMethod/* genericMethod */

};
extern Il2CppType BindingFlags_t142_0_0_0;
extern Il2CppType BindingFlags_t142_0_0_0;
static ParameterInfo ICollection_1_t9426_ICollection_1_Add_m52498_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BindingFlags_t142_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52498_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Add(T)
MethodInfo ICollection_1_Add_m52498_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9426_ICollection_1_Add_m52498_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52498_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52499_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Clear()
MethodInfo ICollection_1_Clear_m52499_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52499_GenericMethod/* genericMethod */

};
extern Il2CppType BindingFlags_t142_0_0_0;
static ParameterInfo ICollection_1_t9426_ICollection_1_Contains_m52500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BindingFlags_t142_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52500_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m52500_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9426_ICollection_1_Contains_m52500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52500_GenericMethod/* genericMethod */

};
extern Il2CppType BindingFlagsU5BU5D_t5533_0_0_0;
extern Il2CppType BindingFlagsU5BU5D_t5533_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9426_ICollection_1_CopyTo_m52501_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BindingFlagsU5BU5D_t5533_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52501_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52501_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9426_ICollection_1_CopyTo_m52501_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52501_GenericMethod/* genericMethod */

};
extern Il2CppType BindingFlags_t142_0_0_0;
static ParameterInfo ICollection_1_t9426_ICollection_1_Remove_m52502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BindingFlags_t142_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52502_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m52502_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9426_ICollection_1_Remove_m52502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52502_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9426_MethodInfos[] =
{
	&ICollection_1_get_Count_m52496_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52497_MethodInfo,
	&ICollection_1_Add_m52498_MethodInfo,
	&ICollection_1_Clear_m52499_MethodInfo,
	&ICollection_1_Contains_m52500_MethodInfo,
	&ICollection_1_CopyTo_m52501_MethodInfo,
	&ICollection_1_Remove_m52502_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9428_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9426_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9428_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9426_0_0_0;
extern Il2CppType ICollection_1_t9426_1_0_0;
struct ICollection_1_t9426;
extern Il2CppGenericClass ICollection_1_t9426_GenericClass;
TypeInfo ICollection_1_t9426_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9426_MethodInfos/* methods */
	, ICollection_1_t9426_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9426_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9426_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9426_0_0_0/* byval_arg */
	, &ICollection_1_t9426_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9426_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.BindingFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.BindingFlags>
extern Il2CppType IEnumerator_1_t7344_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52503_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.BindingFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52503_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9428_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7344_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52503_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9428_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52503_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9428_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9428_0_0_0;
extern Il2CppType IEnumerable_1_t9428_1_0_0;
struct IEnumerable_1_t9428;
extern Il2CppGenericClass IEnumerable_1_t9428_GenericClass;
TypeInfo IEnumerable_1_t9428_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9428_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9428_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9428_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9428_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9428_0_0_0/* byval_arg */
	, &IEnumerable_1_t9428_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9428_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9427_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.BindingFlags>
extern MethodInfo IList_1_get_Item_m52504_MethodInfo;
extern MethodInfo IList_1_set_Item_m52505_MethodInfo;
static PropertyInfo IList_1_t9427____Item_PropertyInfo = 
{
	&IList_1_t9427_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52504_MethodInfo/* get */
	, &IList_1_set_Item_m52505_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9427_PropertyInfos[] =
{
	&IList_1_t9427____Item_PropertyInfo,
	NULL
};
extern Il2CppType BindingFlags_t142_0_0_0;
static ParameterInfo IList_1_t9427_IList_1_IndexOf_m52506_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BindingFlags_t142_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52506_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52506_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9427_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9427_IList_1_IndexOf_m52506_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52506_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BindingFlags_t142_0_0_0;
static ParameterInfo IList_1_t9427_IList_1_Insert_m52507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BindingFlags_t142_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52507_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52507_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9427_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9427_IList_1_Insert_m52507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52507_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9427_IList_1_RemoveAt_m52508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52508_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52508_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9427_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9427_IList_1_RemoveAt_m52508_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52508_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9427_IList_1_get_Item_m52504_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BindingFlags_t142_0_0_0;
extern void* RuntimeInvoker_BindingFlags_t142_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52504_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52504_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9427_il2cpp_TypeInfo/* declaring_type */
	, &BindingFlags_t142_0_0_0/* return_type */
	, RuntimeInvoker_BindingFlags_t142_Int32_t123/* invoker_method */
	, IList_1_t9427_IList_1_get_Item_m52504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52504_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BindingFlags_t142_0_0_0;
static ParameterInfo IList_1_t9427_IList_1_set_Item_m52505_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BindingFlags_t142_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52505_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.BindingFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52505_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9427_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9427_IList_1_set_Item_m52505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52505_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9427_MethodInfos[] =
{
	&IList_1_IndexOf_m52506_MethodInfo,
	&IList_1_Insert_m52507_MethodInfo,
	&IList_1_RemoveAt_m52508_MethodInfo,
	&IList_1_get_Item_m52504_MethodInfo,
	&IList_1_set_Item_m52505_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9427_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9426_il2cpp_TypeInfo,
	&IEnumerable_1_t9428_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9427_0_0_0;
extern Il2CppType IList_1_t9427_1_0_0;
struct IList_1_t9427;
extern Il2CppGenericClass IList_1_t9427_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9427_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9427_MethodInfos/* methods */
	, IList_1_t9427_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9427_il2cpp_TypeInfo/* element_class */
	, IList_1_t9427_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9427_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9427_0_0_0/* byval_arg */
	, &IList_1_t9427_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9427_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7346_il2cpp_TypeInfo;

// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.CallingConventions>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.CallingConventions>
extern MethodInfo IEnumerator_1_get_Current_m52509_MethodInfo;
static PropertyInfo IEnumerator_1_t7346____Current_PropertyInfo = 
{
	&IEnumerator_1_t7346_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52509_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7346_PropertyInfos[] =
{
	&IEnumerator_1_t7346____Current_PropertyInfo,
	NULL
};
extern Il2CppType CallingConventions_t1977_0_0_0;
extern void* RuntimeInvoker_CallingConventions_t1977 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52509_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.CallingConventions>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52509_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7346_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1977_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1977/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52509_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7346_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52509_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7346_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7346_0_0_0;
extern Il2CppType IEnumerator_1_t7346_1_0_0;
struct IEnumerator_1_t7346;
extern Il2CppGenericClass IEnumerator_1_t7346_GenericClass;
TypeInfo IEnumerator_1_t7346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7346_MethodInfos/* methods */
	, IEnumerator_1_t7346_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7346_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7346_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7346_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7346_0_0_0/* byval_arg */
	, &IEnumerator_1_t7346_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7346_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_689.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5233_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_689MethodDeclarations.h"

extern TypeInfo CallingConventions_t1977_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31543_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCallingConventions_t1977_m41299_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.CallingConventions>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CallingConventions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCallingConventions_t1977_m41299 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31539_MethodInfo;
 void InternalEnumerator_1__ctor_m31539 (InternalEnumerator_1_t5233 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540 (InternalEnumerator_1_t5233 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31543(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31543_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CallingConventions_t1977_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31541_MethodInfo;
 void InternalEnumerator_1_Dispose_m31541 (InternalEnumerator_1_t5233 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31542_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31542 (InternalEnumerator_1_t5233 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31543 (InternalEnumerator_1_t5233 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCallingConventions_t1977_m41299(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCallingConventions_t1977_m41299_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5233____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5233, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5233____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5233, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5233_FieldInfos[] =
{
	&InternalEnumerator_1_t5233____array_0_FieldInfo,
	&InternalEnumerator_1_t5233____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5233____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5233_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5233____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5233_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31543_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5233_PropertyInfos[] =
{
	&InternalEnumerator_1_t5233____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5233____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5233_InternalEnumerator_1__ctor_m31539_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31539_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31539_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31539/* method */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5233_InternalEnumerator_1__ctor_m31539_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31539_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540/* method */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31541_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31541_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31541/* method */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31541_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31542_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31542_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31542/* method */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31542_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConventions_t1977_0_0_0;
extern void* RuntimeInvoker_CallingConventions_t1977 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31543_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31543_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31543/* method */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1977_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1977/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31543_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5233_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31539_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_MethodInfo,
	&InternalEnumerator_1_Dispose_m31541_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31542_MethodInfo,
	&InternalEnumerator_1_get_Current_m31543_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5233_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31540_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31542_MethodInfo,
	&InternalEnumerator_1_Dispose_m31541_MethodInfo,
	&InternalEnumerator_1_get_Current_m31543_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5233_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7346_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5233_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7346_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5233_0_0_0;
extern Il2CppType InternalEnumerator_1_t5233_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5233_GenericClass;
TypeInfo InternalEnumerator_1_t5233_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5233_MethodInfos/* methods */
	, InternalEnumerator_1_t5233_PropertyInfos/* properties */
	, InternalEnumerator_1_t5233_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5233_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5233_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5233_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5233_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5233_1_0_0/* this_arg */
	, InternalEnumerator_1_t5233_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5233_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5233)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9429_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>
extern MethodInfo ICollection_1_get_Count_m52510_MethodInfo;
static PropertyInfo ICollection_1_t9429____Count_PropertyInfo = 
{
	&ICollection_1_t9429_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52510_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52511_MethodInfo;
static PropertyInfo ICollection_1_t9429____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9429_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9429_PropertyInfos[] =
{
	&ICollection_1_t9429____Count_PropertyInfo,
	&ICollection_1_t9429____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52510_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::get_Count()
MethodInfo ICollection_1_get_Count_m52510_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52510_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52511_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52511_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52511_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConventions_t1977_0_0_0;
extern Il2CppType CallingConventions_t1977_0_0_0;
static ParameterInfo ICollection_1_t9429_ICollection_1_Add_m52512_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConventions_t1977_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52512_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Add(T)
MethodInfo ICollection_1_Add_m52512_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9429_ICollection_1_Add_m52512_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52512_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52513_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Clear()
MethodInfo ICollection_1_Clear_m52513_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52513_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConventions_t1977_0_0_0;
static ParameterInfo ICollection_1_t9429_ICollection_1_Contains_m52514_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConventions_t1977_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52514_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Contains(T)
MethodInfo ICollection_1_Contains_m52514_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9429_ICollection_1_Contains_m52514_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52514_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConventionsU5BU5D_t5534_0_0_0;
extern Il2CppType CallingConventionsU5BU5D_t5534_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9429_ICollection_1_CopyTo_m52515_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CallingConventionsU5BU5D_t5534_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52515_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52515_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9429_ICollection_1_CopyTo_m52515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52515_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConventions_t1977_0_0_0;
static ParameterInfo ICollection_1_t9429_ICollection_1_Remove_m52516_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConventions_t1977_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52516_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>::Remove(T)
MethodInfo ICollection_1_Remove_m52516_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9429_ICollection_1_Remove_m52516_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52516_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9429_MethodInfos[] =
{
	&ICollection_1_get_Count_m52510_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52511_MethodInfo,
	&ICollection_1_Add_m52512_MethodInfo,
	&ICollection_1_Clear_m52513_MethodInfo,
	&ICollection_1_Contains_m52514_MethodInfo,
	&ICollection_1_CopyTo_m52515_MethodInfo,
	&ICollection_1_Remove_m52516_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9431_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9429_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9431_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9429_0_0_0;
extern Il2CppType ICollection_1_t9429_1_0_0;
struct ICollection_1_t9429;
extern Il2CppGenericClass ICollection_1_t9429_GenericClass;
TypeInfo ICollection_1_t9429_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9429_MethodInfos/* methods */
	, ICollection_1_t9429_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9429_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9429_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9429_0_0_0/* byval_arg */
	, &ICollection_1_t9429_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9429_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CallingConventions>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.CallingConventions>
extern Il2CppType IEnumerator_1_t7346_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52517_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CallingConventions>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52517_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9431_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7346_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52517_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9431_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52517_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9431_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9431_0_0_0;
extern Il2CppType IEnumerable_1_t9431_1_0_0;
struct IEnumerable_1_t9431;
extern Il2CppGenericClass IEnumerable_1_t9431_GenericClass;
TypeInfo IEnumerable_1_t9431_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9431_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9431_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9431_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9431_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9431_0_0_0/* byval_arg */
	, &IEnumerable_1_t9431_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9431_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9430_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.CallingConventions>
extern MethodInfo IList_1_get_Item_m52518_MethodInfo;
extern MethodInfo IList_1_set_Item_m52519_MethodInfo;
static PropertyInfo IList_1_t9430____Item_PropertyInfo = 
{
	&IList_1_t9430_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52518_MethodInfo/* get */
	, &IList_1_set_Item_m52519_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9430_PropertyInfos[] =
{
	&IList_1_t9430____Item_PropertyInfo,
	NULL
};
extern Il2CppType CallingConventions_t1977_0_0_0;
static ParameterInfo IList_1_t9430_IList_1_IndexOf_m52520_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConventions_t1977_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52520_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52520_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9430_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9430_IList_1_IndexOf_m52520_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52520_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CallingConventions_t1977_0_0_0;
static ParameterInfo IList_1_t9430_IList_1_Insert_m52521_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CallingConventions_t1977_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52521_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52521_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9430_IList_1_Insert_m52521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52521_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9430_IList_1_RemoveAt_m52522_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52522_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52522_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9430_IList_1_RemoveAt_m52522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52522_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9430_IList_1_get_Item_m52518_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CallingConventions_t1977_0_0_0;
extern void* RuntimeInvoker_CallingConventions_t1977_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52518_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52518_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9430_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1977_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1977_Int32_t123/* invoker_method */
	, IList_1_t9430_IList_1_get_Item_m52518_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52518_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CallingConventions_t1977_0_0_0;
static ParameterInfo IList_1_t9430_IList_1_set_Item_m52519_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CallingConventions_t1977_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52519_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.CallingConventions>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52519_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9430_IList_1_set_Item_m52519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52519_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9430_MethodInfos[] =
{
	&IList_1_IndexOf_m52520_MethodInfo,
	&IList_1_Insert_m52521_MethodInfo,
	&IList_1_RemoveAt_m52522_MethodInfo,
	&IList_1_get_Item_m52518_MethodInfo,
	&IList_1_set_Item_m52519_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9430_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9429_il2cpp_TypeInfo,
	&IEnumerable_1_t9431_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9430_0_0_0;
extern Il2CppType IList_1_t9430_1_0_0;
struct IList_1_t9430;
extern Il2CppGenericClass IList_1_t9430_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9430_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9430_MethodInfos/* methods */
	, IList_1_t9430_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9430_il2cpp_TypeInfo/* element_class */
	, IList_1_t9430_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9430_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9430_0_0_0/* byval_arg */
	, &IList_1_t9430_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9430_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7348_il2cpp_TypeInfo;

// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.EventAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.EventAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52523_MethodInfo;
static PropertyInfo IEnumerator_1_t7348____Current_PropertyInfo = 
{
	&IEnumerator_1_t7348_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7348_PropertyInfos[] =
{
	&IEnumerator_1_t7348____Current_PropertyInfo,
	NULL
};
extern Il2CppType EventAttributes_t1978_0_0_0;
extern void* RuntimeInvoker_EventAttributes_t1978 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52523_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.EventAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52523_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7348_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t1978_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t1978/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52523_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7348_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52523_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7348_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7348_0_0_0;
extern Il2CppType IEnumerator_1_t7348_1_0_0;
struct IEnumerator_1_t7348;
extern Il2CppGenericClass IEnumerator_1_t7348_GenericClass;
TypeInfo IEnumerator_1_t7348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7348_MethodInfos/* methods */
	, IEnumerator_1_t7348_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7348_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7348_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7348_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7348_0_0_0/* byval_arg */
	, &IEnumerator_1_t7348_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7348_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_690.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5234_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_690MethodDeclarations.h"

extern TypeInfo EventAttributes_t1978_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31548_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEventAttributes_t1978_m41310_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.EventAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.EventAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEventAttributes_t1978_m41310 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31544_MethodInfo;
 void InternalEnumerator_1__ctor_m31544 (InternalEnumerator_1_t5234 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545 (InternalEnumerator_1_t5234 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31548(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31548_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EventAttributes_t1978_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31546_MethodInfo;
 void InternalEnumerator_1_Dispose_m31546 (InternalEnumerator_1_t5234 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31547_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31547 (InternalEnumerator_1_t5234 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31548 (InternalEnumerator_1_t5234 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEventAttributes_t1978_m41310(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEventAttributes_t1978_m41310_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5234____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5234, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5234____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5234, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5234_FieldInfos[] =
{
	&InternalEnumerator_1_t5234____array_0_FieldInfo,
	&InternalEnumerator_1_t5234____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5234____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5234_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5234____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5234_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5234_PropertyInfos[] =
{
	&InternalEnumerator_1_t5234____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5234____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5234_InternalEnumerator_1__ctor_m31544_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31544_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31544_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31544/* method */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5234_InternalEnumerator_1__ctor_m31544_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31544_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545/* method */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31546_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31546_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31546/* method */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31546_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31547_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31547_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31547/* method */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31547_GenericMethod/* genericMethod */

};
extern Il2CppType EventAttributes_t1978_0_0_0;
extern void* RuntimeInvoker_EventAttributes_t1978 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31548_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31548_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31548/* method */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t1978_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t1978/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31548_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5234_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31544_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_MethodInfo,
	&InternalEnumerator_1_Dispose_m31546_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31547_MethodInfo,
	&InternalEnumerator_1_get_Current_m31548_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5234_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31545_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31547_MethodInfo,
	&InternalEnumerator_1_Dispose_m31546_MethodInfo,
	&InternalEnumerator_1_get_Current_m31548_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5234_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7348_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5234_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7348_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5234_0_0_0;
extern Il2CppType InternalEnumerator_1_t5234_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5234_GenericClass;
TypeInfo InternalEnumerator_1_t5234_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5234_MethodInfos/* methods */
	, InternalEnumerator_1_t5234_PropertyInfos/* properties */
	, InternalEnumerator_1_t5234_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5234_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5234_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5234_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5234_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5234_1_0_0/* this_arg */
	, InternalEnumerator_1_t5234_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5234_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5234)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9432_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>
extern MethodInfo ICollection_1_get_Count_m52524_MethodInfo;
static PropertyInfo ICollection_1_t9432____Count_PropertyInfo = 
{
	&ICollection_1_t9432_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52524_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52525_MethodInfo;
static PropertyInfo ICollection_1_t9432____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9432_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9432_PropertyInfos[] =
{
	&ICollection_1_t9432____Count_PropertyInfo,
	&ICollection_1_t9432____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52524_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52524_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52524_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52525_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52525_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52525_GenericMethod/* genericMethod */

};
extern Il2CppType EventAttributes_t1978_0_0_0;
extern Il2CppType EventAttributes_t1978_0_0_0;
static ParameterInfo ICollection_1_t9432_ICollection_1_Add_m52526_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventAttributes_t1978_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52526_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52526_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9432_ICollection_1_Add_m52526_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52526_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52527_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52527_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52527_GenericMethod/* genericMethod */

};
extern Il2CppType EventAttributes_t1978_0_0_0;
static ParameterInfo ICollection_1_t9432_ICollection_1_Contains_m52528_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventAttributes_t1978_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52528_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52528_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9432_ICollection_1_Contains_m52528_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52528_GenericMethod/* genericMethod */

};
extern Il2CppType EventAttributesU5BU5D_t5535_0_0_0;
extern Il2CppType EventAttributesU5BU5D_t5535_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9432_ICollection_1_CopyTo_m52529_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EventAttributesU5BU5D_t5535_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52529_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52529_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9432_ICollection_1_CopyTo_m52529_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52529_GenericMethod/* genericMethod */

};
extern Il2CppType EventAttributes_t1978_0_0_0;
static ParameterInfo ICollection_1_t9432_ICollection_1_Remove_m52530_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventAttributes_t1978_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52530_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52530_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9432_ICollection_1_Remove_m52530_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52530_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9432_MethodInfos[] =
{
	&ICollection_1_get_Count_m52524_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52525_MethodInfo,
	&ICollection_1_Add_m52526_MethodInfo,
	&ICollection_1_Clear_m52527_MethodInfo,
	&ICollection_1_Contains_m52528_MethodInfo,
	&ICollection_1_CopyTo_m52529_MethodInfo,
	&ICollection_1_Remove_m52530_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9434_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9432_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9434_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9432_0_0_0;
extern Il2CppType ICollection_1_t9432_1_0_0;
struct ICollection_1_t9432;
extern Il2CppGenericClass ICollection_1_t9432_GenericClass;
TypeInfo ICollection_1_t9432_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9432_MethodInfos/* methods */
	, ICollection_1_t9432_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9432_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9432_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9432_0_0_0/* byval_arg */
	, &ICollection_1_t9432_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9432_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.EventAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.EventAttributes>
extern Il2CppType IEnumerator_1_t7348_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52531_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.EventAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52531_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9434_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7348_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52531_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9434_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52531_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9434_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9434_0_0_0;
extern Il2CppType IEnumerable_1_t9434_1_0_0;
struct IEnumerable_1_t9434;
extern Il2CppGenericClass IEnumerable_1_t9434_GenericClass;
TypeInfo IEnumerable_1_t9434_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9434_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9434_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9434_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9434_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9434_0_0_0/* byval_arg */
	, &IEnumerable_1_t9434_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9434_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9433_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.EventAttributes>
extern MethodInfo IList_1_get_Item_m52532_MethodInfo;
extern MethodInfo IList_1_set_Item_m52533_MethodInfo;
static PropertyInfo IList_1_t9433____Item_PropertyInfo = 
{
	&IList_1_t9433_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52532_MethodInfo/* get */
	, &IList_1_set_Item_m52533_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9433_PropertyInfos[] =
{
	&IList_1_t9433____Item_PropertyInfo,
	NULL
};
extern Il2CppType EventAttributes_t1978_0_0_0;
static ParameterInfo IList_1_t9433_IList_1_IndexOf_m52534_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventAttributes_t1978_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52534_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52534_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9433_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9433_IList_1_IndexOf_m52534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52534_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventAttributes_t1978_0_0_0;
static ParameterInfo IList_1_t9433_IList_1_Insert_m52535_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EventAttributes_t1978_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52535_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52535_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9433_IList_1_Insert_m52535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52535_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9433_IList_1_RemoveAt_m52536_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52536_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52536_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9433_IList_1_RemoveAt_m52536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52536_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9433_IList_1_get_Item_m52532_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EventAttributes_t1978_0_0_0;
extern void* RuntimeInvoker_EventAttributes_t1978_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52532_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52532_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9433_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t1978_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t1978_Int32_t123/* invoker_method */
	, IList_1_t9433_IList_1_get_Item_m52532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52532_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventAttributes_t1978_0_0_0;
static ParameterInfo IList_1_t9433_IList_1_set_Item_m52533_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EventAttributes_t1978_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52533_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.EventAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52533_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9433_IList_1_set_Item_m52533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52533_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9433_MethodInfos[] =
{
	&IList_1_IndexOf_m52534_MethodInfo,
	&IList_1_Insert_m52535_MethodInfo,
	&IList_1_RemoveAt_m52536_MethodInfo,
	&IList_1_get_Item_m52532_MethodInfo,
	&IList_1_set_Item_m52533_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9433_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9432_il2cpp_TypeInfo,
	&IEnumerable_1_t9434_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9433_0_0_0;
extern Il2CppType IList_1_t9433_1_0_0;
struct IList_1_t9433;
extern Il2CppGenericClass IList_1_t9433_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9433_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9433_MethodInfos/* methods */
	, IList_1_t9433_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9433_il2cpp_TypeInfo/* element_class */
	, IList_1_t9433_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9433_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9433_0_0_0/* byval_arg */
	, &IList_1_t9433_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9433_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7350_il2cpp_TypeInfo;

// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.FieldAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.FieldAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52537_MethodInfo;
static PropertyInfo IEnumerator_1_t7350____Current_PropertyInfo = 
{
	&IEnumerator_1_t7350_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52537_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7350_PropertyInfos[] =
{
	&IEnumerator_1_t7350____Current_PropertyInfo,
	NULL
};
extern Il2CppType FieldAttributes_t1980_0_0_0;
extern void* RuntimeInvoker_FieldAttributes_t1980 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52537_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.FieldAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52537_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t1980_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t1980/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52537_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7350_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52537_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7350_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7350_0_0_0;
extern Il2CppType IEnumerator_1_t7350_1_0_0;
struct IEnumerator_1_t7350;
extern Il2CppGenericClass IEnumerator_1_t7350_GenericClass;
TypeInfo IEnumerator_1_t7350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7350_MethodInfos/* methods */
	, IEnumerator_1_t7350_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7350_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7350_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7350_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7350_0_0_0/* byval_arg */
	, &IEnumerator_1_t7350_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7350_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_691.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5235_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_691MethodDeclarations.h"

extern TypeInfo FieldAttributes_t1980_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31553_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFieldAttributes_t1980_m41321_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.FieldAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.FieldAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFieldAttributes_t1980_m41321 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31549_MethodInfo;
 void InternalEnumerator_1__ctor_m31549 (InternalEnumerator_1_t5235 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550 (InternalEnumerator_1_t5235 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31553(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31553_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FieldAttributes_t1980_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31551_MethodInfo;
 void InternalEnumerator_1_Dispose_m31551 (InternalEnumerator_1_t5235 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31552_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31552 (InternalEnumerator_1_t5235 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31553 (InternalEnumerator_1_t5235 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFieldAttributes_t1980_m41321(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFieldAttributes_t1980_m41321_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5235____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5235, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5235____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5235, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5235_FieldInfos[] =
{
	&InternalEnumerator_1_t5235____array_0_FieldInfo,
	&InternalEnumerator_1_t5235____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5235____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5235_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5235____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5235_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5235_PropertyInfos[] =
{
	&InternalEnumerator_1_t5235____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5235____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5235_InternalEnumerator_1__ctor_m31549_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31549_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31549/* method */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5235_InternalEnumerator_1__ctor_m31549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31549_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550/* method */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31551_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31551_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31551/* method */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31551_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31552_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31552_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31552/* method */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31552_GenericMethod/* genericMethod */

};
extern Il2CppType FieldAttributes_t1980_0_0_0;
extern void* RuntimeInvoker_FieldAttributes_t1980 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31553_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31553_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31553/* method */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t1980_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t1980/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31553_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5235_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31549_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_MethodInfo,
	&InternalEnumerator_1_Dispose_m31551_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31552_MethodInfo,
	&InternalEnumerator_1_get_Current_m31553_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5235_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31552_MethodInfo,
	&InternalEnumerator_1_Dispose_m31551_MethodInfo,
	&InternalEnumerator_1_get_Current_m31553_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5235_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7350_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5235_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7350_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5235_0_0_0;
extern Il2CppType InternalEnumerator_1_t5235_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5235_GenericClass;
TypeInfo InternalEnumerator_1_t5235_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5235_MethodInfos/* methods */
	, InternalEnumerator_1_t5235_PropertyInfos/* properties */
	, InternalEnumerator_1_t5235_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5235_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5235_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5235_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5235_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5235_1_0_0/* this_arg */
	, InternalEnumerator_1_t5235_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5235_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5235)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9435_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>
extern MethodInfo ICollection_1_get_Count_m52538_MethodInfo;
static PropertyInfo ICollection_1_t9435____Count_PropertyInfo = 
{
	&ICollection_1_t9435_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52538_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52539_MethodInfo;
static PropertyInfo ICollection_1_t9435____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9435_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52539_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9435_PropertyInfos[] =
{
	&ICollection_1_t9435____Count_PropertyInfo,
	&ICollection_1_t9435____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52538_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52538_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52538_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52539_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52539_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52539_GenericMethod/* genericMethod */

};
extern Il2CppType FieldAttributes_t1980_0_0_0;
extern Il2CppType FieldAttributes_t1980_0_0_0;
static ParameterInfo ICollection_1_t9435_ICollection_1_Add_m52540_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldAttributes_t1980_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52540_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52540_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9435_ICollection_1_Add_m52540_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52540_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52541_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52541_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52541_GenericMethod/* genericMethod */

};
extern Il2CppType FieldAttributes_t1980_0_0_0;
static ParameterInfo ICollection_1_t9435_ICollection_1_Contains_m52542_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldAttributes_t1980_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52542_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52542_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9435_ICollection_1_Contains_m52542_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52542_GenericMethod/* genericMethod */

};
extern Il2CppType FieldAttributesU5BU5D_t5536_0_0_0;
extern Il2CppType FieldAttributesU5BU5D_t5536_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9435_ICollection_1_CopyTo_m52543_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FieldAttributesU5BU5D_t5536_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52543_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52543_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9435_ICollection_1_CopyTo_m52543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52543_GenericMethod/* genericMethod */

};
extern Il2CppType FieldAttributes_t1980_0_0_0;
static ParameterInfo ICollection_1_t9435_ICollection_1_Remove_m52544_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldAttributes_t1980_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52544_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52544_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9435_ICollection_1_Remove_m52544_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52544_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9435_MethodInfos[] =
{
	&ICollection_1_get_Count_m52538_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52539_MethodInfo,
	&ICollection_1_Add_m52540_MethodInfo,
	&ICollection_1_Clear_m52541_MethodInfo,
	&ICollection_1_Contains_m52542_MethodInfo,
	&ICollection_1_CopyTo_m52543_MethodInfo,
	&ICollection_1_Remove_m52544_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9437_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9435_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9437_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9435_0_0_0;
extern Il2CppType ICollection_1_t9435_1_0_0;
struct ICollection_1_t9435;
extern Il2CppGenericClass ICollection_1_t9435_GenericClass;
TypeInfo ICollection_1_t9435_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9435_MethodInfos/* methods */
	, ICollection_1_t9435_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9435_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9435_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9435_0_0_0/* byval_arg */
	, &ICollection_1_t9435_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9435_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.FieldAttributes>
extern Il2CppType IEnumerator_1_t7350_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52545_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52545_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9437_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7350_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52545_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9437_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52545_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9437_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9437_0_0_0;
extern Il2CppType IEnumerable_1_t9437_1_0_0;
struct IEnumerable_1_t9437;
extern Il2CppGenericClass IEnumerable_1_t9437_GenericClass;
TypeInfo IEnumerable_1_t9437_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9437_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9437_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9437_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9437_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9437_0_0_0/* byval_arg */
	, &IEnumerable_1_t9437_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9437_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9436_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>
extern MethodInfo IList_1_get_Item_m52546_MethodInfo;
extern MethodInfo IList_1_set_Item_m52547_MethodInfo;
static PropertyInfo IList_1_t9436____Item_PropertyInfo = 
{
	&IList_1_t9436_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52546_MethodInfo/* get */
	, &IList_1_set_Item_m52547_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9436_PropertyInfos[] =
{
	&IList_1_t9436____Item_PropertyInfo,
	NULL
};
extern Il2CppType FieldAttributes_t1980_0_0_0;
static ParameterInfo IList_1_t9436_IList_1_IndexOf_m52548_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldAttributes_t1980_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52548_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52548_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9436_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9436_IList_1_IndexOf_m52548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52548_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FieldAttributes_t1980_0_0_0;
static ParameterInfo IList_1_t9436_IList_1_Insert_m52549_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FieldAttributes_t1980_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52549_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52549_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9436_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9436_IList_1_Insert_m52549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52549_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9436_IList_1_RemoveAt_m52550_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52550_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52550_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9436_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9436_IList_1_RemoveAt_m52550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52550_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9436_IList_1_get_Item_m52546_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FieldAttributes_t1980_0_0_0;
extern void* RuntimeInvoker_FieldAttributes_t1980_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52546_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52546_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9436_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t1980_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t1980_Int32_t123/* invoker_method */
	, IList_1_t9436_IList_1_get_Item_m52546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52546_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FieldAttributes_t1980_0_0_0;
static ParameterInfo IList_1_t9436_IList_1_set_Item_m52547_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FieldAttributes_t1980_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52547_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52547_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9436_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9436_IList_1_set_Item_m52547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52547_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9436_MethodInfos[] =
{
	&IList_1_IndexOf_m52548_MethodInfo,
	&IList_1_Insert_m52549_MethodInfo,
	&IList_1_RemoveAt_m52550_MethodInfo,
	&IList_1_get_Item_m52546_MethodInfo,
	&IList_1_set_Item_m52547_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9436_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9435_il2cpp_TypeInfo,
	&IEnumerable_1_t9437_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9436_0_0_0;
extern Il2CppType IList_1_t9436_1_0_0;
struct IList_1_t9436;
extern Il2CppGenericClass IList_1_t9436_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9436_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9436_MethodInfos/* methods */
	, IList_1_t9436_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9436_il2cpp_TypeInfo/* element_class */
	, IList_1_t9436_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9436_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9436_0_0_0/* byval_arg */
	, &IList_1_t9436_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9436_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7352_il2cpp_TypeInfo;

// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.MemberTypes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MemberTypes>
extern MethodInfo IEnumerator_1_get_Current_m52551_MethodInfo;
static PropertyInfo IEnumerator_1_t7352____Current_PropertyInfo = 
{
	&IEnumerator_1_t7352_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52551_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7352_PropertyInfos[] =
{
	&IEnumerator_1_t7352____Current_PropertyInfo,
	NULL
};
extern Il2CppType MemberTypes_t1982_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t1982 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52551_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.MemberTypes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52551_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7352_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1982_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1982/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52551_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7352_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52551_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7352_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7352_0_0_0;
extern Il2CppType IEnumerator_1_t7352_1_0_0;
struct IEnumerator_1_t7352;
extern Il2CppGenericClass IEnumerator_1_t7352_GenericClass;
TypeInfo IEnumerator_1_t7352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7352_MethodInfos/* methods */
	, IEnumerator_1_t7352_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7352_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7352_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7352_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7352_0_0_0/* byval_arg */
	, &IEnumerator_1_t7352_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7352_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_692.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5236_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_692MethodDeclarations.h"

extern TypeInfo MemberTypes_t1982_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31558_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMemberTypes_t1982_m41332_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.MemberTypes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.MemberTypes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMemberTypes_t1982_m41332 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31554_MethodInfo;
 void InternalEnumerator_1__ctor_m31554 (InternalEnumerator_1_t5236 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555 (InternalEnumerator_1_t5236 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31558(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31558_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MemberTypes_t1982_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31556_MethodInfo;
 void InternalEnumerator_1_Dispose_m31556 (InternalEnumerator_1_t5236 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31557_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31557 (InternalEnumerator_1_t5236 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31558 (InternalEnumerator_1_t5236 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMemberTypes_t1982_m41332(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMemberTypes_t1982_m41332_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5236____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5236, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5236____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5236, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5236_FieldInfos[] =
{
	&InternalEnumerator_1_t5236____array_0_FieldInfo,
	&InternalEnumerator_1_t5236____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5236____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5236_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5236____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5236_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31558_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5236_PropertyInfos[] =
{
	&InternalEnumerator_1_t5236____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5236____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5236_InternalEnumerator_1__ctor_m31554_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31554_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31554_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31554/* method */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5236_InternalEnumerator_1__ctor_m31554_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31554_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555/* method */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31556_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31556_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31556/* method */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31556_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31557_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31557_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31557/* method */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31557_GenericMethod/* genericMethod */

};
extern Il2CppType MemberTypes_t1982_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t1982 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31558_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31558_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31558/* method */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1982_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1982/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31558_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5236_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31554_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_MethodInfo,
	&InternalEnumerator_1_Dispose_m31556_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31557_MethodInfo,
	&InternalEnumerator_1_get_Current_m31558_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5236_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31555_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31557_MethodInfo,
	&InternalEnumerator_1_Dispose_m31556_MethodInfo,
	&InternalEnumerator_1_get_Current_m31558_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5236_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7352_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5236_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7352_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5236_0_0_0;
extern Il2CppType InternalEnumerator_1_t5236_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5236_GenericClass;
TypeInfo InternalEnumerator_1_t5236_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5236_MethodInfos/* methods */
	, InternalEnumerator_1_t5236_PropertyInfos/* properties */
	, InternalEnumerator_1_t5236_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5236_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5236_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5236_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5236_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5236_1_0_0/* this_arg */
	, InternalEnumerator_1_t5236_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5236_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5236)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9438_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>
extern MethodInfo ICollection_1_get_Count_m52552_MethodInfo;
static PropertyInfo ICollection_1_t9438____Count_PropertyInfo = 
{
	&ICollection_1_t9438_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52553_MethodInfo;
static PropertyInfo ICollection_1_t9438____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9438_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9438_PropertyInfos[] =
{
	&ICollection_1_t9438____Count_PropertyInfo,
	&ICollection_1_t9438____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52552_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::get_Count()
MethodInfo ICollection_1_get_Count_m52552_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52552_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52553_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52553_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52553_GenericMethod/* genericMethod */

};
extern Il2CppType MemberTypes_t1982_0_0_0;
extern Il2CppType MemberTypes_t1982_0_0_0;
static ParameterInfo ICollection_1_t9438_ICollection_1_Add_m52554_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MemberTypes_t1982_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52554_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Add(T)
MethodInfo ICollection_1_Add_m52554_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9438_ICollection_1_Add_m52554_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52554_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52555_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Clear()
MethodInfo ICollection_1_Clear_m52555_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52555_GenericMethod/* genericMethod */

};
extern Il2CppType MemberTypes_t1982_0_0_0;
static ParameterInfo ICollection_1_t9438_ICollection_1_Contains_m52556_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MemberTypes_t1982_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52556_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Contains(T)
MethodInfo ICollection_1_Contains_m52556_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9438_ICollection_1_Contains_m52556_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52556_GenericMethod/* genericMethod */

};
extern Il2CppType MemberTypesU5BU5D_t5537_0_0_0;
extern Il2CppType MemberTypesU5BU5D_t5537_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9438_ICollection_1_CopyTo_m52557_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MemberTypesU5BU5D_t5537_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52557_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52557_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9438_ICollection_1_CopyTo_m52557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52557_GenericMethod/* genericMethod */

};
extern Il2CppType MemberTypes_t1982_0_0_0;
static ParameterInfo ICollection_1_t9438_ICollection_1_Remove_m52558_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MemberTypes_t1982_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52558_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>::Remove(T)
MethodInfo ICollection_1_Remove_m52558_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9438_ICollection_1_Remove_m52558_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52558_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9438_MethodInfos[] =
{
	&ICollection_1_get_Count_m52552_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52553_MethodInfo,
	&ICollection_1_Add_m52554_MethodInfo,
	&ICollection_1_Clear_m52555_MethodInfo,
	&ICollection_1_Contains_m52556_MethodInfo,
	&ICollection_1_CopyTo_m52557_MethodInfo,
	&ICollection_1_Remove_m52558_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9440_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9438_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9440_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9438_0_0_0;
extern Il2CppType ICollection_1_t9438_1_0_0;
struct ICollection_1_t9438;
extern Il2CppGenericClass ICollection_1_t9438_GenericClass;
TypeInfo ICollection_1_t9438_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9438_MethodInfos/* methods */
	, ICollection_1_t9438_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9438_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9438_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9438_0_0_0/* byval_arg */
	, &ICollection_1_t9438_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9438_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MemberTypes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MemberTypes>
extern Il2CppType IEnumerator_1_t7352_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52559_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MemberTypes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52559_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9440_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7352_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52559_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9440_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52559_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9440_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9440_0_0_0;
extern Il2CppType IEnumerable_1_t9440_1_0_0;
struct IEnumerable_1_t9440;
extern Il2CppGenericClass IEnumerable_1_t9440_GenericClass;
TypeInfo IEnumerable_1_t9440_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9440_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9440_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9440_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9440_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9440_0_0_0/* byval_arg */
	, &IEnumerable_1_t9440_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9440_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9439_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MemberTypes>
extern MethodInfo IList_1_get_Item_m52560_MethodInfo;
extern MethodInfo IList_1_set_Item_m52561_MethodInfo;
static PropertyInfo IList_1_t9439____Item_PropertyInfo = 
{
	&IList_1_t9439_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52560_MethodInfo/* get */
	, &IList_1_set_Item_m52561_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9439_PropertyInfos[] =
{
	&IList_1_t9439____Item_PropertyInfo,
	NULL
};
extern Il2CppType MemberTypes_t1982_0_0_0;
static ParameterInfo IList_1_t9439_IList_1_IndexOf_m52562_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MemberTypes_t1982_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52562_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52562_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9439_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9439_IList_1_IndexOf_m52562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52562_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MemberTypes_t1982_0_0_0;
static ParameterInfo IList_1_t9439_IList_1_Insert_m52563_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MemberTypes_t1982_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52563_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52563_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9439_IList_1_Insert_m52563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52563_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9439_IList_1_RemoveAt_m52564_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52564_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52564_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9439_IList_1_RemoveAt_m52564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52564_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9439_IList_1_get_Item_m52560_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MemberTypes_t1982_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t1982_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52560_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52560_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9439_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1982_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1982_Int32_t123/* invoker_method */
	, IList_1_t9439_IList_1_get_Item_m52560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52560_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MemberTypes_t1982_0_0_0;
static ParameterInfo IList_1_t9439_IList_1_set_Item_m52561_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MemberTypes_t1982_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52561_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MemberTypes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52561_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9439_IList_1_set_Item_m52561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52561_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9439_MethodInfos[] =
{
	&IList_1_IndexOf_m52562_MethodInfo,
	&IList_1_Insert_m52563_MethodInfo,
	&IList_1_RemoveAt_m52564_MethodInfo,
	&IList_1_get_Item_m52560_MethodInfo,
	&IList_1_set_Item_m52561_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9439_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9438_il2cpp_TypeInfo,
	&IEnumerable_1_t9440_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9439_0_0_0;
extern Il2CppType IList_1_t9439_1_0_0;
struct IList_1_t9439;
extern Il2CppGenericClass IList_1_t9439_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9439_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9439_MethodInfos/* methods */
	, IList_1_t9439_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9439_il2cpp_TypeInfo/* element_class */
	, IList_1_t9439_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9439_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9439_0_0_0/* byval_arg */
	, &IList_1_t9439_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9439_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7354_il2cpp_TypeInfo;

// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52565_MethodInfo;
static PropertyInfo IEnumerator_1_t7354____Current_PropertyInfo = 
{
	&IEnumerator_1_t7354_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52565_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7354_PropertyInfos[] =
{
	&IEnumerator_1_t7354____Current_PropertyInfo,
	NULL
};
extern Il2CppType MethodAttributes_t1983_0_0_0;
extern void* RuntimeInvoker_MethodAttributes_t1983 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52565_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52565_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7354_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1983_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1983/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52565_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7354_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52565_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7354_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7354_0_0_0;
extern Il2CppType IEnumerator_1_t7354_1_0_0;
struct IEnumerator_1_t7354;
extern Il2CppGenericClass IEnumerator_1_t7354_GenericClass;
TypeInfo IEnumerator_1_t7354_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7354_MethodInfos/* methods */
	, IEnumerator_1_t7354_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7354_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7354_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7354_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7354_0_0_0/* byval_arg */
	, &IEnumerator_1_t7354_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7354_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_693.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5237_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_693MethodDeclarations.h"

extern TypeInfo MethodAttributes_t1983_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31563_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMethodAttributes_t1983_m41343_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMethodAttributes_t1983_m41343 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31559_MethodInfo;
 void InternalEnumerator_1__ctor_m31559 (InternalEnumerator_1_t5237 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560 (InternalEnumerator_1_t5237 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31563(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31563_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MethodAttributes_t1983_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31561_MethodInfo;
 void InternalEnumerator_1_Dispose_m31561 (InternalEnumerator_1_t5237 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31562_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31562 (InternalEnumerator_1_t5237 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31563 (InternalEnumerator_1_t5237 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMethodAttributes_t1983_m41343(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMethodAttributes_t1983_m41343_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5237____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5237, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5237____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5237, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5237_FieldInfos[] =
{
	&InternalEnumerator_1_t5237____array_0_FieldInfo,
	&InternalEnumerator_1_t5237____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5237____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5237_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5237____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5237_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5237_PropertyInfos[] =
{
	&InternalEnumerator_1_t5237____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5237____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5237_InternalEnumerator_1__ctor_m31559_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31559_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31559_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31559/* method */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5237_InternalEnumerator_1__ctor_m31559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31559_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560/* method */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31561_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31561_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31561/* method */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31561_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31562_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31562_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31562/* method */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31562_GenericMethod/* genericMethod */

};
extern Il2CppType MethodAttributes_t1983_0_0_0;
extern void* RuntimeInvoker_MethodAttributes_t1983 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31563_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31563_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31563/* method */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1983_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1983/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31563_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5237_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31559_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_MethodInfo,
	&InternalEnumerator_1_Dispose_m31561_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31562_MethodInfo,
	&InternalEnumerator_1_get_Current_m31563_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5237_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31560_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31562_MethodInfo,
	&InternalEnumerator_1_Dispose_m31561_MethodInfo,
	&InternalEnumerator_1_get_Current_m31563_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5237_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7354_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5237_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7354_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5237_0_0_0;
extern Il2CppType InternalEnumerator_1_t5237_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5237_GenericClass;
TypeInfo InternalEnumerator_1_t5237_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5237_MethodInfos/* methods */
	, InternalEnumerator_1_t5237_PropertyInfos/* properties */
	, InternalEnumerator_1_t5237_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5237_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5237_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5237_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5237_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5237_1_0_0/* this_arg */
	, InternalEnumerator_1_t5237_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5237_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5237)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9441_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>
extern MethodInfo ICollection_1_get_Count_m52566_MethodInfo;
static PropertyInfo ICollection_1_t9441____Count_PropertyInfo = 
{
	&ICollection_1_t9441_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52566_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52567_MethodInfo;
static PropertyInfo ICollection_1_t9441____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9441_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52567_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9441_PropertyInfos[] =
{
	&ICollection_1_t9441____Count_PropertyInfo,
	&ICollection_1_t9441____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52566_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52566_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52566_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52567_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52567_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52567_GenericMethod/* genericMethod */

};
extern Il2CppType MethodAttributes_t1983_0_0_0;
extern Il2CppType MethodAttributes_t1983_0_0_0;
static ParameterInfo ICollection_1_t9441_ICollection_1_Add_m52568_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodAttributes_t1983_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52568_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52568_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9441_ICollection_1_Add_m52568_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52568_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52569_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52569_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52569_GenericMethod/* genericMethod */

};
extern Il2CppType MethodAttributes_t1983_0_0_0;
static ParameterInfo ICollection_1_t9441_ICollection_1_Contains_m52570_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodAttributes_t1983_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52570_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52570_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9441_ICollection_1_Contains_m52570_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52570_GenericMethod/* genericMethod */

};
extern Il2CppType MethodAttributesU5BU5D_t5538_0_0_0;
extern Il2CppType MethodAttributesU5BU5D_t5538_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9441_ICollection_1_CopyTo_m52571_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodAttributesU5BU5D_t5538_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52571_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52571_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9441_ICollection_1_CopyTo_m52571_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52571_GenericMethod/* genericMethod */

};
extern Il2CppType MethodAttributes_t1983_0_0_0;
static ParameterInfo ICollection_1_t9441_ICollection_1_Remove_m52572_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodAttributes_t1983_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52572_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52572_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9441_ICollection_1_Remove_m52572_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52572_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9441_MethodInfos[] =
{
	&ICollection_1_get_Count_m52566_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52567_MethodInfo,
	&ICollection_1_Add_m52568_MethodInfo,
	&ICollection_1_Clear_m52569_MethodInfo,
	&ICollection_1_Contains_m52570_MethodInfo,
	&ICollection_1_CopyTo_m52571_MethodInfo,
	&ICollection_1_Remove_m52572_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9443_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9441_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9443_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9441_0_0_0;
extern Il2CppType ICollection_1_t9441_1_0_0;
struct ICollection_1_t9441;
extern Il2CppGenericClass ICollection_1_t9441_GenericClass;
TypeInfo ICollection_1_t9441_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9441_MethodInfos/* methods */
	, ICollection_1_t9441_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9441_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9441_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9441_0_0_0/* byval_arg */
	, &ICollection_1_t9441_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9441_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodAttributes>
extern Il2CppType IEnumerator_1_t7354_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52573_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52573_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9443_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7354_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52573_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9443_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52573_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9443_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9443_0_0_0;
extern Il2CppType IEnumerable_1_t9443_1_0_0;
struct IEnumerable_1_t9443;
extern Il2CppGenericClass IEnumerable_1_t9443_GenericClass;
TypeInfo IEnumerable_1_t9443_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9443_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9443_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9443_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9443_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9443_0_0_0/* byval_arg */
	, &IEnumerable_1_t9443_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9443_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9442_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>
extern MethodInfo IList_1_get_Item_m52574_MethodInfo;
extern MethodInfo IList_1_set_Item_m52575_MethodInfo;
static PropertyInfo IList_1_t9442____Item_PropertyInfo = 
{
	&IList_1_t9442_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52574_MethodInfo/* get */
	, &IList_1_set_Item_m52575_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9442_PropertyInfos[] =
{
	&IList_1_t9442____Item_PropertyInfo,
	NULL
};
extern Il2CppType MethodAttributes_t1983_0_0_0;
static ParameterInfo IList_1_t9442_IList_1_IndexOf_m52576_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodAttributes_t1983_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52576_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52576_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9442_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9442_IList_1_IndexOf_m52576_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52576_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodAttributes_t1983_0_0_0;
static ParameterInfo IList_1_t9442_IList_1_Insert_m52577_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodAttributes_t1983_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52577_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52577_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9442_IList_1_Insert_m52577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52577_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9442_IList_1_RemoveAt_m52578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52578_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52578_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9442_IList_1_RemoveAt_m52578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52578_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9442_IList_1_get_Item_m52574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodAttributes_t1983_0_0_0;
extern void* RuntimeInvoker_MethodAttributes_t1983_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52574_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52574_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9442_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1983_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1983_Int32_t123/* invoker_method */
	, IList_1_t9442_IList_1_get_Item_m52574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52574_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodAttributes_t1983_0_0_0;
static ParameterInfo IList_1_t9442_IList_1_set_Item_m52575_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodAttributes_t1983_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52575_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52575_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9442_IList_1_set_Item_m52575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52575_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9442_MethodInfos[] =
{
	&IList_1_IndexOf_m52576_MethodInfo,
	&IList_1_Insert_m52577_MethodInfo,
	&IList_1_RemoveAt_m52578_MethodInfo,
	&IList_1_get_Item_m52574_MethodInfo,
	&IList_1_set_Item_m52575_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9442_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9441_il2cpp_TypeInfo,
	&IEnumerable_1_t9443_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9442_0_0_0;
extern Il2CppType IList_1_t9442_1_0_0;
struct IList_1_t9442;
extern Il2CppGenericClass IList_1_t9442_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9442_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9442_MethodInfos/* methods */
	, IList_1_t9442_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9442_il2cpp_TypeInfo/* element_class */
	, IList_1_t9442_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9442_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9442_0_0_0/* byval_arg */
	, &IList_1_t9442_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9442_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7356_il2cpp_TypeInfo;

// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodImplAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodImplAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52579_MethodInfo;
static PropertyInfo IEnumerator_1_t7356____Current_PropertyInfo = 
{
	&IEnumerator_1_t7356_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52579_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7356_PropertyInfos[] =
{
	&IEnumerator_1_t7356____Current_PropertyInfo,
	NULL
};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
extern void* RuntimeInvoker_MethodImplAttributes_t1984 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52579_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.MethodImplAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52579_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &MethodImplAttributes_t1984_0_0_0/* return_type */
	, RuntimeInvoker_MethodImplAttributes_t1984/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52579_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7356_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52579_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7356_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7356_0_0_0;
extern Il2CppType IEnumerator_1_t7356_1_0_0;
struct IEnumerator_1_t7356;
extern Il2CppGenericClass IEnumerator_1_t7356_GenericClass;
TypeInfo IEnumerator_1_t7356_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7356_MethodInfos/* methods */
	, IEnumerator_1_t7356_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7356_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7356_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7356_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7356_0_0_0/* byval_arg */
	, &IEnumerator_1_t7356_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7356_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_694.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5238_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_694MethodDeclarations.h"

extern TypeInfo MethodImplAttributes_t1984_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31568_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMethodImplAttributes_t1984_m41354_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodImplAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.MethodImplAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMethodImplAttributes_t1984_m41354 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31564_MethodInfo;
 void InternalEnumerator_1__ctor_m31564 (InternalEnumerator_1_t5238 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565 (InternalEnumerator_1_t5238 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31568(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31568_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MethodImplAttributes_t1984_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31566_MethodInfo;
 void InternalEnumerator_1_Dispose_m31566 (InternalEnumerator_1_t5238 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31567_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31567 (InternalEnumerator_1_t5238 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31568 (InternalEnumerator_1_t5238 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMethodImplAttributes_t1984_m41354(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMethodImplAttributes_t1984_m41354_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5238____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5238, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5238____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5238, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5238_FieldInfos[] =
{
	&InternalEnumerator_1_t5238____array_0_FieldInfo,
	&InternalEnumerator_1_t5238____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5238____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5238_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5238____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5238_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5238_PropertyInfos[] =
{
	&InternalEnumerator_1_t5238____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5238____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5238_InternalEnumerator_1__ctor_m31564_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31564_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31564_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31564/* method */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5238_InternalEnumerator_1__ctor_m31564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31564_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565/* method */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31566_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31566_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31566/* method */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31566_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31567_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31567_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31567/* method */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31567_GenericMethod/* genericMethod */

};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
extern void* RuntimeInvoker_MethodImplAttributes_t1984 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31568_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31568_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31568/* method */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* declaring_type */
	, &MethodImplAttributes_t1984_0_0_0/* return_type */
	, RuntimeInvoker_MethodImplAttributes_t1984/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31568_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5238_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31564_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_MethodInfo,
	&InternalEnumerator_1_Dispose_m31566_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31567_MethodInfo,
	&InternalEnumerator_1_get_Current_m31568_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5238_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31565_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31567_MethodInfo,
	&InternalEnumerator_1_Dispose_m31566_MethodInfo,
	&InternalEnumerator_1_get_Current_m31568_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5238_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7356_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5238_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7356_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5238_0_0_0;
extern Il2CppType InternalEnumerator_1_t5238_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5238_GenericClass;
TypeInfo InternalEnumerator_1_t5238_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5238_MethodInfos/* methods */
	, InternalEnumerator_1_t5238_PropertyInfos/* properties */
	, InternalEnumerator_1_t5238_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5238_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5238_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5238_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5238_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5238_1_0_0/* this_arg */
	, InternalEnumerator_1_t5238_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5238_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5238)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9444_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>
extern MethodInfo ICollection_1_get_Count_m52580_MethodInfo;
static PropertyInfo ICollection_1_t9444____Count_PropertyInfo = 
{
	&ICollection_1_t9444_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52580_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52581_MethodInfo;
static PropertyInfo ICollection_1_t9444____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9444_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9444_PropertyInfos[] =
{
	&ICollection_1_t9444____Count_PropertyInfo,
	&ICollection_1_t9444____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52580_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52580_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52580_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52581_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52581_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52581_GenericMethod/* genericMethod */

};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
static ParameterInfo ICollection_1_t9444_ICollection_1_Add_m52582_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributes_t1984_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52582_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52582_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9444_ICollection_1_Add_m52582_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52582_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52583_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52583_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52583_GenericMethod/* genericMethod */

};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
static ParameterInfo ICollection_1_t9444_ICollection_1_Contains_m52584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributes_t1984_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52584_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52584_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9444_ICollection_1_Contains_m52584_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52584_GenericMethod/* genericMethod */

};
extern Il2CppType MethodImplAttributesU5BU5D_t5539_0_0_0;
extern Il2CppType MethodImplAttributesU5BU5D_t5539_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9444_ICollection_1_CopyTo_m52585_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributesU5BU5D_t5539_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52585_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52585_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9444_ICollection_1_CopyTo_m52585_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52585_GenericMethod/* genericMethod */

};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
static ParameterInfo ICollection_1_t9444_ICollection_1_Remove_m52586_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributes_t1984_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52586_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52586_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9444_ICollection_1_Remove_m52586_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52586_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9444_MethodInfos[] =
{
	&ICollection_1_get_Count_m52580_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52581_MethodInfo,
	&ICollection_1_Add_m52582_MethodInfo,
	&ICollection_1_Clear_m52583_MethodInfo,
	&ICollection_1_Contains_m52584_MethodInfo,
	&ICollection_1_CopyTo_m52585_MethodInfo,
	&ICollection_1_Remove_m52586_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9446_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9444_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9446_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9444_0_0_0;
extern Il2CppType ICollection_1_t9444_1_0_0;
struct ICollection_1_t9444;
extern Il2CppGenericClass ICollection_1_t9444_GenericClass;
TypeInfo ICollection_1_t9444_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9444_MethodInfos/* methods */
	, ICollection_1_t9444_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9444_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9444_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9444_0_0_0/* byval_arg */
	, &ICollection_1_t9444_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9444_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodImplAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodImplAttributes>
extern Il2CppType IEnumerator_1_t7356_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52587_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodImplAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52587_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9446_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7356_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52587_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9446_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52587_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9446_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9446_0_0_0;
extern Il2CppType IEnumerable_1_t9446_1_0_0;
struct IEnumerable_1_t9446;
extern Il2CppGenericClass IEnumerable_1_t9446_GenericClass;
TypeInfo IEnumerable_1_t9446_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9446_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9446_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9446_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9446_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9446_0_0_0/* byval_arg */
	, &IEnumerable_1_t9446_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9446_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9445_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>
extern MethodInfo IList_1_get_Item_m52588_MethodInfo;
extern MethodInfo IList_1_set_Item_m52589_MethodInfo;
static PropertyInfo IList_1_t9445____Item_PropertyInfo = 
{
	&IList_1_t9445_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52588_MethodInfo/* get */
	, &IList_1_set_Item_m52589_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9445_PropertyInfos[] =
{
	&IList_1_t9445____Item_PropertyInfo,
	NULL
};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
static ParameterInfo IList_1_t9445_IList_1_IndexOf_m52590_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributes_t1984_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52590_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52590_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9445_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9445_IList_1_IndexOf_m52590_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52590_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
static ParameterInfo IList_1_t9445_IList_1_Insert_m52591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributes_t1984_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52591_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52591_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9445_IList_1_Insert_m52591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52591_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9445_IList_1_RemoveAt_m52592_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52592_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52592_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9445_IList_1_RemoveAt_m52592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52592_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9445_IList_1_get_Item_m52588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
extern void* RuntimeInvoker_MethodImplAttributes_t1984_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52588_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52588_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9445_il2cpp_TypeInfo/* declaring_type */
	, &MethodImplAttributes_t1984_0_0_0/* return_type */
	, RuntimeInvoker_MethodImplAttributes_t1984_Int32_t123/* invoker_method */
	, IList_1_t9445_IList_1_get_Item_m52588_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52588_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodImplAttributes_t1984_0_0_0;
static ParameterInfo IList_1_t9445_IList_1_set_Item_m52589_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodImplAttributes_t1984_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52589_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52589_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9445_IList_1_set_Item_m52589_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52589_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9445_MethodInfos[] =
{
	&IList_1_IndexOf_m52590_MethodInfo,
	&IList_1_Insert_m52591_MethodInfo,
	&IList_1_RemoveAt_m52592_MethodInfo,
	&IList_1_get_Item_m52588_MethodInfo,
	&IList_1_set_Item_m52589_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9445_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9444_il2cpp_TypeInfo,
	&IEnumerable_1_t9446_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9445_0_0_0;
extern Il2CppType IList_1_t9445_1_0_0;
struct IList_1_t9445;
extern Il2CppGenericClass IList_1_t9445_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9445_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9445_MethodInfos/* methods */
	, IList_1_t9445_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9445_il2cpp_TypeInfo/* element_class */
	, IList_1_t9445_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9445_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9445_0_0_0/* byval_arg */
	, &IList_1_t9445_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9445_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7358_il2cpp_TypeInfo;

// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfo.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.PInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.PInfo>
extern MethodInfo IEnumerator_1_get_Current_m52593_MethodInfo;
static PropertyInfo IEnumerator_1_t7358____Current_PropertyInfo = 
{
	&IEnumerator_1_t7358_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52593_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7358_PropertyInfos[] =
{
	&IEnumerator_1_t7358____Current_PropertyInfo,
	NULL
};
extern Il2CppType PInfo_t1997_0_0_0;
extern void* RuntimeInvoker_PInfo_t1997 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52593_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.PInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52593_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7358_il2cpp_TypeInfo/* declaring_type */
	, &PInfo_t1997_0_0_0/* return_type */
	, RuntimeInvoker_PInfo_t1997/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52593_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7358_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52593_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7358_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7358_0_0_0;
extern Il2CppType IEnumerator_1_t7358_1_0_0;
struct IEnumerator_1_t7358;
extern Il2CppGenericClass IEnumerator_1_t7358_GenericClass;
TypeInfo IEnumerator_1_t7358_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7358_MethodInfos/* methods */
	, IEnumerator_1_t7358_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7358_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7358_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7358_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7358_0_0_0/* byval_arg */
	, &IEnumerator_1_t7358_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7358_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.PInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_695.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5239_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.PInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_695MethodDeclarations.h"

extern TypeInfo PInfo_t1997_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31573_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPInfo_t1997_m41365_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.PInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.PInfo>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPInfo_t1997_m41365 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.PInfo>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31569_MethodInfo;
 void InternalEnumerator_1__ctor_m31569 (InternalEnumerator_1_t5239 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PInfo>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570 (InternalEnumerator_1_t5239 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31573(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31573_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&PInfo_t1997_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PInfo>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31571_MethodInfo;
 void InternalEnumerator_1_Dispose_m31571 (InternalEnumerator_1_t5239 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PInfo>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31572_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31572 (InternalEnumerator_1_t5239 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.PInfo>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31573 (InternalEnumerator_1_t5239 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisPInfo_t1997_m41365(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisPInfo_t1997_m41365_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.PInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5239____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5239, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5239____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5239, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5239_FieldInfos[] =
{
	&InternalEnumerator_1_t5239____array_0_FieldInfo,
	&InternalEnumerator_1_t5239____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5239____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5239_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5239____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5239_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31573_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5239_PropertyInfos[] =
{
	&InternalEnumerator_1_t5239____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5239____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5239_InternalEnumerator_1__ctor_m31569_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31569_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31569_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31569/* method */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5239_InternalEnumerator_1__ctor_m31569_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31569_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570/* method */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31571_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31571_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31571/* method */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31571_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31572_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31572_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31572/* method */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31572_GenericMethod/* genericMethod */

};
extern Il2CppType PInfo_t1997_0_0_0;
extern void* RuntimeInvoker_PInfo_t1997 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31573_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.PInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31573_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31573/* method */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* declaring_type */
	, &PInfo_t1997_0_0_0/* return_type */
	, RuntimeInvoker_PInfo_t1997/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31573_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5239_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31569_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_MethodInfo,
	&InternalEnumerator_1_Dispose_m31571_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31572_MethodInfo,
	&InternalEnumerator_1_get_Current_m31573_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5239_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31570_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31572_MethodInfo,
	&InternalEnumerator_1_Dispose_m31571_MethodInfo,
	&InternalEnumerator_1_get_Current_m31573_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5239_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7358_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5239_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7358_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5239_0_0_0;
extern Il2CppType InternalEnumerator_1_t5239_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5239_GenericClass;
TypeInfo InternalEnumerator_1_t5239_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5239_MethodInfos/* methods */
	, InternalEnumerator_1_t5239_PropertyInfos/* properties */
	, InternalEnumerator_1_t5239_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5239_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5239_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5239_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5239_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5239_1_0_0/* this_arg */
	, InternalEnumerator_1_t5239_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5239_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5239)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9447_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.PInfo>
extern MethodInfo ICollection_1_get_Count_m52594_MethodInfo;
static PropertyInfo ICollection_1_t9447____Count_PropertyInfo = 
{
	&ICollection_1_t9447_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52594_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52595_MethodInfo;
static PropertyInfo ICollection_1_t9447____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9447_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52595_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9447_PropertyInfos[] =
{
	&ICollection_1_t9447____Count_PropertyInfo,
	&ICollection_1_t9447____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52594_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m52594_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52594_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52595_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52595_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52595_GenericMethod/* genericMethod */

};
extern Il2CppType PInfo_t1997_0_0_0;
extern Il2CppType PInfo_t1997_0_0_0;
static ParameterInfo ICollection_1_t9447_ICollection_1_Add_m52596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PInfo_t1997_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52596_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Add(T)
MethodInfo ICollection_1_Add_m52596_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9447_ICollection_1_Add_m52596_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52596_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52597_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Clear()
MethodInfo ICollection_1_Clear_m52597_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52597_GenericMethod/* genericMethod */

};
extern Il2CppType PInfo_t1997_0_0_0;
static ParameterInfo ICollection_1_t9447_ICollection_1_Contains_m52598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PInfo_t1997_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52598_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m52598_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9447_ICollection_1_Contains_m52598_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52598_GenericMethod/* genericMethod */

};
extern Il2CppType PInfoU5BU5D_t5540_0_0_0;
extern Il2CppType PInfoU5BU5D_t5540_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9447_ICollection_1_CopyTo_m52599_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PInfoU5BU5D_t5540_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52599_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52599_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9447_ICollection_1_CopyTo_m52599_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52599_GenericMethod/* genericMethod */

};
extern Il2CppType PInfo_t1997_0_0_0;
static ParameterInfo ICollection_1_t9447_ICollection_1_Remove_m52600_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PInfo_t1997_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52600_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m52600_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9447_ICollection_1_Remove_m52600_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52600_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9447_MethodInfos[] =
{
	&ICollection_1_get_Count_m52594_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52595_MethodInfo,
	&ICollection_1_Add_m52596_MethodInfo,
	&ICollection_1_Clear_m52597_MethodInfo,
	&ICollection_1_Contains_m52598_MethodInfo,
	&ICollection_1_CopyTo_m52599_MethodInfo,
	&ICollection_1_Remove_m52600_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9449_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9447_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9449_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9447_0_0_0;
extern Il2CppType ICollection_1_t9447_1_0_0;
struct ICollection_1_t9447;
extern Il2CppGenericClass ICollection_1_t9447_GenericClass;
TypeInfo ICollection_1_t9447_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9447_MethodInfos/* methods */
	, ICollection_1_t9447_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9447_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9447_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9447_0_0_0/* byval_arg */
	, &ICollection_1_t9447_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9447_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.PInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.PInfo>
extern Il2CppType IEnumerator_1_t7358_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52601_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.PInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52601_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9449_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7358_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52601_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9449_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52601_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9449_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9449_0_0_0;
extern Il2CppType IEnumerable_1_t9449_1_0_0;
struct IEnumerable_1_t9449;
extern Il2CppGenericClass IEnumerable_1_t9449_GenericClass;
TypeInfo IEnumerable_1_t9449_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9449_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9449_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9449_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9449_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9449_0_0_0/* byval_arg */
	, &IEnumerable_1_t9449_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9449_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9448_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.PInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.PInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.PInfo>
extern MethodInfo IList_1_get_Item_m52602_MethodInfo;
extern MethodInfo IList_1_set_Item_m52603_MethodInfo;
static PropertyInfo IList_1_t9448____Item_PropertyInfo = 
{
	&IList_1_t9448_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52602_MethodInfo/* get */
	, &IList_1_set_Item_m52603_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9448_PropertyInfos[] =
{
	&IList_1_t9448____Item_PropertyInfo,
	NULL
};
extern Il2CppType PInfo_t1997_0_0_0;
static ParameterInfo IList_1_t9448_IList_1_IndexOf_m52604_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PInfo_t1997_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52604_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.PInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52604_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9448_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9448_IList_1_IndexOf_m52604_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52604_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PInfo_t1997_0_0_0;
static ParameterInfo IList_1_t9448_IList_1_Insert_m52605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PInfo_t1997_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52605_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52605_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9448_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9448_IList_1_Insert_m52605_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52605_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9448_IList_1_RemoveAt_m52606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52606_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52606_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9448_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9448_IList_1_RemoveAt_m52606_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52606_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9448_IList_1_get_Item_m52602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PInfo_t1997_0_0_0;
extern void* RuntimeInvoker_PInfo_t1997_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52602_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.PInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52602_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9448_il2cpp_TypeInfo/* declaring_type */
	, &PInfo_t1997_0_0_0/* return_type */
	, RuntimeInvoker_PInfo_t1997_Int32_t123/* invoker_method */
	, IList_1_t9448_IList_1_get_Item_m52602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52602_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PInfo_t1997_0_0_0;
static ParameterInfo IList_1_t9448_IList_1_set_Item_m52603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PInfo_t1997_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52603_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52603_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9448_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9448_IList_1_set_Item_m52603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52603_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9448_MethodInfos[] =
{
	&IList_1_IndexOf_m52604_MethodInfo,
	&IList_1_Insert_m52605_MethodInfo,
	&IList_1_RemoveAt_m52606_MethodInfo,
	&IList_1_get_Item_m52602_MethodInfo,
	&IList_1_set_Item_m52603_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9448_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9447_il2cpp_TypeInfo,
	&IEnumerable_1_t9449_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9448_0_0_0;
extern Il2CppType IList_1_t9448_1_0_0;
struct IList_1_t9448;
extern Il2CppGenericClass IList_1_t9448_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9448_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9448_MethodInfos/* methods */
	, IList_1_t9448_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9448_il2cpp_TypeInfo/* element_class */
	, IList_1_t9448_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9448_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9448_0_0_0/* byval_arg */
	, &IList_1_t9448_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9448_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Getter_2_t5240_il2cpp_TypeInfo;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_genMethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Getter_2__ctor_m31574_MethodInfo;
 void Getter_2__ctor_m31574_gshared (Getter_2_t5240 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern MethodInfo Getter_2_Invoke_m31575_MethodInfo;
 Object_t * Getter_2_Invoke_m31575_gshared (Getter_2_t5240 * __this, Object_t * ____this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Getter_2_Invoke_m31575((Getter_2_t5240 *)__this->___prev_9,____this, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ____this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____this,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ____this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____this,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(____this,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Getter_2_BeginInvoke_m31576_MethodInfo;
 Object_t * Getter_2_BeginInvoke_m31576_gshared (Getter_2_t5240 * __this, Object_t * ____this, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo Getter_2_EndInvoke_m31577_MethodInfo;
 Object_t * Getter_2_EndInvoke_m31577_gshared (Getter_2_t5240 * __this, Object_t * ___result, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// Metadata Definition System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Getter_2_t5240_Getter_2__ctor_m31574_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Getter_2__ctor_m31574_GenericMethod;
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo Getter_2__ctor_m31574_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Getter_2__ctor_m31574_gshared/* method */
	, &Getter_2_t5240_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Getter_2_t5240_Getter_2__ctor_m31574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Getter_2__ctor_m31574_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Getter_2_t5240_Getter_2_Invoke_m31575_ParameterInfos[] = 
{
	{"_this", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Getter_2_Invoke_m31575_GenericMethod;
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
MethodInfo Getter_2_Invoke_m31575_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Getter_2_Invoke_m31575_gshared/* method */
	, &Getter_2_t5240_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Getter_2_t5240_Getter_2_Invoke_m31575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Getter_2_Invoke_m31575_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Getter_2_t5240_Getter_2_BeginInvoke_m31576_ParameterInfos[] = 
{
	{"_this", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Getter_2_BeginInvoke_m31576_GenericMethod;
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Getter_2_BeginInvoke_m31576_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Getter_2_BeginInvoke_m31576_gshared/* method */
	, &Getter_2_t5240_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Getter_2_t5240_Getter_2_BeginInvoke_m31576_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Getter_2_BeginInvoke_m31576_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Getter_2_t5240_Getter_2_EndInvoke_m31577_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Getter_2_EndInvoke_m31577_GenericMethod;
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo Getter_2_EndInvoke_m31577_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Getter_2_EndInvoke_m31577_gshared/* method */
	, &Getter_2_t5240_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Getter_2_t5240_Getter_2_EndInvoke_m31577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Getter_2_EndInvoke_m31577_GenericMethod/* genericMethod */

};
static MethodInfo* Getter_2_t5240_MethodInfos[] =
{
	&Getter_2__ctor_m31574_MethodInfo,
	&Getter_2_Invoke_m31575_MethodInfo,
	&Getter_2_BeginInvoke_m31576_MethodInfo,
	&Getter_2_EndInvoke_m31577_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
static MethodInfo* Getter_2_t5240_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Getter_2_Invoke_m31575_MethodInfo,
	&Getter_2_BeginInvoke_m31576_MethodInfo,
	&Getter_2_EndInvoke_m31577_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair Getter_2_t5240_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Getter_2_t5240_0_0_0;
extern Il2CppType Getter_2_t5240_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct Getter_2_t5240;
extern Il2CppGenericClass Getter_2_t5240_GenericClass;
extern TypeInfo MonoProperty_t1996_il2cpp_TypeInfo;
TypeInfo Getter_2_t5240_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Getter`2"/* name */
	, ""/* namespaze */
	, Getter_2_t5240_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &MonoProperty_t1996_il2cpp_TypeInfo/* nested_in */
	, &Getter_2_t5240_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Getter_2_t5240_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Getter_2_t5240_il2cpp_TypeInfo/* cast_class */
	, &Getter_2_t5240_0_0_0/* byval_arg */
	, &Getter_2_t5240_1_0_0/* this_arg */
	, Getter_2_t5240_InterfacesOffsets/* interface_offsets */
	, &Getter_2_t5240_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Getter_2_t5240)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo StaticGetter_1_t5241_il2cpp_TypeInfo;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_genMethodDeclarations.h"



// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo StaticGetter_1__ctor_m31578_MethodInfo;
 void StaticGetter_1__ctor_m31578_gshared (StaticGetter_1_t5241 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern MethodInfo StaticGetter_1_Invoke_m31579_MethodInfo;
 Object_t * StaticGetter_1_Invoke_m31579_gshared (StaticGetter_1_t5241 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		StaticGetter_1_Invoke_m31579((StaticGetter_1_t5241 *)__this->___prev_9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	typedef Object_t * (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
	return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern MethodInfo StaticGetter_1_BeginInvoke_m31580_MethodInfo;
 Object_t * StaticGetter_1_BeginInvoke_m31580_gshared (StaticGetter_1_t5241 * __this, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo StaticGetter_1_EndInvoke_m31581_MethodInfo;
 Object_t * StaticGetter_1_EndInvoke_m31581_gshared (StaticGetter_1_t5241 * __this, Object_t * ___result, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// Metadata Definition System.Reflection.MonoProperty/StaticGetter`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo StaticGetter_1_t5241_StaticGetter_1__ctor_m31578_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod StaticGetter_1__ctor_m31578_GenericMethod;
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo StaticGetter_1__ctor_m31578_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StaticGetter_1__ctor_m31578_gshared/* method */
	, &StaticGetter_1_t5241_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, StaticGetter_1_t5241_StaticGetter_1__ctor_m31578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &StaticGetter_1__ctor_m31578_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod StaticGetter_1_Invoke_m31579_GenericMethod;
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
MethodInfo StaticGetter_1_Invoke_m31579_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&StaticGetter_1_Invoke_m31579_gshared/* method */
	, &StaticGetter_1_t5241_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &StaticGetter_1_Invoke_m31579_GenericMethod/* genericMethod */

};
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StaticGetter_1_t5241_StaticGetter_1_BeginInvoke_m31580_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod StaticGetter_1_BeginInvoke_m31580_GenericMethod;
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
MethodInfo StaticGetter_1_BeginInvoke_m31580_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&StaticGetter_1_BeginInvoke_m31580_gshared/* method */
	, &StaticGetter_1_t5241_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, StaticGetter_1_t5241_StaticGetter_1_BeginInvoke_m31580_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &StaticGetter_1_BeginInvoke_m31580_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo StaticGetter_1_t5241_StaticGetter_1_EndInvoke_m31581_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod StaticGetter_1_EndInvoke_m31581_GenericMethod;
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo StaticGetter_1_EndInvoke_m31581_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&StaticGetter_1_EndInvoke_m31581_gshared/* method */
	, &StaticGetter_1_t5241_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StaticGetter_1_t5241_StaticGetter_1_EndInvoke_m31581_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &StaticGetter_1_EndInvoke_m31581_GenericMethod/* genericMethod */

};
static MethodInfo* StaticGetter_1_t5241_MethodInfos[] =
{
	&StaticGetter_1__ctor_m31578_MethodInfo,
	&StaticGetter_1_Invoke_m31579_MethodInfo,
	&StaticGetter_1_BeginInvoke_m31580_MethodInfo,
	&StaticGetter_1_EndInvoke_m31581_MethodInfo,
	NULL
};
static MethodInfo* StaticGetter_1_t5241_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&StaticGetter_1_Invoke_m31579_MethodInfo,
	&StaticGetter_1_BeginInvoke_m31580_MethodInfo,
	&StaticGetter_1_EndInvoke_m31581_MethodInfo,
};
static Il2CppInterfaceOffsetPair StaticGetter_1_t5241_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType StaticGetter_1_t5241_0_0_0;
extern Il2CppType StaticGetter_1_t5241_1_0_0;
struct StaticGetter_1_t5241;
extern Il2CppGenericClass StaticGetter_1_t5241_GenericClass;
TypeInfo StaticGetter_1_t5241_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StaticGetter`1"/* name */
	, ""/* namespaze */
	, StaticGetter_1_t5241_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &MonoProperty_t1996_il2cpp_TypeInfo/* nested_in */
	, &StaticGetter_1_t5241_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, StaticGetter_1_t5241_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &StaticGetter_1_t5241_il2cpp_TypeInfo/* cast_class */
	, &StaticGetter_1_t5241_0_0_0/* byval_arg */
	, &StaticGetter_1_t5241_1_0_0/* this_arg */
	, StaticGetter_1_t5241_InterfacesOffsets/* interface_offsets */
	, &StaticGetter_1_t5241_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StaticGetter_1_t5241)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7360_il2cpp_TypeInfo;

// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52607_MethodInfo;
static PropertyInfo IEnumerator_1_t7360____Current_PropertyInfo = 
{
	&IEnumerator_1_t7360_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7360_PropertyInfos[] =
{
	&IEnumerator_1_t7360____Current_PropertyInfo,
	NULL
};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
extern void* RuntimeInvoker_ParameterAttributes_t2001 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52607_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52607_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7360_il2cpp_TypeInfo/* declaring_type */
	, &ParameterAttributes_t2001_0_0_0/* return_type */
	, RuntimeInvoker_ParameterAttributes_t2001/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52607_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7360_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52607_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7360_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7360_0_0_0;
extern Il2CppType IEnumerator_1_t7360_1_0_0;
struct IEnumerator_1_t7360;
extern Il2CppGenericClass IEnumerator_1_t7360_GenericClass;
TypeInfo IEnumerator_1_t7360_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7360_MethodInfos/* methods */
	, IEnumerator_1_t7360_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7360_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7360_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7360_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7360_0_0_0/* byval_arg */
	, &IEnumerator_1_t7360_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7360_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_696.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5242_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_696MethodDeclarations.h"

extern TypeInfo ParameterAttributes_t2001_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31586_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisParameterAttributes_t2001_m41378_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisParameterAttributes_t2001_m41378 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31582_MethodInfo;
 void InternalEnumerator_1__ctor_m31582 (InternalEnumerator_1_t5242 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583 (InternalEnumerator_1_t5242 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31586(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31586_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ParameterAttributes_t2001_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31584_MethodInfo;
 void InternalEnumerator_1_Dispose_m31584 (InternalEnumerator_1_t5242 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31585_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31585 (InternalEnumerator_1_t5242 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31586 (InternalEnumerator_1_t5242 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisParameterAttributes_t2001_m41378(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisParameterAttributes_t2001_m41378_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5242____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5242, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5242____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5242, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5242_FieldInfos[] =
{
	&InternalEnumerator_1_t5242____array_0_FieldInfo,
	&InternalEnumerator_1_t5242____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5242____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5242_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5242____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5242_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31586_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5242_PropertyInfos[] =
{
	&InternalEnumerator_1_t5242____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5242____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5242_InternalEnumerator_1__ctor_m31582_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31582_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31582_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31582/* method */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5242_InternalEnumerator_1__ctor_m31582_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31582_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583/* method */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31584_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31584_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31584/* method */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31584_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31585_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31585_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31585/* method */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31585_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
extern void* RuntimeInvoker_ParameterAttributes_t2001 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31586_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31586_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31586/* method */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* declaring_type */
	, &ParameterAttributes_t2001_0_0_0/* return_type */
	, RuntimeInvoker_ParameterAttributes_t2001/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31586_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5242_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31582_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_MethodInfo,
	&InternalEnumerator_1_Dispose_m31584_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31585_MethodInfo,
	&InternalEnumerator_1_get_Current_m31586_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5242_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31583_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31585_MethodInfo,
	&InternalEnumerator_1_Dispose_m31584_MethodInfo,
	&InternalEnumerator_1_get_Current_m31586_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5242_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7360_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5242_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7360_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5242_0_0_0;
extern Il2CppType InternalEnumerator_1_t5242_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5242_GenericClass;
TypeInfo InternalEnumerator_1_t5242_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5242_MethodInfos/* methods */
	, InternalEnumerator_1_t5242_PropertyInfos/* properties */
	, InternalEnumerator_1_t5242_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5242_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5242_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5242_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5242_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5242_1_0_0/* this_arg */
	, InternalEnumerator_1_t5242_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5242_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5242)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9450_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>
extern MethodInfo ICollection_1_get_Count_m52608_MethodInfo;
static PropertyInfo ICollection_1_t9450____Count_PropertyInfo = 
{
	&ICollection_1_t9450_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52608_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52609_MethodInfo;
static PropertyInfo ICollection_1_t9450____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9450_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52609_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9450_PropertyInfos[] =
{
	&ICollection_1_t9450____Count_PropertyInfo,
	&ICollection_1_t9450____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52608_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52608_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52608_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52609_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52609_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52609_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
extern Il2CppType ParameterAttributes_t2001_0_0_0;
static ParameterInfo ICollection_1_t9450_ICollection_1_Add_m52610_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterAttributes_t2001_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52610_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52610_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9450_ICollection_1_Add_m52610_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52610_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52611_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52611_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52611_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
static ParameterInfo ICollection_1_t9450_ICollection_1_Contains_m52612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterAttributes_t2001_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52612_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52612_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9450_ICollection_1_Contains_m52612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52612_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterAttributesU5BU5D_t5541_0_0_0;
extern Il2CppType ParameterAttributesU5BU5D_t5541_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9450_ICollection_1_CopyTo_m52613_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ParameterAttributesU5BU5D_t5541_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52613_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52613_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9450_ICollection_1_CopyTo_m52613_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52613_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
static ParameterInfo ICollection_1_t9450_ICollection_1_Remove_m52614_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterAttributes_t2001_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52614_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52614_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9450_ICollection_1_Remove_m52614_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52614_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9450_MethodInfos[] =
{
	&ICollection_1_get_Count_m52608_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52609_MethodInfo,
	&ICollection_1_Add_m52610_MethodInfo,
	&ICollection_1_Clear_m52611_MethodInfo,
	&ICollection_1_Contains_m52612_MethodInfo,
	&ICollection_1_CopyTo_m52613_MethodInfo,
	&ICollection_1_Remove_m52614_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9452_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9450_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9452_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9450_0_0_0;
extern Il2CppType ICollection_1_t9450_1_0_0;
struct ICollection_1_t9450;
extern Il2CppGenericClass ICollection_1_t9450_GenericClass;
TypeInfo ICollection_1_t9450_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9450_MethodInfos/* methods */
	, ICollection_1_t9450_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9450_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9450_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9450_0_0_0/* byval_arg */
	, &ICollection_1_t9450_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9450_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterAttributes>
extern Il2CppType IEnumerator_1_t7360_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52615_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52615_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9452_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7360_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52615_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9452_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52615_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9452_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9452_0_0_0;
extern Il2CppType IEnumerable_1_t9452_1_0_0;
struct IEnumerable_1_t9452;
extern Il2CppGenericClass IEnumerable_1_t9452_GenericClass;
TypeInfo IEnumerable_1_t9452_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9452_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9452_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9452_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9452_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9452_0_0_0/* byval_arg */
	, &IEnumerable_1_t9452_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9452_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9451_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>
extern MethodInfo IList_1_get_Item_m52616_MethodInfo;
extern MethodInfo IList_1_set_Item_m52617_MethodInfo;
static PropertyInfo IList_1_t9451____Item_PropertyInfo = 
{
	&IList_1_t9451_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52616_MethodInfo/* get */
	, &IList_1_set_Item_m52617_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9451_PropertyInfos[] =
{
	&IList_1_t9451____Item_PropertyInfo,
	NULL
};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
static ParameterInfo IList_1_t9451_IList_1_IndexOf_m52618_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterAttributes_t2001_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52618_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52618_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9451_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9451_IList_1_IndexOf_m52618_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52618_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ParameterAttributes_t2001_0_0_0;
static ParameterInfo IList_1_t9451_IList_1_Insert_m52619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ParameterAttributes_t2001_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52619_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52619_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9451_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9451_IList_1_Insert_m52619_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52619_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9451_IList_1_RemoveAt_m52620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52620_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52620_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9451_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9451_IList_1_RemoveAt_m52620_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52620_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9451_IList_1_get_Item_m52616_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ParameterAttributes_t2001_0_0_0;
extern void* RuntimeInvoker_ParameterAttributes_t2001_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52616_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52616_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9451_il2cpp_TypeInfo/* declaring_type */
	, &ParameterAttributes_t2001_0_0_0/* return_type */
	, RuntimeInvoker_ParameterAttributes_t2001_Int32_t123/* invoker_method */
	, IList_1_t9451_IList_1_get_Item_m52616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52616_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ParameterAttributes_t2001_0_0_0;
static ParameterInfo IList_1_t9451_IList_1_set_Item_m52617_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ParameterAttributes_t2001_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52617_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52617_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9451_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9451_IList_1_set_Item_m52617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52617_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9451_MethodInfos[] =
{
	&IList_1_IndexOf_m52618_MethodInfo,
	&IList_1_Insert_m52619_MethodInfo,
	&IList_1_RemoveAt_m52620_MethodInfo,
	&IList_1_get_Item_m52616_MethodInfo,
	&IList_1_set_Item_m52617_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9451_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9450_il2cpp_TypeInfo,
	&IEnumerable_1_t9452_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9451_0_0_0;
extern Il2CppType IList_1_t9451_1_0_0;
struct IList_1_t9451;
extern Il2CppGenericClass IList_1_t9451_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9451_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9451_MethodInfos/* methods */
	, IList_1_t9451_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9451_il2cpp_TypeInfo/* element_class */
	, IList_1_t9451_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9451_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9451_0_0_0/* byval_arg */
	, &IList_1_t9451_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9451_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7362_il2cpp_TypeInfo;

// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.ProcessorArchitecture>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ProcessorArchitecture>
extern MethodInfo IEnumerator_1_get_Current_m52621_MethodInfo;
static PropertyInfo IEnumerator_1_t7362____Current_PropertyInfo = 
{
	&IEnumerator_1_t7362_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7362_PropertyInfos[] =
{
	&IEnumerator_1_t7362____Current_PropertyInfo,
	NULL
};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
extern void* RuntimeInvoker_ProcessorArchitecture_t2003 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52621_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.ProcessorArchitecture>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52621_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &ProcessorArchitecture_t2003_0_0_0/* return_type */
	, RuntimeInvoker_ProcessorArchitecture_t2003/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52621_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7362_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52621_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7362_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7362_0_0_0;
extern Il2CppType IEnumerator_1_t7362_1_0_0;
struct IEnumerator_1_t7362;
extern Il2CppGenericClass IEnumerator_1_t7362_GenericClass;
TypeInfo IEnumerator_1_t7362_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7362_MethodInfos/* methods */
	, IEnumerator_1_t7362_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7362_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7362_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7362_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7362_0_0_0/* byval_arg */
	, &IEnumerator_1_t7362_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7362_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_697.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5243_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_697MethodDeclarations.h"

extern TypeInfo ProcessorArchitecture_t2003_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31591_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisProcessorArchitecture_t2003_m41389_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.ProcessorArchitecture>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ProcessorArchitecture>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisProcessorArchitecture_t2003_m41389 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31587_MethodInfo;
 void InternalEnumerator_1__ctor_m31587 (InternalEnumerator_1_t5243 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588 (InternalEnumerator_1_t5243 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31591(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31591_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ProcessorArchitecture_t2003_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31589_MethodInfo;
 void InternalEnumerator_1_Dispose_m31589 (InternalEnumerator_1_t5243 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31590_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31590 (InternalEnumerator_1_t5243 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31591 (InternalEnumerator_1_t5243 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisProcessorArchitecture_t2003_m41389(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisProcessorArchitecture_t2003_m41389_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5243____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5243, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5243____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5243, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5243_FieldInfos[] =
{
	&InternalEnumerator_1_t5243____array_0_FieldInfo,
	&InternalEnumerator_1_t5243____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5243____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5243_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5243____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5243_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31591_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5243_PropertyInfos[] =
{
	&InternalEnumerator_1_t5243____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5243____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5243_InternalEnumerator_1__ctor_m31587_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31587_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31587_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31587/* method */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5243_InternalEnumerator_1__ctor_m31587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31587_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588/* method */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31589_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31589_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31589/* method */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31589_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31590_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31590_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31590/* method */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31590_GenericMethod/* genericMethod */

};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
extern void* RuntimeInvoker_ProcessorArchitecture_t2003 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31591_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31591_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31591/* method */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* declaring_type */
	, &ProcessorArchitecture_t2003_0_0_0/* return_type */
	, RuntimeInvoker_ProcessorArchitecture_t2003/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31591_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5243_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31587_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_MethodInfo,
	&InternalEnumerator_1_Dispose_m31589_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31590_MethodInfo,
	&InternalEnumerator_1_get_Current_m31591_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5243_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31588_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31590_MethodInfo,
	&InternalEnumerator_1_Dispose_m31589_MethodInfo,
	&InternalEnumerator_1_get_Current_m31591_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5243_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7362_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5243_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7362_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5243_0_0_0;
extern Il2CppType InternalEnumerator_1_t5243_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5243_GenericClass;
TypeInfo InternalEnumerator_1_t5243_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5243_MethodInfos/* methods */
	, InternalEnumerator_1_t5243_PropertyInfos/* properties */
	, InternalEnumerator_1_t5243_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5243_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5243_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5243_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5243_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5243_1_0_0/* this_arg */
	, InternalEnumerator_1_t5243_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5243_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5243)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9453_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>
extern MethodInfo ICollection_1_get_Count_m52622_MethodInfo;
static PropertyInfo ICollection_1_t9453____Count_PropertyInfo = 
{
	&ICollection_1_t9453_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52622_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52623_MethodInfo;
static PropertyInfo ICollection_1_t9453____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9453_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52623_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9453_PropertyInfos[] =
{
	&ICollection_1_t9453____Count_PropertyInfo,
	&ICollection_1_t9453____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52622_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::get_Count()
MethodInfo ICollection_1_get_Count_m52622_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52622_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52623_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52623_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52623_GenericMethod/* genericMethod */

};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
static ParameterInfo ICollection_1_t9453_ICollection_1_Add_m52624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitecture_t2003_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52624_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Add(T)
MethodInfo ICollection_1_Add_m52624_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9453_ICollection_1_Add_m52624_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52624_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52625_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Clear()
MethodInfo ICollection_1_Clear_m52625_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52625_GenericMethod/* genericMethod */

};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
static ParameterInfo ICollection_1_t9453_ICollection_1_Contains_m52626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitecture_t2003_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52626_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Contains(T)
MethodInfo ICollection_1_Contains_m52626_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9453_ICollection_1_Contains_m52626_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52626_GenericMethod/* genericMethod */

};
extern Il2CppType ProcessorArchitectureU5BU5D_t5542_0_0_0;
extern Il2CppType ProcessorArchitectureU5BU5D_t5542_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9453_ICollection_1_CopyTo_m52627_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitectureU5BU5D_t5542_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52627_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52627_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9453_ICollection_1_CopyTo_m52627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52627_GenericMethod/* genericMethod */

};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
static ParameterInfo ICollection_1_t9453_ICollection_1_Remove_m52628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitecture_t2003_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52628_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>::Remove(T)
MethodInfo ICollection_1_Remove_m52628_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9453_ICollection_1_Remove_m52628_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52628_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9453_MethodInfos[] =
{
	&ICollection_1_get_Count_m52622_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52623_MethodInfo,
	&ICollection_1_Add_m52624_MethodInfo,
	&ICollection_1_Clear_m52625_MethodInfo,
	&ICollection_1_Contains_m52626_MethodInfo,
	&ICollection_1_CopyTo_m52627_MethodInfo,
	&ICollection_1_Remove_m52628_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9455_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9453_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9455_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9453_0_0_0;
extern Il2CppType ICollection_1_t9453_1_0_0;
struct ICollection_1_t9453;
extern Il2CppGenericClass ICollection_1_t9453_GenericClass;
TypeInfo ICollection_1_t9453_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9453_MethodInfos/* methods */
	, ICollection_1_t9453_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9453_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9453_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9453_0_0_0/* byval_arg */
	, &ICollection_1_t9453_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9453_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.ProcessorArchitecture>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ProcessorArchitecture>
extern Il2CppType IEnumerator_1_t7362_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52629_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.ProcessorArchitecture>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52629_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9455_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7362_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52629_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9455_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52629_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9455_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9455_0_0_0;
extern Il2CppType IEnumerable_1_t9455_1_0_0;
struct IEnumerable_1_t9455;
extern Il2CppGenericClass IEnumerable_1_t9455_GenericClass;
TypeInfo IEnumerable_1_t9455_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9455_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9455_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9455_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9455_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9455_0_0_0/* byval_arg */
	, &IEnumerable_1_t9455_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9455_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9454_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>
extern MethodInfo IList_1_get_Item_m52630_MethodInfo;
extern MethodInfo IList_1_set_Item_m52631_MethodInfo;
static PropertyInfo IList_1_t9454____Item_PropertyInfo = 
{
	&IList_1_t9454_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52630_MethodInfo/* get */
	, &IList_1_set_Item_m52631_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9454_PropertyInfos[] =
{
	&IList_1_t9454____Item_PropertyInfo,
	NULL
};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
static ParameterInfo IList_1_t9454_IList_1_IndexOf_m52632_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitecture_t2003_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52632_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52632_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9454_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9454_IList_1_IndexOf_m52632_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52632_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
static ParameterInfo IList_1_t9454_IList_1_Insert_m52633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitecture_t2003_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52633_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52633_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9454_IList_1_Insert_m52633_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52633_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9454_IList_1_RemoveAt_m52634_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52634_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52634_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9454_IList_1_RemoveAt_m52634_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52634_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9454_IList_1_get_Item_m52630_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
extern void* RuntimeInvoker_ProcessorArchitecture_t2003_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52630_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52630_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9454_il2cpp_TypeInfo/* declaring_type */
	, &ProcessorArchitecture_t2003_0_0_0/* return_type */
	, RuntimeInvoker_ProcessorArchitecture_t2003_Int32_t123/* invoker_method */
	, IList_1_t9454_IList_1_get_Item_m52630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52630_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ProcessorArchitecture_t2003_0_0_0;
static ParameterInfo IList_1_t9454_IList_1_set_Item_m52631_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ProcessorArchitecture_t2003_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52631_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52631_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9454_IList_1_set_Item_m52631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52631_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9454_MethodInfos[] =
{
	&IList_1_IndexOf_m52632_MethodInfo,
	&IList_1_Insert_m52633_MethodInfo,
	&IList_1_RemoveAt_m52634_MethodInfo,
	&IList_1_get_Item_m52630_MethodInfo,
	&IList_1_set_Item_m52631_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9454_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9453_il2cpp_TypeInfo,
	&IEnumerable_1_t9455_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9454_0_0_0;
extern Il2CppType IList_1_t9454_1_0_0;
struct IList_1_t9454;
extern Il2CppGenericClass IList_1_t9454_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9454_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9454_MethodInfos/* methods */
	, IList_1_t9454_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9454_il2cpp_TypeInfo/* element_class */
	, IList_1_t9454_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9454_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9454_0_0_0/* byval_arg */
	, &IList_1_t9454_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9454_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7364_il2cpp_TypeInfo;

// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52635_MethodInfo;
static PropertyInfo IEnumerator_1_t7364____Current_PropertyInfo = 
{
	&IEnumerator_1_t7364_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52635_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7364_PropertyInfos[] =
{
	&IEnumerator_1_t7364____Current_PropertyInfo,
	NULL
};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
extern void* RuntimeInvoker_PropertyAttributes_t2004 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52635_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52635_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7364_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t2004_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t2004/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52635_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7364_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52635_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7364_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7364_0_0_0;
extern Il2CppType IEnumerator_1_t7364_1_0_0;
struct IEnumerator_1_t7364;
extern Il2CppGenericClass IEnumerator_1_t7364_GenericClass;
TypeInfo IEnumerator_1_t7364_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7364_MethodInfos/* methods */
	, IEnumerator_1_t7364_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7364_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7364_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7364_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7364_0_0_0/* byval_arg */
	, &IEnumerator_1_t7364_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7364_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_698.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5244_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_698MethodDeclarations.h"

extern TypeInfo PropertyAttributes_t2004_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31596_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPropertyAttributes_t2004_m41400_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.PropertyAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.PropertyAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPropertyAttributes_t2004_m41400 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31592_MethodInfo;
 void InternalEnumerator_1__ctor_m31592 (InternalEnumerator_1_t5244 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593 (InternalEnumerator_1_t5244 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31596(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31596_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&PropertyAttributes_t2004_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31594_MethodInfo;
 void InternalEnumerator_1_Dispose_m31594 (InternalEnumerator_1_t5244 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31595_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31595 (InternalEnumerator_1_t5244 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31596 (InternalEnumerator_1_t5244 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisPropertyAttributes_t2004_m41400(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisPropertyAttributes_t2004_m41400_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5244____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5244, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5244____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5244, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5244_FieldInfos[] =
{
	&InternalEnumerator_1_t5244____array_0_FieldInfo,
	&InternalEnumerator_1_t5244____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5244____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5244_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5244____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5244_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31596_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5244_PropertyInfos[] =
{
	&InternalEnumerator_1_t5244____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5244____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5244_InternalEnumerator_1__ctor_m31592_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31592_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31592_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31592/* method */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5244_InternalEnumerator_1__ctor_m31592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31592_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593/* method */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31594_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31594_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31594/* method */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31594_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31595_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31595_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31595/* method */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31595_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
extern void* RuntimeInvoker_PropertyAttributes_t2004 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31596_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31596_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31596/* method */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t2004_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t2004/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31596_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5244_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31592_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_MethodInfo,
	&InternalEnumerator_1_Dispose_m31594_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31595_MethodInfo,
	&InternalEnumerator_1_get_Current_m31596_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5244_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31593_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31595_MethodInfo,
	&InternalEnumerator_1_Dispose_m31594_MethodInfo,
	&InternalEnumerator_1_get_Current_m31596_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5244_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7364_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5244_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7364_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5244_0_0_0;
extern Il2CppType InternalEnumerator_1_t5244_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5244_GenericClass;
TypeInfo InternalEnumerator_1_t5244_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5244_MethodInfos/* methods */
	, InternalEnumerator_1_t5244_PropertyInfos/* properties */
	, InternalEnumerator_1_t5244_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5244_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5244_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5244_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5244_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5244_1_0_0/* this_arg */
	, InternalEnumerator_1_t5244_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5244_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5244)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9456_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>
extern MethodInfo ICollection_1_get_Count_m52636_MethodInfo;
static PropertyInfo ICollection_1_t9456____Count_PropertyInfo = 
{
	&ICollection_1_t9456_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52637_MethodInfo;
static PropertyInfo ICollection_1_t9456____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9456_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52637_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9456_PropertyInfos[] =
{
	&ICollection_1_t9456____Count_PropertyInfo,
	&ICollection_1_t9456____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52636_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52636_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52636_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52637_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52637_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52637_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
extern Il2CppType PropertyAttributes_t2004_0_0_0;
static ParameterInfo ICollection_1_t9456_ICollection_1_Add_m52638_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttributes_t2004_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52638_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52638_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9456_ICollection_1_Add_m52638_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52638_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52639_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52639_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52639_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
static ParameterInfo ICollection_1_t9456_ICollection_1_Contains_m52640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttributes_t2004_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52640_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52640_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9456_ICollection_1_Contains_m52640_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52640_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttributesU5BU5D_t5543_0_0_0;
extern Il2CppType PropertyAttributesU5BU5D_t5543_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9456_ICollection_1_CopyTo_m52641_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttributesU5BU5D_t5543_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52641_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52641_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9456_ICollection_1_CopyTo_m52641_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52641_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
static ParameterInfo ICollection_1_t9456_ICollection_1_Remove_m52642_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttributes_t2004_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52642_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52642_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9456_ICollection_1_Remove_m52642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52642_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9456_MethodInfos[] =
{
	&ICollection_1_get_Count_m52636_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52637_MethodInfo,
	&ICollection_1_Add_m52638_MethodInfo,
	&ICollection_1_Clear_m52639_MethodInfo,
	&ICollection_1_Contains_m52640_MethodInfo,
	&ICollection_1_CopyTo_m52641_MethodInfo,
	&ICollection_1_Remove_m52642_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9458_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9456_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9458_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9456_0_0_0;
extern Il2CppType ICollection_1_t9456_1_0_0;
struct ICollection_1_t9456;
extern Il2CppGenericClass ICollection_1_t9456_GenericClass;
TypeInfo ICollection_1_t9456_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9456_MethodInfos/* methods */
	, ICollection_1_t9456_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9456_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9456_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9456_0_0_0/* byval_arg */
	, &ICollection_1_t9456_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9456_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyAttributes>
extern Il2CppType IEnumerator_1_t7364_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52643_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52643_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9458_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7364_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52643_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9458_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52643_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9458_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9458_0_0_0;
extern Il2CppType IEnumerable_1_t9458_1_0_0;
struct IEnumerable_1_t9458;
extern Il2CppGenericClass IEnumerable_1_t9458_GenericClass;
TypeInfo IEnumerable_1_t9458_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9458_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9458_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9458_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9458_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9458_0_0_0/* byval_arg */
	, &IEnumerable_1_t9458_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9458_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9457_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>
extern MethodInfo IList_1_get_Item_m52644_MethodInfo;
extern MethodInfo IList_1_set_Item_m52645_MethodInfo;
static PropertyInfo IList_1_t9457____Item_PropertyInfo = 
{
	&IList_1_t9457_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52644_MethodInfo/* get */
	, &IList_1_set_Item_m52645_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9457_PropertyInfos[] =
{
	&IList_1_t9457____Item_PropertyInfo,
	NULL
};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
static ParameterInfo IList_1_t9457_IList_1_IndexOf_m52646_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttributes_t2004_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52646_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52646_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9457_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9457_IList_1_IndexOf_m52646_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52646_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropertyAttributes_t2004_0_0_0;
static ParameterInfo IList_1_t9457_IList_1_Insert_m52647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PropertyAttributes_t2004_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52647_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52647_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9457_IList_1_Insert_m52647_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52647_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9457_IList_1_RemoveAt_m52648_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52648_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52648_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9457_IList_1_RemoveAt_m52648_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52648_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9457_IList_1_get_Item_m52644_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PropertyAttributes_t2004_0_0_0;
extern void* RuntimeInvoker_PropertyAttributes_t2004_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52644_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52644_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9457_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t2004_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t2004_Int32_t123/* invoker_method */
	, IList_1_t9457_IList_1_get_Item_m52644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52644_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropertyAttributes_t2004_0_0_0;
static ParameterInfo IList_1_t9457_IList_1_set_Item_m52645_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PropertyAttributes_t2004_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52645_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52645_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9457_IList_1_set_Item_m52645_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52645_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9457_MethodInfos[] =
{
	&IList_1_IndexOf_m52646_MethodInfo,
	&IList_1_Insert_m52647_MethodInfo,
	&IList_1_RemoveAt_m52648_MethodInfo,
	&IList_1_get_Item_m52644_MethodInfo,
	&IList_1_set_Item_m52645_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9457_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9456_il2cpp_TypeInfo,
	&IEnumerable_1_t9458_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9457_0_0_0;
extern Il2CppType IList_1_t9457_1_0_0;
struct IList_1_t9457;
extern Il2CppGenericClass IList_1_t9457_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9457_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9457_MethodInfos/* methods */
	, IList_1_t9457_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9457_il2cpp_TypeInfo/* element_class */
	, IList_1_t9457_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9457_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9457_0_0_0/* byval_arg */
	, &IList_1_t9457_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9457_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7366_il2cpp_TypeInfo;

// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.TypeAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.TypeAttributes>
extern MethodInfo IEnumerator_1_get_Current_m52649_MethodInfo;
static PropertyInfo IEnumerator_1_t7366____Current_PropertyInfo = 
{
	&IEnumerator_1_t7366_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7366_PropertyInfos[] =
{
	&IEnumerator_1_t7366____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeAttributes_t2008_0_0_0;
extern void* RuntimeInvoker_TypeAttributes_t2008 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52649_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.TypeAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52649_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7366_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2008_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2008/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52649_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7366_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52649_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7366_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7366_0_0_0;
extern Il2CppType IEnumerator_1_t7366_1_0_0;
struct IEnumerator_1_t7366;
extern Il2CppGenericClass IEnumerator_1_t7366_GenericClass;
TypeInfo IEnumerator_1_t7366_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7366_MethodInfos/* methods */
	, IEnumerator_1_t7366_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7366_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7366_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7366_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7366_0_0_0/* byval_arg */
	, &IEnumerator_1_t7366_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7366_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_699.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5245_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_699MethodDeclarations.h"

extern TypeInfo TypeAttributes_t2008_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31601_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeAttributes_t2008_m41411_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.TypeAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.TypeAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypeAttributes_t2008_m41411 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31597_MethodInfo;
 void InternalEnumerator_1__ctor_m31597 (InternalEnumerator_1_t5245 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598 (InternalEnumerator_1_t5245 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31601(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31601_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TypeAttributes_t2008_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31599_MethodInfo;
 void InternalEnumerator_1_Dispose_m31599 (InternalEnumerator_1_t5245 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31600_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31600 (InternalEnumerator_1_t5245 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31601 (InternalEnumerator_1_t5245 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTypeAttributes_t2008_m41411(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTypeAttributes_t2008_m41411_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5245____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5245, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5245____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5245, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5245_FieldInfos[] =
{
	&InternalEnumerator_1_t5245____array_0_FieldInfo,
	&InternalEnumerator_1_t5245____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5245____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5245_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5245____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5245_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31601_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5245_PropertyInfos[] =
{
	&InternalEnumerator_1_t5245____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5245____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5245_InternalEnumerator_1__ctor_m31597_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31597_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31597_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31597/* method */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5245_InternalEnumerator_1__ctor_m31597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31597_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598/* method */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31599_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31599_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31599/* method */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31599_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31600_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31600_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31600/* method */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31600_GenericMethod/* genericMethod */

};
extern Il2CppType TypeAttributes_t2008_0_0_0;
extern void* RuntimeInvoker_TypeAttributes_t2008 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31601_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31601_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31601/* method */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2008_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2008/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31601_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5245_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31597_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_MethodInfo,
	&InternalEnumerator_1_Dispose_m31599_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31600_MethodInfo,
	&InternalEnumerator_1_get_Current_m31601_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5245_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31598_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31600_MethodInfo,
	&InternalEnumerator_1_Dispose_m31599_MethodInfo,
	&InternalEnumerator_1_get_Current_m31601_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5245_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7366_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5245_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7366_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5245_0_0_0;
extern Il2CppType InternalEnumerator_1_t5245_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5245_GenericClass;
TypeInfo InternalEnumerator_1_t5245_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5245_MethodInfos/* methods */
	, InternalEnumerator_1_t5245_PropertyInfos/* properties */
	, InternalEnumerator_1_t5245_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5245_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5245_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5245_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5245_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5245_1_0_0/* this_arg */
	, InternalEnumerator_1_t5245_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5245_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5245)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9459_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>
extern MethodInfo ICollection_1_get_Count_m52650_MethodInfo;
static PropertyInfo ICollection_1_t9459____Count_PropertyInfo = 
{
	&ICollection_1_t9459_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52650_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52651_MethodInfo;
static PropertyInfo ICollection_1_t9459____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9459_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52651_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9459_PropertyInfos[] =
{
	&ICollection_1_t9459____Count_PropertyInfo,
	&ICollection_1_t9459____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52650_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m52650_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52650_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52651_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52651_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52651_GenericMethod/* genericMethod */

};
extern Il2CppType TypeAttributes_t2008_0_0_0;
extern Il2CppType TypeAttributes_t2008_0_0_0;
static ParameterInfo ICollection_1_t9459_ICollection_1_Add_m52652_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeAttributes_t2008_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52652_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Add(T)
MethodInfo ICollection_1_Add_m52652_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9459_ICollection_1_Add_m52652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52652_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52653_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Clear()
MethodInfo ICollection_1_Clear_m52653_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52653_GenericMethod/* genericMethod */

};
extern Il2CppType TypeAttributes_t2008_0_0_0;
static ParameterInfo ICollection_1_t9459_ICollection_1_Contains_m52654_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeAttributes_t2008_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52654_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m52654_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9459_ICollection_1_Contains_m52654_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52654_GenericMethod/* genericMethod */

};
extern Il2CppType TypeAttributesU5BU5D_t5544_0_0_0;
extern Il2CppType TypeAttributesU5BU5D_t5544_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9459_ICollection_1_CopyTo_m52655_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeAttributesU5BU5D_t5544_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52655_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52655_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9459_ICollection_1_CopyTo_m52655_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52655_GenericMethod/* genericMethod */

};
extern Il2CppType TypeAttributes_t2008_0_0_0;
static ParameterInfo ICollection_1_t9459_ICollection_1_Remove_m52656_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeAttributes_t2008_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52656_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m52656_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9459_ICollection_1_Remove_m52656_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52656_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9459_MethodInfos[] =
{
	&ICollection_1_get_Count_m52650_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52651_MethodInfo,
	&ICollection_1_Add_m52652_MethodInfo,
	&ICollection_1_Clear_m52653_MethodInfo,
	&ICollection_1_Contains_m52654_MethodInfo,
	&ICollection_1_CopyTo_m52655_MethodInfo,
	&ICollection_1_Remove_m52656_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9461_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9459_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9461_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9459_0_0_0;
extern Il2CppType ICollection_1_t9459_1_0_0;
struct ICollection_1_t9459;
extern Il2CppGenericClass ICollection_1_t9459_GenericClass;
TypeInfo ICollection_1_t9459_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9459_MethodInfos/* methods */
	, ICollection_1_t9459_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9459_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9459_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9459_0_0_0/* byval_arg */
	, &ICollection_1_t9459_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9459_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.TypeAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.TypeAttributes>
extern Il2CppType IEnumerator_1_t7366_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52657_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.TypeAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52657_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9461_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7366_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52657_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9461_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52657_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9461_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9461_0_0_0;
extern Il2CppType IEnumerable_1_t9461_1_0_0;
struct IEnumerable_1_t9461;
extern Il2CppGenericClass IEnumerable_1_t9461_GenericClass;
TypeInfo IEnumerable_1_t9461_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9461_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9461_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9461_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9461_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9461_0_0_0/* byval_arg */
	, &IEnumerable_1_t9461_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9461_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9460_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>
extern MethodInfo IList_1_get_Item_m52658_MethodInfo;
extern MethodInfo IList_1_set_Item_m52659_MethodInfo;
static PropertyInfo IList_1_t9460____Item_PropertyInfo = 
{
	&IList_1_t9460_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52658_MethodInfo/* get */
	, &IList_1_set_Item_m52659_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9460_PropertyInfos[] =
{
	&IList_1_t9460____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeAttributes_t2008_0_0_0;
static ParameterInfo IList_1_t9460_IList_1_IndexOf_m52660_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeAttributes_t2008_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52660_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52660_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9460_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9460_IList_1_IndexOf_m52660_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52660_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeAttributes_t2008_0_0_0;
static ParameterInfo IList_1_t9460_IList_1_Insert_m52661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeAttributes_t2008_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52661_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52661_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9460_IList_1_Insert_m52661_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52661_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9460_IList_1_RemoveAt_m52662_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52662_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52662_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9460_IList_1_RemoveAt_m52662_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52662_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9460_IList_1_get_Item_m52658_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeAttributes_t2008_0_0_0;
extern void* RuntimeInvoker_TypeAttributes_t2008_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52658_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52658_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9460_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2008_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2008_Int32_t123/* invoker_method */
	, IList_1_t9460_IList_1_get_Item_m52658_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52658_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeAttributes_t2008_0_0_0;
static ParameterInfo IList_1_t9460_IList_1_set_Item_m52659_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeAttributes_t2008_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52659_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52659_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9460_IList_1_set_Item_m52659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52659_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9460_MethodInfos[] =
{
	&IList_1_IndexOf_m52660_MethodInfo,
	&IList_1_Insert_m52661_MethodInfo,
	&IList_1_RemoveAt_m52662_MethodInfo,
	&IList_1_get_Item_m52658_MethodInfo,
	&IList_1_set_Item_m52659_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9460_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9459_il2cpp_TypeInfo,
	&IEnumerable_1_t9461_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9460_0_0_0;
extern Il2CppType IList_1_t9460_1_0_0;
struct IList_1_t9460;
extern Il2CppGenericClass IList_1_t9460_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9460_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9460_MethodInfos/* methods */
	, IList_1_t9460_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9460_il2cpp_TypeInfo/* element_class */
	, IList_1_t9460_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9460_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9460_0_0_0/* byval_arg */
	, &IList_1_t9460_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9460_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7368_il2cpp_TypeInfo;

// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52663_MethodInfo;
static PropertyInfo IEnumerator_1_t7368____Current_PropertyInfo = 
{
	&IEnumerator_1_t7368_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7368_PropertyInfos[] =
{
	&IEnumerator_1_t7368____Current_PropertyInfo,
	NULL
};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52663_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52663_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &NeutralResourcesLanguageAttribute_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52663_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7368_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52663_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7368_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7368_0_0_0;
extern Il2CppType IEnumerator_1_t7368_1_0_0;
struct IEnumerator_1_t7368;
extern Il2CppGenericClass IEnumerator_1_t7368_GenericClass;
TypeInfo IEnumerator_1_t7368_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7368_MethodInfos/* methods */
	, IEnumerator_1_t7368_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7368_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7368_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7368_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7368_0_0_0/* byval_arg */
	, &IEnumerator_1_t7368_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7368_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_700.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5246_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_700MethodDeclarations.h"

extern TypeInfo NeutralResourcesLanguageAttribute_t1339_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31606_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisNeutralResourcesLanguageAttribute_t1339_m41422_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Resources.NeutralResourcesLanguageAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Resources.NeutralResourcesLanguageAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisNeutralResourcesLanguageAttribute_t1339_m41422(__this, p0, method) (NeutralResourcesLanguageAttribute_t1339 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5246____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5246, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5246____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5246, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5246_FieldInfos[] =
{
	&InternalEnumerator_1_t5246____array_0_FieldInfo,
	&InternalEnumerator_1_t5246____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5246____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5246_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5246____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5246_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5246_PropertyInfos[] =
{
	&InternalEnumerator_1_t5246____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5246____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5246_InternalEnumerator_1__ctor_m31602_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31602_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31602_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5246_InternalEnumerator_1__ctor_m31602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31602_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31604_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31604_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31604_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31605_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31605_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31605_GenericMethod/* genericMethod */

};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31606_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31606_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* declaring_type */
	, &NeutralResourcesLanguageAttribute_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31606_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5246_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31602_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_MethodInfo,
	&InternalEnumerator_1_Dispose_m31604_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31605_MethodInfo,
	&InternalEnumerator_1_get_Current_m31606_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31605_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31604_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5246_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31603_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31605_MethodInfo,
	&InternalEnumerator_1_Dispose_m31604_MethodInfo,
	&InternalEnumerator_1_get_Current_m31606_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5246_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7368_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5246_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7368_il2cpp_TypeInfo, 7},
};
extern TypeInfo NeutralResourcesLanguageAttribute_t1339_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5246_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31606_MethodInfo/* Method Usage */,
	&NeutralResourcesLanguageAttribute_t1339_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisNeutralResourcesLanguageAttribute_t1339_m41422_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5246_0_0_0;
extern Il2CppType InternalEnumerator_1_t5246_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5246_GenericClass;
TypeInfo InternalEnumerator_1_t5246_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5246_MethodInfos/* methods */
	, InternalEnumerator_1_t5246_PropertyInfos/* properties */
	, InternalEnumerator_1_t5246_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5246_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5246_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5246_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5246_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5246_1_0_0/* this_arg */
	, InternalEnumerator_1_t5246_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5246_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5246_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5246)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9462_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>
extern MethodInfo ICollection_1_get_Count_m52664_MethodInfo;
static PropertyInfo ICollection_1_t9462____Count_PropertyInfo = 
{
	&ICollection_1_t9462_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52664_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52665_MethodInfo;
static PropertyInfo ICollection_1_t9462____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9462_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52665_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9462_PropertyInfos[] =
{
	&ICollection_1_t9462____Count_PropertyInfo,
	&ICollection_1_t9462____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52664_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52664_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52664_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52665_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52665_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52665_GenericMethod/* genericMethod */

};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
static ParameterInfo ICollection_1_t9462_ICollection_1_Add_m52666_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttribute_t1339_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52666_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52666_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9462_ICollection_1_Add_m52666_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52666_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52667_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52667_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52667_GenericMethod/* genericMethod */

};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
static ParameterInfo ICollection_1_t9462_ICollection_1_Contains_m52668_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttribute_t1339_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52668_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52668_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9462_ICollection_1_Contains_m52668_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52668_GenericMethod/* genericMethod */

};
extern Il2CppType NeutralResourcesLanguageAttributeU5BU5D_t5545_0_0_0;
extern Il2CppType NeutralResourcesLanguageAttributeU5BU5D_t5545_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9462_ICollection_1_CopyTo_m52669_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttributeU5BU5D_t5545_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52669_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52669_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9462_ICollection_1_CopyTo_m52669_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52669_GenericMethod/* genericMethod */

};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
static ParameterInfo ICollection_1_t9462_ICollection_1_Remove_m52670_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttribute_t1339_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52670_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52670_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9462_ICollection_1_Remove_m52670_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52670_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9462_MethodInfos[] =
{
	&ICollection_1_get_Count_m52664_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52665_MethodInfo,
	&ICollection_1_Add_m52666_MethodInfo,
	&ICollection_1_Clear_m52667_MethodInfo,
	&ICollection_1_Contains_m52668_MethodInfo,
	&ICollection_1_CopyTo_m52669_MethodInfo,
	&ICollection_1_Remove_m52670_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9464_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9462_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9464_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9462_0_0_0;
extern Il2CppType ICollection_1_t9462_1_0_0;
struct ICollection_1_t9462;
extern Il2CppGenericClass ICollection_1_t9462_GenericClass;
TypeInfo ICollection_1_t9462_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9462_MethodInfos/* methods */
	, ICollection_1_t9462_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9462_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9462_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9462_0_0_0/* byval_arg */
	, &ICollection_1_t9462_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9462_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Resources.NeutralResourcesLanguageAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Resources.NeutralResourcesLanguageAttribute>
extern Il2CppType IEnumerator_1_t7368_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52671_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Resources.NeutralResourcesLanguageAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52671_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9464_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7368_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52671_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9464_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52671_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9464_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9464_0_0_0;
extern Il2CppType IEnumerable_1_t9464_1_0_0;
struct IEnumerable_1_t9464;
extern Il2CppGenericClass IEnumerable_1_t9464_GenericClass;
TypeInfo IEnumerable_1_t9464_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9464_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9464_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9464_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9464_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9464_0_0_0/* byval_arg */
	, &IEnumerable_1_t9464_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9464_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9463_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>
extern MethodInfo IList_1_get_Item_m52672_MethodInfo;
extern MethodInfo IList_1_set_Item_m52673_MethodInfo;
static PropertyInfo IList_1_t9463____Item_PropertyInfo = 
{
	&IList_1_t9463_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52672_MethodInfo/* get */
	, &IList_1_set_Item_m52673_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9463_PropertyInfos[] =
{
	&IList_1_t9463____Item_PropertyInfo,
	NULL
};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
static ParameterInfo IList_1_t9463_IList_1_IndexOf_m52674_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttribute_t1339_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52674_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52674_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9463_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9463_IList_1_IndexOf_m52674_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52674_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
static ParameterInfo IList_1_t9463_IList_1_Insert_m52675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttribute_t1339_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52675_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52675_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9463_IList_1_Insert_m52675_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52675_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9463_IList_1_RemoveAt_m52676_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52676_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52676_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9463_IList_1_RemoveAt_m52676_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52676_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9463_IList_1_get_Item_m52672_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52672_GenericMethod;
// T System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52672_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9463_il2cpp_TypeInfo/* declaring_type */
	, &NeutralResourcesLanguageAttribute_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9463_IList_1_get_Item_m52672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52672_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NeutralResourcesLanguageAttribute_t1339_0_0_0;
static ParameterInfo IList_1_t9463_IList_1_set_Item_m52673_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &NeutralResourcesLanguageAttribute_t1339_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52673_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52673_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9463_IList_1_set_Item_m52673_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52673_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9463_MethodInfos[] =
{
	&IList_1_IndexOf_m52674_MethodInfo,
	&IList_1_Insert_m52675_MethodInfo,
	&IList_1_RemoveAt_m52676_MethodInfo,
	&IList_1_get_Item_m52672_MethodInfo,
	&IList_1_set_Item_m52673_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9463_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9462_il2cpp_TypeInfo,
	&IEnumerable_1_t9464_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9463_0_0_0;
extern Il2CppType IList_1_t9463_1_0_0;
struct IList_1_t9463;
extern Il2CppGenericClass IList_1_t9463_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9463_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9463_MethodInfos/* methods */
	, IList_1_t9463_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9463_il2cpp_TypeInfo/* element_class */
	, IList_1_t9463_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9463_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9463_0_0_0/* byval_arg */
	, &IList_1_t9463_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9463_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7370_il2cpp_TypeInfo;

// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Resources.SatelliteContractVersionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52677_MethodInfo;
static PropertyInfo IEnumerator_1_t7370____Current_PropertyInfo = 
{
	&IEnumerator_1_t7370_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7370_PropertyInfos[] =
{
	&IEnumerator_1_t7370____Current_PropertyInfo,
	NULL
};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52677_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52677_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7370_il2cpp_TypeInfo/* declaring_type */
	, &SatelliteContractVersionAttribute_t1334_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52677_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7370_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52677_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7370_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7370_0_0_0;
extern Il2CppType IEnumerator_1_t7370_1_0_0;
struct IEnumerator_1_t7370;
extern Il2CppGenericClass IEnumerator_1_t7370_GenericClass;
TypeInfo IEnumerator_1_t7370_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7370_MethodInfos/* methods */
	, IEnumerator_1_t7370_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7370_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7370_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7370_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7370_0_0_0/* byval_arg */
	, &IEnumerator_1_t7370_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7370_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_701.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5247_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_701MethodDeclarations.h"

extern TypeInfo SatelliteContractVersionAttribute_t1334_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31611_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSatelliteContractVersionAttribute_t1334_m41433_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Resources.SatelliteContractVersionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Resources.SatelliteContractVersionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSatelliteContractVersionAttribute_t1334_m41433(__this, p0, method) (SatelliteContractVersionAttribute_t1334 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5247____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5247, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5247____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5247, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5247_FieldInfos[] =
{
	&InternalEnumerator_1_t5247____array_0_FieldInfo,
	&InternalEnumerator_1_t5247____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5247____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5247_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5247____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5247_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31611_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5247_PropertyInfos[] =
{
	&InternalEnumerator_1_t5247____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5247____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5247_InternalEnumerator_1__ctor_m31607_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31607_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31607_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5247_InternalEnumerator_1__ctor_m31607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31607_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31609_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31609_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31609_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31610_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31610_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31610_GenericMethod/* genericMethod */

};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31611_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31611_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* declaring_type */
	, &SatelliteContractVersionAttribute_t1334_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31611_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5247_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31607_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_MethodInfo,
	&InternalEnumerator_1_Dispose_m31609_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31610_MethodInfo,
	&InternalEnumerator_1_get_Current_m31611_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31610_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31609_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5247_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31608_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31610_MethodInfo,
	&InternalEnumerator_1_Dispose_m31609_MethodInfo,
	&InternalEnumerator_1_get_Current_m31611_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5247_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7370_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5247_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7370_il2cpp_TypeInfo, 7},
};
extern TypeInfo SatelliteContractVersionAttribute_t1334_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5247_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31611_MethodInfo/* Method Usage */,
	&SatelliteContractVersionAttribute_t1334_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSatelliteContractVersionAttribute_t1334_m41433_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5247_0_0_0;
extern Il2CppType InternalEnumerator_1_t5247_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5247_GenericClass;
TypeInfo InternalEnumerator_1_t5247_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5247_MethodInfos/* methods */
	, InternalEnumerator_1_t5247_PropertyInfos/* properties */
	, InternalEnumerator_1_t5247_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5247_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5247_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5247_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5247_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5247_1_0_0/* this_arg */
	, InternalEnumerator_1_t5247_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5247_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5247_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5247)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9465_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>
extern MethodInfo ICollection_1_get_Count_m52678_MethodInfo;
static PropertyInfo ICollection_1_t9465____Count_PropertyInfo = 
{
	&ICollection_1_t9465_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52679_MethodInfo;
static PropertyInfo ICollection_1_t9465____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9465_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52679_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9465_PropertyInfos[] =
{
	&ICollection_1_t9465____Count_PropertyInfo,
	&ICollection_1_t9465____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52678_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52678_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52678_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52679_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52679_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52679_GenericMethod/* genericMethod */

};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
static ParameterInfo ICollection_1_t9465_ICollection_1_Add_m52680_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttribute_t1334_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52680_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52680_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9465_ICollection_1_Add_m52680_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52680_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52681_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52681_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52681_GenericMethod/* genericMethod */

};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
static ParameterInfo ICollection_1_t9465_ICollection_1_Contains_m52682_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttribute_t1334_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52682_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52682_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9465_ICollection_1_Contains_m52682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52682_GenericMethod/* genericMethod */

};
extern Il2CppType SatelliteContractVersionAttributeU5BU5D_t5546_0_0_0;
extern Il2CppType SatelliteContractVersionAttributeU5BU5D_t5546_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9465_ICollection_1_CopyTo_m52683_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttributeU5BU5D_t5546_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52683_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52683_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9465_ICollection_1_CopyTo_m52683_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52683_GenericMethod/* genericMethod */

};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
static ParameterInfo ICollection_1_t9465_ICollection_1_Remove_m52684_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttribute_t1334_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52684_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52684_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9465_ICollection_1_Remove_m52684_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52684_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9465_MethodInfos[] =
{
	&ICollection_1_get_Count_m52678_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52679_MethodInfo,
	&ICollection_1_Add_m52680_MethodInfo,
	&ICollection_1_Clear_m52681_MethodInfo,
	&ICollection_1_Contains_m52682_MethodInfo,
	&ICollection_1_CopyTo_m52683_MethodInfo,
	&ICollection_1_Remove_m52684_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9467_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9465_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9467_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9465_0_0_0;
extern Il2CppType ICollection_1_t9465_1_0_0;
struct ICollection_1_t9465;
extern Il2CppGenericClass ICollection_1_t9465_GenericClass;
TypeInfo ICollection_1_t9465_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9465_MethodInfos/* methods */
	, ICollection_1_t9465_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9465_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9465_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9465_0_0_0/* byval_arg */
	, &ICollection_1_t9465_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9465_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
