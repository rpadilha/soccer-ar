﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_59.h"
// UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>
struct CachedInvokableCall_1_t3140  : public InvokableCall_1_t3141
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<PlayerRelativeControl>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
