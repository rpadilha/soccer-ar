﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>
struct InternalEnumerator_1_t4940;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29864 (InternalEnumerator_1_t4940 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865 (InternalEnumerator_1_t4940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::Dispose()
 void InternalEnumerator_1_Dispose_m29866 (InternalEnumerator_1_t4940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29867 (InternalEnumerator_1_t4940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29868 (InternalEnumerator_1_t4940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
