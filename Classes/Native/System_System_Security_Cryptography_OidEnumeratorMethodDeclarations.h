﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.OidEnumerator
struct OidEnumerator_t1450;
// System.Object
struct Object_t;
// System.Security.Cryptography.OidCollection
struct OidCollection_t1434;

// System.Void System.Security.Cryptography.OidEnumerator::.ctor(System.Security.Cryptography.OidCollection)
 void OidEnumerator__ctor_m7325 (OidEnumerator_t1450 * __this, OidCollection_t1434 * ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.OidEnumerator::System.Collections.IEnumerator.get_Current()
 Object_t * OidEnumerator_System_Collections_IEnumerator_get_Current_m7326 (OidEnumerator_t1450 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.OidEnumerator::MoveNext()
 bool OidEnumerator_MoveNext_m7327 (OidEnumerator_t1450 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
