﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour/WorldCenterMode>
struct InternalEnumerator_1_t4517;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour/WorldCenterMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27243 (InternalEnumerator_1_t4517 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour/WorldCenterMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27244 (InternalEnumerator_1_t4517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour/WorldCenterMode>::Dispose()
 void InternalEnumerator_1_Dispose_m27245 (InternalEnumerator_1_t4517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour/WorldCenterMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27246 (InternalEnumerator_1_t4517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour/WorldCenterMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m27247 (InternalEnumerator_1_t4517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
