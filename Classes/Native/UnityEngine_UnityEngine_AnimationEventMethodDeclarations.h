﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationEvent
struct AnimationEvent_t1063;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// UnityEngine.AnimationState
struct AnimationState_t186;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"

// System.Void UnityEngine.AnimationEvent::.ctor()
 void AnimationEvent__ctor_m6287 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_data()
 String_t* AnimationEvent_get_data_m6288 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
 void AnimationEvent_set_data_m6289 (AnimationEvent_t1063 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_stringParameter()
 String_t* AnimationEvent_get_stringParameter_m6290 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
 void AnimationEvent_set_stringParameter_m6291 (AnimationEvent_t1063 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
 float AnimationEvent_get_floatParameter_m6292 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
 void AnimationEvent_set_floatParameter_m6293 (AnimationEvent_t1063 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
 int32_t AnimationEvent_get_intParameter_m6294 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
 void AnimationEvent_set_intParameter_m6295 (AnimationEvent_t1063 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
 Object_t120 * AnimationEvent_get_objectReferenceParameter_m6296 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
 void AnimationEvent_set_objectReferenceParameter_m6297 (AnimationEvent_t1063 * __this, Object_t120 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_functionName()
 String_t* AnimationEvent_get_functionName_m6298 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
 void AnimationEvent_set_functionName_m6299 (AnimationEvent_t1063 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationEvent::get_time()
 float AnimationEvent_get_time_m6300 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
 void AnimationEvent_set_time_m6301 (AnimationEvent_t1063 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
 int32_t AnimationEvent_get_messageOptions_m6302 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
 void AnimationEvent_set_messageOptions_m6303 (AnimationEvent_t1063 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
 bool AnimationEvent_get_isFiredByLegacy_m6304 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
 bool AnimationEvent_get_isFiredByAnimator_m6305 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
 AnimationState_t186 * AnimationEvent_get_animationState_m6306 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
 AnimatorStateInfo_t1064  AnimationEvent_get_animatorStateInfo_m6307 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
 AnimatorClipInfo_t1065  AnimationEvent_get_animatorClipInfo_m6308 (AnimationEvent_t1063 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
