﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DefaultUriParser
struct DefaultUriParser_t1513;
// System.String
struct String_t;

// System.Void System.DefaultUriParser::.ctor()
 void DefaultUriParser__ctor_m7699 (DefaultUriParser_t1513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DefaultUriParser::.ctor(System.String)
 void DefaultUriParser__ctor_m7700 (DefaultUriParser_t1513 * __this, String_t* ___scheme, MethodInfo* method) IL2CPP_METHOD_ATTR;
