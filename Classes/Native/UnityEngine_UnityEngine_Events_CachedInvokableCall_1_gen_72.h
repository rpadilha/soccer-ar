﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.UIBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_69.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.UIBehaviour>
struct CachedInvokableCall_1_t3311  : public InvokableCall_1_t3312
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.UIBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
