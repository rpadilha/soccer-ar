﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
// Vuforia.VirtualButton/Sensitivity
struct Sensitivity_t789 
{
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___1;
};
