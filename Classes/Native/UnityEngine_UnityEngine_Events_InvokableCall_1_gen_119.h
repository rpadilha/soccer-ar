﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceAbstractBehaviour>
struct UnityAction_1_t3904;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceAbstractBehaviour>
struct InvokableCall_1_t3903  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceAbstractBehaviour>::Delegate
	UnityAction_1_t3904 * ___Delegate_0;
};
