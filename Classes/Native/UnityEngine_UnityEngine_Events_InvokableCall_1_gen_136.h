﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.WebCamAbstractBehaviour>
struct UnityAction_1_t4598;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamAbstractBehaviour>
struct InvokableCall_1_t4597  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.WebCamAbstractBehaviour>::Delegate
	UnityAction_1_t4598 * ___Delegate_0;
};
