﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#pragma pack(push, tp, 1)
struct VirtualButtonData_t685 
{
	// System.Int32 Vuforia.QCARManagerImpl/VirtualButtonData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.QCARManagerImpl/VirtualButtonData::isPressed
	int32_t ___isPressed_1;
};
#pragma pack(pop, tp)
