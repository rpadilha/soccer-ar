﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Selectable>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_90.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Selectable>
struct CachedInvokableCall_1_t3638  : public InvokableCall_1_t3639
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Selectable>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
