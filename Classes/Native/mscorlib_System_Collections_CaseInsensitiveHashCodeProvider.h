﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveHashCodeProvider
struct CaseInsensitiveHashCodeProvider_t1545;
// System.Object
struct Object_t;
// System.Globalization.TextInfo
struct TextInfo_t1799;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveHashCodeProvider
struct CaseInsensitiveHashCodeProvider_t1545  : public Object_t
{
	// System.Globalization.TextInfo System.Collections.CaseInsensitiveHashCodeProvider::m_text
	TextInfo_t1799 * ___m_text_2;
};
struct CaseInsensitiveHashCodeProvider_t1545_StaticFields{
	// System.Collections.CaseInsensitiveHashCodeProvider System.Collections.CaseInsensitiveHashCodeProvider::singletonInvariant
	CaseInsensitiveHashCodeProvider_t1545 * ___singletonInvariant_0;
	// System.Object System.Collections.CaseInsensitiveHashCodeProvider::sync
	Object_t * ___sync_1;
};
