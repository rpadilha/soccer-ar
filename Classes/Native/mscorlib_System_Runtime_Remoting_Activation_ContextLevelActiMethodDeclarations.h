﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t2038;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2033;

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
 void ContextLevelActivator__ctor_m11621 (ContextLevelActivator_t2038 * __this, Object_t * ___next, MethodInfo* method) IL2CPP_METHOD_ATTR;
