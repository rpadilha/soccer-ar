﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t396;

// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
 void ScrollRectEvent__ctor_m1564 (ScrollRectEvent_t396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
