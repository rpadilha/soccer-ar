﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamProfile
struct WebCamProfile_t793;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"

// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamProfile::get_Default()
 ProfileData_t791  WebCamProfile_get_Default_m4242 (WebCamProfile_t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamProfile::.ctor()
 void WebCamProfile__ctor_m4243 (WebCamProfile_t793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamProfile::GetProfile(System.String)
 ProfileData_t791  WebCamProfile_GetProfile_m4244 (WebCamProfile_t793 * __this, String_t* ___webcamName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamProfile::ProfileAvailable(System.String)
 bool WebCamProfile_ProfileAvailable_m4245 (WebCamProfile_t793 * __this, String_t* ___webcamName, MethodInfo* method) IL2CPP_METHOD_ATTR;
