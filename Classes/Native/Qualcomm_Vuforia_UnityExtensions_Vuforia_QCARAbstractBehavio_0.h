﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
struct WorldCenterMode_t799 
{
	// System.Int32 Vuforia.QCARAbstractBehaviour/WorldCenterMode::value__
	int32_t ___value___1;
};
