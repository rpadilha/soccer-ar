﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>
struct KeyValuePair_2_t3433;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t330;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m18173 (KeyValuePair_2_t3433 * __this, Object_t * ___key, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Key()
 Object_t * KeyValuePair_2_get_Key_m18174 (KeyValuePair_2_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m18175 (KeyValuePair_2_t3433 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Value()
 int32_t KeyValuePair_2_get_Value_m18176 (KeyValuePair_2_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m18177 (KeyValuePair_2_t3433 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::ToString()
 String_t* KeyValuePair_2_ToString_m18178 (KeyValuePair_2_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
