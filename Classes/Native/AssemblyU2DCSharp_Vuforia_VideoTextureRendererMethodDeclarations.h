﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t59;

// System.Void Vuforia.VideoTextureRenderer::.ctor()
 void VideoTextureRenderer__ctor_m93 (VideoTextureRenderer_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
