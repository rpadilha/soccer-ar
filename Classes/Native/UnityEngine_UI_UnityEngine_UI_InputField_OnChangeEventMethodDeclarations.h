﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t369;

// System.Void UnityEngine.UI.InputField/OnChangeEvent::.ctor()
 void OnChangeEvent__ctor_m1347 (OnChangeEvent_t369 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
