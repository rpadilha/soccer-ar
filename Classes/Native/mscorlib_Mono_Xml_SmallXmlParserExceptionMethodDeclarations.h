﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Xml.SmallXmlParserException
struct SmallXmlParserException_t1849;
// System.String
struct String_t;

// System.Void Mono.Xml.SmallXmlParserException::.ctor(System.String,System.Int32,System.Int32)
 void SmallXmlParserException__ctor_m10416 (SmallXmlParserException_t1849 * __this, String_t* ___msg, int32_t ___line, int32_t ___column, MethodInfo* method) IL2CPP_METHOD_ATTR;
