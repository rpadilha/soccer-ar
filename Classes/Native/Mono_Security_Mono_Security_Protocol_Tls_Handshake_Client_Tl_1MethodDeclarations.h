﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
struct TlsClientFinished_t1691;
// Mono.Security.Protocol.Tls.Context
struct Context_t1642;

// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::.ctor(Mono.Security.Protocol.Tls.Context)
 void TlsClientFinished__ctor_m8891 (TlsClientFinished_t1691 * __this, Context_t1642 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::.cctor()
 void TlsClientFinished__cctor_m8892 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::Update()
 void TlsClientFinished_Update_m8893 (TlsClientFinished_t1691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::ProcessAsSsl3()
 void TlsClientFinished_ProcessAsSsl3_m8894 (TlsClientFinished_t1691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::ProcessAsTls1()
 void TlsClientFinished_ProcessAsTls1_m8895 (TlsClientFinished_t1691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
