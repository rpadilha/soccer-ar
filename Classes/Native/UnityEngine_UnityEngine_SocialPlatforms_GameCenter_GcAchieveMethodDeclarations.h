﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t979;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t1095;

// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
 AchievementDescription_t1095 * GcAchievementDescriptionData_ToAchievementDescription_m6422 (GcAchievementDescriptionData_t979 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
