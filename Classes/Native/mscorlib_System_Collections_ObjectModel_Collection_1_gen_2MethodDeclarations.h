﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>
struct Collection_1_t3187;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t234;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t3177;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseInputModule>
struct IEnumerator_1_t3179;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.BaseInputModule>
struct IList_1_t3186;

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m16175(__this, method) (void)Collection_1__ctor_m14594_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16176(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14595_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m16177(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14596_gshared((Collection_1_t2839 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16178(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14597_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m16179(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14598_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m16180(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14599_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m16181(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14600_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m16182(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14601_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m16183(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14602_gshared((Collection_1_t2839 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16184(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14603_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16185(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14604_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16186(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14605_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16187(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14606_gshared((Collection_1_t2839 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m16188(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14607_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m16189(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14608_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::Add(T)
#define Collection_1_Add_m16190(__this, ___item, method) (void)Collection_1_Add_m14609_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::Clear()
#define Collection_1_Clear_m16191(__this, method) (void)Collection_1_Clear_m14610_gshared((Collection_1_t2839 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::ClearItems()
#define Collection_1_ClearItems_m16192(__this, method) (void)Collection_1_ClearItems_m14611_gshared((Collection_1_t2839 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::Contains(T)
#define Collection_1_Contains_m16193(__this, ___item, method) (bool)Collection_1_Contains_m14612_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m16194(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14613_gshared((Collection_1_t2839 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::GetEnumerator()
#define Collection_1_GetEnumerator_m16195(__this, method) (Object_t*)Collection_1_GetEnumerator_m14614_gshared((Collection_1_t2839 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::IndexOf(T)
#define Collection_1_IndexOf_m16196(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14615_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::Insert(System.Int32,T)
#define Collection_1_Insert_m16197(__this, ___index, ___item, method) (void)Collection_1_Insert_m14616_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m16198(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14617_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::Remove(T)
#define Collection_1_Remove_m16199(__this, ___item, method) (bool)Collection_1_Remove_m14618_gshared((Collection_1_t2839 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m16200(__this, ___index, method) (void)Collection_1_RemoveAt_m14619_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m16201(__this, ___index, method) (void)Collection_1_RemoveItem_m14620_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::get_Count()
#define Collection_1_get_Count_m16202(__this, method) (int32_t)Collection_1_get_Count_m14621_gshared((Collection_1_t2839 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::get_Item(System.Int32)
#define Collection_1_get_Item_m16203(__this, ___index, method) (BaseInputModule_t234 *)Collection_1_get_Item_m14622_gshared((Collection_1_t2839 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m16204(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14623_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m16205(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14624_gshared((Collection_1_t2839 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m16206(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14625_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m16207(__this/* static, unused */, ___item, method) (BaseInputModule_t234 *)Collection_1_ConvertItem_m14626_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m16208(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14627_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m16209(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14628_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseInputModule>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m16210(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14629_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
