﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>
struct IList_1_t4509;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVideoBackgroundEventHandler>
struct ReadOnlyCollection_1_t4506  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVideoBackgroundEventHandler>::list
	Object_t* ___list_0;
};
