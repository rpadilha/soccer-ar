﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.OrdinalComparer
struct OrdinalComparer_t2299;
// System.String
struct String_t;

// System.Void System.OrdinalComparer::.ctor(System.Boolean)
 void OrdinalComparer__ctor_m13363 (OrdinalComparer_t2299 * __this, bool ___ignoreCase, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.OrdinalComparer::Compare(System.String,System.String)
 int32_t OrdinalComparer_Compare_m13364 (OrdinalComparer_t2299 * __this, String_t* ___x, String_t* ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.OrdinalComparer::Equals(System.String,System.String)
 bool OrdinalComparer_Equals_m13365 (OrdinalComparer_t2299 * __this, String_t* ___x, String_t* ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.OrdinalComparer::GetHashCode(System.String)
 int32_t OrdinalComparer_GetHashCode_m13366 (OrdinalComparer_t2299 * __this, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
