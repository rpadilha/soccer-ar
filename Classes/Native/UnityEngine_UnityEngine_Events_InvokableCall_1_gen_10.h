﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ComponentFactoryStarterBehaviour>
struct UnityAction_1_t2825;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>
struct InvokableCall_1_t2824  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>::Delegate
	UnityAction_1_t2825 * ___Delegate_0;
};
