﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<PlayerRelativeControl>
struct UnityAction_1_t3142;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>
struct InvokableCall_1_t3141  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<PlayerRelativeControl>::Delegate
	UnityAction_1_t3142 * ___Delegate_0;
};
