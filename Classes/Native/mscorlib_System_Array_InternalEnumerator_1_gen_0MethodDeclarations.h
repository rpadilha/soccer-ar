﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t2752;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m14156_gshared (InternalEnumerator_1_t2752 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m14156(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
 void InternalEnumerator_1_Dispose_m14160_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14160(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m14162_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14162(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
 Object_t * InternalEnumerator_1_get_Current_m14164_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14164(__this, method) (Object_t *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
