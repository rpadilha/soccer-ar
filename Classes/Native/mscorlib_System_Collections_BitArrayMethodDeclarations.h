﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.BitArray
struct BitArray_t1479;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;

// System.Void System.Collections.BitArray::.ctor(System.Int32)
 void BitArray__ctor_m8131 (BitArray_t1479 * __this, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Collections.BitArray::getByte(System.Int32)
 uint8_t BitArray_getByte_m10510 (BitArray_t1479 * __this, int32_t ___byteIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Count()
 int32_t BitArray_get_Count_m10511 (BitArray_t1479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_IsSynchronized()
 bool BitArray_get_IsSynchronized_m10512 (BitArray_t1479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_Item(System.Int32)
 bool BitArray_get_Item_m8112 (BitArray_t1479 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::set_Item(System.Int32,System.Boolean)
 void BitArray_set_Item_m8132 (BitArray_t1479 * __this, int32_t ___index, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Length()
 int32_t BitArray_get_Length_m8111 (BitArray_t1479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray::get_SyncRoot()
 Object_t * BitArray_get_SyncRoot_m10513 (BitArray_t1479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::CopyTo(System.Array,System.Int32)
 void BitArray_CopyTo_m10514 (BitArray_t1479 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::Get(System.Int32)
 bool BitArray_Get_m10515 (BitArray_t1479 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::Set(System.Int32,System.Boolean)
 void BitArray_Set_m10516 (BitArray_t1479 * __this, int32_t ___index, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.BitArray::GetEnumerator()
 Object_t * BitArray_GetEnumerator_m10517 (BitArray_t1479 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
