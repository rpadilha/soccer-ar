﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t150;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
struct Enumerator_t143 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::l
	List_1_t150 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::current
	MethodInfo_t141 * ___current_3;
};
