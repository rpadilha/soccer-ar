﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UI.Toggle>
struct Comparer_1_t3720;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UI.Toggle>
struct Comparer_1_t3720  : public Object_t
{
};
struct Comparer_1_t3720_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UI.Toggle>::_default
	Comparer_1_t3720 * ____default_0;
};
