﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<ZoomCamera>
struct UnityAction_1_t3170;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<ZoomCamera>
struct InvokableCall_1_t3169  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<ZoomCamera>::Delegate
	UnityAction_1_t3170 * ___Delegate_0;
};
