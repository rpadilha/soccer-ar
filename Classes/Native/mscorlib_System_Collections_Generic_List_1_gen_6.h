﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t3280;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t245  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::_items
	EntryU5BU5D_t3280* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::_version
	int32_t ____version_3;
};
struct List_1_t245_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::EmptyArray
	EntryU5BU5D_t3280* ___EmptyArray_4;
};
