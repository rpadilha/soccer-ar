﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t15;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Vuforia.Prop>
struct Action_1_t126  : public MulticastDelegate_t373
{
};
