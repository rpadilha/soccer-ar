﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>
struct Enumerator_t897;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t16;
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t767;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"
#define Enumerator__ctor_m25561(__this, ___l, method) (void)Enumerator__ctor_m14558_gshared((Enumerator_t2837 *)__this, (List_1_t149 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25562(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m25563(__this, method) (void)Enumerator_Dispose_m14560_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::VerifyState()
#define Enumerator_VerifyState_m25564(__this, method) (void)Enumerator_VerifyState_m14561_gshared((Enumerator_t2837 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m5345(__this, method) (bool)Enumerator_MoveNext_m14562_gshared((Enumerator_t2837 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m5343(__this, method) (Object_t *)Enumerator_get_Current_m14563_gshared((Enumerator_t2837 *)__this, method)
