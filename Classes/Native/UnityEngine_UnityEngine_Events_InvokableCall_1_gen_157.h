﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
struct UnityAction_1_t4778;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
struct InvokableCall_1_t4777  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Delegate
	UnityAction_1_t4778 * ___Delegate_0;
};
