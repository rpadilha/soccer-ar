﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ScoreHUD
struct ScoreHUD_t89;

// System.Void ScoreHUD::.ctor()
 void ScoreHUD__ctor_m141 (ScoreHUD_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreHUD::Start()
 void ScoreHUD_Start_m142 (ScoreHUD_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreHUD::LateUpdate()
 void ScoreHUD_LateUpdate_m143 (ScoreHUD_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
