﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_484.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InternalEnumerator_1_t4936_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_484MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Array
#include "mscorlib_System_Array.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo ScreenOrientation_t137_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m29848_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisScreenOrientation_t137_m38962_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.ScreenOrientation>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ScreenOrientation>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisScreenOrientation_t137_m38962 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29844_MethodInfo;
 void InternalEnumerator_1__ctor_m29844 (InternalEnumerator_1_t4936 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845 (InternalEnumerator_1_t4936 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29848(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29848_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ScreenOrientation_t137_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29846_MethodInfo;
 void InternalEnumerator_1_Dispose_m29846 (InternalEnumerator_1_t4936 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29847_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29847 (InternalEnumerator_1_t4936 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29848 (InternalEnumerator_1_t4936 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisScreenOrientation_t137_m38962(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisScreenOrientation_t137_m38962_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4936____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4936, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4936____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4936, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4936_FieldInfos[] =
{
	&InternalEnumerator_1_t4936____array_0_FieldInfo,
	&InternalEnumerator_1_t4936____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4936____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4936_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4936____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4936_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29848_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4936_PropertyInfos[] =
{
	&InternalEnumerator_1_t4936____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4936____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4936_InternalEnumerator_1__ctor_m29844_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29844_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29844_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29844/* method */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4936_InternalEnumerator_1__ctor_m29844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29844_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845/* method */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29846_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29846_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29846/* method */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29846_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29847_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29847_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29847/* method */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29847_GenericMethod/* genericMethod */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t137 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29848_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29848_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29848/* method */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t137_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t137/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29848_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4936_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29844_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_MethodInfo,
	&InternalEnumerator_1_Dispose_m29846_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29847_MethodInfo,
	&InternalEnumerator_1_get_Current_m29848_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4936_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29847_MethodInfo,
	&InternalEnumerator_1_Dispose_m29846_MethodInfo,
	&InternalEnumerator_1_get_Current_m29848_MethodInfo,
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t6948_il2cpp_TypeInfo;
static TypeInfo* InternalEnumerator_1_t4936_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6948_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4936_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6948_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4936_0_0_0;
extern Il2CppType InternalEnumerator_1_t4936_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4936_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4936_MethodInfos/* methods */
	, InternalEnumerator_1_t4936_PropertyInfos/* properties */
	, InternalEnumerator_1_t4936_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4936_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4936_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4936_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4936_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4936_1_0_0/* this_arg */
	, InternalEnumerator_1_t4936_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4936)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8820_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>
extern MethodInfo ICollection_1_get_Count_m49592_MethodInfo;
static PropertyInfo ICollection_1_t8820____Count_PropertyInfo = 
{
	&ICollection_1_t8820_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49592_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49593_MethodInfo;
static PropertyInfo ICollection_1_t8820____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8820_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49593_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8820_PropertyInfos[] =
{
	&ICollection_1_t8820____Count_PropertyInfo,
	&ICollection_1_t8820____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49592_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::get_Count()
MethodInfo ICollection_1_get_Count_m49592_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49592_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49593_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49593_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49593_GenericMethod/* genericMethod */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo ICollection_1_t8820_ICollection_1_Add_m49594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49594_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Add(T)
MethodInfo ICollection_1_Add_m49594_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8820_ICollection_1_Add_m49594_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49594_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49595_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Clear()
MethodInfo ICollection_1_Clear_m49595_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49595_GenericMethod/* genericMethod */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo ICollection_1_t8820_ICollection_1_Contains_m49596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49596_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Contains(T)
MethodInfo ICollection_1_Contains_m49596_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8820_ICollection_1_Contains_m49596_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49596_GenericMethod/* genericMethod */

};
extern Il2CppType ScreenOrientationU5BU5D_t5745_0_0_0;
extern Il2CppType ScreenOrientationU5BU5D_t5745_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8820_ICollection_1_CopyTo_m49597_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ScreenOrientationU5BU5D_t5745_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49597_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49597_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8820_ICollection_1_CopyTo_m49597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49597_GenericMethod/* genericMethod */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo ICollection_1_t8820_ICollection_1_Remove_m49598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49598_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.ScreenOrientation>::Remove(T)
MethodInfo ICollection_1_Remove_m49598_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8820_ICollection_1_Remove_m49598_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49598_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8820_MethodInfos[] =
{
	&ICollection_1_get_Count_m49592_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49593_MethodInfo,
	&ICollection_1_Add_m49594_MethodInfo,
	&ICollection_1_Clear_m49595_MethodInfo,
	&ICollection_1_Contains_m49596_MethodInfo,
	&ICollection_1_CopyTo_m49597_MethodInfo,
	&ICollection_1_Remove_m49598_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8822_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8820_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8822_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8820_0_0_0;
extern Il2CppType ICollection_1_t8820_1_0_0;
struct ICollection_1_t8820;
extern Il2CppGenericClass ICollection_1_t8820_GenericClass;
TypeInfo ICollection_1_t8820_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8820_MethodInfos/* methods */
	, ICollection_1_t8820_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8820_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8820_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8820_0_0_0/* byval_arg */
	, &ICollection_1_t8820_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8820_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.ScreenOrientation>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.ScreenOrientation>
extern Il2CppType IEnumerator_1_t6948_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49599_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.ScreenOrientation>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49599_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8822_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6948_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49599_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8822_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49599_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8822_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8822_0_0_0;
extern Il2CppType IEnumerable_1_t8822_1_0_0;
struct IEnumerable_1_t8822;
extern Il2CppGenericClass IEnumerable_1_t8822_GenericClass;
TypeInfo IEnumerable_1_t8822_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8822_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8822_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8822_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8822_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8822_0_0_0/* byval_arg */
	, &IEnumerable_1_t8822_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8822_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8821_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>
extern MethodInfo IList_1_get_Item_m49600_MethodInfo;
extern MethodInfo IList_1_set_Item_m49601_MethodInfo;
static PropertyInfo IList_1_t8821____Item_PropertyInfo = 
{
	&IList_1_t8821_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49600_MethodInfo/* get */
	, &IList_1_set_Item_m49601_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8821_PropertyInfos[] =
{
	&IList_1_t8821____Item_PropertyInfo,
	NULL
};
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo IList_1_t8821_IList_1_IndexOf_m49602_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49602_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49602_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8821_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8821_IList_1_IndexOf_m49602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49602_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo IList_1_t8821_IList_1_Insert_m49603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49603_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49603_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8821_IList_1_Insert_m49603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49603_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8821_IList_1_RemoveAt_m49604_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49604_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49604_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8821_IList_1_RemoveAt_m49604_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49604_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8821_IList_1_get_Item_m49600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t137_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49600_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49600_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8821_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t137_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t137_Int32_t123/* invoker_method */
	, IList_1_t8821_IList_1_get_Item_m49600_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49600_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo IList_1_t8821_IList_1_set_Item_m49601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49601_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.ScreenOrientation>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49601_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8821_IList_1_set_Item_m49601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49601_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8821_MethodInfos[] =
{
	&IList_1_IndexOf_m49602_MethodInfo,
	&IList_1_Insert_m49603_MethodInfo,
	&IList_1_RemoveAt_m49604_MethodInfo,
	&IList_1_get_Item_m49600_MethodInfo,
	&IList_1_set_Item_m49601_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8821_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8820_il2cpp_TypeInfo,
	&IEnumerable_1_t8822_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8821_0_0_0;
extern Il2CppType IList_1_t8821_1_0_0;
struct IList_1_t8821;
extern Il2CppGenericClass IList_1_t8821_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8821_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8821_MethodInfos/* methods */
	, IList_1_t8821_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8821_il2cpp_TypeInfo/* element_class */
	, IList_1_t8821_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8821_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8821_0_0_0/* byval_arg */
	, &IList_1_t8821_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8821_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6950_il2cpp_TypeInfo;

// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.FilterMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.FilterMode>
extern MethodInfo IEnumerator_1_get_Current_m49605_MethodInfo;
static PropertyInfo IEnumerator_1_t6950____Current_PropertyInfo = 
{
	&IEnumerator_1_t6950_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49605_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6950_PropertyInfos[] =
{
	&IEnumerator_1_t6950____Current_PropertyInfo,
	NULL
};
extern Il2CppType FilterMode_t1099_0_0_0;
extern void* RuntimeInvoker_FilterMode_t1099 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49605_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.FilterMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49605_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6950_il2cpp_TypeInfo/* declaring_type */
	, &FilterMode_t1099_0_0_0/* return_type */
	, RuntimeInvoker_FilterMode_t1099/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49605_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6950_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49605_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6950_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6950_0_0_0;
extern Il2CppType IEnumerator_1_t6950_1_0_0;
struct IEnumerator_1_t6950;
extern Il2CppGenericClass IEnumerator_1_t6950_GenericClass;
TypeInfo IEnumerator_1_t6950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6950_MethodInfos/* methods */
	, IEnumerator_1_t6950_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6950_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6950_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6950_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6950_0_0_0/* byval_arg */
	, &IEnumerator_1_t6950_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.FilterMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_485.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4937_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.FilterMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_485MethodDeclarations.h"

extern TypeInfo FilterMode_t1099_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29853_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFilterMode_t1099_m38973_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.FilterMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.FilterMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFilterMode_t1099_m38973 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29849_MethodInfo;
 void InternalEnumerator_1__ctor_m29849 (InternalEnumerator_1_t4937 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850 (InternalEnumerator_1_t4937 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29853(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29853_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FilterMode_t1099_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29851_MethodInfo;
 void InternalEnumerator_1_Dispose_m29851 (InternalEnumerator_1_t4937 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29852_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29852 (InternalEnumerator_1_t4937 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29853 (InternalEnumerator_1_t4937 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFilterMode_t1099_m38973(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFilterMode_t1099_m38973_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.FilterMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4937____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4937, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4937____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4937, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4937_FieldInfos[] =
{
	&InternalEnumerator_1_t4937____array_0_FieldInfo,
	&InternalEnumerator_1_t4937____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4937____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4937_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4937____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4937_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4937_PropertyInfos[] =
{
	&InternalEnumerator_1_t4937____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4937____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4937_InternalEnumerator_1__ctor_m29849_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29849_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29849_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29849/* method */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4937_InternalEnumerator_1__ctor_m29849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29849_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850/* method */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29851_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29851_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29851/* method */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29851_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29852_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29852_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29852/* method */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29852_GenericMethod/* genericMethod */

};
extern Il2CppType FilterMode_t1099_0_0_0;
extern void* RuntimeInvoker_FilterMode_t1099 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29853_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29853_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29853/* method */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* declaring_type */
	, &FilterMode_t1099_0_0_0/* return_type */
	, RuntimeInvoker_FilterMode_t1099/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29853_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4937_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29849_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_MethodInfo,
	&InternalEnumerator_1_Dispose_m29851_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29852_MethodInfo,
	&InternalEnumerator_1_get_Current_m29853_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4937_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29852_MethodInfo,
	&InternalEnumerator_1_Dispose_m29851_MethodInfo,
	&InternalEnumerator_1_get_Current_m29853_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4937_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6950_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4937_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6950_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4937_0_0_0;
extern Il2CppType InternalEnumerator_1_t4937_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4937_GenericClass;
TypeInfo InternalEnumerator_1_t4937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4937_MethodInfos/* methods */
	, InternalEnumerator_1_t4937_PropertyInfos/* properties */
	, InternalEnumerator_1_t4937_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4937_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4937_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4937_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4937_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4937_1_0_0/* this_arg */
	, InternalEnumerator_1_t4937_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4937)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8823_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>
extern MethodInfo ICollection_1_get_Count_m49606_MethodInfo;
static PropertyInfo ICollection_1_t8823____Count_PropertyInfo = 
{
	&ICollection_1_t8823_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49607_MethodInfo;
static PropertyInfo ICollection_1_t8823____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8823_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8823_PropertyInfos[] =
{
	&ICollection_1_t8823____Count_PropertyInfo,
	&ICollection_1_t8823____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49606_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49606_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49606_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49607_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49607_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49607_GenericMethod/* genericMethod */

};
extern Il2CppType FilterMode_t1099_0_0_0;
extern Il2CppType FilterMode_t1099_0_0_0;
static ParameterInfo ICollection_1_t8823_ICollection_1_Add_m49608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FilterMode_t1099_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49608_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Add(T)
MethodInfo ICollection_1_Add_m49608_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8823_ICollection_1_Add_m49608_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49608_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49609_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Clear()
MethodInfo ICollection_1_Clear_m49609_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49609_GenericMethod/* genericMethod */

};
extern Il2CppType FilterMode_t1099_0_0_0;
static ParameterInfo ICollection_1_t8823_ICollection_1_Contains_m49610_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FilterMode_t1099_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49610_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49610_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8823_ICollection_1_Contains_m49610_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49610_GenericMethod/* genericMethod */

};
extern Il2CppType FilterModeU5BU5D_t5746_0_0_0;
extern Il2CppType FilterModeU5BU5D_t5746_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8823_ICollection_1_CopyTo_m49611_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FilterModeU5BU5D_t5746_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49611_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49611_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8823_ICollection_1_CopyTo_m49611_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49611_GenericMethod/* genericMethod */

};
extern Il2CppType FilterMode_t1099_0_0_0;
static ParameterInfo ICollection_1_t8823_ICollection_1_Remove_m49612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FilterMode_t1099_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49612_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.FilterMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49612_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8823_ICollection_1_Remove_m49612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49612_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8823_MethodInfos[] =
{
	&ICollection_1_get_Count_m49606_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49607_MethodInfo,
	&ICollection_1_Add_m49608_MethodInfo,
	&ICollection_1_Clear_m49609_MethodInfo,
	&ICollection_1_Contains_m49610_MethodInfo,
	&ICollection_1_CopyTo_m49611_MethodInfo,
	&ICollection_1_Remove_m49612_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8825_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8823_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8825_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8823_0_0_0;
extern Il2CppType ICollection_1_t8823_1_0_0;
struct ICollection_1_t8823;
extern Il2CppGenericClass ICollection_1_t8823_GenericClass;
TypeInfo ICollection_1_t8823_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8823_MethodInfos/* methods */
	, ICollection_1_t8823_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8823_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8823_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8823_0_0_0/* byval_arg */
	, &ICollection_1_t8823_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8823_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.FilterMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.FilterMode>
extern Il2CppType IEnumerator_1_t6950_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49613_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.FilterMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49613_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8825_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6950_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49613_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8825_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49613_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8825_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8825_0_0_0;
extern Il2CppType IEnumerable_1_t8825_1_0_0;
struct IEnumerable_1_t8825;
extern Il2CppGenericClass IEnumerable_1_t8825_GenericClass;
TypeInfo IEnumerable_1_t8825_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8825_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8825_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8825_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8825_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8825_0_0_0/* byval_arg */
	, &IEnumerable_1_t8825_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8825_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8824_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.FilterMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.FilterMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.FilterMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.FilterMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.FilterMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.FilterMode>
extern MethodInfo IList_1_get_Item_m49614_MethodInfo;
extern MethodInfo IList_1_set_Item_m49615_MethodInfo;
static PropertyInfo IList_1_t8824____Item_PropertyInfo = 
{
	&IList_1_t8824_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49614_MethodInfo/* get */
	, &IList_1_set_Item_m49615_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8824_PropertyInfos[] =
{
	&IList_1_t8824____Item_PropertyInfo,
	NULL
};
extern Il2CppType FilterMode_t1099_0_0_0;
static ParameterInfo IList_1_t8824_IList_1_IndexOf_m49616_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FilterMode_t1099_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49616_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.FilterMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49616_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8824_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8824_IList_1_IndexOf_m49616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49616_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FilterMode_t1099_0_0_0;
static ParameterInfo IList_1_t8824_IList_1_Insert_m49617_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FilterMode_t1099_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49617_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.FilterMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49617_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8824_IList_1_Insert_m49617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49617_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8824_IList_1_RemoveAt_m49618_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49618_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.FilterMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49618_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8824_IList_1_RemoveAt_m49618_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49618_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8824_IList_1_get_Item_m49614_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FilterMode_t1099_0_0_0;
extern void* RuntimeInvoker_FilterMode_t1099_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49614_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.FilterMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49614_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8824_il2cpp_TypeInfo/* declaring_type */
	, &FilterMode_t1099_0_0_0/* return_type */
	, RuntimeInvoker_FilterMode_t1099_Int32_t123/* invoker_method */
	, IList_1_t8824_IList_1_get_Item_m49614_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49614_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FilterMode_t1099_0_0_0;
static ParameterInfo IList_1_t8824_IList_1_set_Item_m49615_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FilterMode_t1099_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49615_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.FilterMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49615_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8824_IList_1_set_Item_m49615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49615_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8824_MethodInfos[] =
{
	&IList_1_IndexOf_m49616_MethodInfo,
	&IList_1_Insert_m49617_MethodInfo,
	&IList_1_RemoveAt_m49618_MethodInfo,
	&IList_1_get_Item_m49614_MethodInfo,
	&IList_1_set_Item_m49615_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8824_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8823_il2cpp_TypeInfo,
	&IEnumerable_1_t8825_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8824_0_0_0;
extern Il2CppType IList_1_t8824_1_0_0;
struct IList_1_t8824;
extern Il2CppGenericClass IList_1_t8824_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8824_MethodInfos/* methods */
	, IList_1_t8824_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8824_il2cpp_TypeInfo/* element_class */
	, IList_1_t8824_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8824_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8824_0_0_0/* byval_arg */
	, &IList_1_t8824_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8824_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6952_il2cpp_TypeInfo;

// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextureWrapMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextureWrapMode>
extern MethodInfo IEnumerator_1_get_Current_m49619_MethodInfo;
static PropertyInfo IEnumerator_1_t6952____Current_PropertyInfo = 
{
	&IEnumerator_1_t6952_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49619_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6952_PropertyInfos[] =
{
	&IEnumerator_1_t6952____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
extern void* RuntimeInvoker_TextureWrapMode_t1100 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49619_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextureWrapMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49619_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6952_il2cpp_TypeInfo/* declaring_type */
	, &TextureWrapMode_t1100_0_0_0/* return_type */
	, RuntimeInvoker_TextureWrapMode_t1100/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49619_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6952_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49619_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6952_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6952_0_0_0;
extern Il2CppType IEnumerator_1_t6952_1_0_0;
struct IEnumerator_1_t6952;
extern Il2CppGenericClass IEnumerator_1_t6952_GenericClass;
TypeInfo IEnumerator_1_t6952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6952_MethodInfos/* methods */
	, IEnumerator_1_t6952_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6952_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6952_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6952_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6952_0_0_0/* byval_arg */
	, &IEnumerator_1_t6952_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_486.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4938_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_486MethodDeclarations.h"

extern TypeInfo TextureWrapMode_t1100_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29858_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextureWrapMode_t1100_m38984_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TextureWrapMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextureWrapMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTextureWrapMode_t1100_m38984 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29854_MethodInfo;
 void InternalEnumerator_1__ctor_m29854 (InternalEnumerator_1_t4938 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855 (InternalEnumerator_1_t4938 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29858(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29858_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TextureWrapMode_t1100_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29856_MethodInfo;
 void InternalEnumerator_1_Dispose_m29856 (InternalEnumerator_1_t4938 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29857_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29857 (InternalEnumerator_1_t4938 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29858 (InternalEnumerator_1_t4938 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTextureWrapMode_t1100_m38984(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTextureWrapMode_t1100_m38984_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4938____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4938, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4938____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4938, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4938_FieldInfos[] =
{
	&InternalEnumerator_1_t4938____array_0_FieldInfo,
	&InternalEnumerator_1_t4938____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4938____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4938_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4938____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4938_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29858_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4938_PropertyInfos[] =
{
	&InternalEnumerator_1_t4938____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4938____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4938_InternalEnumerator_1__ctor_m29854_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29854_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29854_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29854/* method */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4938_InternalEnumerator_1__ctor_m29854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29854_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855/* method */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29856_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29856_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29856/* method */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29856_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29857_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29857_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29857/* method */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29857_GenericMethod/* genericMethod */

};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
extern void* RuntimeInvoker_TextureWrapMode_t1100 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29858_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TextureWrapMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29858_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29858/* method */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* declaring_type */
	, &TextureWrapMode_t1100_0_0_0/* return_type */
	, RuntimeInvoker_TextureWrapMode_t1100/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29858_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4938_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29854_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_MethodInfo,
	&InternalEnumerator_1_Dispose_m29856_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29857_MethodInfo,
	&InternalEnumerator_1_get_Current_m29858_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4938_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29855_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29857_MethodInfo,
	&InternalEnumerator_1_Dispose_m29856_MethodInfo,
	&InternalEnumerator_1_get_Current_m29858_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4938_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6952_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4938_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6952_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4938_0_0_0;
extern Il2CppType InternalEnumerator_1_t4938_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4938_GenericClass;
TypeInfo InternalEnumerator_1_t4938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4938_MethodInfos/* methods */
	, InternalEnumerator_1_t4938_PropertyInfos/* properties */
	, InternalEnumerator_1_t4938_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4938_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4938_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4938_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4938_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4938_1_0_0/* this_arg */
	, InternalEnumerator_1_t4938_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4938_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4938)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8826_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>
extern MethodInfo ICollection_1_get_Count_m49620_MethodInfo;
static PropertyInfo ICollection_1_t8826____Count_PropertyInfo = 
{
	&ICollection_1_t8826_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49621_MethodInfo;
static PropertyInfo ICollection_1_t8826____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8826_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8826_PropertyInfos[] =
{
	&ICollection_1_t8826____Count_PropertyInfo,
	&ICollection_1_t8826____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49620_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49620_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49620_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49621_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49621_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49621_GenericMethod/* genericMethod */

};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
extern Il2CppType TextureWrapMode_t1100_0_0_0;
static ParameterInfo ICollection_1_t8826_ICollection_1_Add_m49622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureWrapMode_t1100_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49622_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Add(T)
MethodInfo ICollection_1_Add_m49622_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8826_ICollection_1_Add_m49622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49622_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49623_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Clear()
MethodInfo ICollection_1_Clear_m49623_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49623_GenericMethod/* genericMethod */

};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
static ParameterInfo ICollection_1_t8826_ICollection_1_Contains_m49624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureWrapMode_t1100_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49624_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49624_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8826_ICollection_1_Contains_m49624_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49624_GenericMethod/* genericMethod */

};
extern Il2CppType TextureWrapModeU5BU5D_t5747_0_0_0;
extern Il2CppType TextureWrapModeU5BU5D_t5747_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8826_ICollection_1_CopyTo_m49625_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextureWrapModeU5BU5D_t5747_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49625_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49625_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8826_ICollection_1_CopyTo_m49625_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49625_GenericMethod/* genericMethod */

};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
static ParameterInfo ICollection_1_t8826_ICollection_1_Remove_m49626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureWrapMode_t1100_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49626_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureWrapMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49626_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8826_ICollection_1_Remove_m49626_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49626_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8826_MethodInfos[] =
{
	&ICollection_1_get_Count_m49620_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49621_MethodInfo,
	&ICollection_1_Add_m49622_MethodInfo,
	&ICollection_1_Clear_m49623_MethodInfo,
	&ICollection_1_Contains_m49624_MethodInfo,
	&ICollection_1_CopyTo_m49625_MethodInfo,
	&ICollection_1_Remove_m49626_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8828_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8826_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8828_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8826_0_0_0;
extern Il2CppType ICollection_1_t8826_1_0_0;
struct ICollection_1_t8826;
extern Il2CppGenericClass ICollection_1_t8826_GenericClass;
TypeInfo ICollection_1_t8826_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8826_MethodInfos/* methods */
	, ICollection_1_t8826_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8826_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8826_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8826_0_0_0/* byval_arg */
	, &ICollection_1_t8826_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8826_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextureWrapMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextureWrapMode>
extern Il2CppType IEnumerator_1_t6952_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49627_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextureWrapMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49627_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8828_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6952_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49627_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8828_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49627_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8828_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8828_0_0_0;
extern Il2CppType IEnumerable_1_t8828_1_0_0;
struct IEnumerable_1_t8828;
extern Il2CppGenericClass IEnumerable_1_t8828_GenericClass;
TypeInfo IEnumerable_1_t8828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8828_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8828_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8828_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8828_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8828_0_0_0/* byval_arg */
	, &IEnumerable_1_t8828_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8827_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>
extern MethodInfo IList_1_get_Item_m49628_MethodInfo;
extern MethodInfo IList_1_set_Item_m49629_MethodInfo;
static PropertyInfo IList_1_t8827____Item_PropertyInfo = 
{
	&IList_1_t8827_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49628_MethodInfo/* get */
	, &IList_1_set_Item_m49629_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8827_PropertyInfos[] =
{
	&IList_1_t8827____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
static ParameterInfo IList_1_t8827_IList_1_IndexOf_m49630_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureWrapMode_t1100_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49630_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49630_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8827_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8827_IList_1_IndexOf_m49630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49630_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextureWrapMode_t1100_0_0_0;
static ParameterInfo IList_1_t8827_IList_1_Insert_m49631_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextureWrapMode_t1100_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49631_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49631_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8827_IList_1_Insert_m49631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49631_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8827_IList_1_RemoveAt_m49632_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49632_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49632_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8827_IList_1_RemoveAt_m49632_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49632_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8827_IList_1_get_Item_m49628_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TextureWrapMode_t1100_0_0_0;
extern void* RuntimeInvoker_TextureWrapMode_t1100_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49628_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49628_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8827_il2cpp_TypeInfo/* declaring_type */
	, &TextureWrapMode_t1100_0_0_0/* return_type */
	, RuntimeInvoker_TextureWrapMode_t1100_Int32_t123/* invoker_method */
	, IList_1_t8827_IList_1_get_Item_m49628_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49628_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextureWrapMode_t1100_0_0_0;
static ParameterInfo IList_1_t8827_IList_1_set_Item_m49629_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextureWrapMode_t1100_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49629_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureWrapMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49629_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8827_IList_1_set_Item_m49629_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49629_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8827_MethodInfos[] =
{
	&IList_1_IndexOf_m49630_MethodInfo,
	&IList_1_Insert_m49631_MethodInfo,
	&IList_1_RemoveAt_m49632_MethodInfo,
	&IList_1_get_Item_m49628_MethodInfo,
	&IList_1_set_Item_m49629_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8827_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8826_il2cpp_TypeInfo,
	&IEnumerable_1_t8828_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8827_0_0_0;
extern Il2CppType IList_1_t8827_1_0_0;
struct IList_1_t8827;
extern Il2CppGenericClass IList_1_t8827_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8827_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8827_MethodInfos/* methods */
	, IList_1_t8827_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8827_il2cpp_TypeInfo/* element_class */
	, IList_1_t8827_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8827_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8827_0_0_0/* byval_arg */
	, &IList_1_t8827_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8827_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6954_il2cpp_TypeInfo;

// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextureFormat>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextureFormat>
extern MethodInfo IEnumerator_1_get_Current_m49633_MethodInfo;
static PropertyInfo IEnumerator_1_t6954____Current_PropertyInfo = 
{
	&IEnumerator_1_t6954_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6954_PropertyInfos[] =
{
	&IEnumerator_1_t6954____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextureFormat_t841_0_0_0;
extern void* RuntimeInvoker_TextureFormat_t841 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49633_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextureFormat>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49633_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6954_il2cpp_TypeInfo/* declaring_type */
	, &TextureFormat_t841_0_0_0/* return_type */
	, RuntimeInvoker_TextureFormat_t841/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49633_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6954_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49633_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6954_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6954_0_0_0;
extern Il2CppType IEnumerator_1_t6954_1_0_0;
struct IEnumerator_1_t6954;
extern Il2CppGenericClass IEnumerator_1_t6954_GenericClass;
TypeInfo IEnumerator_1_t6954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6954_MethodInfos/* methods */
	, IEnumerator_1_t6954_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6954_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6954_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6954_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6954_0_0_0/* byval_arg */
	, &IEnumerator_1_t6954_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_487.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4939_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_487MethodDeclarations.h"

extern TypeInfo TextureFormat_t841_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29863_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextureFormat_t841_m38995_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TextureFormat>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextureFormat>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTextureFormat_t841_m38995 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29859_MethodInfo;
 void InternalEnumerator_1__ctor_m29859 (InternalEnumerator_1_t4939 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860 (InternalEnumerator_1_t4939 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29863(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29863_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TextureFormat_t841_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29861_MethodInfo;
 void InternalEnumerator_1_Dispose_m29861 (InternalEnumerator_1_t4939 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29862_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29862 (InternalEnumerator_1_t4939 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29863 (InternalEnumerator_1_t4939 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTextureFormat_t841_m38995(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTextureFormat_t841_m38995_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4939____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4939, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4939____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4939, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4939_FieldInfos[] =
{
	&InternalEnumerator_1_t4939____array_0_FieldInfo,
	&InternalEnumerator_1_t4939____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4939____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4939_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4939____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4939_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29863_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4939_PropertyInfos[] =
{
	&InternalEnumerator_1_t4939____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4939____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4939_InternalEnumerator_1__ctor_m29859_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29859_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29859_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29859/* method */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4939_InternalEnumerator_1__ctor_m29859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29859_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860/* method */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29861_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29861_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29861/* method */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29861_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29862_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29862_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29862/* method */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29862_GenericMethod/* genericMethod */

};
extern Il2CppType TextureFormat_t841_0_0_0;
extern void* RuntimeInvoker_TextureFormat_t841 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29863_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29863_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29863/* method */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* declaring_type */
	, &TextureFormat_t841_0_0_0/* return_type */
	, RuntimeInvoker_TextureFormat_t841/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29863_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4939_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29859_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_MethodInfo,
	&InternalEnumerator_1_Dispose_m29861_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29862_MethodInfo,
	&InternalEnumerator_1_get_Current_m29863_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4939_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29862_MethodInfo,
	&InternalEnumerator_1_Dispose_m29861_MethodInfo,
	&InternalEnumerator_1_get_Current_m29863_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4939_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6954_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4939_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6954_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4939_0_0_0;
extern Il2CppType InternalEnumerator_1_t4939_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4939_GenericClass;
TypeInfo InternalEnumerator_1_t4939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4939_MethodInfos/* methods */
	, InternalEnumerator_1_t4939_PropertyInfos/* properties */
	, InternalEnumerator_1_t4939_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4939_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4939_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4939_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4939_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4939_1_0_0/* this_arg */
	, InternalEnumerator_1_t4939_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4939_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4939)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8829_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>
extern MethodInfo ICollection_1_get_Count_m49634_MethodInfo;
static PropertyInfo ICollection_1_t8829____Count_PropertyInfo = 
{
	&ICollection_1_t8829_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49634_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49635_MethodInfo;
static PropertyInfo ICollection_1_t8829____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8829_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49635_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8829_PropertyInfos[] =
{
	&ICollection_1_t8829____Count_PropertyInfo,
	&ICollection_1_t8829____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49634_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::get_Count()
MethodInfo ICollection_1_get_Count_m49634_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49634_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49635_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49635_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49635_GenericMethod/* genericMethod */

};
extern Il2CppType TextureFormat_t841_0_0_0;
extern Il2CppType TextureFormat_t841_0_0_0;
static ParameterInfo ICollection_1_t8829_ICollection_1_Add_m49636_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureFormat_t841_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49636_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Add(T)
MethodInfo ICollection_1_Add_m49636_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8829_ICollection_1_Add_m49636_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49636_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49637_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Clear()
MethodInfo ICollection_1_Clear_m49637_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49637_GenericMethod/* genericMethod */

};
extern Il2CppType TextureFormat_t841_0_0_0;
static ParameterInfo ICollection_1_t8829_ICollection_1_Contains_m49638_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureFormat_t841_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49638_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Contains(T)
MethodInfo ICollection_1_Contains_m49638_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8829_ICollection_1_Contains_m49638_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49638_GenericMethod/* genericMethod */

};
extern Il2CppType TextureFormatU5BU5D_t5748_0_0_0;
extern Il2CppType TextureFormatU5BU5D_t5748_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8829_ICollection_1_CopyTo_m49639_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextureFormatU5BU5D_t5748_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49639_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49639_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8829_ICollection_1_CopyTo_m49639_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49639_GenericMethod/* genericMethod */

};
extern Il2CppType TextureFormat_t841_0_0_0;
static ParameterInfo ICollection_1_t8829_ICollection_1_Remove_m49640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureFormat_t841_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49640_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextureFormat>::Remove(T)
MethodInfo ICollection_1_Remove_m49640_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8829_ICollection_1_Remove_m49640_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49640_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8829_MethodInfos[] =
{
	&ICollection_1_get_Count_m49634_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49635_MethodInfo,
	&ICollection_1_Add_m49636_MethodInfo,
	&ICollection_1_Clear_m49637_MethodInfo,
	&ICollection_1_Contains_m49638_MethodInfo,
	&ICollection_1_CopyTo_m49639_MethodInfo,
	&ICollection_1_Remove_m49640_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8831_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8829_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8831_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8829_0_0_0;
extern Il2CppType ICollection_1_t8829_1_0_0;
struct ICollection_1_t8829;
extern Il2CppGenericClass ICollection_1_t8829_GenericClass;
TypeInfo ICollection_1_t8829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8829_MethodInfos/* methods */
	, ICollection_1_t8829_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8829_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8829_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8829_0_0_0/* byval_arg */
	, &ICollection_1_t8829_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8829_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextureFormat>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextureFormat>
extern Il2CppType IEnumerator_1_t6954_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49641_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextureFormat>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49641_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8831_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6954_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49641_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8831_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49641_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8831_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8831_0_0_0;
extern Il2CppType IEnumerable_1_t8831_1_0_0;
struct IEnumerable_1_t8831;
extern Il2CppGenericClass IEnumerable_1_t8831_GenericClass;
TypeInfo IEnumerable_1_t8831_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8831_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8831_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8831_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8831_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8831_0_0_0/* byval_arg */
	, &IEnumerable_1_t8831_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8831_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8830_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextureFormat>
extern MethodInfo IList_1_get_Item_m49642_MethodInfo;
extern MethodInfo IList_1_set_Item_m49643_MethodInfo;
static PropertyInfo IList_1_t8830____Item_PropertyInfo = 
{
	&IList_1_t8830_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49642_MethodInfo/* get */
	, &IList_1_set_Item_m49643_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8830_PropertyInfos[] =
{
	&IList_1_t8830____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextureFormat_t841_0_0_0;
static ParameterInfo IList_1_t8830_IList_1_IndexOf_m49644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextureFormat_t841_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49644_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49644_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8830_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8830_IList_1_IndexOf_m49644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49644_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextureFormat_t841_0_0_0;
static ParameterInfo IList_1_t8830_IList_1_Insert_m49645_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextureFormat_t841_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49645_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49645_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8830_IList_1_Insert_m49645_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49645_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8830_IList_1_RemoveAt_m49646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49646_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49646_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8830_IList_1_RemoveAt_m49646_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49646_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8830_IList_1_get_Item_m49642_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TextureFormat_t841_0_0_0;
extern void* RuntimeInvoker_TextureFormat_t841_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49642_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49642_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8830_il2cpp_TypeInfo/* declaring_type */
	, &TextureFormat_t841_0_0_0/* return_type */
	, RuntimeInvoker_TextureFormat_t841_Int32_t123/* invoker_method */
	, IList_1_t8830_IList_1_get_Item_m49642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49642_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextureFormat_t841_0_0_0;
static ParameterInfo IList_1_t8830_IList_1_set_Item_m49643_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextureFormat_t841_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49643_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextureFormat>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49643_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8830_IList_1_set_Item_m49643_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49643_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8830_MethodInfos[] =
{
	&IList_1_IndexOf_m49644_MethodInfo,
	&IList_1_Insert_m49645_MethodInfo,
	&IList_1_RemoveAt_m49646_MethodInfo,
	&IList_1_get_Item_m49642_MethodInfo,
	&IList_1_set_Item_m49643_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8830_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8829_il2cpp_TypeInfo,
	&IEnumerable_1_t8831_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8830_0_0_0;
extern Il2CppType IList_1_t8830_1_0_0;
struct IList_1_t8830;
extern Il2CppGenericClass IList_1_t8830_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8830_MethodInfos/* methods */
	, IList_1_t8830_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8830_il2cpp_TypeInfo/* element_class */
	, IList_1_t8830_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8830_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8830_0_0_0/* byval_arg */
	, &IList_1_t8830_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8830_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6956_il2cpp_TypeInfo;

// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTextureFormat>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTextureFormat>
extern MethodInfo IEnumerator_1_get_Current_m49647_MethodInfo;
static PropertyInfo IEnumerator_1_t6956____Current_PropertyInfo = 
{
	&IEnumerator_1_t6956_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49647_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6956_PropertyInfos[] =
{
	&IEnumerator_1_t6956____Current_PropertyInfo,
	NULL
};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
extern void* RuntimeInvoker_RenderTextureFormat_t1101 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49647_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTextureFormat>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49647_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6956_il2cpp_TypeInfo/* declaring_type */
	, &RenderTextureFormat_t1101_0_0_0/* return_type */
	, RuntimeInvoker_RenderTextureFormat_t1101/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49647_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6956_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49647_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6956_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6956_0_0_0;
extern Il2CppType IEnumerator_1_t6956_1_0_0;
struct IEnumerator_1_t6956;
extern Il2CppGenericClass IEnumerator_1_t6956_GenericClass;
TypeInfo IEnumerator_1_t6956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6956_MethodInfos/* methods */
	, IEnumerator_1_t6956_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6956_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6956_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6956_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6956_0_0_0/* byval_arg */
	, &IEnumerator_1_t6956_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_488.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4940_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_488MethodDeclarations.h"

extern TypeInfo RenderTextureFormat_t1101_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29868_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderTextureFormat_t1101_m39006_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.RenderTextureFormat>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RenderTextureFormat>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisRenderTextureFormat_t1101_m39006 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29864_MethodInfo;
 void InternalEnumerator_1__ctor_m29864 (InternalEnumerator_1_t4940 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865 (InternalEnumerator_1_t4940 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29868(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29868_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&RenderTextureFormat_t1101_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29866_MethodInfo;
 void InternalEnumerator_1_Dispose_m29866 (InternalEnumerator_1_t4940 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29867_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29867 (InternalEnumerator_1_t4940 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29868 (InternalEnumerator_1_t4940 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisRenderTextureFormat_t1101_m39006(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisRenderTextureFormat_t1101_m39006_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4940____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4940, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4940____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4940, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4940_FieldInfos[] =
{
	&InternalEnumerator_1_t4940____array_0_FieldInfo,
	&InternalEnumerator_1_t4940____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4940____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4940_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4940____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4940_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29868_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4940_PropertyInfos[] =
{
	&InternalEnumerator_1_t4940____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4940____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4940_InternalEnumerator_1__ctor_m29864_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29864_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29864/* method */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4940_InternalEnumerator_1__ctor_m29864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29864_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865/* method */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29866_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29866_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29866/* method */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29866_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29867_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29867_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29867/* method */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29867_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
extern void* RuntimeInvoker_RenderTextureFormat_t1101 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29868_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.RenderTextureFormat>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29868_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29868/* method */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* declaring_type */
	, &RenderTextureFormat_t1101_0_0_0/* return_type */
	, RuntimeInvoker_RenderTextureFormat_t1101/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29868_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4940_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29864_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_MethodInfo,
	&InternalEnumerator_1_Dispose_m29866_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29867_MethodInfo,
	&InternalEnumerator_1_get_Current_m29868_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4940_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29865_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29867_MethodInfo,
	&InternalEnumerator_1_Dispose_m29866_MethodInfo,
	&InternalEnumerator_1_get_Current_m29868_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4940_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6956_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4940_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6956_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4940_0_0_0;
extern Il2CppType InternalEnumerator_1_t4940_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4940_GenericClass;
TypeInfo InternalEnumerator_1_t4940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4940_MethodInfos/* methods */
	, InternalEnumerator_1_t4940_PropertyInfos/* properties */
	, InternalEnumerator_1_t4940_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4940_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4940_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4940_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4940_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4940_1_0_0/* this_arg */
	, InternalEnumerator_1_t4940_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4940)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8832_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>
extern MethodInfo ICollection_1_get_Count_m49648_MethodInfo;
static PropertyInfo ICollection_1_t8832____Count_PropertyInfo = 
{
	&ICollection_1_t8832_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49649_MethodInfo;
static PropertyInfo ICollection_1_t8832____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8832_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8832_PropertyInfos[] =
{
	&ICollection_1_t8832____Count_PropertyInfo,
	&ICollection_1_t8832____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49648_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::get_Count()
MethodInfo ICollection_1_get_Count_m49648_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49648_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49649_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49649_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49649_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
static ParameterInfo ICollection_1_t8832_ICollection_1_Add_m49650_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormat_t1101_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49650_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Add(T)
MethodInfo ICollection_1_Add_m49650_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8832_ICollection_1_Add_m49650_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49650_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49651_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Clear()
MethodInfo ICollection_1_Clear_m49651_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49651_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
static ParameterInfo ICollection_1_t8832_ICollection_1_Contains_m49652_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormat_t1101_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49652_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Contains(T)
MethodInfo ICollection_1_Contains_m49652_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8832_ICollection_1_Contains_m49652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49652_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureFormatU5BU5D_t5749_0_0_0;
extern Il2CppType RenderTextureFormatU5BU5D_t5749_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8832_ICollection_1_CopyTo_m49653_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormatU5BU5D_t5749_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49653_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49653_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8832_ICollection_1_CopyTo_m49653_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49653_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
static ParameterInfo ICollection_1_t8832_ICollection_1_Remove_m49654_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormat_t1101_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49654_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureFormat>::Remove(T)
MethodInfo ICollection_1_Remove_m49654_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8832_ICollection_1_Remove_m49654_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49654_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8832_MethodInfos[] =
{
	&ICollection_1_get_Count_m49648_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49649_MethodInfo,
	&ICollection_1_Add_m49650_MethodInfo,
	&ICollection_1_Clear_m49651_MethodInfo,
	&ICollection_1_Contains_m49652_MethodInfo,
	&ICollection_1_CopyTo_m49653_MethodInfo,
	&ICollection_1_Remove_m49654_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8834_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8832_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8834_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8832_0_0_0;
extern Il2CppType ICollection_1_t8832_1_0_0;
struct ICollection_1_t8832;
extern Il2CppGenericClass ICollection_1_t8832_GenericClass;
TypeInfo ICollection_1_t8832_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8832_MethodInfos/* methods */
	, ICollection_1_t8832_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8832_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8832_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8832_0_0_0/* byval_arg */
	, &ICollection_1_t8832_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8832_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTextureFormat>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTextureFormat>
extern Il2CppType IEnumerator_1_t6956_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49655_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTextureFormat>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49655_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8834_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6956_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49655_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8834_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49655_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8834_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8834_0_0_0;
extern Il2CppType IEnumerable_1_t8834_1_0_0;
struct IEnumerable_1_t8834;
extern Il2CppGenericClass IEnumerable_1_t8834_GenericClass;
TypeInfo IEnumerable_1_t8834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8834_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8834_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8834_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8834_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8834_0_0_0/* byval_arg */
	, &IEnumerable_1_t8834_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8834_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8833_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>
extern MethodInfo IList_1_get_Item_m49656_MethodInfo;
extern MethodInfo IList_1_set_Item_m49657_MethodInfo;
static PropertyInfo IList_1_t8833____Item_PropertyInfo = 
{
	&IList_1_t8833_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49656_MethodInfo/* get */
	, &IList_1_set_Item_m49657_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8833_PropertyInfos[] =
{
	&IList_1_t8833____Item_PropertyInfo,
	NULL
};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
static ParameterInfo IList_1_t8833_IList_1_IndexOf_m49658_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormat_t1101_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49658_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49658_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8833_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8833_IList_1_IndexOf_m49658_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49658_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
static ParameterInfo IList_1_t8833_IList_1_Insert_m49659_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormat_t1101_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49659_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49659_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8833_IList_1_Insert_m49659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49659_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8833_IList_1_RemoveAt_m49660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49660_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49660_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8833_IList_1_RemoveAt_m49660_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49660_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8833_IList_1_get_Item_m49656_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
extern void* RuntimeInvoker_RenderTextureFormat_t1101_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49656_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49656_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8833_il2cpp_TypeInfo/* declaring_type */
	, &RenderTextureFormat_t1101_0_0_0/* return_type */
	, RuntimeInvoker_RenderTextureFormat_t1101_Int32_t123/* invoker_method */
	, IList_1_t8833_IList_1_get_Item_m49656_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49656_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RenderTextureFormat_t1101_0_0_0;
static ParameterInfo IList_1_t8833_IList_1_set_Item_m49657_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RenderTextureFormat_t1101_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49657_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureFormat>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49657_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8833_IList_1_set_Item_m49657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49657_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8833_MethodInfos[] =
{
	&IList_1_IndexOf_m49658_MethodInfo,
	&IList_1_Insert_m49659_MethodInfo,
	&IList_1_RemoveAt_m49660_MethodInfo,
	&IList_1_get_Item_m49656_MethodInfo,
	&IList_1_set_Item_m49657_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8833_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8832_il2cpp_TypeInfo,
	&IEnumerable_1_t8834_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8833_0_0_0;
extern Il2CppType IList_1_t8833_1_0_0;
struct IList_1_t8833;
extern Il2CppGenericClass IList_1_t8833_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8833_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8833_MethodInfos/* methods */
	, IList_1_t8833_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8833_il2cpp_TypeInfo/* element_class */
	, IList_1_t8833_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8833_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8833_0_0_0/* byval_arg */
	, &IList_1_t8833_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8833_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6958_il2cpp_TypeInfo;

// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTextureReadWrite>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTextureReadWrite>
extern MethodInfo IEnumerator_1_get_Current_m49661_MethodInfo;
static PropertyInfo IEnumerator_1_t6958____Current_PropertyInfo = 
{
	&IEnumerator_1_t6958_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6958_PropertyInfos[] =
{
	&IEnumerator_1_t6958____Current_PropertyInfo,
	NULL
};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
extern void* RuntimeInvoker_RenderTextureReadWrite_t1102 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49661_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.RenderTextureReadWrite>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49661_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6958_il2cpp_TypeInfo/* declaring_type */
	, &RenderTextureReadWrite_t1102_0_0_0/* return_type */
	, RuntimeInvoker_RenderTextureReadWrite_t1102/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49661_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6958_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49661_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6958_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6958_0_0_0;
extern Il2CppType IEnumerator_1_t6958_1_0_0;
struct IEnumerator_1_t6958;
extern Il2CppGenericClass IEnumerator_1_t6958_GenericClass;
TypeInfo IEnumerator_1_t6958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6958_MethodInfos/* methods */
	, IEnumerator_1_t6958_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6958_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6958_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6958_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6958_0_0_0/* byval_arg */
	, &IEnumerator_1_t6958_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6958_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_489.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4941_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_489MethodDeclarations.h"

extern TypeInfo RenderTextureReadWrite_t1102_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29873_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderTextureReadWrite_t1102_m39017_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.RenderTextureReadWrite>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RenderTextureReadWrite>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisRenderTextureReadWrite_t1102_m39017 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29869_MethodInfo;
 void InternalEnumerator_1__ctor_m29869 (InternalEnumerator_1_t4941 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870 (InternalEnumerator_1_t4941 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29873(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29873_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&RenderTextureReadWrite_t1102_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29871_MethodInfo;
 void InternalEnumerator_1_Dispose_m29871 (InternalEnumerator_1_t4941 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29872_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29872 (InternalEnumerator_1_t4941 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29873 (InternalEnumerator_1_t4941 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisRenderTextureReadWrite_t1102_m39017(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisRenderTextureReadWrite_t1102_m39017_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4941____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4941, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4941____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4941, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4941_FieldInfos[] =
{
	&InternalEnumerator_1_t4941____array_0_FieldInfo,
	&InternalEnumerator_1_t4941____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4941____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4941_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4941____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4941_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4941_PropertyInfos[] =
{
	&InternalEnumerator_1_t4941____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4941____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4941_InternalEnumerator_1__ctor_m29869_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29869_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29869_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29869/* method */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4941_InternalEnumerator_1__ctor_m29869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29869_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870/* method */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29871_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29871_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29871/* method */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29871_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29872_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29872_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29872/* method */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29872_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
extern void* RuntimeInvoker_RenderTextureReadWrite_t1102 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29873_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.RenderTextureReadWrite>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29873_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29873/* method */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* declaring_type */
	, &RenderTextureReadWrite_t1102_0_0_0/* return_type */
	, RuntimeInvoker_RenderTextureReadWrite_t1102/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29873_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4941_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29869_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_MethodInfo,
	&InternalEnumerator_1_Dispose_m29871_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29872_MethodInfo,
	&InternalEnumerator_1_get_Current_m29873_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4941_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29870_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29872_MethodInfo,
	&InternalEnumerator_1_Dispose_m29871_MethodInfo,
	&InternalEnumerator_1_get_Current_m29873_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4941_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6958_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4941_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6958_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4941_0_0_0;
extern Il2CppType InternalEnumerator_1_t4941_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4941_GenericClass;
TypeInfo InternalEnumerator_1_t4941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4941_MethodInfos/* methods */
	, InternalEnumerator_1_t4941_PropertyInfos/* properties */
	, InternalEnumerator_1_t4941_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4941_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4941_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4941_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4941_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4941_1_0_0/* this_arg */
	, InternalEnumerator_1_t4941_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4941)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8835_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>
extern MethodInfo ICollection_1_get_Count_m49662_MethodInfo;
static PropertyInfo ICollection_1_t8835____Count_PropertyInfo = 
{
	&ICollection_1_t8835_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49662_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49663_MethodInfo;
static PropertyInfo ICollection_1_t8835____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8835_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8835_PropertyInfos[] =
{
	&ICollection_1_t8835____Count_PropertyInfo,
	&ICollection_1_t8835____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49662_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::get_Count()
MethodInfo ICollection_1_get_Count_m49662_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49662_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49663_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49663_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49663_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
static ParameterInfo ICollection_1_t8835_ICollection_1_Add_m49664_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWrite_t1102_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49664_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Add(T)
MethodInfo ICollection_1_Add_m49664_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8835_ICollection_1_Add_m49664_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49664_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49665_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Clear()
MethodInfo ICollection_1_Clear_m49665_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49665_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
static ParameterInfo ICollection_1_t8835_ICollection_1_Contains_m49666_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWrite_t1102_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49666_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Contains(T)
MethodInfo ICollection_1_Contains_m49666_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8835_ICollection_1_Contains_m49666_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49666_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureReadWriteU5BU5D_t5750_0_0_0;
extern Il2CppType RenderTextureReadWriteU5BU5D_t5750_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8835_ICollection_1_CopyTo_m49667_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWriteU5BU5D_t5750_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49667_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49667_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8835_ICollection_1_CopyTo_m49667_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49667_GenericMethod/* genericMethod */

};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
static ParameterInfo ICollection_1_t8835_ICollection_1_Remove_m49668_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWrite_t1102_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49668_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RenderTextureReadWrite>::Remove(T)
MethodInfo ICollection_1_Remove_m49668_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8835_ICollection_1_Remove_m49668_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49668_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8835_MethodInfos[] =
{
	&ICollection_1_get_Count_m49662_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49663_MethodInfo,
	&ICollection_1_Add_m49664_MethodInfo,
	&ICollection_1_Clear_m49665_MethodInfo,
	&ICollection_1_Contains_m49666_MethodInfo,
	&ICollection_1_CopyTo_m49667_MethodInfo,
	&ICollection_1_Remove_m49668_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8837_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8835_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8837_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8835_0_0_0;
extern Il2CppType ICollection_1_t8835_1_0_0;
struct ICollection_1_t8835;
extern Il2CppGenericClass ICollection_1_t8835_GenericClass;
TypeInfo ICollection_1_t8835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8835_MethodInfos/* methods */
	, ICollection_1_t8835_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8835_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8835_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8835_0_0_0/* byval_arg */
	, &ICollection_1_t8835_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8835_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTextureReadWrite>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTextureReadWrite>
extern Il2CppType IEnumerator_1_t6958_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49669_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RenderTextureReadWrite>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49669_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8837_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6958_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49669_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8837_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49669_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8837_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8837_0_0_0;
extern Il2CppType IEnumerable_1_t8837_1_0_0;
struct IEnumerable_1_t8837;
extern Il2CppGenericClass IEnumerable_1_t8837_GenericClass;
TypeInfo IEnumerable_1_t8837_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8837_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8837_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8837_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8837_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8837_0_0_0/* byval_arg */
	, &IEnumerable_1_t8837_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8837_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8836_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>
extern MethodInfo IList_1_get_Item_m49670_MethodInfo;
extern MethodInfo IList_1_set_Item_m49671_MethodInfo;
static PropertyInfo IList_1_t8836____Item_PropertyInfo = 
{
	&IList_1_t8836_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49670_MethodInfo/* get */
	, &IList_1_set_Item_m49671_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8836_PropertyInfos[] =
{
	&IList_1_t8836____Item_PropertyInfo,
	NULL
};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
static ParameterInfo IList_1_t8836_IList_1_IndexOf_m49672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWrite_t1102_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49672_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49672_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8836_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8836_IList_1_IndexOf_m49672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49672_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
static ParameterInfo IList_1_t8836_IList_1_Insert_m49673_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWrite_t1102_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49673_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49673_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8836_IList_1_Insert_m49673_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49673_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8836_IList_1_RemoveAt_m49674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49674_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49674_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8836_IList_1_RemoveAt_m49674_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49674_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8836_IList_1_get_Item_m49670_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
extern void* RuntimeInvoker_RenderTextureReadWrite_t1102_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49670_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49670_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8836_il2cpp_TypeInfo/* declaring_type */
	, &RenderTextureReadWrite_t1102_0_0_0/* return_type */
	, RuntimeInvoker_RenderTextureReadWrite_t1102_Int32_t123/* invoker_method */
	, IList_1_t8836_IList_1_get_Item_m49670_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49670_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RenderTextureReadWrite_t1102_0_0_0;
static ParameterInfo IList_1_t8836_IList_1_set_Item_m49671_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RenderTextureReadWrite_t1102_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49671_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RenderTextureReadWrite>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49671_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8836_IList_1_set_Item_m49671_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49671_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8836_MethodInfos[] =
{
	&IList_1_IndexOf_m49672_MethodInfo,
	&IList_1_Insert_m49673_MethodInfo,
	&IList_1_RemoveAt_m49674_MethodInfo,
	&IList_1_get_Item_m49670_MethodInfo,
	&IList_1_set_Item_m49671_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8836_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8835_il2cpp_TypeInfo,
	&IEnumerable_1_t8837_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8836_0_0_0;
extern Il2CppType IList_1_t8836_1_0_0;
struct IList_1_t8836;
extern Il2CppGenericClass IList_1_t8836_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8836_MethodInfos/* methods */
	, IList_1_t8836_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8836_il2cpp_TypeInfo/* element_class */
	, IList_1_t8836_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8836_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8836_0_0_0/* byval_arg */
	, &IList_1_t8836_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8836_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6960_il2cpp_TypeInfo;

// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
extern MethodInfo IEnumerator_1_get_Current_m49675_MethodInfo;
static PropertyInfo IEnumerator_1_t6960____Current_PropertyInfo = 
{
	&IEnumerator_1_t6960_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49675_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6960_PropertyInfos[] =
{
	&IEnumerator_1_t6960____Current_PropertyInfo,
	NULL
};
extern Il2CppType HitInfo_t1108_0_0_0;
extern void* RuntimeInvoker_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49675_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49675_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6960_il2cpp_TypeInfo/* declaring_type */
	, &HitInfo_t1108_0_0_0/* return_type */
	, RuntimeInvoker_HitInfo_t1108/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49675_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6960_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49675_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6960_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6960_0_0_0;
extern Il2CppType IEnumerator_1_t6960_1_0_0;
struct IEnumerator_1_t6960;
extern Il2CppGenericClass IEnumerator_1_t6960_GenericClass;
TypeInfo IEnumerator_1_t6960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6960_MethodInfos/* methods */
	, IEnumerator_1_t6960_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6960_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6960_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6960_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6960_0_0_0/* byval_arg */
	, &IEnumerator_1_t6960_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6960_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_490.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4942_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_490MethodDeclarations.h"

extern TypeInfo HitInfo_t1108_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29878_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHitInfo_t1108_m39028_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
 HitInfo_t1108  Array_InternalArray__get_Item_TisHitInfo_t1108_m39028 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29874_MethodInfo;
 void InternalEnumerator_1__ctor_m29874 (InternalEnumerator_1_t4942 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875 (InternalEnumerator_1_t4942 * __this, MethodInfo* method){
	{
		HitInfo_t1108  L_0 = InternalEnumerator_1_get_Current_m29878(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29878_MethodInfo);
		HitInfo_t1108  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HitInfo_t1108_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29876_MethodInfo;
 void InternalEnumerator_1_Dispose_m29876 (InternalEnumerator_1_t4942 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29877_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29877 (InternalEnumerator_1_t4942 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
 HitInfo_t1108  InternalEnumerator_1_get_Current_m29878 (InternalEnumerator_1_t4942 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		HitInfo_t1108  L_8 = Array_InternalArray__get_Item_TisHitInfo_t1108_m39028(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHitInfo_t1108_m39028_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4942____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4942, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4942____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4942, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4942_FieldInfos[] =
{
	&InternalEnumerator_1_t4942____array_0_FieldInfo,
	&InternalEnumerator_1_t4942____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4942____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4942_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4942____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4942_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29878_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4942_PropertyInfos[] =
{
	&InternalEnumerator_1_t4942____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4942____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4942_InternalEnumerator_1__ctor_m29874_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29874_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29874_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29874/* method */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4942_InternalEnumerator_1__ctor_m29874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29874_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875/* method */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29876_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29876_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29876/* method */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29876_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29877_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29877_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29877/* method */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29877_GenericMethod/* genericMethod */

};
extern Il2CppType HitInfo_t1108_0_0_0;
extern void* RuntimeInvoker_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29878_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29878_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29878/* method */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* declaring_type */
	, &HitInfo_t1108_0_0_0/* return_type */
	, RuntimeInvoker_HitInfo_t1108/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29878_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4942_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29874_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_MethodInfo,
	&InternalEnumerator_1_Dispose_m29876_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29877_MethodInfo,
	&InternalEnumerator_1_get_Current_m29878_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4942_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29875_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29877_MethodInfo,
	&InternalEnumerator_1_Dispose_m29876_MethodInfo,
	&InternalEnumerator_1_get_Current_m29878_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4942_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6960_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4942_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6960_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4942_0_0_0;
extern Il2CppType InternalEnumerator_1_t4942_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4942_GenericClass;
TypeInfo InternalEnumerator_1_t4942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4942_MethodInfos/* methods */
	, InternalEnumerator_1_t4942_PropertyInfos/* properties */
	, InternalEnumerator_1_t4942_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4942_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4942_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4942_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4942_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4942_1_0_0/* this_arg */
	, InternalEnumerator_1_t4942_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4942)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8838_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>
extern MethodInfo ICollection_1_get_Count_m49676_MethodInfo;
static PropertyInfo ICollection_1_t8838____Count_PropertyInfo = 
{
	&ICollection_1_t8838_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49677_MethodInfo;
static PropertyInfo ICollection_1_t8838____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8838_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8838_PropertyInfos[] =
{
	&ICollection_1_t8838____Count_PropertyInfo,
	&ICollection_1_t8838____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49676_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m49676_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49676_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49677_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49677_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49677_GenericMethod/* genericMethod */

};
extern Il2CppType HitInfo_t1108_0_0_0;
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo ICollection_1_t8838_ICollection_1_Add_m49678_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49678_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Add(T)
MethodInfo ICollection_1_Add_m49678_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_HitInfo_t1108/* invoker_method */
	, ICollection_1_t8838_ICollection_1_Add_m49678_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49678_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49679_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Clear()
MethodInfo ICollection_1_Clear_m49679_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49679_GenericMethod/* genericMethod */

};
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo ICollection_1_t8838_ICollection_1_Contains_m49680_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49680_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m49680_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_HitInfo_t1108/* invoker_method */
	, ICollection_1_t8838_ICollection_1_Contains_m49680_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49680_GenericMethod/* genericMethod */

};
extern Il2CppType HitInfoU5BU5D_t1109_0_0_0;
extern Il2CppType HitInfoU5BU5D_t1109_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8838_ICollection_1_CopyTo_m49681_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HitInfoU5BU5D_t1109_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49681_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49681_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8838_ICollection_1_CopyTo_m49681_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49681_GenericMethod/* genericMethod */

};
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo ICollection_1_t8838_ICollection_1_Remove_m49682_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49682_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SendMouseEvents/HitInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m49682_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_HitInfo_t1108/* invoker_method */
	, ICollection_1_t8838_ICollection_1_Remove_m49682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49682_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8838_MethodInfos[] =
{
	&ICollection_1_get_Count_m49676_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49677_MethodInfo,
	&ICollection_1_Add_m49678_MethodInfo,
	&ICollection_1_Clear_m49679_MethodInfo,
	&ICollection_1_Contains_m49680_MethodInfo,
	&ICollection_1_CopyTo_m49681_MethodInfo,
	&ICollection_1_Remove_m49682_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8840_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8838_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8840_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8838_0_0_0;
extern Il2CppType ICollection_1_t8838_1_0_0;
struct ICollection_1_t8838;
extern Il2CppGenericClass ICollection_1_t8838_GenericClass;
TypeInfo ICollection_1_t8838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8838_MethodInfos/* methods */
	, ICollection_1_t8838_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8838_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8838_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8838_0_0_0/* byval_arg */
	, &ICollection_1_t8838_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8838_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SendMouseEvents/HitInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SendMouseEvents/HitInfo>
extern Il2CppType IEnumerator_1_t6960_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49683_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SendMouseEvents/HitInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49683_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8840_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6960_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49683_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8840_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49683_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8840_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8840_0_0_0;
extern Il2CppType IEnumerable_1_t8840_1_0_0;
struct IEnumerable_1_t8840;
extern Il2CppGenericClass IEnumerable_1_t8840_GenericClass;
TypeInfo IEnumerable_1_t8840_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8840_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8840_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8840_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8840_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8840_0_0_0/* byval_arg */
	, &IEnumerable_1_t8840_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8840_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8839_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>
extern MethodInfo IList_1_get_Item_m49684_MethodInfo;
extern MethodInfo IList_1_set_Item_m49685_MethodInfo;
static PropertyInfo IList_1_t8839____Item_PropertyInfo = 
{
	&IList_1_t8839_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49684_MethodInfo/* get */
	, &IList_1_set_Item_m49685_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8839_PropertyInfos[] =
{
	&IList_1_t8839____Item_PropertyInfo,
	NULL
};
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo IList_1_t8839_IList_1_IndexOf_m49686_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49686_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49686_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8839_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_HitInfo_t1108/* invoker_method */
	, IList_1_t8839_IList_1_IndexOf_m49686_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49686_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo IList_1_t8839_IList_1_Insert_m49687_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49687_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49687_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_HitInfo_t1108/* invoker_method */
	, IList_1_t8839_IList_1_Insert_m49687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49687_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8839_IList_1_RemoveAt_m49688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49688_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49688_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8839_IList_1_RemoveAt_m49688_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49688_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8839_IList_1_get_Item_m49684_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HitInfo_t1108_0_0_0;
extern void* RuntimeInvoker_HitInfo_t1108_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49684_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49684_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8839_il2cpp_TypeInfo/* declaring_type */
	, &HitInfo_t1108_0_0_0/* return_type */
	, RuntimeInvoker_HitInfo_t1108_Int32_t123/* invoker_method */
	, IList_1_t8839_IList_1_get_Item_m49684_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49684_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HitInfo_t1108_0_0_0;
static ParameterInfo IList_1_t8839_IList_1_set_Item_m49685_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HitInfo_t1108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_HitInfo_t1108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49685_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SendMouseEvents/HitInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49685_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_HitInfo_t1108/* invoker_method */
	, IList_1_t8839_IList_1_set_Item_m49685_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49685_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8839_MethodInfos[] =
{
	&IList_1_IndexOf_m49686_MethodInfo,
	&IList_1_Insert_m49687_MethodInfo,
	&IList_1_RemoveAt_m49688_MethodInfo,
	&IList_1_get_Item_m49684_MethodInfo,
	&IList_1_set_Item_m49685_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8839_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8838_il2cpp_TypeInfo,
	&IEnumerable_1_t8840_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8839_0_0_0;
extern Il2CppType IList_1_t8839_1_0_0;
struct IList_1_t8839;
extern Il2CppGenericClass IList_1_t8839_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8839_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8839_MethodInfos/* methods */
	, IList_1_t8839_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8839_il2cpp_TypeInfo/* element_class */
	, IList_1_t8839_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8839_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8839_0_0_0/* byval_arg */
	, &IList_1_t8839_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8839_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.GUILayer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_36.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t4943_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.GUILayer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_36MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.GUILayer>
extern Il2CppType GUILayer_t991_0_0_6;
FieldInfo CastHelper_1_t4943____t_0_FieldInfo = 
{
	"t"/* name */
	, &GUILayer_t991_0_0_6/* type */
	, &CastHelper_1_t4943_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t4943, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t4943____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t4943_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t4943, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t4943_FieldInfos[] =
{
	&CastHelper_1_t4943____t_0_FieldInfo,
	&CastHelper_1_t4943____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t4943_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t4943_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t4943_0_0_0;
extern Il2CppType CastHelper_1_t4943_1_0_0;
extern Il2CppGenericClass CastHelper_1_t4943_GenericClass;
TypeInfo CastHelper_1_t4943_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t4943_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t4943_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t4943_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t4943_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t4943_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t4943_0_0_0/* byval_arg */
	, &CastHelper_1_t4943_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t4943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t4943)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6962_il2cpp_TypeInfo;

// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.UserState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.UserState>
extern MethodInfo IEnumerator_1_get_Current_m49689_MethodInfo;
static PropertyInfo IEnumerator_1_t6962____Current_PropertyInfo = 
{
	&IEnumerator_1_t6962_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49689_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6962_PropertyInfos[] =
{
	&IEnumerator_1_t6962____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserState_t1111_0_0_0;
extern void* RuntimeInvoker_UserState_t1111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49689_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.UserState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49689_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6962_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1111_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49689_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6962_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49689_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6962_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6962_0_0_0;
extern Il2CppType IEnumerator_1_t6962_1_0_0;
struct IEnumerator_1_t6962;
extern Il2CppGenericClass IEnumerator_1_t6962_GenericClass;
TypeInfo IEnumerator_1_t6962_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6962_MethodInfos/* methods */
	, IEnumerator_1_t6962_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6962_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6962_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6962_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6962_0_0_0/* byval_arg */
	, &IEnumerator_1_t6962_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6962_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_491.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4944_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_491MethodDeclarations.h"

extern TypeInfo UserState_t1111_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29883_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserState_t1111_m39039_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.UserState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.UserState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisUserState_t1111_m39039 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29879_MethodInfo;
 void InternalEnumerator_1__ctor_m29879 (InternalEnumerator_1_t4944 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880 (InternalEnumerator_1_t4944 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29883(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29883_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UserState_t1111_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29881_MethodInfo;
 void InternalEnumerator_1_Dispose_m29881 (InternalEnumerator_1_t4944 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29882_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29882 (InternalEnumerator_1_t4944 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29883 (InternalEnumerator_1_t4944 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisUserState_t1111_m39039(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUserState_t1111_m39039_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4944____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4944, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4944____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4944, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4944_FieldInfos[] =
{
	&InternalEnumerator_1_t4944____array_0_FieldInfo,
	&InternalEnumerator_1_t4944____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4944____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4944_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4944____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4944_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29883_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4944_PropertyInfos[] =
{
	&InternalEnumerator_1_t4944____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4944____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4944_InternalEnumerator_1__ctor_m29879_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29879_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29879_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29879/* method */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4944_InternalEnumerator_1__ctor_m29879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29879_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880/* method */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29881_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29881_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29881/* method */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29881_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29882_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29882_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29882/* method */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29882_GenericMethod/* genericMethod */

};
extern Il2CppType UserState_t1111_0_0_0;
extern void* RuntimeInvoker_UserState_t1111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29883_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29883_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29883/* method */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1111_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29883_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4944_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29879_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_MethodInfo,
	&InternalEnumerator_1_Dispose_m29881_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29882_MethodInfo,
	&InternalEnumerator_1_get_Current_m29883_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4944_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29880_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29882_MethodInfo,
	&InternalEnumerator_1_Dispose_m29881_MethodInfo,
	&InternalEnumerator_1_get_Current_m29883_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4944_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6962_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4944_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6962_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4944_0_0_0;
extern Il2CppType InternalEnumerator_1_t4944_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4944_GenericClass;
TypeInfo InternalEnumerator_1_t4944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4944_MethodInfos/* methods */
	, InternalEnumerator_1_t4944_PropertyInfos/* properties */
	, InternalEnumerator_1_t4944_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4944_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4944_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4944_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4944_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4944_1_0_0/* this_arg */
	, InternalEnumerator_1_t4944_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4944)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8841_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>
extern MethodInfo ICollection_1_get_Count_m49690_MethodInfo;
static PropertyInfo ICollection_1_t8841____Count_PropertyInfo = 
{
	&ICollection_1_t8841_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49690_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49691_MethodInfo;
static PropertyInfo ICollection_1_t8841____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8841_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49691_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8841_PropertyInfos[] =
{
	&ICollection_1_t8841____Count_PropertyInfo,
	&ICollection_1_t8841____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49690_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::get_Count()
MethodInfo ICollection_1_get_Count_m49690_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49690_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49691_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49691_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49691_GenericMethod/* genericMethod */

};
extern Il2CppType UserState_t1111_0_0_0;
extern Il2CppType UserState_t1111_0_0_0;
static ParameterInfo ICollection_1_t8841_ICollection_1_Add_m49692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49692_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Add(T)
MethodInfo ICollection_1_Add_m49692_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8841_ICollection_1_Add_m49692_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49692_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49693_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Clear()
MethodInfo ICollection_1_Clear_m49693_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49693_GenericMethod/* genericMethod */

};
extern Il2CppType UserState_t1111_0_0_0;
static ParameterInfo ICollection_1_t8841_ICollection_1_Contains_m49694_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49694_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Contains(T)
MethodInfo ICollection_1_Contains_m49694_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8841_ICollection_1_Contains_m49694_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49694_GenericMethod/* genericMethod */

};
extern Il2CppType UserStateU5BU5D_t5751_0_0_0;
extern Il2CppType UserStateU5BU5D_t5751_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8841_ICollection_1_CopyTo_m49695_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserStateU5BU5D_t5751_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49695_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49695_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8841_ICollection_1_CopyTo_m49695_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49695_GenericMethod/* genericMethod */

};
extern Il2CppType UserState_t1111_0_0_0;
static ParameterInfo ICollection_1_t8841_ICollection_1_Remove_m49696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49696_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserState>::Remove(T)
MethodInfo ICollection_1_Remove_m49696_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8841_ICollection_1_Remove_m49696_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49696_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8841_MethodInfos[] =
{
	&ICollection_1_get_Count_m49690_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49691_MethodInfo,
	&ICollection_1_Add_m49692_MethodInfo,
	&ICollection_1_Clear_m49693_MethodInfo,
	&ICollection_1_Contains_m49694_MethodInfo,
	&ICollection_1_CopyTo_m49695_MethodInfo,
	&ICollection_1_Remove_m49696_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8843_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8841_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8843_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8841_0_0_0;
extern Il2CppType ICollection_1_t8841_1_0_0;
struct ICollection_1_t8841;
extern Il2CppGenericClass ICollection_1_t8841_GenericClass;
TypeInfo ICollection_1_t8841_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8841_MethodInfos/* methods */
	, ICollection_1_t8841_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8841_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8841_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8841_0_0_0/* byval_arg */
	, &ICollection_1_t8841_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8841_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.UserState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.UserState>
extern Il2CppType IEnumerator_1_t6962_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49697_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.UserState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49697_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8843_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6962_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49697_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8843_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49697_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8843_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8843_0_0_0;
extern Il2CppType IEnumerable_1_t8843_1_0_0;
struct IEnumerable_1_t8843;
extern Il2CppGenericClass IEnumerable_1_t8843_GenericClass;
TypeInfo IEnumerable_1_t8843_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8843_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8843_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8843_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8843_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8843_0_0_0/* byval_arg */
	, &IEnumerable_1_t8843_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8843_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8842_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>
extern MethodInfo IList_1_get_Item_m49698_MethodInfo;
extern MethodInfo IList_1_set_Item_m49699_MethodInfo;
static PropertyInfo IList_1_t8842____Item_PropertyInfo = 
{
	&IList_1_t8842_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49698_MethodInfo/* get */
	, &IList_1_set_Item_m49699_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8842_PropertyInfos[] =
{
	&IList_1_t8842____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserState_t1111_0_0_0;
static ParameterInfo IList_1_t8842_IList_1_IndexOf_m49700_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49700_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49700_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8842_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8842_IList_1_IndexOf_m49700_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49700_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserState_t1111_0_0_0;
static ParameterInfo IList_1_t8842_IList_1_Insert_m49701_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49701_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49701_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8842_IList_1_Insert_m49701_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49701_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8842_IList_1_RemoveAt_m49702_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49702_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49702_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8842_IList_1_RemoveAt_m49702_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49702_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8842_IList_1_get_Item_m49698_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UserState_t1111_0_0_0;
extern void* RuntimeInvoker_UserState_t1111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49698_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49698_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8842_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1111_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1111_Int32_t123/* invoker_method */
	, IList_1_t8842_IList_1_get_Item_m49698_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49698_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserState_t1111_0_0_0;
static ParameterInfo IList_1_t8842_IList_1_set_Item_m49699_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserState_t1111_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49699_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49699_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8842_IList_1_set_Item_m49699_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49699_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8842_MethodInfos[] =
{
	&IList_1_IndexOf_m49700_MethodInfo,
	&IList_1_Insert_m49701_MethodInfo,
	&IList_1_RemoveAt_m49702_MethodInfo,
	&IList_1_get_Item_m49698_MethodInfo,
	&IList_1_set_Item_m49699_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8842_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8841_il2cpp_TypeInfo,
	&IEnumerable_1_t8843_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8842_0_0_0;
extern Il2CppType IList_1_t8842_1_0_0;
struct IList_1_t8842;
extern Il2CppGenericClass IList_1_t8842_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8842_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8842_MethodInfos/* methods */
	, IList_1_t8842_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8842_il2cpp_TypeInfo/* element_class */
	, IList_1_t8842_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8842_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8842_0_0_0/* byval_arg */
	, &IList_1_t8842_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8842_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6964_il2cpp_TypeInfo;

// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.UserScope>
extern MethodInfo IEnumerator_1_get_Current_m49703_MethodInfo;
static PropertyInfo IEnumerator_1_t6964____Current_PropertyInfo = 
{
	&IEnumerator_1_t6964_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49703_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6964_PropertyInfos[] =
{
	&IEnumerator_1_t6964____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserScope_t1112_0_0_0;
extern void* RuntimeInvoker_UserScope_t1112 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49703_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49703_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6964_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1112_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1112/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49703_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6964_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49703_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6964_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6964_0_0_0;
extern Il2CppType IEnumerator_1_t6964_1_0_0;
struct IEnumerator_1_t6964;
extern Il2CppGenericClass IEnumerator_1_t6964_GenericClass;
TypeInfo IEnumerator_1_t6964_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6964_MethodInfos/* methods */
	, IEnumerator_1_t6964_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6964_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6964_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6964_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6964_0_0_0/* byval_arg */
	, &IEnumerator_1_t6964_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6964_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_492.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4945_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_492MethodDeclarations.h"

extern TypeInfo UserScope_t1112_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29888_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserScope_t1112_m39050_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.UserScope>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.UserScope>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisUserScope_t1112_m39050 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29884_MethodInfo;
 void InternalEnumerator_1__ctor_m29884 (InternalEnumerator_1_t4945 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885 (InternalEnumerator_1_t4945 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29888(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29888_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UserScope_t1112_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29886_MethodInfo;
 void InternalEnumerator_1_Dispose_m29886 (InternalEnumerator_1_t4945 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29887_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29887 (InternalEnumerator_1_t4945 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29888 (InternalEnumerator_1_t4945 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisUserScope_t1112_m39050(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUserScope_t1112_m39050_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4945____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4945, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4945____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4945, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4945_FieldInfos[] =
{
	&InternalEnumerator_1_t4945____array_0_FieldInfo,
	&InternalEnumerator_1_t4945____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4945____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4945_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4945____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4945_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29888_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4945_PropertyInfos[] =
{
	&InternalEnumerator_1_t4945____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4945____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4945_InternalEnumerator_1__ctor_m29884_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29884_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29884_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29884/* method */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4945_InternalEnumerator_1__ctor_m29884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29884_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885/* method */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29886_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29886_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29886/* method */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29886_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29887_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29887_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29887/* method */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29887_GenericMethod/* genericMethod */

};
extern Il2CppType UserScope_t1112_0_0_0;
extern void* RuntimeInvoker_UserScope_t1112 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29888_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29888_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29888/* method */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1112_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1112/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29888_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4945_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29884_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_MethodInfo,
	&InternalEnumerator_1_Dispose_m29886_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29887_MethodInfo,
	&InternalEnumerator_1_get_Current_m29888_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4945_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29885_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29887_MethodInfo,
	&InternalEnumerator_1_Dispose_m29886_MethodInfo,
	&InternalEnumerator_1_get_Current_m29888_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4945_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6964_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4945_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6964_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4945_0_0_0;
extern Il2CppType InternalEnumerator_1_t4945_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4945_GenericClass;
TypeInfo InternalEnumerator_1_t4945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4945_MethodInfos/* methods */
	, InternalEnumerator_1_t4945_PropertyInfos/* properties */
	, InternalEnumerator_1_t4945_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4945_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4945_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4945_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4945_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4945_1_0_0/* this_arg */
	, InternalEnumerator_1_t4945_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4945)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8844_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>
extern MethodInfo ICollection_1_get_Count_m49704_MethodInfo;
static PropertyInfo ICollection_1_t8844____Count_PropertyInfo = 
{
	&ICollection_1_t8844_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49705_MethodInfo;
static PropertyInfo ICollection_1_t8844____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8844_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49705_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8844_PropertyInfos[] =
{
	&ICollection_1_t8844____Count_PropertyInfo,
	&ICollection_1_t8844____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49704_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::get_Count()
MethodInfo ICollection_1_get_Count_m49704_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49704_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49705_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49705_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49705_GenericMethod/* genericMethod */

};
extern Il2CppType UserScope_t1112_0_0_0;
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo ICollection_1_t8844_ICollection_1_Add_m49706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49706_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Add(T)
MethodInfo ICollection_1_Add_m49706_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8844_ICollection_1_Add_m49706_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49706_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49707_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Clear()
MethodInfo ICollection_1_Clear_m49707_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49707_GenericMethod/* genericMethod */

};
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo ICollection_1_t8844_ICollection_1_Contains_m49708_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49708_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Contains(T)
MethodInfo ICollection_1_Contains_m49708_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8844_ICollection_1_Contains_m49708_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49708_GenericMethod/* genericMethod */

};
extern Il2CppType UserScopeU5BU5D_t5752_0_0_0;
extern Il2CppType UserScopeU5BU5D_t5752_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8844_ICollection_1_CopyTo_m49709_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserScopeU5BU5D_t5752_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49709_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49709_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8844_ICollection_1_CopyTo_m49709_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49709_GenericMethod/* genericMethod */

};
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo ICollection_1_t8844_ICollection_1_Remove_m49710_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49710_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.UserScope>::Remove(T)
MethodInfo ICollection_1_Remove_m49710_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8844_ICollection_1_Remove_m49710_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49710_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8844_MethodInfos[] =
{
	&ICollection_1_get_Count_m49704_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49705_MethodInfo,
	&ICollection_1_Add_m49706_MethodInfo,
	&ICollection_1_Clear_m49707_MethodInfo,
	&ICollection_1_Contains_m49708_MethodInfo,
	&ICollection_1_CopyTo_m49709_MethodInfo,
	&ICollection_1_Remove_m49710_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8846_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8844_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8846_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8844_0_0_0;
extern Il2CppType ICollection_1_t8844_1_0_0;
struct ICollection_1_t8844;
extern Il2CppGenericClass ICollection_1_t8844_GenericClass;
TypeInfo ICollection_1_t8844_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8844_MethodInfos/* methods */
	, ICollection_1_t8844_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8844_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8844_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8844_0_0_0/* byval_arg */
	, &ICollection_1_t8844_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8844_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.UserScope>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.UserScope>
extern Il2CppType IEnumerator_1_t6964_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49711_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.UserScope>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49711_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8846_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6964_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49711_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8846_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49711_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8846_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8846_0_0_0;
extern Il2CppType IEnumerable_1_t8846_1_0_0;
struct IEnumerable_1_t8846;
extern Il2CppGenericClass IEnumerable_1_t8846_GenericClass;
TypeInfo IEnumerable_1_t8846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8846_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8846_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8846_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8846_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8846_0_0_0/* byval_arg */
	, &IEnumerable_1_t8846_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8846_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8845_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>
extern MethodInfo IList_1_get_Item_m49712_MethodInfo;
extern MethodInfo IList_1_set_Item_m49713_MethodInfo;
static PropertyInfo IList_1_t8845____Item_PropertyInfo = 
{
	&IList_1_t8845_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49712_MethodInfo/* get */
	, &IList_1_set_Item_m49713_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8845_PropertyInfos[] =
{
	&IList_1_t8845____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo IList_1_t8845_IList_1_IndexOf_m49714_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49714_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49714_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8845_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8845_IList_1_IndexOf_m49714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49714_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo IList_1_t8845_IList_1_Insert_m49715_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49715_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49715_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8845_IList_1_Insert_m49715_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49715_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8845_IList_1_RemoveAt_m49716_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49716_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49716_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8845_IList_1_RemoveAt_m49716_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49716_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8845_IList_1_get_Item_m49712_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UserScope_t1112_0_0_0;
extern void* RuntimeInvoker_UserScope_t1112_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49712_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49712_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8845_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1112_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1112_Int32_t123/* invoker_method */
	, IList_1_t8845_IList_1_get_Item_m49712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49712_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserScope_t1112_0_0_0;
static ParameterInfo IList_1_t8845_IList_1_set_Item_m49713_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserScope_t1112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49713_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.UserScope>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49713_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8845_IList_1_set_Item_m49713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49713_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8845_MethodInfos[] =
{
	&IList_1_IndexOf_m49714_MethodInfo,
	&IList_1_Insert_m49715_MethodInfo,
	&IList_1_RemoveAt_m49716_MethodInfo,
	&IList_1_get_Item_m49712_MethodInfo,
	&IList_1_set_Item_m49713_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8845_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8844_il2cpp_TypeInfo,
	&IEnumerable_1_t8846_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8845_0_0_0;
extern Il2CppType IList_1_t8845_1_0_0;
struct IList_1_t8845;
extern Il2CppGenericClass IList_1_t8845_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8845_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8845_MethodInfos/* methods */
	, IList_1_t8845_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8845_il2cpp_TypeInfo/* element_class */
	, IList_1_t8845_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8845_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8845_0_0_0/* byval_arg */
	, &IList_1_t8845_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8845_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6966_il2cpp_TypeInfo;

// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>
extern MethodInfo IEnumerator_1_get_Current_m49717_MethodInfo;
static PropertyInfo IEnumerator_1_t6966____Current_PropertyInfo = 
{
	&IEnumerator_1_t6966_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49717_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6966_PropertyInfos[] =
{
	&IEnumerator_1_t6966____Current_PropertyInfo,
	NULL
};
extern Il2CppType TimeScope_t1113_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1113 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49717_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49717_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6966_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1113_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1113/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49717_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6966_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49717_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6966_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6966_0_0_0;
extern Il2CppType IEnumerator_1_t6966_1_0_0;
struct IEnumerator_1_t6966;
extern Il2CppGenericClass IEnumerator_1_t6966_GenericClass;
TypeInfo IEnumerator_1_t6966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6966_MethodInfos/* methods */
	, IEnumerator_1_t6966_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6966_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6966_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6966_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6966_0_0_0/* byval_arg */
	, &IEnumerator_1_t6966_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6966_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_493.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4946_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_493MethodDeclarations.h"

extern TypeInfo TimeScope_t1113_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29893_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTimeScope_t1113_m39061_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.TimeScope>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.TimeScope>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTimeScope_t1113_m39061 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29889_MethodInfo;
 void InternalEnumerator_1__ctor_m29889 (InternalEnumerator_1_t4946 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890 (InternalEnumerator_1_t4946 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29893(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29893_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TimeScope_t1113_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29891_MethodInfo;
 void InternalEnumerator_1_Dispose_m29891 (InternalEnumerator_1_t4946 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29892_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29892 (InternalEnumerator_1_t4946 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29893 (InternalEnumerator_1_t4946 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTimeScope_t1113_m39061(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTimeScope_t1113_m39061_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4946____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4946, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4946____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4946, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4946_FieldInfos[] =
{
	&InternalEnumerator_1_t4946____array_0_FieldInfo,
	&InternalEnumerator_1_t4946____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4946____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4946_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4946____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4946_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29893_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4946_PropertyInfos[] =
{
	&InternalEnumerator_1_t4946____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4946____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4946_InternalEnumerator_1__ctor_m29889_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29889_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29889_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29889/* method */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4946_InternalEnumerator_1__ctor_m29889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29889_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890/* method */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29891_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29891_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29891/* method */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29891_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29892_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29892_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29892/* method */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29892_GenericMethod/* genericMethod */

};
extern Il2CppType TimeScope_t1113_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1113 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29893_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29893_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29893/* method */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1113_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1113/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29893_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4946_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29889_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_MethodInfo,
	&InternalEnumerator_1_Dispose_m29891_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29892_MethodInfo,
	&InternalEnumerator_1_get_Current_m29893_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4946_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29890_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29892_MethodInfo,
	&InternalEnumerator_1_Dispose_m29891_MethodInfo,
	&InternalEnumerator_1_get_Current_m29893_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4946_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6966_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4946_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6966_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4946_0_0_0;
extern Il2CppType InternalEnumerator_1_t4946_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4946_GenericClass;
TypeInfo InternalEnumerator_1_t4946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4946_MethodInfos/* methods */
	, InternalEnumerator_1_t4946_PropertyInfos/* properties */
	, InternalEnumerator_1_t4946_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4946_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4946_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4946_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4946_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4946_1_0_0/* this_arg */
	, InternalEnumerator_1_t4946_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4946)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8847_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>
extern MethodInfo ICollection_1_get_Count_m49718_MethodInfo;
static PropertyInfo ICollection_1_t8847____Count_PropertyInfo = 
{
	&ICollection_1_t8847_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49719_MethodInfo;
static PropertyInfo ICollection_1_t8847____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8847_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8847_PropertyInfos[] =
{
	&ICollection_1_t8847____Count_PropertyInfo,
	&ICollection_1_t8847____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49718_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::get_Count()
MethodInfo ICollection_1_get_Count_m49718_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49718_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49719_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49719_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49719_GenericMethod/* genericMethod */

};
extern Il2CppType TimeScope_t1113_0_0_0;
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo ICollection_1_t8847_ICollection_1_Add_m49720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49720_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Add(T)
MethodInfo ICollection_1_Add_m49720_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8847_ICollection_1_Add_m49720_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49720_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49721_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Clear()
MethodInfo ICollection_1_Clear_m49721_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49721_GenericMethod/* genericMethod */

};
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo ICollection_1_t8847_ICollection_1_Contains_m49722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49722_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Contains(T)
MethodInfo ICollection_1_Contains_m49722_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8847_ICollection_1_Contains_m49722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49722_GenericMethod/* genericMethod */

};
extern Il2CppType TimeScopeU5BU5D_t5753_0_0_0;
extern Il2CppType TimeScopeU5BU5D_t5753_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8847_ICollection_1_CopyTo_m49723_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TimeScopeU5BU5D_t5753_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49723_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49723_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8847_ICollection_1_CopyTo_m49723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49723_GenericMethod/* genericMethod */

};
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo ICollection_1_t8847_ICollection_1_Remove_m49724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49724_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.TimeScope>::Remove(T)
MethodInfo ICollection_1_Remove_m49724_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8847_ICollection_1_Remove_m49724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49724_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8847_MethodInfos[] =
{
	&ICollection_1_get_Count_m49718_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49719_MethodInfo,
	&ICollection_1_Add_m49720_MethodInfo,
	&ICollection_1_Clear_m49721_MethodInfo,
	&ICollection_1_Contains_m49722_MethodInfo,
	&ICollection_1_CopyTo_m49723_MethodInfo,
	&ICollection_1_Remove_m49724_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8849_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8847_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8849_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8847_0_0_0;
extern Il2CppType ICollection_1_t8847_1_0_0;
struct ICollection_1_t8847;
extern Il2CppGenericClass ICollection_1_t8847_GenericClass;
TypeInfo ICollection_1_t8847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8847_MethodInfos/* methods */
	, ICollection_1_t8847_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8847_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8847_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8847_0_0_0/* byval_arg */
	, &ICollection_1_t8847_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8847_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.TimeScope>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.TimeScope>
extern Il2CppType IEnumerator_1_t6966_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49725_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.TimeScope>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49725_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8849_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6966_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49725_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8849_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49725_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8849_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8849_0_0_0;
extern Il2CppType IEnumerable_1_t8849_1_0_0;
struct IEnumerable_1_t8849;
extern Il2CppGenericClass IEnumerable_1_t8849_GenericClass;
TypeInfo IEnumerable_1_t8849_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8849_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8849_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8849_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8849_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8849_0_0_0/* byval_arg */
	, &IEnumerable_1_t8849_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8849_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8848_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>
extern MethodInfo IList_1_get_Item_m49726_MethodInfo;
extern MethodInfo IList_1_set_Item_m49727_MethodInfo;
static PropertyInfo IList_1_t8848____Item_PropertyInfo = 
{
	&IList_1_t8848_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49726_MethodInfo/* get */
	, &IList_1_set_Item_m49727_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8848_PropertyInfos[] =
{
	&IList_1_t8848____Item_PropertyInfo,
	NULL
};
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo IList_1_t8848_IList_1_IndexOf_m49728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49728_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49728_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8848_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8848_IList_1_IndexOf_m49728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49728_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo IList_1_t8848_IList_1_Insert_m49729_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49729_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49729_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8848_IList_1_Insert_m49729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49729_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8848_IList_1_RemoveAt_m49730_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49730_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49730_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8848_IList_1_RemoveAt_m49730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49730_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8848_IList_1_get_Item_m49726_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TimeScope_t1113_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1113_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49726_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49726_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8848_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1113_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1113_Int32_t123/* invoker_method */
	, IList_1_t8848_IList_1_get_Item_m49726_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49726_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TimeScope_t1113_0_0_0;
static ParameterInfo IList_1_t8848_IList_1_set_Item_m49727_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TimeScope_t1113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49727_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.TimeScope>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49727_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8848_IList_1_set_Item_m49727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49727_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8848_MethodInfos[] =
{
	&IList_1_IndexOf_m49728_MethodInfo,
	&IList_1_Insert_m49729_MethodInfo,
	&IList_1_RemoveAt_m49730_MethodInfo,
	&IList_1_get_Item_m49726_MethodInfo,
	&IList_1_set_Item_m49727_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8848_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8847_il2cpp_TypeInfo,
	&IEnumerable_1_t8849_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8848_0_0_0;
extern Il2CppType IList_1_t8848_1_0_0;
struct IList_1_t8848;
extern Il2CppGenericClass IList_1_t8848_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8848_MethodInfos/* methods */
	, IList_1_t8848_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8848_il2cpp_TypeInfo/* element_class */
	, IList_1_t8848_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8848_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8848_0_0_0/* byval_arg */
	, &IList_1_t8848_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8848_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6968_il2cpp_TypeInfo;

// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.PropertyAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.PropertyAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49731_MethodInfo;
static PropertyInfo IEnumerator_1_t6968____Current_PropertyInfo = 
{
	&IEnumerator_1_t6968_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6968_PropertyInfos[] =
{
	&IEnumerator_1_t6968____Current_PropertyInfo,
	NULL
};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49731_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.PropertyAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49731_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6968_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttribute_t1114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49731_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6968_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49731_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6968_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6968_0_0_0;
extern Il2CppType IEnumerator_1_t6968_1_0_0;
struct IEnumerator_1_t6968;
extern Il2CppGenericClass IEnumerator_1_t6968_GenericClass;
TypeInfo IEnumerator_1_t6968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6968_MethodInfos/* methods */
	, IEnumerator_1_t6968_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6968_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6968_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6968_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6968_0_0_0/* byval_arg */
	, &IEnumerator_1_t6968_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6968_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_494.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4947_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_494MethodDeclarations.h"

extern TypeInfo PropertyAttribute_t1114_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29898_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPropertyAttribute_t1114_m39072_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.PropertyAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.PropertyAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisPropertyAttribute_t1114_m39072(__this, p0, method) (PropertyAttribute_t1114 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4947____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4947, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4947____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4947, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4947_FieldInfos[] =
{
	&InternalEnumerator_1_t4947____array_0_FieldInfo,
	&InternalEnumerator_1_t4947____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4947____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4947_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4947____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4947_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29898_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4947_PropertyInfos[] =
{
	&InternalEnumerator_1_t4947____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4947____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4947_InternalEnumerator_1__ctor_m29894_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29894_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29894_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4947_InternalEnumerator_1__ctor_m29894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29894_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29896_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29896_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29896_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29897_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29897_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29897_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29898_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.PropertyAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29898_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttribute_t1114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29898_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4947_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29894_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_MethodInfo,
	&InternalEnumerator_1_Dispose_m29896_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29897_MethodInfo,
	&InternalEnumerator_1_get_Current_m29898_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29897_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29896_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4947_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29895_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29897_MethodInfo,
	&InternalEnumerator_1_Dispose_m29896_MethodInfo,
	&InternalEnumerator_1_get_Current_m29898_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4947_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6968_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4947_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6968_il2cpp_TypeInfo, 7},
};
extern TypeInfo PropertyAttribute_t1114_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4947_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29898_MethodInfo/* Method Usage */,
	&PropertyAttribute_t1114_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPropertyAttribute_t1114_m39072_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4947_0_0_0;
extern Il2CppType InternalEnumerator_1_t4947_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4947_GenericClass;
TypeInfo InternalEnumerator_1_t4947_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4947_MethodInfos/* methods */
	, InternalEnumerator_1_t4947_PropertyInfos/* properties */
	, InternalEnumerator_1_t4947_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4947_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4947_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4947_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4947_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4947_1_0_0/* this_arg */
	, InternalEnumerator_1_t4947_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4947_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4947_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4947)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8850_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>
extern MethodInfo ICollection_1_get_Count_m49732_MethodInfo;
static PropertyInfo ICollection_1_t8850____Count_PropertyInfo = 
{
	&ICollection_1_t8850_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49733_MethodInfo;
static PropertyInfo ICollection_1_t8850____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8850_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8850_PropertyInfos[] =
{
	&ICollection_1_t8850____Count_PropertyInfo,
	&ICollection_1_t8850____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49732_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49732_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49732_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49733_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49733_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49733_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
extern Il2CppType PropertyAttribute_t1114_0_0_0;
static ParameterInfo ICollection_1_t8850_ICollection_1_Add_m49734_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttribute_t1114_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49734_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49734_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8850_ICollection_1_Add_m49734_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49734_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49735_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49735_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49735_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
static ParameterInfo ICollection_1_t8850_ICollection_1_Contains_m49736_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttribute_t1114_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49736_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49736_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8850_ICollection_1_Contains_m49736_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49736_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttributeU5BU5D_t5754_0_0_0;
extern Il2CppType PropertyAttributeU5BU5D_t5754_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8850_ICollection_1_CopyTo_m49737_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttributeU5BU5D_t5754_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49737_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49737_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8850_ICollection_1_CopyTo_m49737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49737_GenericMethod/* genericMethod */

};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
static ParameterInfo ICollection_1_t8850_ICollection_1_Remove_m49738_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttribute_t1114_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49738_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PropertyAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49738_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8850_ICollection_1_Remove_m49738_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49738_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8850_MethodInfos[] =
{
	&ICollection_1_get_Count_m49732_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49733_MethodInfo,
	&ICollection_1_Add_m49734_MethodInfo,
	&ICollection_1_Clear_m49735_MethodInfo,
	&ICollection_1_Contains_m49736_MethodInfo,
	&ICollection_1_CopyTo_m49737_MethodInfo,
	&ICollection_1_Remove_m49738_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8852_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8850_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8852_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8850_0_0_0;
extern Il2CppType ICollection_1_t8850_1_0_0;
struct ICollection_1_t8850;
extern Il2CppGenericClass ICollection_1_t8850_GenericClass;
TypeInfo ICollection_1_t8850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8850_MethodInfos/* methods */
	, ICollection_1_t8850_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8850_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8850_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8850_0_0_0/* byval_arg */
	, &ICollection_1_t8850_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8850_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.PropertyAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.PropertyAttribute>
extern Il2CppType IEnumerator_1_t6968_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49739_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.PropertyAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49739_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8852_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6968_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49739_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8852_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49739_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8852_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8852_0_0_0;
extern Il2CppType IEnumerable_1_t8852_1_0_0;
struct IEnumerable_1_t8852;
extern Il2CppGenericClass IEnumerable_1_t8852_GenericClass;
TypeInfo IEnumerable_1_t8852_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8852_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8852_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8852_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8852_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8852_0_0_0/* byval_arg */
	, &IEnumerable_1_t8852_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8852_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8851_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>
extern MethodInfo IList_1_get_Item_m49740_MethodInfo;
extern MethodInfo IList_1_set_Item_m49741_MethodInfo;
static PropertyInfo IList_1_t8851____Item_PropertyInfo = 
{
	&IList_1_t8851_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49740_MethodInfo/* get */
	, &IList_1_set_Item_m49741_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8851_PropertyInfos[] =
{
	&IList_1_t8851____Item_PropertyInfo,
	NULL
};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
static ParameterInfo IList_1_t8851_IList_1_IndexOf_m49742_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropertyAttribute_t1114_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49742_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49742_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8851_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8851_IList_1_IndexOf_m49742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49742_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropertyAttribute_t1114_0_0_0;
static ParameterInfo IList_1_t8851_IList_1_Insert_m49743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PropertyAttribute_t1114_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49743_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49743_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8851_IList_1_Insert_m49743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49743_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8851_IList_1_RemoveAt_m49744_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49744_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49744_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8851_IList_1_RemoveAt_m49744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49744_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8851_IList_1_get_Item_m49740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PropertyAttribute_t1114_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49740_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49740_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8851_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttribute_t1114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8851_IList_1_get_Item_m49740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49740_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropertyAttribute_t1114_0_0_0;
static ParameterInfo IList_1_t8851_IList_1_set_Item_m49741_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PropertyAttribute_t1114_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49741_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.PropertyAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49741_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8851_IList_1_set_Item_m49741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49741_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8851_MethodInfos[] =
{
	&IList_1_IndexOf_m49742_MethodInfo,
	&IList_1_Insert_m49743_MethodInfo,
	&IList_1_RemoveAt_m49744_MethodInfo,
	&IList_1_get_Item_m49740_MethodInfo,
	&IList_1_set_Item_m49741_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8851_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8850_il2cpp_TypeInfo,
	&IEnumerable_1_t8852_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8851_0_0_0;
extern Il2CppType IList_1_t8851_1_0_0;
struct IList_1_t8851;
extern Il2CppGenericClass IList_1_t8851_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8851_MethodInfos/* methods */
	, IList_1_t8851_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8851_il2cpp_TypeInfo/* element_class */
	, IList_1_t8851_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8851_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8851_0_0_0/* byval_arg */
	, &IList_1_t8851_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8851_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6970_il2cpp_TypeInfo;

// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TooltipAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TooltipAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49745_MethodInfo;
static PropertyInfo IEnumerator_1_t6970____Current_PropertyInfo = 
{
	&IEnumerator_1_t6970_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49745_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6970_PropertyInfos[] =
{
	&IEnumerator_1_t6970____Current_PropertyInfo,
	NULL
};
extern Il2CppType TooltipAttribute_t544_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49745_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TooltipAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49745_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6970_il2cpp_TypeInfo/* declaring_type */
	, &TooltipAttribute_t544_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49745_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6970_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49745_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6970_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6970_0_0_0;
extern Il2CppType IEnumerator_1_t6970_1_0_0;
struct IEnumerator_1_t6970;
extern Il2CppGenericClass IEnumerator_1_t6970_GenericClass;
TypeInfo IEnumerator_1_t6970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6970_MethodInfos/* methods */
	, IEnumerator_1_t6970_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6970_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6970_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6970_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6970_0_0_0/* byval_arg */
	, &IEnumerator_1_t6970_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6970_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_495.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4948_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_495MethodDeclarations.h"

extern TypeInfo TooltipAttribute_t544_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29903_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTooltipAttribute_t544_m39083_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TooltipAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TooltipAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTooltipAttribute_t544_m39083(__this, p0, method) (TooltipAttribute_t544 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4948____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4948, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4948____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4948, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4948_FieldInfos[] =
{
	&InternalEnumerator_1_t4948____array_0_FieldInfo,
	&InternalEnumerator_1_t4948____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4948____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4948_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4948____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4948_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29903_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4948_PropertyInfos[] =
{
	&InternalEnumerator_1_t4948____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4948____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4948_InternalEnumerator_1__ctor_m29899_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29899_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29899_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4948_InternalEnumerator_1__ctor_m29899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29899_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29901_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29901_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29901_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29902_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29902_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29902_GenericMethod/* genericMethod */

};
extern Il2CppType TooltipAttribute_t544_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29903_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TooltipAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29903_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* declaring_type */
	, &TooltipAttribute_t544_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29903_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4948_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29899_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_MethodInfo,
	&InternalEnumerator_1_Dispose_m29901_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29902_MethodInfo,
	&InternalEnumerator_1_get_Current_m29903_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29902_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29901_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4948_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29900_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29902_MethodInfo,
	&InternalEnumerator_1_Dispose_m29901_MethodInfo,
	&InternalEnumerator_1_get_Current_m29903_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4948_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6970_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4948_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6970_il2cpp_TypeInfo, 7},
};
extern TypeInfo TooltipAttribute_t544_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4948_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29903_MethodInfo/* Method Usage */,
	&TooltipAttribute_t544_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTooltipAttribute_t544_m39083_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4948_0_0_0;
extern Il2CppType InternalEnumerator_1_t4948_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4948_GenericClass;
TypeInfo InternalEnumerator_1_t4948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4948_MethodInfos/* methods */
	, InternalEnumerator_1_t4948_PropertyInfos/* properties */
	, InternalEnumerator_1_t4948_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4948_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4948_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4948_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4948_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4948_1_0_0/* this_arg */
	, InternalEnumerator_1_t4948_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4948_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4948)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8853_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>
extern MethodInfo ICollection_1_get_Count_m49746_MethodInfo;
static PropertyInfo ICollection_1_t8853____Count_PropertyInfo = 
{
	&ICollection_1_t8853_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49746_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49747_MethodInfo;
static PropertyInfo ICollection_1_t8853____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8853_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49747_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8853_PropertyInfos[] =
{
	&ICollection_1_t8853____Count_PropertyInfo,
	&ICollection_1_t8853____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49746_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49746_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49746_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49747_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49747_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49747_GenericMethod/* genericMethod */

};
extern Il2CppType TooltipAttribute_t544_0_0_0;
extern Il2CppType TooltipAttribute_t544_0_0_0;
static ParameterInfo ICollection_1_t8853_ICollection_1_Add_m49748_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TooltipAttribute_t544_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49748_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49748_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8853_ICollection_1_Add_m49748_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49748_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49749_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49749_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49749_GenericMethod/* genericMethod */

};
extern Il2CppType TooltipAttribute_t544_0_0_0;
static ParameterInfo ICollection_1_t8853_ICollection_1_Contains_m49750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TooltipAttribute_t544_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49750_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49750_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8853_ICollection_1_Contains_m49750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49750_GenericMethod/* genericMethod */

};
extern Il2CppType TooltipAttributeU5BU5D_t5755_0_0_0;
extern Il2CppType TooltipAttributeU5BU5D_t5755_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8853_ICollection_1_CopyTo_m49751_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TooltipAttributeU5BU5D_t5755_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49751_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49751_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8853_ICollection_1_CopyTo_m49751_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49751_GenericMethod/* genericMethod */

};
extern Il2CppType TooltipAttribute_t544_0_0_0;
static ParameterInfo ICollection_1_t8853_ICollection_1_Remove_m49752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TooltipAttribute_t544_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49752_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TooltipAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49752_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8853_ICollection_1_Remove_m49752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49752_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8853_MethodInfos[] =
{
	&ICollection_1_get_Count_m49746_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49747_MethodInfo,
	&ICollection_1_Add_m49748_MethodInfo,
	&ICollection_1_Clear_m49749_MethodInfo,
	&ICollection_1_Contains_m49750_MethodInfo,
	&ICollection_1_CopyTo_m49751_MethodInfo,
	&ICollection_1_Remove_m49752_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8855_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8853_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8855_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8853_0_0_0;
extern Il2CppType ICollection_1_t8853_1_0_0;
struct ICollection_1_t8853;
extern Il2CppGenericClass ICollection_1_t8853_GenericClass;
TypeInfo ICollection_1_t8853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8853_MethodInfos/* methods */
	, ICollection_1_t8853_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8853_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8853_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8853_0_0_0/* byval_arg */
	, &ICollection_1_t8853_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8853_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TooltipAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TooltipAttribute>
extern Il2CppType IEnumerator_1_t6970_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49753_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TooltipAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49753_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8855_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6970_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49753_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8855_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49753_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8855_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8855_0_0_0;
extern Il2CppType IEnumerable_1_t8855_1_0_0;
struct IEnumerable_1_t8855;
extern Il2CppGenericClass IEnumerable_1_t8855_GenericClass;
TypeInfo IEnumerable_1_t8855_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8855_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8855_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8855_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8855_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8855_0_0_0/* byval_arg */
	, &IEnumerable_1_t8855_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8855_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8854_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>
extern MethodInfo IList_1_get_Item_m49754_MethodInfo;
extern MethodInfo IList_1_set_Item_m49755_MethodInfo;
static PropertyInfo IList_1_t8854____Item_PropertyInfo = 
{
	&IList_1_t8854_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49754_MethodInfo/* get */
	, &IList_1_set_Item_m49755_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8854_PropertyInfos[] =
{
	&IList_1_t8854____Item_PropertyInfo,
	NULL
};
extern Il2CppType TooltipAttribute_t544_0_0_0;
static ParameterInfo IList_1_t8854_IList_1_IndexOf_m49756_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TooltipAttribute_t544_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49756_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49756_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8854_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8854_IList_1_IndexOf_m49756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49756_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TooltipAttribute_t544_0_0_0;
static ParameterInfo IList_1_t8854_IList_1_Insert_m49757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TooltipAttribute_t544_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49757_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49757_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8854_IList_1_Insert_m49757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49757_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8854_IList_1_RemoveAt_m49758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49758_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49758_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8854_IList_1_RemoveAt_m49758_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49758_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8854_IList_1_get_Item_m49754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TooltipAttribute_t544_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49754_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49754_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8854_il2cpp_TypeInfo/* declaring_type */
	, &TooltipAttribute_t544_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8854_IList_1_get_Item_m49754_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49754_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TooltipAttribute_t544_0_0_0;
static ParameterInfo IList_1_t8854_IList_1_set_Item_m49755_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TooltipAttribute_t544_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49755_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TooltipAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49755_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8854_IList_1_set_Item_m49755_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49755_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8854_MethodInfos[] =
{
	&IList_1_IndexOf_m49756_MethodInfo,
	&IList_1_Insert_m49757_MethodInfo,
	&IList_1_RemoveAt_m49758_MethodInfo,
	&IList_1_get_Item_m49754_MethodInfo,
	&IList_1_set_Item_m49755_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8854_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8853_il2cpp_TypeInfo,
	&IEnumerable_1_t8855_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8854_0_0_0;
extern Il2CppType IList_1_t8854_1_0_0;
struct IList_1_t8854;
extern Il2CppGenericClass IList_1_t8854_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8854_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8854_MethodInfos/* methods */
	, IList_1_t8854_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8854_il2cpp_TypeInfo/* element_class */
	, IList_1_t8854_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8854_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8854_0_0_0/* byval_arg */
	, &IList_1_t8854_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8854_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6972_il2cpp_TypeInfo;

// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SpaceAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SpaceAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49759_MethodInfo;
static PropertyInfo IEnumerator_1_t6972____Current_PropertyInfo = 
{
	&IEnumerator_1_t6972_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6972_PropertyInfos[] =
{
	&IEnumerator_1_t6972____Current_PropertyInfo,
	NULL
};
extern Il2CppType SpaceAttribute_t540_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49759_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SpaceAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49759_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6972_il2cpp_TypeInfo/* declaring_type */
	, &SpaceAttribute_t540_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49759_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6972_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49759_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6972_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6972_0_0_0;
extern Il2CppType IEnumerator_1_t6972_1_0_0;
struct IEnumerator_1_t6972;
extern Il2CppGenericClass IEnumerator_1_t6972_GenericClass;
TypeInfo IEnumerator_1_t6972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6972_MethodInfos/* methods */
	, IEnumerator_1_t6972_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6972_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6972_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6972_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6972_0_0_0/* byval_arg */
	, &IEnumerator_1_t6972_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6972_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_496.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4949_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_496MethodDeclarations.h"

extern TypeInfo SpaceAttribute_t540_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29908_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSpaceAttribute_t540_m39094_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SpaceAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SpaceAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSpaceAttribute_t540_m39094(__this, p0, method) (SpaceAttribute_t540 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4949____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4949, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4949____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4949, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4949_FieldInfos[] =
{
	&InternalEnumerator_1_t4949____array_0_FieldInfo,
	&InternalEnumerator_1_t4949____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4949____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4949_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4949____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4949_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4949_PropertyInfos[] =
{
	&InternalEnumerator_1_t4949____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4949____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4949_InternalEnumerator_1__ctor_m29904_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29904_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29904_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4949_InternalEnumerator_1__ctor_m29904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29904_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29906_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29906_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29906_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29907_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29907_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29907_GenericMethod/* genericMethod */

};
extern Il2CppType SpaceAttribute_t540_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29908_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SpaceAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29908_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* declaring_type */
	, &SpaceAttribute_t540_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29908_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4949_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29904_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_MethodInfo,
	&InternalEnumerator_1_Dispose_m29906_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29907_MethodInfo,
	&InternalEnumerator_1_get_Current_m29908_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29907_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29906_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4949_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29905_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29907_MethodInfo,
	&InternalEnumerator_1_Dispose_m29906_MethodInfo,
	&InternalEnumerator_1_get_Current_m29908_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4949_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6972_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4949_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6972_il2cpp_TypeInfo, 7},
};
extern TypeInfo SpaceAttribute_t540_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4949_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29908_MethodInfo/* Method Usage */,
	&SpaceAttribute_t540_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSpaceAttribute_t540_m39094_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4949_0_0_0;
extern Il2CppType InternalEnumerator_1_t4949_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4949_GenericClass;
TypeInfo InternalEnumerator_1_t4949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4949_MethodInfos/* methods */
	, InternalEnumerator_1_t4949_PropertyInfos/* properties */
	, InternalEnumerator_1_t4949_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4949_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4949_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4949_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4949_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4949_1_0_0/* this_arg */
	, InternalEnumerator_1_t4949_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4949_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4949_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4949)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8856_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>
extern MethodInfo ICollection_1_get_Count_m49760_MethodInfo;
static PropertyInfo ICollection_1_t8856____Count_PropertyInfo = 
{
	&ICollection_1_t8856_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49761_MethodInfo;
static PropertyInfo ICollection_1_t8856____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8856_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8856_PropertyInfos[] =
{
	&ICollection_1_t8856____Count_PropertyInfo,
	&ICollection_1_t8856____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49760_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49760_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49760_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49761_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49761_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49761_GenericMethod/* genericMethod */

};
extern Il2CppType SpaceAttribute_t540_0_0_0;
extern Il2CppType SpaceAttribute_t540_0_0_0;
static ParameterInfo ICollection_1_t8856_ICollection_1_Add_m49762_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpaceAttribute_t540_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49762_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49762_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8856_ICollection_1_Add_m49762_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49762_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49763_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49763_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49763_GenericMethod/* genericMethod */

};
extern Il2CppType SpaceAttribute_t540_0_0_0;
static ParameterInfo ICollection_1_t8856_ICollection_1_Contains_m49764_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpaceAttribute_t540_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49764_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49764_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8856_ICollection_1_Contains_m49764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49764_GenericMethod/* genericMethod */

};
extern Il2CppType SpaceAttributeU5BU5D_t5756_0_0_0;
extern Il2CppType SpaceAttributeU5BU5D_t5756_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8856_ICollection_1_CopyTo_m49765_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SpaceAttributeU5BU5D_t5756_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49765_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49765_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8856_ICollection_1_CopyTo_m49765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49765_GenericMethod/* genericMethod */

};
extern Il2CppType SpaceAttribute_t540_0_0_0;
static ParameterInfo ICollection_1_t8856_ICollection_1_Remove_m49766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpaceAttribute_t540_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49766_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpaceAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49766_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8856_ICollection_1_Remove_m49766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49766_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8856_MethodInfos[] =
{
	&ICollection_1_get_Count_m49760_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49761_MethodInfo,
	&ICollection_1_Add_m49762_MethodInfo,
	&ICollection_1_Clear_m49763_MethodInfo,
	&ICollection_1_Contains_m49764_MethodInfo,
	&ICollection_1_CopyTo_m49765_MethodInfo,
	&ICollection_1_Remove_m49766_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8858_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8856_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8858_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8856_0_0_0;
extern Il2CppType ICollection_1_t8856_1_0_0;
struct ICollection_1_t8856;
extern Il2CppGenericClass ICollection_1_t8856_GenericClass;
TypeInfo ICollection_1_t8856_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8856_MethodInfos/* methods */
	, ICollection_1_t8856_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8856_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8856_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8856_0_0_0/* byval_arg */
	, &ICollection_1_t8856_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8856_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SpaceAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SpaceAttribute>
extern Il2CppType IEnumerator_1_t6972_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49767_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SpaceAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49767_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8858_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6972_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49767_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8858_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49767_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8858_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8858_0_0_0;
extern Il2CppType IEnumerable_1_t8858_1_0_0;
struct IEnumerable_1_t8858;
extern Il2CppGenericClass IEnumerable_1_t8858_GenericClass;
TypeInfo IEnumerable_1_t8858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8858_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8858_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8858_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8858_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8858_0_0_0/* byval_arg */
	, &IEnumerable_1_t8858_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8858_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8857_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>
extern MethodInfo IList_1_get_Item_m49768_MethodInfo;
extern MethodInfo IList_1_set_Item_m49769_MethodInfo;
static PropertyInfo IList_1_t8857____Item_PropertyInfo = 
{
	&IList_1_t8857_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49768_MethodInfo/* get */
	, &IList_1_set_Item_m49769_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8857_PropertyInfos[] =
{
	&IList_1_t8857____Item_PropertyInfo,
	NULL
};
extern Il2CppType SpaceAttribute_t540_0_0_0;
static ParameterInfo IList_1_t8857_IList_1_IndexOf_m49770_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpaceAttribute_t540_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49770_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49770_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8857_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8857_IList_1_IndexOf_m49770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49770_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpaceAttribute_t540_0_0_0;
static ParameterInfo IList_1_t8857_IList_1_Insert_m49771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SpaceAttribute_t540_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49771_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49771_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8857_IList_1_Insert_m49771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49771_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8857_IList_1_RemoveAt_m49772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49772_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49772_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8857_IList_1_RemoveAt_m49772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49772_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8857_IList_1_get_Item_m49768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SpaceAttribute_t540_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49768_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49768_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8857_il2cpp_TypeInfo/* declaring_type */
	, &SpaceAttribute_t540_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8857_IList_1_get_Item_m49768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49768_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpaceAttribute_t540_0_0_0;
static ParameterInfo IList_1_t8857_IList_1_set_Item_m49769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SpaceAttribute_t540_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49769_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpaceAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49769_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8857_IList_1_set_Item_m49769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49769_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8857_MethodInfos[] =
{
	&IList_1_IndexOf_m49770_MethodInfo,
	&IList_1_Insert_m49771_MethodInfo,
	&IList_1_RemoveAt_m49772_MethodInfo,
	&IList_1_get_Item_m49768_MethodInfo,
	&IList_1_set_Item_m49769_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8857_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8856_il2cpp_TypeInfo,
	&IEnumerable_1_t8858_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8857_0_0_0;
extern Il2CppType IList_1_t8857_1_0_0;
struct IList_1_t8857;
extern Il2CppGenericClass IList_1_t8857_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8857_MethodInfos/* methods */
	, IList_1_t8857_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8857_il2cpp_TypeInfo/* element_class */
	, IList_1_t8857_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8857_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8857_0_0_0/* byval_arg */
	, &IList_1_t8857_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8857_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6974_il2cpp_TypeInfo;

// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.RangeAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RangeAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49773_MethodInfo;
static PropertyInfo IEnumerator_1_t6974____Current_PropertyInfo = 
{
	&IEnumerator_1_t6974_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49773_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6974_PropertyInfos[] =
{
	&IEnumerator_1_t6974____Current_PropertyInfo,
	NULL
};
extern Il2CppType RangeAttribute_t501_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49773_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.RangeAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49773_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6974_il2cpp_TypeInfo/* declaring_type */
	, &RangeAttribute_t501_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49773_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6974_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49773_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6974_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6974_0_0_0;
extern Il2CppType IEnumerator_1_t6974_1_0_0;
struct IEnumerator_1_t6974;
extern Il2CppGenericClass IEnumerator_1_t6974_GenericClass;
TypeInfo IEnumerator_1_t6974_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6974_MethodInfos/* methods */
	, IEnumerator_1_t6974_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6974_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6974_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6974_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6974_0_0_0/* byval_arg */
	, &IEnumerator_1_t6974_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6974_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_497.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4950_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_497MethodDeclarations.h"

extern TypeInfo RangeAttribute_t501_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29913_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRangeAttribute_t501_m39105_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.RangeAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RangeAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisRangeAttribute_t501_m39105(__this, p0, method) (RangeAttribute_t501 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4950____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4950, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4950____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4950, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4950_FieldInfos[] =
{
	&InternalEnumerator_1_t4950____array_0_FieldInfo,
	&InternalEnumerator_1_t4950____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4950____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4950_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4950____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4950_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29913_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4950_PropertyInfos[] =
{
	&InternalEnumerator_1_t4950____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4950____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4950_InternalEnumerator_1__ctor_m29909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29909_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29909_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4950_InternalEnumerator_1__ctor_m29909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29909_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29911_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29911_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29911_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29912_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29912_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29912_GenericMethod/* genericMethod */

};
extern Il2CppType RangeAttribute_t501_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29913_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.RangeAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29913_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* declaring_type */
	, &RangeAttribute_t501_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29913_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4950_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29909_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_MethodInfo,
	&InternalEnumerator_1_Dispose_m29911_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29912_MethodInfo,
	&InternalEnumerator_1_get_Current_m29913_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29912_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29911_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4950_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29910_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29912_MethodInfo,
	&InternalEnumerator_1_Dispose_m29911_MethodInfo,
	&InternalEnumerator_1_get_Current_m29913_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4950_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6974_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4950_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6974_il2cpp_TypeInfo, 7},
};
extern TypeInfo RangeAttribute_t501_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4950_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29913_MethodInfo/* Method Usage */,
	&RangeAttribute_t501_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRangeAttribute_t501_m39105_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4950_0_0_0;
extern Il2CppType InternalEnumerator_1_t4950_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4950_GenericClass;
TypeInfo InternalEnumerator_1_t4950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4950_MethodInfos/* methods */
	, InternalEnumerator_1_t4950_PropertyInfos/* properties */
	, InternalEnumerator_1_t4950_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4950_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4950_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4950_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4950_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4950_1_0_0/* this_arg */
	, InternalEnumerator_1_t4950_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4950_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4950)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8859_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>
extern MethodInfo ICollection_1_get_Count_m49774_MethodInfo;
static PropertyInfo ICollection_1_t8859____Count_PropertyInfo = 
{
	&ICollection_1_t8859_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49775_MethodInfo;
static PropertyInfo ICollection_1_t8859____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8859_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8859_PropertyInfos[] =
{
	&ICollection_1_t8859____Count_PropertyInfo,
	&ICollection_1_t8859____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49774_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49774_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49774_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49775_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49775_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49775_GenericMethod/* genericMethod */

};
extern Il2CppType RangeAttribute_t501_0_0_0;
extern Il2CppType RangeAttribute_t501_0_0_0;
static ParameterInfo ICollection_1_t8859_ICollection_1_Add_m49776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RangeAttribute_t501_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49776_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49776_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8859_ICollection_1_Add_m49776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49776_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49777_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49777_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49777_GenericMethod/* genericMethod */

};
extern Il2CppType RangeAttribute_t501_0_0_0;
static ParameterInfo ICollection_1_t8859_ICollection_1_Contains_m49778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RangeAttribute_t501_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49778_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49778_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8859_ICollection_1_Contains_m49778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49778_GenericMethod/* genericMethod */

};
extern Il2CppType RangeAttributeU5BU5D_t5757_0_0_0;
extern Il2CppType RangeAttributeU5BU5D_t5757_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8859_ICollection_1_CopyTo_m49779_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RangeAttributeU5BU5D_t5757_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49779_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49779_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8859_ICollection_1_CopyTo_m49779_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49779_GenericMethod/* genericMethod */

};
extern Il2CppType RangeAttribute_t501_0_0_0;
static ParameterInfo ICollection_1_t8859_ICollection_1_Remove_m49780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RangeAttribute_t501_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49780_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RangeAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49780_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8859_ICollection_1_Remove_m49780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49780_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8859_MethodInfos[] =
{
	&ICollection_1_get_Count_m49774_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49775_MethodInfo,
	&ICollection_1_Add_m49776_MethodInfo,
	&ICollection_1_Clear_m49777_MethodInfo,
	&ICollection_1_Contains_m49778_MethodInfo,
	&ICollection_1_CopyTo_m49779_MethodInfo,
	&ICollection_1_Remove_m49780_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8861_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8859_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8861_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8859_0_0_0;
extern Il2CppType ICollection_1_t8859_1_0_0;
struct ICollection_1_t8859;
extern Il2CppGenericClass ICollection_1_t8859_GenericClass;
TypeInfo ICollection_1_t8859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8859_MethodInfos/* methods */
	, ICollection_1_t8859_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8859_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8859_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8859_0_0_0/* byval_arg */
	, &ICollection_1_t8859_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8859_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RangeAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RangeAttribute>
extern Il2CppType IEnumerator_1_t6974_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49781_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RangeAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49781_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8861_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6974_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49781_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8861_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49781_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8861_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8861_0_0_0;
extern Il2CppType IEnumerable_1_t8861_1_0_0;
struct IEnumerable_1_t8861;
extern Il2CppGenericClass IEnumerable_1_t8861_GenericClass;
TypeInfo IEnumerable_1_t8861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8861_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8861_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8861_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8861_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8861_0_0_0/* byval_arg */
	, &IEnumerable_1_t8861_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8861_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8860_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>
extern MethodInfo IList_1_get_Item_m49782_MethodInfo;
extern MethodInfo IList_1_set_Item_m49783_MethodInfo;
static PropertyInfo IList_1_t8860____Item_PropertyInfo = 
{
	&IList_1_t8860_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49782_MethodInfo/* get */
	, &IList_1_set_Item_m49783_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8860_PropertyInfos[] =
{
	&IList_1_t8860____Item_PropertyInfo,
	NULL
};
extern Il2CppType RangeAttribute_t501_0_0_0;
static ParameterInfo IList_1_t8860_IList_1_IndexOf_m49784_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RangeAttribute_t501_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49784_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49784_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8860_IList_1_IndexOf_m49784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RangeAttribute_t501_0_0_0;
static ParameterInfo IList_1_t8860_IList_1_Insert_m49785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RangeAttribute_t501_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49785_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49785_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8860_IList_1_Insert_m49785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49785_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8860_IList_1_RemoveAt_m49786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49786_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49786_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8860_IList_1_RemoveAt_m49786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49786_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8860_IList_1_get_Item_m49782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType RangeAttribute_t501_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49782_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49782_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &RangeAttribute_t501_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8860_IList_1_get_Item_m49782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49782_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RangeAttribute_t501_0_0_0;
static ParameterInfo IList_1_t8860_IList_1_set_Item_m49783_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RangeAttribute_t501_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49783_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RangeAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49783_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8860_IList_1_set_Item_m49783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49783_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8860_MethodInfos[] =
{
	&IList_1_IndexOf_m49784_MethodInfo,
	&IList_1_Insert_m49785_MethodInfo,
	&IList_1_RemoveAt_m49786_MethodInfo,
	&IList_1_get_Item_m49782_MethodInfo,
	&IList_1_set_Item_m49783_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8860_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8859_il2cpp_TypeInfo,
	&IEnumerable_1_t8861_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8860_0_0_0;
extern Il2CppType IList_1_t8860_1_0_0;
struct IList_1_t8860;
extern Il2CppGenericClass IList_1_t8860_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8860_MethodInfos/* methods */
	, IList_1_t8860_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8860_il2cpp_TypeInfo/* element_class */
	, IList_1_t8860_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8860_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8860_0_0_0/* byval_arg */
	, &IList_1_t8860_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8860_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6976_il2cpp_TypeInfo;

// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextAreaAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextAreaAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49787_MethodInfo;
static PropertyInfo IEnumerator_1_t6976____Current_PropertyInfo = 
{
	&IEnumerator_1_t6976_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6976_PropertyInfos[] =
{
	&IEnumerator_1_t6976____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49787_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextAreaAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49787_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6976_il2cpp_TypeInfo/* declaring_type */
	, &TextAreaAttribute_t550_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49787_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6976_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49787_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6976_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6976_0_0_0;
extern Il2CppType IEnumerator_1_t6976_1_0_0;
struct IEnumerator_1_t6976;
extern Il2CppGenericClass IEnumerator_1_t6976_GenericClass;
TypeInfo IEnumerator_1_t6976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6976_MethodInfos/* methods */
	, IEnumerator_1_t6976_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6976_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6976_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6976_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6976_0_0_0/* byval_arg */
	, &IEnumerator_1_t6976_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6976_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_498.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4951_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_498MethodDeclarations.h"

extern TypeInfo TextAreaAttribute_t550_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29918_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextAreaAttribute_t550_m39116_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TextAreaAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextAreaAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTextAreaAttribute_t550_m39116(__this, p0, method) (TextAreaAttribute_t550 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4951____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4951, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4951____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4951, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4951_FieldInfos[] =
{
	&InternalEnumerator_1_t4951____array_0_FieldInfo,
	&InternalEnumerator_1_t4951____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4951____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4951_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4951____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4951_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29918_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4951_PropertyInfos[] =
{
	&InternalEnumerator_1_t4951____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4951____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4951_InternalEnumerator_1__ctor_m29914_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29914_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29914_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4951_InternalEnumerator_1__ctor_m29914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29914_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29916_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29916_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29916_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29917_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29917_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29917_GenericMethod/* genericMethod */

};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29918_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TextAreaAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29918_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* declaring_type */
	, &TextAreaAttribute_t550_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29918_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4951_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29914_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_MethodInfo,
	&InternalEnumerator_1_Dispose_m29916_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29917_MethodInfo,
	&InternalEnumerator_1_get_Current_m29918_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29917_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29916_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4951_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29915_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29917_MethodInfo,
	&InternalEnumerator_1_Dispose_m29916_MethodInfo,
	&InternalEnumerator_1_get_Current_m29918_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4951_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6976_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4951_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6976_il2cpp_TypeInfo, 7},
};
extern TypeInfo TextAreaAttribute_t550_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4951_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29918_MethodInfo/* Method Usage */,
	&TextAreaAttribute_t550_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTextAreaAttribute_t550_m39116_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4951_0_0_0;
extern Il2CppType InternalEnumerator_1_t4951_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4951_GenericClass;
TypeInfo InternalEnumerator_1_t4951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4951_MethodInfos/* methods */
	, InternalEnumerator_1_t4951_PropertyInfos/* properties */
	, InternalEnumerator_1_t4951_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4951_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4951_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4951_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4951_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4951_1_0_0/* this_arg */
	, InternalEnumerator_1_t4951_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4951_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4951_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4951)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8862_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>
extern MethodInfo ICollection_1_get_Count_m49788_MethodInfo;
static PropertyInfo ICollection_1_t8862____Count_PropertyInfo = 
{
	&ICollection_1_t8862_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49788_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49789_MethodInfo;
static PropertyInfo ICollection_1_t8862____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8862_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8862_PropertyInfos[] =
{
	&ICollection_1_t8862____Count_PropertyInfo,
	&ICollection_1_t8862____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49788_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49788_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49788_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49789_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49789_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49789_GenericMethod/* genericMethod */

};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
extern Il2CppType TextAreaAttribute_t550_0_0_0;
static ParameterInfo ICollection_1_t8862_ICollection_1_Add_m49790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAreaAttribute_t550_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49790_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49790_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8862_ICollection_1_Add_m49790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49790_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49791_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49791_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49791_GenericMethod/* genericMethod */

};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
static ParameterInfo ICollection_1_t8862_ICollection_1_Contains_m49792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAreaAttribute_t550_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49792_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49792_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8862_ICollection_1_Contains_m49792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49792_GenericMethod/* genericMethod */

};
extern Il2CppType TextAreaAttributeU5BU5D_t5758_0_0_0;
extern Il2CppType TextAreaAttributeU5BU5D_t5758_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8862_ICollection_1_CopyTo_m49793_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextAreaAttributeU5BU5D_t5758_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49793_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49793_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8862_ICollection_1_CopyTo_m49793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49793_GenericMethod/* genericMethod */

};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
static ParameterInfo ICollection_1_t8862_ICollection_1_Remove_m49794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAreaAttribute_t550_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49794_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAreaAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49794_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8862_ICollection_1_Remove_m49794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49794_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8862_MethodInfos[] =
{
	&ICollection_1_get_Count_m49788_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49789_MethodInfo,
	&ICollection_1_Add_m49790_MethodInfo,
	&ICollection_1_Clear_m49791_MethodInfo,
	&ICollection_1_Contains_m49792_MethodInfo,
	&ICollection_1_CopyTo_m49793_MethodInfo,
	&ICollection_1_Remove_m49794_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8864_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8862_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8864_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8862_0_0_0;
extern Il2CppType ICollection_1_t8862_1_0_0;
struct ICollection_1_t8862;
extern Il2CppGenericClass ICollection_1_t8862_GenericClass;
TypeInfo ICollection_1_t8862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8862_MethodInfos/* methods */
	, ICollection_1_t8862_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8862_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8862_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8862_0_0_0/* byval_arg */
	, &ICollection_1_t8862_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8862_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextAreaAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextAreaAttribute>
extern Il2CppType IEnumerator_1_t6976_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49795_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextAreaAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49795_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8864_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6976_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49795_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8864_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49795_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8864_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8864_0_0_0;
extern Il2CppType IEnumerable_1_t8864_1_0_0;
struct IEnumerable_1_t8864;
extern Il2CppGenericClass IEnumerable_1_t8864_GenericClass;
TypeInfo IEnumerable_1_t8864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8864_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8864_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8864_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8864_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8864_0_0_0/* byval_arg */
	, &IEnumerable_1_t8864_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8864_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8863_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>
extern MethodInfo IList_1_get_Item_m49796_MethodInfo;
extern MethodInfo IList_1_set_Item_m49797_MethodInfo;
static PropertyInfo IList_1_t8863____Item_PropertyInfo = 
{
	&IList_1_t8863_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49796_MethodInfo/* get */
	, &IList_1_set_Item_m49797_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8863_PropertyInfos[] =
{
	&IList_1_t8863____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
static ParameterInfo IList_1_t8863_IList_1_IndexOf_m49798_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAreaAttribute_t550_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49798_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49798_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8863_IList_1_IndexOf_m49798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextAreaAttribute_t550_0_0_0;
static ParameterInfo IList_1_t8863_IList_1_Insert_m49799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextAreaAttribute_t550_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49799_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49799_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8863_IList_1_Insert_m49799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49799_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8863_IList_1_RemoveAt_m49800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49800_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49800_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8863_IList_1_RemoveAt_m49800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49800_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8863_IList_1_get_Item_m49796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TextAreaAttribute_t550_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49796_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49796_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &TextAreaAttribute_t550_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8863_IList_1_get_Item_m49796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49796_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextAreaAttribute_t550_0_0_0;
static ParameterInfo IList_1_t8863_IList_1_set_Item_m49797_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextAreaAttribute_t550_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49797_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAreaAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49797_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8863_IList_1_set_Item_m49797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49797_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8863_MethodInfos[] =
{
	&IList_1_IndexOf_m49798_MethodInfo,
	&IList_1_Insert_m49799_MethodInfo,
	&IList_1_RemoveAt_m49800_MethodInfo,
	&IList_1_get_Item_m49796_MethodInfo,
	&IList_1_set_Item_m49797_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8863_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8862_il2cpp_TypeInfo,
	&IEnumerable_1_t8864_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8863_0_0_0;
extern Il2CppType IList_1_t8863_1_0_0;
struct IList_1_t8863;
extern Il2CppGenericClass IList_1_t8863_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8863_MethodInfos/* methods */
	, IList_1_t8863_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8863_il2cpp_TypeInfo/* element_class */
	, IList_1_t8863_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8863_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8863_0_0_0/* byval_arg */
	, &IList_1_t8863_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8863_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6978_il2cpp_TypeInfo;

// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SelectionBaseAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SelectionBaseAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49801_MethodInfo;
static PropertyInfo IEnumerator_1_t6978____Current_PropertyInfo = 
{
	&IEnumerator_1_t6978_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49801_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6978_PropertyInfos[] =
{
	&IEnumerator_1_t6978____Current_PropertyInfo,
	NULL
};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49801_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SelectionBaseAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49801_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6978_il2cpp_TypeInfo/* declaring_type */
	, &SelectionBaseAttribute_t542_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49801_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6978_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49801_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6978_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6978_0_0_0;
extern Il2CppType IEnumerator_1_t6978_1_0_0;
struct IEnumerator_1_t6978;
extern Il2CppGenericClass IEnumerator_1_t6978_GenericClass;
TypeInfo IEnumerator_1_t6978_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6978_MethodInfos/* methods */
	, IEnumerator_1_t6978_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6978_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6978_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6978_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6978_0_0_0/* byval_arg */
	, &IEnumerator_1_t6978_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6978_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_499.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4952_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_499MethodDeclarations.h"

extern TypeInfo SelectionBaseAttribute_t542_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29923_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSelectionBaseAttribute_t542_m39127_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SelectionBaseAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SelectionBaseAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSelectionBaseAttribute_t542_m39127(__this, p0, method) (SelectionBaseAttribute_t542 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4952____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4952, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4952____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4952, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4952_FieldInfos[] =
{
	&InternalEnumerator_1_t4952____array_0_FieldInfo,
	&InternalEnumerator_1_t4952____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4952____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4952_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4952____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4952_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4952_PropertyInfos[] =
{
	&InternalEnumerator_1_t4952____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4952____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4952_InternalEnumerator_1__ctor_m29919_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29919_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29919_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4952_InternalEnumerator_1__ctor_m29919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29919_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29921_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29921_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29921_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29922_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29922_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29922_GenericMethod/* genericMethod */

};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29923_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SelectionBaseAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29923_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* declaring_type */
	, &SelectionBaseAttribute_t542_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29923_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4952_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29919_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_MethodInfo,
	&InternalEnumerator_1_Dispose_m29921_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29922_MethodInfo,
	&InternalEnumerator_1_get_Current_m29923_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29922_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29921_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4952_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29920_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29922_MethodInfo,
	&InternalEnumerator_1_Dispose_m29921_MethodInfo,
	&InternalEnumerator_1_get_Current_m29923_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4952_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6978_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4952_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6978_il2cpp_TypeInfo, 7},
};
extern TypeInfo SelectionBaseAttribute_t542_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4952_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29923_MethodInfo/* Method Usage */,
	&SelectionBaseAttribute_t542_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSelectionBaseAttribute_t542_m39127_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4952_0_0_0;
extern Il2CppType InternalEnumerator_1_t4952_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4952_GenericClass;
TypeInfo InternalEnumerator_1_t4952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4952_MethodInfos/* methods */
	, InternalEnumerator_1_t4952_PropertyInfos/* properties */
	, InternalEnumerator_1_t4952_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4952_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4952_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4952_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4952_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4952_1_0_0/* this_arg */
	, InternalEnumerator_1_t4952_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4952_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4952)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8865_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>
extern MethodInfo ICollection_1_get_Count_m49802_MethodInfo;
static PropertyInfo ICollection_1_t8865____Count_PropertyInfo = 
{
	&ICollection_1_t8865_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49802_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49803_MethodInfo;
static PropertyInfo ICollection_1_t8865____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8865_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8865_PropertyInfos[] =
{
	&ICollection_1_t8865____Count_PropertyInfo,
	&ICollection_1_t8865____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49802_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49802_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49802_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49803_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49803_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49803_GenericMethod/* genericMethod */

};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
static ParameterInfo ICollection_1_t8865_ICollection_1_Add_m49804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttribute_t542_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49804_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49804_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8865_ICollection_1_Add_m49804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49804_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49805_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49805_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49805_GenericMethod/* genericMethod */

};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
static ParameterInfo ICollection_1_t8865_ICollection_1_Contains_m49806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttribute_t542_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49806_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49806_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8865_ICollection_1_Contains_m49806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49806_GenericMethod/* genericMethod */

};
extern Il2CppType SelectionBaseAttributeU5BU5D_t5759_0_0_0;
extern Il2CppType SelectionBaseAttributeU5BU5D_t5759_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8865_ICollection_1_CopyTo_m49807_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttributeU5BU5D_t5759_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49807_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49807_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8865_ICollection_1_CopyTo_m49807_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49807_GenericMethod/* genericMethod */

};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
static ParameterInfo ICollection_1_t8865_ICollection_1_Remove_m49808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttribute_t542_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49808_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SelectionBaseAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49808_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8865_ICollection_1_Remove_m49808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49808_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8865_MethodInfos[] =
{
	&ICollection_1_get_Count_m49802_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49803_MethodInfo,
	&ICollection_1_Add_m49804_MethodInfo,
	&ICollection_1_Clear_m49805_MethodInfo,
	&ICollection_1_Contains_m49806_MethodInfo,
	&ICollection_1_CopyTo_m49807_MethodInfo,
	&ICollection_1_Remove_m49808_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8867_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8865_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8867_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8865_0_0_0;
extern Il2CppType ICollection_1_t8865_1_0_0;
struct ICollection_1_t8865;
extern Il2CppGenericClass ICollection_1_t8865_GenericClass;
TypeInfo ICollection_1_t8865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8865_MethodInfos/* methods */
	, ICollection_1_t8865_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8865_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8865_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8865_0_0_0/* byval_arg */
	, &ICollection_1_t8865_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8865_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SelectionBaseAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SelectionBaseAttribute>
extern Il2CppType IEnumerator_1_t6978_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49809_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SelectionBaseAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49809_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8867_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6978_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49809_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8867_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49809_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8867_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8867_0_0_0;
extern Il2CppType IEnumerable_1_t8867_1_0_0;
struct IEnumerable_1_t8867;
extern Il2CppGenericClass IEnumerable_1_t8867_GenericClass;
TypeInfo IEnumerable_1_t8867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8867_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8867_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8867_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8867_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8867_0_0_0/* byval_arg */
	, &IEnumerable_1_t8867_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8867_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8866_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>
extern MethodInfo IList_1_get_Item_m49810_MethodInfo;
extern MethodInfo IList_1_set_Item_m49811_MethodInfo;
static PropertyInfo IList_1_t8866____Item_PropertyInfo = 
{
	&IList_1_t8866_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49810_MethodInfo/* get */
	, &IList_1_set_Item_m49811_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8866_PropertyInfos[] =
{
	&IList_1_t8866____Item_PropertyInfo,
	NULL
};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
static ParameterInfo IList_1_t8866_IList_1_IndexOf_m49812_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttribute_t542_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49812_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49812_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8866_IList_1_IndexOf_m49812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49812_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
static ParameterInfo IList_1_t8866_IList_1_Insert_m49813_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttribute_t542_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49813_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49813_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8866_IList_1_Insert_m49813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49813_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8866_IList_1_RemoveAt_m49814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49814_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49814_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8866_IList_1_RemoveAt_m49814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8866_IList_1_get_Item_m49810_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49810_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49810_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &SelectionBaseAttribute_t542_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8866_IList_1_get_Item_m49810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49810_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SelectionBaseAttribute_t542_0_0_0;
static ParameterInfo IList_1_t8866_IList_1_set_Item_m49811_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SelectionBaseAttribute_t542_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49811_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SelectionBaseAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49811_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8866_IList_1_set_Item_m49811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49811_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8866_MethodInfos[] =
{
	&IList_1_IndexOf_m49812_MethodInfo,
	&IList_1_Insert_m49813_MethodInfo,
	&IList_1_RemoveAt_m49814_MethodInfo,
	&IList_1_get_Item_m49810_MethodInfo,
	&IList_1_set_Item_m49811_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8866_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8865_il2cpp_TypeInfo,
	&IEnumerable_1_t8867_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8866_0_0_0;
extern Il2CppType IList_1_t8866_1_0_0;
struct IList_1_t8866;
extern Il2CppGenericClass IList_1_t8866_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8866_MethodInfos/* methods */
	, IList_1_t8866_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8866_il2cpp_TypeInfo/* element_class */
	, IList_1_t8866_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8866_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8866_0_0_0/* byval_arg */
	, &IList_1_t8866_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8866_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6980_il2cpp_TypeInfo;

// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterInfo>
extern MethodInfo IEnumerator_1_get_Current_m49815_MethodInfo;
static PropertyInfo IEnumerator_1_t6980____Current_PropertyInfo = 
{
	&IEnumerator_1_t6980_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49815_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6980_PropertyInfos[] =
{
	&IEnumerator_1_t6980____Current_PropertyInfo,
	NULL
};
extern Il2CppType ParameterInfo_t1222_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49815_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49815_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6980_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfo_t1222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49815_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6980_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49815_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6980_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6980_0_0_0;
extern Il2CppType IEnumerator_1_t6980_1_0_0;
struct IEnumerator_1_t6980;
extern Il2CppGenericClass IEnumerator_1_t6980_GenericClass;
TypeInfo IEnumerator_1_t6980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6980_MethodInfos/* methods */
	, IEnumerator_1_t6980_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6980_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6980_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6980_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6980_0_0_0/* byval_arg */
	, &IEnumerator_1_t6980_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6980_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_500.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4953_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_500MethodDeclarations.h"

extern TypeInfo ParameterInfo_t1222_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29928_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisParameterInfo_t1222_m39138_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterInfo>(System.Int32)
#define Array_InternalArray__get_Item_TisParameterInfo_t1222_m39138(__this, p0, method) (ParameterInfo_t1222 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4953____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4953, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4953____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4953, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4953_FieldInfos[] =
{
	&InternalEnumerator_1_t4953____array_0_FieldInfo,
	&InternalEnumerator_1_t4953____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4953____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4953_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4953____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4953_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29928_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4953_PropertyInfos[] =
{
	&InternalEnumerator_1_t4953____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4953____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4953_InternalEnumerator_1__ctor_m29924_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29924_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29924_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4953_InternalEnumerator_1__ctor_m29924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29924_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29926_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29926_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29926_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29927_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29927_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29927_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterInfo_t1222_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29928_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29928_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfo_t1222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29928_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4953_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29924_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_MethodInfo,
	&InternalEnumerator_1_Dispose_m29926_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29927_MethodInfo,
	&InternalEnumerator_1_get_Current_m29928_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29927_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29926_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4953_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29925_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29927_MethodInfo,
	&InternalEnumerator_1_Dispose_m29926_MethodInfo,
	&InternalEnumerator_1_get_Current_m29928_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4953_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6980_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4953_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6980_il2cpp_TypeInfo, 7},
};
extern TypeInfo ParameterInfo_t1222_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4953_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29928_MethodInfo/* Method Usage */,
	&ParameterInfo_t1222_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisParameterInfo_t1222_m39138_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4953_0_0_0;
extern Il2CppType InternalEnumerator_1_t4953_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4953_GenericClass;
TypeInfo InternalEnumerator_1_t4953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4953_MethodInfos/* methods */
	, InternalEnumerator_1_t4953_PropertyInfos/* properties */
	, InternalEnumerator_1_t4953_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4953_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4953_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4953_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4953_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4953_1_0_0/* this_arg */
	, InternalEnumerator_1_t4953_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4953_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4953_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4953)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8868_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>
extern MethodInfo ICollection_1_get_Count_m49816_MethodInfo;
static PropertyInfo ICollection_1_t8868____Count_PropertyInfo = 
{
	&ICollection_1_t8868_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49816_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49817_MethodInfo;
static PropertyInfo ICollection_1_t8868____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8868_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49817_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8868_PropertyInfos[] =
{
	&ICollection_1_t8868____Count_PropertyInfo,
	&ICollection_1_t8868____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49816_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m49816_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49816_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49817_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49817_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49817_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterInfo_t1222_0_0_0;
extern Il2CppType ParameterInfo_t1222_0_0_0;
static ParameterInfo ICollection_1_t8868_ICollection_1_Add_m49818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterInfo_t1222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49818_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Add(T)
MethodInfo ICollection_1_Add_m49818_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8868_ICollection_1_Add_m49818_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49818_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49819_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Clear()
MethodInfo ICollection_1_Clear_m49819_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49819_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterInfo_t1222_0_0_0;
static ParameterInfo ICollection_1_t8868_ICollection_1_Contains_m49820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterInfo_t1222_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49820_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m49820_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8868_ICollection_1_Contains_m49820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49820_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterInfoU5BU5D_t1221_0_0_0;
extern Il2CppType ParameterInfoU5BU5D_t1221_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8868_ICollection_1_CopyTo_m49821_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ParameterInfoU5BU5D_t1221_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49821_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49821_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8868_ICollection_1_CopyTo_m49821_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49821_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterInfo_t1222_0_0_0;
static ParameterInfo ICollection_1_t8868_ICollection_1_Remove_m49822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterInfo_t1222_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49822_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.ParameterInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m49822_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8868_ICollection_1_Remove_m49822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49822_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8868_MethodInfos[] =
{
	&ICollection_1_get_Count_m49816_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49817_MethodInfo,
	&ICollection_1_Add_m49818_MethodInfo,
	&ICollection_1_Clear_m49819_MethodInfo,
	&ICollection_1_Contains_m49820_MethodInfo,
	&ICollection_1_CopyTo_m49821_MethodInfo,
	&ICollection_1_Remove_m49822_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8870_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8868_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8870_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8868_0_0_0;
extern Il2CppType ICollection_1_t8868_1_0_0;
struct ICollection_1_t8868;
extern Il2CppGenericClass ICollection_1_t8868_GenericClass;
TypeInfo ICollection_1_t8868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8868_MethodInfos/* methods */
	, ICollection_1_t8868_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8868_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8868_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8868_0_0_0/* byval_arg */
	, &ICollection_1_t8868_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8868_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterInfo>
extern Il2CppType IEnumerator_1_t6980_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49823_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49823_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8870_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49823_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8870_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49823_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8870_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8870_0_0_0;
extern Il2CppType IEnumerable_1_t8870_1_0_0;
struct IEnumerable_1_t8870;
extern Il2CppGenericClass IEnumerable_1_t8870_GenericClass;
TypeInfo IEnumerable_1_t8870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8870_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8870_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8870_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8870_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8870_0_0_0/* byval_arg */
	, &IEnumerable_1_t8870_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8869_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>
extern MethodInfo IList_1_get_Item_m49824_MethodInfo;
extern MethodInfo IList_1_set_Item_m49825_MethodInfo;
static PropertyInfo IList_1_t8869____Item_PropertyInfo = 
{
	&IList_1_t8869_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49824_MethodInfo/* get */
	, &IList_1_set_Item_m49825_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8869_PropertyInfos[] =
{
	&IList_1_t8869____Item_PropertyInfo,
	NULL
};
extern Il2CppType ParameterInfo_t1222_0_0_0;
static ParameterInfo IList_1_t8869_IList_1_IndexOf_m49826_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterInfo_t1222_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49826_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49826_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8869_IList_1_IndexOf_m49826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ParameterInfo_t1222_0_0_0;
static ParameterInfo IList_1_t8869_IList_1_Insert_m49827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ParameterInfo_t1222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49827_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49827_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8869_IList_1_Insert_m49827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49827_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8869_IList_1_RemoveAt_m49828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49828_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49828_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8869_IList_1_RemoveAt_m49828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8869_IList_1_get_Item_m49824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ParameterInfo_t1222_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49824_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49824_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfo_t1222_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8869_IList_1_get_Item_m49824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49824_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ParameterInfo_t1222_0_0_0;
static ParameterInfo IList_1_t8869_IList_1_set_Item_m49825_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ParameterInfo_t1222_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49825_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.ParameterInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49825_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8869_IList_1_set_Item_m49825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49825_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8869_MethodInfos[] =
{
	&IList_1_IndexOf_m49826_MethodInfo,
	&IList_1_Insert_m49827_MethodInfo,
	&IList_1_RemoveAt_m49828_MethodInfo,
	&IList_1_get_Item_m49824_MethodInfo,
	&IList_1_set_Item_m49825_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8869_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8868_il2cpp_TypeInfo,
	&IEnumerable_1_t8870_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8869_0_0_0;
extern Il2CppType IList_1_t8869_1_0_0;
struct IList_1_t8869;
extern Il2CppGenericClass IList_1_t8869_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8869_MethodInfos/* methods */
	, IList_1_t8869_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8869_il2cpp_TypeInfo/* element_class */
	, IList_1_t8869_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8869_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8869_0_0_0/* byval_arg */
	, &IList_1_t8869_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8869_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8871_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>
extern MethodInfo ICollection_1_get_Count_m49829_MethodInfo;
static PropertyInfo ICollection_1_t8871____Count_PropertyInfo = 
{
	&ICollection_1_t8871_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49829_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49830_MethodInfo;
static PropertyInfo ICollection_1_t8871____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8871_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49830_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8871_PropertyInfos[] =
{
	&ICollection_1_t8871____Count_PropertyInfo,
	&ICollection_1_t8871____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49829_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m49829_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49829_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49830_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49830_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49830_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
extern Il2CppType _ParameterInfo_t2678_0_0_0;
static ParameterInfo ICollection_1_t8871_ICollection_1_Add_m49831_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterInfo_t2678_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49831_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Add(T)
MethodInfo ICollection_1_Add_m49831_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8871_ICollection_1_Add_m49831_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49831_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49832_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Clear()
MethodInfo ICollection_1_Clear_m49832_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49832_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
static ParameterInfo ICollection_1_t8871_ICollection_1_Contains_m49833_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterInfo_t2678_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49833_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m49833_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8871_ICollection_1_Contains_m49833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49833_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterInfoU5BU5D_t5445_0_0_0;
extern Il2CppType _ParameterInfoU5BU5D_t5445_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8871_ICollection_1_CopyTo_m49834_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterInfoU5BU5D_t5445_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49834_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49834_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8871_ICollection_1_CopyTo_m49834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49834_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
static ParameterInfo ICollection_1_t8871_ICollection_1_Remove_m49835_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterInfo_t2678_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49835_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m49835_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8871_ICollection_1_Remove_m49835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49835_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8871_MethodInfos[] =
{
	&ICollection_1_get_Count_m49829_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49830_MethodInfo,
	&ICollection_1_Add_m49831_MethodInfo,
	&ICollection_1_Clear_m49832_MethodInfo,
	&ICollection_1_Contains_m49833_MethodInfo,
	&ICollection_1_CopyTo_m49834_MethodInfo,
	&ICollection_1_Remove_m49835_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8873_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8871_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8873_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8871_0_0_0;
extern Il2CppType ICollection_1_t8871_1_0_0;
struct ICollection_1_t8871;
extern Il2CppGenericClass ICollection_1_t8871_GenericClass;
TypeInfo ICollection_1_t8871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8871_MethodInfos/* methods */
	, ICollection_1_t8871_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8871_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8871_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8871_0_0_0/* byval_arg */
	, &ICollection_1_t8871_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8871_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterInfo>
extern Il2CppType IEnumerator_1_t6982_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49836_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49836_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8873_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6982_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49836_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8873_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49836_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8873_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8873_0_0_0;
extern Il2CppType IEnumerable_1_t8873_1_0_0;
struct IEnumerable_1_t8873;
extern Il2CppGenericClass IEnumerable_1_t8873_GenericClass;
TypeInfo IEnumerable_1_t8873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8873_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8873_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8873_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8873_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8873_0_0_0/* byval_arg */
	, &IEnumerable_1_t8873_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8873_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6982_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterInfo>
extern MethodInfo IEnumerator_1_get_Current_m49837_MethodInfo;
static PropertyInfo IEnumerator_1_t6982____Current_PropertyInfo = 
{
	&IEnumerator_1_t6982_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49837_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6982_PropertyInfos[] =
{
	&IEnumerator_1_t6982____Current_PropertyInfo,
	NULL
};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49837_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49837_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6982_il2cpp_TypeInfo/* declaring_type */
	, &_ParameterInfo_t2678_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49837_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6982_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49837_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6982_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6982_0_0_0;
extern Il2CppType IEnumerator_1_t6982_1_0_0;
struct IEnumerator_1_t6982;
extern Il2CppGenericClass IEnumerator_1_t6982_GenericClass;
TypeInfo IEnumerator_1_t6982_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6982_MethodInfos/* methods */
	, IEnumerator_1_t6982_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6982_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6982_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6982_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6982_0_0_0/* byval_arg */
	, &IEnumerator_1_t6982_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6982_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_501.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4954_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_501MethodDeclarations.h"

extern TypeInfo _ParameterInfo_t2678_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29933_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_ParameterInfo_t2678_m39149_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ParameterInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ParameterInfo>(System.Int32)
#define Array_InternalArray__get_Item_Tis_ParameterInfo_t2678_m39149(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4954____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4954, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4954____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4954, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4954_FieldInfos[] =
{
	&InternalEnumerator_1_t4954____array_0_FieldInfo,
	&InternalEnumerator_1_t4954____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4954____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4954_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4954____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4954_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29933_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4954_PropertyInfos[] =
{
	&InternalEnumerator_1_t4954____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4954____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4954_InternalEnumerator_1__ctor_m29929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29929_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29929_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4954_InternalEnumerator_1__ctor_m29929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29929_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29931_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29931_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29931_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29932_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29932_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29932_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29933_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29933_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* declaring_type */
	, &_ParameterInfo_t2678_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29933_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4954_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29929_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_MethodInfo,
	&InternalEnumerator_1_Dispose_m29931_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29932_MethodInfo,
	&InternalEnumerator_1_get_Current_m29933_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29932_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29931_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4954_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29930_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29932_MethodInfo,
	&InternalEnumerator_1_Dispose_m29931_MethodInfo,
	&InternalEnumerator_1_get_Current_m29933_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4954_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6982_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4954_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6982_il2cpp_TypeInfo, 7},
};
extern TypeInfo _ParameterInfo_t2678_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4954_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29933_MethodInfo/* Method Usage */,
	&_ParameterInfo_t2678_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_ParameterInfo_t2678_m39149_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4954_0_0_0;
extern Il2CppType InternalEnumerator_1_t4954_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4954_GenericClass;
TypeInfo InternalEnumerator_1_t4954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4954_MethodInfos/* methods */
	, InternalEnumerator_1_t4954_PropertyInfos/* properties */
	, InternalEnumerator_1_t4954_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4954_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4954_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4954_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4954_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4954_1_0_0/* this_arg */
	, InternalEnumerator_1_t4954_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4954_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4954)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8872_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>
extern MethodInfo IList_1_get_Item_m49838_MethodInfo;
extern MethodInfo IList_1_set_Item_m49839_MethodInfo;
static PropertyInfo IList_1_t8872____Item_PropertyInfo = 
{
	&IList_1_t8872_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49838_MethodInfo/* get */
	, &IList_1_set_Item_m49839_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8872_PropertyInfos[] =
{
	&IList_1_t8872____Item_PropertyInfo,
	NULL
};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
static ParameterInfo IList_1_t8872_IList_1_IndexOf_m49840_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterInfo_t2678_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49840_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49840_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8872_IList_1_IndexOf_m49840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49840_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _ParameterInfo_t2678_0_0_0;
static ParameterInfo IList_1_t8872_IList_1_Insert_m49841_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_ParameterInfo_t2678_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49841_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49841_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8872_IList_1_Insert_m49841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49841_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8872_IList_1_RemoveAt_m49842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49842_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49842_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8872_IList_1_RemoveAt_m49842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49842_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8872_IList_1_get_Item_m49838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType _ParameterInfo_t2678_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49838_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49838_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &_ParameterInfo_t2678_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8872_IList_1_get_Item_m49838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49838_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _ParameterInfo_t2678_0_0_0;
static ParameterInfo IList_1_t8872_IList_1_set_Item_m49839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_ParameterInfo_t2678_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49839_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49839_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8872_IList_1_set_Item_m49839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49839_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8872_MethodInfos[] =
{
	&IList_1_IndexOf_m49840_MethodInfo,
	&IList_1_Insert_m49841_MethodInfo,
	&IList_1_RemoveAt_m49842_MethodInfo,
	&IList_1_get_Item_m49838_MethodInfo,
	&IList_1_set_Item_m49839_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8872_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8871_il2cpp_TypeInfo,
	&IEnumerable_1_t8873_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8872_0_0_0;
extern Il2CppType IList_1_t8872_1_0_0;
struct IList_1_t8872;
extern Il2CppGenericClass IList_1_t8872_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8872_MethodInfos/* methods */
	, IList_1_t8872_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8872_il2cpp_TypeInfo/* element_class */
	, IList_1_t8872_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8872_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8872_0_0_0/* byval_arg */
	, &IList_1_t8872_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8872_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6984_il2cpp_TypeInfo;

// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49843_MethodInfo;
static PropertyInfo IEnumerator_1_t6984____Current_PropertyInfo = 
{
	&IEnumerator_1_t6984_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49843_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6984_PropertyInfos[] =
{
	&IEnumerator_1_t6984____Current_PropertyInfo,
	NULL
};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49843_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49843_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6984_il2cpp_TypeInfo/* declaring_type */
	, &SharedBetweenAnimatorsAttribute_t1120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49843_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6984_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49843_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6984_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6984_0_0_0;
extern Il2CppType IEnumerator_1_t6984_1_0_0;
struct IEnumerator_1_t6984;
extern Il2CppGenericClass IEnumerator_1_t6984_GenericClass;
TypeInfo IEnumerator_1_t6984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6984_MethodInfos/* methods */
	, IEnumerator_1_t6984_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6984_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6984_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6984_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6984_0_0_0/* byval_arg */
	, &IEnumerator_1_t6984_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6984_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_502.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4955_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_502MethodDeclarations.h"

extern TypeInfo SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29938_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSharedBetweenAnimatorsAttribute_t1120_m39160_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SharedBetweenAnimatorsAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SharedBetweenAnimatorsAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSharedBetweenAnimatorsAttribute_t1120_m39160(__this, p0, method) (SharedBetweenAnimatorsAttribute_t1120 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4955____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4955, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4955____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4955, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4955_FieldInfos[] =
{
	&InternalEnumerator_1_t4955____array_0_FieldInfo,
	&InternalEnumerator_1_t4955____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4955____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4955_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4955____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4955_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29938_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4955_PropertyInfos[] =
{
	&InternalEnumerator_1_t4955____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4955____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4955_InternalEnumerator_1__ctor_m29934_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29934_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29934_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4955_InternalEnumerator_1__ctor_m29934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29934_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29936_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29936_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29936_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29937_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29937_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29937_GenericMethod/* genericMethod */

};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29938_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29938_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* declaring_type */
	, &SharedBetweenAnimatorsAttribute_t1120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29938_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4955_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29934_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_MethodInfo,
	&InternalEnumerator_1_Dispose_m29936_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29937_MethodInfo,
	&InternalEnumerator_1_get_Current_m29938_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29937_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29936_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4955_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29935_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29937_MethodInfo,
	&InternalEnumerator_1_Dispose_m29936_MethodInfo,
	&InternalEnumerator_1_get_Current_m29938_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4955_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6984_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4955_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6984_il2cpp_TypeInfo, 7},
};
extern TypeInfo SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4955_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29938_MethodInfo/* Method Usage */,
	&SharedBetweenAnimatorsAttribute_t1120_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSharedBetweenAnimatorsAttribute_t1120_m39160_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4955_0_0_0;
extern Il2CppType InternalEnumerator_1_t4955_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4955_GenericClass;
TypeInfo InternalEnumerator_1_t4955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4955_MethodInfos/* methods */
	, InternalEnumerator_1_t4955_PropertyInfos/* properties */
	, InternalEnumerator_1_t4955_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4955_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4955_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4955_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4955_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4955_1_0_0/* this_arg */
	, InternalEnumerator_1_t4955_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4955_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4955_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4955)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8874_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern MethodInfo ICollection_1_get_Count_m49844_MethodInfo;
static PropertyInfo ICollection_1_t8874____Count_PropertyInfo = 
{
	&ICollection_1_t8874_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49845_MethodInfo;
static PropertyInfo ICollection_1_t8874____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8874_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8874_PropertyInfos[] =
{
	&ICollection_1_t8874____Count_PropertyInfo,
	&ICollection_1_t8874____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49844_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49844_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49844_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49845_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49845_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49845_GenericMethod/* genericMethod */

};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
static ParameterInfo ICollection_1_t8874_ICollection_1_Add_m49846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttribute_t1120_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49846_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49846_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8874_ICollection_1_Add_m49846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49846_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49847_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49847_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49847_GenericMethod/* genericMethod */

};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
static ParameterInfo ICollection_1_t8874_ICollection_1_Contains_m49848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttribute_t1120_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49848_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49848_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8874_ICollection_1_Contains_m49848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49848_GenericMethod/* genericMethod */

};
extern Il2CppType SharedBetweenAnimatorsAttributeU5BU5D_t5760_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttributeU5BU5D_t5760_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8874_ICollection_1_CopyTo_m49849_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttributeU5BU5D_t5760_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49849_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49849_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8874_ICollection_1_CopyTo_m49849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49849_GenericMethod/* genericMethod */

};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
static ParameterInfo ICollection_1_t8874_ICollection_1_Remove_m49850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttribute_t1120_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49850_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49850_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8874_ICollection_1_Remove_m49850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49850_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8874_MethodInfos[] =
{
	&ICollection_1_get_Count_m49844_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49845_MethodInfo,
	&ICollection_1_Add_m49846_MethodInfo,
	&ICollection_1_Clear_m49847_MethodInfo,
	&ICollection_1_Contains_m49848_MethodInfo,
	&ICollection_1_CopyTo_m49849_MethodInfo,
	&ICollection_1_Remove_m49850_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8876_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8874_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8876_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8874_0_0_0;
extern Il2CppType ICollection_1_t8874_1_0_0;
struct ICollection_1_t8874;
extern Il2CppGenericClass ICollection_1_t8874_GenericClass;
TypeInfo ICollection_1_t8874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8874_MethodInfos/* methods */
	, ICollection_1_t8874_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8874_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8874_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8874_0_0_0/* byval_arg */
	, &ICollection_1_t8874_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8874_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SharedBetweenAnimatorsAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern Il2CppType IEnumerator_1_t6984_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49851_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SharedBetweenAnimatorsAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49851_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8876_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6984_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49851_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8876_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49851_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8876_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8876_0_0_0;
extern Il2CppType IEnumerable_1_t8876_1_0_0;
struct IEnumerable_1_t8876;
extern Il2CppGenericClass IEnumerable_1_t8876_GenericClass;
TypeInfo IEnumerable_1_t8876_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8876_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8876_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8876_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8876_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8876_0_0_0/* byval_arg */
	, &IEnumerable_1_t8876_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8876_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8875_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>
extern MethodInfo IList_1_get_Item_m49852_MethodInfo;
extern MethodInfo IList_1_set_Item_m49853_MethodInfo;
static PropertyInfo IList_1_t8875____Item_PropertyInfo = 
{
	&IList_1_t8875_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49852_MethodInfo/* get */
	, &IList_1_set_Item_m49853_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8875_PropertyInfos[] =
{
	&IList_1_t8875____Item_PropertyInfo,
	NULL
};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
static ParameterInfo IList_1_t8875_IList_1_IndexOf_m49854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttribute_t1120_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49854_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49854_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8875_IList_1_IndexOf_m49854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49854_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
static ParameterInfo IList_1_t8875_IList_1_Insert_m49855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttribute_t1120_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49855_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49855_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8875_IList_1_Insert_m49855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49855_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8875_IList_1_RemoveAt_m49856_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49856_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49856_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8875_IList_1_RemoveAt_m49856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49856_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8875_IList_1_get_Item_m49852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49852_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49852_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &SharedBetweenAnimatorsAttribute_t1120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8875_IList_1_get_Item_m49852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49852_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1120_0_0_0;
static ParameterInfo IList_1_t8875_IList_1_set_Item_m49853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SharedBetweenAnimatorsAttribute_t1120_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49853_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SharedBetweenAnimatorsAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49853_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8875_IList_1_set_Item_m49853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49853_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8875_MethodInfos[] =
{
	&IList_1_IndexOf_m49854_MethodInfo,
	&IList_1_Insert_m49855_MethodInfo,
	&IList_1_RemoveAt_m49856_MethodInfo,
	&IList_1_get_Item_m49852_MethodInfo,
	&IList_1_set_Item_m49853_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8875_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8874_il2cpp_TypeInfo,
	&IEnumerable_1_t8876_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8875_0_0_0;
extern Il2CppType IList_1_t8875_1_0_0;
struct IList_1_t8875;
extern Il2CppGenericClass IList_1_t8875_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8875_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8875_MethodInfos/* methods */
	, IList_1_t8875_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8875_il2cpp_TypeInfo/* element_class */
	, IList_1_t8875_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8875_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8875_0_0_0/* byval_arg */
	, &IList_1_t8875_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8875_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6986_il2cpp_TypeInfo;

// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.StateMachineBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.StateMachineBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m49857_MethodInfo;
static PropertyInfo IEnumerator_1_t6986____Current_PropertyInfo = 
{
	&IEnumerator_1_t6986_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49857_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6986_PropertyInfos[] =
{
	&IEnumerator_1_t6986____Current_PropertyInfo,
	NULL
};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49857_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.StateMachineBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49857_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6986_il2cpp_TypeInfo/* declaring_type */
	, &StateMachineBehaviour_t1121_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49857_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6986_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49857_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6986_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6986_0_0_0;
extern Il2CppType IEnumerator_1_t6986_1_0_0;
struct IEnumerator_1_t6986;
extern Il2CppGenericClass IEnumerator_1_t6986_GenericClass;
TypeInfo IEnumerator_1_t6986_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6986_MethodInfos/* methods */
	, IEnumerator_1_t6986_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6986_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6986_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6986_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6986_0_0_0/* byval_arg */
	, &IEnumerator_1_t6986_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6986_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_503.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4956_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_503MethodDeclarations.h"

extern TypeInfo StateMachineBehaviour_t1121_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29943_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisStateMachineBehaviour_t1121_m39171_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.StateMachineBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.StateMachineBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisStateMachineBehaviour_t1121_m39171(__this, p0, method) (StateMachineBehaviour_t1121 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4956____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4956, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4956____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4956, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4956_FieldInfos[] =
{
	&InternalEnumerator_1_t4956____array_0_FieldInfo,
	&InternalEnumerator_1_t4956____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4956____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4956_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4956____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4956_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29943_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4956_PropertyInfos[] =
{
	&InternalEnumerator_1_t4956____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4956____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4956_InternalEnumerator_1__ctor_m29939_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29939_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29939_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4956_InternalEnumerator_1__ctor_m29939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29939_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29941_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29941_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29941_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29942_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29942_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29942_GenericMethod/* genericMethod */

};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29943_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.StateMachineBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29943_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* declaring_type */
	, &StateMachineBehaviour_t1121_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29943_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4956_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29939_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_MethodInfo,
	&InternalEnumerator_1_Dispose_m29941_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29942_MethodInfo,
	&InternalEnumerator_1_get_Current_m29943_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29942_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29941_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4956_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29940_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29942_MethodInfo,
	&InternalEnumerator_1_Dispose_m29941_MethodInfo,
	&InternalEnumerator_1_get_Current_m29943_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4956_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6986_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4956_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6986_il2cpp_TypeInfo, 7},
};
extern TypeInfo StateMachineBehaviour_t1121_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4956_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29943_MethodInfo/* Method Usage */,
	&StateMachineBehaviour_t1121_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisStateMachineBehaviour_t1121_m39171_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4956_0_0_0;
extern Il2CppType InternalEnumerator_1_t4956_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4956_GenericClass;
TypeInfo InternalEnumerator_1_t4956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4956_MethodInfos/* methods */
	, InternalEnumerator_1_t4956_PropertyInfos/* properties */
	, InternalEnumerator_1_t4956_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4956_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4956_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4956_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4956_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4956_1_0_0/* this_arg */
	, InternalEnumerator_1_t4956_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4956_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4956)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8877_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>
extern MethodInfo ICollection_1_get_Count_m49858_MethodInfo;
static PropertyInfo ICollection_1_t8877____Count_PropertyInfo = 
{
	&ICollection_1_t8877_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49858_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49859_MethodInfo;
static PropertyInfo ICollection_1_t8877____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8877_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49859_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8877_PropertyInfos[] =
{
	&ICollection_1_t8877____Count_PropertyInfo,
	&ICollection_1_t8877____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49858_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m49858_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49858_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49859_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49859_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49859_GenericMethod/* genericMethod */

};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
static ParameterInfo ICollection_1_t8877_ICollection_1_Add_m49860_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviour_t1121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49860_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m49860_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8877_ICollection_1_Add_m49860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49860_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49861_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m49861_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49861_GenericMethod/* genericMethod */

};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
static ParameterInfo ICollection_1_t8877_ICollection_1_Contains_m49862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviour_t1121_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49862_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m49862_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8877_ICollection_1_Contains_m49862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49862_GenericMethod/* genericMethod */

};
extern Il2CppType StateMachineBehaviourU5BU5D_t5761_0_0_0;
extern Il2CppType StateMachineBehaviourU5BU5D_t5761_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8877_ICollection_1_CopyTo_m49863_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviourU5BU5D_t5761_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49863_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49863_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8877_ICollection_1_CopyTo_m49863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49863_GenericMethod/* genericMethod */

};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
static ParameterInfo ICollection_1_t8877_ICollection_1_Remove_m49864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviour_t1121_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49864_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.StateMachineBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m49864_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8877_ICollection_1_Remove_m49864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49864_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8877_MethodInfos[] =
{
	&ICollection_1_get_Count_m49858_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49859_MethodInfo,
	&ICollection_1_Add_m49860_MethodInfo,
	&ICollection_1_Clear_m49861_MethodInfo,
	&ICollection_1_Contains_m49862_MethodInfo,
	&ICollection_1_CopyTo_m49863_MethodInfo,
	&ICollection_1_Remove_m49864_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8879_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8877_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8879_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8877_0_0_0;
extern Il2CppType ICollection_1_t8877_1_0_0;
struct ICollection_1_t8877;
extern Il2CppGenericClass ICollection_1_t8877_GenericClass;
TypeInfo ICollection_1_t8877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8877_MethodInfos/* methods */
	, ICollection_1_t8877_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8877_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8877_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8877_0_0_0/* byval_arg */
	, &ICollection_1_t8877_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8877_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.StateMachineBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.StateMachineBehaviour>
extern Il2CppType IEnumerator_1_t6986_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49865_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.StateMachineBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49865_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8879_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6986_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49865_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8879_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49865_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8879_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8879_0_0_0;
extern Il2CppType IEnumerable_1_t8879_1_0_0;
struct IEnumerable_1_t8879;
extern Il2CppGenericClass IEnumerable_1_t8879_GenericClass;
TypeInfo IEnumerable_1_t8879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8879_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8879_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8879_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8879_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8879_0_0_0/* byval_arg */
	, &IEnumerable_1_t8879_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8879_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8878_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>
extern MethodInfo IList_1_get_Item_m49866_MethodInfo;
extern MethodInfo IList_1_set_Item_m49867_MethodInfo;
static PropertyInfo IList_1_t8878____Item_PropertyInfo = 
{
	&IList_1_t8878_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49866_MethodInfo/* get */
	, &IList_1_set_Item_m49867_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8878_PropertyInfos[] =
{
	&IList_1_t8878____Item_PropertyInfo,
	NULL
};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
static ParameterInfo IList_1_t8878_IList_1_IndexOf_m49868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviour_t1121_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49868_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49868_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8878_IList_1_IndexOf_m49868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49868_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
static ParameterInfo IList_1_t8878_IList_1_Insert_m49869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviour_t1121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49869_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49869_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8878_IList_1_Insert_m49869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49869_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8878_IList_1_RemoveAt_m49870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49870_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49870_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8878_IList_1_RemoveAt_m49870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49870_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8878_IList_1_get_Item_m49866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49866_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49866_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &StateMachineBehaviour_t1121_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8878_IList_1_get_Item_m49866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49866_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StateMachineBehaviour_t1121_0_0_0;
static ParameterInfo IList_1_t8878_IList_1_set_Item_m49867_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StateMachineBehaviour_t1121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49867_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.StateMachineBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49867_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8878_IList_1_set_Item_m49867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49867_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8878_MethodInfos[] =
{
	&IList_1_IndexOf_m49868_MethodInfo,
	&IList_1_Insert_m49869_MethodInfo,
	&IList_1_RemoveAt_m49870_MethodInfo,
	&IList_1_get_Item_m49866_MethodInfo,
	&IList_1_set_Item_m49867_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8878_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8877_il2cpp_TypeInfo,
	&IEnumerable_1_t8879_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8878_0_0_0;
extern Il2CppType IList_1_t8878_1_0_0;
struct IList_1_t8878;
extern Il2CppGenericClass IList_1_t8878_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8878_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8878_MethodInfos/* methods */
	, IList_1_t8878_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8878_il2cpp_TypeInfo/* element_class */
	, IList_1_t8878_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8878_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8878_0_0_0/* byval_arg */
	, &IList_1_t8878_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8878_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
