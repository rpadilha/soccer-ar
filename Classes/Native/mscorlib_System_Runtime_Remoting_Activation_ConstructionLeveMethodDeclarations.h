﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ConstructionLevelActivator
struct ConstructionLevelActivator_t2037;

// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
 void ConstructionLevelActivator__ctor_m11620 (ConstructionLevelActivator_t2037 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
