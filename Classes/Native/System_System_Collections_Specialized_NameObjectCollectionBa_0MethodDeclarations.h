﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
struct _KeysEnumerator_t1359;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t1358;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
 void _KeysEnumerator__ctor_m6940 (_KeysEnumerator_t1359 * __this, NameObjectCollectionBase_t1358 * ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::get_Current()
 Object_t * _KeysEnumerator_get_Current_m6941 (_KeysEnumerator_t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::MoveNext()
 bool _KeysEnumerator_MoveNext_m6942 (_KeysEnumerator_t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::Reset()
 void _KeysEnumerator_Reset_m6943 (_KeysEnumerator_t1359 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
