﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct EqualityComparer_1_t4361;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct EqualityComparer_1_t4361  : public Object_t
{
};
struct EqualityComparer_1_t4361_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::_default
	EqualityComparer_1_t4361 * ____default_0;
};
