﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t36;
// Vuforia.ObjectTarget
struct ObjectTarget_t598;
// UnityEngine.Texture2D
struct Texture2D_t196;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t597;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.ObjectTargetAbstractBehaviour::.ctor()
 void ObjectTargetAbstractBehaviour__ctor_m376 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::get_ObjectTarget()
 Object_t * ObjectTargetAbstractBehaviour_get_ObjectTarget_m2814 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::OnDrawGizmos()
 void ObjectTargetAbstractBehaviour_OnDrawGizmos_m2815 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::CorrectScaleImpl()
 bool ObjectTargetAbstractBehaviour_CorrectScaleImpl_m382 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::InternalUnregisterTrackable()
 void ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m381 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m383 (ObjectTargetAbstractBehaviour_t36 * __this, Vector3_t73 * ___boundsMin, Vector3_t73 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m384 (ObjectTargetAbstractBehaviour_t36 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetBoundingBox(UnityEngine.Vector3,UnityEngine.Vector3)
 void ObjectTargetAbstractBehaviour_SetBoundingBox_m396 (ObjectTargetAbstractBehaviour_t36 * __this, Vector3_t73  ___minBBox, Vector3_t73  ___maxBBox, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::GetSize()
 Vector3_t73  ObjectTargetAbstractBehaviour_GetSize_m390 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetLength(System.Single)
 void ObjectTargetAbstractBehaviour_SetLength_m391 (ObjectTargetAbstractBehaviour_t36 * __this, float ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetWidth(System.Single)
 void ObjectTargetAbstractBehaviour_SetWidth_m392 (ObjectTargetAbstractBehaviour_t36 * __this, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetHeight(System.Single)
 void ObjectTargetAbstractBehaviour_SetHeight_m393 (ObjectTargetAbstractBehaviour_t36 * __this, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_AspectRatioXY()
 float ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m387 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_AspectRatioXZ()
 float ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m388 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetAspectRatio(System.Single,System.Single)
 bool ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m389 (ObjectTargetAbstractBehaviour_t36 * __this, float ___aspectRatioXY, float ___aspectRatioXZ, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.InitializeObjectTarget(Vuforia.ObjectTarget)
 void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m397 (ObjectTargetAbstractBehaviour_t36 * __this, Object_t * ___objectTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetShowBoundingBox(System.Boolean)
 void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m394 (ObjectTargetAbstractBehaviour_t36 * __this, bool ___showBoundingBox, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_ShowBoundingBox()
 bool ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m395 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_PreviewImage()
 Texture2D_t196 * ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m385 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetPreviewImage(UnityEngine.Texture2D)
 void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m386 (ObjectTargetAbstractBehaviour_t36 * __this, Texture2D_t196 * ___previewImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m377 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m378 (ObjectTargetAbstractBehaviour_t36 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m379 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m380 (ObjectTargetAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
