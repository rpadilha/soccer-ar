﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
struct InternalEnumerator_1_t5109;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.X509.Extensions.KeyUsages
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsages.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30919 (InternalEnumerator_1_t5109 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920 (InternalEnumerator_1_t5109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::Dispose()
 void InternalEnumerator_1_Dispose_m30921 (InternalEnumerator_1_t5109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30922 (InternalEnumerator_1_t5109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30923 (InternalEnumerator_1_t5109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
