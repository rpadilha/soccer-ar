﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// Vuforia.Word
struct Word_t736;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
 void WordAbstractBehaviour_InternalUnregisterTrackable_m553 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
 Object_t * WordAbstractBehaviour_get_Word_m4413 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
 String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555 (WordAbstractBehaviour_t34 * __this, String_t* ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
 int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
 bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
 bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559 (WordAbstractBehaviour_t34 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560 (WordAbstractBehaviour_t34 * __this, Object_t * ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
 void WordAbstractBehaviour__ctor_m548 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550 (WordAbstractBehaviour_t34 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552 (WordAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
