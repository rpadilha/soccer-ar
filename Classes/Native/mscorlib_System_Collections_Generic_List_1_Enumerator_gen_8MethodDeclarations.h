﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>
struct Enumerator_t863;
// System.Object
struct Object_t;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t46;
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t714;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"
#define Enumerator__ctor_m23400(__this, ___l, method) (void)Enumerator__ctor_m14558_gshared((Enumerator_t2837 *)__this, (List_1_t149 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23401(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m23402(__this, method) (void)Enumerator_Dispose_m14560_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m23403(__this, method) (void)Enumerator_VerifyState_m14561_gshared((Enumerator_t2837 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m5074(__this, method) (bool)Enumerator_MoveNext_m14562_gshared((Enumerator_t2837 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m5073(__this, method) (ReconstructionAbstractBehaviour_t46 *)Enumerator_get_Current_m14563_gshared((Enumerator_t2837 *)__this, method)
