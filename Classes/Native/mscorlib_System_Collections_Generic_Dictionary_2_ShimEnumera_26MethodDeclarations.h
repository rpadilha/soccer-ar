﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>
struct ShimEnumerator_t4731;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1016;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3MethodDeclarations.h"
#define ShimEnumerator__ctor_m28673(__this, ___host, method) (void)ShimEnumerator__ctor_m18416_gshared((ShimEnumerator_t3466 *)__this, (Dictionary_2_t3452 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define ShimEnumerator_MoveNext_m28674(__this, method) (bool)ShimEnumerator_MoveNext_m18417_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>::get_Entry()
#define ShimEnumerator_get_Entry_m28675(__this, method) (DictionaryEntry_t1355 )ShimEnumerator_get_Entry_m18418_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>::get_Key()
#define ShimEnumerator_get_Key_m28676(__this, method) (Object_t *)ShimEnumerator_get_Key_m18419_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>::get_Value()
#define ShimEnumerator_get_Value_m28677(__this, method) (Object_t *)ShimEnumerator_get_Value_m18420_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define ShimEnumerator_get_Current_m28678(__this, method) (Object_t *)ShimEnumerator_get_Current_m18421_gshared((ShimEnumerator_t3466 *)__this, method)
