﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t54;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
struct UnityAction_1_t2953  : public MulticastDelegate_t373
{
};
