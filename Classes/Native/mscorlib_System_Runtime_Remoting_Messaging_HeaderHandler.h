﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2069;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Runtime.Remoting.Messaging.HeaderHandler
struct HeaderHandler_t2113  : public MulticastDelegate_t373
{
};
