﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
struct UnityAction_1_t3881;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
struct InvokableCall_1_t3880  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Delegate
	UnityAction_1_t3881 * ___Delegate_0;
};
