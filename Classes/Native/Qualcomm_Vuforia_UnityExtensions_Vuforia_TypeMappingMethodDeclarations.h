﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TypeMapping
struct TypeMapping_t727;
// System.Type
struct Type_t;

// System.UInt16 Vuforia.TypeMapping::GetTypeID(System.Type)
 uint16_t TypeMapping_GetTypeID_m3246 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TypeMapping::.cctor()
 void TypeMapping__cctor_m3247 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
