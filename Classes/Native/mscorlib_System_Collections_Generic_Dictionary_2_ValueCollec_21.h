﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t782;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.ImageTarget>
struct ValueCollection_t921  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.ImageTarget>::dictionary
	Dictionary_2_t782 * ___dictionary_0;
};
