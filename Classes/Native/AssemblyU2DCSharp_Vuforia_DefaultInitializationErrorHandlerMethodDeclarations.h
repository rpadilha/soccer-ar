﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t9;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
 void DefaultInitializationErrorHandler__ctor_m5 (DefaultInitializationErrorHandler_t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
 void DefaultInitializationErrorHandler_Awake_m6 (DefaultInitializationErrorHandler_t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
 void DefaultInitializationErrorHandler_OnGUI_m7 (DefaultInitializationErrorHandler_t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
 void DefaultInitializationErrorHandler_OnDestroy_m8 (DefaultInitializationErrorHandler_t9 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
 void DefaultInitializationErrorHandler_DrawWindowContent_m9 (DefaultInitializationErrorHandler_t9 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
 void DefaultInitializationErrorHandler_SetErrorCode_m10 (DefaultInitializationErrorHandler_t9 * __this, int32_t ___errorCode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
 void DefaultInitializationErrorHandler_SetErrorOccurred_m11 (DefaultInitializationErrorHandler_t9 * __this, bool ___errorOccurred, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
 void DefaultInitializationErrorHandler_OnQCARInitializationError_m12 (DefaultInitializationErrorHandler_t9 * __this, int32_t ___initError, MethodInfo* method) IL2CPP_METHOD_ATTR;
