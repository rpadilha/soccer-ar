﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/FrameState
#pragma pack(push, tp, 1)
struct FrameState_t695 
{
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::trackableDataArray
	IntPtr_t121 ___trackableDataArray_0;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::vbDataArray
	IntPtr_t121 ___vbDataArray_1;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::wordResultArray
	IntPtr_t121 ___wordResultArray_2;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::newWordDataArray
	IntPtr_t121 ___newWordDataArray_3;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::propTrackableDataArray
	IntPtr_t121 ___propTrackableDataArray_4;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::smartTerrainRevisionsArray
	IntPtr_t121 ___smartTerrainRevisionsArray_5;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::updatedSurfacesArray
	IntPtr_t121 ___updatedSurfacesArray_6;
	// System.IntPtr Vuforia.QCARManagerImpl/FrameState::updatedPropsArray
	IntPtr_t121 ___updatedPropsArray_7;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numTrackableResults
	int32_t ___numTrackableResults_8;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numVirtualButtonResults
	int32_t ___numVirtualButtonResults_9;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::frameIndex
	int32_t ___frameIndex_10;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numWordResults
	int32_t ___numWordResults_11;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numNewWords
	int32_t ___numNewWords_12;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numPropTrackableResults
	int32_t ___numPropTrackableResults_13;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numSmartTerrainRevisions
	int32_t ___numSmartTerrainRevisions_14;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numUpdatedSurfaces
	int32_t ___numUpdatedSurfaces_15;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::numUpdatedProps
	int32_t ___numUpdatedProps_16;
	// System.Int32 Vuforia.QCARManagerImpl/FrameState::unused
	int32_t ___unused_17;
};
#pragma pack(pop, tp)
