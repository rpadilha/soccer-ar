﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t233;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t234;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseInputModule>
struct IEnumerable_1_t3178;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseInputModule>
struct IEnumerator_1_t3179;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseInputModule>
struct ICollection_1_t3180;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseInputModule>
struct ReadOnlyCollection_1_t3181;
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t3177;
// System.Predicate`1<UnityEngine.EventSystems.BaseInputModule>
struct Predicate_1_t3182;
// System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>
struct Comparison_1_t3183;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m2058(__this, method) (void)List_1__ctor_m14461_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16088(__this, ___collection, method) (void)List_1__ctor_m14463_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Int32)
#define List_1__ctor_m16089(__this, ___capacity, method) (void)List_1__ctor_m14465_gshared((List_1_t149 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::.cctor()
#define List_1__cctor_m16090(__this/* static, unused */, method) (void)List_1__cctor_m14467_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16091(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14469_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16092(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14471_gshared((List_1_t149 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16093(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14473_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16094(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14475_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16095(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14477_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16096(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14479_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16097(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14481_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16098(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14483_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16099(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14485_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16100(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14487_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16101(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14489_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16102(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14491_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16103(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14493_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16104(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14495_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16105(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14497_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Add(T)
#define List_1_Add_m16106(__this, ___item, method) (void)List_1_Add_m14499_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16107(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14501_gshared((List_1_t149 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16108(__this, ___collection, method) (void)List_1_AddCollection_m14503_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16109(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14505_gshared((List_1_t149 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16110(__this, ___collection, method) (void)List_1_AddRange_m14506_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::AsReadOnly()
#define List_1_AsReadOnly_m16111(__this, method) (ReadOnlyCollection_1_t3181 *)List_1_AsReadOnly_m14508_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Clear()
#define List_1_Clear_m16112(__this, method) (void)List_1_Clear_m14510_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Contains(T)
#define List_1_Contains_m16113(__this, ___item, method) (bool)List_1_Contains_m14512_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16114(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14514_gshared((List_1_t149 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Find(System.Predicate`1<T>)
#define List_1_Find_m16115(__this, ___match, method) (BaseInputModule_t234 *)List_1_Find_m14516_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16116(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14518_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2832 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16117(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14520_gshared((List_1_t149 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2832 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::GetEnumerator()
 Enumerator_t3184  List_1_GetEnumerator_m16118 (List_1_t233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::IndexOf(T)
#define List_1_IndexOf_m16119(__this, ___item, method) (int32_t)List_1_IndexOf_m14522_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16120(__this, ___start, ___delta, method) (void)List_1_Shift_m14524_gshared((List_1_t149 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16121(__this, ___index, method) (void)List_1_CheckIndex_m14526_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Insert(System.Int32,T)
#define List_1_Insert_m16122(__this, ___index, ___item, method) (void)List_1_Insert_m14528_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16123(__this, ___collection, method) (void)List_1_CheckCollection_m14530_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Remove(T)
#define List_1_Remove_m16124(__this, ___item, method) (bool)List_1_Remove_m14532_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16125(__this, ___match, method) (int32_t)List_1_RemoveAll_m14534_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2063(__this, ___index, method) (void)List_1_RemoveAt_m14536_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Reverse()
#define List_1_Reverse_m16126(__this, method) (void)List_1_Reverse_m14538_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Sort()
#define List_1_Sort_m16127(__this, method) (void)List_1_Sort_m14540_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16128(__this, ___comparison, method) (void)List_1_Sort_m14542_gshared((List_1_t149 *)__this, (Comparison_1_t2833 *)___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::ToArray()
#define List_1_ToArray_m16129(__this, method) (BaseInputModuleU5BU5D_t3177*)List_1_ToArray_m14544_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::TrimExcess()
#define List_1_TrimExcess_m16130(__this, method) (void)List_1_TrimExcess_m14546_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Capacity()
#define List_1_get_Capacity_m16131(__this, method) (int32_t)List_1_get_Capacity_m14548_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16132(__this, ___value, method) (void)List_1_set_Capacity_m14550_gshared((List_1_t149 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Count()
#define List_1_get_Count_m2061(__this, method) (int32_t)List_1_get_Count_m14552_gshared((List_1_t149 *)__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::get_Item(System.Int32)
#define List_1_get_Item_m2062(__this, ___index, method) (BaseInputModule_t234 *)List_1_get_Item_m14554_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::set_Item(System.Int32,T)
#define List_1_set_Item_m16133(__this, ___index, ___value, method) (void)List_1_set_Item_m14556_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
