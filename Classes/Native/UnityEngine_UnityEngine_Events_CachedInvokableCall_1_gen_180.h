﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_182.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasGroup>
struct CachedInvokableCall_1_t4918  : public InvokableCall_1_t4919
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasGroup>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
