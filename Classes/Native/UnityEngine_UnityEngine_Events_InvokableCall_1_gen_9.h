﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ImageTargetBehaviour>
struct UnityAction_1_t2821;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetBehaviour>
struct InvokableCall_1_t2820  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetBehaviour>::Delegate
	UnityAction_1_t2821 * ___Delegate_0;
};
