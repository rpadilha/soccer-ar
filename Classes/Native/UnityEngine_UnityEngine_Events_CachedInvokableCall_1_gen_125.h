﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_127.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutAbstractBehaviour>
struct CachedInvokableCall_1_t4476  : public InvokableCall_1_t4477
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
