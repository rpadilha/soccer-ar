﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
struct KeyValuePair_2_t4116 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::key
	Type_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::value
	uint16_t ___value_1;
};
