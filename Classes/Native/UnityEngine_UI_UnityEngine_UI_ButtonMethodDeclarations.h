﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t322;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t320;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t239;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t235;
// System.Collections.IEnumerator
struct IEnumerator_t318;

// System.Void UnityEngine.UI.Button::.ctor()
 void Button__ctor_m1162 (Button_t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
 ButtonClickedEvent_t320 * Button_get_onClick_m1163 (Button_t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
 void Button_set_onClick_m1164 (Button_t322 * __this, ButtonClickedEvent_t320 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
 void Button_Press_m1165 (Button_t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
 void Button_OnPointerClick_m1166 (Button_t322 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
 void Button_OnSubmit_m1167 (Button_t322 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
 Object_t * Button_OnFinishSubmit_m1168 (Button_t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
