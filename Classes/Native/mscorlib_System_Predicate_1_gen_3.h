﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1127;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct Predicate_1_t1247  : public MulticastDelegate_t373
{
};
