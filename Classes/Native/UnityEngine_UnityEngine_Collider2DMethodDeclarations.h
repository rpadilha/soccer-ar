﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider2D
struct Collider2D_t191;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1058;

// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
 Rigidbody2D_t1058 * Collider2D_get_attachedRigidbody_m6272 (Collider2D_t191 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
