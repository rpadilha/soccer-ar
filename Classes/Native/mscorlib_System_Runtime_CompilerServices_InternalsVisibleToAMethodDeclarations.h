﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t948;
// System.String
struct String_t;

// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
 void InternalsVisibleToAttribute__ctor_m5568 (InternalsVisibleToAttribute_t948 * __this, String_t* ___assemblyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
