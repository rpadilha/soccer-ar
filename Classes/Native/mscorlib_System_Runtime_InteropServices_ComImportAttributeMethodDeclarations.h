﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.ComImportAttribute
struct ComImportAttribute_t1765;

// System.Void System.Runtime.InteropServices.ComImportAttribute::.ctor()
 void ComImportAttribute__ctor_m9915 (ComImportAttribute_t1765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
