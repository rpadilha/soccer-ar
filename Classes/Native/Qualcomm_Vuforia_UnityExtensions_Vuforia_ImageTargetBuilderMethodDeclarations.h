﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t645;
// System.String
struct String_t;
// Vuforia.TrackableSource
struct TrackableSource_t630;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Boolean Vuforia.ImageTargetBuilder::Build(System.String,System.Single)
// System.Void Vuforia.ImageTargetBuilder::StartScan()
// System.Void Vuforia.ImageTargetBuilder::StopScan()
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilder::GetFrameQuality()
// Vuforia.TrackableSource Vuforia.ImageTargetBuilder::GetTrackableSource()
// System.Void Vuforia.ImageTargetBuilder::.ctor()
 void ImageTargetBuilder__ctor_m3003 (ImageTargetBuilder_t645 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
