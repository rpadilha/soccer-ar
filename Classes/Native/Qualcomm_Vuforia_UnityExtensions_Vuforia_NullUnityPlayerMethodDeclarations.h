﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t160;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
 void NullUnityPlayer_LoadNativeLibraries_m2869 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
 void NullUnityPlayer_InitializePlatform_m2870 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
 int32_t NullUnityPlayer_Start_m2871 (NullUnityPlayer_t160 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Update()
 void NullUnityPlayer_Update_m2872 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Dispose()
 void NullUnityPlayer_Dispose_m2873 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnPause()
 void NullUnityPlayer_OnPause_m2874 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnResume()
 void NullUnityPlayer_OnResume_m2875 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
 void NullUnityPlayer_OnDestroy_m2876 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
 void NullUnityPlayer__ctor_m414 (NullUnityPlayer_t160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
