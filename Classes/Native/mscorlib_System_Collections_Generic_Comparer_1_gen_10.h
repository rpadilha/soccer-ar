﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UI.ICanvasElement>
struct Comparer_1_t3426;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UI.ICanvasElement>
struct Comparer_1_t3426  : public Object_t
{
};
struct Comparer_1_t3426_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UI.ICanvasElement>::_default
	Comparer_1_t3426 * ____default_0;
};
