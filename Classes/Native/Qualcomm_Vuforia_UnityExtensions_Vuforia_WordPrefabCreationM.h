﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
// Vuforia.WordPrefabCreationMode
struct WordPrefabCreationMode_t732 
{
	// System.Int32 Vuforia.WordPrefabCreationMode::value__
	int32_t ___value___1;
};
