﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t714;
// Vuforia.SmartTerrainBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder.h"
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t715  : public SmartTerrainBuilder_t618
{
	// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilderImpl::mReconstructionBehaviours
	List_1_t714 * ___mReconstructionBehaviours_0;
	// System.Boolean Vuforia.SmartTerrainBuilderImpl::mIsInitialized
	bool ___mIsInitialized_1;
};
