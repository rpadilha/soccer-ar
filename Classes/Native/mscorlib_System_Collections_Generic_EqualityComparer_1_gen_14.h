﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>
struct EqualityComparer_1_t3497;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>
struct EqualityComparer_1_t3497  : public Object_t
{
};
struct EqualityComparer_1_t3497_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>::_default
	EqualityComparer_1_t3497 * ____default_0;
};
