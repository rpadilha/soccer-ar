﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptor
struct WebCamTexAdaptor_t672;
// UnityEngine.Texture
struct Texture_t107;

// System.Boolean Vuforia.WebCamTexAdaptor::get_DidUpdateThisFrame()
// System.Boolean Vuforia.WebCamTexAdaptor::get_IsPlaying()
// UnityEngine.Texture Vuforia.WebCamTexAdaptor::get_Texture()
// System.Void Vuforia.WebCamTexAdaptor::Play()
// System.Void Vuforia.WebCamTexAdaptor::Stop()
// System.Void Vuforia.WebCamTexAdaptor::.ctor()
 void WebCamTexAdaptor__ctor_m3126 (WebCamTexAdaptor_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
