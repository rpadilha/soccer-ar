﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.OidCollection
struct OidCollection_t1434;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.OidEnumerator
struct OidEnumerator_t1450  : public Object_t
{
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::_collection
	OidCollection_t1434 * ____collection_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::_position
	int32_t ____position_1;
};
