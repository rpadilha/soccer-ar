﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.QCARManagerImpl/Obb3D
#pragma pack(push, tp, 1)
struct Obb3D_t687 
{
	// UnityEngine.Vector3 Vuforia.QCARManagerImpl/Obb3D::center
	Vector3_t73  ___center_0;
	// UnityEngine.Vector3 Vuforia.QCARManagerImpl/Obb3D::halfExtents
	Vector3_t73  ___halfExtents_1;
	// System.Single Vuforia.QCARManagerImpl/Obb3D::rotationZ
	float ___rotationZ_2;
	// System.Int32 Vuforia.QCARManagerImpl/Obb3D::unused
	int32_t ___unused_3;
};
#pragma pack(pop, tp)
