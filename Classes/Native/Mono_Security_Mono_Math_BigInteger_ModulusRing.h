﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1590;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1591  : public Object_t
{
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::mod
	BigInteger_t1590 * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::constant
	BigInteger_t1590 * ___constant_1;
};
