﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Player_Script
struct Player_Script_t94;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t95;
// UnityEngine.Collision
struct Collision_t82;

// System.Void Player_Script::.ctor()
 void Player_Script__ctor_m175 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::Awake()
 void Player_Script_Awake_m176 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::Start()
 void Player_Script_Start_m177 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::Case_Controlling()
 void Player_Script_Case_Controlling_m178 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player_Script::NoOneInFront(UnityEngine.GameObject[])
 bool Player_Script_NoOneInFront_m179 (Player_Script_t94 * __this, GameObjectU5BU5D_t95* ___team_players, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::Case_Oponent_Attack()
 void Player_Script_Case_Oponent_Attack_m180 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::LateUpdate()
 void Player_Script_LateUpdate_m181 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::Update()
 void Player_Script_Update_m182 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::OnCollisionStay(UnityEngine.Collision)
 void Player_Script_OnCollisionStay_m183 (Player_Script_t94 * __this, Collision_t82 * ___coll, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player_Script::OnGUI()
 void Player_Script_OnGUI_m184 (Player_Script_t94 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
