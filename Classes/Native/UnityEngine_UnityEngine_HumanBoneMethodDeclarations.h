﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.HumanBone
struct HumanBone_t1076;
struct HumanBone_t1076_marshaled;
// System.String
struct String_t;

// System.String UnityEngine.HumanBone::get_boneName()
 String_t* HumanBone_get_boneName_m6345 (HumanBone_t1076 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
 void HumanBone_set_boneName_m6346 (HumanBone_t1076 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.HumanBone::get_humanName()
 String_t* HumanBone_get_humanName_m6347 (HumanBone_t1076 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
 void HumanBone_set_humanName_m6348 (HumanBone_t1076 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
void HumanBone_t1076_marshal(const HumanBone_t1076& unmarshaled, HumanBone_t1076_marshaled& marshaled);
void HumanBone_t1076_marshal_back(const HumanBone_t1076_marshaled& marshaled, HumanBone_t1076& unmarshaled);
void HumanBone_t1076_marshal_cleanup(HumanBone_t1076_marshaled& marshaled);
