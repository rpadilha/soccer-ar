﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t193;
// UnityEngine.Sprite
struct Sprite_t194;

// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
 void SpriteRenderer_set_sprite_m650 (SpriteRenderer_t193 * __this, Sprite_t194 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
 void SpriteRenderer_SetSprite_INTERNAL_m6099 (SpriteRenderer_t193 * __this, Sprite_t194 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
