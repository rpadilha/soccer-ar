﻿#pragma once
#include <stdint.h>
// System.Array
struct Array_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
struct InternalEnumerator_1_t2970 
{
	// System.Array System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::array
	Array_t * ___array_0;
	// System.Int32 System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::idx
	int32_t ___idx_1;
};
