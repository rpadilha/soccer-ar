﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t41;

// System.Void Vuforia.MultiTargetBehaviour::.ctor()
 void MultiTargetBehaviour__ctor_m77 (MultiTargetBehaviour_t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
