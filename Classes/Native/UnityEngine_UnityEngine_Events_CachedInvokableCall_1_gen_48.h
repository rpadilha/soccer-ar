﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<SetShield>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_44.h"
// UnityEngine.Events.CachedInvokableCall`1<SetShield>
struct CachedInvokableCall_1_t3052  : public InvokableCall_1_t3053
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<SetShield>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
