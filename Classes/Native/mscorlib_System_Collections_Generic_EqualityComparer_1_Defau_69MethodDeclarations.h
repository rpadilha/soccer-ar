﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t4911;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
 void DefaultComparer__ctor_m29725 (DefaultComparer_t4911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m29726 (DefaultComparer_t4911 * __this, UILineInfo_t531  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
 bool DefaultComparer_Equals_m29727 (DefaultComparer_t4911 * __this, UILineInfo_t531  ___x, UILineInfo_t531  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
