﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1342;
// System.String
struct String_t;

// System.Void System.MonoTODOAttribute::.ctor()
 void MonoTODOAttribute__ctor_m6897 (MonoTODOAttribute_t1342 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
 void MonoTODOAttribute__ctor_m6898 (MonoTODOAttribute_t1342 * __this, String_t* ___comment, MethodInfo* method) IL2CPP_METHOD_ATTR;
