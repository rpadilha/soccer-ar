﻿#pragma once
#include <stdint.h>
// Vuforia.InternalEyewear
struct InternalEyewear_t623;
// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t587;
// Vuforia.InternalEyewearUserCalibrator
struct InternalEyewearUserCalibrator_t589;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.InternalEyewear
struct InternalEyewear_t623  : public Object_t
{
	// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::mProfileManager
	InternalEyewearCalibrationProfileManager_t587 * ___mProfileManager_1;
	// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::mCalibrator
	InternalEyewearUserCalibrator_t589 * ___mCalibrator_2;
};
struct InternalEyewear_t623_StaticFields{
	// Vuforia.InternalEyewear Vuforia.InternalEyewear::mInstance
	InternalEyewear_t623 * ___mInstance_0;
};
