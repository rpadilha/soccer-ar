﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ConstraintAxis
#include "AssemblyU2DUnityScript_ConstraintAxis.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// RotationConstraint
struct RotationConstraint_t220  : public MonoBehaviour_t10
{
	// ConstraintAxis RotationConstraint::axis
	int32_t ___axis_2;
	// System.Single RotationConstraint::min
	float ___min_3;
	// System.Single RotationConstraint::max
	float ___max_4;
	// UnityEngine.Transform RotationConstraint::thisTransform
	Transform_t74 * ___thisTransform_5;
	// UnityEngine.Vector3 RotationConstraint::rotateAround
	Vector3_t73  ___rotateAround_6;
	// UnityEngine.Quaternion RotationConstraint::minQuaternion
	Quaternion_t108  ___minQuaternion_7;
	// UnityEngine.Quaternion RotationConstraint::maxQuaternion
	Quaternion_t108  ___maxQuaternion_8;
	// System.Single RotationConstraint::range
	float ___range_9;
};
