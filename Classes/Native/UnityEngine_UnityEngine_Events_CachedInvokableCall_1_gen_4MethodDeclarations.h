﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t2761;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t130;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
 void CachedInvokableCall_1__ctor_m14196_gshared (CachedInvokableCall_1_t2761 * __this, Object_t120 * ___target, MethodInfo_t141 * ___theFunction, Object_t * ___argument, MethodInfo* method);
#define CachedInvokableCall_1__ctor_m14196(__this, ___target, ___theFunction, ___argument, method) (void)CachedInvokableCall_1__ctor_m14196_gshared((CachedInvokableCall_1_t2761 *)__this, (Object_t120 *)___target, (MethodInfo_t141 *)___theFunction, (Object_t *)___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
 void CachedInvokableCall_1_Invoke_m14198_gshared (CachedInvokableCall_1_t2761 * __this, ObjectU5BU5D_t130* ___args, MethodInfo* method);
#define CachedInvokableCall_1_Invoke_m14198(__this, ___args, method) (void)CachedInvokableCall_1_Invoke_m14198_gshared((CachedInvokableCall_1_t2761 *)__this, (ObjectU5BU5D_t130*)___args, method)
