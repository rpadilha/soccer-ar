﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<ObliqueNear>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_58.h"
// UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>
struct CachedInvokableCall_1_t3135  : public InvokableCall_1_t3136
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ObliqueNear>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
