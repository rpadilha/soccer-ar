﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2718;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
 void GenericEqualityComparer_1__ctor_m14127 (GenericEqualityComparer_1_t2718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
 int32_t GenericEqualityComparer_1_GetHashCode_m32134 (GenericEqualityComparer_1_t2718 * __this, DateTimeOffset_t2251  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
 bool GenericEqualityComparer_1_Equals_m32135 (GenericEqualityComparer_1_t2718 * __this, DateTimeOffset_t2251  ___x, DateTimeOffset_t2251  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
