﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/ImageHeaderData
#pragma pack(push, tp, 1)
struct ImageHeaderData_t690 
{
	// System.IntPtr Vuforia.QCARManagerImpl/ImageHeaderData::data
	IntPtr_t121 ___data_0;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::width
	int32_t ___width_1;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::height
	int32_t ___height_2;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::stride
	int32_t ___stride_3;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::bufferWidth
	int32_t ___bufferWidth_4;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::bufferHeight
	int32_t ___bufferHeight_5;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::format
	int32_t ___format_6;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::reallocate
	int32_t ___reallocate_7;
	// System.Int32 Vuforia.QCARManagerImpl/ImageHeaderData::updated
	int32_t ___updated_8;
};
#pragma pack(pop, tp)
