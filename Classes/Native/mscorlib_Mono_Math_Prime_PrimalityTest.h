﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1804;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo ConfidenceFactor_t1806_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
struct PrimalityTest_t1803  : public MulticastDelegate_t373
{
};
