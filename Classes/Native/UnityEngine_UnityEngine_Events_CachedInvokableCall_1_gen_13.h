﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_9.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ImageTargetBehaviour>
struct CachedInvokableCall_1_t2819  : public InvokableCall_1_t2820
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ImageTargetBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
