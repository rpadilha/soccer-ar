﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_136.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamAbstractBehaviour>
struct CachedInvokableCall_1_t4596  : public InvokableCall_1_t4597
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
