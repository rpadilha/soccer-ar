﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_71.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
struct CachedInvokableCall_1_t3321  : public InvokableCall_1_t3322
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PointerInputModule>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
