﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1271;

// System.Void System.MonoTODOAttribute::.ctor()
 void MonoTODOAttribute__ctor_m6800 (MonoTODOAttribute_t1271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
