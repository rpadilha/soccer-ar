﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RangeAttribute
struct RangeAttribute_t501;

// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
 void RangeAttribute__ctor_m2275 (RangeAttribute_t501 * __this, float ___min, float ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
