﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CameraDeviceImpl
struct CameraDeviceImpl_t649;
// Vuforia.WebCamImpl
struct WebCamImpl_t648;
// Vuforia.Image
struct Image_t604;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t646;
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// Vuforia.WebCamImpl Vuforia.CameraDeviceImpl::get_WebCam()
 WebCamImpl_t648 * CameraDeviceImpl_get_WebCam_m3004 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::get_CameraReady()
 bool CameraDeviceImpl_get_CameraReady_m3005 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Init(Vuforia.CameraDevice/CameraDirection)
 bool CameraDeviceImpl_Init_m3006 (CameraDeviceImpl_t649 * __this, int32_t ___cameraDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Deinit()
 bool CameraDeviceImpl_Deinit_m3007 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Start()
 bool CameraDeviceImpl_Start_m3008 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Stop()
 bool CameraDeviceImpl_Stop_m3009 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode()
 VideoModeData_t602  CameraDeviceImpl_GetVideoMode_m3010 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
 VideoModeData_t602  CameraDeviceImpl_GetVideoMode_m3011 (CameraDeviceImpl_t649 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SelectVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
 bool CameraDeviceImpl_SelectVideoMode_m3012 (CameraDeviceImpl_t649 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::GetSelectedVideoMode(Vuforia.CameraDevice/CameraDeviceMode&)
 bool CameraDeviceImpl_GetSelectedVideoMode_m3013 (CameraDeviceImpl_t649 * __this, int32_t* ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SetFlashTorchMode(System.Boolean)
 bool CameraDeviceImpl_SetFlashTorchMode_m3014 (CameraDeviceImpl_t649 * __this, bool ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SetFocusMode(Vuforia.CameraDevice/FocusMode)
 bool CameraDeviceImpl_SetFocusMode_m3015 (CameraDeviceImpl_t649 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
 bool CameraDeviceImpl_SetFrameFormat_m3016 (CameraDeviceImpl_t649 * __this, int32_t ___format, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Image Vuforia.CameraDeviceImpl::GetCameraImage(Vuforia.Image/PIXEL_FORMAT)
 Image_t604 * CameraDeviceImpl_GetCameraImage_m3017 (CameraDeviceImpl_t649 * __this, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::GetCameraDirection()
 int32_t CameraDeviceImpl_GetCameraDirection_m3018 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDeviceImpl::GetAllImages()
 Dictionary_2_t646 * CameraDeviceImpl_GetAllImages_m3019 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::IsDirty()
 bool CameraDeviceImpl_IsDirty_m3020 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::ResetDirtyFlag()
 void CameraDeviceImpl_ResetDirtyFlag_m3021 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::.ctor()
 void CameraDeviceImpl__ctor_m3022 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::ForceFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
 void CameraDeviceImpl_ForceFrameFormat_m3023 (CameraDeviceImpl_t649 * __this, int32_t ___format, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::InitCameraDevice(System.Int32)
 int32_t CameraDeviceImpl_InitCameraDevice_m3024 (CameraDeviceImpl_t649 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::DeinitCameraDevice()
 int32_t CameraDeviceImpl_DeinitCameraDevice_m3025 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::StartCameraDevice()
 int32_t CameraDeviceImpl_StartCameraDevice_m3026 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::StopCameraDevice()
 int32_t CameraDeviceImpl_StopCameraDevice_m3027 (CameraDeviceImpl_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::.cctor()
 void CameraDeviceImpl__cctor_m3028 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
