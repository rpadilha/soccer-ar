﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.HorizontalLayoutGroup>
struct UnityAction_1_t3771;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>
struct InvokableCall_1_t3770  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>::Delegate
	UnityAction_1_t3771 * ___Delegate_0;
};
