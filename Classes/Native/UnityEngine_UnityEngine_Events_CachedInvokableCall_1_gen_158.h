﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_160.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>
struct CachedInvokableCall_1_t4791  : public InvokableCall_1_t4792
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
