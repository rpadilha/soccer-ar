﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
struct UnityAction_1_t2968;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
struct InvokableCall_1_t2967  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Delegate
	UnityAction_1_t2968 * ___Delegate_0;
};
