﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern TypeInfo Char_t371_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t372  : public MulticastDelegate_t373
{
};
