﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Shoot_Script
struct Shoot_Script_t109;

// System.Void Shoot_Script::.ctor()
 void Shoot_Script__ctor_m185 (Shoot_Script_t109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shoot_Script::Update()
 void Shoot_Script_Update_m186 (Shoot_Script_t109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
