﻿#pragma once
#include <stdint.h>
// UnityEngine.ReflectionProbe
struct ReflectionProbe_t989;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Rendering.ReflectionProbeBlendInfo
struct ReflectionProbeBlendInfo_t1103 
{
	// UnityEngine.ReflectionProbe UnityEngine.Rendering.ReflectionProbeBlendInfo::probe
	ReflectionProbe_t989 * ___probe_0;
	// System.Single UnityEngine.Rendering.ReflectionProbeBlendInfo::weight
	float ___weight_1;
};
