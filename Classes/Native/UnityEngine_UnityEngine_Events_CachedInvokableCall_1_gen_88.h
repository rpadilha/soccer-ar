﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ScrollRect>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_88.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ScrollRect>
struct CachedInvokableCall_1_t3632  : public InvokableCall_1_t3633
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ScrollRect>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
