﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/AutoRotationState
struct AutoRotationState_t696;
struct AutoRotationState_t696_marshaled;

void AutoRotationState_t696_marshal(const AutoRotationState_t696& unmarshaled, AutoRotationState_t696_marshaled& marshaled);
void AutoRotationState_t696_marshal_back(const AutoRotationState_t696_marshaled& marshaled, AutoRotationState_t696& unmarshaled);
void AutoRotationState_t696_marshal_cleanup(AutoRotationState_t696_marshaled& marshaled);
