﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<RotationConstraint>
struct UnityAction_1_t3152;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<RotationConstraint>
struct InvokableCall_1_t3151  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<RotationConstraint>::Delegate
	UnityAction_1_t3152 * ___Delegate_0;
};
