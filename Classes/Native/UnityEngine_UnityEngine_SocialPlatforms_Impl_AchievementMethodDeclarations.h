﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1096;
// System.String
struct String_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
 void Achievement__ctor_m6440 (Achievement_t1096 * __this, String_t* ___id, double ___percentCompleted, bool ___completed, bool ___hidden, DateTime_t674  ___lastReportedDate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
 void Achievement__ctor_m6441 (Achievement_t1096 * __this, String_t* ___id, double ___percent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
 void Achievement__ctor_m6442 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
 String_t* Achievement_ToString_m6443 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
 String_t* Achievement_get_id_m6444 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
 void Achievement_set_id_m6445 (Achievement_t1096 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
 double Achievement_get_percentCompleted_m6446 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
 void Achievement_set_percentCompleted_m6447 (Achievement_t1096 * __this, double ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
 bool Achievement_get_completed_m6448 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
 bool Achievement_get_hidden_m6449 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
 DateTime_t674  Achievement_get_lastReportedDate_m6450 (Achievement_t1096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
