﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>
struct Enumerator_t891;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t16;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t759;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m24945 (Enumerator_t891 * __this, Dictionary_2_t759 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24946 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IDictionaryEnumerator.get_Entry()
 DictionaryEntry_t1355  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24947 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IDictionaryEnumerator.get_Key()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24948 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IDictionaryEnumerator.get_Value()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24949 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::MoveNext()
 bool Enumerator_MoveNext_m5322 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::get_Current()
 KeyValuePair_2_t892  Enumerator_get_Current_m5320 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::get_CurrentKey()
 int32_t Enumerator_get_CurrentKey_m24950 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::get_CurrentValue()
 Object_t * Enumerator_get_CurrentValue_m24951 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::VerifyState()
 void Enumerator_VerifyState_m24952 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::VerifyCurrent()
 void Enumerator_VerifyCurrent_m24953 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::Dispose()
 void Enumerator_Dispose_m24954 (Enumerator_t891 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
