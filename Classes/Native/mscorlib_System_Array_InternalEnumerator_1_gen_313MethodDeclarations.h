﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDirection>
struct InternalEnumerator_1_t3858;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDirection>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21093 (InternalEnumerator_1_t3858 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDirection>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21094 (InternalEnumerator_1_t3858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDirection>::Dispose()
 void InternalEnumerator_1_Dispose_m21095 (InternalEnumerator_1_t3858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDirection>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21096 (InternalEnumerator_1_t3858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDirection>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21097 (InternalEnumerator_1_t3858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
