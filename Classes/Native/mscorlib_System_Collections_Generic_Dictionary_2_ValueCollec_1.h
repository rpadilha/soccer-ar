﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
struct Dictionary_2_t650;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Trackable>
struct ValueCollection_t836  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Trackable>::dictionary
	Dictionary_2_t650 * ___dictionary_0;
};
