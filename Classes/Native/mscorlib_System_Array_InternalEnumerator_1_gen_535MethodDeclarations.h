﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>
struct InternalEnumerator_1_t5074;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.StoreName
#include "System_System_Security_Cryptography_X509Certificates_StoreNa.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30744 (InternalEnumerator_1_t5074 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30745 (InternalEnumerator_1_t5074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>::Dispose()
 void InternalEnumerator_1_Dispose_m30746 (InternalEnumerator_1_t5074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30747 (InternalEnumerator_1_t5074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.StoreName>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30748 (InternalEnumerator_1_t5074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
