﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t4;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t607;

// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
 bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m2819 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
 void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m2820 (CloudRecoAbstractBehaviour_t4 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
 bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m2821 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
 void CloudRecoAbstractBehaviour_Initialize_m2822 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
 void CloudRecoAbstractBehaviour_Deinitialize_m2823 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
 void CloudRecoAbstractBehaviour_CheckInitialization_m2824 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
 void CloudRecoAbstractBehaviour_StartCloudReco_m2825 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
 void CloudRecoAbstractBehaviour_StopCloudReco_m2826 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
 void CloudRecoAbstractBehaviour_RegisterEventHandler_m2827 (CloudRecoAbstractBehaviour_t4 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
 bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2828 (CloudRecoAbstractBehaviour_t4 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
 void CloudRecoAbstractBehaviour_OnEnable_m2829 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
 void CloudRecoAbstractBehaviour_OnDisable_m2830 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
 void CloudRecoAbstractBehaviour_Start_m2831 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
 void CloudRecoAbstractBehaviour_Update_m2832 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
 void CloudRecoAbstractBehaviour_OnDestroy_m2833 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnQCARStarted()
 void CloudRecoAbstractBehaviour_OnQCARStarted_m2834 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
 void CloudRecoAbstractBehaviour__ctor_m202 (CloudRecoAbstractBehaviour_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
