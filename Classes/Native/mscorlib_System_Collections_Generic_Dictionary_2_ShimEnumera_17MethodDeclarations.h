﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>
struct ShimEnumerator_t4294;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t761;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25243 (ShimEnumerator_t4294 * __this, Dictionary_2_t761 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::MoveNext()
 bool ShimEnumerator_MoveNext_m25244 (ShimEnumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m25245 (ShimEnumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25246 (ShimEnumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25247 (ShimEnumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25248 (ShimEnumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
