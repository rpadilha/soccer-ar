﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyValuePair_2_t4194;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t742;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m24336(__this, ___key, ___value, method) (void)KeyValuePair_2__ctor_m18343_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Key()
#define KeyValuePair_2_get_Key_m24337(__this, method) (String_t*)KeyValuePair_2_get_Key_m18344_gshared((KeyValuePair_2_t3458 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m24338(__this, ___value, method) (void)KeyValuePair_2_set_Key_m18345_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Value()
#define KeyValuePair_2_get_Value_m24339(__this, method) (List_1_t742 *)KeyValuePair_2_get_Value_m18346_gshared((KeyValuePair_2_t3458 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m24340(__this, ___value, method) (void)KeyValuePair_2_set_Value_m18347_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToString()
#define KeyValuePair_2_ToString_m24341(__this, method) (String_t*)KeyValuePair_2_ToString_m18348_gshared((KeyValuePair_2_t3458 *)__this, method)
