﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t389;

// System.Void UnityEngine.UI.Scrollbar/ScrollEvent::.ctor()
 void ScrollEvent__ctor_m1517 (ScrollEvent_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
