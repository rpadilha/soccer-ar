﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.HideInInspector
struct HideInInspector_t204;

// System.Void UnityEngine.HideInInspector::.ctor()
 void HideInInspector__ctor_m721 (HideInInspector_t204 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
