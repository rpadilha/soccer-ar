﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.FactorySetter
struct FactorySetter_t146;

// System.Void Vuforia.FactorySetter::.ctor()
 void FactorySetter__ctor_m336 (FactorySetter_t146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
