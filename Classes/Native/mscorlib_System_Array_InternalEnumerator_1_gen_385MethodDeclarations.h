﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARUnity/QCARHint>
struct InternalEnumerator_1_t4483;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARUnity/QCARHint>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26942 (InternalEnumerator_1_t4483 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARUnity/QCARHint>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26943 (InternalEnumerator_1_t4483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARUnity/QCARHint>::Dispose()
 void InternalEnumerator_1_Dispose_m26944 (InternalEnumerator_1_t4483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARUnity/QCARHint>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26945 (InternalEnumerator_1_t4483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARUnity/QCARHint>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26946 (InternalEnumerator_1_t4483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
