﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRuntimeUtilities
struct QCARRuntimeUtilities_t156;
// System.String
struct String_t;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"

// System.String Vuforia.QCARRuntimeUtilities::StripFileNameFromPath(System.String)
 String_t* QCARRuntimeUtilities_StripFileNameFromPath_m4319 (Object_t * __this/* static, unused */, String_t* ___fullPath, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.QCARRuntimeUtilities::StripExtensionFromPath(System.String)
 String_t* QCARRuntimeUtilities_StripExtensionFromPath_m4320 (Object_t * __this/* static, unused */, String_t* ___fullPath, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.QCARRuntimeUtilities::get_ScreenOrientation()
 int32_t QCARRuntimeUtilities_get_ScreenOrientation_m4321 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsLandscapeOrientation()
 bool QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsPortraitOrientation()
 bool QCARRuntimeUtilities_get_IsPortraitOrientation_m4323 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::ForceDisableTrackables()
 void QCARRuntimeUtilities_ForceDisableTrackables_m4324 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRuntimeUtilities::IsPlayMode()
 bool QCARRuntimeUtilities_IsPlayMode_m416 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRuntimeUtilities::IsQCAREnabled()
 bool QCARRuntimeUtilities_IsQCAREnabled_m361 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRuntimeUtilities::ScreenSpaceToCameraFrameCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 Vec2I_t675  QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325 (Object_t * __this/* static, unused */, Vector2_t99  ___screenSpaceCoordinate, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 Vector2_t99  QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326 (Object_t * __this/* static, unused */, Vector2_t99  ___cameraFrameCoordinate, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.OrientedBoundingBox Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(Vuforia.OrientedBoundingBox,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 OrientedBoundingBox_t634  QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327 (Object_t * __this/* static, unused */, OrientedBoundingBox_t634  ___cameraFrameObb, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::SelectRectTopLeftAndBottomRightForLandscapeLeft(UnityEngine.Rect,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
 void QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328 (Object_t * __this/* static, unused */, Rect_t103  ___screenSpaceRect, bool ___isMirrored, Vector2_t99 * ___topLeft, Vector2_t99 * ___bottomRight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.QCARRuntimeUtilities::CalculateRectFromLandscapeLeftCorners(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
 Rect_t103  QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329 (Object_t * __this/* static, unused */, Vector2_t99  ___topLeft, Vector2_t99  ___bottomRight, bool ___isMirrored, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::DisableSleepMode()
 void QCARRuntimeUtilities_DisableSleepMode_m4330 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::ResetSleepMode()
 void QCARRuntimeUtilities_ResetSleepMode_m4331 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::PrepareCoordinateConversion(System.Boolean,System.Single&,System.Single&,System.Single&,System.Single&,System.Boolean&)
 void QCARRuntimeUtilities_PrepareCoordinateConversion_m4332 (Object_t * __this/* static, unused */, bool ___isTextureMirrored, float* ___prefixX, float* ___prefixY, float* ___inversionMultiplierX, float* ___inversionMultiplierY, bool* ___isPortrait, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::.ctor()
 void QCARRuntimeUtilities__ctor_m4333 (QCARRuntimeUtilities_t156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRuntimeUtilities::.cctor()
 void QCARRuntimeUtilities__cctor_m4334 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
