﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>
struct Transform_1_t4058;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t667;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m22869 (Transform_1_t4058 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::Invoke(TKey,TValue)
 KeyValuePair_2_t4049  Transform_1_Invoke_m22870 (Transform_1_t4058 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m22871 (Transform_1_t4058 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::EndInvoke(System.IAsyncResult)
 KeyValuePair_2_t4049  Transform_1_EndInvoke_m22872 (Transform_1_t4058 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
