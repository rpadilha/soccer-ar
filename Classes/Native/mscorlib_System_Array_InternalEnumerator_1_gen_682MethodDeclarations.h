﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
struct InternalEnumerator_1_t5226;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31504 (InternalEnumerator_1_t5226 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31505 (InternalEnumerator_1_t5226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::Dispose()
 void InternalEnumerator_1_Dispose_m31506 (InternalEnumerator_1_t5226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31507 (InternalEnumerator_1_t5226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31508 (InternalEnumerator_1_t5226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
