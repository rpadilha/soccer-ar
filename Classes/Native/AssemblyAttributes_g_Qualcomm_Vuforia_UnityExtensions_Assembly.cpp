﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo InternalsVisibleToAttribute_t948_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
extern MethodInfo InternalsVisibleToAttribute__ctor_m5568_MethodInfo;
extern TypeInfo DebuggableAttribute_t230_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
extern MethodInfo DebuggableAttribute__ctor_m861_MethodInfo;
extern TypeInfo CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern MethodInfo CompilationRelaxationsAttribute__ctor_m5569_MethodInfo;
extern TypeInfo RuntimeCompatibilityAttribute_t205_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern MethodInfo RuntimeCompatibilityAttribute__ctor_m729_MethodInfo;
extern MethodInfo RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m730_MethodInfo;
extern TypeInfo ExtensionAttribute_t822_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern MethodInfo ExtensionAttribute__ctor_m4522_MethodInfo;
void g_Qualcomm_Vuforia_UnityExtensions_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Eyewear"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t230 * tmp;
		tmp = (DebuggableAttribute_t230 *)il2cpp_codegen_object_new (&DebuggableAttribute_t230_il2cpp_TypeInfo);
		DebuggableAttribute__ctor_m861(tmp, 2, &DebuggableAttribute__ctor_m861_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t949 * tmp;
		tmp = (CompilationRelaxationsAttribute_t949 *)il2cpp_codegen_object_new (&CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo);
		CompilationRelaxationsAttribute__ctor_m5569(tmp, 8, &CompilationRelaxationsAttribute__ctor_m5569_MethodInfo);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t205 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t205 *)il2cpp_codegen_object_new (&RuntimeCompatibilityAttribute_t205_il2cpp_TypeInfo);
		RuntimeCompatibilityAttribute__ctor_m729(tmp, &RuntimeCompatibilityAttribute__ctor_m729_MethodInfo);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m730(tmp, true, &RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m730_MethodInfo);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.NUnitTests"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Editor"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Premium.Editor"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Premium"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.UnitTests"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_Qualcomm_Vuforia_UnityExtensions_Assembly__CustomAttributeCache = {
10,
NULL,
&g_Qualcomm_Vuforia_UnityExtensions_Assembly_CustomAttributesCacheGenerator
};
