﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct Enumerator_t3210;
// System.Object
struct Object_t;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3209;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
 void Enumerator__ctor_m16415_gshared (Enumerator_t3210 * __this, Stack_1_t3209 * ___t, MethodInfo* method);
#define Enumerator__ctor_m16415(__this, ___t, method) (void)Enumerator__ctor_m16415_gshared((Enumerator_t3210 *)__this, (Stack_1_t3209 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16416_gshared (Enumerator_t3210 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16416(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m16416_gshared((Enumerator_t3210 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
 void Enumerator_Dispose_m16417_gshared (Enumerator_t3210 * __this, MethodInfo* method);
#define Enumerator_Dispose_m16417(__this, method) (void)Enumerator_Dispose_m16417_gshared((Enumerator_t3210 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
 bool Enumerator_MoveNext_m16418_gshared (Enumerator_t3210 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m16418(__this, method) (bool)Enumerator_MoveNext_m16418_gshared((Enumerator_t3210 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m16419_gshared (Enumerator_t3210 * __this, MethodInfo* method);
#define Enumerator_get_Current_m16419(__this, method) (Object_t *)Enumerator_get_Current_m16419_gshared((Enumerator_t3210 *)__this, method)
