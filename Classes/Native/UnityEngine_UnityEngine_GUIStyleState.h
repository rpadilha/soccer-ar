﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t999;
// UnityEngine.Texture2D
struct Texture2D_t196;
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.GUIStyleState
struct GUIStyleState_t1017  : public Object_t
{
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	IntPtr_t121 ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t999 * ___m_SourceStyle_1;
	// UnityEngine.Texture2D UnityEngine.GUIStyleState::m_Background
	Texture2D_t196 * ___m_Background_2;
};
