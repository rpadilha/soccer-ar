﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.TimeSpan>
struct InternalEnumerator_1_t5291;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31827 (InternalEnumerator_1_t5291 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31828 (InternalEnumerator_1_t5291 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
 void InternalEnumerator_1_Dispose_m31829 (InternalEnumerator_1_t5291 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31830 (InternalEnumerator_1_t5291 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
 TimeSpan_t852  InternalEnumerator_1_get_Current_m31831 (InternalEnumerator_1_t5291 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
