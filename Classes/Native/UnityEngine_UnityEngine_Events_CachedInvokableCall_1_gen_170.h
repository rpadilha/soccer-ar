﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_172.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>
struct CachedInvokableCall_1_t4850  : public InvokableCall_1_t4851
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
