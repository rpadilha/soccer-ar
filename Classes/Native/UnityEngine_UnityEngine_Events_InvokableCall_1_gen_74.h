﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseRaycaster>
struct UnityAction_1_t3377;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>
struct InvokableCall_1_t3376  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>::Delegate
	UnityAction_1_t3377 * ___Delegate_0;
};
