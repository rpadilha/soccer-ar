﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t739;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t747;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t734;
// System.Collections.Generic.IEnumerator`1<Vuforia.WordResult>
struct IEnumerator_1_t944;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.ICollection`1<Vuforia.WordResult>
struct ICollection_1_t4151;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordResult>
struct ReadOnlyCollection_1_t4152;
// Vuforia.WordResult[]
struct WordResultU5BU5D_t4135;
// System.Predicate`1<Vuforia.WordResult>
struct Predicate_1_t4153;
// System.Comparison`1<Vuforia.WordResult>
struct Comparison_1_t4154;
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m5190(__this, method) (void)List_1__ctor_m14461_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23784(__this, ___collection, method) (void)List_1__ctor_m14463_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Int32)
#define List_1__ctor_m23785(__this, ___capacity, method) (void)List_1__ctor_m14465_gshared((List_1_t149 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.cctor()
#define List_1__cctor_m23786(__this/* static, unused */, method) (void)List_1__cctor_m14467_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23787(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14469_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23788(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14471_gshared((List_1_t149 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23789(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14473_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23790(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14475_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23791(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14477_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23792(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14479_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23793(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14481_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23794(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14483_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23795(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14485_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23796(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14487_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23797(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14489_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23798(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14491_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23799(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14493_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23800(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14495_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23801(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14497_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Add(T)
#define List_1_Add_m5153(__this, ___item, method) (void)List_1_Add_m14499_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23802(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14501_gshared((List_1_t149 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23803(__this, ___collection, method) (void)List_1_AddCollection_m14503_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23804(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14505_gshared((List_1_t149 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23805(__this, ___collection, method) (void)List_1_AddRange_m14506_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::AsReadOnly()
#define List_1_AsReadOnly_m23806(__this, method) (ReadOnlyCollection_1_t4152 *)List_1_AsReadOnly_m14508_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Clear()
#define List_1_Clear_m5147(__this, method) (void)List_1_Clear_m14510_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Contains(T)
#define List_1_Contains_m23807(__this, ___item, method) (bool)List_1_Contains_m14512_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23808(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14514_gshared((List_1_t149 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::Find(System.Predicate`1<T>)
#define List_1_Find_m23809(__this, ___match, method) (WordResult_t747 *)List_1_Find_m14516_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23810(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14518_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2832 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23811(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14520_gshared((List_1_t149 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2832 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.WordResult>::GetEnumerator()
 Enumerator_t876  List_1_GetEnumerator_m5170 (List_1_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::IndexOf(T)
#define List_1_IndexOf_m23812(__this, ___item, method) (int32_t)List_1_IndexOf_m14522_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23813(__this, ___start, ___delta, method) (void)List_1_Shift_m14524_gshared((List_1_t149 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23814(__this, ___index, method) (void)List_1_CheckIndex_m14526_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Insert(System.Int32,T)
#define List_1_Insert_m23815(__this, ___index, ___item, method) (void)List_1_Insert_m14528_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23816(__this, ___collection, method) (void)List_1_CheckCollection_m14530_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Remove(T)
#define List_1_Remove_m23817(__this, ___item, method) (bool)List_1_Remove_m14532_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23818(__this, ___match, method) (int32_t)List_1_RemoveAll_m14534_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23819(__this, ___index, method) (void)List_1_RemoveAt_m14536_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Reverse()
#define List_1_Reverse_m23820(__this, method) (void)List_1_Reverse_m14538_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort()
#define List_1_Sort_m23821(__this, method) (void)List_1_Sort_m14540_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23822(__this, ___comparison, method) (void)List_1_Sort_m14542_gshared((List_1_t149 *)__this, (Comparison_1_t2833 *)___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.WordResult>::ToArray()
#define List_1_ToArray_m23823(__this, method) (WordResultU5BU5D_t4135*)List_1_ToArray_m14544_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::TrimExcess()
#define List_1_TrimExcess_m23824(__this, method) (void)List_1_TrimExcess_m14546_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Capacity()
#define List_1_get_Capacity_m23825(__this, method) (int32_t)List_1_get_Capacity_m14548_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23826(__this, ___value, method) (void)List_1_set_Capacity_m14550_gshared((List_1_t149 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Count()
#define List_1_get_Count_m23827(__this, method) (int32_t)List_1_get_Count_m14552_gshared((List_1_t149 *)__this, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::get_Item(System.Int32)
#define List_1_get_Item_m23828(__this, ___index, method) (WordResult_t747 *)List_1_get_Item_m14554_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Item(System.Int32,T)
#define List_1_set_Item_m23829(__this, ___index, ___value, method) (void)List_1_set_Item_m14556_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
