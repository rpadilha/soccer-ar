﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t594;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Trackable,System.Int32>
struct Transform_1_t3983  : public MulticastDelegate_t373
{
};
