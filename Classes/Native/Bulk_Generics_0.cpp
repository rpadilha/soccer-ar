﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t5930_il2cpp_TypeInfo;

// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"

// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42363_MethodInfo;
static PropertyInfo IEnumerator_1_t5930____Current_PropertyInfo = 
{
	&IEnumerator_1_t5930_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42363_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5930_PropertyInfos[] =
{
	&IEnumerator_1_t5930____Current_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42363_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42363_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5930_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneBehaviour_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42363_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5930_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42363_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5930_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5930_0_0_0;
extern Il2CppType IEnumerator_1_t5930_1_0_0;
struct IEnumerator_1_t5930;
extern Il2CppGenericClass IEnumerator_1_t5930_GenericClass;
TypeInfo IEnumerator_1_t5930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5930_MethodInfos/* methods */
	, IEnumerator_1_t5930_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5930_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5930_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5930_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5930_0_0_0/* byval_arg */
	, &IEnumerator_1_t5930_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t499_il2cpp_TypeInfo;

// System.Object
#include "mscorlib_System_Object.h"


// T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Object>
extern MethodInfo IEnumerator_1_get_Current_m36248_MethodInfo;
static PropertyInfo IEnumerator_1_t499____Current_PropertyInfo = 
{
	&IEnumerator_1_t499_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m36248_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t499_PropertyInfos[] =
{
	&IEnumerator_1_t499____Current_PropertyInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m36248_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current()
MethodInfo IEnumerator_1_get_Current_m36248_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t499_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m36248_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t499_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m36248_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t499_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t499_0_0_0;
extern Il2CppType IEnumerator_1_t499_1_0_0;
struct IEnumerator_1_t499;
extern Il2CppGenericClass IEnumerator_1_t499_GenericClass;
TypeInfo IEnumerator_1_t499_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t499_MethodInfos/* methods */
	, IEnumerator_1_t499_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t499_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t499_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t499_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t499_0_0_0/* byval_arg */
	, &IEnumerator_1_t499_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t499_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2751_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14163_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBackgroundPlaneBehaviour_t1_m32235_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisBackgroundPlaneBehaviour_t1_m32235(__this, p0, method) (BackgroundPlaneBehaviour_t1 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2751____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2751, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2751____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2751, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2751_FieldInfos[] =
{
	&InternalEnumerator_1_t2751____array_0_FieldInfo,
	&InternalEnumerator_1_t2751____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2751____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2751_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2751____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2751_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14163_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2751_PropertyInfos[] =
{
	&InternalEnumerator_1_t2751____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2751____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2751_InternalEnumerator_1__ctor_m14155_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14155_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14155_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2751_InternalEnumerator_1__ctor_m14155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14155_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14159_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14159_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14159_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14161_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14161_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14161_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14163_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14163_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneBehaviour_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14163_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2751_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14155_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_MethodInfo,
	&InternalEnumerator_1_Dispose_m14159_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14161_MethodInfo,
	&InternalEnumerator_1_get_Current_m14163_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m14161_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14159_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2751_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14157_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14161_MethodInfo,
	&InternalEnumerator_1_Dispose_m14159_MethodInfo,
	&InternalEnumerator_1_get_Current_m14163_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2751_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5930_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2751_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5930_il2cpp_TypeInfo, 7},
};
extern TypeInfo BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2751_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14163_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBackgroundPlaneBehaviour_t1_m32235_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2751_0_0_0;
extern Il2CppType InternalEnumerator_1_t2751_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2751_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2751_MethodInfos/* methods */
	, InternalEnumerator_1_t2751_PropertyInfos/* properties */
	, InternalEnumerator_1_t2751_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2751_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2751_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2751_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2751_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2751_1_0_0/* this_arg */
	, InternalEnumerator_1_t2751_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2751_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2751_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2751)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2752_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

extern TypeInfo Object_t_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14164_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObject_t_m32233_MethodInfo;


// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m14156_MethodInfo;
 void InternalEnumerator_1__ctor_m14156_gshared (InternalEnumerator_1_t2752 * __this, Array_t * ___array, MethodInfo* method)
{
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (InternalEnumerator_1_t2752 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = L_0;
		return ((Object_t *)L_1);
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m14160_MethodInfo;
 void InternalEnumerator_1_Dispose_m14160_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m14162_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m14162_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
 Object_t * InternalEnumerator_1_get_Current_m14164_gshared (InternalEnumerator_1_t2752 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		Object_t * L_8 = (( Object_t * (*) (Array_t * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Object>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2752____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2752, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2752____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2752, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2752_FieldInfos[] =
{
	&InternalEnumerator_1_t2752____array_0_FieldInfo,
	&InternalEnumerator_1_t2752____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2752____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2752_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2752____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2752_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14164_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2752_PropertyInfos[] =
{
	&InternalEnumerator_1_t2752____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2752____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2752_InternalEnumerator_1__ctor_m14156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14156_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14156_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2752_InternalEnumerator_1__ctor_m14156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14156_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14160_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14160_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14160_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14162_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14162_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14162_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14164_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14164_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14164_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2752_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14156_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo,
	&InternalEnumerator_1_Dispose_m14160_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14162_MethodInfo,
	&InternalEnumerator_1_get_Current_m14164_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2752_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14162_MethodInfo,
	&InternalEnumerator_1_Dispose_m14160_MethodInfo,
	&InternalEnumerator_1_get_Current_m14164_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2752_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t499_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2752_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t499_il2cpp_TypeInfo, 7},
};
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2752_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14164_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObject_t_m32233_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2752_0_0_0;
extern Il2CppType InternalEnumerator_1_t2752_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2752_GenericClass;
TypeInfo InternalEnumerator_1_t2752_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2752_MethodInfos/* methods */
	, InternalEnumerator_1_t2752_PropertyInfos/* properties */
	, InternalEnumerator_1_t2752_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2752_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2752_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2752_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2752_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2752_1_0_0/* this_arg */
	, InternalEnumerator_1_t2752_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2752_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2752_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2752)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t2835_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Object>
extern MethodInfo ICollection_1_get_Count_m36794_MethodInfo;
static PropertyInfo ICollection_1_t2835____Count_PropertyInfo = 
{
	&ICollection_1_t2835_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m36794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42364_MethodInfo;
static PropertyInfo ICollection_1_t2835____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t2835_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42364_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t2835_PropertyInfos[] =
{
	&ICollection_1_t2835____Count_PropertyInfo,
	&ICollection_1_t2835____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m36794_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count()
MethodInfo ICollection_1_get_Count_m36794_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m36794_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42364_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42364_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42364_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ICollection_1_t2835_ICollection_1_Add_m42365_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42365_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(T)
MethodInfo ICollection_1_Add_m42365_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t2835_ICollection_1_Add_m42365_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42365_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42366_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear()
MethodInfo ICollection_1_Clear_m42366_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42366_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ICollection_1_t2835_ICollection_1_Contains_m36246_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m36246_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T)
MethodInfo ICollection_1_Contains_m36246_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t2835_ICollection_1_Contains_m36246_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m36246_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t2835_ICollection_1_CopyTo_m37090_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m37090_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m37090_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t2835_ICollection_1_CopyTo_m37090_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m37090_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ICollection_1_t2835_ICollection_1_Remove_m42367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42367_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(T)
MethodInfo ICollection_1_Remove_m42367_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t2835_ICollection_1_Remove_m42367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42367_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t2835_MethodInfos[] =
{
	&ICollection_1_get_Count_m36794_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42364_MethodInfo,
	&ICollection_1_Add_m42365_MethodInfo,
	&ICollection_1_Clear_m42366_MethodInfo,
	&ICollection_1_Contains_m36246_MethodInfo,
	&ICollection_1_CopyTo_m37090_MethodInfo,
	&ICollection_1_Remove_m42367_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t2834_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t2835_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t2834_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t2835_0_0_0;
extern Il2CppType ICollection_1_t2835_1_0_0;
struct ICollection_1_t2835;
extern Il2CppGenericClass ICollection_1_t2835_GenericClass;
TypeInfo ICollection_1_t2835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t2835_MethodInfos/* methods */
	, ICollection_1_t2835_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t2835_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t2835_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t2835_0_0_0/* byval_arg */
	, &ICollection_1_t2835_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t2835_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Object>
extern Il2CppType IEnumerator_1_t499_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m36247_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m36247_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t2834_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t499_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m36247_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t2834_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m36247_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t2834_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t2834_0_0_0;
extern Il2CppType IEnumerable_1_t2834_1_0_0;
struct IEnumerable_1_t2834;
extern Il2CppGenericClass IEnumerable_1_t2834_GenericClass;
TypeInfo IEnumerable_1_t2834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t2834_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t2834_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t2834_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t2834_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t2834_0_0_0/* byval_arg */
	, &IEnumerable_1_t2834_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t2834_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t2838_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Object>
extern MethodInfo IList_1_get_Item_m37093_MethodInfo;
extern MethodInfo IList_1_set_Item_m42368_MethodInfo;
static PropertyInfo IList_1_t2838____Item_PropertyInfo = 
{
	&IList_1_t2838_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m37093_MethodInfo/* get */
	, &IList_1_set_Item_m42368_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t2838_PropertyInfos[] =
{
	&IList_1_t2838____Item_PropertyInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IList_1_t2838_IList_1_IndexOf_m42369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42369_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42369_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t2838_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t2838_IList_1_IndexOf_m42369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42369_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IList_1_t2838_IList_1_Insert_m42370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42370_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42370_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t2838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t2838_IList_1_Insert_m42370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42370_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t2838_IList_1_RemoveAt_m42371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42371_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42371_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t2838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t2838_IList_1_RemoveAt_m42371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42371_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t2838_IList_1_get_Item_m37093_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m37093_GenericMethod;
// T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m37093_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t2838_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t2838_IList_1_get_Item_m37093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m37093_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IList_1_t2838_IList_1_set_Item_m42368_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42368_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42368_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t2838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t2838_IList_1_set_Item_m42368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42368_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t2838_MethodInfos[] =
{
	&IList_1_IndexOf_m42369_MethodInfo,
	&IList_1_Insert_m42370_MethodInfo,
	&IList_1_RemoveAt_m42371_MethodInfo,
	&IList_1_get_Item_m37093_MethodInfo,
	&IList_1_set_Item_m42368_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t2838_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t2835_il2cpp_TypeInfo,
	&IEnumerable_1_t2834_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t2838_0_0_0;
extern Il2CppType IList_1_t2838_1_0_0;
struct IList_1_t2838;
extern Il2CppGenericClass IList_1_t2838_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t2838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t2838_MethodInfos/* methods */
	, IList_1_t2838_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t2838_il2cpp_TypeInfo/* element_class */
	, IList_1_t2838_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t2838_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t2838_0_0_0/* byval_arg */
	, &IList_1_t2838_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t2838_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7533_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>
extern MethodInfo ICollection_1_get_Count_m42372_MethodInfo;
static PropertyInfo ICollection_1_t7533____Count_PropertyInfo = 
{
	&ICollection_1_t7533_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42373_MethodInfo;
static PropertyInfo ICollection_1_t7533____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7533_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7533_PropertyInfos[] =
{
	&ICollection_1_t7533____Count_PropertyInfo,
	&ICollection_1_t7533____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42372_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42372_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42372_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42373_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42373_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42373_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_Add_m42374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42374_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42374_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7533_ICollection_1_Add_m42374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42374_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42375_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42375_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42375_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_Contains_m42376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42376_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42376_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7533_ICollection_1_Contains_m42376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42376_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviourU5BU5D_t5358_0_0_0;
extern Il2CppType BackgroundPlaneBehaviourU5BU5D_t5358_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_CopyTo_m42377_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviourU5BU5D_t5358_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42377_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42377_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7533_ICollection_1_CopyTo_m42377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42377_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_Remove_m42378_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42378_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42378_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7533_ICollection_1_Remove_m42378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42378_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7533_MethodInfos[] =
{
	&ICollection_1_get_Count_m42372_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42373_MethodInfo,
	&ICollection_1_Add_m42374_MethodInfo,
	&ICollection_1_Clear_m42375_MethodInfo,
	&ICollection_1_Contains_m42376_MethodInfo,
	&ICollection_1_CopyTo_m42377_MethodInfo,
	&ICollection_1_Remove_m42378_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7535_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7533_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7535_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7533_0_0_0;
extern Il2CppType ICollection_1_t7533_1_0_0;
struct ICollection_1_t7533;
extern Il2CppGenericClass ICollection_1_t7533_GenericClass;
TypeInfo ICollection_1_t7533_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7533_MethodInfos/* methods */
	, ICollection_1_t7533_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7533_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7533_0_0_0/* byval_arg */
	, &ICollection_1_t7533_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7533_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType IEnumerator_1_t5930_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42379_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42379_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7535_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5930_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42379_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7535_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42379_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7535_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7535_0_0_0;
extern Il2CppType IEnumerable_1_t7535_1_0_0;
struct IEnumerable_1_t7535;
extern Il2CppGenericClass IEnumerable_1_t7535_GenericClass;
TypeInfo IEnumerable_1_t7535_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7535_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7535_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7535_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7535_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7535_0_0_0/* byval_arg */
	, &IEnumerable_1_t7535_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7535_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7534_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>
extern MethodInfo IList_1_get_Item_m42380_MethodInfo;
extern MethodInfo IList_1_set_Item_m42381_MethodInfo;
static PropertyInfo IList_1_t7534____Item_PropertyInfo = 
{
	&IList_1_t7534_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42380_MethodInfo/* get */
	, &IList_1_set_Item_m42381_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7534_PropertyInfos[] =
{
	&IList_1_t7534____Item_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_IndexOf_m42382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42382_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42382_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7534_IList_1_IndexOf_m42382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42382_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_Insert_m42383_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42383_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42383_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7534_IList_1_Insert_m42383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42383_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_RemoveAt_m42384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42384_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42384_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7534_IList_1_RemoveAt_m42384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_get_Item_m42380_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42380_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42380_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneBehaviour_t1_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7534_IList_1_get_Item_m42380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42380_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_set_Item_m42381_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42381_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42381_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7534_IList_1_set_Item_m42381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42381_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7534_MethodInfos[] =
{
	&IList_1_IndexOf_m42382_MethodInfo,
	&IList_1_Insert_m42383_MethodInfo,
	&IList_1_RemoveAt_m42384_MethodInfo,
	&IList_1_get_Item_m42380_MethodInfo,
	&IList_1_set_Item_m42381_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7534_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7533_il2cpp_TypeInfo,
	&IEnumerable_1_t7535_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7534_0_0_0;
extern Il2CppType IList_1_t7534_1_0_0;
struct IList_1_t7534;
extern Il2CppGenericClass IList_1_t7534_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7534_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7534_MethodInfos/* methods */
	, IList_1_t7534_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7534_il2cpp_TypeInfo/* element_class */
	, IList_1_t7534_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7534_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7534_0_0_0/* byval_arg */
	, &IList_1_t7534_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7534_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7536_il2cpp_TypeInfo;

// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42385_MethodInfo;
static PropertyInfo ICollection_1_t7536____Count_PropertyInfo = 
{
	&ICollection_1_t7536_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42385_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42386_MethodInfo;
static PropertyInfo ICollection_1_t7536____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7536_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7536_PropertyInfos[] =
{
	&ICollection_1_t7536____Count_PropertyInfo,
	&ICollection_1_t7536____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42385_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42385_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42385_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42386_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42386_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42386_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_Add_m42387_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t2_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42387_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42387_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7536_ICollection_1_Add_m42387_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42387_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42388_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42388_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42388_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_Contains_m42389_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t2_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42389_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42389_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7536_ICollection_1_Contains_m42389_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42389_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviourU5BU5D_t938_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviourU5BU5D_t938_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_CopyTo_m42390_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviourU5BU5D_t938_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42390_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42390_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7536_ICollection_1_CopyTo_m42390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42390_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_Remove_m42391_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t2_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42391_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42391_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7536_ICollection_1_Remove_m42391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42391_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7536_MethodInfos[] =
{
	&ICollection_1_get_Count_m42385_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42386_MethodInfo,
	&ICollection_1_Add_m42387_MethodInfo,
	&ICollection_1_Clear_m42388_MethodInfo,
	&ICollection_1_Contains_m42389_MethodInfo,
	&ICollection_1_CopyTo_m42390_MethodInfo,
	&ICollection_1_Remove_m42391_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7538_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7536_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7538_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7536_0_0_0;
extern Il2CppType ICollection_1_t7536_1_0_0;
struct ICollection_1_t7536;
extern Il2CppGenericClass ICollection_1_t7536_GenericClass;
TypeInfo ICollection_1_t7536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7536_MethodInfos/* methods */
	, ICollection_1_t7536_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7536_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7536_0_0_0/* byval_arg */
	, &ICollection_1_t7536_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7536_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5932_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42392_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42392_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7538_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5932_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42392_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7538_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42392_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7538_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7538_0_0_0;
extern Il2CppType IEnumerable_1_t7538_1_0_0;
struct IEnumerable_1_t7538;
extern Il2CppGenericClass IEnumerable_1_t7538_GenericClass;
TypeInfo IEnumerable_1_t7538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7538_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7538_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7538_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7538_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7538_0_0_0/* byval_arg */
	, &IEnumerable_1_t7538_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7538_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5932_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42393_MethodInfo;
static PropertyInfo IEnumerator_1_t5932____Current_PropertyInfo = 
{
	&IEnumerator_1_t5932_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42393_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5932_PropertyInfos[] =
{
	&IEnumerator_1_t5932____Current_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42393_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42393_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5932_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneAbstractBehaviour_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42393_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5932_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42393_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5932_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5932_0_0_0;
extern Il2CppType IEnumerator_1_t5932_1_0_0;
struct IEnumerator_1_t5932;
extern Il2CppGenericClass IEnumerator_1_t5932_GenericClass;
TypeInfo IEnumerator_1_t5932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5932_MethodInfos/* methods */
	, IEnumerator_1_t5932_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5932_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5932_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5932_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5932_0_0_0/* byval_arg */
	, &IEnumerator_1_t5932_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2753_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1MethodDeclarations.h"

extern TypeInfo BackgroundPlaneAbstractBehaviour_t2_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14169_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBackgroundPlaneAbstractBehaviour_t2_m32255_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisBackgroundPlaneAbstractBehaviour_t2_m32255(__this, p0, method) (BackgroundPlaneAbstractBehaviour_t2 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2753____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2753, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2753____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2753, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2753_FieldInfos[] =
{
	&InternalEnumerator_1_t2753____array_0_FieldInfo,
	&InternalEnumerator_1_t2753____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2753____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2753_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2753____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2753_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14169_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2753_PropertyInfos[] =
{
	&InternalEnumerator_1_t2753____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2753____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2753_InternalEnumerator_1__ctor_m14165_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14165_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14165_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2753_InternalEnumerator_1__ctor_m14165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14165_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14167_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14167_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14167_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14168_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14168_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14168_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14169_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14169_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneAbstractBehaviour_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14169_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2753_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14165_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_MethodInfo,
	&InternalEnumerator_1_Dispose_m14167_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14168_MethodInfo,
	&InternalEnumerator_1_get_Current_m14169_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14168_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14167_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2753_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14166_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14168_MethodInfo,
	&InternalEnumerator_1_Dispose_m14167_MethodInfo,
	&InternalEnumerator_1_get_Current_m14169_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2753_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5932_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2753_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5932_il2cpp_TypeInfo, 7},
};
extern TypeInfo BackgroundPlaneAbstractBehaviour_t2_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2753_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14169_MethodInfo/* Method Usage */,
	&BackgroundPlaneAbstractBehaviour_t2_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBackgroundPlaneAbstractBehaviour_t2_m32255_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2753_0_0_0;
extern Il2CppType InternalEnumerator_1_t2753_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2753_GenericClass;
TypeInfo InternalEnumerator_1_t2753_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2753_MethodInfos/* methods */
	, InternalEnumerator_1_t2753_PropertyInfos/* properties */
	, InternalEnumerator_1_t2753_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2753_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2753_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2753_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2753_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2753_1_0_0/* this_arg */
	, InternalEnumerator_1_t2753_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2753_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2753_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2753)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7537_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42394_MethodInfo;
extern MethodInfo IList_1_set_Item_m42395_MethodInfo;
static PropertyInfo IList_1_t7537____Item_PropertyInfo = 
{
	&IList_1_t7537_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42394_MethodInfo/* get */
	, &IList_1_set_Item_m42395_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7537_PropertyInfos[] =
{
	&IList_1_t7537____Item_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_IndexOf_m42396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t2_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42396_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42396_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7537_IList_1_IndexOf_m42396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42396_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_Insert_m42397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t2_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42397_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42397_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7537_IList_1_Insert_m42397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42397_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_RemoveAt_m42398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42398_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42398_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7537_IList_1_RemoveAt_m42398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_get_Item_m42394_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42394_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42394_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneAbstractBehaviour_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7537_IList_1_get_Item_m42394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42394_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_set_Item_m42395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t2_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42395_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42395_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7537_IList_1_set_Item_m42395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42395_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7537_MethodInfos[] =
{
	&IList_1_IndexOf_m42396_MethodInfo,
	&IList_1_Insert_m42397_MethodInfo,
	&IList_1_RemoveAt_m42398_MethodInfo,
	&IList_1_get_Item_m42394_MethodInfo,
	&IList_1_set_Item_m42395_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7537_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7536_il2cpp_TypeInfo,
	&IEnumerable_1_t7538_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7537_0_0_0;
extern Il2CppType IList_1_t7537_1_0_0;
struct IList_1_t7537;
extern Il2CppGenericClass IList_1_t7537_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7537_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7537_MethodInfos/* methods */
	, IList_1_t7537_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7537_il2cpp_TypeInfo/* element_class */
	, IList_1_t7537_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7537_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7537_0_0_0/* byval_arg */
	, &IList_1_t7537_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7537_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4505_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>
extern MethodInfo ICollection_1_get_Count_m42399_MethodInfo;
static PropertyInfo ICollection_1_t4505____Count_PropertyInfo = 
{
	&ICollection_1_t4505_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42399_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42400_MethodInfo;
static PropertyInfo ICollection_1_t4505____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4505_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4505_PropertyInfos[] =
{
	&ICollection_1_t4505____Count_PropertyInfo,
	&ICollection_1_t4505____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42399_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m42399_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42399_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42400_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42400_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42400_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
static ParameterInfo ICollection_1_t4505_ICollection_1_Add_m42401_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42401_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m42401_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t4505_ICollection_1_Add_m42401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42401_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42402_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m42402_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42402_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
static ParameterInfo ICollection_1_t4505_ICollection_1_Contains_m42403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t112_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42403_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m42403_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4505_ICollection_1_Contains_m42403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42403_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandlerU5BU5D_t4502_0_0_0;
extern Il2CppType IVideoBackgroundEventHandlerU5BU5D_t4502_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t4505_ICollection_1_CopyTo_m42404_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandlerU5BU5D_t4502_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42404_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42404_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t4505_ICollection_1_CopyTo_m42404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42404_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
static ParameterInfo ICollection_1_t4505_ICollection_1_Remove_m42405_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t112_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42405_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m42405_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4505_ICollection_1_Remove_m42405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42405_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4505_MethodInfos[] =
{
	&ICollection_1_get_Count_m42399_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42400_MethodInfo,
	&ICollection_1_Add_m42401_MethodInfo,
	&ICollection_1_Clear_m42402_MethodInfo,
	&ICollection_1_Contains_m42403_MethodInfo,
	&ICollection_1_CopyTo_m42404_MethodInfo,
	&ICollection_1_Remove_m42405_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t4503_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4505_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t4503_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4505_0_0_0;
extern Il2CppType ICollection_1_t4505_1_0_0;
struct ICollection_1_t4505;
extern Il2CppGenericClass ICollection_1_t4505_GenericClass;
TypeInfo ICollection_1_t4505_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4505_MethodInfos/* methods */
	, ICollection_1_t4505_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4505_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4505_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4505_0_0_0/* byval_arg */
	, &ICollection_1_t4505_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4505_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>
extern Il2CppType IEnumerator_1_t4504_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42406_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42406_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t4503_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4504_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42406_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t4503_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42406_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t4503_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t4503_0_0_0;
extern Il2CppType IEnumerable_1_t4503_1_0_0;
struct IEnumerable_1_t4503;
extern Il2CppGenericClass IEnumerable_1_t4503_GenericClass;
TypeInfo IEnumerable_1_t4503_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t4503_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t4503_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t4503_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t4503_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t4503_0_0_0/* byval_arg */
	, &IEnumerable_1_t4503_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t4503_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4504_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m42407_MethodInfo;
static PropertyInfo IEnumerator_1_t4504____Current_PropertyInfo = 
{
	&IEnumerator_1_t4504_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42407_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4504_PropertyInfos[] =
{
	&IEnumerator_1_t4504____Current_PropertyInfo,
	NULL
};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42407_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42407_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4504_il2cpp_TypeInfo/* declaring_type */
	, &IVideoBackgroundEventHandler_t112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42407_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4504_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42407_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4504_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4504_0_0_0;
extern Il2CppType IEnumerator_1_t4504_1_0_0;
struct IEnumerator_1_t4504;
extern Il2CppGenericClass IEnumerator_1_t4504_GenericClass;
TypeInfo IEnumerator_1_t4504_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4504_MethodInfos/* methods */
	, IEnumerator_1_t4504_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4504_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4504_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4504_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4504_0_0_0/* byval_arg */
	, &IEnumerator_1_t4504_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4504_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2754_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_2MethodDeclarations.h"

extern TypeInfo IVideoBackgroundEventHandler_t112_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14174_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIVideoBackgroundEventHandler_t112_m32266_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IVideoBackgroundEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IVideoBackgroundEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIVideoBackgroundEventHandler_t112_m32266(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2754____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2754, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2754____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2754, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2754_FieldInfos[] =
{
	&InternalEnumerator_1_t2754____array_0_FieldInfo,
	&InternalEnumerator_1_t2754____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2754____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2754____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14174_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2754_PropertyInfos[] =
{
	&InternalEnumerator_1_t2754____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2754____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2754_InternalEnumerator_1__ctor_m14170_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14170_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14170_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2754_InternalEnumerator_1__ctor_m14170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14170_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14172_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14172_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14172_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14173_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14173_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14173_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14174_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14174_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &IVideoBackgroundEventHandler_t112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14174_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2754_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14170_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_MethodInfo,
	&InternalEnumerator_1_Dispose_m14172_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14173_MethodInfo,
	&InternalEnumerator_1_get_Current_m14174_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14173_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14172_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2754_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14171_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14173_MethodInfo,
	&InternalEnumerator_1_Dispose_m14172_MethodInfo,
	&InternalEnumerator_1_get_Current_m14174_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2754_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t4504_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2754_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4504_il2cpp_TypeInfo, 7},
};
extern TypeInfo IVideoBackgroundEventHandler_t112_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2754_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14174_MethodInfo/* Method Usage */,
	&IVideoBackgroundEventHandler_t112_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIVideoBackgroundEventHandler_t112_m32266_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2754_0_0_0;
extern Il2CppType InternalEnumerator_1_t2754_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2754_GenericClass;
TypeInfo InternalEnumerator_1_t2754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2754_MethodInfos/* methods */
	, InternalEnumerator_1_t2754_PropertyInfos/* properties */
	, InternalEnumerator_1_t2754_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2754_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2754_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2754_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2754_1_0_0/* this_arg */
	, InternalEnumerator_1_t2754_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2754_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2754_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2754)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4509_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>
extern MethodInfo IList_1_get_Item_m42408_MethodInfo;
extern MethodInfo IList_1_set_Item_m42409_MethodInfo;
static PropertyInfo IList_1_t4509____Item_PropertyInfo = 
{
	&IList_1_t4509_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42408_MethodInfo/* get */
	, &IList_1_set_Item_m42409_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4509_PropertyInfos[] =
{
	&IList_1_t4509____Item_PropertyInfo,
	NULL
};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
static ParameterInfo IList_1_t4509_IList_1_IndexOf_m42410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t112_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42410_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42410_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4509_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4509_IList_1_IndexOf_m42410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42410_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
static ParameterInfo IList_1_t4509_IList_1_Insert_m42411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42411_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42411_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4509_IList_1_Insert_m42411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42411_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4509_IList_1_RemoveAt_m42412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42412_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42412_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t4509_IList_1_RemoveAt_m42412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4509_IList_1_get_Item_m42408_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42408_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42408_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4509_il2cpp_TypeInfo/* declaring_type */
	, &IVideoBackgroundEventHandler_t112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t4509_IList_1_get_Item_m42408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42408_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IVideoBackgroundEventHandler_t112_0_0_0;
static ParameterInfo IList_1_t4509_IList_1_set_Item_m42409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42409_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42409_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4509_IList_1_set_Item_m42409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42409_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4509_MethodInfos[] =
{
	&IList_1_IndexOf_m42410_MethodInfo,
	&IList_1_Insert_m42411_MethodInfo,
	&IList_1_RemoveAt_m42412_MethodInfo,
	&IList_1_get_Item_m42408_MethodInfo,
	&IList_1_set_Item_m42409_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4509_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t4505_il2cpp_TypeInfo,
	&IEnumerable_1_t4503_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4509_0_0_0;
extern Il2CppType IList_1_t4509_1_0_0;
struct IList_1_t4509;
extern Il2CppGenericClass IList_1_t4509_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t4509_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4509_MethodInfos/* methods */
	, IList_1_t4509_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4509_il2cpp_TypeInfo/* element_class */
	, IList_1_t4509_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4509_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4509_0_0_0/* byval_arg */
	, &IList_1_t4509_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4509_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7539_il2cpp_TypeInfo;

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>
extern MethodInfo ICollection_1_get_Count_m42413_MethodInfo;
static PropertyInfo ICollection_1_t7539____Count_PropertyInfo = 
{
	&ICollection_1_t7539_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42414_MethodInfo;
static PropertyInfo ICollection_1_t7539____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7539_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7539_PropertyInfos[] =
{
	&ICollection_1_t7539____Count_PropertyInfo,
	&ICollection_1_t7539____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42413_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42413_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42413_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42414_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42414_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42414_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t10_0_0_0;
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_Add_m42415_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42415_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42415_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7539_ICollection_1_Add_m42415_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42415_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42416_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42416_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42416_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_Contains_m42417_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42417_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42417_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7539_ICollection_1_Contains_m42417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42417_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviourU5BU5D_t5678_0_0_0;
extern Il2CppType MonoBehaviourU5BU5D_t5678_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_CopyTo_m42418_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviourU5BU5D_t5678_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42418_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42418_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7539_ICollection_1_CopyTo_m42418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42418_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_Remove_m42419_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42419_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42419_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7539_ICollection_1_Remove_m42419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42419_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7539_MethodInfos[] =
{
	&ICollection_1_get_Count_m42413_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42414_MethodInfo,
	&ICollection_1_Add_m42415_MethodInfo,
	&ICollection_1_Clear_m42416_MethodInfo,
	&ICollection_1_Contains_m42417_MethodInfo,
	&ICollection_1_CopyTo_m42418_MethodInfo,
	&ICollection_1_Remove_m42419_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7541_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7539_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7541_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7539_0_0_0;
extern Il2CppType ICollection_1_t7539_1_0_0;
struct ICollection_1_t7539;
extern Il2CppGenericClass ICollection_1_t7539_GenericClass;
TypeInfo ICollection_1_t7539_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7539_MethodInfos/* methods */
	, ICollection_1_t7539_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7539_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7539_0_0_0/* byval_arg */
	, &ICollection_1_t7539_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7539_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>
extern Il2CppType IEnumerator_1_t5935_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42420_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42420_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7541_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5935_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42420_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7541_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42420_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7541_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7541_0_0_0;
extern Il2CppType IEnumerable_1_t7541_1_0_0;
struct IEnumerable_1_t7541;
extern Il2CppGenericClass IEnumerable_1_t7541_GenericClass;
TypeInfo IEnumerable_1_t7541_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7541_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7541_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7541_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7541_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7541_0_0_0/* byval_arg */
	, &IEnumerable_1_t7541_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7541_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5935_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42421_MethodInfo;
static PropertyInfo IEnumerator_1_t5935____Current_PropertyInfo = 
{
	&IEnumerator_1_t5935_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42421_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5935_PropertyInfos[] =
{
	&IEnumerator_1_t5935____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoBehaviour_t10_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42421_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42421_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5935_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t10_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42421_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5935_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42421_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5935_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5935_0_0_0;
extern Il2CppType IEnumerator_1_t5935_1_0_0;
struct IEnumerator_1_t5935;
extern Il2CppGenericClass IEnumerator_1_t5935_GenericClass;
TypeInfo IEnumerator_1_t5935_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5935_MethodInfos/* methods */
	, IEnumerator_1_t5935_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5935_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5935_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5935_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5935_0_0_0/* byval_arg */
	, &IEnumerator_1_t5935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2755_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_3MethodDeclarations.h"

extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14179_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoBehaviour_t10_m32277_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.MonoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.MonoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMonoBehaviour_t10_m32277(__this, p0, method) (MonoBehaviour_t10 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2755____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2755, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2755____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2755, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2755_FieldInfos[] =
{
	&InternalEnumerator_1_t2755____array_0_FieldInfo,
	&InternalEnumerator_1_t2755____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2755____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2755____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14179_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2755_PropertyInfos[] =
{
	&InternalEnumerator_1_t2755____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2755____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2755_InternalEnumerator_1__ctor_m14175_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14175_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14175_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2755_InternalEnumerator_1__ctor_m14175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14175_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14177_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14177_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14177_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14178_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14178_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14178_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t10_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14179_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14179_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t10_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14179_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2755_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14175_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_MethodInfo,
	&InternalEnumerator_1_Dispose_m14177_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14178_MethodInfo,
	&InternalEnumerator_1_get_Current_m14179_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14178_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14177_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2755_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14176_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14178_MethodInfo,
	&InternalEnumerator_1_Dispose_m14177_MethodInfo,
	&InternalEnumerator_1_get_Current_m14179_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2755_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5935_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2755_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5935_il2cpp_TypeInfo, 7},
};
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2755_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14179_MethodInfo/* Method Usage */,
	&MonoBehaviour_t10_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMonoBehaviour_t10_m32277_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2755_0_0_0;
extern Il2CppType InternalEnumerator_1_t2755_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2755_GenericClass;
TypeInfo InternalEnumerator_1_t2755_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2755_MethodInfos/* methods */
	, InternalEnumerator_1_t2755_PropertyInfos/* properties */
	, InternalEnumerator_1_t2755_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2755_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2755_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2755_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2755_1_0_0/* this_arg */
	, InternalEnumerator_1_t2755_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2755_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2755_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2755)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7540_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>
extern MethodInfo IList_1_get_Item_m42422_MethodInfo;
extern MethodInfo IList_1_set_Item_m42423_MethodInfo;
static PropertyInfo IList_1_t7540____Item_PropertyInfo = 
{
	&IList_1_t7540_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42422_MethodInfo/* get */
	, &IList_1_set_Item_m42423_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7540_PropertyInfos[] =
{
	&IList_1_t7540____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_IndexOf_m42424_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42424_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42424_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7540_IList_1_IndexOf_m42424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42424_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_Insert_m42425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42425_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42425_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7540_IList_1_Insert_m42425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42425_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_RemoveAt_m42426_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42426_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42426_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7540_IList_1_RemoveAt_m42426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42426_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_get_Item_m42422_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MonoBehaviour_t10_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42422_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42422_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t10_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7540_IList_1_get_Item_m42422_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42422_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoBehaviour_t10_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_set_Item_m42423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t10_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42423_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42423_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7540_IList_1_set_Item_m42423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42423_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7540_MethodInfos[] =
{
	&IList_1_IndexOf_m42424_MethodInfo,
	&IList_1_Insert_m42425_MethodInfo,
	&IList_1_RemoveAt_m42426_MethodInfo,
	&IList_1_get_Item_m42422_MethodInfo,
	&IList_1_set_Item_m42423_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7540_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7539_il2cpp_TypeInfo,
	&IEnumerable_1_t7541_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7540_0_0_0;
extern Il2CppType IList_1_t7540_1_0_0;
struct IList_1_t7540;
extern Il2CppGenericClass IList_1_t7540_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7540_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7540_MethodInfos/* methods */
	, IList_1_t7540_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7540_il2cpp_TypeInfo/* element_class */
	, IList_1_t7540_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7540_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7540_0_0_0/* byval_arg */
	, &IList_1_t7540_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7540_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7542_il2cpp_TypeInfo;

// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>
extern MethodInfo ICollection_1_get_Count_m42427_MethodInfo;
static PropertyInfo ICollection_1_t7542____Count_PropertyInfo = 
{
	&ICollection_1_t7542_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42428_MethodInfo;
static PropertyInfo ICollection_1_t7542____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7542_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42428_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7542_PropertyInfos[] =
{
	&ICollection_1_t7542____Count_PropertyInfo,
	&ICollection_1_t7542____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42427_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42427_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42427_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42428_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42428_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42428_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t559_0_0_0;
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_Add_m42429_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42429_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Add(T)
MethodInfo ICollection_1_Add_m42429_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7542_ICollection_1_Add_m42429_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42429_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42430_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Clear()
MethodInfo ICollection_1_Clear_m42430_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42430_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_Contains_m42431_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42431_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42431_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7542_ICollection_1_Contains_m42431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42431_GenericMethod/* genericMethod */

};
extern Il2CppType BehaviourU5BU5D_t5679_0_0_0;
extern Il2CppType BehaviourU5BU5D_t5679_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_CopyTo_m42432_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BehaviourU5BU5D_t5679_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42432_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42432_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7542_ICollection_1_CopyTo_m42432_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42432_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_Remove_m42433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42433_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42433_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7542_ICollection_1_Remove_m42433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42433_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7542_MethodInfos[] =
{
	&ICollection_1_get_Count_m42427_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42428_MethodInfo,
	&ICollection_1_Add_m42429_MethodInfo,
	&ICollection_1_Clear_m42430_MethodInfo,
	&ICollection_1_Contains_m42431_MethodInfo,
	&ICollection_1_CopyTo_m42432_MethodInfo,
	&ICollection_1_Remove_m42433_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7544_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7542_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7544_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7542_0_0_0;
extern Il2CppType ICollection_1_t7542_1_0_0;
struct ICollection_1_t7542;
extern Il2CppGenericClass ICollection_1_t7542_GenericClass;
TypeInfo ICollection_1_t7542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7542_MethodInfos/* methods */
	, ICollection_1_t7542_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7542_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7542_0_0_0/* byval_arg */
	, &ICollection_1_t7542_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7542_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>
extern Il2CppType IEnumerator_1_t5937_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42434_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42434_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7544_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5937_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42434_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7544_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42434_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7544_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7544_0_0_0;
extern Il2CppType IEnumerable_1_t7544_1_0_0;
struct IEnumerable_1_t7544;
extern Il2CppGenericClass IEnumerable_1_t7544_GenericClass;
TypeInfo IEnumerable_1_t7544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7544_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7544_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7544_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7544_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7544_0_0_0/* byval_arg */
	, &IEnumerable_1_t7544_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7544_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5937_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>
extern MethodInfo IEnumerator_1_get_Current_m42435_MethodInfo;
static PropertyInfo IEnumerator_1_t5937____Current_PropertyInfo = 
{
	&IEnumerator_1_t5937_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42435_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5937_PropertyInfos[] =
{
	&IEnumerator_1_t5937____Current_PropertyInfo,
	NULL
};
extern Il2CppType Behaviour_t559_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42435_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42435_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5937_il2cpp_TypeInfo/* declaring_type */
	, &Behaviour_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42435_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5937_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42435_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5937_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5937_0_0_0;
extern Il2CppType IEnumerator_1_t5937_1_0_0;
struct IEnumerator_1_t5937;
extern Il2CppGenericClass IEnumerator_1_t5937_GenericClass;
TypeInfo IEnumerator_1_t5937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5937_MethodInfos/* methods */
	, IEnumerator_1_t5937_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5937_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5937_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5937_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5937_0_0_0/* byval_arg */
	, &IEnumerator_1_t5937_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2756_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4MethodDeclarations.h"

extern TypeInfo Behaviour_t559_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14184_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBehaviour_t559_m32288_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Behaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Behaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisBehaviour_t559_m32288(__this, p0, method) (Behaviour_t559 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2756____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2756, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2756____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2756, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2756_FieldInfos[] =
{
	&InternalEnumerator_1_t2756____array_0_FieldInfo,
	&InternalEnumerator_1_t2756____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2756____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2756_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2756____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2756_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14184_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2756_PropertyInfos[] =
{
	&InternalEnumerator_1_t2756____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2756____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2756_InternalEnumerator_1__ctor_m14180_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14180_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14180_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2756_InternalEnumerator_1__ctor_m14180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14180_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14182_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14182_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14182_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14183_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14183_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14183_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t559_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14184_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14184_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Behaviour_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14184_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2756_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14180_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_MethodInfo,
	&InternalEnumerator_1_Dispose_m14182_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14183_MethodInfo,
	&InternalEnumerator_1_get_Current_m14184_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14183_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14182_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2756_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14181_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14183_MethodInfo,
	&InternalEnumerator_1_Dispose_m14182_MethodInfo,
	&InternalEnumerator_1_get_Current_m14184_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2756_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5937_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2756_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5937_il2cpp_TypeInfo, 7},
};
extern TypeInfo Behaviour_t559_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2756_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14184_MethodInfo/* Method Usage */,
	&Behaviour_t559_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBehaviour_t559_m32288_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2756_0_0_0;
extern Il2CppType InternalEnumerator_1_t2756_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2756_GenericClass;
TypeInfo InternalEnumerator_1_t2756_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2756_MethodInfos/* methods */
	, InternalEnumerator_1_t2756_PropertyInfos/* properties */
	, InternalEnumerator_1_t2756_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2756_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2756_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2756_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2756_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2756_1_0_0/* this_arg */
	, InternalEnumerator_1_t2756_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2756_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2756_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2756)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7543_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Behaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Behaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Behaviour>
extern MethodInfo IList_1_get_Item_m42436_MethodInfo;
extern MethodInfo IList_1_set_Item_m42437_MethodInfo;
static PropertyInfo IList_1_t7543____Item_PropertyInfo = 
{
	&IList_1_t7543_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42436_MethodInfo/* get */
	, &IList_1_set_Item_m42437_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7543_PropertyInfos[] =
{
	&IList_1_t7543____Item_PropertyInfo,
	NULL
};
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_IndexOf_m42438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42438_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Behaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42438_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7543_IList_1_IndexOf_m42438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42438_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_Insert_m42439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42439_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42439_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7543_IList_1_Insert_m42439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42439_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_RemoveAt_m42440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42440_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42440_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7543_IList_1_RemoveAt_m42440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42440_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_get_Item_m42436_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Behaviour_t559_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42436_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Behaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42436_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Behaviour_t559_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7543_IList_1_get_Item_m42436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42436_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Behaviour_t559_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_set_Item_m42437_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Behaviour_t559_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42437_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42437_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7543_IList_1_set_Item_m42437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42437_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7543_MethodInfos[] =
{
	&IList_1_IndexOf_m42438_MethodInfo,
	&IList_1_Insert_m42439_MethodInfo,
	&IList_1_RemoveAt_m42440_MethodInfo,
	&IList_1_get_Item_m42436_MethodInfo,
	&IList_1_set_Item_m42437_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7543_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7542_il2cpp_TypeInfo,
	&IEnumerable_1_t7544_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7543_0_0_0;
extern Il2CppType IList_1_t7543_1_0_0;
struct IList_1_t7543;
extern Il2CppGenericClass IList_1_t7543_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7543_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7543_MethodInfos/* methods */
	, IList_1_t7543_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7543_il2cpp_TypeInfo/* element_class */
	, IList_1_t7543_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7543_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7543_0_0_0/* byval_arg */
	, &IList_1_t7543_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7543_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3220_il2cpp_TypeInfo;

// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Component>
extern MethodInfo ICollection_1_get_Count_m42441_MethodInfo;
static PropertyInfo ICollection_1_t3220____Count_PropertyInfo = 
{
	&ICollection_1_t3220_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42441_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42442_MethodInfo;
static PropertyInfo ICollection_1_t3220____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3220_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42442_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3220_PropertyInfos[] =
{
	&ICollection_1_t3220____Count_PropertyInfo,
	&ICollection_1_t3220____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42441_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_Count()
MethodInfo ICollection_1_get_Count_m42441_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42441_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42442_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42442_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42442_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t128_0_0_0;
extern Il2CppType Component_t128_0_0_0;
static ParameterInfo ICollection_1_t3220_ICollection_1_Add_m42443_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t128_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42443_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Add(T)
MethodInfo ICollection_1_Add_m42443_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t3220_ICollection_1_Add_m42443_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42443_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42444_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Clear()
MethodInfo ICollection_1_Clear_m42444_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42444_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t128_0_0_0;
static ParameterInfo ICollection_1_t3220_ICollection_1_Contains_m42445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t128_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42445_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Contains(T)
MethodInfo ICollection_1_Contains_m42445_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t3220_ICollection_1_Contains_m42445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42445_GenericMethod/* genericMethod */

};
extern Il2CppType ComponentU5BU5D_t3217_0_0_0;
extern Il2CppType ComponentU5BU5D_t3217_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t3220_ICollection_1_CopyTo_m42446_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ComponentU5BU5D_t3217_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42446_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42446_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t3220_ICollection_1_CopyTo_m42446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42446_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t128_0_0_0;
static ParameterInfo ICollection_1_t3220_ICollection_1_Remove_m42447_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t128_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42447_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Remove(T)
MethodInfo ICollection_1_Remove_m42447_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t3220_ICollection_1_Remove_m42447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42447_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3220_MethodInfos[] =
{
	&ICollection_1_get_Count_m42441_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42442_MethodInfo,
	&ICollection_1_Add_m42443_MethodInfo,
	&ICollection_1_Clear_m42444_MethodInfo,
	&ICollection_1_Contains_m42445_MethodInfo,
	&ICollection_1_CopyTo_m42446_MethodInfo,
	&ICollection_1_Remove_m42447_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3218_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3220_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t3218_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3220_0_0_0;
extern Il2CppType ICollection_1_t3220_1_0_0;
struct ICollection_1_t3220;
extern Il2CppGenericClass ICollection_1_t3220_GenericClass;
TypeInfo ICollection_1_t3220_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3220_MethodInfos/* methods */
	, ICollection_1_t3220_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3220_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3220_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3220_0_0_0/* byval_arg */
	, &ICollection_1_t3220_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3220_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Component>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Component>
extern Il2CppType IEnumerator_1_t3219_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42448_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Component>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42448_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3218_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3219_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42448_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3218_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42448_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3218_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3218_0_0_0;
extern Il2CppType IEnumerable_1_t3218_1_0_0;
struct IEnumerable_1_t3218;
extern Il2CppGenericClass IEnumerable_1_t3218_GenericClass;
TypeInfo IEnumerable_1_t3218_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3218_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3218_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3218_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3218_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3218_0_0_0/* byval_arg */
	, &IEnumerable_1_t3218_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3218_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3219_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Component>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Component>
extern MethodInfo IEnumerator_1_get_Current_m42449_MethodInfo;
static PropertyInfo IEnumerator_1_t3219____Current_PropertyInfo = 
{
	&IEnumerator_1_t3219_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42449_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3219_PropertyInfos[] =
{
	&IEnumerator_1_t3219____Current_PropertyInfo,
	NULL
};
extern Il2CppType Component_t128_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42449_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Component>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42449_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3219_il2cpp_TypeInfo/* declaring_type */
	, &Component_t128_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42449_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3219_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42449_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3219_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3219_0_0_0;
extern Il2CppType IEnumerator_1_t3219_1_0_0;
struct IEnumerator_1_t3219;
extern Il2CppGenericClass IEnumerator_1_t3219_GenericClass;
TypeInfo IEnumerator_1_t3219_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3219_MethodInfos/* methods */
	, IEnumerator_1_t3219_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3219_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3219_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3219_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3219_0_0_0/* byval_arg */
	, &IEnumerator_1_t3219_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3219_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Component>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2757_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Component>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_5MethodDeclarations.h"

extern TypeInfo Component_t128_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14189_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisComponent_t128_m32299_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Component>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Component>(System.Int32)
#define Array_InternalArray__get_Item_TisComponent_t128_m32299(__this, p0, method) (Component_t128 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Component>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Component>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Component>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2757____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2757, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2757____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2757, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2757_FieldInfos[] =
{
	&InternalEnumerator_1_t2757____array_0_FieldInfo,
	&InternalEnumerator_1_t2757____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2757____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2757_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2757____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2757_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2757_PropertyInfos[] =
{
	&InternalEnumerator_1_t2757____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2757____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2757_InternalEnumerator_1__ctor_m14185_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14185_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14185_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2757_InternalEnumerator_1__ctor_m14185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14185_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14187_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14187_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14187_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14188_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Component>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14188_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14188_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t128_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14189_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Component>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14189_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Component_t128_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14189_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2757_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14185_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_MethodInfo,
	&InternalEnumerator_1_Dispose_m14187_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14188_MethodInfo,
	&InternalEnumerator_1_get_Current_m14189_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14188_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14187_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2757_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14186_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14188_MethodInfo,
	&InternalEnumerator_1_Dispose_m14187_MethodInfo,
	&InternalEnumerator_1_get_Current_m14189_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2757_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t3219_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2757_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3219_il2cpp_TypeInfo, 7},
};
extern TypeInfo Component_t128_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2757_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14189_MethodInfo/* Method Usage */,
	&Component_t128_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisComponent_t128_m32299_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2757_0_0_0;
extern Il2CppType InternalEnumerator_1_t2757_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2757_GenericClass;
TypeInfo InternalEnumerator_1_t2757_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2757_MethodInfos/* methods */
	, InternalEnumerator_1_t2757_PropertyInfos/* properties */
	, InternalEnumerator_1_t2757_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2757_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2757_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2757_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2757_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2757_1_0_0/* this_arg */
	, InternalEnumerator_1_t2757_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2757_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2757_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2757)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3224_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Component>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Component>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Component>
extern MethodInfo IList_1_get_Item_m42450_MethodInfo;
extern MethodInfo IList_1_set_Item_m42451_MethodInfo;
static PropertyInfo IList_1_t3224____Item_PropertyInfo = 
{
	&IList_1_t3224_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42450_MethodInfo/* get */
	, &IList_1_set_Item_m42451_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3224_PropertyInfos[] =
{
	&IList_1_t3224____Item_PropertyInfo,
	NULL
};
extern Il2CppType Component_t128_0_0_0;
static ParameterInfo IList_1_t3224_IList_1_IndexOf_m42452_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t128_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42452_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Component>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42452_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3224_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t3224_IList_1_IndexOf_m42452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42452_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Component_t128_0_0_0;
static ParameterInfo IList_1_t3224_IList_1_Insert_m42453_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Component_t128_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42453_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42453_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t3224_IList_1_Insert_m42453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42453_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t3224_IList_1_RemoveAt_m42454_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42454_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42454_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t3224_IList_1_RemoveAt_m42454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42454_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t3224_IList_1_get_Item_m42450_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Component_t128_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42450_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Component>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42450_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3224_il2cpp_TypeInfo/* declaring_type */
	, &Component_t128_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t3224_IList_1_get_Item_m42450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42450_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Component_t128_0_0_0;
static ParameterInfo IList_1_t3224_IList_1_set_Item_m42451_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Component_t128_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42451_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42451_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t3224_IList_1_set_Item_m42451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42451_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3224_MethodInfos[] =
{
	&IList_1_IndexOf_m42452_MethodInfo,
	&IList_1_Insert_m42453_MethodInfo,
	&IList_1_RemoveAt_m42454_MethodInfo,
	&IList_1_get_Item_m42450_MethodInfo,
	&IList_1_set_Item_m42451_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3224_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t3220_il2cpp_TypeInfo,
	&IEnumerable_1_t3218_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3224_0_0_0;
extern Il2CppType IList_1_t3224_1_0_0;
struct IList_1_t3224;
extern Il2CppGenericClass IList_1_t3224_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t3224_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3224_MethodInfos/* methods */
	, IList_1_t3224_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3224_il2cpp_TypeInfo/* element_class */
	, IList_1_t3224_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3224_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3224_0_0_0/* byval_arg */
	, &IList_1_t3224_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3224_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7545_il2cpp_TypeInfo;

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Object>
extern MethodInfo ICollection_1_get_Count_m42455_MethodInfo;
static PropertyInfo ICollection_1_t7545____Count_PropertyInfo = 
{
	&ICollection_1_t7545_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42455_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42456_MethodInfo;
static PropertyInfo ICollection_1_t7545____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7545_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42456_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7545_PropertyInfos[] =
{
	&ICollection_1_t7545____Count_PropertyInfo,
	&ICollection_1_t7545____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42455_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_Count()
MethodInfo ICollection_1_get_Count_m42455_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42455_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42456_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42456_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42456_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_Add_m42457_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42457_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Add(T)
MethodInfo ICollection_1_Add_m42457_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7545_ICollection_1_Add_m42457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42457_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42458_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Clear()
MethodInfo ICollection_1_Clear_m42458_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42458_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t120_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_Contains_m42459_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42459_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Contains(T)
MethodInfo ICollection_1_Contains_m42459_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7545_ICollection_1_Contains_m42459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42459_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t227_0_0_0;
extern Il2CppType ObjectU5BU5D_t227_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_CopyTo_m42460_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t227_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42460_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42460_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7545_ICollection_1_CopyTo_m42460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42460_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t120_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_Remove_m42461_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42461_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Remove(T)
MethodInfo ICollection_1_Remove_m42461_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7545_ICollection_1_Remove_m42461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42461_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7545_MethodInfos[] =
{
	&ICollection_1_get_Count_m42455_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42456_MethodInfo,
	&ICollection_1_Add_m42457_MethodInfo,
	&ICollection_1_Clear_m42458_MethodInfo,
	&ICollection_1_Contains_m42459_MethodInfo,
	&ICollection_1_CopyTo_m42460_MethodInfo,
	&ICollection_1_Remove_m42461_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7547_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7545_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7547_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7545_0_0_0;
extern Il2CppType ICollection_1_t7545_1_0_0;
struct ICollection_1_t7545;
extern Il2CppGenericClass ICollection_1_t7545_GenericClass;
TypeInfo ICollection_1_t7545_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7545_MethodInfos/* methods */
	, ICollection_1_t7545_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7545_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7545_0_0_0/* byval_arg */
	, &ICollection_1_t7545_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7545_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Object>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Object>
extern Il2CppType IEnumerator_1_t5940_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42462_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Object>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42462_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7547_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5940_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42462_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7547_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42462_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7547_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7547_0_0_0;
extern Il2CppType IEnumerable_1_t7547_1_0_0;
struct IEnumerable_1_t7547;
extern Il2CppGenericClass IEnumerable_1_t7547_GenericClass;
TypeInfo IEnumerable_1_t7547_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7547_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7547_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7547_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7547_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7547_0_0_0/* byval_arg */
	, &IEnumerable_1_t7547_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7547_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5940_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Object>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Object>
extern MethodInfo IEnumerator_1_get_Current_m42463_MethodInfo;
static PropertyInfo IEnumerator_1_t5940____Current_PropertyInfo = 
{
	&IEnumerator_1_t5940_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42463_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5940_PropertyInfos[] =
{
	&IEnumerator_1_t5940____Current_PropertyInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42463_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Object>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42463_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5940_il2cpp_TypeInfo/* declaring_type */
	, &Object_t120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42463_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5940_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42463_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5940_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5940_0_0_0;
extern Il2CppType IEnumerator_1_t5940_1_0_0;
struct IEnumerator_1_t5940;
extern Il2CppGenericClass IEnumerator_1_t5940_GenericClass;
TypeInfo IEnumerator_1_t5940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5940_MethodInfos/* methods */
	, IEnumerator_1_t5940_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5940_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5940_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5940_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5940_0_0_0/* byval_arg */
	, &IEnumerator_1_t5940_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2758_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_6MethodDeclarations.h"

extern TypeInfo Object_t120_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14194_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObject_t120_m32310_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Object>(System.Int32)
#define Array_InternalArray__get_Item_TisObject_t120_m32310(__this, p0, method) (Object_t120 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Object>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Object>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Object>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Object>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2758____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2758, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2758____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2758, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2758_FieldInfos[] =
{
	&InternalEnumerator_1_t2758____array_0_FieldInfo,
	&InternalEnumerator_1_t2758____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2758____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2758_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2758____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2758_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14194_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2758_PropertyInfos[] =
{
	&InternalEnumerator_1_t2758____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2758____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2758_InternalEnumerator_1__ctor_m14190_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14190_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14190_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2758_InternalEnumerator_1__ctor_m14190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14190_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Object>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14192_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14192_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14192_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14193_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Object>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14193_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14193_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14194_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Object>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14194_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Object_t120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14194_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2758_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14190_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_MethodInfo,
	&InternalEnumerator_1_Dispose_m14192_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14193_MethodInfo,
	&InternalEnumerator_1_get_Current_m14194_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14193_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14192_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2758_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14191_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14193_MethodInfo,
	&InternalEnumerator_1_Dispose_m14192_MethodInfo,
	&InternalEnumerator_1_get_Current_m14194_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2758_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5940_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2758_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5940_il2cpp_TypeInfo, 7},
};
extern TypeInfo Object_t120_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2758_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14194_MethodInfo/* Method Usage */,
	&Object_t120_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObject_t120_m32310_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2758_0_0_0;
extern Il2CppType InternalEnumerator_1_t2758_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2758_GenericClass;
TypeInfo InternalEnumerator_1_t2758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2758_MethodInfos/* methods */
	, InternalEnumerator_1_t2758_PropertyInfos/* properties */
	, InternalEnumerator_1_t2758_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2758_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2758_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2758_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2758_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2758_1_0_0/* this_arg */
	, InternalEnumerator_1_t2758_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2758_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2758_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2758)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7546_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Object>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Object>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Object>
extern MethodInfo IList_1_get_Item_m42464_MethodInfo;
extern MethodInfo IList_1_set_Item_m42465_MethodInfo;
static PropertyInfo IList_1_t7546____Item_PropertyInfo = 
{
	&IList_1_t7546_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42464_MethodInfo/* get */
	, &IList_1_set_Item_m42465_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7546_PropertyInfos[] =
{
	&IList_1_t7546____Item_PropertyInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_IndexOf_m42466_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42466_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Object>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42466_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7546_IList_1_IndexOf_m42466_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42466_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t120_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_Insert_m42467_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42467_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42467_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7546_IList_1_Insert_m42467_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42467_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_RemoveAt_m42468_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42468_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42468_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7546_IList_1_RemoveAt_m42468_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42468_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_get_Item_m42464_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Object_t120_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42464_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Object>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42464_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Object_t120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7546_IList_1_get_Item_m42464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42464_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t120_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_set_Item_m42465_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42465_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42465_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7546_IList_1_set_Item_m42465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42465_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7546_MethodInfos[] =
{
	&IList_1_IndexOf_m42466_MethodInfo,
	&IList_1_Insert_m42467_MethodInfo,
	&IList_1_RemoveAt_m42468_MethodInfo,
	&IList_1_get_Item_m42464_MethodInfo,
	&IList_1_set_Item_m42465_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7546_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7545_il2cpp_TypeInfo,
	&IEnumerable_1_t7547_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7546_0_0_0;
extern Il2CppType IList_1_t7546_1_0_0;
struct IList_1_t7546;
extern Il2CppGenericClass IList_1_t7546_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7546_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7546_MethodInfos/* methods */
	, IList_1_t7546_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7546_il2cpp_TypeInfo/* element_class */
	, IList_1_t7546_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7546_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7546_0_0_0/* byval_arg */
	, &IList_1_t7546_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7546_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2759_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3MethodDeclarations.h"

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2760_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_genMethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14207_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14209_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2759____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2759_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2759, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2759_FieldInfos[] =
{
	&CachedInvokableCall_1_t2759____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2759_CachedInvokableCall_1__ctor_m14195_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14195_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14195_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2759_CachedInvokableCall_1__ctor_m14195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14195_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2759_CachedInvokableCall_1_Invoke_m14197_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14197_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14197_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2759_CachedInvokableCall_1_Invoke_m14197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14197_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2759_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14195_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14197_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m14197_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14210_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2759_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14197_MethodInfo,
	&InvokableCall_1_Find_m14210_MethodInfo,
};
extern Il2CppType UnityAction_1_t2764_0_0_0;
extern TypeInfo UnityAction_1_t2764_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t1_m32321_MethodInfo;
extern TypeInfo BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14212_MethodInfo;
extern TypeInfo BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2759_RGCTXData[8] = 
{
	&UnityAction_1_t2764_0_0_0/* Type Usage */,
	&UnityAction_1_t2764_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t1_m32321_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14212_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14207_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14209_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2759_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2759_1_0_0;
struct CachedInvokableCall_1_t2759;
extern Il2CppGenericClass CachedInvokableCall_1_t2759_GenericClass;
TypeInfo CachedInvokableCall_1_t2759_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2759_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2759_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2759_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2759_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2759_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2759_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2759_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2759_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2759_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2759)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2761_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_0.h"
extern TypeInfo InvokableCall_1_t2762_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_0MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14199_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14201_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern MethodInfo CachedInvokableCall_1__ctor_m14196_MethodInfo;
 void CachedInvokableCall_1__ctor_m14196_gshared (CachedInvokableCall_1_t2761 * __this, Object_t120 * ___target, MethodInfo_t141 * ___theFunction, Object_t * ___argument, MethodInfo* method)
{
	{
		__this->___m_Arg1_1 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 1));
		(( void (*) (InvokableCall_1_t2762 * __this, Object_t * p0, MethodInfo_t141 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(__this, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectU5BU5D_t130* L_0 = (__this->___m_Arg1_1);
		Object_t * L_1 = ___argument;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, ((Object_t *)L_1));
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)((Object_t *)L_1);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern MethodInfo CachedInvokableCall_1_Invoke_m14198_MethodInfo;
 void CachedInvokableCall_1_Invoke_m14198_gshared (CachedInvokableCall_1_t2761 * __this, ObjectU5BU5D_t130* ___args, MethodInfo* method)
{
	{
		ObjectU5BU5D_t130* L_0 = (__this->___m_Arg1_1);
		(( void (*) (InvokableCall_1_t2762 * __this, ObjectU5BU5D_t130* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(__this, L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.Object>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2761____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2761, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2761_FieldInfos[] =
{
	&CachedInvokableCall_1_t2761____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2761_CachedInvokableCall_1__ctor_m14196_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14196_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14196_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2761_CachedInvokableCall_1__ctor_m14196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14196_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2761_CachedInvokableCall_1_Invoke_m14198_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14198_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14198_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2761_CachedInvokableCall_1_Invoke_m14198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14198_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2761_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14196_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14198_MethodInfo,
	NULL
};
extern MethodInfo InvokableCall_1_Find_m14202_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2761_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14198_MethodInfo,
	&InvokableCall_1_Find_m14202_MethodInfo,
};
extern Il2CppType UnityAction_1_t2763_0_0_0;
extern TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_MethodInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14204_MethodInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2761_RGCTXData[8] = 
{
	&UnityAction_1_t2763_0_0_0/* Type Usage */,
	&UnityAction_1_t2763_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14204_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14199_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14201_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2761_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2761_1_0_0;
struct CachedInvokableCall_1_t2761;
extern Il2CppGenericClass CachedInvokableCall_1_t2761_GenericClass;
TypeInfo CachedInvokableCall_1_t2761_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2761_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2761_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2761_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2761_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2761_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2761_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2761_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2761)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
 void InvokableCall_1__ctor_m14199_gshared (InvokableCall_1_t2762 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method)
{
	{
		BaseInvokableCall__ctor_m6534(__this, ___target, ___theFunction, /*hidden argument*/&BaseInvokableCall__ctor_m6534_MethodInfo);
		UnityAction_1_t2763 * L_0 = (__this->___Delegate_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Delegate_t152 * L_2 = Delegate_CreateDelegate_m330(NULL /*static, unused*/, L_1, ___target, ___theFunction, /*hidden argument*/&Delegate_CreateDelegate_m330_MethodInfo);
		Delegate_t152 * L_3 = Delegate_Combine_m2327(NULL /*static, unused*/, L_0, ((UnityAction_1_t2763 *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), /*hidden argument*/&Delegate_Combine_m2327_MethodInfo);
		__this->___Delegate_0 = ((UnityAction_1_t2763 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern MethodInfo InvokableCall_1__ctor_m14200_MethodInfo;
 void InvokableCall_1__ctor_m14200_gshared (InvokableCall_1_t2762 * __this, UnityAction_1_t2763 * ___callback, MethodInfo* method)
{
	{
		BaseInvokableCall__ctor_m6533(__this, /*hidden argument*/&BaseInvokableCall__ctor_m6533_MethodInfo);
		UnityAction_1_t2763 * L_0 = (__this->___Delegate_0);
		Delegate_t152 * L_1 = Delegate_Combine_m2327(NULL /*static, unused*/, L_0, ___callback, /*hidden argument*/&Delegate_Combine_m2327_MethodInfo);
		__this->___Delegate_0 = ((UnityAction_1_t2763 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
 void InvokableCall_1_Invoke_m14201_gshared (InvokableCall_1_t2762 * __this, ObjectU5BU5D_t130* ___args, MethodInfo* method)
{
	{
		NullCheck(___args);
		if ((((int32_t)(((int32_t)(((Array_t *)___args)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t551 * L_0 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_0, (String_t*) &_stringLiteral471, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		NullCheck(___args);
		IL2CPP_ARRAY_BOUNDS_CHECK(___args, 0);
		int32_t L_1 = 0;
		(( void (*) (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (*(Object_t **)(Object_t **)SZArrayLdElema(___args, L_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		UnityAction_1_t2763 * L_2 = (__this->___Delegate_0);
		bool L_3 = BaseInvokableCall_AllowInvoke_m6535(NULL /*static, unused*/, L_2, /*hidden argument*/&BaseInvokableCall_AllowInvoke_m6535_MethodInfo);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t2763 * L_4 = (__this->___Delegate_0);
		NullCheck(___args);
		IL2CPP_ARRAY_BOUNDS_CHECK(___args, 0);
		int32_t L_5 = 0;
		NullCheck(L_4);
		VirtActionInvoker1< Object_t * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_4, ((Object_t *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(___args, L_5)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
 bool InvokableCall_1_Find_m14202_gshared (InvokableCall_1_t2762 * __this, Object_t * ___targetObj, MethodInfo_t141 * ___method, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t2763 * L_0 = (__this->___Delegate_0);
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m6713(L_0, /*hidden argument*/&Delegate_get_Target_m6713_MethodInfo);
		if ((((Object_t *)L_1) != ((Object_t *)___targetObj)))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t2763 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		MethodInfo_t141 * L_3 = Delegate_get_Method_m6711(L_2, /*hidden argument*/&Delegate_get_Method_m6711_MethodInfo);
		G_B3_0 = ((((MethodInfo_t141 *)L_3) == ((MethodInfo_t141 *)___method))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.Object>
extern Il2CppType UnityAction_1_t2763_0_0_1;
FieldInfo InvokableCall_1_t2762____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2763_0_0_1/* type */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2762, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2762_FieldInfos[] =
{
	&InvokableCall_1_t2762____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1__ctor_m14199_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14199_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1__ctor_m14199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14199_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2763_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1__ctor_m14200_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2763_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14200_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14200_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1__ctor_m14200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14200_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1_Invoke_m14201_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14201_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14201_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1_Invoke_m14201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14201_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1_Find_m14202_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14202_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14202_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1_Find_m14202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14202_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2762_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14199_MethodInfo,
	&InvokableCall_1__ctor_m14200_MethodInfo,
	&InvokableCall_1_Invoke_m14201_MethodInfo,
	&InvokableCall_1_Find_m14202_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2762_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14201_MethodInfo,
	&InvokableCall_1_Find_m14202_MethodInfo,
};
extern TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2762_RGCTXData[5] = 
{
	&UnityAction_1_t2763_0_0_0/* Type Usage */,
	&UnityAction_1_t2763_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14204_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2762_0_0_0;
extern Il2CppType InvokableCall_1_t2762_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2762;
extern Il2CppGenericClass InvokableCall_1_t2762_GenericClass;
TypeInfo InvokableCall_1_t2762_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2762_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2762_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2762_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2762_0_0_0/* byval_arg */
	, &InvokableCall_1_t2762_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2762_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2762_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2762)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo UnityAction_1__ctor_m14203_MethodInfo;
 void UnityAction_1__ctor_m14203_gshared (UnityAction_1_t2763 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
 void UnityAction_1_Invoke_m14204_gshared (UnityAction_1_t2763 * __this, Object_t * ___arg0, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAction_1_Invoke_m14204((UnityAction_1_t2763 *)__this->___prev_9,___arg0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg0, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___arg0, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern MethodInfo UnityAction_1_BeginInvoke_m14205_MethodInfo;
 Object_t * UnityAction_1_BeginInvoke_m14205_gshared (UnityAction_1_t2763 * __this, Object_t * ___arg0, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg0;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo UnityAction_1_EndInvoke_m14206_MethodInfo;
 void UnityAction_1_EndInvoke_m14206_gshared (UnityAction_1_t2763 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1__ctor_m14203_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14203_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1__ctor_m14203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14203_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1_Invoke_m14204_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14204_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14204_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1_Invoke_m14204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14204_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1_BeginInvoke_m14205_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14205_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14205_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1_BeginInvoke_m14205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14205_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1_EndInvoke_m14206_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14206_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14206_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1_EndInvoke_m14206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14206_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2763_MethodInfos[] =
{
	&UnityAction_1__ctor_m14203_MethodInfo,
	&UnityAction_1_Invoke_m14204_MethodInfo,
	&UnityAction_1_BeginInvoke_m14205_MethodInfo,
	&UnityAction_1_EndInvoke_m14206_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
static MethodInfo* UnityAction_1_t2763_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14204_MethodInfo,
	&UnityAction_1_BeginInvoke_m14205_MethodInfo,
	&UnityAction_1_EndInvoke_m14206_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2763_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2763_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t2763;
extern Il2CppGenericClass UnityAction_1_t2763_GenericClass;
TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2763_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2763_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2763_0_0_0/* byval_arg */
	, &UnityAction_1_t2763_1_0_0/* this_arg */
	, UnityAction_1_t2763_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2763)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_7.h"
extern TypeInfo UnityAction_1_t2764_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_7MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.BackgroundPlaneBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.BackgroundPlaneBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t1_m32321(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType UnityAction_1_t2764_0_0_1;
FieldInfo InvokableCall_1_t2760____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2764_0_0_1/* type */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2760, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2760_FieldInfos[] =
{
	&InvokableCall_1_t2760____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2760_InvokableCall_1__ctor_m14207_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14207_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14207_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2760_InvokableCall_1__ctor_m14207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14207_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2764_0_0_0;
static ParameterInfo InvokableCall_1_t2760_InvokableCall_1__ctor_m14208_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2764_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14208_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14208_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2760_InvokableCall_1__ctor_m14208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14208_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2760_InvokableCall_1_Invoke_m14209_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14209_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14209_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2760_InvokableCall_1_Invoke_m14209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14209_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2760_InvokableCall_1_Find_m14210_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14210_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14210_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2760_InvokableCall_1_Find_m14210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14210_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2760_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14207_MethodInfo,
	&InvokableCall_1__ctor_m14208_MethodInfo,
	&InvokableCall_1_Invoke_m14209_MethodInfo,
	&InvokableCall_1_Find_m14210_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2760_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14209_MethodInfo,
	&InvokableCall_1_Find_m14210_MethodInfo,
};
extern TypeInfo UnityAction_1_t2764_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2760_RGCTXData[5] = 
{
	&UnityAction_1_t2764_0_0_0/* Type Usage */,
	&UnityAction_1_t2764_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t1_m32321_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14212_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2760_0_0_0;
extern Il2CppType InvokableCall_1_t2760_1_0_0;
struct InvokableCall_1_t2760;
extern Il2CppGenericClass InvokableCall_1_t2760_GenericClass;
TypeInfo InvokableCall_1_t2760_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2760_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2760_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2760_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2760_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2760_0_0_0/* byval_arg */
	, &InvokableCall_1_t2760_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2760_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2760_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2760)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2764_UnityAction_1__ctor_m14211_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14211_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2764_UnityAction_1__ctor_m14211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14211_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
static ParameterInfo UnityAction_1_t2764_UnityAction_1_Invoke_m14212_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14212_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14212_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2764_UnityAction_1_Invoke_m14212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14212_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t1_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2764_UnityAction_1_BeginInvoke_m14213_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t1_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14213_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14213_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2764_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2764_UnityAction_1_BeginInvoke_m14213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14213_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2764_UnityAction_1_EndInvoke_m14214_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14214_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14214_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2764_UnityAction_1_EndInvoke_m14214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14214_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2764_MethodInfos[] =
{
	&UnityAction_1__ctor_m14211_MethodInfo,
	&UnityAction_1_Invoke_m14212_MethodInfo,
	&UnityAction_1_BeginInvoke_m14213_MethodInfo,
	&UnityAction_1_EndInvoke_m14214_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14213_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14214_MethodInfo;
static MethodInfo* UnityAction_1_t2764_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14212_MethodInfo,
	&UnityAction_1_BeginInvoke_m14213_MethodInfo,
	&UnityAction_1_EndInvoke_m14214_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2764_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2764_1_0_0;
struct UnityAction_1_t2764;
extern Il2CppGenericClass UnityAction_1_t2764_GenericClass;
TypeInfo UnityAction_1_t2764_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2764_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2764_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2764_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2764_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2764_0_0_0/* byval_arg */
	, &UnityAction_1_t2764_1_0_0/* this_arg */
	, UnityAction_1_t2764_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2764_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2764)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5942_il2cpp_TypeInfo;

// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42469_MethodInfo;
static PropertyInfo IEnumerator_1_t5942____Current_PropertyInfo = 
{
	&IEnumerator_1_t5942_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42469_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5942_PropertyInfos[] =
{
	&IEnumerator_1_t5942____Current_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42469_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42469_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5942_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoBehaviour_t3_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42469_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5942_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42469_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5942_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5942_0_0_0;
extern Il2CppType IEnumerator_1_t5942_1_0_0;
struct IEnumerator_1_t5942;
extern Il2CppGenericClass IEnumerator_1_t5942_GenericClass;
TypeInfo IEnumerator_1_t5942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5942_MethodInfos/* methods */
	, IEnumerator_1_t5942_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5942_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5942_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5942_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5942_0_0_0/* byval_arg */
	, &IEnumerator_1_t5942_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2765_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_7MethodDeclarations.h"

extern TypeInfo CloudRecoBehaviour_t3_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14219_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCloudRecoBehaviour_t3_m32323_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCloudRecoBehaviour_t3_m32323(__this, p0, method) (CloudRecoBehaviour_t3 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2765____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2765, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2765____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2765, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2765_FieldInfos[] =
{
	&InternalEnumerator_1_t2765____array_0_FieldInfo,
	&InternalEnumerator_1_t2765____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2765____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2765____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14219_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2765_PropertyInfos[] =
{
	&InternalEnumerator_1_t2765____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2765____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2765_InternalEnumerator_1__ctor_m14215_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14215_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2765_InternalEnumerator_1__ctor_m14215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14215_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14217_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14217_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14217_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14218_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14218_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14218_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14219_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14219_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoBehaviour_t3_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14219_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2765_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14215_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_MethodInfo,
	&InternalEnumerator_1_Dispose_m14217_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14218_MethodInfo,
	&InternalEnumerator_1_get_Current_m14219_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14218_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14217_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2765_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14216_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14218_MethodInfo,
	&InternalEnumerator_1_Dispose_m14217_MethodInfo,
	&InternalEnumerator_1_get_Current_m14219_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2765_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5942_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2765_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5942_il2cpp_TypeInfo, 7},
};
extern TypeInfo CloudRecoBehaviour_t3_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2765_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14219_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t3_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCloudRecoBehaviour_t3_m32323_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2765_0_0_0;
extern Il2CppType InternalEnumerator_1_t2765_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2765_GenericClass;
TypeInfo InternalEnumerator_1_t2765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2765_MethodInfos/* methods */
	, InternalEnumerator_1_t2765_PropertyInfos/* properties */
	, InternalEnumerator_1_t2765_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2765_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2765_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2765_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2765_1_0_0/* this_arg */
	, InternalEnumerator_1_t2765_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2765_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2765)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7548_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>
extern MethodInfo ICollection_1_get_Count_m42470_MethodInfo;
static PropertyInfo ICollection_1_t7548____Count_PropertyInfo = 
{
	&ICollection_1_t7548_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42471_MethodInfo;
static PropertyInfo ICollection_1_t7548____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7548_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7548_PropertyInfos[] =
{
	&ICollection_1_t7548____Count_PropertyInfo,
	&ICollection_1_t7548____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42470_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42470_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42470_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42471_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42471_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42471_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_Add_m42472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42472_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42472_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7548_ICollection_1_Add_m42472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42472_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42473_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42473_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42473_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_Contains_m42474_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42474_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42474_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7548_ICollection_1_Contains_m42474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42474_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviourU5BU5D_t5359_0_0_0;
extern Il2CppType CloudRecoBehaviourU5BU5D_t5359_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_CopyTo_m42475_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviourU5BU5D_t5359_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42475_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42475_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7548_ICollection_1_CopyTo_m42475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42475_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_Remove_m42476_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42476_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42476_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7548_ICollection_1_Remove_m42476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42476_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7548_MethodInfos[] =
{
	&ICollection_1_get_Count_m42470_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42471_MethodInfo,
	&ICollection_1_Add_m42472_MethodInfo,
	&ICollection_1_Clear_m42473_MethodInfo,
	&ICollection_1_Contains_m42474_MethodInfo,
	&ICollection_1_CopyTo_m42475_MethodInfo,
	&ICollection_1_Remove_m42476_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7550_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7548_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7550_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7548_0_0_0;
extern Il2CppType ICollection_1_t7548_1_0_0;
struct ICollection_1_t7548;
extern Il2CppGenericClass ICollection_1_t7548_GenericClass;
TypeInfo ICollection_1_t7548_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7548_MethodInfos/* methods */
	, ICollection_1_t7548_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7548_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7548_0_0_0/* byval_arg */
	, &ICollection_1_t7548_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7548_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType IEnumerator_1_t5942_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42477_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42477_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7550_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5942_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42477_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7550_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42477_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7550_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7550_0_0_0;
extern Il2CppType IEnumerable_1_t7550_1_0_0;
struct IEnumerable_1_t7550;
extern Il2CppGenericClass IEnumerable_1_t7550_GenericClass;
TypeInfo IEnumerable_1_t7550_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7550_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7550_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7550_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7550_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7550_0_0_0/* byval_arg */
	, &IEnumerable_1_t7550_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7550_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7549_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>
extern MethodInfo IList_1_get_Item_m42478_MethodInfo;
extern MethodInfo IList_1_set_Item_m42479_MethodInfo;
static PropertyInfo IList_1_t7549____Item_PropertyInfo = 
{
	&IList_1_t7549_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42478_MethodInfo/* get */
	, &IList_1_set_Item_m42479_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7549_PropertyInfos[] =
{
	&IList_1_t7549____Item_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_IndexOf_m42480_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42480_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42480_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7549_IList_1_IndexOf_m42480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42480_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_Insert_m42481_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42481_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42481_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7549_IList_1_Insert_m42481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42481_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_RemoveAt_m42482_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42482_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42482_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7549_IList_1_RemoveAt_m42482_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42482_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_get_Item_m42478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42478_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42478_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoBehaviour_t3_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7549_IList_1_get_Item_m42478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42478_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_set_Item_m42479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42479_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42479_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7549_IList_1_set_Item_m42479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42479_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7549_MethodInfos[] =
{
	&IList_1_IndexOf_m42480_MethodInfo,
	&IList_1_Insert_m42481_MethodInfo,
	&IList_1_RemoveAt_m42482_MethodInfo,
	&IList_1_get_Item_m42478_MethodInfo,
	&IList_1_set_Item_m42479_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7549_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7548_il2cpp_TypeInfo,
	&IEnumerable_1_t7550_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7549_0_0_0;
extern Il2CppType IList_1_t7549_1_0_0;
struct IList_1_t7549;
extern Il2CppGenericClass IList_1_t7549_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7549_MethodInfos/* methods */
	, IList_1_t7549_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7549_il2cpp_TypeInfo/* element_class */
	, IList_1_t7549_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7549_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7549_0_0_0/* byval_arg */
	, &IList_1_t7549_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7549_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7551_il2cpp_TypeInfo;

// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42483_MethodInfo;
static PropertyInfo ICollection_1_t7551____Count_PropertyInfo = 
{
	&ICollection_1_t7551_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42483_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42484_MethodInfo;
static PropertyInfo ICollection_1_t7551____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7551_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42484_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7551_PropertyInfos[] =
{
	&ICollection_1_t7551____Count_PropertyInfo,
	&ICollection_1_t7551____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42483_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42483_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42483_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42484_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42484_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42484_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_Add_m42485_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t4_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42485_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42485_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7551_ICollection_1_Add_m42485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42485_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42486_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42486_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42486_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_Contains_m42487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t4_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42487_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42487_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7551_ICollection_1_Contains_m42487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42487_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviourU5BU5D_t5620_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviourU5BU5D_t5620_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_CopyTo_m42488_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviourU5BU5D_t5620_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42488_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42488_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7551_ICollection_1_CopyTo_m42488_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42488_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_Remove_m42489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t4_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42489_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42489_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7551_ICollection_1_Remove_m42489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42489_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7551_MethodInfos[] =
{
	&ICollection_1_get_Count_m42483_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42484_MethodInfo,
	&ICollection_1_Add_m42485_MethodInfo,
	&ICollection_1_Clear_m42486_MethodInfo,
	&ICollection_1_Contains_m42487_MethodInfo,
	&ICollection_1_CopyTo_m42488_MethodInfo,
	&ICollection_1_Remove_m42489_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7553_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7551_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7553_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7551_0_0_0;
extern Il2CppType ICollection_1_t7551_1_0_0;
struct ICollection_1_t7551;
extern Il2CppGenericClass ICollection_1_t7551_GenericClass;
TypeInfo ICollection_1_t7551_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7551_MethodInfos/* methods */
	, ICollection_1_t7551_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7551_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7551_0_0_0/* byval_arg */
	, &ICollection_1_t7551_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7551_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5944_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42490_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42490_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7553_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5944_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42490_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7553_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42490_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7553_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7553_0_0_0;
extern Il2CppType IEnumerable_1_t7553_1_0_0;
struct IEnumerable_1_t7553;
extern Il2CppGenericClass IEnumerable_1_t7553_GenericClass;
TypeInfo IEnumerable_1_t7553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7553_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7553_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7553_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7553_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7553_0_0_0/* byval_arg */
	, &IEnumerable_1_t7553_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7553_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5944_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42491_MethodInfo;
static PropertyInfo IEnumerator_1_t5944____Current_PropertyInfo = 
{
	&IEnumerator_1_t5944_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42491_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5944_PropertyInfos[] =
{
	&IEnumerator_1_t5944____Current_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42491_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42491_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5944_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoAbstractBehaviour_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42491_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5944_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42491_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5944_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5944_0_0_0;
extern Il2CppType IEnumerator_1_t5944_1_0_0;
struct IEnumerator_1_t5944;
extern Il2CppGenericClass IEnumerator_1_t5944_GenericClass;
TypeInfo IEnumerator_1_t5944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5944_MethodInfos/* methods */
	, IEnumerator_1_t5944_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5944_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5944_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5944_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5944_0_0_0/* byval_arg */
	, &IEnumerator_1_t5944_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_8.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2766_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_8MethodDeclarations.h"

extern TypeInfo CloudRecoAbstractBehaviour_t4_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14224_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCloudRecoAbstractBehaviour_t4_m32334_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCloudRecoAbstractBehaviour_t4_m32334(__this, p0, method) (CloudRecoAbstractBehaviour_t4 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2766____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2766, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2766____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2766, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2766_FieldInfos[] =
{
	&InternalEnumerator_1_t2766____array_0_FieldInfo,
	&InternalEnumerator_1_t2766____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2766____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2766_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2766____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2766_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14224_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2766_PropertyInfos[] =
{
	&InternalEnumerator_1_t2766____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2766____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2766_InternalEnumerator_1__ctor_m14220_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14220_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14220_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2766_InternalEnumerator_1__ctor_m14220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14220_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14222_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14222_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14222_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14223_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14223_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14223_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14224_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14224_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoAbstractBehaviour_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14224_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2766_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14220_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_MethodInfo,
	&InternalEnumerator_1_Dispose_m14222_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14223_MethodInfo,
	&InternalEnumerator_1_get_Current_m14224_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14223_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14222_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2766_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14221_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14223_MethodInfo,
	&InternalEnumerator_1_Dispose_m14222_MethodInfo,
	&InternalEnumerator_1_get_Current_m14224_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2766_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5944_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2766_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5944_il2cpp_TypeInfo, 7},
};
extern TypeInfo CloudRecoAbstractBehaviour_t4_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2766_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14224_MethodInfo/* Method Usage */,
	&CloudRecoAbstractBehaviour_t4_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCloudRecoAbstractBehaviour_t4_m32334_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2766_0_0_0;
extern Il2CppType InternalEnumerator_1_t2766_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2766_GenericClass;
TypeInfo InternalEnumerator_1_t2766_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2766_MethodInfos/* methods */
	, InternalEnumerator_1_t2766_PropertyInfos/* properties */
	, InternalEnumerator_1_t2766_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2766_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2766_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2766_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2766_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2766_1_0_0/* this_arg */
	, InternalEnumerator_1_t2766_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2766_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2766_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2766)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7552_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42492_MethodInfo;
extern MethodInfo IList_1_set_Item_m42493_MethodInfo;
static PropertyInfo IList_1_t7552____Item_PropertyInfo = 
{
	&IList_1_t7552_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42492_MethodInfo/* get */
	, &IList_1_set_Item_m42493_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7552_PropertyInfos[] =
{
	&IList_1_t7552____Item_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_IndexOf_m42494_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t4_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42494_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42494_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7552_IList_1_IndexOf_m42494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42494_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_Insert_m42495_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t4_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42495_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42495_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7552_IList_1_Insert_m42495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42495_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_RemoveAt_m42496_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42496_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42496_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7552_IList_1_RemoveAt_m42496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42496_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_get_Item_m42492_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42492_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42492_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoAbstractBehaviour_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7552_IList_1_get_Item_m42492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42492_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviour_t4_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_set_Item_m42493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t4_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42493_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42493_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7552_IList_1_set_Item_m42493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42493_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7552_MethodInfos[] =
{
	&IList_1_IndexOf_m42494_MethodInfo,
	&IList_1_Insert_m42495_MethodInfo,
	&IList_1_RemoveAt_m42496_MethodInfo,
	&IList_1_get_Item_m42492_MethodInfo,
	&IList_1_set_Item_m42493_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7552_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7551_il2cpp_TypeInfo,
	&IEnumerable_1_t7553_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7552_0_0_0;
extern Il2CppType IList_1_t7552_1_0_0;
struct IList_1_t7552;
extern Il2CppGenericClass IList_1_t7552_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7552_MethodInfos/* methods */
	, IList_1_t7552_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7552_il2cpp_TypeInfo/* element_class */
	, IList_1_t7552_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7552_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7552_0_0_0/* byval_arg */
	, &IList_1_t7552_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7552_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2767_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_5MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_1.h"
extern TypeInfo InvokableCall_1_t2768_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_1MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14227_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14229_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2767____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2767_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2767, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2767_FieldInfos[] =
{
	&CachedInvokableCall_1_t2767____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2767_CachedInvokableCall_1__ctor_m14225_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14225_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14225_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2767_CachedInvokableCall_1__ctor_m14225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14225_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2767_CachedInvokableCall_1_Invoke_m14226_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14226_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14226_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2767_CachedInvokableCall_1_Invoke_m14226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14226_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2767_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14225_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14226_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14226_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14230_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2767_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14226_MethodInfo,
	&InvokableCall_1_Find_m14230_MethodInfo,
};
extern Il2CppType UnityAction_1_t2769_0_0_0;
extern TypeInfo UnityAction_1_t2769_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t3_m32344_MethodInfo;
extern TypeInfo CloudRecoBehaviour_t3_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14232_MethodInfo;
extern TypeInfo CloudRecoBehaviour_t3_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2767_RGCTXData[8] = 
{
	&UnityAction_1_t2769_0_0_0/* Type Usage */,
	&UnityAction_1_t2769_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t3_m32344_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t3_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14232_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14227_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t3_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14229_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2767_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2767_1_0_0;
struct CachedInvokableCall_1_t2767;
extern Il2CppGenericClass CachedInvokableCall_1_t2767_GenericClass;
TypeInfo CachedInvokableCall_1_t2767_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2767_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2767_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2767_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2767_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2767_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2767_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2767_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2767_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2767)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_8.h"
extern TypeInfo UnityAction_1_t2769_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_8MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CloudRecoBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CloudRecoBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t3_m32344(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType UnityAction_1_t2769_0_0_1;
FieldInfo InvokableCall_1_t2768____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2769_0_0_1/* type */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2768, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2768_FieldInfos[] =
{
	&InvokableCall_1_t2768____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2768_InvokableCall_1__ctor_m14227_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14227_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2768_InvokableCall_1__ctor_m14227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14227_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2769_0_0_0;
static ParameterInfo InvokableCall_1_t2768_InvokableCall_1__ctor_m14228_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2769_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14228_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14228_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2768_InvokableCall_1__ctor_m14228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14228_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2768_InvokableCall_1_Invoke_m14229_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14229_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14229_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2768_InvokableCall_1_Invoke_m14229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14229_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2768_InvokableCall_1_Find_m14230_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14230_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14230_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2768_InvokableCall_1_Find_m14230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14230_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2768_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14227_MethodInfo,
	&InvokableCall_1__ctor_m14228_MethodInfo,
	&InvokableCall_1_Invoke_m14229_MethodInfo,
	&InvokableCall_1_Find_m14230_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2768_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14229_MethodInfo,
	&InvokableCall_1_Find_m14230_MethodInfo,
};
extern TypeInfo UnityAction_1_t2769_il2cpp_TypeInfo;
extern TypeInfo CloudRecoBehaviour_t3_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2768_RGCTXData[5] = 
{
	&UnityAction_1_t2769_0_0_0/* Type Usage */,
	&UnityAction_1_t2769_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t3_m32344_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t3_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14232_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2768_0_0_0;
extern Il2CppType InvokableCall_1_t2768_1_0_0;
struct InvokableCall_1_t2768;
extern Il2CppGenericClass InvokableCall_1_t2768_GenericClass;
TypeInfo InvokableCall_1_t2768_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2768_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2768_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2768_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2768_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2768_0_0_0/* byval_arg */
	, &InvokableCall_1_t2768_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2768_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2768_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2768)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2769_UnityAction_1__ctor_m14231_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14231_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14231_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2769_UnityAction_1__ctor_m14231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14231_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
static ParameterInfo UnityAction_1_t2769_UnityAction_1_Invoke_m14232_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14232_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14232_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2769_UnityAction_1_Invoke_m14232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14232_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t3_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2769_UnityAction_1_BeginInvoke_m14233_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t3_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14233_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14233_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2769_UnityAction_1_BeginInvoke_m14233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14233_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2769_UnityAction_1_EndInvoke_m14234_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14234_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14234_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2769_UnityAction_1_EndInvoke_m14234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14234_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2769_MethodInfos[] =
{
	&UnityAction_1__ctor_m14231_MethodInfo,
	&UnityAction_1_Invoke_m14232_MethodInfo,
	&UnityAction_1_BeginInvoke_m14233_MethodInfo,
	&UnityAction_1_EndInvoke_m14234_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14233_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14234_MethodInfo;
static MethodInfo* UnityAction_1_t2769_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14232_MethodInfo,
	&UnityAction_1_BeginInvoke_m14233_MethodInfo,
	&UnityAction_1_EndInvoke_m14234_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2769_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2769_1_0_0;
struct UnityAction_1_t2769;
extern Il2CppGenericClass UnityAction_1_t2769_GenericClass;
TypeInfo UnityAction_1_t2769_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2769_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2769_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2769_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2769_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2769_0_0_0/* byval_arg */
	, &UnityAction_1_t2769_1_0_0/* this_arg */
	, UnityAction_1_t2769_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2769)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5946_il2cpp_TypeInfo;

// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42497_MethodInfo;
static PropertyInfo IEnumerator_1_t5946____Current_PropertyInfo = 
{
	&IEnumerator_1_t5946_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42497_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5946_PropertyInfos[] =
{
	&IEnumerator_1_t5946____Current_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42497_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42497_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5946_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetBehaviour_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42497_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5946_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42497_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5946_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5946_0_0_0;
extern Il2CppType IEnumerator_1_t5946_1_0_0;
struct IEnumerator_1_t5946;
extern Il2CppGenericClass IEnumerator_1_t5946_GenericClass;
TypeInfo IEnumerator_1_t5946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5946_MethodInfos/* methods */
	, IEnumerator_1_t5946_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5946_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5946_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5946_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5946_0_0_0/* byval_arg */
	, &IEnumerator_1_t5946_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2770_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_9MethodDeclarations.h"

extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14239_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCylinderTargetBehaviour_t5_m32346_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCylinderTargetBehaviour_t5_m32346(__this, p0, method) (CylinderTargetBehaviour_t5 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2770____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2770, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2770____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2770, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2770_FieldInfos[] =
{
	&InternalEnumerator_1_t2770____array_0_FieldInfo,
	&InternalEnumerator_1_t2770____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2770____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2770_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2770____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2770_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2770_PropertyInfos[] =
{
	&InternalEnumerator_1_t2770____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2770____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2770_InternalEnumerator_1__ctor_m14235_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14235_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2770_InternalEnumerator_1__ctor_m14235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14235_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14237_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14237_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14237_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14238_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14238_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14238_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14239_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14239_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetBehaviour_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14239_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2770_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14235_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_MethodInfo,
	&InternalEnumerator_1_Dispose_m14237_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14238_MethodInfo,
	&InternalEnumerator_1_get_Current_m14239_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14238_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14237_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2770_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14236_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14238_MethodInfo,
	&InternalEnumerator_1_Dispose_m14237_MethodInfo,
	&InternalEnumerator_1_get_Current_m14239_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2770_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5946_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2770_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5946_il2cpp_TypeInfo, 7},
};
extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2770_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14239_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t5_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCylinderTargetBehaviour_t5_m32346_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2770_0_0_0;
extern Il2CppType InternalEnumerator_1_t2770_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2770_GenericClass;
TypeInfo InternalEnumerator_1_t2770_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2770_MethodInfos/* methods */
	, InternalEnumerator_1_t2770_PropertyInfos/* properties */
	, InternalEnumerator_1_t2770_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2770_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2770_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2770_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2770_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2770_1_0_0/* this_arg */
	, InternalEnumerator_1_t2770_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2770_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2770_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2770)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7554_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m42498_MethodInfo;
static PropertyInfo ICollection_1_t7554____Count_PropertyInfo = 
{
	&ICollection_1_t7554_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42498_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42499_MethodInfo;
static PropertyInfo ICollection_1_t7554____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7554_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42499_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7554_PropertyInfos[] =
{
	&ICollection_1_t7554____Count_PropertyInfo,
	&ICollection_1_t7554____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42498_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42498_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42498_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42499_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42499_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42499_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_Add_m42500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42500_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42500_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7554_ICollection_1_Add_m42500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42500_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42501_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42501_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42501_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_Contains_m42502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42502_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42502_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7554_ICollection_1_Contains_m42502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42502_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviourU5BU5D_t5360_0_0_0;
extern Il2CppType CylinderTargetBehaviourU5BU5D_t5360_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_CopyTo_m42503_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviourU5BU5D_t5360_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42503_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42503_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7554_ICollection_1_CopyTo_m42503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42503_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_Remove_m42504_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42504_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42504_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7554_ICollection_1_Remove_m42504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42504_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7554_MethodInfos[] =
{
	&ICollection_1_get_Count_m42498_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42499_MethodInfo,
	&ICollection_1_Add_m42500_MethodInfo,
	&ICollection_1_Clear_m42501_MethodInfo,
	&ICollection_1_Contains_m42502_MethodInfo,
	&ICollection_1_CopyTo_m42503_MethodInfo,
	&ICollection_1_Remove_m42504_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7556_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7554_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7556_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7554_0_0_0;
extern Il2CppType ICollection_1_t7554_1_0_0;
struct ICollection_1_t7554;
extern Il2CppGenericClass ICollection_1_t7554_GenericClass;
TypeInfo ICollection_1_t7554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7554_MethodInfos/* methods */
	, ICollection_1_t7554_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7554_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7554_0_0_0/* byval_arg */
	, &ICollection_1_t7554_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7554_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType IEnumerator_1_t5946_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42505_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42505_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7556_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5946_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42505_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7556_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42505_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7556_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7556_0_0_0;
extern Il2CppType IEnumerable_1_t7556_1_0_0;
struct IEnumerable_1_t7556;
extern Il2CppGenericClass IEnumerable_1_t7556_GenericClass;
TypeInfo IEnumerable_1_t7556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7556_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7556_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7556_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7556_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7556_0_0_0/* byval_arg */
	, &IEnumerable_1_t7556_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7556_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7555_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>
extern MethodInfo IList_1_get_Item_m42506_MethodInfo;
extern MethodInfo IList_1_set_Item_m42507_MethodInfo;
static PropertyInfo IList_1_t7555____Item_PropertyInfo = 
{
	&IList_1_t7555_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42506_MethodInfo/* get */
	, &IList_1_set_Item_m42507_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7555_PropertyInfos[] =
{
	&IList_1_t7555____Item_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_IndexOf_m42508_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42508_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42508_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7555_IList_1_IndexOf_m42508_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42508_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_Insert_m42509_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42509_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42509_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7555_IList_1_Insert_m42509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42509_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_RemoveAt_m42510_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42510_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42510_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7555_IList_1_RemoveAt_m42510_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42510_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_get_Item_m42506_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42506_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42506_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetBehaviour_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7555_IList_1_get_Item_m42506_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42506_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_set_Item_m42507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42507_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42507_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7555_IList_1_set_Item_m42507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42507_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7555_MethodInfos[] =
{
	&IList_1_IndexOf_m42508_MethodInfo,
	&IList_1_Insert_m42509_MethodInfo,
	&IList_1_RemoveAt_m42510_MethodInfo,
	&IList_1_get_Item_m42506_MethodInfo,
	&IList_1_set_Item_m42507_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7555_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7554_il2cpp_TypeInfo,
	&IEnumerable_1_t7556_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7555_0_0_0;
extern Il2CppType IList_1_t7555_1_0_0;
struct IList_1_t7555;
extern Il2CppGenericClass IList_1_t7555_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7555_MethodInfos/* methods */
	, IList_1_t7555_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7555_il2cpp_TypeInfo/* element_class */
	, IList_1_t7555_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7555_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7555_0_0_0/* byval_arg */
	, &IList_1_t7555_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7555_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7557_il2cpp_TypeInfo;

// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42511_MethodInfo;
static PropertyInfo ICollection_1_t7557____Count_PropertyInfo = 
{
	&ICollection_1_t7557_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42512_MethodInfo;
static PropertyInfo ICollection_1_t7557____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7557_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7557_PropertyInfos[] =
{
	&ICollection_1_t7557____Count_PropertyInfo,
	&ICollection_1_t7557____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42511_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42511_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42511_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42512_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42512_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42512_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
static ParameterInfo ICollection_1_t7557_ICollection_1_Add_m42513_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42513_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42513_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7557_ICollection_1_Add_m42513_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42513_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42514_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42514_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42514_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
static ParameterInfo ICollection_1_t7557_ICollection_1_Contains_m42515_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t6_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42515_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42515_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7557_ICollection_1_Contains_m42515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42515_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviourU5BU5D_t5621_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviourU5BU5D_t5621_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7557_ICollection_1_CopyTo_m42516_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviourU5BU5D_t5621_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42516_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42516_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7557_ICollection_1_CopyTo_m42516_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42516_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
static ParameterInfo ICollection_1_t7557_ICollection_1_Remove_m42517_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t6_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42517_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42517_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7557_ICollection_1_Remove_m42517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42517_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7557_MethodInfos[] =
{
	&ICollection_1_get_Count_m42511_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42512_MethodInfo,
	&ICollection_1_Add_m42513_MethodInfo,
	&ICollection_1_Clear_m42514_MethodInfo,
	&ICollection_1_Contains_m42515_MethodInfo,
	&ICollection_1_CopyTo_m42516_MethodInfo,
	&ICollection_1_Remove_m42517_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7559_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7557_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7559_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7557_0_0_0;
extern Il2CppType ICollection_1_t7557_1_0_0;
struct ICollection_1_t7557;
extern Il2CppGenericClass ICollection_1_t7557_GenericClass;
TypeInfo ICollection_1_t7557_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7557_MethodInfos/* methods */
	, ICollection_1_t7557_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7557_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7557_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7557_0_0_0/* byval_arg */
	, &ICollection_1_t7557_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7557_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5948_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42518_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42518_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7559_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5948_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42518_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7559_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42518_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7559_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7559_0_0_0;
extern Il2CppType IEnumerable_1_t7559_1_0_0;
struct IEnumerable_1_t7559;
extern Il2CppGenericClass IEnumerable_1_t7559_GenericClass;
TypeInfo IEnumerable_1_t7559_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7559_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7559_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7559_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7559_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7559_0_0_0/* byval_arg */
	, &IEnumerable_1_t7559_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7559_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5948_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42519_MethodInfo;
static PropertyInfo IEnumerator_1_t5948____Current_PropertyInfo = 
{
	&IEnumerator_1_t5948_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42519_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5948_PropertyInfos[] =
{
	&IEnumerator_1_t5948____Current_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42519_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42519_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5948_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t6_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42519_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5948_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42519_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5948_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5948_0_0_0;
extern Il2CppType IEnumerator_1_t5948_1_0_0;
struct IEnumerator_1_t5948;
extern Il2CppGenericClass IEnumerator_1_t5948_GenericClass;
TypeInfo IEnumerator_1_t5948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5948_MethodInfos/* methods */
	, IEnumerator_1_t5948_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5948_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5948_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5948_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5948_0_0_0/* byval_arg */
	, &IEnumerator_1_t5948_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2771_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10MethodDeclarations.h"

extern TypeInfo CylinderTargetAbstractBehaviour_t6_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14244_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCylinderTargetAbstractBehaviour_t6_m32357_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCylinderTargetAbstractBehaviour_t6_m32357(__this, p0, method) (CylinderTargetAbstractBehaviour_t6 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2771____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2771, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2771____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2771, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2771_FieldInfos[] =
{
	&InternalEnumerator_1_t2771____array_0_FieldInfo,
	&InternalEnumerator_1_t2771____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2771____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2771_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2771____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2771_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14244_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2771_PropertyInfos[] =
{
	&InternalEnumerator_1_t2771____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2771____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2771_InternalEnumerator_1__ctor_m14240_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14240_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14240_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2771_InternalEnumerator_1__ctor_m14240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14240_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14242_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14242_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14242_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14243_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14243_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14243_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14244_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14244_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t6_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14244_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2771_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14240_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_MethodInfo,
	&InternalEnumerator_1_Dispose_m14242_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14243_MethodInfo,
	&InternalEnumerator_1_get_Current_m14244_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14243_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14242_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2771_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14241_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14243_MethodInfo,
	&InternalEnumerator_1_Dispose_m14242_MethodInfo,
	&InternalEnumerator_1_get_Current_m14244_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2771_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5948_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2771_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5948_il2cpp_TypeInfo, 7},
};
extern TypeInfo CylinderTargetAbstractBehaviour_t6_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2771_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14244_MethodInfo/* Method Usage */,
	&CylinderTargetAbstractBehaviour_t6_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCylinderTargetAbstractBehaviour_t6_m32357_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2771_0_0_0;
extern Il2CppType InternalEnumerator_1_t2771_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2771_GenericClass;
TypeInfo InternalEnumerator_1_t2771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2771_MethodInfos/* methods */
	, InternalEnumerator_1_t2771_PropertyInfos/* properties */
	, InternalEnumerator_1_t2771_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2771_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2771_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2771_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2771_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2771_1_0_0/* this_arg */
	, InternalEnumerator_1_t2771_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2771_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2771)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7558_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42520_MethodInfo;
extern MethodInfo IList_1_set_Item_m42521_MethodInfo;
static PropertyInfo IList_1_t7558____Item_PropertyInfo = 
{
	&IList_1_t7558_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42520_MethodInfo/* get */
	, &IList_1_set_Item_m42521_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7558_PropertyInfos[] =
{
	&IList_1_t7558____Item_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
static ParameterInfo IList_1_t7558_IList_1_IndexOf_m42522_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t6_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42522_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42522_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7558_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7558_IList_1_IndexOf_m42522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42522_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
static ParameterInfo IList_1_t7558_IList_1_Insert_m42523_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42523_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42523_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7558_IList_1_Insert_m42523_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42523_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7558_IList_1_RemoveAt_m42524_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42524_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42524_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7558_IList_1_RemoveAt_m42524_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42524_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7558_IList_1_get_Item_m42520_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42520_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42520_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7558_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t6_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7558_IList_1_get_Item_m42520_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42520_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviour_t6_0_0_0;
static ParameterInfo IList_1_t7558_IList_1_set_Item_m42521_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42521_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42521_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7558_IList_1_set_Item_m42521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42521_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7558_MethodInfos[] =
{
	&IList_1_IndexOf_m42522_MethodInfo,
	&IList_1_Insert_m42523_MethodInfo,
	&IList_1_RemoveAt_m42524_MethodInfo,
	&IList_1_get_Item_m42520_MethodInfo,
	&IList_1_set_Item_m42521_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7558_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7557_il2cpp_TypeInfo,
	&IEnumerable_1_t7559_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7558_0_0_0;
extern Il2CppType IList_1_t7558_1_0_0;
struct IList_1_t7558;
extern Il2CppGenericClass IList_1_t7558_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7558_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7558_MethodInfos/* methods */
	, IList_1_t7558_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7558_il2cpp_TypeInfo/* element_class */
	, IList_1_t7558_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7558_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7558_0_0_0/* byval_arg */
	, &IList_1_t7558_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7558_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7560_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m42525_MethodInfo;
static PropertyInfo ICollection_1_t7560____Count_PropertyInfo = 
{
	&ICollection_1_t7560_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42526_MethodInfo;
static PropertyInfo ICollection_1_t7560____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7560_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42526_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7560_PropertyInfos[] =
{
	&ICollection_1_t7560____Count_PropertyInfo,
	&ICollection_1_t7560____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42525_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42525_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42525_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42526_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42526_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42526_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
static ParameterInfo ICollection_1_t7560_ICollection_1_Add_m42527_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42527_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42527_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7560_ICollection_1_Add_m42527_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42527_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42528_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42528_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42528_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
static ParameterInfo ICollection_1_t7560_ICollection_1_Contains_m42529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t113_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42529_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42529_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7560_ICollection_1_Contains_m42529_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42529_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviourU5BU5D_t5622_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviourU5BU5D_t5622_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7560_ICollection_1_CopyTo_m42530_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviourU5BU5D_t5622_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42530_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42530_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7560_ICollection_1_CopyTo_m42530_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42530_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
static ParameterInfo ICollection_1_t7560_ICollection_1_Remove_m42531_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t113_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42531_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42531_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7560_ICollection_1_Remove_m42531_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42531_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7560_MethodInfos[] =
{
	&ICollection_1_get_Count_m42525_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42526_MethodInfo,
	&ICollection_1_Add_m42527_MethodInfo,
	&ICollection_1_Clear_m42528_MethodInfo,
	&ICollection_1_Contains_m42529_MethodInfo,
	&ICollection_1_CopyTo_m42530_MethodInfo,
	&ICollection_1_Remove_m42531_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7562_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7560_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7562_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7560_0_0_0;
extern Il2CppType ICollection_1_t7560_1_0_0;
struct ICollection_1_t7560;
extern Il2CppGenericClass ICollection_1_t7560_GenericClass;
TypeInfo ICollection_1_t7560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7560_MethodInfos/* methods */
	, ICollection_1_t7560_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7560_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7560_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7560_0_0_0/* byval_arg */
	, &ICollection_1_t7560_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7560_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorCylinderTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorCylinderTargetBehaviour>
extern Il2CppType IEnumerator_1_t5950_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42532_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorCylinderTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42532_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7562_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5950_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42532_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7562_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42532_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7562_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7562_0_0_0;
extern Il2CppType IEnumerable_1_t7562_1_0_0;
struct IEnumerable_1_t7562;
extern Il2CppGenericClass IEnumerable_1_t7562_GenericClass;
TypeInfo IEnumerable_1_t7562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7562_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7562_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7562_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7562_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7562_0_0_0/* byval_arg */
	, &IEnumerable_1_t7562_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7562_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5950_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42533_MethodInfo;
static PropertyInfo IEnumerator_1_t5950____Current_PropertyInfo = 
{
	&IEnumerator_1_t5950_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5950_PropertyInfos[] =
{
	&IEnumerator_1_t5950____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42533_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42533_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5950_il2cpp_TypeInfo/* declaring_type */
	, &IEditorCylinderTargetBehaviour_t113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42533_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5950_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42533_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5950_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5950_0_0_0;
extern Il2CppType IEnumerator_1_t5950_1_0_0;
struct IEnumerator_1_t5950;
extern Il2CppGenericClass IEnumerator_1_t5950_GenericClass;
TypeInfo IEnumerator_1_t5950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5950_MethodInfos/* methods */
	, IEnumerator_1_t5950_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5950_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5950_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5950_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5950_0_0_0/* byval_arg */
	, &IEnumerator_1_t5950_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_11.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2772_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_11MethodDeclarations.h"

extern TypeInfo IEditorCylinderTargetBehaviour_t113_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14249_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorCylinderTargetBehaviour_t113_m32368_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorCylinderTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorCylinderTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorCylinderTargetBehaviour_t113_m32368(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2772____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2772, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2772____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2772, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2772_FieldInfos[] =
{
	&InternalEnumerator_1_t2772____array_0_FieldInfo,
	&InternalEnumerator_1_t2772____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2772____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2772_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2772____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2772_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14249_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2772_PropertyInfos[] =
{
	&InternalEnumerator_1_t2772____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2772____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2772_InternalEnumerator_1__ctor_m14245_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14245_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14245_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2772_InternalEnumerator_1__ctor_m14245_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14245_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14247_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14247_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14247_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14248_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14248_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14248_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14249_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14249_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &IEditorCylinderTargetBehaviour_t113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14249_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2772_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14245_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_MethodInfo,
	&InternalEnumerator_1_Dispose_m14247_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14248_MethodInfo,
	&InternalEnumerator_1_get_Current_m14249_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14248_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14247_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2772_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14246_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14248_MethodInfo,
	&InternalEnumerator_1_Dispose_m14247_MethodInfo,
	&InternalEnumerator_1_get_Current_m14249_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2772_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5950_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2772_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5950_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorCylinderTargetBehaviour_t113_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2772_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14249_MethodInfo/* Method Usage */,
	&IEditorCylinderTargetBehaviour_t113_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorCylinderTargetBehaviour_t113_m32368_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2772_0_0_0;
extern Il2CppType InternalEnumerator_1_t2772_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2772_GenericClass;
TypeInfo InternalEnumerator_1_t2772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2772_MethodInfos/* methods */
	, InternalEnumerator_1_t2772_PropertyInfos/* properties */
	, InternalEnumerator_1_t2772_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2772_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2772_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2772_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2772_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2772_1_0_0/* this_arg */
	, InternalEnumerator_1_t2772_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2772_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2772_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2772)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7561_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>
extern MethodInfo IList_1_get_Item_m42534_MethodInfo;
extern MethodInfo IList_1_set_Item_m42535_MethodInfo;
static PropertyInfo IList_1_t7561____Item_PropertyInfo = 
{
	&IList_1_t7561_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42534_MethodInfo/* get */
	, &IList_1_set_Item_m42535_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7561_PropertyInfos[] =
{
	&IList_1_t7561____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
static ParameterInfo IList_1_t7561_IList_1_IndexOf_m42536_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t113_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42536_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42536_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7561_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7561_IList_1_IndexOf_m42536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42536_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
static ParameterInfo IList_1_t7561_IList_1_Insert_m42537_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42537_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42537_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7561_IList_1_Insert_m42537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42537_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7561_IList_1_RemoveAt_m42538_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42538_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42538_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7561_IList_1_RemoveAt_m42538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42538_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7561_IList_1_get_Item_m42534_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42534_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42534_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7561_il2cpp_TypeInfo/* declaring_type */
	, &IEditorCylinderTargetBehaviour_t113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7561_IList_1_get_Item_m42534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42534_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviour_t113_0_0_0;
static ParameterInfo IList_1_t7561_IList_1_set_Item_m42535_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t113_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42535_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42535_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7561_IList_1_set_Item_m42535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42535_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7561_MethodInfos[] =
{
	&IList_1_IndexOf_m42536_MethodInfo,
	&IList_1_Insert_m42537_MethodInfo,
	&IList_1_RemoveAt_m42538_MethodInfo,
	&IList_1_get_Item_m42534_MethodInfo,
	&IList_1_set_Item_m42535_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7561_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7560_il2cpp_TypeInfo,
	&IEnumerable_1_t7562_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7561_0_0_0;
extern Il2CppType IList_1_t7561_1_0_0;
struct IList_1_t7561;
extern Il2CppGenericClass IList_1_t7561_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7561_MethodInfos/* methods */
	, IList_1_t7561_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7561_il2cpp_TypeInfo/* element_class */
	, IList_1_t7561_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7561_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7561_0_0_0/* byval_arg */
	, &IList_1_t7561_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7561_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7563_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m42539_MethodInfo;
static PropertyInfo ICollection_1_t7563____Count_PropertyInfo = 
{
	&ICollection_1_t7563_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42539_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42540_MethodInfo;
static PropertyInfo ICollection_1_t7563____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7563_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7563_PropertyInfos[] =
{
	&ICollection_1_t7563____Count_PropertyInfo,
	&ICollection_1_t7563____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42539_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42539_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42539_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42540_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42540_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42540_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
static ParameterInfo ICollection_1_t7563_ICollection_1_Add_m42541_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t114_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42541_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42541_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7563_ICollection_1_Add_m42541_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42541_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42542_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42542_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42542_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
static ParameterInfo ICollection_1_t7563_ICollection_1_Contains_m42543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t114_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42543_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42543_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7563_ICollection_1_Contains_m42543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42543_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviourU5BU5D_t5623_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviourU5BU5D_t5623_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7563_ICollection_1_CopyTo_m42544_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviourU5BU5D_t5623_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42544_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42544_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7563_ICollection_1_CopyTo_m42544_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42544_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
static ParameterInfo ICollection_1_t7563_ICollection_1_Remove_m42545_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t114_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42545_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42545_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7563_ICollection_1_Remove_m42545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42545_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7563_MethodInfos[] =
{
	&ICollection_1_get_Count_m42539_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42540_MethodInfo,
	&ICollection_1_Add_m42541_MethodInfo,
	&ICollection_1_Clear_m42542_MethodInfo,
	&ICollection_1_Contains_m42543_MethodInfo,
	&ICollection_1_CopyTo_m42544_MethodInfo,
	&ICollection_1_Remove_m42545_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7565_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7563_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7565_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7563_0_0_0;
extern Il2CppType ICollection_1_t7563_1_0_0;
struct ICollection_1_t7563;
extern Il2CppGenericClass ICollection_1_t7563_GenericClass;
TypeInfo ICollection_1_t7563_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7563_MethodInfos/* methods */
	, ICollection_1_t7563_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7563_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7563_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7563_0_0_0/* byval_arg */
	, &ICollection_1_t7563_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7563_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorDataSetTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5952_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42546_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorDataSetTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42546_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7565_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5952_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42546_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7565_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42546_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7565_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7565_0_0_0;
extern Il2CppType IEnumerable_1_t7565_1_0_0;
struct IEnumerable_1_t7565;
extern Il2CppGenericClass IEnumerable_1_t7565_GenericClass;
TypeInfo IEnumerable_1_t7565_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7565_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7565_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7565_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7565_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7565_0_0_0/* byval_arg */
	, &IEnumerable_1_t7565_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7565_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5952_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42547_MethodInfo;
static PropertyInfo IEnumerator_1_t5952____Current_PropertyInfo = 
{
	&IEnumerator_1_t5952_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42547_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5952_PropertyInfos[] =
{
	&IEnumerator_1_t5952____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42547_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42547_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5952_il2cpp_TypeInfo/* declaring_type */
	, &IEditorDataSetTrackableBehaviour_t114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42547_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5952_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42547_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5952_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5952_0_0_0;
extern Il2CppType IEnumerator_1_t5952_1_0_0;
struct IEnumerator_1_t5952;
extern Il2CppGenericClass IEnumerator_1_t5952_GenericClass;
TypeInfo IEnumerator_1_t5952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5952_MethodInfos/* methods */
	, IEnumerator_1_t5952_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5952_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5952_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5952_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5952_0_0_0/* byval_arg */
	, &IEnumerator_1_t5952_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_12.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2773_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_12MethodDeclarations.h"

extern TypeInfo IEditorDataSetTrackableBehaviour_t114_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14254_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorDataSetTrackableBehaviour_t114_m32379_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorDataSetTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorDataSetTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorDataSetTrackableBehaviour_t114_m32379(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2773____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2773, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2773____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2773, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2773_FieldInfos[] =
{
	&InternalEnumerator_1_t2773____array_0_FieldInfo,
	&InternalEnumerator_1_t2773____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2773____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2773____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14254_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2773_PropertyInfos[] =
{
	&InternalEnumerator_1_t2773____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2773____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2773_InternalEnumerator_1__ctor_m14250_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14250_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14250_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2773_InternalEnumerator_1__ctor_m14250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14250_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14252_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14252_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14252_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14253_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14253_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14253_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14254_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14254_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &IEditorDataSetTrackableBehaviour_t114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14254_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2773_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14250_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_MethodInfo,
	&InternalEnumerator_1_Dispose_m14252_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14253_MethodInfo,
	&InternalEnumerator_1_get_Current_m14254_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14253_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14252_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2773_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14251_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14253_MethodInfo,
	&InternalEnumerator_1_Dispose_m14252_MethodInfo,
	&InternalEnumerator_1_get_Current_m14254_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2773_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5952_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2773_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5952_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorDataSetTrackableBehaviour_t114_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2773_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14254_MethodInfo/* Method Usage */,
	&IEditorDataSetTrackableBehaviour_t114_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorDataSetTrackableBehaviour_t114_m32379_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2773_0_0_0;
extern Il2CppType InternalEnumerator_1_t2773_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2773_GenericClass;
TypeInfo InternalEnumerator_1_t2773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2773_MethodInfos/* methods */
	, InternalEnumerator_1_t2773_PropertyInfos/* properties */
	, InternalEnumerator_1_t2773_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2773_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2773_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2773_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2773_1_0_0/* this_arg */
	, InternalEnumerator_1_t2773_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2773_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2773)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7564_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m42548_MethodInfo;
extern MethodInfo IList_1_set_Item_m42549_MethodInfo;
static PropertyInfo IList_1_t7564____Item_PropertyInfo = 
{
	&IList_1_t7564_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42548_MethodInfo/* get */
	, &IList_1_set_Item_m42549_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7564_PropertyInfos[] =
{
	&IList_1_t7564____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
static ParameterInfo IList_1_t7564_IList_1_IndexOf_m42550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t114_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42550_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42550_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7564_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7564_IList_1_IndexOf_m42550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42550_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
static ParameterInfo IList_1_t7564_IList_1_Insert_m42551_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t114_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42551_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42551_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7564_IList_1_Insert_m42551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42551_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7564_IList_1_RemoveAt_m42552_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42552_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42552_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7564_IList_1_RemoveAt_m42552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42552_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7564_IList_1_get_Item_m42548_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42548_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42548_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7564_il2cpp_TypeInfo/* declaring_type */
	, &IEditorDataSetTrackableBehaviour_t114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7564_IList_1_get_Item_m42548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42548_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviour_t114_0_0_0;
static ParameterInfo IList_1_t7564_IList_1_set_Item_m42549_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t114_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42549_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42549_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7564_IList_1_set_Item_m42549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42549_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7564_MethodInfos[] =
{
	&IList_1_IndexOf_m42550_MethodInfo,
	&IList_1_Insert_m42551_MethodInfo,
	&IList_1_RemoveAt_m42552_MethodInfo,
	&IList_1_get_Item_m42548_MethodInfo,
	&IList_1_set_Item_m42549_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7564_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7563_il2cpp_TypeInfo,
	&IEnumerable_1_t7565_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7564_0_0_0;
extern Il2CppType IList_1_t7564_1_0_0;
struct IList_1_t7564;
extern Il2CppGenericClass IList_1_t7564_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7564_MethodInfos/* methods */
	, IList_1_t7564_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7564_il2cpp_TypeInfo/* element_class */
	, IList_1_t7564_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7564_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7564_0_0_0/* byval_arg */
	, &IList_1_t7564_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7564_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7566_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m42553_MethodInfo;
static PropertyInfo ICollection_1_t7566____Count_PropertyInfo = 
{
	&ICollection_1_t7566_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42554_MethodInfo;
static PropertyInfo ICollection_1_t7566____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7566_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7566_PropertyInfos[] =
{
	&ICollection_1_t7566____Count_PropertyInfo,
	&ICollection_1_t7566____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42553_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42553_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42553_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42554_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42554_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42554_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
static ParameterInfo ICollection_1_t7566_ICollection_1_Add_m42555_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t115_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42555_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42555_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7566_ICollection_1_Add_m42555_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42555_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42556_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42556_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42556_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
static ParameterInfo ICollection_1_t7566_ICollection_1_Contains_m42557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t115_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42557_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42557_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7566_ICollection_1_Contains_m42557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42557_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviourU5BU5D_t5624_0_0_0;
extern Il2CppType IEditorTrackableBehaviourU5BU5D_t5624_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7566_ICollection_1_CopyTo_m42558_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviourU5BU5D_t5624_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42558_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42558_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7566_ICollection_1_CopyTo_m42558_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42558_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
static ParameterInfo ICollection_1_t7566_ICollection_1_Remove_m42559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t115_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42559_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42559_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7566_ICollection_1_Remove_m42559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42559_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7566_MethodInfos[] =
{
	&ICollection_1_get_Count_m42553_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42554_MethodInfo,
	&ICollection_1_Add_m42555_MethodInfo,
	&ICollection_1_Clear_m42556_MethodInfo,
	&ICollection_1_Contains_m42557_MethodInfo,
	&ICollection_1_CopyTo_m42558_MethodInfo,
	&ICollection_1_Remove_m42559_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7568_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7566_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7568_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7566_0_0_0;
extern Il2CppType ICollection_1_t7566_1_0_0;
struct ICollection_1_t7566;
extern Il2CppGenericClass ICollection_1_t7566_GenericClass;
TypeInfo ICollection_1_t7566_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7566_MethodInfos/* methods */
	, ICollection_1_t7566_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7566_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7566_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7566_0_0_0/* byval_arg */
	, &ICollection_1_t7566_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7566_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5954_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42560_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42560_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7568_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5954_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42560_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7568_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42560_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7568_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7568_0_0_0;
extern Il2CppType IEnumerable_1_t7568_1_0_0;
struct IEnumerable_1_t7568;
extern Il2CppGenericClass IEnumerable_1_t7568_GenericClass;
TypeInfo IEnumerable_1_t7568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7568_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7568_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7568_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7568_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7568_0_0_0/* byval_arg */
	, &IEnumerable_1_t7568_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7568_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5954_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42561_MethodInfo;
static PropertyInfo IEnumerator_1_t5954____Current_PropertyInfo = 
{
	&IEnumerator_1_t5954_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5954_PropertyInfos[] =
{
	&IEnumerator_1_t5954____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42561_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42561_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5954_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTrackableBehaviour_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42561_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5954_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42561_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5954_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5954_0_0_0;
extern Il2CppType IEnumerator_1_t5954_1_0_0;
struct IEnumerator_1_t5954;
extern Il2CppGenericClass IEnumerator_1_t5954_GenericClass;
TypeInfo IEnumerator_1_t5954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5954_MethodInfos/* methods */
	, IEnumerator_1_t5954_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5954_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5954_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5954_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5954_0_0_0/* byval_arg */
	, &IEnumerator_1_t5954_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_13.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2774_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_13MethodDeclarations.h"

extern TypeInfo IEditorTrackableBehaviour_t115_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14259_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorTrackableBehaviour_t115_m32390_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorTrackableBehaviour_t115_m32390(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2774____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2774, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2774____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2774, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2774_FieldInfos[] =
{
	&InternalEnumerator_1_t2774____array_0_FieldInfo,
	&InternalEnumerator_1_t2774____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2774____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2774____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2774_PropertyInfos[] =
{
	&InternalEnumerator_1_t2774____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2774____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2774_InternalEnumerator_1__ctor_m14255_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14255_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14255_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2774_InternalEnumerator_1__ctor_m14255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14255_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14257_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14257_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14257_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14258_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14258_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14258_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14259_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14259_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTrackableBehaviour_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14259_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2774_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14255_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_MethodInfo,
	&InternalEnumerator_1_Dispose_m14257_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14258_MethodInfo,
	&InternalEnumerator_1_get_Current_m14259_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14258_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14257_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2774_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14256_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14258_MethodInfo,
	&InternalEnumerator_1_Dispose_m14257_MethodInfo,
	&InternalEnumerator_1_get_Current_m14259_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2774_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5954_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2774_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5954_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorTrackableBehaviour_t115_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2774_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14259_MethodInfo/* Method Usage */,
	&IEditorTrackableBehaviour_t115_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorTrackableBehaviour_t115_m32390_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2774_0_0_0;
extern Il2CppType InternalEnumerator_1_t2774_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2774_GenericClass;
TypeInfo InternalEnumerator_1_t2774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2774_MethodInfos/* methods */
	, InternalEnumerator_1_t2774_PropertyInfos/* properties */
	, InternalEnumerator_1_t2774_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2774_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2774_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2774_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2774_1_0_0/* this_arg */
	, InternalEnumerator_1_t2774_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2774_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2774_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2774)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7567_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m42562_MethodInfo;
extern MethodInfo IList_1_set_Item_m42563_MethodInfo;
static PropertyInfo IList_1_t7567____Item_PropertyInfo = 
{
	&IList_1_t7567_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42562_MethodInfo/* get */
	, &IList_1_set_Item_m42563_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7567_PropertyInfos[] =
{
	&IList_1_t7567____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
static ParameterInfo IList_1_t7567_IList_1_IndexOf_m42564_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t115_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42564_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42564_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7567_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7567_IList_1_IndexOf_m42564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42564_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
static ParameterInfo IList_1_t7567_IList_1_Insert_m42565_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t115_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42565_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42565_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7567_IList_1_Insert_m42565_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42565_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7567_IList_1_RemoveAt_m42566_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42566_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42566_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7567_IList_1_RemoveAt_m42566_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42566_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7567_IList_1_get_Item_m42562_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42562_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42562_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7567_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTrackableBehaviour_t115_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7567_IList_1_get_Item_m42562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42562_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorTrackableBehaviour_t115_0_0_0;
static ParameterInfo IList_1_t7567_IList_1_set_Item_m42563_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t115_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42563_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42563_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7567_IList_1_set_Item_m42563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42563_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7567_MethodInfos[] =
{
	&IList_1_IndexOf_m42564_MethodInfo,
	&IList_1_Insert_m42565_MethodInfo,
	&IList_1_RemoveAt_m42566_MethodInfo,
	&IList_1_get_Item_m42562_MethodInfo,
	&IList_1_set_Item_m42563_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7567_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7566_il2cpp_TypeInfo,
	&IEnumerable_1_t7568_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7567_0_0_0;
extern Il2CppType IList_1_t7567_1_0_0;
struct IList_1_t7567;
extern Il2CppGenericClass IList_1_t7567_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7567_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7567_MethodInfos/* methods */
	, IList_1_t7567_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7567_il2cpp_TypeInfo/* element_class */
	, IList_1_t7567_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7567_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7567_0_0_0/* byval_arg */
	, &IList_1_t7567_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7567_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7569_il2cpp_TypeInfo;

// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m42567_MethodInfo;
static PropertyInfo ICollection_1_t7569____Count_PropertyInfo = 
{
	&ICollection_1_t7569_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42567_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42568_MethodInfo;
static PropertyInfo ICollection_1_t7569____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7569_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7569_PropertyInfos[] =
{
	&ICollection_1_t7569____Count_PropertyInfo,
	&ICollection_1_t7569____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42567_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42567_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42567_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42568_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42568_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42568_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
static ParameterInfo ICollection_1_t7569_ICollection_1_Add_m42569_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t596_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42569_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42569_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7569_ICollection_1_Add_m42569_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42569_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42570_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42570_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42570_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
static ParameterInfo ICollection_1_t7569_ICollection_1_Contains_m42571_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t596_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42571_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42571_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7569_ICollection_1_Contains_m42571_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42571_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviourU5BU5D_t909_0_0_0;
extern Il2CppType DataSetTrackableBehaviourU5BU5D_t909_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7569_ICollection_1_CopyTo_m42572_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviourU5BU5D_t909_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42572_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42572_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7569_ICollection_1_CopyTo_m42572_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42572_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
static ParameterInfo ICollection_1_t7569_ICollection_1_Remove_m42573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t596_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42573_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42573_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7569_ICollection_1_Remove_m42573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42573_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7569_MethodInfos[] =
{
	&ICollection_1_get_Count_m42567_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42568_MethodInfo,
	&ICollection_1_Add_m42569_MethodInfo,
	&ICollection_1_Clear_m42570_MethodInfo,
	&ICollection_1_Contains_m42571_MethodInfo,
	&ICollection_1_CopyTo_m42572_MethodInfo,
	&ICollection_1_Remove_m42573_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7571_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7569_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7571_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7569_0_0_0;
extern Il2CppType ICollection_1_t7569_1_0_0;
struct ICollection_1_t7569;
extern Il2CppGenericClass ICollection_1_t7569_GenericClass;
TypeInfo ICollection_1_t7569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7569_MethodInfos/* methods */
	, ICollection_1_t7569_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7569_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7569_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7569_0_0_0/* byval_arg */
	, &ICollection_1_t7569_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7569_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DataSetTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5956_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42574_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42574_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7571_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5956_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42574_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7571_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42574_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7571_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7571_0_0_0;
extern Il2CppType IEnumerable_1_t7571_1_0_0;
struct IEnumerable_1_t7571;
extern Il2CppGenericClass IEnumerable_1_t7571_GenericClass;
TypeInfo IEnumerable_1_t7571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7571_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7571_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7571_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7571_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7571_0_0_0/* byval_arg */
	, &IEnumerable_1_t7571_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7571_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5956_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DataSetTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42575_MethodInfo;
static PropertyInfo IEnumerator_1_t5956____Current_PropertyInfo = 
{
	&IEnumerator_1_t5956_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42575_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5956_PropertyInfos[] =
{
	&IEnumerator_1_t5956____Current_PropertyInfo,
	NULL
};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42575_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42575_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5956_il2cpp_TypeInfo/* declaring_type */
	, &DataSetTrackableBehaviour_t596_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42575_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5956_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42575_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5956_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5956_0_0_0;
extern Il2CppType IEnumerator_1_t5956_1_0_0;
struct IEnumerator_1_t5956;
extern Il2CppGenericClass IEnumerator_1_t5956_GenericClass;
TypeInfo IEnumerator_1_t5956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5956_MethodInfos/* methods */
	, IEnumerator_1_t5956_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5956_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5956_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5956_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5956_0_0_0/* byval_arg */
	, &IEnumerator_1_t5956_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_14.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2775_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_14MethodDeclarations.h"

extern TypeInfo DataSetTrackableBehaviour_t596_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14264_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetTrackableBehaviour_t596_m32401_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisDataSetTrackableBehaviour_t596_m32401(__this, p0, method) (DataSetTrackableBehaviour_t596 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2775____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2775, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2775____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2775, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2775_FieldInfos[] =
{
	&InternalEnumerator_1_t2775____array_0_FieldInfo,
	&InternalEnumerator_1_t2775____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2775____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2775____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14264_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2775_PropertyInfos[] =
{
	&InternalEnumerator_1_t2775____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2775____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2775_InternalEnumerator_1__ctor_m14260_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14260_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14260_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2775_InternalEnumerator_1__ctor_m14260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14260_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14262_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14262_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14262_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14263_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14263_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14263_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14264_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14264_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &DataSetTrackableBehaviour_t596_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14264_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2775_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14260_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_MethodInfo,
	&InternalEnumerator_1_Dispose_m14262_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14263_MethodInfo,
	&InternalEnumerator_1_get_Current_m14264_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14263_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14262_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2775_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14263_MethodInfo,
	&InternalEnumerator_1_Dispose_m14262_MethodInfo,
	&InternalEnumerator_1_get_Current_m14264_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2775_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5956_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2775_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5956_il2cpp_TypeInfo, 7},
};
extern TypeInfo DataSetTrackableBehaviour_t596_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2775_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14264_MethodInfo/* Method Usage */,
	&DataSetTrackableBehaviour_t596_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDataSetTrackableBehaviour_t596_m32401_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2775_0_0_0;
extern Il2CppType InternalEnumerator_1_t2775_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2775_GenericClass;
TypeInfo InternalEnumerator_1_t2775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2775_MethodInfos/* methods */
	, InternalEnumerator_1_t2775_PropertyInfos/* properties */
	, InternalEnumerator_1_t2775_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2775_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2775_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2775_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2775_1_0_0/* this_arg */
	, InternalEnumerator_1_t2775_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2775_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2775)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7570_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m42576_MethodInfo;
extern MethodInfo IList_1_set_Item_m42577_MethodInfo;
static PropertyInfo IList_1_t7570____Item_PropertyInfo = 
{
	&IList_1_t7570_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42576_MethodInfo/* get */
	, &IList_1_set_Item_m42577_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7570_PropertyInfos[] =
{
	&IList_1_t7570____Item_PropertyInfo,
	NULL
};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
static ParameterInfo IList_1_t7570_IList_1_IndexOf_m42578_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t596_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42578_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42578_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7570_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7570_IList_1_IndexOf_m42578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42578_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
static ParameterInfo IList_1_t7570_IList_1_Insert_m42579_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t596_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42579_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42579_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7570_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7570_IList_1_Insert_m42579_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42579_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7570_IList_1_RemoveAt_m42580_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42580_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42580_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7570_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7570_IList_1_RemoveAt_m42580_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42580_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7570_IList_1_get_Item_m42576_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42576_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42576_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7570_il2cpp_TypeInfo/* declaring_type */
	, &DataSetTrackableBehaviour_t596_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7570_IList_1_get_Item_m42576_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42576_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DataSetTrackableBehaviour_t596_0_0_0;
static ParameterInfo IList_1_t7570_IList_1_set_Item_m42577_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t596_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42577_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42577_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7570_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7570_IList_1_set_Item_m42577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42577_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7570_MethodInfos[] =
{
	&IList_1_IndexOf_m42578_MethodInfo,
	&IList_1_Insert_m42579_MethodInfo,
	&IList_1_RemoveAt_m42580_MethodInfo,
	&IList_1_get_Item_m42576_MethodInfo,
	&IList_1_set_Item_m42577_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7570_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7569_il2cpp_TypeInfo,
	&IEnumerable_1_t7571_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7570_0_0_0;
extern Il2CppType IList_1_t7570_1_0_0;
struct IList_1_t7570;
extern Il2CppGenericClass IList_1_t7570_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7570_MethodInfos/* methods */
	, IList_1_t7570_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7570_il2cpp_TypeInfo/* element_class */
	, IList_1_t7570_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7570_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7570_0_0_0/* byval_arg */
	, &IList_1_t7570_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7570_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7572_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m42581_MethodInfo;
static PropertyInfo ICollection_1_t7572____Count_PropertyInfo = 
{
	&ICollection_1_t7572_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42582_MethodInfo;
static PropertyInfo ICollection_1_t7572____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7572_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42582_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7572_PropertyInfos[] =
{
	&ICollection_1_t7572____Count_PropertyInfo,
	&ICollection_1_t7572____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42581_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42581_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42581_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42582_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42582_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42582_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
static ParameterInfo ICollection_1_t7572_ICollection_1_Add_m42583_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t116_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42583_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42583_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7572_ICollection_1_Add_m42583_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42583_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42584_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42584_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42584_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
static ParameterInfo ICollection_1_t7572_ICollection_1_Contains_m42585_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t116_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42585_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42585_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7572_ICollection_1_Contains_m42585_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42585_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviourU5BU5D_t5625_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviourU5BU5D_t5625_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7572_ICollection_1_CopyTo_m42586_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviourU5BU5D_t5625_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42586_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42586_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7572_ICollection_1_CopyTo_m42586_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42586_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
static ParameterInfo ICollection_1_t7572_ICollection_1_Remove_m42587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t116_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42587_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42587_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7572_ICollection_1_Remove_m42587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42587_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7572_MethodInfos[] =
{
	&ICollection_1_get_Count_m42581_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42582_MethodInfo,
	&ICollection_1_Add_m42583_MethodInfo,
	&ICollection_1_Clear_m42584_MethodInfo,
	&ICollection_1_Contains_m42585_MethodInfo,
	&ICollection_1_CopyTo_m42586_MethodInfo,
	&ICollection_1_Remove_m42587_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7574_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7572_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7574_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7572_0_0_0;
extern Il2CppType ICollection_1_t7572_1_0_0;
struct ICollection_1_t7572;
extern Il2CppGenericClass ICollection_1_t7572_GenericClass;
TypeInfo ICollection_1_t7572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7572_MethodInfos/* methods */
	, ICollection_1_t7572_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7572_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7572_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7572_0_0_0/* byval_arg */
	, &ICollection_1_t7572_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7572_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WorldCenterTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WorldCenterTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5958_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42588_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WorldCenterTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42588_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7574_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5958_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42588_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7574_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42588_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7574_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7574_0_0_0;
extern Il2CppType IEnumerable_1_t7574_1_0_0;
struct IEnumerable_1_t7574;
extern Il2CppGenericClass IEnumerable_1_t7574_GenericClass;
TypeInfo IEnumerable_1_t7574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7574_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7574_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7574_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7574_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7574_0_0_0/* byval_arg */
	, &IEnumerable_1_t7574_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7574_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5958_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42589_MethodInfo;
static PropertyInfo IEnumerator_1_t5958____Current_PropertyInfo = 
{
	&IEnumerator_1_t5958_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42589_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5958_PropertyInfos[] =
{
	&IEnumerator_1_t5958____Current_PropertyInfo,
	NULL
};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42589_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42589_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5958_il2cpp_TypeInfo/* declaring_type */
	, &WorldCenterTrackableBehaviour_t116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42589_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5958_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42589_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5958_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5958_0_0_0;
extern Il2CppType IEnumerator_1_t5958_1_0_0;
struct IEnumerator_1_t5958;
extern Il2CppGenericClass IEnumerator_1_t5958_GenericClass;
TypeInfo IEnumerator_1_t5958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5958_MethodInfos/* methods */
	, IEnumerator_1_t5958_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5958_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5958_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5958_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5958_0_0_0/* byval_arg */
	, &IEnumerator_1_t5958_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5958_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_15.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2776_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_15MethodDeclarations.h"

extern TypeInfo WorldCenterTrackableBehaviour_t116_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14269_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWorldCenterTrackableBehaviour_t116_m32412_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WorldCenterTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WorldCenterTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWorldCenterTrackableBehaviour_t116_m32412(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2776____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2776, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2776____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2776, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2776_FieldInfos[] =
{
	&InternalEnumerator_1_t2776____array_0_FieldInfo,
	&InternalEnumerator_1_t2776____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2776____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2776____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14269_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2776_PropertyInfos[] =
{
	&InternalEnumerator_1_t2776____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2776____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2776_InternalEnumerator_1__ctor_m14265_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14265_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14265_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2776_InternalEnumerator_1__ctor_m14265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14265_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14267_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14267_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14267_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14268_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14268_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14268_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14269_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14269_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &WorldCenterTrackableBehaviour_t116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14269_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2776_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14265_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_MethodInfo,
	&InternalEnumerator_1_Dispose_m14267_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14268_MethodInfo,
	&InternalEnumerator_1_get_Current_m14269_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14268_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14267_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2776_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14266_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14268_MethodInfo,
	&InternalEnumerator_1_Dispose_m14267_MethodInfo,
	&InternalEnumerator_1_get_Current_m14269_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2776_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5958_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2776_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5958_il2cpp_TypeInfo, 7},
};
extern TypeInfo WorldCenterTrackableBehaviour_t116_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2776_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14269_MethodInfo/* Method Usage */,
	&WorldCenterTrackableBehaviour_t116_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWorldCenterTrackableBehaviour_t116_m32412_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2776_0_0_0;
extern Il2CppType InternalEnumerator_1_t2776_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2776_GenericClass;
TypeInfo InternalEnumerator_1_t2776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2776_MethodInfos/* methods */
	, InternalEnumerator_1_t2776_PropertyInfos/* properties */
	, InternalEnumerator_1_t2776_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2776_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2776_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2776_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2776_1_0_0/* this_arg */
	, InternalEnumerator_1_t2776_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2776_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2776_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2776)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7573_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m42590_MethodInfo;
extern MethodInfo IList_1_set_Item_m42591_MethodInfo;
static PropertyInfo IList_1_t7573____Item_PropertyInfo = 
{
	&IList_1_t7573_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42590_MethodInfo/* get */
	, &IList_1_set_Item_m42591_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7573_PropertyInfos[] =
{
	&IList_1_t7573____Item_PropertyInfo,
	NULL
};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
static ParameterInfo IList_1_t7573_IList_1_IndexOf_m42592_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t116_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42592_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42592_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7573_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7573_IList_1_IndexOf_m42592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42592_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
static ParameterInfo IList_1_t7573_IList_1_Insert_m42593_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t116_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42593_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42593_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7573_IList_1_Insert_m42593_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42593_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7573_IList_1_RemoveAt_m42594_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42594_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42594_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7573_IList_1_RemoveAt_m42594_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42594_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7573_IList_1_get_Item_m42590_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42590_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42590_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7573_il2cpp_TypeInfo/* declaring_type */
	, &WorldCenterTrackableBehaviour_t116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7573_IList_1_get_Item_m42590_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42590_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviour_t116_0_0_0;
static ParameterInfo IList_1_t7573_IList_1_set_Item_m42591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t116_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42591_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42591_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7573_IList_1_set_Item_m42591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42591_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7573_MethodInfos[] =
{
	&IList_1_IndexOf_m42592_MethodInfo,
	&IList_1_Insert_m42593_MethodInfo,
	&IList_1_RemoveAt_m42594_MethodInfo,
	&IList_1_get_Item_m42590_MethodInfo,
	&IList_1_set_Item_m42591_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7573_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7572_il2cpp_TypeInfo,
	&IEnumerable_1_t7574_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7573_0_0_0;
extern Il2CppType IList_1_t7573_1_0_0;
struct IList_1_t7573;
extern Il2CppGenericClass IList_1_t7573_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7573_MethodInfos/* methods */
	, IList_1_t7573_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7573_il2cpp_TypeInfo/* element_class */
	, IList_1_t7573_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7573_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7573_0_0_0/* byval_arg */
	, &IList_1_t7573_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7573_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4225_il2cpp_TypeInfo;

// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m37365_MethodInfo;
static PropertyInfo ICollection_1_t4225____Count_PropertyInfo = 
{
	&ICollection_1_t4225_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m37365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42595_MethodInfo;
static PropertyInfo ICollection_1_t4225____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4225_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42595_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4225_PropertyInfos[] =
{
	&ICollection_1_t4225____Count_PropertyInfo,
	&ICollection_1_t4225____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m37365_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m37365_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m37365_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42595_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42595_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42595_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
extern Il2CppType TrackableBehaviour_t17_0_0_0;
static ParameterInfo ICollection_1_t4225_ICollection_1_Add_m42596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t17_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42596_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42596_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t4225_ICollection_1_Add_m42596_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42596_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42597_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42597_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42597_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
static ParameterInfo ICollection_1_t4225_ICollection_1_Contains_m37139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t17_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m37139_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m37139_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4225_ICollection_1_Contains_m37139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m37139_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviourU5BU5D_t857_0_0_0;
extern Il2CppType TrackableBehaviourU5BU5D_t857_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t4225_ICollection_1_CopyTo_m37366_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviourU5BU5D_t857_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m37366_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m37366_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t4225_ICollection_1_CopyTo_m37366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m37366_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
static ParameterInfo ICollection_1_t4225_ICollection_1_Remove_m42598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t17_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42598_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42598_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4225_ICollection_1_Remove_m42598_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42598_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4225_MethodInfos[] =
{
	&ICollection_1_get_Count_m37365_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42595_MethodInfo,
	&ICollection_1_Add_m42596_MethodInfo,
	&ICollection_1_Clear_m42597_MethodInfo,
	&ICollection_1_Contains_m37139_MethodInfo,
	&ICollection_1_CopyTo_m37366_MethodInfo,
	&ICollection_1_Remove_m42598_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t769_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4225_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t769_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4225_0_0_0;
extern Il2CppType ICollection_1_t4225_1_0_0;
struct ICollection_1_t4225;
extern Il2CppGenericClass ICollection_1_t4225_GenericClass;
TypeInfo ICollection_1_t4225_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4225_MethodInfos/* methods */
	, ICollection_1_t4225_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4225_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4225_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4225_0_0_0/* byval_arg */
	, &ICollection_1_t4225_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4225_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
extern Il2CppType IEnumerator_1_t936_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m5513_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m5513_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t769_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t936_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m5513_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t769_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m5513_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t769_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t769_0_0_0;
extern Il2CppType IEnumerable_1_t769_1_0_0;
struct IEnumerable_1_t769;
extern Il2CppGenericClass IEnumerable_1_t769_GenericClass;
TypeInfo IEnumerable_1_t769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t769_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t769_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t769_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t769_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t769_0_0_0/* byval_arg */
	, &IEnumerable_1_t769_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t936_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m5514_MethodInfo;
static PropertyInfo IEnumerator_1_t936____Current_PropertyInfo = 
{
	&IEnumerator_1_t936_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m5514_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t936_PropertyInfos[] =
{
	&IEnumerator_1_t936____Current_PropertyInfo,
	NULL
};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m5514_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m5514_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t936_il2cpp_TypeInfo/* declaring_type */
	, &TrackableBehaviour_t17_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m5514_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t936_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m5514_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t936_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t936_0_0_0;
extern Il2CppType IEnumerator_1_t936_1_0_0;
struct IEnumerator_1_t936;
extern Il2CppGenericClass IEnumerator_1_t936_GenericClass;
TypeInfo IEnumerator_1_t936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t936_MethodInfos/* methods */
	, IEnumerator_1_t936_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t936_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t936_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t936_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t936_0_0_0/* byval_arg */
	, &IEnumerator_1_t936_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_16.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2777_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_16MethodDeclarations.h"

extern TypeInfo TrackableBehaviour_t17_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14274_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTrackableBehaviour_t17_m32423_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTrackableBehaviour_t17_m32423(__this, p0, method) (TrackableBehaviour_t17 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2777____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2777, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2777____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2777, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2777_FieldInfos[] =
{
	&InternalEnumerator_1_t2777____array_0_FieldInfo,
	&InternalEnumerator_1_t2777____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2777____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2777_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2777____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2777_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14274_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2777_PropertyInfos[] =
{
	&InternalEnumerator_1_t2777____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2777____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2777_InternalEnumerator_1__ctor_m14270_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14270_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14270_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2777_InternalEnumerator_1__ctor_m14270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14270_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14272_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14272_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14272_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14273_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14273_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14273_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14274_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14274_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &TrackableBehaviour_t17_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14274_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2777_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14270_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_MethodInfo,
	&InternalEnumerator_1_Dispose_m14272_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14273_MethodInfo,
	&InternalEnumerator_1_get_Current_m14274_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14273_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14272_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2777_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14273_MethodInfo,
	&InternalEnumerator_1_Dispose_m14272_MethodInfo,
	&InternalEnumerator_1_get_Current_m14274_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2777_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t936_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2777_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t936_il2cpp_TypeInfo, 7},
};
extern TypeInfo TrackableBehaviour_t17_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2777_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14274_MethodInfo/* Method Usage */,
	&TrackableBehaviour_t17_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTrackableBehaviour_t17_m32423_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2777_0_0_0;
extern Il2CppType InternalEnumerator_1_t2777_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2777_GenericClass;
TypeInfo InternalEnumerator_1_t2777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2777_MethodInfos/* methods */
	, InternalEnumerator_1_t2777_PropertyInfos/* properties */
	, InternalEnumerator_1_t2777_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2777_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2777_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2777_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2777_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2777_1_0_0/* this_arg */
	, InternalEnumerator_1_t2777_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2777_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2777)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4230_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>
extern MethodInfo IList_1_get_Item_m42599_MethodInfo;
extern MethodInfo IList_1_set_Item_m42600_MethodInfo;
static PropertyInfo IList_1_t4230____Item_PropertyInfo = 
{
	&IList_1_t4230_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42599_MethodInfo/* get */
	, &IList_1_set_Item_m42600_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4230_PropertyInfos[] =
{
	&IList_1_t4230____Item_PropertyInfo,
	NULL
};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
static ParameterInfo IList_1_t4230_IList_1_IndexOf_m42601_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t17_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42601_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42601_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4230_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4230_IList_1_IndexOf_m42601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42601_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TrackableBehaviour_t17_0_0_0;
static ParameterInfo IList_1_t4230_IList_1_Insert_m42602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t17_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42602_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42602_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4230_IList_1_Insert_m42602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42602_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4230_IList_1_RemoveAt_m42603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42603_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42603_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t4230_IList_1_RemoveAt_m42603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42603_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4230_IList_1_get_Item_m42599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TrackableBehaviour_t17_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42599_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42599_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4230_il2cpp_TypeInfo/* declaring_type */
	, &TrackableBehaviour_t17_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t4230_IList_1_get_Item_m42599_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42599_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TrackableBehaviour_t17_0_0_0;
static ParameterInfo IList_1_t4230_IList_1_set_Item_m42600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t17_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42600_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42600_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4230_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4230_IList_1_set_Item_m42600_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42600_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4230_MethodInfos[] =
{
	&IList_1_IndexOf_m42601_MethodInfo,
	&IList_1_Insert_m42602_MethodInfo,
	&IList_1_RemoveAt_m42603_MethodInfo,
	&IList_1_get_Item_m42599_MethodInfo,
	&IList_1_set_Item_m42600_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4230_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t4225_il2cpp_TypeInfo,
	&IEnumerable_1_t769_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4230_0_0_0;
extern Il2CppType IList_1_t4230_1_0_0;
struct IList_1_t4230;
extern Il2CppGenericClass IList_1_t4230_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t4230_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4230_MethodInfos/* methods */
	, IList_1_t4230_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4230_il2cpp_TypeInfo/* element_class */
	, IList_1_t4230_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4230_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4230_0_0_0/* byval_arg */
	, &IList_1_t4230_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4230_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2778_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_6MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2.h"
extern TypeInfo InvokableCall_1_t2779_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14277_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14279_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2778____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2778_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2778, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2778_FieldInfos[] =
{
	&CachedInvokableCall_1_t2778____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2778_CachedInvokableCall_1__ctor_m14275_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14275_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14275_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2778_CachedInvokableCall_1__ctor_m14275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14275_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2778_CachedInvokableCall_1_Invoke_m14276_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14276_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14276_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2778_CachedInvokableCall_1_Invoke_m14276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14276_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2778_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14275_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14276_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14276_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14280_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2778_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14276_MethodInfo,
	&InvokableCall_1_Find_m14280_MethodInfo,
};
extern Il2CppType UnityAction_1_t2780_0_0_0;
extern TypeInfo UnityAction_1_t2780_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t5_m32433_MethodInfo;
extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14282_MethodInfo;
extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2778_RGCTXData[8] = 
{
	&UnityAction_1_t2780_0_0_0/* Type Usage */,
	&UnityAction_1_t2780_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t5_m32433_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t5_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14282_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14277_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t5_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14279_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2778_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2778_1_0_0;
struct CachedInvokableCall_1_t2778;
extern Il2CppGenericClass CachedInvokableCall_1_t2778_GenericClass;
TypeInfo CachedInvokableCall_1_t2778_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2778_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2778_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2778_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2778_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2778_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2778_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2778_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2778_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2778_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2778)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
