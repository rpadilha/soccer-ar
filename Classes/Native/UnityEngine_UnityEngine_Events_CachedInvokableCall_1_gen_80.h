﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Button>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_78.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Button>
struct CachedInvokableCall_1_t3393  : public InvokableCall_1_t3394
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Button>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
