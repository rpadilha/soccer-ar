﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t21;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct UnityAction_1_t3878  : public MulticastDelegate_t373
{
};
