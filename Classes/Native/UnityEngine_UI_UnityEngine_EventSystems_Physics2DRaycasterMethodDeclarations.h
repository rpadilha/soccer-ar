﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.Physics2DRaycaster
struct Physics2DRaycaster_t307;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t239;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t240;

// System.Void UnityEngine.EventSystems.Physics2DRaycaster::.ctor()
 void Physics2DRaycaster__ctor_m1120 (Physics2DRaycaster_t307 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.Physics2DRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
 void Physics2DRaycaster_Raycast_m1121 (Physics2DRaycaster_t307 * __this, PointerEventData_t239 * ___eventData, List_1_t240 * ___resultAppendList, MethodInfo* method) IL2CPP_METHOD_ATTR;
