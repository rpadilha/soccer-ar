﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<ScorerTimeHUD>
struct InternalEnumerator_1_t3047;
// System.Object
struct Object_t;
// ScorerTimeHUD
struct ScorerTimeHUD_t90;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<ScorerTimeHUD>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15524(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<ScorerTimeHUD>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15525(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<ScorerTimeHUD>::Dispose()
#define InternalEnumerator_1_Dispose_m15526(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ScorerTimeHUD>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15527(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<ScorerTimeHUD>::get_Current()
#define InternalEnumerator_1_get_Current_m15528(__this, method) (ScorerTimeHUD_t90 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
