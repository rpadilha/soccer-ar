﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.DataSetImpl>
struct Comparer_1_t4029;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.DataSetImpl>
struct Comparer_1_t4029  : public Object_t
{
};
struct Comparer_1_t4029_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.DataSetImpl>::_default
	Comparer_1_t4029 * ____default_0;
};
