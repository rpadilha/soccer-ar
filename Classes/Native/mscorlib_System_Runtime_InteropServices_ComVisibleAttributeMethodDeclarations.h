﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_t581;

// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
 void ComVisibleAttribute__ctor_m2755 (ComVisibleAttribute_t581 * __this, bool ___visibility, MethodInfo* method) IL2CPP_METHOD_ATTR;
