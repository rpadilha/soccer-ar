﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Single>
struct InternalEnumerator_1_t3824;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m20872 (InternalEnumerator_1_t3824 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20873 (InternalEnumerator_1_t3824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
 void InternalEnumerator_1_Dispose_m20874 (InternalEnumerator_1_t3824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m20875 (InternalEnumerator_1_t3824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
 float InternalEnumerator_1_get_Current_m20876 (InternalEnumerator_1_t3824 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
