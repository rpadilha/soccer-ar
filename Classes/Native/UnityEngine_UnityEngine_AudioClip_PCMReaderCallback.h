﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.Single[]
struct SingleU5BU5D_t622;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1059  : public MulticastDelegate_t373
{
};
