﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
struct InternalEnumerator_1_t5037;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30495 (InternalEnumerator_1_t5037 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496 (InternalEnumerator_1_t5037 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::Dispose()
 void InternalEnumerator_1_Dispose_m30497 (InternalEnumerator_1_t5037 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30498 (InternalEnumerator_1_t5037 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30499 (InternalEnumerator_1_t5037 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
