﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.WordTemplateMode>
struct InternalEnumerator_1_t3998;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WordTemplateMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22308 (InternalEnumerator_1_t3998 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordTemplateMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22309 (InternalEnumerator_1_t3998 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordTemplateMode>::Dispose()
 void InternalEnumerator_1_Dispose_m22310 (InternalEnumerator_1_t3998 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordTemplateMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22311 (InternalEnumerator_1_t3998 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.WordTemplateMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m22312 (InternalEnumerator_1_t3998 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
