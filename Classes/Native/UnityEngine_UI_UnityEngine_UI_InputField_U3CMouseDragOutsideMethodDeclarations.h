﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3
struct U3CMouseDragOutsideRectU3Ec__Iterator3_t376;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::.ctor()
 void U3CMouseDragOutsideRectU3Ec__Iterator3__ctor_m1358 (U3CMouseDragOutsideRectU3Ec__Iterator3_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
 Object_t * U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1359 (U3CMouseDragOutsideRectU3Ec__Iterator3_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::System.Collections.IEnumerator.get_Current()
 Object_t * U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1360 (U3CMouseDragOutsideRectU3Ec__Iterator3_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::MoveNext()
 bool U3CMouseDragOutsideRectU3Ec__Iterator3_MoveNext_m1361 (U3CMouseDragOutsideRectU3Ec__Iterator3_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::Dispose()
 void U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m1362 (U3CMouseDragOutsideRectU3Ec__Iterator3_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3::Reset()
 void U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m1363 (U3CMouseDragOutsideRectU3Ec__Iterator3_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
