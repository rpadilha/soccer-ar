﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1403;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.AsymmetricAlgorithm::.ctor()
 void AsymmetricAlgorithm__ctor_m12020 (AsymmetricAlgorithm_t1403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::System.IDisposable.Dispose()
 void AsymmetricAlgorithm_System_IDisposable_Dispose_m9007 (AsymmetricAlgorithm_t1403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::get_KeySize()
 int32_t AsymmetricAlgorithm_get_KeySize_m7966 (AsymmetricAlgorithm_t1403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::set_KeySize(System.Int32)
 void AsymmetricAlgorithm_set_KeySize_m9004 (AsymmetricAlgorithm_t1403 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::Clear()
 void AsymmetricAlgorithm_Clear_m9133 (AsymmetricAlgorithm_t1403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::Dispose(System.Boolean)
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::FromXmlString(System.String)
// System.String System.Security.Cryptography.AsymmetricAlgorithm::ToXmlString(System.Boolean)
// System.Byte[] System.Security.Cryptography.AsymmetricAlgorithm::GetNamedParam(System.String,System.String)
 ByteU5BU5D_t653* AsymmetricAlgorithm_GetNamedParam_m12021 (Object_t * __this/* static, unused */, String_t* ___xml, String_t* ___param, MethodInfo* method) IL2CPP_METHOD_ATTR;
