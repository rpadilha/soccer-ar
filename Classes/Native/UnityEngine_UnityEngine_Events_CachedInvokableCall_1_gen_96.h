﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ContentSizeFitter>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_98.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ContentSizeFitter>
struct CachedInvokableCall_1_t3739  : public InvokableCall_1_t3740
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ContentSizeFitter>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
