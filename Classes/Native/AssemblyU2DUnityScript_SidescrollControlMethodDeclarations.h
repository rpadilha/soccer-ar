﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SidescrollControl
struct SidescrollControl_t221;

// System.Void SidescrollControl::.ctor()
 void SidescrollControl__ctor_m777 (SidescrollControl_t221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SidescrollControl::Start()
 void SidescrollControl_Start_m778 (SidescrollControl_t221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SidescrollControl::OnEndGame()
 void SidescrollControl_OnEndGame_m779 (SidescrollControl_t221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SidescrollControl::Update()
 void SidescrollControl_Update_m780 (SidescrollControl_t221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SidescrollControl::Main()
 void SidescrollControl_Main_m781 (SidescrollControl_t221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
