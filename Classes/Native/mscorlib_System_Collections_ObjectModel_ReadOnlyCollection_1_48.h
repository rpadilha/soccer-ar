﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.IUserDefinedTargetEventHandler>
struct IList_1_t4550;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ReadOnlyCollection_1_t4546  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::list
	Object_t* ___list_0;
};
