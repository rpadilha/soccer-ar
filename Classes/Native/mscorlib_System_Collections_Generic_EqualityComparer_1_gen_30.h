﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.VirtualButton>
struct EqualityComparer_1_t3934;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.VirtualButton>
struct EqualityComparer_1_t3934  : public Object_t
{
};
struct EqualityComparer_1_t3934_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.VirtualButton>::_default
	EqualityComparer_1_t3934 * ____default_0;
};
