﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t238;

// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
 void UIBehaviour__ctor_m957 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Awake()
 void UIBehaviour_Awake_m958 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
 void UIBehaviour_OnEnable_m959 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Start()
 void UIBehaviour_Start_m960 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
 void UIBehaviour_OnDisable_m961 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDestroy()
 void UIBehaviour_OnDestroy_m962 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive()
 bool UIBehaviour_IsActive_m963 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
 void UIBehaviour_OnRectTransformDimensionsChange_m964 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnBeforeTransformParentChanged()
 void UIBehaviour_OnBeforeTransformParentChanged_m965 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged()
 void UIBehaviour_OnTransformParentChanged_m966 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDidApplyAnimationProperties()
 void UIBehaviour_OnDidApplyAnimationProperties_m967 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasGroupChanged()
 void UIBehaviour_OnCanvasGroupChanged_m968 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
 bool UIBehaviour_IsDestroyed_m969 (UIBehaviour_t238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
