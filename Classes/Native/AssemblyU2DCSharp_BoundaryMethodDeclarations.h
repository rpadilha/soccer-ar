﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Boundary
struct Boundary_t98;

// System.Void Boundary::.ctor()
 void Boundary__ctor_m160 (Boundary_t98 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
