﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct DefaultComparer_t3590;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t517;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor()
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_0MethodDeclarations.h"
#define DefaultComparer__ctor_m19461(__this, method) (void)DefaultComparer__ctor_m14665_gshared((DefaultComparer_t2847 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::GetHashCode(T)
#define DefaultComparer_GetHashCode_m19462(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14666_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Equals(T,T)
#define DefaultComparer_Equals_m19463(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14667_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___x, (Object_t *)___y, method)
