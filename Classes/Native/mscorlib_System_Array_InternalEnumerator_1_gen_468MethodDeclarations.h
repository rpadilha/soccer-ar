﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>
struct InternalEnumerator_1_t4881;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29416 (InternalEnumerator_1_t4881 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::Dispose()
 void InternalEnumerator_1_Dispose_m29418 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29419 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29420 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
