﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct ShimEnumerator_t4280  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::host_enumerator
	Enumerator_t4273  ___host_enumerator_0;
};
