﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.GLErrorHandler
struct GLErrorHandler_t19  : public MonoBehaviour_t10
{
};
struct GLErrorHandler_t19_StaticFields{
	// System.String Vuforia.GLErrorHandler::WINDOW_TITLE
	String_t* ___WINDOW_TITLE_2;
	// System.String Vuforia.GLErrorHandler::mErrorText
	String_t* ___mErrorText_3;
	// System.Boolean Vuforia.GLErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_4;
};
