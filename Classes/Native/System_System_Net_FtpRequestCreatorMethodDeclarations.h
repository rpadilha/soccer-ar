﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t1380;
// System.Net.WebRequest
struct WebRequest_t1374;
// System.Uri
struct Uri_t1375;

// System.Void System.Net.FtpRequestCreator::.ctor()
 void FtpRequestCreator__ctor_m6991 (FtpRequestCreator_t1380 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
 WebRequest_t1374 * FtpRequestCreator_Create_m6992 (FtpRequestCreator_t1380 * __this, Uri_t1375 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
