﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SliderState
struct SliderState_t1115;

// System.Void UnityEngine.SliderState::.ctor()
 void SliderState__ctor_m6491 (SliderState_t1115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
