﻿#pragma once
#include <stdint.h>
// UnityEngine.GUITexture
struct GUITexture_t100;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.GUITexture>
struct CastHelper_1_t3082 
{
	// T UnityEngine.CastHelper`1<UnityEngine.GUITexture>::t
	GUITexture_t100 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.GUITexture>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
