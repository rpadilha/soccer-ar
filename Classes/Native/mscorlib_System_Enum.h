﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t378;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct Enum_t185  : public ValueType_t484
{
};
struct Enum_t185_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t378* ___split_char_0;
};
