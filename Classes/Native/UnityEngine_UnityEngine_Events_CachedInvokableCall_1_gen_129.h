﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_131.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffAbstractBehaviour>
struct CachedInvokableCall_1_t4536  : public InvokableCall_1_t4537
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
