﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>
struct UnityAction_1_t2983;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>
struct InvokableCall_1_t2982  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::Delegate
	UnityAction_1_t2983 * ___Delegate_0;
};
