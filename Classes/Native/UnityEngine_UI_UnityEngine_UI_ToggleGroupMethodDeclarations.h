﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t417;
// UnityEngine.UI.Toggle
struct Toggle_t418;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle>
struct IEnumerable_1_t422;

// System.Void UnityEngine.UI.ToggleGroup::.ctor()
 void ToggleGroup__ctor_m1802 (ToggleGroup_t417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
 bool ToggleGroup_get_allowSwitchOff_m1803 (ToggleGroup_t417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
 void ToggleGroup_set_allowSwitchOff_m1804 (ToggleGroup_t417 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
 void ToggleGroup_ValidateToggleIsInGroup_m1805 (ToggleGroup_t417 * __this, Toggle_t418 * ___toggle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
 void ToggleGroup_NotifyToggleOn_m1806 (ToggleGroup_t417 * __this, Toggle_t418 * ___toggle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
 void ToggleGroup_UnregisterToggle_m1807 (ToggleGroup_t417 * __this, Toggle_t418 * ___toggle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
 void ToggleGroup_RegisterToggle_m1808 (ToggleGroup_t417 * __this, Toggle_t418 * ___toggle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
 bool ToggleGroup_AnyTogglesOn_m1809 (ToggleGroup_t417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
 Object_t* ToggleGroup_ActiveToggles_m1810 (ToggleGroup_t417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
 void ToggleGroup_SetAllTogglesOff_m1811 (ToggleGroup_t417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__7(UnityEngine.UI.Toggle)
 bool ToggleGroup_U3CAnyTogglesOnU3Em__7_m1812 (Object_t * __this/* static, unused */, Toggle_t418 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__8(UnityEngine.UI.Toggle)
 bool ToggleGroup_U3CActiveTogglesU3Em__8_m1813 (Object_t * __this/* static, unused */, Toggle_t418 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
