﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<TapControl>
struct UnityAction_1_t3165;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<TapControl>
struct InvokableCall_1_t3164  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<TapControl>::Delegate
	UnityAction_1_t3165 * ___Delegate_0;
};
