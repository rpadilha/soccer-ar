﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RIPEMD160
struct RIPEMD160_t2158;

// System.Void System.Security.Cryptography.RIPEMD160::.ctor()
 void RIPEMD160__ctor_m12148 (RIPEMD160_t2158 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
