﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>
struct InternalEnumerator_1_t3736;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m20413 (InternalEnumerator_1_t3736 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20414 (InternalEnumerator_1_t3736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>::Dispose()
 void InternalEnumerator_1_Dispose_m20415 (InternalEnumerator_1_t3736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m20416 (InternalEnumerator_1_t3736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m20417 (InternalEnumerator_1_t3736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
