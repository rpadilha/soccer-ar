﻿#pragma once
#include <stdint.h>
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t85;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.RectTransformUtility
struct RectTransformUtility_t513  : public Object_t
{
};
struct RectTransformUtility_t513_StaticFields{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t85* ___s_Corners_0;
};
