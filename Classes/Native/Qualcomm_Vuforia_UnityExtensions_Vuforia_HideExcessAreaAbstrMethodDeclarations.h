﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t21;
// UnityEngine.GameObject
struct GameObject_t29;
// System.String
struct String_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
 GameObject_t29 * HideExcessAreaAbstractBehaviour_CreateQuad_m2848 (HideExcessAreaAbstractBehaviour_t21 * __this, GameObject_t29 * ___parent, String_t* ___name, Vector3_t73  ___position, Quaternion_t108  ___rotation, Vector3_t73  ___scale, int32_t ___layer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
 void HideExcessAreaAbstractBehaviour_SetPlanesActive_m2849 (HideExcessAreaAbstractBehaviour_t21 * __this, bool ___activeflag, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnQCARStarted()
 void HideExcessAreaAbstractBehaviour_OnQCARStarted_m2850 (HideExcessAreaAbstractBehaviour_t21 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
 bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2851 (HideExcessAreaAbstractBehaviour_t21 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
 void HideExcessAreaAbstractBehaviour_Start_m2852 (HideExcessAreaAbstractBehaviour_t21 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
 void HideExcessAreaAbstractBehaviour_OnDestroy_m2853 (HideExcessAreaAbstractBehaviour_t21 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
 void HideExcessAreaAbstractBehaviour_Update_m2854 (HideExcessAreaAbstractBehaviour_t21 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
 void HideExcessAreaAbstractBehaviour__ctor_m290 (HideExcessAreaAbstractBehaviour_t21 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
