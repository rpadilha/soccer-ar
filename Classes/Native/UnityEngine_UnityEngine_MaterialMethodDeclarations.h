﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Material
struct Material_t64;
// UnityEngine.Shader
struct Shader_t173;
// UnityEngine.Texture
struct Texture_t107;
// System.String
struct String_t;

// System.Void UnityEngine.Material::.ctor(System.String)
 void Material__ctor_m517 (Material_t64 * __this, String_t* ___contents, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
 void Material__ctor_m4608 (Material_t64 * __this, Shader_t173 * ___shader, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
 void Material__ctor_m2609 (Material_t64 * __this, Material_t64 * ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Material::get_shader()
 Shader_t173 * Material_get_shader_m519 (Material_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
 Texture_t107 * Material_get_mainTexture_m2617 (Material_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
 void Material_set_mainTexture_m654 (Material_t64 * __this, Texture_t107 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
 void Material_SetTexture_m6071 (Material_t64 * __this, String_t* ___propertyName, Texture_t107 * ___texture, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
 void Material_SetTexture_m6072 (Material_t64 * __this, int32_t ___nameID, Texture_t107 * ___texture, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
 Texture_t107 * Material_GetTexture_m6073 (Material_t64 * __this, String_t* ___propertyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
 Texture_t107 * Material_GetTexture_m6074 (Material_t64 * __this, int32_t ___nameID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
 void Material_SetFloat_m6075 (Material_t64 * __this, String_t* ___propertyName, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
 void Material_SetFloat_m6076 (Material_t64 * __this, int32_t ___nameID, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
 void Material_SetInt_m2611 (Material_t64 * __this, String_t* ___propertyName, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.String)
 bool Material_HasProperty_m2605 (Material_t64 * __this, String_t* ___propertyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
 bool Material_HasProperty_m6077 (Material_t64 * __this, int32_t ___nameID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
 bool Material_SetPass_m531 (Material_t64 * __this, int32_t ___pass, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithString(UnityEngine.Material,System.String)
 void Material_Internal_CreateWithString_m6078 (Object_t * __this/* static, unused */, Material_t64 * ___mono, String_t* ___contents, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
 void Material_Internal_CreateWithShader_m6079 (Object_t * __this/* static, unused */, Material_t64 * ___mono, Shader_t173 * ___shader, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
 void Material_Internal_CreateWithMaterial_m6080 (Object_t * __this/* static, unused */, Material_t64 * ___mono, Material_t64 * ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
