﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableSource
struct TrackableSource_t630;
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilderImpl
struct ImageTargetBuilderImpl_t656  : public ImageTargetBuilder_t645
{
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t630 * ___mTrackableSource_0;
};
