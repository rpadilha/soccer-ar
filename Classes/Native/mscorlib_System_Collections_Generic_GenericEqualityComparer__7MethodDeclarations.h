﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t5070;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
 void GenericEqualityComparer_1__ctor_m30728 (GenericEqualityComparer_1_t5070 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
 int32_t GenericEqualityComparer_1_GetHashCode_m30729 (GenericEqualityComparer_1_t5070 * __this, bool ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
 bool GenericEqualityComparer_1_Equals_m30730 (GenericEqualityComparer_1_t5070 * __this, bool ___x, bool ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
