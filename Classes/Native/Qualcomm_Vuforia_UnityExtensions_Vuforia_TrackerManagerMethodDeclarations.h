﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManager
struct TrackerManager_t787;
// Vuforia.StateManager
struct StateManager_t768;

// Vuforia.TrackerManager Vuforia.TrackerManager::get_Instance()
 TrackerManager_t787 * TrackerManager_get_Instance_m4207 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.StateManager Vuforia.TrackerManager::GetStateManager()
// System.Void Vuforia.TrackerManager::.ctor()
 void TrackerManager__ctor_m4208 (TrackerManager_t787 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.cctor()
 void TrackerManager__cctor_m4209 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
