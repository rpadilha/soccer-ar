﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1058;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody2D>
struct UnityAction_1_t4844  : public MulticastDelegate_t373
{
};
