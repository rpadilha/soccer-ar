﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t762;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.PropAbstractBehaviour>
struct ValueCollection_t904  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.PropAbstractBehaviour>::dictionary
	Dictionary_2_t762 * ___dictionary_0;
};
