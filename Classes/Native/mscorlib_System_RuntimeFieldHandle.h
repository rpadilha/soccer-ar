﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.RuntimeFieldHandle
struct RuntimeFieldHandle_t1760 
{
	// System.IntPtr System.RuntimeFieldHandle::value
	IntPtr_t121 ___value_0;
};
