﻿#pragma once
#include <stdint.h>
// Vuforia.Marker[]
struct MarkerU5BU5D_t4045;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
struct List_1_t849  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Marker>::_items
	MarkerU5BU5D_t4045* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::_version
	int32_t ____version_3;
};
struct List_1_t849_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.Marker>::EmptyArray
	MarkerU5BU5D_t4045* ___EmptyArray_4;
};
