﻿#pragma once
#include <stdint.h>
// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t58;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.VideoBackgroundAbstractBehaviour>
struct CastHelper_1_t4515 
{
	// T UnityEngine.CastHelper`1<Vuforia.VideoBackgroundAbstractBehaviour>::t
	VideoBackgroundAbstractBehaviour_t58 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.VideoBackgroundAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
