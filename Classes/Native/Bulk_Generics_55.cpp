﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerable_1_t9467_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Resources.SatelliteContractVersionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Resources.SatelliteContractVersionAttribute>
extern Il2CppType IEnumerator_1_t7370_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52685_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Resources.SatelliteContractVersionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52685_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9467_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7370_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52685_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9467_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52685_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
static TypeInfo* IEnumerable_1_t9467_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9467_0_0_0;
extern Il2CppType IEnumerable_1_t9467_1_0_0;
struct IEnumerable_1_t9467;
extern Il2CppGenericClass IEnumerable_1_t9467_GenericClass;
TypeInfo IEnumerable_1_t9467_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9467_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9467_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9467_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9467_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9467_0_0_0/* byval_arg */
	, &IEnumerable_1_t9467_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9467_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9466_il2cpp_TypeInfo;

// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"


// System.Int32 System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>
extern MethodInfo IList_1_get_Item_m52686_MethodInfo;
extern MethodInfo IList_1_set_Item_m52687_MethodInfo;
static PropertyInfo IList_1_t9466____Item_PropertyInfo = 
{
	&IList_1_t9466_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52686_MethodInfo/* get */
	, &IList_1_set_Item_m52687_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9466_PropertyInfos[] =
{
	&IList_1_t9466____Item_PropertyInfo,
	NULL
};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
static ParameterInfo IList_1_t9466_IList_1_IndexOf_m52688_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttribute_t1334_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52688_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52688_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9466_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9466_IList_1_IndexOf_m52688_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52688_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
static ParameterInfo IList_1_t9466_IList_1_Insert_m52689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttribute_t1334_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52689_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52689_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9466_IList_1_Insert_m52689_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52689_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9466_IList_1_RemoveAt_m52690_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52690_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52690_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9466_IList_1_RemoveAt_m52690_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52690_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9466_IList_1_get_Item_m52686_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52686_GenericMethod;
// T System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52686_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9466_il2cpp_TypeInfo/* declaring_type */
	, &SatelliteContractVersionAttribute_t1334_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9466_IList_1_get_Item_m52686_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52686_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SatelliteContractVersionAttribute_t1334_0_0_0;
static ParameterInfo IList_1_t9466_IList_1_set_Item_m52687_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SatelliteContractVersionAttribute_t1334_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52687_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52687_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9466_IList_1_set_Item_m52687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52687_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9466_MethodInfos[] =
{
	&IList_1_IndexOf_m52688_MethodInfo,
	&IList_1_Insert_m52689_MethodInfo,
	&IList_1_RemoveAt_m52690_MethodInfo,
	&IList_1_get_Item_m52686_MethodInfo,
	&IList_1_set_Item_m52687_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t9465_il2cpp_TypeInfo;
static TypeInfo* IList_1_t9466_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9465_il2cpp_TypeInfo,
	&IEnumerable_1_t9467_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9466_0_0_0;
extern Il2CppType IList_1_t9466_1_0_0;
struct IList_1_t9466;
extern Il2CppGenericClass IList_1_t9466_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9466_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9466_MethodInfos/* methods */
	, IList_1_t9466_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9466_il2cpp_TypeInfo/* element_class */
	, IList_1_t9466_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9466_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9466_0_0_0/* byval_arg */
	, &IList_1_t9466_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9466_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7372_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern MethodInfo IEnumerator_1_get_Current_m52691_MethodInfo;
static PropertyInfo IEnumerator_1_t7372____Current_PropertyInfo = 
{
	&IEnumerator_1_t7372_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52691_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7372_PropertyInfos[] =
{
	&IEnumerator_1_t7372____Current_PropertyInfo,
	NULL
};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
extern void* RuntimeInvoker_CompilationRelaxations_t2009 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52691_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52691_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7372_il2cpp_TypeInfo/* declaring_type */
	, &CompilationRelaxations_t2009_0_0_0/* return_type */
	, RuntimeInvoker_CompilationRelaxations_t2009/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52691_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7372_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52691_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t7372_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7372_0_0_0;
extern Il2CppType IEnumerator_1_t7372_1_0_0;
struct IEnumerator_1_t7372;
extern Il2CppGenericClass IEnumerator_1_t7372_GenericClass;
TypeInfo IEnumerator_1_t7372_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7372_MethodInfos/* methods */
	, IEnumerator_1_t7372_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7372_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7372_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7372_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7372_0_0_0/* byval_arg */
	, &IEnumerator_1_t7372_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7372_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_702.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5248_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_702MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo CompilationRelaxations_t2009_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m31616_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCompilationRelaxations_t2009_m41444_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.CompilationRelaxations>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.CompilationRelaxations>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCompilationRelaxations_t2009_m41444 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31612_MethodInfo;
 void InternalEnumerator_1__ctor_m31612 (InternalEnumerator_1_t5248 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613 (InternalEnumerator_1_t5248 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31616(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31616_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CompilationRelaxations_t2009_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31614_MethodInfo;
 void InternalEnumerator_1_Dispose_m31614 (InternalEnumerator_1_t5248 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31615_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31615 (InternalEnumerator_1_t5248 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31616 (InternalEnumerator_1_t5248 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCompilationRelaxations_t2009_m41444(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCompilationRelaxations_t2009_m41444_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5248____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5248, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5248____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5248, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5248_FieldInfos[] =
{
	&InternalEnumerator_1_t5248____array_0_FieldInfo,
	&InternalEnumerator_1_t5248____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5248____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5248_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5248____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5248_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31616_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5248_PropertyInfos[] =
{
	&InternalEnumerator_1_t5248____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5248____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5248_InternalEnumerator_1__ctor_m31612_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31612_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31612_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31612/* method */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5248_InternalEnumerator_1__ctor_m31612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31612_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613/* method */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31614_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31614_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31614/* method */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31614_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31615_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31615_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31615/* method */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31615_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
extern void* RuntimeInvoker_CompilationRelaxations_t2009 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31616_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31616_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31616/* method */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* declaring_type */
	, &CompilationRelaxations_t2009_0_0_0/* return_type */
	, RuntimeInvoker_CompilationRelaxations_t2009/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31616_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5248_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31612_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_MethodInfo,
	&InternalEnumerator_1_Dispose_m31614_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31615_MethodInfo,
	&InternalEnumerator_1_get_Current_m31616_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5248_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31613_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31615_MethodInfo,
	&InternalEnumerator_1_Dispose_m31614_MethodInfo,
	&InternalEnumerator_1_get_Current_m31616_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5248_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7372_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5248_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7372_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5248_0_0_0;
extern Il2CppType InternalEnumerator_1_t5248_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5248_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5248_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5248_MethodInfos/* methods */
	, InternalEnumerator_1_t5248_PropertyInfos/* properties */
	, InternalEnumerator_1_t5248_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5248_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5248_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5248_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5248_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5248_1_0_0/* this_arg */
	, InternalEnumerator_1_t5248_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5248_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5248)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9468_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern MethodInfo ICollection_1_get_Count_m52692_MethodInfo;
static PropertyInfo ICollection_1_t9468____Count_PropertyInfo = 
{
	&ICollection_1_t9468_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52692_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52693_MethodInfo;
static PropertyInfo ICollection_1_t9468____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9468_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52693_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9468_PropertyInfos[] =
{
	&ICollection_1_t9468____Count_PropertyInfo,
	&ICollection_1_t9468____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52692_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Count()
MethodInfo ICollection_1_get_Count_m52692_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52692_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52693_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52693_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52693_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
static ParameterInfo ICollection_1_t9468_ICollection_1_Add_m52694_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxations_t2009_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52694_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Add(T)
MethodInfo ICollection_1_Add_m52694_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9468_ICollection_1_Add_m52694_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52694_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52695_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Clear()
MethodInfo ICollection_1_Clear_m52695_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52695_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
static ParameterInfo ICollection_1_t9468_ICollection_1_Contains_m52696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxations_t2009_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52696_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Contains(T)
MethodInfo ICollection_1_Contains_m52696_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9468_ICollection_1_Contains_m52696_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52696_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxationsU5BU5D_t5547_0_0_0;
extern Il2CppType CompilationRelaxationsU5BU5D_t5547_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9468_ICollection_1_CopyTo_m52697_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsU5BU5D_t5547_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52697_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52697_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9468_ICollection_1_CopyTo_m52697_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52697_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
static ParameterInfo ICollection_1_t9468_ICollection_1_Remove_m52698_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxations_t2009_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52698_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>::Remove(T)
MethodInfo ICollection_1_Remove_m52698_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9468_ICollection_1_Remove_m52698_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52698_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9468_MethodInfos[] =
{
	&ICollection_1_get_Count_m52692_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52693_MethodInfo,
	&ICollection_1_Add_m52694_MethodInfo,
	&ICollection_1_Clear_m52695_MethodInfo,
	&ICollection_1_Contains_m52696_MethodInfo,
	&ICollection_1_CopyTo_m52697_MethodInfo,
	&ICollection_1_Remove_m52698_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9470_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9468_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9470_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9468_0_0_0;
extern Il2CppType ICollection_1_t9468_1_0_0;
struct ICollection_1_t9468;
extern Il2CppGenericClass ICollection_1_t9468_GenericClass;
TypeInfo ICollection_1_t9468_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9468_MethodInfos/* methods */
	, ICollection_1_t9468_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9468_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9468_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9468_0_0_0/* byval_arg */
	, &ICollection_1_t9468_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9468_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxations>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern Il2CppType IEnumerator_1_t7372_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52699_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxations>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52699_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9470_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7372_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52699_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9470_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52699_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9470_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9470_0_0_0;
extern Il2CppType IEnumerable_1_t9470_1_0_0;
struct IEnumerable_1_t9470;
extern Il2CppGenericClass IEnumerable_1_t9470_GenericClass;
TypeInfo IEnumerable_1_t9470_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9470_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9470_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9470_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9470_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9470_0_0_0/* byval_arg */
	, &IEnumerable_1_t9470_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9470_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9469_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern MethodInfo IList_1_get_Item_m52700_MethodInfo;
extern MethodInfo IList_1_set_Item_m52701_MethodInfo;
static PropertyInfo IList_1_t9469____Item_PropertyInfo = 
{
	&IList_1_t9469_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52700_MethodInfo/* get */
	, &IList_1_set_Item_m52701_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9469_PropertyInfos[] =
{
	&IList_1_t9469____Item_PropertyInfo,
	NULL
};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
static ParameterInfo IList_1_t9469_IList_1_IndexOf_m52702_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxations_t2009_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52702_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52702_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9469_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9469_IList_1_IndexOf_m52702_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52702_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
static ParameterInfo IList_1_t9469_IList_1_Insert_m52703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxations_t2009_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52703_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52703_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9469_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9469_IList_1_Insert_m52703_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52703_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9469_IList_1_RemoveAt_m52704_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52704_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52704_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9469_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9469_IList_1_RemoveAt_m52704_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52704_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9469_IList_1_get_Item_m52700_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
extern void* RuntimeInvoker_CompilationRelaxations_t2009_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52700_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52700_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9469_il2cpp_TypeInfo/* declaring_type */
	, &CompilationRelaxations_t2009_0_0_0/* return_type */
	, RuntimeInvoker_CompilationRelaxations_t2009_Int32_t123/* invoker_method */
	, IList_1_t9469_IList_1_get_Item_m52700_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52700_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CompilationRelaxations_t2009_0_0_0;
static ParameterInfo IList_1_t9469_IList_1_set_Item_m52701_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxations_t2009_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52701_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52701_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9469_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9469_IList_1_set_Item_m52701_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52701_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9469_MethodInfos[] =
{
	&IList_1_IndexOf_m52702_MethodInfo,
	&IList_1_Insert_m52703_MethodInfo,
	&IList_1_RemoveAt_m52704_MethodInfo,
	&IList_1_get_Item_m52700_MethodInfo,
	&IList_1_set_Item_m52701_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9469_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9468_il2cpp_TypeInfo,
	&IEnumerable_1_t9470_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9469_0_0_0;
extern Il2CppType IList_1_t9469_1_0_0;
struct IList_1_t9469;
extern Il2CppGenericClass IList_1_t9469_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9469_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9469_MethodInfos/* methods */
	, IList_1_t9469_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9469_il2cpp_TypeInfo/* element_class */
	, IList_1_t9469_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9469_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9469_0_0_0/* byval_arg */
	, &IList_1_t9469_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9469_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7374_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52705_MethodInfo;
static PropertyInfo IEnumerator_1_t7374____Current_PropertyInfo = 
{
	&IEnumerator_1_t7374_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52705_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7374_PropertyInfos[] =
{
	&IEnumerator_1_t7374____Current_PropertyInfo,
	NULL
};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52705_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52705_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &CompilationRelaxationsAttribute_t949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52705_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7374_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52705_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7374_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7374_0_0_0;
extern Il2CppType IEnumerator_1_t7374_1_0_0;
struct IEnumerator_1_t7374;
extern Il2CppGenericClass IEnumerator_1_t7374_GenericClass;
TypeInfo IEnumerator_1_t7374_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7374_MethodInfos/* methods */
	, IEnumerator_1_t7374_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7374_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7374_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7374_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7374_0_0_0/* byval_arg */
	, &IEnumerator_1_t7374_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7374_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_703.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5249_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_703MethodDeclarations.h"

extern TypeInfo CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31621_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCompilationRelaxationsAttribute_t949_m41455_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisCompilationRelaxationsAttribute_t949_m41455(__this, p0, method) (CompilationRelaxationsAttribute_t949 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5249____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5249, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5249____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5249, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5249_FieldInfos[] =
{
	&InternalEnumerator_1_t5249____array_0_FieldInfo,
	&InternalEnumerator_1_t5249____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5249____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5249_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5249____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5249_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5249_PropertyInfos[] =
{
	&InternalEnumerator_1_t5249____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5249____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5249_InternalEnumerator_1__ctor_m31617_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31617_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31617_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5249_InternalEnumerator_1__ctor_m31617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31617_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31619_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31619_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31619_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31620_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31620_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31620_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31621_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31621_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* declaring_type */
	, &CompilationRelaxationsAttribute_t949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31621_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5249_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31617_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_MethodInfo,
	&InternalEnumerator_1_Dispose_m31619_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31620_MethodInfo,
	&InternalEnumerator_1_get_Current_m31621_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31620_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31619_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5249_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31618_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31620_MethodInfo,
	&InternalEnumerator_1_Dispose_m31619_MethodInfo,
	&InternalEnumerator_1_get_Current_m31621_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5249_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7374_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5249_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7374_il2cpp_TypeInfo, 7},
};
extern TypeInfo CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5249_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31621_MethodInfo/* Method Usage */,
	&CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCompilationRelaxationsAttribute_t949_m41455_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5249_0_0_0;
extern Il2CppType InternalEnumerator_1_t5249_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5249_GenericClass;
TypeInfo InternalEnumerator_1_t5249_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5249_MethodInfos/* methods */
	, InternalEnumerator_1_t5249_PropertyInfos/* properties */
	, InternalEnumerator_1_t5249_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5249_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5249_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5249_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5249_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5249_1_0_0/* this_arg */
	, InternalEnumerator_1_t5249_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5249_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5249_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5249)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9471_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern MethodInfo ICollection_1_get_Count_m52706_MethodInfo;
static PropertyInfo ICollection_1_t9471____Count_PropertyInfo = 
{
	&ICollection_1_t9471_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52707_MethodInfo;
static PropertyInfo ICollection_1_t9471____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9471_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52707_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9471_PropertyInfos[] =
{
	&ICollection_1_t9471____Count_PropertyInfo,
	&ICollection_1_t9471____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52706_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52706_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52706_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52707_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52707_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52707_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
static ParameterInfo ICollection_1_t9471_ICollection_1_Add_m52708_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttribute_t949_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52708_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52708_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9471_ICollection_1_Add_m52708_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52708_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52709_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52709_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52709_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
static ParameterInfo ICollection_1_t9471_ICollection_1_Contains_m52710_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttribute_t949_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52710_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52710_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9471_ICollection_1_Contains_m52710_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52710_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxationsAttributeU5BU5D_t5548_0_0_0;
extern Il2CppType CompilationRelaxationsAttributeU5BU5D_t5548_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9471_ICollection_1_CopyTo_m52711_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttributeU5BU5D_t5548_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52711_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52711_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9471_ICollection_1_CopyTo_m52711_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52711_GenericMethod/* genericMethod */

};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
static ParameterInfo ICollection_1_t9471_ICollection_1_Remove_m52712_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttribute_t949_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52712_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52712_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9471_ICollection_1_Remove_m52712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52712_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9471_MethodInfos[] =
{
	&ICollection_1_get_Count_m52706_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52707_MethodInfo,
	&ICollection_1_Add_m52708_MethodInfo,
	&ICollection_1_Clear_m52709_MethodInfo,
	&ICollection_1_Contains_m52710_MethodInfo,
	&ICollection_1_CopyTo_m52711_MethodInfo,
	&ICollection_1_Remove_m52712_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9473_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9471_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9473_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9471_0_0_0;
extern Il2CppType ICollection_1_t9471_1_0_0;
struct ICollection_1_t9471;
extern Il2CppGenericClass ICollection_1_t9471_GenericClass;
TypeInfo ICollection_1_t9471_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9471_MethodInfos/* methods */
	, ICollection_1_t9471_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9471_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9471_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9471_0_0_0/* byval_arg */
	, &ICollection_1_t9471_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9471_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern Il2CppType IEnumerator_1_t7374_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52713_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52713_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9473_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7374_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52713_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9473_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52713_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9473_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9473_0_0_0;
extern Il2CppType IEnumerable_1_t9473_1_0_0;
struct IEnumerable_1_t9473;
extern Il2CppGenericClass IEnumerable_1_t9473_GenericClass;
TypeInfo IEnumerable_1_t9473_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9473_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9473_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9473_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9473_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9473_0_0_0/* byval_arg */
	, &IEnumerable_1_t9473_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9473_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9472_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern MethodInfo IList_1_get_Item_m52714_MethodInfo;
extern MethodInfo IList_1_set_Item_m52715_MethodInfo;
static PropertyInfo IList_1_t9472____Item_PropertyInfo = 
{
	&IList_1_t9472_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52714_MethodInfo/* get */
	, &IList_1_set_Item_m52715_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9472_PropertyInfos[] =
{
	&IList_1_t9472____Item_PropertyInfo,
	NULL
};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
static ParameterInfo IList_1_t9472_IList_1_IndexOf_m52716_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttribute_t949_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52716_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52716_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9472_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9472_IList_1_IndexOf_m52716_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52716_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
static ParameterInfo IList_1_t9472_IList_1_Insert_m52717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttribute_t949_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52717_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52717_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9472_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9472_IList_1_Insert_m52717_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52717_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9472_IList_1_RemoveAt_m52718_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52718_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52718_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9472_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9472_IList_1_RemoveAt_m52718_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52718_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9472_IList_1_get_Item_m52714_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52714_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52714_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9472_il2cpp_TypeInfo/* declaring_type */
	, &CompilationRelaxationsAttribute_t949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9472_IList_1_get_Item_m52714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52714_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CompilationRelaxationsAttribute_t949_0_0_0;
static ParameterInfo IList_1_t9472_IList_1_set_Item_m52715_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CompilationRelaxationsAttribute_t949_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52715_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52715_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9472_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9472_IList_1_set_Item_m52715_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52715_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9472_MethodInfos[] =
{
	&IList_1_IndexOf_m52716_MethodInfo,
	&IList_1_Insert_m52717_MethodInfo,
	&IList_1_RemoveAt_m52718_MethodInfo,
	&IList_1_get_Item_m52714_MethodInfo,
	&IList_1_set_Item_m52715_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9472_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9471_il2cpp_TypeInfo,
	&IEnumerable_1_t9473_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9472_0_0_0;
extern Il2CppType IList_1_t9472_1_0_0;
struct IList_1_t9472;
extern Il2CppGenericClass IList_1_t9472_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9472_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9472_MethodInfos/* methods */
	, IList_1_t9472_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9472_il2cpp_TypeInfo/* element_class */
	, IList_1_t9472_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9472_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9472_0_0_0/* byval_arg */
	, &IList_1_t9472_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9472_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7376_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52719_MethodInfo;
static PropertyInfo IEnumerator_1_t7376____Current_PropertyInfo = 
{
	&IEnumerator_1_t7376_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7376_PropertyInfos[] =
{
	&IEnumerator_1_t7376____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52719_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52719_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7376_il2cpp_TypeInfo/* declaring_type */
	, &DefaultDependencyAttribute_t2010_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52719_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7376_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52719_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7376_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7376_0_0_0;
extern Il2CppType IEnumerator_1_t7376_1_0_0;
struct IEnumerator_1_t7376;
extern Il2CppGenericClass IEnumerator_1_t7376_GenericClass;
TypeInfo IEnumerator_1_t7376_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7376_MethodInfos/* methods */
	, IEnumerator_1_t7376_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7376_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7376_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7376_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7376_0_0_0/* byval_arg */
	, &IEnumerator_1_t7376_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7376_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_704.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5250_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_704MethodDeclarations.h"

extern TypeInfo DefaultDependencyAttribute_t2010_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31626_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultDependencyAttribute_t2010_m41466_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.DefaultDependencyAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.DefaultDependencyAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultDependencyAttribute_t2010_m41466(__this, p0, method) (DefaultDependencyAttribute_t2010 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5250____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5250, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5250____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5250, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5250_FieldInfos[] =
{
	&InternalEnumerator_1_t5250____array_0_FieldInfo,
	&InternalEnumerator_1_t5250____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5250____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5250_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5250____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5250_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31626_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5250_PropertyInfos[] =
{
	&InternalEnumerator_1_t5250____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5250____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5250_InternalEnumerator_1__ctor_m31622_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31622_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31622_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5250_InternalEnumerator_1__ctor_m31622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31622_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31624_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31624_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31624_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31625_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31625_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31625_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31626_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31626_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* declaring_type */
	, &DefaultDependencyAttribute_t2010_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31626_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5250_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31622_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_MethodInfo,
	&InternalEnumerator_1_Dispose_m31624_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31625_MethodInfo,
	&InternalEnumerator_1_get_Current_m31626_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31625_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31624_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5250_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31623_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31625_MethodInfo,
	&InternalEnumerator_1_Dispose_m31624_MethodInfo,
	&InternalEnumerator_1_get_Current_m31626_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5250_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7376_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5250_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7376_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultDependencyAttribute_t2010_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5250_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31626_MethodInfo/* Method Usage */,
	&DefaultDependencyAttribute_t2010_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultDependencyAttribute_t2010_m41466_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5250_0_0_0;
extern Il2CppType InternalEnumerator_1_t5250_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5250_GenericClass;
TypeInfo InternalEnumerator_1_t5250_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5250_MethodInfos/* methods */
	, InternalEnumerator_1_t5250_PropertyInfos/* properties */
	, InternalEnumerator_1_t5250_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5250_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5250_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5250_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5250_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5250_1_0_0/* this_arg */
	, InternalEnumerator_1_t5250_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5250_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5250_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5250)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9474_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern MethodInfo ICollection_1_get_Count_m52720_MethodInfo;
static PropertyInfo ICollection_1_t9474____Count_PropertyInfo = 
{
	&ICollection_1_t9474_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52720_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52721_MethodInfo;
static PropertyInfo ICollection_1_t9474____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9474_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52721_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9474_PropertyInfos[] =
{
	&ICollection_1_t9474____Count_PropertyInfo,
	&ICollection_1_t9474____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52720_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52720_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52720_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52721_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52721_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52721_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
static ParameterInfo ICollection_1_t9474_ICollection_1_Add_m52722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttribute_t2010_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52722_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52722_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9474_ICollection_1_Add_m52722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52722_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52723_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52723_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52723_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
static ParameterInfo ICollection_1_t9474_ICollection_1_Contains_m52724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttribute_t2010_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52724_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52724_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9474_ICollection_1_Contains_m52724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52724_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultDependencyAttributeU5BU5D_t5549_0_0_0;
extern Il2CppType DefaultDependencyAttributeU5BU5D_t5549_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9474_ICollection_1_CopyTo_m52725_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttributeU5BU5D_t5549_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52725_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52725_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9474_ICollection_1_CopyTo_m52725_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52725_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
static ParameterInfo ICollection_1_t9474_ICollection_1_Remove_m52726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttribute_t2010_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52726_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52726_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9474_ICollection_1_Remove_m52726_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52726_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9474_MethodInfos[] =
{
	&ICollection_1_get_Count_m52720_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52721_MethodInfo,
	&ICollection_1_Add_m52722_MethodInfo,
	&ICollection_1_Clear_m52723_MethodInfo,
	&ICollection_1_Contains_m52724_MethodInfo,
	&ICollection_1_CopyTo_m52725_MethodInfo,
	&ICollection_1_Remove_m52726_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9476_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9474_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9476_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9474_0_0_0;
extern Il2CppType ICollection_1_t9474_1_0_0;
struct ICollection_1_t9474;
extern Il2CppGenericClass ICollection_1_t9474_GenericClass;
TypeInfo ICollection_1_t9474_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9474_MethodInfos/* methods */
	, ICollection_1_t9474_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9474_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9474_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9474_0_0_0/* byval_arg */
	, &ICollection_1_t9474_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9474_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern Il2CppType IEnumerator_1_t7376_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52727_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52727_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9476_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7376_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52727_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9476_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52727_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9476_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9476_0_0_0;
extern Il2CppType IEnumerable_1_t9476_1_0_0;
struct IEnumerable_1_t9476;
extern Il2CppGenericClass IEnumerable_1_t9476_GenericClass;
TypeInfo IEnumerable_1_t9476_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9476_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9476_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9476_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9476_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9476_0_0_0/* byval_arg */
	, &IEnumerable_1_t9476_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9476_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9475_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern MethodInfo IList_1_get_Item_m52728_MethodInfo;
extern MethodInfo IList_1_set_Item_m52729_MethodInfo;
static PropertyInfo IList_1_t9475____Item_PropertyInfo = 
{
	&IList_1_t9475_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52728_MethodInfo/* get */
	, &IList_1_set_Item_m52729_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9475_PropertyInfos[] =
{
	&IList_1_t9475____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
static ParameterInfo IList_1_t9475_IList_1_IndexOf_m52730_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttribute_t2010_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52730_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52730_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9475_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9475_IList_1_IndexOf_m52730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52730_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
static ParameterInfo IList_1_t9475_IList_1_Insert_m52731_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttribute_t2010_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52731_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52731_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9475_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9475_IList_1_Insert_m52731_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52731_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9475_IList_1_RemoveAt_m52732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52732_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52732_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9475_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9475_IList_1_RemoveAt_m52732_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52732_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9475_IList_1_get_Item_m52728_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52728_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52728_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9475_il2cpp_TypeInfo/* declaring_type */
	, &DefaultDependencyAttribute_t2010_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9475_IList_1_get_Item_m52728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52728_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultDependencyAttribute_t2010_0_0_0;
static ParameterInfo IList_1_t9475_IList_1_set_Item_m52729_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultDependencyAttribute_t2010_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52729_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52729_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9475_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9475_IList_1_set_Item_m52729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52729_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9475_MethodInfos[] =
{
	&IList_1_IndexOf_m52730_MethodInfo,
	&IList_1_Insert_m52731_MethodInfo,
	&IList_1_RemoveAt_m52732_MethodInfo,
	&IList_1_get_Item_m52728_MethodInfo,
	&IList_1_set_Item_m52729_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9475_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9474_il2cpp_TypeInfo,
	&IEnumerable_1_t9476_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9475_0_0_0;
extern Il2CppType IList_1_t9475_1_0_0;
struct IList_1_t9475;
extern Il2CppGenericClass IList_1_t9475_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9475_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9475_MethodInfos/* methods */
	, IList_1_t9475_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9475_il2cpp_TypeInfo/* element_class */
	, IList_1_t9475_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9475_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9475_0_0_0/* byval_arg */
	, &IList_1_t9475_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9475_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7378_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.LoadHint>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.LoadHint>
extern MethodInfo IEnumerator_1_get_Current_m52733_MethodInfo;
static PropertyInfo IEnumerator_1_t7378____Current_PropertyInfo = 
{
	&IEnumerator_1_t7378_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7378_PropertyInfos[] =
{
	&IEnumerator_1_t7378____Current_PropertyInfo,
	NULL
};
extern Il2CppType LoadHint_t2012_0_0_0;
extern void* RuntimeInvoker_LoadHint_t2012 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52733_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.LoadHint>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52733_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7378_il2cpp_TypeInfo/* declaring_type */
	, &LoadHint_t2012_0_0_0/* return_type */
	, RuntimeInvoker_LoadHint_t2012/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52733_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7378_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52733_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7378_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7378_0_0_0;
extern Il2CppType IEnumerator_1_t7378_1_0_0;
struct IEnumerator_1_t7378;
extern Il2CppGenericClass IEnumerator_1_t7378_GenericClass;
TypeInfo IEnumerator_1_t7378_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7378_MethodInfos/* methods */
	, IEnumerator_1_t7378_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7378_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7378_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7378_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7378_0_0_0/* byval_arg */
	, &IEnumerator_1_t7378_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7378_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_705.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5251_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_705MethodDeclarations.h"

extern TypeInfo LoadHint_t2012_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31631_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLoadHint_t2012_m41477_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.LoadHint>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.LoadHint>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisLoadHint_t2012_m41477 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31627_MethodInfo;
 void InternalEnumerator_1__ctor_m31627 (InternalEnumerator_1_t5251 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628 (InternalEnumerator_1_t5251 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31631(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31631_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&LoadHint_t2012_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31629_MethodInfo;
 void InternalEnumerator_1_Dispose_m31629 (InternalEnumerator_1_t5251 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31630_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31630 (InternalEnumerator_1_t5251 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31631 (InternalEnumerator_1_t5251 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisLoadHint_t2012_m41477(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisLoadHint_t2012_m41477_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5251____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5251, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5251____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5251, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5251_FieldInfos[] =
{
	&InternalEnumerator_1_t5251____array_0_FieldInfo,
	&InternalEnumerator_1_t5251____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5251____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5251_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5251____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5251_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31631_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5251_PropertyInfos[] =
{
	&InternalEnumerator_1_t5251____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5251____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5251_InternalEnumerator_1__ctor_m31627_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31627_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31627_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31627/* method */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5251_InternalEnumerator_1__ctor_m31627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31627_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628/* method */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31629_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31629_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31629/* method */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31629_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31630_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31630_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31630/* method */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31630_GenericMethod/* genericMethod */

};
extern Il2CppType LoadHint_t2012_0_0_0;
extern void* RuntimeInvoker_LoadHint_t2012 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31631_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31631_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31631/* method */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* declaring_type */
	, &LoadHint_t2012_0_0_0/* return_type */
	, RuntimeInvoker_LoadHint_t2012/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31631_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5251_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31627_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_MethodInfo,
	&InternalEnumerator_1_Dispose_m31629_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31630_MethodInfo,
	&InternalEnumerator_1_get_Current_m31631_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5251_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31630_MethodInfo,
	&InternalEnumerator_1_Dispose_m31629_MethodInfo,
	&InternalEnumerator_1_get_Current_m31631_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5251_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7378_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5251_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7378_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5251_0_0_0;
extern Il2CppType InternalEnumerator_1_t5251_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5251_GenericClass;
TypeInfo InternalEnumerator_1_t5251_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5251_MethodInfos/* methods */
	, InternalEnumerator_1_t5251_PropertyInfos/* properties */
	, InternalEnumerator_1_t5251_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5251_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5251_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5251_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5251_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5251_1_0_0/* this_arg */
	, InternalEnumerator_1_t5251_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5251_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5251)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9477_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>
extern MethodInfo ICollection_1_get_Count_m52734_MethodInfo;
static PropertyInfo ICollection_1_t9477____Count_PropertyInfo = 
{
	&ICollection_1_t9477_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52734_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52735_MethodInfo;
static PropertyInfo ICollection_1_t9477____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9477_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52735_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9477_PropertyInfos[] =
{
	&ICollection_1_t9477____Count_PropertyInfo,
	&ICollection_1_t9477____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52734_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::get_Count()
MethodInfo ICollection_1_get_Count_m52734_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52734_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52735_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52735_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52735_GenericMethod/* genericMethod */

};
extern Il2CppType LoadHint_t2012_0_0_0;
extern Il2CppType LoadHint_t2012_0_0_0;
static ParameterInfo ICollection_1_t9477_ICollection_1_Add_m52736_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoadHint_t2012_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52736_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Add(T)
MethodInfo ICollection_1_Add_m52736_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9477_ICollection_1_Add_m52736_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52736_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52737_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Clear()
MethodInfo ICollection_1_Clear_m52737_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52737_GenericMethod/* genericMethod */

};
extern Il2CppType LoadHint_t2012_0_0_0;
static ParameterInfo ICollection_1_t9477_ICollection_1_Contains_m52738_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoadHint_t2012_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52738_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Contains(T)
MethodInfo ICollection_1_Contains_m52738_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9477_ICollection_1_Contains_m52738_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52738_GenericMethod/* genericMethod */

};
extern Il2CppType LoadHintU5BU5D_t5550_0_0_0;
extern Il2CppType LoadHintU5BU5D_t5550_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9477_ICollection_1_CopyTo_m52739_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &LoadHintU5BU5D_t5550_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52739_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52739_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9477_ICollection_1_CopyTo_m52739_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52739_GenericMethod/* genericMethod */

};
extern Il2CppType LoadHint_t2012_0_0_0;
static ParameterInfo ICollection_1_t9477_ICollection_1_Remove_m52740_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoadHint_t2012_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52740_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>::Remove(T)
MethodInfo ICollection_1_Remove_m52740_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9477_ICollection_1_Remove_m52740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52740_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9477_MethodInfos[] =
{
	&ICollection_1_get_Count_m52734_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52735_MethodInfo,
	&ICollection_1_Add_m52736_MethodInfo,
	&ICollection_1_Clear_m52737_MethodInfo,
	&ICollection_1_Contains_m52738_MethodInfo,
	&ICollection_1_CopyTo_m52739_MethodInfo,
	&ICollection_1_Remove_m52740_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9479_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9477_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9479_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9477_0_0_0;
extern Il2CppType ICollection_1_t9477_1_0_0;
struct ICollection_1_t9477;
extern Il2CppGenericClass ICollection_1_t9477_GenericClass;
TypeInfo ICollection_1_t9477_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9477_MethodInfos/* methods */
	, ICollection_1_t9477_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9477_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9477_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9477_0_0_0/* byval_arg */
	, &ICollection_1_t9477_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9477_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.LoadHint>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.LoadHint>
extern Il2CppType IEnumerator_1_t7378_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52741_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.LoadHint>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52741_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9479_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7378_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52741_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9479_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52741_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9479_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9479_0_0_0;
extern Il2CppType IEnumerable_1_t9479_1_0_0;
struct IEnumerable_1_t9479;
extern Il2CppGenericClass IEnumerable_1_t9479_GenericClass;
TypeInfo IEnumerable_1_t9479_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9479_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9479_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9479_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9479_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9479_0_0_0/* byval_arg */
	, &IEnumerable_1_t9479_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9479_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9478_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>
extern MethodInfo IList_1_get_Item_m52742_MethodInfo;
extern MethodInfo IList_1_set_Item_m52743_MethodInfo;
static PropertyInfo IList_1_t9478____Item_PropertyInfo = 
{
	&IList_1_t9478_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52742_MethodInfo/* get */
	, &IList_1_set_Item_m52743_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9478_PropertyInfos[] =
{
	&IList_1_t9478____Item_PropertyInfo,
	NULL
};
extern Il2CppType LoadHint_t2012_0_0_0;
static ParameterInfo IList_1_t9478_IList_1_IndexOf_m52744_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoadHint_t2012_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52744_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52744_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9478_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9478_IList_1_IndexOf_m52744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52744_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoadHint_t2012_0_0_0;
static ParameterInfo IList_1_t9478_IList_1_Insert_m52745_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &LoadHint_t2012_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52745_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52745_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9478_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9478_IList_1_Insert_m52745_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52745_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9478_IList_1_RemoveAt_m52746_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52746_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52746_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9478_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9478_IList_1_RemoveAt_m52746_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52746_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9478_IList_1_get_Item_m52742_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType LoadHint_t2012_0_0_0;
extern void* RuntimeInvoker_LoadHint_t2012_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52742_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52742_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9478_il2cpp_TypeInfo/* declaring_type */
	, &LoadHint_t2012_0_0_0/* return_type */
	, RuntimeInvoker_LoadHint_t2012_Int32_t123/* invoker_method */
	, IList_1_t9478_IList_1_get_Item_m52742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52742_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoadHint_t2012_0_0_0;
static ParameterInfo IList_1_t9478_IList_1_set_Item_m52743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &LoadHint_t2012_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52743_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52743_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9478_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9478_IList_1_set_Item_m52743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52743_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9478_MethodInfos[] =
{
	&IList_1_IndexOf_m52744_MethodInfo,
	&IList_1_Insert_m52745_MethodInfo,
	&IList_1_RemoveAt_m52746_MethodInfo,
	&IList_1_get_Item_m52742_MethodInfo,
	&IList_1_set_Item_m52743_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9478_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9477_il2cpp_TypeInfo,
	&IEnumerable_1_t9479_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9478_0_0_0;
extern Il2CppType IList_1_t9478_1_0_0;
struct IList_1_t9478;
extern Il2CppGenericClass IList_1_t9478_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9478_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9478_MethodInfos/* methods */
	, IList_1_t9478_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9478_il2cpp_TypeInfo/* element_class */
	, IList_1_t9478_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9478_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9478_0_0_0/* byval_arg */
	, &IList_1_t9478_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9478_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7380_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52747_MethodInfo;
static PropertyInfo IEnumerator_1_t7380____Current_PropertyInfo = 
{
	&IEnumerator_1_t7380_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52747_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7380_PropertyInfos[] =
{
	&IEnumerator_1_t7380____Current_PropertyInfo,
	NULL
};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52747_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52747_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &StringFreezingAttribute_t2013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52747_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7380_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52747_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7380_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7380_0_0_0;
extern Il2CppType IEnumerator_1_t7380_1_0_0;
struct IEnumerator_1_t7380;
extern Il2CppGenericClass IEnumerator_1_t7380_GenericClass;
TypeInfo IEnumerator_1_t7380_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7380_MethodInfos/* methods */
	, IEnumerator_1_t7380_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7380_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7380_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7380_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7380_0_0_0/* byval_arg */
	, &IEnumerator_1_t7380_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7380_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_706.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5252_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_706MethodDeclarations.h"

extern TypeInfo StringFreezingAttribute_t2013_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31636_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisStringFreezingAttribute_t2013_m41488_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.StringFreezingAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.StringFreezingAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisStringFreezingAttribute_t2013_m41488(__this, p0, method) (StringFreezingAttribute_t2013 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5252____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5252, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5252____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5252, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5252_FieldInfos[] =
{
	&InternalEnumerator_1_t5252____array_0_FieldInfo,
	&InternalEnumerator_1_t5252____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5252____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5252_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5252____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5252_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5252_PropertyInfos[] =
{
	&InternalEnumerator_1_t5252____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5252____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5252_InternalEnumerator_1__ctor_m31632_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31632_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31632_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5252_InternalEnumerator_1__ctor_m31632_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31632_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31634_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31634_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31634_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31635_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31635_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31635_GenericMethod/* genericMethod */

};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31636_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31636_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* declaring_type */
	, &StringFreezingAttribute_t2013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31636_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5252_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31632_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_MethodInfo,
	&InternalEnumerator_1_Dispose_m31634_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31635_MethodInfo,
	&InternalEnumerator_1_get_Current_m31636_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31635_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31634_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5252_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31633_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31635_MethodInfo,
	&InternalEnumerator_1_Dispose_m31634_MethodInfo,
	&InternalEnumerator_1_get_Current_m31636_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5252_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7380_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5252_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7380_il2cpp_TypeInfo, 7},
};
extern TypeInfo StringFreezingAttribute_t2013_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5252_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31636_MethodInfo/* Method Usage */,
	&StringFreezingAttribute_t2013_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisStringFreezingAttribute_t2013_m41488_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5252_0_0_0;
extern Il2CppType InternalEnumerator_1_t5252_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5252_GenericClass;
TypeInfo InternalEnumerator_1_t5252_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5252_MethodInfos/* methods */
	, InternalEnumerator_1_t5252_PropertyInfos/* properties */
	, InternalEnumerator_1_t5252_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5252_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5252_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5252_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5252_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5252_1_0_0/* this_arg */
	, InternalEnumerator_1_t5252_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5252_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5252_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5252)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9480_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern MethodInfo ICollection_1_get_Count_m52748_MethodInfo;
static PropertyInfo ICollection_1_t9480____Count_PropertyInfo = 
{
	&ICollection_1_t9480_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52748_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52749_MethodInfo;
static PropertyInfo ICollection_1_t9480____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9480_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52749_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9480_PropertyInfos[] =
{
	&ICollection_1_t9480____Count_PropertyInfo,
	&ICollection_1_t9480____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52748_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52748_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52748_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52749_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52749_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52749_GenericMethod/* genericMethod */

};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
static ParameterInfo ICollection_1_t9480_ICollection_1_Add_m52750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttribute_t2013_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52750_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52750_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9480_ICollection_1_Add_m52750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52750_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52751_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52751_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52751_GenericMethod/* genericMethod */

};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
static ParameterInfo ICollection_1_t9480_ICollection_1_Contains_m52752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttribute_t2013_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52752_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52752_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9480_ICollection_1_Contains_m52752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52752_GenericMethod/* genericMethod */

};
extern Il2CppType StringFreezingAttributeU5BU5D_t5551_0_0_0;
extern Il2CppType StringFreezingAttributeU5BU5D_t5551_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9480_ICollection_1_CopyTo_m52753_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttributeU5BU5D_t5551_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52753_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52753_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9480_ICollection_1_CopyTo_m52753_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52753_GenericMethod/* genericMethod */

};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
static ParameterInfo ICollection_1_t9480_ICollection_1_Remove_m52754_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttribute_t2013_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52754_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52754_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9480_ICollection_1_Remove_m52754_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52754_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9480_MethodInfos[] =
{
	&ICollection_1_get_Count_m52748_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52749_MethodInfo,
	&ICollection_1_Add_m52750_MethodInfo,
	&ICollection_1_Clear_m52751_MethodInfo,
	&ICollection_1_Contains_m52752_MethodInfo,
	&ICollection_1_CopyTo_m52753_MethodInfo,
	&ICollection_1_Remove_m52754_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9482_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9480_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9482_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9480_0_0_0;
extern Il2CppType ICollection_1_t9480_1_0_0;
struct ICollection_1_t9480;
extern Il2CppGenericClass ICollection_1_t9480_GenericClass;
TypeInfo ICollection_1_t9480_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9480_MethodInfos/* methods */
	, ICollection_1_t9480_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9480_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9480_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9480_0_0_0/* byval_arg */
	, &ICollection_1_t9480_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9480_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.StringFreezingAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern Il2CppType IEnumerator_1_t7380_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52755_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.StringFreezingAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52755_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9482_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7380_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52755_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9482_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52755_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9482_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9482_0_0_0;
extern Il2CppType IEnumerable_1_t9482_1_0_0;
struct IEnumerable_1_t9482;
extern Il2CppGenericClass IEnumerable_1_t9482_GenericClass;
TypeInfo IEnumerable_1_t9482_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9482_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9482_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9482_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9482_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9482_0_0_0/* byval_arg */
	, &IEnumerable_1_t9482_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9482_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9481_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern MethodInfo IList_1_get_Item_m52756_MethodInfo;
extern MethodInfo IList_1_set_Item_m52757_MethodInfo;
static PropertyInfo IList_1_t9481____Item_PropertyInfo = 
{
	&IList_1_t9481_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52756_MethodInfo/* get */
	, &IList_1_set_Item_m52757_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9481_PropertyInfos[] =
{
	&IList_1_t9481____Item_PropertyInfo,
	NULL
};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
static ParameterInfo IList_1_t9481_IList_1_IndexOf_m52758_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttribute_t2013_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52758_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52758_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9481_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9481_IList_1_IndexOf_m52758_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52758_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
static ParameterInfo IList_1_t9481_IList_1_Insert_m52759_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttribute_t2013_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52759_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52759_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9481_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9481_IList_1_Insert_m52759_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52759_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9481_IList_1_RemoveAt_m52760_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52760_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52760_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9481_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9481_IList_1_RemoveAt_m52760_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52760_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9481_IList_1_get_Item_m52756_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52756_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52756_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9481_il2cpp_TypeInfo/* declaring_type */
	, &StringFreezingAttribute_t2013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9481_IList_1_get_Item_m52756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52756_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringFreezingAttribute_t2013_0_0_0;
static ParameterInfo IList_1_t9481_IList_1_set_Item_m52757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringFreezingAttribute_t2013_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52757_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52757_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9481_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9481_IList_1_set_Item_m52757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52757_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9481_MethodInfos[] =
{
	&IList_1_IndexOf_m52758_MethodInfo,
	&IList_1_Insert_m52759_MethodInfo,
	&IList_1_RemoveAt_m52760_MethodInfo,
	&IList_1_get_Item_m52756_MethodInfo,
	&IList_1_set_Item_m52757_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9481_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9480_il2cpp_TypeInfo,
	&IEnumerable_1_t9482_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9481_0_0_0;
extern Il2CppType IList_1_t9481_1_0_0;
struct IList_1_t9481;
extern Il2CppGenericClass IList_1_t9481_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9481_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9481_MethodInfos/* methods */
	, IList_1_t9481_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9481_il2cpp_TypeInfo/* element_class */
	, IList_1_t9481_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9481_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9481_0_0_0/* byval_arg */
	, &IList_1_t9481_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9481_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7382_il2cpp_TypeInfo;

// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
extern MethodInfo IEnumerator_1_get_Current_m52761_MethodInfo;
static PropertyInfo IEnumerator_1_t7382____Current_PropertyInfo = 
{
	&IEnumerator_1_t7382_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7382_PropertyInfos[] =
{
	&IEnumerator_1_t7382____Current_PropertyInfo,
	NULL
};
extern Il2CppType Cer_t2014_0_0_0;
extern void* RuntimeInvoker_Cer_t2014 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52761_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52761_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7382_il2cpp_TypeInfo/* declaring_type */
	, &Cer_t2014_0_0_0/* return_type */
	, RuntimeInvoker_Cer_t2014/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52761_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7382_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52761_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7382_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7382_0_0_0;
extern Il2CppType IEnumerator_1_t7382_1_0_0;
struct IEnumerator_1_t7382;
extern Il2CppGenericClass IEnumerator_1_t7382_GenericClass;
TypeInfo IEnumerator_1_t7382_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7382_MethodInfos/* methods */
	, IEnumerator_1_t7382_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7382_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7382_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7382_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7382_0_0_0/* byval_arg */
	, &IEnumerator_1_t7382_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7382_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_707.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5253_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_707MethodDeclarations.h"

extern TypeInfo Cer_t2014_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31641_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCer_t2014_m41499_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.ConstrainedExecution.Cer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.ConstrainedExecution.Cer>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCer_t2014_m41499 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31637_MethodInfo;
 void InternalEnumerator_1__ctor_m31637 (InternalEnumerator_1_t5253 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638 (InternalEnumerator_1_t5253 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31641(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31641_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Cer_t2014_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31639_MethodInfo;
 void InternalEnumerator_1_Dispose_m31639 (InternalEnumerator_1_t5253 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31640_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31640 (InternalEnumerator_1_t5253 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31641 (InternalEnumerator_1_t5253 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCer_t2014_m41499(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCer_t2014_m41499_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5253____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5253, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5253____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5253, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5253_FieldInfos[] =
{
	&InternalEnumerator_1_t5253____array_0_FieldInfo,
	&InternalEnumerator_1_t5253____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5253____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5253_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5253____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5253_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31641_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5253_PropertyInfos[] =
{
	&InternalEnumerator_1_t5253____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5253____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5253_InternalEnumerator_1__ctor_m31637_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31637_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31637_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31637/* method */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5253_InternalEnumerator_1__ctor_m31637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31637_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638/* method */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31639_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31639_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31639/* method */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31639_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31640_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31640_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31640/* method */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31640_GenericMethod/* genericMethod */

};
extern Il2CppType Cer_t2014_0_0_0;
extern void* RuntimeInvoker_Cer_t2014 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31641_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31641_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31641/* method */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* declaring_type */
	, &Cer_t2014_0_0_0/* return_type */
	, RuntimeInvoker_Cer_t2014/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31641_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5253_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31637_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_MethodInfo,
	&InternalEnumerator_1_Dispose_m31639_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31640_MethodInfo,
	&InternalEnumerator_1_get_Current_m31641_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5253_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31638_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31640_MethodInfo,
	&InternalEnumerator_1_Dispose_m31639_MethodInfo,
	&InternalEnumerator_1_get_Current_m31641_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5253_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7382_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5253_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7382_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5253_0_0_0;
extern Il2CppType InternalEnumerator_1_t5253_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5253_GenericClass;
TypeInfo InternalEnumerator_1_t5253_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5253_MethodInfos/* methods */
	, InternalEnumerator_1_t5253_PropertyInfos/* properties */
	, InternalEnumerator_1_t5253_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5253_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5253_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5253_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5253_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5253_1_0_0/* this_arg */
	, InternalEnumerator_1_t5253_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5253_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5253)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9483_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>
extern MethodInfo ICollection_1_get_Count_m52762_MethodInfo;
static PropertyInfo ICollection_1_t9483____Count_PropertyInfo = 
{
	&ICollection_1_t9483_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52762_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52763_MethodInfo;
static PropertyInfo ICollection_1_t9483____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9483_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9483_PropertyInfos[] =
{
	&ICollection_1_t9483____Count_PropertyInfo,
	&ICollection_1_t9483____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52762_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::get_Count()
MethodInfo ICollection_1_get_Count_m52762_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52762_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52763_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52763_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52763_GenericMethod/* genericMethod */

};
extern Il2CppType Cer_t2014_0_0_0;
extern Il2CppType Cer_t2014_0_0_0;
static ParameterInfo ICollection_1_t9483_ICollection_1_Add_m52764_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Cer_t2014_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52764_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Add(T)
MethodInfo ICollection_1_Add_m52764_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9483_ICollection_1_Add_m52764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52764_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52765_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Clear()
MethodInfo ICollection_1_Clear_m52765_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52765_GenericMethod/* genericMethod */

};
extern Il2CppType Cer_t2014_0_0_0;
static ParameterInfo ICollection_1_t9483_ICollection_1_Contains_m52766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Cer_t2014_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52766_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Contains(T)
MethodInfo ICollection_1_Contains_m52766_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9483_ICollection_1_Contains_m52766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52766_GenericMethod/* genericMethod */

};
extern Il2CppType CerU5BU5D_t5552_0_0_0;
extern Il2CppType CerU5BU5D_t5552_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9483_ICollection_1_CopyTo_m52767_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CerU5BU5D_t5552_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52767_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52767_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9483_ICollection_1_CopyTo_m52767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52767_GenericMethod/* genericMethod */

};
extern Il2CppType Cer_t2014_0_0_0;
static ParameterInfo ICollection_1_t9483_ICollection_1_Remove_m52768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Cer_t2014_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52768_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>::Remove(T)
MethodInfo ICollection_1_Remove_m52768_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9483_ICollection_1_Remove_m52768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52768_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9483_MethodInfos[] =
{
	&ICollection_1_get_Count_m52762_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52763_MethodInfo,
	&ICollection_1_Add_m52764_MethodInfo,
	&ICollection_1_Clear_m52765_MethodInfo,
	&ICollection_1_Contains_m52766_MethodInfo,
	&ICollection_1_CopyTo_m52767_MethodInfo,
	&ICollection_1_Remove_m52768_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9485_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9483_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9485_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9483_0_0_0;
extern Il2CppType ICollection_1_t9483_1_0_0;
struct ICollection_1_t9483;
extern Il2CppGenericClass ICollection_1_t9483_GenericClass;
TypeInfo ICollection_1_t9483_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9483_MethodInfos/* methods */
	, ICollection_1_t9483_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9483_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9483_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9483_0_0_0/* byval_arg */
	, &ICollection_1_t9483_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9483_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Cer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Cer>
extern Il2CppType IEnumerator_1_t7382_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52769_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Cer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52769_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9485_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7382_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52769_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9485_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52769_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9485_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9485_0_0_0;
extern Il2CppType IEnumerable_1_t9485_1_0_0;
struct IEnumerable_1_t9485;
extern Il2CppGenericClass IEnumerable_1_t9485_GenericClass;
TypeInfo IEnumerable_1_t9485_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9485_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9485_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9485_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9485_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9485_0_0_0/* byval_arg */
	, &IEnumerable_1_t9485_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9485_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9484_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>
extern MethodInfo IList_1_get_Item_m52770_MethodInfo;
extern MethodInfo IList_1_set_Item_m52771_MethodInfo;
static PropertyInfo IList_1_t9484____Item_PropertyInfo = 
{
	&IList_1_t9484_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52770_MethodInfo/* get */
	, &IList_1_set_Item_m52771_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9484_PropertyInfos[] =
{
	&IList_1_t9484____Item_PropertyInfo,
	NULL
};
extern Il2CppType Cer_t2014_0_0_0;
static ParameterInfo IList_1_t9484_IList_1_IndexOf_m52772_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Cer_t2014_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52772_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52772_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9484_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9484_IList_1_IndexOf_m52772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52772_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Cer_t2014_0_0_0;
static ParameterInfo IList_1_t9484_IList_1_Insert_m52773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Cer_t2014_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52773_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52773_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9484_IList_1_Insert_m52773_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52773_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9484_IList_1_RemoveAt_m52774_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52774_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52774_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9484_IList_1_RemoveAt_m52774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52774_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9484_IList_1_get_Item_m52770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Cer_t2014_0_0_0;
extern void* RuntimeInvoker_Cer_t2014_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52770_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52770_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9484_il2cpp_TypeInfo/* declaring_type */
	, &Cer_t2014_0_0_0/* return_type */
	, RuntimeInvoker_Cer_t2014_Int32_t123/* invoker_method */
	, IList_1_t9484_IList_1_get_Item_m52770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52770_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Cer_t2014_0_0_0;
static ParameterInfo IList_1_t9484_IList_1_set_Item_m52771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Cer_t2014_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52771_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52771_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9484_IList_1_set_Item_m52771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52771_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9484_MethodInfos[] =
{
	&IList_1_IndexOf_m52772_MethodInfo,
	&IList_1_Insert_m52773_MethodInfo,
	&IList_1_RemoveAt_m52774_MethodInfo,
	&IList_1_get_Item_m52770_MethodInfo,
	&IList_1_set_Item_m52771_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9484_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9483_il2cpp_TypeInfo,
	&IEnumerable_1_t9485_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9484_0_0_0;
extern Il2CppType IList_1_t9484_1_0_0;
struct IList_1_t9484;
extern Il2CppGenericClass IList_1_t9484_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9484_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9484_MethodInfos/* methods */
	, IList_1_t9484_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9484_il2cpp_TypeInfo/* element_class */
	, IList_1_t9484_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9484_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9484_0_0_0/* byval_arg */
	, &IList_1_t9484_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9484_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7384_il2cpp_TypeInfo;

// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>
extern MethodInfo IEnumerator_1_get_Current_m52775_MethodInfo;
static PropertyInfo IEnumerator_1_t7384____Current_PropertyInfo = 
{
	&IEnumerator_1_t7384_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7384_PropertyInfos[] =
{
	&IEnumerator_1_t7384____Current_PropertyInfo,
	NULL
};
extern Il2CppType Consistency_t2015_0_0_0;
extern void* RuntimeInvoker_Consistency_t2015 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52775_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52775_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7384_il2cpp_TypeInfo/* declaring_type */
	, &Consistency_t2015_0_0_0/* return_type */
	, RuntimeInvoker_Consistency_t2015/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52775_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7384_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52775_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7384_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7384_0_0_0;
extern Il2CppType IEnumerator_1_t7384_1_0_0;
struct IEnumerator_1_t7384;
extern Il2CppGenericClass IEnumerator_1_t7384_GenericClass;
TypeInfo IEnumerator_1_t7384_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7384_MethodInfos/* methods */
	, IEnumerator_1_t7384_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7384_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7384_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7384_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7384_0_0_0/* byval_arg */
	, &IEnumerator_1_t7384_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7384_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_708.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5254_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_708MethodDeclarations.h"

extern TypeInfo Consistency_t2015_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31646_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisConsistency_t2015_m41510_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.ConstrainedExecution.Consistency>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.ConstrainedExecution.Consistency>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisConsistency_t2015_m41510 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31642_MethodInfo;
 void InternalEnumerator_1__ctor_m31642 (InternalEnumerator_1_t5254 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643 (InternalEnumerator_1_t5254 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31646(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31646_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Consistency_t2015_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31644_MethodInfo;
 void InternalEnumerator_1_Dispose_m31644 (InternalEnumerator_1_t5254 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31645_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31645 (InternalEnumerator_1_t5254 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31646 (InternalEnumerator_1_t5254 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisConsistency_t2015_m41510(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisConsistency_t2015_m41510_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5254____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5254, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5254____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5254, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5254_FieldInfos[] =
{
	&InternalEnumerator_1_t5254____array_0_FieldInfo,
	&InternalEnumerator_1_t5254____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5254____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5254_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5254____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5254_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5254_PropertyInfos[] =
{
	&InternalEnumerator_1_t5254____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5254____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5254_InternalEnumerator_1__ctor_m31642_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31642_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31642_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31642/* method */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5254_InternalEnumerator_1__ctor_m31642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31642_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643/* method */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31644_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31644_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31644/* method */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31644_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31645_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31645_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31645/* method */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31645_GenericMethod/* genericMethod */

};
extern Il2CppType Consistency_t2015_0_0_0;
extern void* RuntimeInvoker_Consistency_t2015 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31646_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31646_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31646/* method */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* declaring_type */
	, &Consistency_t2015_0_0_0/* return_type */
	, RuntimeInvoker_Consistency_t2015/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31646_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5254_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31642_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_MethodInfo,
	&InternalEnumerator_1_Dispose_m31644_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31645_MethodInfo,
	&InternalEnumerator_1_get_Current_m31646_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5254_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31643_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31645_MethodInfo,
	&InternalEnumerator_1_Dispose_m31644_MethodInfo,
	&InternalEnumerator_1_get_Current_m31646_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5254_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7384_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5254_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7384_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5254_0_0_0;
extern Il2CppType InternalEnumerator_1_t5254_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5254_GenericClass;
TypeInfo InternalEnumerator_1_t5254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5254_MethodInfos/* methods */
	, InternalEnumerator_1_t5254_PropertyInfos/* properties */
	, InternalEnumerator_1_t5254_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5254_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5254_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5254_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5254_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5254_1_0_0/* this_arg */
	, InternalEnumerator_1_t5254_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5254_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5254)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9486_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>
extern MethodInfo ICollection_1_get_Count_m52776_MethodInfo;
static PropertyInfo ICollection_1_t9486____Count_PropertyInfo = 
{
	&ICollection_1_t9486_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52777_MethodInfo;
static PropertyInfo ICollection_1_t9486____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9486_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52777_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9486_PropertyInfos[] =
{
	&ICollection_1_t9486____Count_PropertyInfo,
	&ICollection_1_t9486____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52776_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::get_Count()
MethodInfo ICollection_1_get_Count_m52776_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52776_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52777_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52777_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52777_GenericMethod/* genericMethod */

};
extern Il2CppType Consistency_t2015_0_0_0;
extern Il2CppType Consistency_t2015_0_0_0;
static ParameterInfo ICollection_1_t9486_ICollection_1_Add_m52778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Consistency_t2015_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52778_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Add(T)
MethodInfo ICollection_1_Add_m52778_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9486_ICollection_1_Add_m52778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52778_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52779_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Clear()
MethodInfo ICollection_1_Clear_m52779_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52779_GenericMethod/* genericMethod */

};
extern Il2CppType Consistency_t2015_0_0_0;
static ParameterInfo ICollection_1_t9486_ICollection_1_Contains_m52780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Consistency_t2015_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52780_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Contains(T)
MethodInfo ICollection_1_Contains_m52780_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9486_ICollection_1_Contains_m52780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52780_GenericMethod/* genericMethod */

};
extern Il2CppType ConsistencyU5BU5D_t5553_0_0_0;
extern Il2CppType ConsistencyU5BU5D_t5553_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9486_ICollection_1_CopyTo_m52781_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ConsistencyU5BU5D_t5553_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52781_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52781_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9486_ICollection_1_CopyTo_m52781_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52781_GenericMethod/* genericMethod */

};
extern Il2CppType Consistency_t2015_0_0_0;
static ParameterInfo ICollection_1_t9486_ICollection_1_Remove_m52782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Consistency_t2015_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52782_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>::Remove(T)
MethodInfo ICollection_1_Remove_m52782_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9486_ICollection_1_Remove_m52782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52782_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9486_MethodInfos[] =
{
	&ICollection_1_get_Count_m52776_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52777_MethodInfo,
	&ICollection_1_Add_m52778_MethodInfo,
	&ICollection_1_Clear_m52779_MethodInfo,
	&ICollection_1_Contains_m52780_MethodInfo,
	&ICollection_1_CopyTo_m52781_MethodInfo,
	&ICollection_1_Remove_m52782_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9488_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9486_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9488_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9486_0_0_0;
extern Il2CppType ICollection_1_t9486_1_0_0;
struct ICollection_1_t9486;
extern Il2CppGenericClass ICollection_1_t9486_GenericClass;
TypeInfo ICollection_1_t9486_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9486_MethodInfos/* methods */
	, ICollection_1_t9486_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9486_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9486_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9486_0_0_0/* byval_arg */
	, &ICollection_1_t9486_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9486_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Consistency>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Consistency>
extern Il2CppType IEnumerator_1_t7384_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52783_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Consistency>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52783_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9488_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7384_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52783_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9488_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52783_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9488_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9488_0_0_0;
extern Il2CppType IEnumerable_1_t9488_1_0_0;
struct IEnumerable_1_t9488;
extern Il2CppGenericClass IEnumerable_1_t9488_GenericClass;
TypeInfo IEnumerable_1_t9488_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9488_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9488_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9488_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9488_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9488_0_0_0/* byval_arg */
	, &IEnumerable_1_t9488_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9488_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9487_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>
extern MethodInfo IList_1_get_Item_m52784_MethodInfo;
extern MethodInfo IList_1_set_Item_m52785_MethodInfo;
static PropertyInfo IList_1_t9487____Item_PropertyInfo = 
{
	&IList_1_t9487_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52784_MethodInfo/* get */
	, &IList_1_set_Item_m52785_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9487_PropertyInfos[] =
{
	&IList_1_t9487____Item_PropertyInfo,
	NULL
};
extern Il2CppType Consistency_t2015_0_0_0;
static ParameterInfo IList_1_t9487_IList_1_IndexOf_m52786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Consistency_t2015_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52786_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52786_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9487_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9487_IList_1_IndexOf_m52786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52786_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Consistency_t2015_0_0_0;
static ParameterInfo IList_1_t9487_IList_1_Insert_m52787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Consistency_t2015_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52787_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52787_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9487_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9487_IList_1_Insert_m52787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52787_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9487_IList_1_RemoveAt_m52788_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52788_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52788_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9487_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9487_IList_1_RemoveAt_m52788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52788_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9487_IList_1_get_Item_m52784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Consistency_t2015_0_0_0;
extern void* RuntimeInvoker_Consistency_t2015_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52784_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52784_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9487_il2cpp_TypeInfo/* declaring_type */
	, &Consistency_t2015_0_0_0/* return_type */
	, RuntimeInvoker_Consistency_t2015_Int32_t123/* invoker_method */
	, IList_1_t9487_IList_1_get_Item_m52784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Consistency_t2015_0_0_0;
static ParameterInfo IList_1_t9487_IList_1_set_Item_m52785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Consistency_t2015_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52785_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52785_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9487_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9487_IList_1_set_Item_m52785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52785_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9487_MethodInfos[] =
{
	&IList_1_IndexOf_m52786_MethodInfo,
	&IList_1_Insert_m52787_MethodInfo,
	&IList_1_RemoveAt_m52788_MethodInfo,
	&IList_1_get_Item_m52784_MethodInfo,
	&IList_1_set_Item_m52785_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9487_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9486_il2cpp_TypeInfo,
	&IEnumerable_1_t9488_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9487_0_0_0;
extern Il2CppType IList_1_t9487_1_0_0;
struct IList_1_t9487;
extern Il2CppGenericClass IList_1_t9487_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9487_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9487_MethodInfos/* methods */
	, IList_1_t9487_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9487_il2cpp_TypeInfo/* element_class */
	, IList_1_t9487_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9487_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9487_0_0_0/* byval_arg */
	, &IList_1_t9487_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9487_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7386_il2cpp_TypeInfo;

// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52789_MethodInfo;
static PropertyInfo IEnumerator_1_t7386____Current_PropertyInfo = 
{
	&IEnumerator_1_t7386_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7386_PropertyInfos[] =
{
	&IEnumerator_1_t7386____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52789_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52789_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7386_il2cpp_TypeInfo/* declaring_type */
	, &ReliabilityContractAttribute_t2017_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52789_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7386_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52789_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7386_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7386_0_0_0;
extern Il2CppType IEnumerator_1_t7386_1_0_0;
struct IEnumerator_1_t7386;
extern Il2CppGenericClass IEnumerator_1_t7386_GenericClass;
TypeInfo IEnumerator_1_t7386_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7386_MethodInfos/* methods */
	, IEnumerator_1_t7386_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7386_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7386_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7386_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7386_0_0_0/* byval_arg */
	, &IEnumerator_1_t7386_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7386_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_709.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5255_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_709MethodDeclarations.h"

extern TypeInfo ReliabilityContractAttribute_t2017_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31651_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReliabilityContractAttribute_t2017_m41521_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisReliabilityContractAttribute_t2017_m41521(__this, p0, method) (ReliabilityContractAttribute_t2017 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5255____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5255, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5255____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5255, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5255_FieldInfos[] =
{
	&InternalEnumerator_1_t5255____array_0_FieldInfo,
	&InternalEnumerator_1_t5255____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5255____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5255_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5255____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5255_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31651_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5255_PropertyInfos[] =
{
	&InternalEnumerator_1_t5255____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5255____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5255_InternalEnumerator_1__ctor_m31647_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31647_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31647_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5255_InternalEnumerator_1__ctor_m31647_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31647_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31649_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31649_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31649_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31650_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31650_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31650_GenericMethod/* genericMethod */

};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31651_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31651_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* declaring_type */
	, &ReliabilityContractAttribute_t2017_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31651_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5255_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31647_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_MethodInfo,
	&InternalEnumerator_1_Dispose_m31649_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31650_MethodInfo,
	&InternalEnumerator_1_get_Current_m31651_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31650_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31649_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5255_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31648_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31650_MethodInfo,
	&InternalEnumerator_1_Dispose_m31649_MethodInfo,
	&InternalEnumerator_1_get_Current_m31651_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5255_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7386_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5255_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7386_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReliabilityContractAttribute_t2017_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5255_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31651_MethodInfo/* Method Usage */,
	&ReliabilityContractAttribute_t2017_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReliabilityContractAttribute_t2017_m41521_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5255_0_0_0;
extern Il2CppType InternalEnumerator_1_t5255_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5255_GenericClass;
TypeInfo InternalEnumerator_1_t5255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5255_MethodInfos/* methods */
	, InternalEnumerator_1_t5255_PropertyInfos/* properties */
	, InternalEnumerator_1_t5255_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5255_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5255_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5255_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5255_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5255_1_0_0/* this_arg */
	, InternalEnumerator_1_t5255_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5255_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5255_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5255)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9489_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern MethodInfo ICollection_1_get_Count_m52790_MethodInfo;
static PropertyInfo ICollection_1_t9489____Count_PropertyInfo = 
{
	&ICollection_1_t9489_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52791_MethodInfo;
static PropertyInfo ICollection_1_t9489____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9489_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9489_PropertyInfos[] =
{
	&ICollection_1_t9489____Count_PropertyInfo,
	&ICollection_1_t9489____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52790_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52790_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52790_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52791_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52791_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52791_GenericMethod/* genericMethod */

};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
static ParameterInfo ICollection_1_t9489_ICollection_1_Add_m52792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttribute_t2017_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52792_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52792_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9489_ICollection_1_Add_m52792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52792_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52793_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52793_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52793_GenericMethod/* genericMethod */

};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
static ParameterInfo ICollection_1_t9489_ICollection_1_Contains_m52794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttribute_t2017_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52794_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52794_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9489_ICollection_1_Contains_m52794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52794_GenericMethod/* genericMethod */

};
extern Il2CppType ReliabilityContractAttributeU5BU5D_t5554_0_0_0;
extern Il2CppType ReliabilityContractAttributeU5BU5D_t5554_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9489_ICollection_1_CopyTo_m52795_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttributeU5BU5D_t5554_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52795_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52795_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9489_ICollection_1_CopyTo_m52795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52795_GenericMethod/* genericMethod */

};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
static ParameterInfo ICollection_1_t9489_ICollection_1_Remove_m52796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttribute_t2017_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52796_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52796_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9489_ICollection_1_Remove_m52796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52796_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9489_MethodInfos[] =
{
	&ICollection_1_get_Count_m52790_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52791_MethodInfo,
	&ICollection_1_Add_m52792_MethodInfo,
	&ICollection_1_Clear_m52793_MethodInfo,
	&ICollection_1_Contains_m52794_MethodInfo,
	&ICollection_1_CopyTo_m52795_MethodInfo,
	&ICollection_1_Remove_m52796_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9491_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9489_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9491_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9489_0_0_0;
extern Il2CppType ICollection_1_t9489_1_0_0;
struct ICollection_1_t9489;
extern Il2CppGenericClass ICollection_1_t9489_GenericClass;
TypeInfo ICollection_1_t9489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9489_MethodInfos/* methods */
	, ICollection_1_t9489_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9489_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9489_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9489_0_0_0/* byval_arg */
	, &ICollection_1_t9489_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9489_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern Il2CppType IEnumerator_1_t7386_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52797_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52797_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9491_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7386_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52797_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9491_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52797_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9491_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9491_0_0_0;
extern Il2CppType IEnumerable_1_t9491_1_0_0;
struct IEnumerable_1_t9491;
extern Il2CppGenericClass IEnumerable_1_t9491_GenericClass;
TypeInfo IEnumerable_1_t9491_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9491_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9491_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9491_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9491_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9491_0_0_0/* byval_arg */
	, &IEnumerable_1_t9491_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9491_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9490_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern MethodInfo IList_1_get_Item_m52798_MethodInfo;
extern MethodInfo IList_1_set_Item_m52799_MethodInfo;
static PropertyInfo IList_1_t9490____Item_PropertyInfo = 
{
	&IList_1_t9490_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52798_MethodInfo/* get */
	, &IList_1_set_Item_m52799_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9490_PropertyInfos[] =
{
	&IList_1_t9490____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
static ParameterInfo IList_1_t9490_IList_1_IndexOf_m52800_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttribute_t2017_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52800_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52800_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9490_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9490_IList_1_IndexOf_m52800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52800_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
static ParameterInfo IList_1_t9490_IList_1_Insert_m52801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttribute_t2017_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52801_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52801_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9490_IList_1_Insert_m52801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52801_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9490_IList_1_RemoveAt_m52802_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52802_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52802_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9490_IList_1_RemoveAt_m52802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52802_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9490_IList_1_get_Item_m52798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52798_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52798_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9490_il2cpp_TypeInfo/* declaring_type */
	, &ReliabilityContractAttribute_t2017_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9490_IList_1_get_Item_m52798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReliabilityContractAttribute_t2017_0_0_0;
static ParameterInfo IList_1_t9490_IList_1_set_Item_m52799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReliabilityContractAttribute_t2017_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52799_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52799_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9490_IList_1_set_Item_m52799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52799_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9490_MethodInfos[] =
{
	&IList_1_IndexOf_m52800_MethodInfo,
	&IList_1_Insert_m52801_MethodInfo,
	&IList_1_RemoveAt_m52802_MethodInfo,
	&IList_1_get_Item_m52798_MethodInfo,
	&IList_1_set_Item_m52799_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9490_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9489_il2cpp_TypeInfo,
	&IEnumerable_1_t9491_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9490_0_0_0;
extern Il2CppType IList_1_t9490_1_0_0;
struct IList_1_t9490;
extern Il2CppGenericClass IList_1_t9490_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9490_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9490_MethodInfos/* methods */
	, IList_1_t9490_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9490_il2cpp_TypeInfo/* element_class */
	, IList_1_t9490_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9490_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9490_0_0_0/* byval_arg */
	, &IList_1_t9490_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9490_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7388_il2cpp_TypeInfo;

// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CallingConvention>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CallingConvention>
extern MethodInfo IEnumerator_1_get_Current_m52803_MethodInfo;
static PropertyInfo IEnumerator_1_t7388____Current_PropertyInfo = 
{
	&IEnumerator_1_t7388_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7388_PropertyInfos[] =
{
	&IEnumerator_1_t7388____Current_PropertyInfo,
	NULL
};
extern Il2CppType CallingConvention_t2019_0_0_0;
extern void* RuntimeInvoker_CallingConvention_t2019 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52803_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CallingConvention>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52803_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7388_il2cpp_TypeInfo/* declaring_type */
	, &CallingConvention_t2019_0_0_0/* return_type */
	, RuntimeInvoker_CallingConvention_t2019/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52803_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7388_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52803_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7388_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7388_0_0_0;
extern Il2CppType IEnumerator_1_t7388_1_0_0;
struct IEnumerator_1_t7388;
extern Il2CppGenericClass IEnumerator_1_t7388_GenericClass;
TypeInfo IEnumerator_1_t7388_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7388_MethodInfos/* methods */
	, IEnumerator_1_t7388_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7388_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7388_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7388_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7388_0_0_0/* byval_arg */
	, &IEnumerator_1_t7388_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7388_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_710.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5256_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_710MethodDeclarations.h"

extern TypeInfo CallingConvention_t2019_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCallingConvention_t2019_m41532_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.CallingConvention>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.CallingConvention>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCallingConvention_t2019_m41532 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31652_MethodInfo;
 void InternalEnumerator_1__ctor_m31652 (InternalEnumerator_1_t5256 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653 (InternalEnumerator_1_t5256 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31656(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31656_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CallingConvention_t2019_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31654_MethodInfo;
 void InternalEnumerator_1_Dispose_m31654 (InternalEnumerator_1_t5256 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31655_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31655 (InternalEnumerator_1_t5256 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31656 (InternalEnumerator_1_t5256 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCallingConvention_t2019_m41532(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCallingConvention_t2019_m41532_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5256____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5256, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5256____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5256, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5256_FieldInfos[] =
{
	&InternalEnumerator_1_t5256____array_0_FieldInfo,
	&InternalEnumerator_1_t5256____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5256____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5256_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5256____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5256_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31656_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5256_PropertyInfos[] =
{
	&InternalEnumerator_1_t5256____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5256____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5256_InternalEnumerator_1__ctor_m31652_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31652_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31652_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31652/* method */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5256_InternalEnumerator_1__ctor_m31652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31652_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653/* method */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31654_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31654_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31654/* method */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31654_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31655_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31655_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31655/* method */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31655_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConvention_t2019_0_0_0;
extern void* RuntimeInvoker_CallingConvention_t2019 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31656_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31656_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31656/* method */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* declaring_type */
	, &CallingConvention_t2019_0_0_0/* return_type */
	, RuntimeInvoker_CallingConvention_t2019/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31656_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5256_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31652_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_MethodInfo,
	&InternalEnumerator_1_Dispose_m31654_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31655_MethodInfo,
	&InternalEnumerator_1_get_Current_m31656_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5256_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31653_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31655_MethodInfo,
	&InternalEnumerator_1_Dispose_m31654_MethodInfo,
	&InternalEnumerator_1_get_Current_m31656_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5256_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7388_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5256_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7388_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5256_0_0_0;
extern Il2CppType InternalEnumerator_1_t5256_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5256_GenericClass;
TypeInfo InternalEnumerator_1_t5256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5256_MethodInfos/* methods */
	, InternalEnumerator_1_t5256_PropertyInfos/* properties */
	, InternalEnumerator_1_t5256_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5256_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5256_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5256_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5256_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5256_1_0_0/* this_arg */
	, InternalEnumerator_1_t5256_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5256_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5256)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9492_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>
extern MethodInfo ICollection_1_get_Count_m52804_MethodInfo;
static PropertyInfo ICollection_1_t9492____Count_PropertyInfo = 
{
	&ICollection_1_t9492_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52804_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52805_MethodInfo;
static PropertyInfo ICollection_1_t9492____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9492_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52805_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9492_PropertyInfos[] =
{
	&ICollection_1_t9492____Count_PropertyInfo,
	&ICollection_1_t9492____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52804_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::get_Count()
MethodInfo ICollection_1_get_Count_m52804_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52804_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52805_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52805_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52805_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConvention_t2019_0_0_0;
extern Il2CppType CallingConvention_t2019_0_0_0;
static ParameterInfo ICollection_1_t9492_ICollection_1_Add_m52806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConvention_t2019_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52806_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Add(T)
MethodInfo ICollection_1_Add_m52806_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9492_ICollection_1_Add_m52806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52806_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52807_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Clear()
MethodInfo ICollection_1_Clear_m52807_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52807_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConvention_t2019_0_0_0;
static ParameterInfo ICollection_1_t9492_ICollection_1_Contains_m52808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConvention_t2019_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52808_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Contains(T)
MethodInfo ICollection_1_Contains_m52808_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9492_ICollection_1_Contains_m52808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52808_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConventionU5BU5D_t5555_0_0_0;
extern Il2CppType CallingConventionU5BU5D_t5555_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9492_ICollection_1_CopyTo_m52809_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CallingConventionU5BU5D_t5555_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52809_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52809_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9492_ICollection_1_CopyTo_m52809_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52809_GenericMethod/* genericMethod */

};
extern Il2CppType CallingConvention_t2019_0_0_0;
static ParameterInfo ICollection_1_t9492_ICollection_1_Remove_m52810_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConvention_t2019_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52810_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>::Remove(T)
MethodInfo ICollection_1_Remove_m52810_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9492_ICollection_1_Remove_m52810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52810_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9492_MethodInfos[] =
{
	&ICollection_1_get_Count_m52804_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52805_MethodInfo,
	&ICollection_1_Add_m52806_MethodInfo,
	&ICollection_1_Clear_m52807_MethodInfo,
	&ICollection_1_Contains_m52808_MethodInfo,
	&ICollection_1_CopyTo_m52809_MethodInfo,
	&ICollection_1_Remove_m52810_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9494_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9492_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9494_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9492_0_0_0;
extern Il2CppType ICollection_1_t9492_1_0_0;
struct ICollection_1_t9492;
extern Il2CppGenericClass ICollection_1_t9492_GenericClass;
TypeInfo ICollection_1_t9492_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9492_MethodInfos/* methods */
	, ICollection_1_t9492_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9492_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9492_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9492_0_0_0/* byval_arg */
	, &ICollection_1_t9492_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9492_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CallingConvention>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CallingConvention>
extern Il2CppType IEnumerator_1_t7388_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52811_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CallingConvention>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52811_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9494_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7388_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52811_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9494_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52811_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9494_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9494_0_0_0;
extern Il2CppType IEnumerable_1_t9494_1_0_0;
struct IEnumerable_1_t9494;
extern Il2CppGenericClass IEnumerable_1_t9494_GenericClass;
TypeInfo IEnumerable_1_t9494_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9494_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9494_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9494_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9494_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9494_0_0_0/* byval_arg */
	, &IEnumerable_1_t9494_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9494_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9493_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>
extern MethodInfo IList_1_get_Item_m52812_MethodInfo;
extern MethodInfo IList_1_set_Item_m52813_MethodInfo;
static PropertyInfo IList_1_t9493____Item_PropertyInfo = 
{
	&IList_1_t9493_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52812_MethodInfo/* get */
	, &IList_1_set_Item_m52813_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9493_PropertyInfos[] =
{
	&IList_1_t9493____Item_PropertyInfo,
	NULL
};
extern Il2CppType CallingConvention_t2019_0_0_0;
static ParameterInfo IList_1_t9493_IList_1_IndexOf_m52814_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CallingConvention_t2019_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52814_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52814_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9493_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9493_IList_1_IndexOf_m52814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CallingConvention_t2019_0_0_0;
static ParameterInfo IList_1_t9493_IList_1_Insert_m52815_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CallingConvention_t2019_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52815_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52815_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9493_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9493_IList_1_Insert_m52815_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52815_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9493_IList_1_RemoveAt_m52816_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52816_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52816_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9493_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9493_IList_1_RemoveAt_m52816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52816_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9493_IList_1_get_Item_m52812_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CallingConvention_t2019_0_0_0;
extern void* RuntimeInvoker_CallingConvention_t2019_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52812_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52812_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9493_il2cpp_TypeInfo/* declaring_type */
	, &CallingConvention_t2019_0_0_0/* return_type */
	, RuntimeInvoker_CallingConvention_t2019_Int32_t123/* invoker_method */
	, IList_1_t9493_IList_1_get_Item_m52812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52812_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CallingConvention_t2019_0_0_0;
static ParameterInfo IList_1_t9493_IList_1_set_Item_m52813_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CallingConvention_t2019_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52813_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52813_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9493_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9493_IList_1_set_Item_m52813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52813_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9493_MethodInfos[] =
{
	&IList_1_IndexOf_m52814_MethodInfo,
	&IList_1_Insert_m52815_MethodInfo,
	&IList_1_RemoveAt_m52816_MethodInfo,
	&IList_1_get_Item_m52812_MethodInfo,
	&IList_1_set_Item_m52813_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9493_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9492_il2cpp_TypeInfo,
	&IEnumerable_1_t9494_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9493_0_0_0;
extern Il2CppType IList_1_t9493_1_0_0;
struct IList_1_t9493;
extern Il2CppGenericClass IList_1_t9493_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9493_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9493_MethodInfos/* methods */
	, IList_1_t9493_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9493_il2cpp_TypeInfo/* element_class */
	, IList_1_t9493_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9493_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9493_0_0_0/* byval_arg */
	, &IList_1_t9493_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9493_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7390_il2cpp_TypeInfo;

// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CharSet>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CharSet>
extern MethodInfo IEnumerator_1_get_Current_m52817_MethodInfo;
static PropertyInfo IEnumerator_1_t7390____Current_PropertyInfo = 
{
	&IEnumerator_1_t7390_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52817_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7390_PropertyInfos[] =
{
	&IEnumerator_1_t7390____Current_PropertyInfo,
	NULL
};
extern Il2CppType CharSet_t2020_0_0_0;
extern void* RuntimeInvoker_CharSet_t2020 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52817_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CharSet>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52817_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7390_il2cpp_TypeInfo/* declaring_type */
	, &CharSet_t2020_0_0_0/* return_type */
	, RuntimeInvoker_CharSet_t2020/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52817_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7390_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52817_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7390_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7390_0_0_0;
extern Il2CppType IEnumerator_1_t7390_1_0_0;
struct IEnumerator_1_t7390;
extern Il2CppGenericClass IEnumerator_1_t7390_GenericClass;
TypeInfo IEnumerator_1_t7390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7390_MethodInfos/* methods */
	, IEnumerator_1_t7390_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7390_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7390_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7390_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7390_0_0_0/* byval_arg */
	, &IEnumerator_1_t7390_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7390_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_711.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5257_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_711MethodDeclarations.h"

extern TypeInfo CharSet_t2020_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31661_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCharSet_t2020_m41543_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.CharSet>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.CharSet>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCharSet_t2020_m41543 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31657_MethodInfo;
 void InternalEnumerator_1__ctor_m31657 (InternalEnumerator_1_t5257 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658 (InternalEnumerator_1_t5257 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31661(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31661_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CharSet_t2020_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31659_MethodInfo;
 void InternalEnumerator_1_Dispose_m31659 (InternalEnumerator_1_t5257 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31660_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31660 (InternalEnumerator_1_t5257 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31661 (InternalEnumerator_1_t5257 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCharSet_t2020_m41543(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCharSet_t2020_m41543_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5257____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5257, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5257____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5257, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5257_FieldInfos[] =
{
	&InternalEnumerator_1_t5257____array_0_FieldInfo,
	&InternalEnumerator_1_t5257____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5257____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5257_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5257____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5257_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5257_PropertyInfos[] =
{
	&InternalEnumerator_1_t5257____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5257____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5257_InternalEnumerator_1__ctor_m31657_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31657_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31657_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31657/* method */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5257_InternalEnumerator_1__ctor_m31657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31657_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658/* method */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31659_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31659_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31659/* method */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31659_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31660_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31660_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31660/* method */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31660_GenericMethod/* genericMethod */

};
extern Il2CppType CharSet_t2020_0_0_0;
extern void* RuntimeInvoker_CharSet_t2020 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31661_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31661_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31661/* method */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* declaring_type */
	, &CharSet_t2020_0_0_0/* return_type */
	, RuntimeInvoker_CharSet_t2020/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31661_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5257_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31657_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_MethodInfo,
	&InternalEnumerator_1_Dispose_m31659_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31660_MethodInfo,
	&InternalEnumerator_1_get_Current_m31661_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5257_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31658_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31660_MethodInfo,
	&InternalEnumerator_1_Dispose_m31659_MethodInfo,
	&InternalEnumerator_1_get_Current_m31661_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5257_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7390_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5257_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7390_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5257_0_0_0;
extern Il2CppType InternalEnumerator_1_t5257_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5257_GenericClass;
TypeInfo InternalEnumerator_1_t5257_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5257_MethodInfos/* methods */
	, InternalEnumerator_1_t5257_PropertyInfos/* properties */
	, InternalEnumerator_1_t5257_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5257_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5257_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5257_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5257_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5257_1_0_0/* this_arg */
	, InternalEnumerator_1_t5257_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5257_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5257)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9495_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>
extern MethodInfo ICollection_1_get_Count_m52818_MethodInfo;
static PropertyInfo ICollection_1_t9495____Count_PropertyInfo = 
{
	&ICollection_1_t9495_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52818_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52819_MethodInfo;
static PropertyInfo ICollection_1_t9495____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9495_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52819_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9495_PropertyInfos[] =
{
	&ICollection_1_t9495____Count_PropertyInfo,
	&ICollection_1_t9495____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52818_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::get_Count()
MethodInfo ICollection_1_get_Count_m52818_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52818_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52819_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52819_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52819_GenericMethod/* genericMethod */

};
extern Il2CppType CharSet_t2020_0_0_0;
extern Il2CppType CharSet_t2020_0_0_0;
static ParameterInfo ICollection_1_t9495_ICollection_1_Add_m52820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharSet_t2020_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52820_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Add(T)
MethodInfo ICollection_1_Add_m52820_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9495_ICollection_1_Add_m52820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52820_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52821_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Clear()
MethodInfo ICollection_1_Clear_m52821_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52821_GenericMethod/* genericMethod */

};
extern Il2CppType CharSet_t2020_0_0_0;
static ParameterInfo ICollection_1_t9495_ICollection_1_Contains_m52822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharSet_t2020_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52822_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Contains(T)
MethodInfo ICollection_1_Contains_m52822_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9495_ICollection_1_Contains_m52822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52822_GenericMethod/* genericMethod */

};
extern Il2CppType CharSetU5BU5D_t5556_0_0_0;
extern Il2CppType CharSetU5BU5D_t5556_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9495_ICollection_1_CopyTo_m52823_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CharSetU5BU5D_t5556_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52823_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52823_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9495_ICollection_1_CopyTo_m52823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52823_GenericMethod/* genericMethod */

};
extern Il2CppType CharSet_t2020_0_0_0;
static ParameterInfo ICollection_1_t9495_ICollection_1_Remove_m52824_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharSet_t2020_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52824_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>::Remove(T)
MethodInfo ICollection_1_Remove_m52824_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9495_ICollection_1_Remove_m52824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52824_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9495_MethodInfos[] =
{
	&ICollection_1_get_Count_m52818_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52819_MethodInfo,
	&ICollection_1_Add_m52820_MethodInfo,
	&ICollection_1_Clear_m52821_MethodInfo,
	&ICollection_1_Contains_m52822_MethodInfo,
	&ICollection_1_CopyTo_m52823_MethodInfo,
	&ICollection_1_Remove_m52824_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9497_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9495_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9497_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9495_0_0_0;
extern Il2CppType ICollection_1_t9495_1_0_0;
struct ICollection_1_t9495;
extern Il2CppGenericClass ICollection_1_t9495_GenericClass;
TypeInfo ICollection_1_t9495_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9495_MethodInfos/* methods */
	, ICollection_1_t9495_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9495_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9495_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9495_0_0_0/* byval_arg */
	, &ICollection_1_t9495_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9495_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CharSet>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CharSet>
extern Il2CppType IEnumerator_1_t7390_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52825_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CharSet>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52825_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9497_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7390_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52825_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9497_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52825_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9497_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9497_0_0_0;
extern Il2CppType IEnumerable_1_t9497_1_0_0;
struct IEnumerable_1_t9497;
extern Il2CppGenericClass IEnumerable_1_t9497_GenericClass;
TypeInfo IEnumerable_1_t9497_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9497_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9497_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9497_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9497_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9497_0_0_0/* byval_arg */
	, &IEnumerable_1_t9497_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9497_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9496_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>
extern MethodInfo IList_1_get_Item_m52826_MethodInfo;
extern MethodInfo IList_1_set_Item_m52827_MethodInfo;
static PropertyInfo IList_1_t9496____Item_PropertyInfo = 
{
	&IList_1_t9496_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52826_MethodInfo/* get */
	, &IList_1_set_Item_m52827_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9496_PropertyInfos[] =
{
	&IList_1_t9496____Item_PropertyInfo,
	NULL
};
extern Il2CppType CharSet_t2020_0_0_0;
static ParameterInfo IList_1_t9496_IList_1_IndexOf_m52828_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharSet_t2020_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52828_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52828_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9496_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9496_IList_1_IndexOf_m52828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CharSet_t2020_0_0_0;
static ParameterInfo IList_1_t9496_IList_1_Insert_m52829_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CharSet_t2020_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52829_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52829_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9496_IList_1_Insert_m52829_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52829_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9496_IList_1_RemoveAt_m52830_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52830_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52830_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9496_IList_1_RemoveAt_m52830_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52830_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9496_IList_1_get_Item_m52826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CharSet_t2020_0_0_0;
extern void* RuntimeInvoker_CharSet_t2020_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52826_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52826_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9496_il2cpp_TypeInfo/* declaring_type */
	, &CharSet_t2020_0_0_0/* return_type */
	, RuntimeInvoker_CharSet_t2020_Int32_t123/* invoker_method */
	, IList_1_t9496_IList_1_get_Item_m52826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CharSet_t2020_0_0_0;
static ParameterInfo IList_1_t9496_IList_1_set_Item_m52827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CharSet_t2020_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52827_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52827_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9496_IList_1_set_Item_m52827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52827_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9496_MethodInfos[] =
{
	&IList_1_IndexOf_m52828_MethodInfo,
	&IList_1_Insert_m52829_MethodInfo,
	&IList_1_RemoveAt_m52830_MethodInfo,
	&IList_1_get_Item_m52826_MethodInfo,
	&IList_1_set_Item_m52827_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9496_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9495_il2cpp_TypeInfo,
	&IEnumerable_1_t9497_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9496_0_0_0;
extern Il2CppType IList_1_t9496_1_0_0;
struct IList_1_t9496;
extern Il2CppGenericClass IList_1_t9496_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9496_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9496_MethodInfos/* methods */
	, IList_1_t9496_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9496_il2cpp_TypeInfo/* element_class */
	, IList_1_t9496_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9496_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9496_0_0_0/* byval_arg */
	, &IList_1_t9496_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9496_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7392_il2cpp_TypeInfo;

// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52831_MethodInfo;
static PropertyInfo IEnumerator_1_t7392____Current_PropertyInfo = 
{
	&IEnumerator_1_t7392_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7392_PropertyInfos[] =
{
	&IEnumerator_1_t7392____Current_PropertyInfo,
	NULL
};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52831_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52831_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7392_il2cpp_TypeInfo/* declaring_type */
	, &ClassInterfaceAttribute_t2021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52831_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7392_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52831_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7392_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7392_0_0_0;
extern Il2CppType IEnumerator_1_t7392_1_0_0;
struct IEnumerator_1_t7392;
extern Il2CppGenericClass IEnumerator_1_t7392_GenericClass;
TypeInfo IEnumerator_1_t7392_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7392_MethodInfos/* methods */
	, IEnumerator_1_t7392_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7392_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7392_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7392_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7392_0_0_0/* byval_arg */
	, &IEnumerator_1_t7392_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7392_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_712.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5258_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_712MethodDeclarations.h"

extern TypeInfo ClassInterfaceAttribute_t2021_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31666_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisClassInterfaceAttribute_t2021_m41554_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ClassInterfaceAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ClassInterfaceAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisClassInterfaceAttribute_t2021_m41554(__this, p0, method) (ClassInterfaceAttribute_t2021 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5258____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5258, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5258____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5258, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5258_FieldInfos[] =
{
	&InternalEnumerator_1_t5258____array_0_FieldInfo,
	&InternalEnumerator_1_t5258____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5258____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5258_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5258____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5258_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31666_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5258_PropertyInfos[] =
{
	&InternalEnumerator_1_t5258____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5258____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5258_InternalEnumerator_1__ctor_m31662_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31662_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31662_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5258_InternalEnumerator_1__ctor_m31662_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31662_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31664_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31664_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31664_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31665_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31665_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31665_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31666_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31666_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* declaring_type */
	, &ClassInterfaceAttribute_t2021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31666_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5258_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31662_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_MethodInfo,
	&InternalEnumerator_1_Dispose_m31664_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31665_MethodInfo,
	&InternalEnumerator_1_get_Current_m31666_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31665_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31664_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5258_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31663_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31665_MethodInfo,
	&InternalEnumerator_1_Dispose_m31664_MethodInfo,
	&InternalEnumerator_1_get_Current_m31666_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5258_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7392_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5258_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7392_il2cpp_TypeInfo, 7},
};
extern TypeInfo ClassInterfaceAttribute_t2021_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5258_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31666_MethodInfo/* Method Usage */,
	&ClassInterfaceAttribute_t2021_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisClassInterfaceAttribute_t2021_m41554_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5258_0_0_0;
extern Il2CppType InternalEnumerator_1_t5258_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5258_GenericClass;
TypeInfo InternalEnumerator_1_t5258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5258_MethodInfos/* methods */
	, InternalEnumerator_1_t5258_PropertyInfos/* properties */
	, InternalEnumerator_1_t5258_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5258_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5258_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5258_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5258_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5258_1_0_0/* this_arg */
	, InternalEnumerator_1_t5258_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5258_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5258_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5258)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9498_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern MethodInfo ICollection_1_get_Count_m52832_MethodInfo;
static PropertyInfo ICollection_1_t9498____Count_PropertyInfo = 
{
	&ICollection_1_t9498_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52832_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52833_MethodInfo;
static PropertyInfo ICollection_1_t9498____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9498_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52833_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9498_PropertyInfos[] =
{
	&ICollection_1_t9498____Count_PropertyInfo,
	&ICollection_1_t9498____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52832_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52832_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52832_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52833_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52833_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52833_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
static ParameterInfo ICollection_1_t9498_ICollection_1_Add_m52834_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttribute_t2021_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52834_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52834_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9498_ICollection_1_Add_m52834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52834_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52835_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52835_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52835_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
static ParameterInfo ICollection_1_t9498_ICollection_1_Contains_m52836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttribute_t2021_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52836_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52836_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9498_ICollection_1_Contains_m52836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52836_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceAttributeU5BU5D_t5557_0_0_0;
extern Il2CppType ClassInterfaceAttributeU5BU5D_t5557_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9498_ICollection_1_CopyTo_m52837_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttributeU5BU5D_t5557_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52837_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52837_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9498_ICollection_1_CopyTo_m52837_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52837_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
static ParameterInfo ICollection_1_t9498_ICollection_1_Remove_m52838_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttribute_t2021_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52838_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52838_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9498_ICollection_1_Remove_m52838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52838_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9498_MethodInfos[] =
{
	&ICollection_1_get_Count_m52832_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52833_MethodInfo,
	&ICollection_1_Add_m52834_MethodInfo,
	&ICollection_1_Clear_m52835_MethodInfo,
	&ICollection_1_Contains_m52836_MethodInfo,
	&ICollection_1_CopyTo_m52837_MethodInfo,
	&ICollection_1_Remove_m52838_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9500_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9498_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9500_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9498_0_0_0;
extern Il2CppType ICollection_1_t9498_1_0_0;
struct ICollection_1_t9498;
extern Il2CppGenericClass ICollection_1_t9498_GenericClass;
TypeInfo ICollection_1_t9498_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9498_MethodInfos/* methods */
	, ICollection_1_t9498_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9498_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9498_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9498_0_0_0/* byval_arg */
	, &ICollection_1_t9498_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9498_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern Il2CppType IEnumerator_1_t7392_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52839_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52839_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9500_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7392_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52839_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9500_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52839_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9500_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9500_0_0_0;
extern Il2CppType IEnumerable_1_t9500_1_0_0;
struct IEnumerable_1_t9500;
extern Il2CppGenericClass IEnumerable_1_t9500_GenericClass;
TypeInfo IEnumerable_1_t9500_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9500_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9500_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9500_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9500_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9500_0_0_0/* byval_arg */
	, &IEnumerable_1_t9500_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9500_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9499_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern MethodInfo IList_1_get_Item_m52840_MethodInfo;
extern MethodInfo IList_1_set_Item_m52841_MethodInfo;
static PropertyInfo IList_1_t9499____Item_PropertyInfo = 
{
	&IList_1_t9499_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52840_MethodInfo/* get */
	, &IList_1_set_Item_m52841_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9499_PropertyInfos[] =
{
	&IList_1_t9499____Item_PropertyInfo,
	NULL
};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
static ParameterInfo IList_1_t9499_IList_1_IndexOf_m52842_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttribute_t2021_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52842_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52842_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9499_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9499_IList_1_IndexOf_m52842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52842_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
static ParameterInfo IList_1_t9499_IList_1_Insert_m52843_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttribute_t2021_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52843_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52843_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9499_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9499_IList_1_Insert_m52843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52843_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9499_IList_1_RemoveAt_m52844_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52844_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52844_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9499_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9499_IList_1_RemoveAt_m52844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52844_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9499_IList_1_get_Item_m52840_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52840_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52840_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9499_il2cpp_TypeInfo/* declaring_type */
	, &ClassInterfaceAttribute_t2021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9499_IList_1_get_Item_m52840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52840_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ClassInterfaceAttribute_t2021_0_0_0;
static ParameterInfo IList_1_t9499_IList_1_set_Item_m52841_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceAttribute_t2021_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52841_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52841_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9499_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9499_IList_1_set_Item_m52841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52841_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9499_MethodInfos[] =
{
	&IList_1_IndexOf_m52842_MethodInfo,
	&IList_1_Insert_m52843_MethodInfo,
	&IList_1_RemoveAt_m52844_MethodInfo,
	&IList_1_get_Item_m52840_MethodInfo,
	&IList_1_set_Item_m52841_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9499_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9498_il2cpp_TypeInfo,
	&IEnumerable_1_t9500_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9499_0_0_0;
extern Il2CppType IList_1_t9499_1_0_0;
struct IList_1_t9499;
extern Il2CppGenericClass IList_1_t9499_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9499_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9499_MethodInfos/* methods */
	, IList_1_t9499_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9499_il2cpp_TypeInfo/* element_class */
	, IList_1_t9499_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9499_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9499_0_0_0/* byval_arg */
	, &IList_1_t9499_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9499_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7394_il2cpp_TypeInfo;

// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>
extern MethodInfo IEnumerator_1_get_Current_m52845_MethodInfo;
static PropertyInfo IEnumerator_1_t7394____Current_PropertyInfo = 
{
	&IEnumerator_1_t7394_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7394_PropertyInfos[] =
{
	&IEnumerator_1_t7394____Current_PropertyInfo,
	NULL
};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
extern void* RuntimeInvoker_ClassInterfaceType_t2022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52845_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52845_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7394_il2cpp_TypeInfo/* declaring_type */
	, &ClassInterfaceType_t2022_0_0_0/* return_type */
	, RuntimeInvoker_ClassInterfaceType_t2022/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52845_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7394_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52845_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7394_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7394_0_0_0;
extern Il2CppType IEnumerator_1_t7394_1_0_0;
struct IEnumerator_1_t7394;
extern Il2CppGenericClass IEnumerator_1_t7394_GenericClass;
TypeInfo IEnumerator_1_t7394_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7394_MethodInfos/* methods */
	, IEnumerator_1_t7394_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7394_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7394_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7394_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7394_0_0_0/* byval_arg */
	, &IEnumerator_1_t7394_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7394_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_713.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5259_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_713MethodDeclarations.h"

extern TypeInfo ClassInterfaceType_t2022_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31671_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisClassInterfaceType_t2022_m41565_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ClassInterfaceType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ClassInterfaceType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisClassInterfaceType_t2022_m41565 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31667_MethodInfo;
 void InternalEnumerator_1__ctor_m31667 (InternalEnumerator_1_t5259 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668 (InternalEnumerator_1_t5259 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31671(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31671_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ClassInterfaceType_t2022_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31669_MethodInfo;
 void InternalEnumerator_1_Dispose_m31669 (InternalEnumerator_1_t5259 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31670_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31670 (InternalEnumerator_1_t5259 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31671 (InternalEnumerator_1_t5259 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisClassInterfaceType_t2022_m41565(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisClassInterfaceType_t2022_m41565_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5259____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5259, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5259____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5259, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5259_FieldInfos[] =
{
	&InternalEnumerator_1_t5259____array_0_FieldInfo,
	&InternalEnumerator_1_t5259____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5259____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5259_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5259____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5259_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5259_PropertyInfos[] =
{
	&InternalEnumerator_1_t5259____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5259____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5259_InternalEnumerator_1__ctor_m31667_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31667_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31667_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31667/* method */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5259_InternalEnumerator_1__ctor_m31667_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31667_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668/* method */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31669_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31669_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31669/* method */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31669_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31670_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31670_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31670/* method */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31670_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
extern void* RuntimeInvoker_ClassInterfaceType_t2022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31671_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31671_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31671/* method */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* declaring_type */
	, &ClassInterfaceType_t2022_0_0_0/* return_type */
	, RuntimeInvoker_ClassInterfaceType_t2022/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31671_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5259_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31667_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_MethodInfo,
	&InternalEnumerator_1_Dispose_m31669_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31670_MethodInfo,
	&InternalEnumerator_1_get_Current_m31671_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5259_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31668_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31670_MethodInfo,
	&InternalEnumerator_1_Dispose_m31669_MethodInfo,
	&InternalEnumerator_1_get_Current_m31671_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5259_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7394_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5259_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7394_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5259_0_0_0;
extern Il2CppType InternalEnumerator_1_t5259_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5259_GenericClass;
TypeInfo InternalEnumerator_1_t5259_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5259_MethodInfos/* methods */
	, InternalEnumerator_1_t5259_PropertyInfos/* properties */
	, InternalEnumerator_1_t5259_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5259_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5259_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5259_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5259_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5259_1_0_0/* this_arg */
	, InternalEnumerator_1_t5259_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5259_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5259)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9501_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>
extern MethodInfo ICollection_1_get_Count_m52846_MethodInfo;
static PropertyInfo ICollection_1_t9501____Count_PropertyInfo = 
{
	&ICollection_1_t9501_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52846_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52847_MethodInfo;
static PropertyInfo ICollection_1_t9501____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9501_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52847_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9501_PropertyInfos[] =
{
	&ICollection_1_t9501____Count_PropertyInfo,
	&ICollection_1_t9501____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52846_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Count()
MethodInfo ICollection_1_get_Count_m52846_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52846_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52847_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52847_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52847_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
static ParameterInfo ICollection_1_t9501_ICollection_1_Add_m52848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceType_t2022_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52848_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Add(T)
MethodInfo ICollection_1_Add_m52848_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9501_ICollection_1_Add_m52848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52848_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52849_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Clear()
MethodInfo ICollection_1_Clear_m52849_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52849_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
static ParameterInfo ICollection_1_t9501_ICollection_1_Contains_m52850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceType_t2022_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52850_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Contains(T)
MethodInfo ICollection_1_Contains_m52850_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9501_ICollection_1_Contains_m52850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52850_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceTypeU5BU5D_t5558_0_0_0;
extern Il2CppType ClassInterfaceTypeU5BU5D_t5558_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9501_ICollection_1_CopyTo_m52851_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceTypeU5BU5D_t5558_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52851_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52851_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9501_ICollection_1_CopyTo_m52851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52851_GenericMethod/* genericMethod */

};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
static ParameterInfo ICollection_1_t9501_ICollection_1_Remove_m52852_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceType_t2022_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52852_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>::Remove(T)
MethodInfo ICollection_1_Remove_m52852_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9501_ICollection_1_Remove_m52852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52852_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9501_MethodInfos[] =
{
	&ICollection_1_get_Count_m52846_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52847_MethodInfo,
	&ICollection_1_Add_m52848_MethodInfo,
	&ICollection_1_Clear_m52849_MethodInfo,
	&ICollection_1_Contains_m52850_MethodInfo,
	&ICollection_1_CopyTo_m52851_MethodInfo,
	&ICollection_1_Remove_m52852_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9503_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9501_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9503_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9501_0_0_0;
extern Il2CppType ICollection_1_t9501_1_0_0;
struct ICollection_1_t9501;
extern Il2CppGenericClass ICollection_1_t9501_GenericClass;
TypeInfo ICollection_1_t9501_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9501_MethodInfos/* methods */
	, ICollection_1_t9501_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9501_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9501_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9501_0_0_0/* byval_arg */
	, &ICollection_1_t9501_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9501_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceType>
extern Il2CppType IEnumerator_1_t7394_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52853_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52853_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9503_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7394_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52853_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9503_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52853_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9503_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9503_0_0_0;
extern Il2CppType IEnumerable_1_t9503_1_0_0;
struct IEnumerable_1_t9503;
extern Il2CppGenericClass IEnumerable_1_t9503_GenericClass;
TypeInfo IEnumerable_1_t9503_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9503_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9503_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9503_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9503_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9503_0_0_0/* byval_arg */
	, &IEnumerable_1_t9503_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9503_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9502_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>
extern MethodInfo IList_1_get_Item_m52854_MethodInfo;
extern MethodInfo IList_1_set_Item_m52855_MethodInfo;
static PropertyInfo IList_1_t9502____Item_PropertyInfo = 
{
	&IList_1_t9502_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52854_MethodInfo/* get */
	, &IList_1_set_Item_m52855_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9502_PropertyInfos[] =
{
	&IList_1_t9502____Item_PropertyInfo,
	NULL
};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
static ParameterInfo IList_1_t9502_IList_1_IndexOf_m52856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceType_t2022_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52856_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52856_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9502_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9502_IList_1_IndexOf_m52856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52856_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
static ParameterInfo IList_1_t9502_IList_1_Insert_m52857_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceType_t2022_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52857_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52857_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9502_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9502_IList_1_Insert_m52857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52857_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9502_IList_1_RemoveAt_m52858_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52858_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52858_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9502_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9502_IList_1_RemoveAt_m52858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52858_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9502_IList_1_get_Item_m52854_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
extern void* RuntimeInvoker_ClassInterfaceType_t2022_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52854_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52854_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9502_il2cpp_TypeInfo/* declaring_type */
	, &ClassInterfaceType_t2022_0_0_0/* return_type */
	, RuntimeInvoker_ClassInterfaceType_t2022_Int32_t123/* invoker_method */
	, IList_1_t9502_IList_1_get_Item_m52854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52854_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ClassInterfaceType_t2022_0_0_0;
static ParameterInfo IList_1_t9502_IList_1_set_Item_m52855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ClassInterfaceType_t2022_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52855_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52855_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9502_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9502_IList_1_set_Item_m52855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52855_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9502_MethodInfos[] =
{
	&IList_1_IndexOf_m52856_MethodInfo,
	&IList_1_Insert_m52857_MethodInfo,
	&IList_1_RemoveAt_m52858_MethodInfo,
	&IList_1_get_Item_m52854_MethodInfo,
	&IList_1_set_Item_m52855_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9502_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9501_il2cpp_TypeInfo,
	&IEnumerable_1_t9503_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9502_0_0_0;
extern Il2CppType IList_1_t9502_1_0_0;
struct IList_1_t9502;
extern Il2CppGenericClass IList_1_t9502_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9502_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9502_MethodInfos/* methods */
	, IList_1_t9502_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9502_il2cpp_TypeInfo/* element_class */
	, IList_1_t9502_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9502_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9502_0_0_0/* byval_arg */
	, &IList_1_t9502_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9502_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7396_il2cpp_TypeInfo;

// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52859_MethodInfo;
static PropertyInfo IEnumerator_1_t7396____Current_PropertyInfo = 
{
	&IEnumerator_1_t7396_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52859_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7396_PropertyInfos[] =
{
	&IEnumerator_1_t7396____Current_PropertyInfo,
	NULL
};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52859_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52859_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7396_il2cpp_TypeInfo/* declaring_type */
	, &ComDefaultInterfaceAttribute_t2023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52859_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7396_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52859_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7396_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7396_0_0_0;
extern Il2CppType IEnumerator_1_t7396_1_0_0;
struct IEnumerator_1_t7396;
extern Il2CppGenericClass IEnumerator_1_t7396_GenericClass;
TypeInfo IEnumerator_1_t7396_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7396_MethodInfos/* methods */
	, IEnumerator_1_t7396_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7396_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7396_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7396_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7396_0_0_0/* byval_arg */
	, &IEnumerator_1_t7396_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7396_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_714.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5260_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_714MethodDeclarations.h"

extern TypeInfo ComDefaultInterfaceAttribute_t2023_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31676_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisComDefaultInterfaceAttribute_t2023_m41576_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisComDefaultInterfaceAttribute_t2023_m41576(__this, p0, method) (ComDefaultInterfaceAttribute_t2023 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5260____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5260, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5260____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5260, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5260_FieldInfos[] =
{
	&InternalEnumerator_1_t5260____array_0_FieldInfo,
	&InternalEnumerator_1_t5260____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5260____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5260_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5260____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5260_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5260_PropertyInfos[] =
{
	&InternalEnumerator_1_t5260____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5260____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5260_InternalEnumerator_1__ctor_m31672_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31672_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31672_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5260_InternalEnumerator_1__ctor_m31672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31672_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31674_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31674_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31674_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31675_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31675_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31675_GenericMethod/* genericMethod */

};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31676_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31676_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* declaring_type */
	, &ComDefaultInterfaceAttribute_t2023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31676_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5260_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31672_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_MethodInfo,
	&InternalEnumerator_1_Dispose_m31674_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31675_MethodInfo,
	&InternalEnumerator_1_get_Current_m31676_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31675_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31674_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5260_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31673_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31675_MethodInfo,
	&InternalEnumerator_1_Dispose_m31674_MethodInfo,
	&InternalEnumerator_1_get_Current_m31676_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5260_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7396_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5260_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7396_il2cpp_TypeInfo, 7},
};
extern TypeInfo ComDefaultInterfaceAttribute_t2023_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5260_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31676_MethodInfo/* Method Usage */,
	&ComDefaultInterfaceAttribute_t2023_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisComDefaultInterfaceAttribute_t2023_m41576_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5260_0_0_0;
extern Il2CppType InternalEnumerator_1_t5260_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5260_GenericClass;
TypeInfo InternalEnumerator_1_t5260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5260_MethodInfos/* methods */
	, InternalEnumerator_1_t5260_PropertyInfos/* properties */
	, InternalEnumerator_1_t5260_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5260_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5260_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5260_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5260_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5260_1_0_0/* this_arg */
	, InternalEnumerator_1_t5260_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5260_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5260_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5260)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9504_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern MethodInfo ICollection_1_get_Count_m52860_MethodInfo;
static PropertyInfo ICollection_1_t9504____Count_PropertyInfo = 
{
	&ICollection_1_t9504_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52860_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52861_MethodInfo;
static PropertyInfo ICollection_1_t9504____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9504_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52861_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9504_PropertyInfos[] =
{
	&ICollection_1_t9504____Count_PropertyInfo,
	&ICollection_1_t9504____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52860_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52860_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52860_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52861_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52861_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52861_GenericMethod/* genericMethod */

};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
static ParameterInfo ICollection_1_t9504_ICollection_1_Add_m52862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttribute_t2023_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52862_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52862_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9504_ICollection_1_Add_m52862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52862_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52863_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52863_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52863_GenericMethod/* genericMethod */

};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
static ParameterInfo ICollection_1_t9504_ICollection_1_Contains_m52864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttribute_t2023_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52864_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52864_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9504_ICollection_1_Contains_m52864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52864_GenericMethod/* genericMethod */

};
extern Il2CppType ComDefaultInterfaceAttributeU5BU5D_t5559_0_0_0;
extern Il2CppType ComDefaultInterfaceAttributeU5BU5D_t5559_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9504_ICollection_1_CopyTo_m52865_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttributeU5BU5D_t5559_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52865_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52865_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9504_ICollection_1_CopyTo_m52865_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52865_GenericMethod/* genericMethod */

};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
static ParameterInfo ICollection_1_t9504_ICollection_1_Remove_m52866_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttribute_t2023_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52866_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52866_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9504_ICollection_1_Remove_m52866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52866_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9504_MethodInfos[] =
{
	&ICollection_1_get_Count_m52860_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52861_MethodInfo,
	&ICollection_1_Add_m52862_MethodInfo,
	&ICollection_1_Clear_m52863_MethodInfo,
	&ICollection_1_Contains_m52864_MethodInfo,
	&ICollection_1_CopyTo_m52865_MethodInfo,
	&ICollection_1_Remove_m52866_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9506_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9504_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9506_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9504_0_0_0;
extern Il2CppType ICollection_1_t9504_1_0_0;
struct ICollection_1_t9504;
extern Il2CppGenericClass ICollection_1_t9504_GenericClass;
TypeInfo ICollection_1_t9504_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9504_MethodInfos/* methods */
	, ICollection_1_t9504_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9504_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9504_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9504_0_0_0/* byval_arg */
	, &ICollection_1_t9504_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9504_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern Il2CppType IEnumerator_1_t7396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52867_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52867_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9506_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52867_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9506_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52867_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9506_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9506_0_0_0;
extern Il2CppType IEnumerable_1_t9506_1_0_0;
struct IEnumerable_1_t9506;
extern Il2CppGenericClass IEnumerable_1_t9506_GenericClass;
TypeInfo IEnumerable_1_t9506_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9506_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9506_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9506_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9506_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9506_0_0_0/* byval_arg */
	, &IEnumerable_1_t9506_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9506_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9505_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern MethodInfo IList_1_get_Item_m52868_MethodInfo;
extern MethodInfo IList_1_set_Item_m52869_MethodInfo;
static PropertyInfo IList_1_t9505____Item_PropertyInfo = 
{
	&IList_1_t9505_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52868_MethodInfo/* get */
	, &IList_1_set_Item_m52869_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9505_PropertyInfos[] =
{
	&IList_1_t9505____Item_PropertyInfo,
	NULL
};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
static ParameterInfo IList_1_t9505_IList_1_IndexOf_m52870_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttribute_t2023_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52870_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52870_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9505_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9505_IList_1_IndexOf_m52870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52870_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
static ParameterInfo IList_1_t9505_IList_1_Insert_m52871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttribute_t2023_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52871_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52871_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9505_IList_1_Insert_m52871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52871_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9505_IList_1_RemoveAt_m52872_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52872_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52872_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9505_IList_1_RemoveAt_m52872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52872_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9505_IList_1_get_Item_m52868_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52868_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52868_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9505_il2cpp_TypeInfo/* declaring_type */
	, &ComDefaultInterfaceAttribute_t2023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9505_IList_1_get_Item_m52868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52868_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ComDefaultInterfaceAttribute_t2023_0_0_0;
static ParameterInfo IList_1_t9505_IList_1_set_Item_m52869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ComDefaultInterfaceAttribute_t2023_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52869_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52869_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9505_IList_1_set_Item_m52869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52869_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9505_MethodInfos[] =
{
	&IList_1_IndexOf_m52870_MethodInfo,
	&IList_1_Insert_m52871_MethodInfo,
	&IList_1_RemoveAt_m52872_MethodInfo,
	&IList_1_get_Item_m52868_MethodInfo,
	&IList_1_set_Item_m52869_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9505_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9504_il2cpp_TypeInfo,
	&IEnumerable_1_t9506_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9505_0_0_0;
extern Il2CppType IList_1_t9505_1_0_0;
struct IList_1_t9505;
extern Il2CppGenericClass IList_1_t9505_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9505_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9505_MethodInfos/* methods */
	, IList_1_t9505_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9505_il2cpp_TypeInfo/* element_class */
	, IList_1_t9505_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9505_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9505_0_0_0/* byval_arg */
	, &IList_1_t9505_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9505_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7398_il2cpp_TypeInfo;

// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
extern MethodInfo IEnumerator_1_get_Current_m52873_MethodInfo;
static PropertyInfo IEnumerator_1_t7398____Current_PropertyInfo = 
{
	&IEnumerator_1_t7398_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7398_PropertyInfos[] =
{
	&IEnumerator_1_t7398____Current_PropertyInfo,
	NULL
};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
extern void* RuntimeInvoker_ComInterfaceType_t2024 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52873_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52873_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7398_il2cpp_TypeInfo/* declaring_type */
	, &ComInterfaceType_t2024_0_0_0/* return_type */
	, RuntimeInvoker_ComInterfaceType_t2024/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52873_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7398_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52873_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7398_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7398_0_0_0;
extern Il2CppType IEnumerator_1_t7398_1_0_0;
struct IEnumerator_1_t7398;
extern Il2CppGenericClass IEnumerator_1_t7398_GenericClass;
TypeInfo IEnumerator_1_t7398_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7398_MethodInfos/* methods */
	, IEnumerator_1_t7398_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7398_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7398_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7398_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7398_0_0_0/* byval_arg */
	, &IEnumerator_1_t7398_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7398_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_715.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5261_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_715MethodDeclarations.h"

extern TypeInfo ComInterfaceType_t2024_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31681_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisComInterfaceType_t2024_m41587_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComInterfaceType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComInterfaceType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisComInterfaceType_t2024_m41587 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31677_MethodInfo;
 void InternalEnumerator_1__ctor_m31677 (InternalEnumerator_1_t5261 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678 (InternalEnumerator_1_t5261 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31681(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31681_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ComInterfaceType_t2024_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31679_MethodInfo;
 void InternalEnumerator_1_Dispose_m31679 (InternalEnumerator_1_t5261 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31680_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31680 (InternalEnumerator_1_t5261 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31681 (InternalEnumerator_1_t5261 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisComInterfaceType_t2024_m41587(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisComInterfaceType_t2024_m41587_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5261____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5261, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5261____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5261, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5261_FieldInfos[] =
{
	&InternalEnumerator_1_t5261____array_0_FieldInfo,
	&InternalEnumerator_1_t5261____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5261____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5261_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5261____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5261_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31681_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5261_PropertyInfos[] =
{
	&InternalEnumerator_1_t5261____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5261____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5261_InternalEnumerator_1__ctor_m31677_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31677_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31677_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31677/* method */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5261_InternalEnumerator_1__ctor_m31677_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31677_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678/* method */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31679_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31679_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31679/* method */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31679_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31680_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31680_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31680/* method */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31680_GenericMethod/* genericMethod */

};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
extern void* RuntimeInvoker_ComInterfaceType_t2024 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31681_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31681_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31681/* method */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* declaring_type */
	, &ComInterfaceType_t2024_0_0_0/* return_type */
	, RuntimeInvoker_ComInterfaceType_t2024/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31681_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5261_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31677_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_MethodInfo,
	&InternalEnumerator_1_Dispose_m31679_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31680_MethodInfo,
	&InternalEnumerator_1_get_Current_m31681_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5261_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31680_MethodInfo,
	&InternalEnumerator_1_Dispose_m31679_MethodInfo,
	&InternalEnumerator_1_get_Current_m31681_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5261_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7398_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5261_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7398_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5261_0_0_0;
extern Il2CppType InternalEnumerator_1_t5261_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5261_GenericClass;
TypeInfo InternalEnumerator_1_t5261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5261_MethodInfos/* methods */
	, InternalEnumerator_1_t5261_PropertyInfos/* properties */
	, InternalEnumerator_1_t5261_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5261_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5261_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5261_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5261_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5261_1_0_0/* this_arg */
	, InternalEnumerator_1_t5261_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5261_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5261)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9507_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>
extern MethodInfo ICollection_1_get_Count_m52874_MethodInfo;
static PropertyInfo ICollection_1_t9507____Count_PropertyInfo = 
{
	&ICollection_1_t9507_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52874_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52875_MethodInfo;
static PropertyInfo ICollection_1_t9507____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9507_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52875_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9507_PropertyInfos[] =
{
	&ICollection_1_t9507____Count_PropertyInfo,
	&ICollection_1_t9507____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52874_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::get_Count()
MethodInfo ICollection_1_get_Count_m52874_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52874_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52875_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52875_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52875_GenericMethod/* genericMethod */

};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
extern Il2CppType ComInterfaceType_t2024_0_0_0;
static ParameterInfo ICollection_1_t9507_ICollection_1_Add_m52876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComInterfaceType_t2024_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52876_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Add(T)
MethodInfo ICollection_1_Add_m52876_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9507_ICollection_1_Add_m52876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52876_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52877_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Clear()
MethodInfo ICollection_1_Clear_m52877_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52877_GenericMethod/* genericMethod */

};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
static ParameterInfo ICollection_1_t9507_ICollection_1_Contains_m52878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComInterfaceType_t2024_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52878_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Contains(T)
MethodInfo ICollection_1_Contains_m52878_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9507_ICollection_1_Contains_m52878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52878_GenericMethod/* genericMethod */

};
extern Il2CppType ComInterfaceTypeU5BU5D_t5560_0_0_0;
extern Il2CppType ComInterfaceTypeU5BU5D_t5560_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9507_ICollection_1_CopyTo_m52879_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ComInterfaceTypeU5BU5D_t5560_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52879_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52879_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9507_ICollection_1_CopyTo_m52879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52879_GenericMethod/* genericMethod */

};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
static ParameterInfo ICollection_1_t9507_ICollection_1_Remove_m52880_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComInterfaceType_t2024_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52880_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>::Remove(T)
MethodInfo ICollection_1_Remove_m52880_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9507_ICollection_1_Remove_m52880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52880_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9507_MethodInfos[] =
{
	&ICollection_1_get_Count_m52874_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52875_MethodInfo,
	&ICollection_1_Add_m52876_MethodInfo,
	&ICollection_1_Clear_m52877_MethodInfo,
	&ICollection_1_Contains_m52878_MethodInfo,
	&ICollection_1_CopyTo_m52879_MethodInfo,
	&ICollection_1_Remove_m52880_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9509_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9507_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9509_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9507_0_0_0;
extern Il2CppType ICollection_1_t9507_1_0_0;
struct ICollection_1_t9507;
extern Il2CppGenericClass ICollection_1_t9507_GenericClass;
TypeInfo ICollection_1_t9507_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9507_MethodInfos/* methods */
	, ICollection_1_t9507_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9507_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9507_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9507_0_0_0/* byval_arg */
	, &ICollection_1_t9507_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9507_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComInterfaceType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComInterfaceType>
extern Il2CppType IEnumerator_1_t7398_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52881_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComInterfaceType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52881_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9509_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7398_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52881_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9509_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52881_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9509_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9509_0_0_0;
extern Il2CppType IEnumerable_1_t9509_1_0_0;
struct IEnumerable_1_t9509;
extern Il2CppGenericClass IEnumerable_1_t9509_GenericClass;
TypeInfo IEnumerable_1_t9509_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9509_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9509_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9509_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9509_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9509_0_0_0/* byval_arg */
	, &IEnumerable_1_t9509_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9509_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9508_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>
extern MethodInfo IList_1_get_Item_m52882_MethodInfo;
extern MethodInfo IList_1_set_Item_m52883_MethodInfo;
static PropertyInfo IList_1_t9508____Item_PropertyInfo = 
{
	&IList_1_t9508_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52882_MethodInfo/* get */
	, &IList_1_set_Item_m52883_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9508_PropertyInfos[] =
{
	&IList_1_t9508____Item_PropertyInfo,
	NULL
};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
static ParameterInfo IList_1_t9508_IList_1_IndexOf_m52884_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComInterfaceType_t2024_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52884_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52884_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9508_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9508_IList_1_IndexOf_m52884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52884_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ComInterfaceType_t2024_0_0_0;
static ParameterInfo IList_1_t9508_IList_1_Insert_m52885_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ComInterfaceType_t2024_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52885_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52885_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9508_IList_1_Insert_m52885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52885_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9508_IList_1_RemoveAt_m52886_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52886_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52886_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9508_IList_1_RemoveAt_m52886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52886_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9508_IList_1_get_Item_m52882_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ComInterfaceType_t2024_0_0_0;
extern void* RuntimeInvoker_ComInterfaceType_t2024_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52882_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52882_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9508_il2cpp_TypeInfo/* declaring_type */
	, &ComInterfaceType_t2024_0_0_0/* return_type */
	, RuntimeInvoker_ComInterfaceType_t2024_Int32_t123/* invoker_method */
	, IList_1_t9508_IList_1_get_Item_m52882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52882_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ComInterfaceType_t2024_0_0_0;
static ParameterInfo IList_1_t9508_IList_1_set_Item_m52883_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ComInterfaceType_t2024_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52883_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52883_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9508_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9508_IList_1_set_Item_m52883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52883_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9508_MethodInfos[] =
{
	&IList_1_IndexOf_m52884_MethodInfo,
	&IList_1_Insert_m52885_MethodInfo,
	&IList_1_RemoveAt_m52886_MethodInfo,
	&IList_1_get_Item_m52882_MethodInfo,
	&IList_1_set_Item_m52883_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9508_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9507_il2cpp_TypeInfo,
	&IEnumerable_1_t9509_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9508_0_0_0;
extern Il2CppType IList_1_t9508_1_0_0;
struct IList_1_t9508;
extern Il2CppGenericClass IList_1_t9508_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9508_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9508_MethodInfos/* methods */
	, IList_1_t9508_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9508_il2cpp_TypeInfo/* element_class */
	, IList_1_t9508_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9508_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9508_0_0_0/* byval_arg */
	, &IList_1_t9508_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9508_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7400_il2cpp_TypeInfo;

// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52887_MethodInfo;
static PropertyInfo IEnumerator_1_t7400____Current_PropertyInfo = 
{
	&IEnumerator_1_t7400_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52887_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7400_PropertyInfos[] =
{
	&IEnumerator_1_t7400____Current_PropertyInfo,
	NULL
};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52887_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52887_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7400_il2cpp_TypeInfo/* declaring_type */
	, &DispIdAttribute_t2025_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52887_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7400_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52887_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7400_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7400_0_0_0;
extern Il2CppType IEnumerator_1_t7400_1_0_0;
struct IEnumerator_1_t7400;
extern Il2CppGenericClass IEnumerator_1_t7400_GenericClass;
TypeInfo IEnumerator_1_t7400_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7400_MethodInfos/* methods */
	, IEnumerator_1_t7400_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7400_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7400_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7400_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7400_0_0_0/* byval_arg */
	, &IEnumerator_1_t7400_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7400_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_716.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5262_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_716MethodDeclarations.h"

extern TypeInfo DispIdAttribute_t2025_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31686_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDispIdAttribute_t2025_m41598_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.DispIdAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.DispIdAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisDispIdAttribute_t2025_m41598(__this, p0, method) (DispIdAttribute_t2025 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5262____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5262, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5262____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5262, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5262_FieldInfos[] =
{
	&InternalEnumerator_1_t5262____array_0_FieldInfo,
	&InternalEnumerator_1_t5262____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5262____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5262_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5262____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5262_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31686_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5262_PropertyInfos[] =
{
	&InternalEnumerator_1_t5262____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5262____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5262_InternalEnumerator_1__ctor_m31682_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31682_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31682_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5262_InternalEnumerator_1__ctor_m31682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31682_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31684_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31684_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31684_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31685_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31685_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31685_GenericMethod/* genericMethod */

};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31686_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31686_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* declaring_type */
	, &DispIdAttribute_t2025_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31686_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5262_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31682_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_MethodInfo,
	&InternalEnumerator_1_Dispose_m31684_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31685_MethodInfo,
	&InternalEnumerator_1_get_Current_m31686_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31685_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31684_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5262_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31683_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31685_MethodInfo,
	&InternalEnumerator_1_Dispose_m31684_MethodInfo,
	&InternalEnumerator_1_get_Current_m31686_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5262_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7400_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5262_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7400_il2cpp_TypeInfo, 7},
};
extern TypeInfo DispIdAttribute_t2025_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5262_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31686_MethodInfo/* Method Usage */,
	&DispIdAttribute_t2025_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDispIdAttribute_t2025_m41598_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5262_0_0_0;
extern Il2CppType InternalEnumerator_1_t5262_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5262_GenericClass;
TypeInfo InternalEnumerator_1_t5262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5262_MethodInfos/* methods */
	, InternalEnumerator_1_t5262_PropertyInfos/* properties */
	, InternalEnumerator_1_t5262_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5262_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5262_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5262_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5262_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5262_1_0_0/* this_arg */
	, InternalEnumerator_1_t5262_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5262_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5262_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5262)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9510_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>
extern MethodInfo ICollection_1_get_Count_m52888_MethodInfo;
static PropertyInfo ICollection_1_t9510____Count_PropertyInfo = 
{
	&ICollection_1_t9510_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52888_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52889_MethodInfo;
static PropertyInfo ICollection_1_t9510____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9510_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52889_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9510_PropertyInfos[] =
{
	&ICollection_1_t9510____Count_PropertyInfo,
	&ICollection_1_t9510____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52888_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52888_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52888_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52889_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52889_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52889_GenericMethod/* genericMethod */

};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
extern Il2CppType DispIdAttribute_t2025_0_0_0;
static ParameterInfo ICollection_1_t9510_ICollection_1_Add_m52890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DispIdAttribute_t2025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52890_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52890_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9510_ICollection_1_Add_m52890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52890_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52891_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52891_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52891_GenericMethod/* genericMethod */

};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
static ParameterInfo ICollection_1_t9510_ICollection_1_Contains_m52892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DispIdAttribute_t2025_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52892_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52892_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9510_ICollection_1_Contains_m52892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52892_GenericMethod/* genericMethod */

};
extern Il2CppType DispIdAttributeU5BU5D_t5561_0_0_0;
extern Il2CppType DispIdAttributeU5BU5D_t5561_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9510_ICollection_1_CopyTo_m52893_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DispIdAttributeU5BU5D_t5561_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52893_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52893_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9510_ICollection_1_CopyTo_m52893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52893_GenericMethod/* genericMethod */

};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
static ParameterInfo ICollection_1_t9510_ICollection_1_Remove_m52894_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DispIdAttribute_t2025_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52894_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52894_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9510_ICollection_1_Remove_m52894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52894_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9510_MethodInfos[] =
{
	&ICollection_1_get_Count_m52888_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52889_MethodInfo,
	&ICollection_1_Add_m52890_MethodInfo,
	&ICollection_1_Clear_m52891_MethodInfo,
	&ICollection_1_Contains_m52892_MethodInfo,
	&ICollection_1_CopyTo_m52893_MethodInfo,
	&ICollection_1_Remove_m52894_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9512_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9510_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9512_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9510_0_0_0;
extern Il2CppType ICollection_1_t9510_1_0_0;
struct ICollection_1_t9510;
extern Il2CppGenericClass ICollection_1_t9510_GenericClass;
TypeInfo ICollection_1_t9510_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9510_MethodInfos/* methods */
	, ICollection_1_t9510_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9510_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9510_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9510_0_0_0/* byval_arg */
	, &ICollection_1_t9510_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9510_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.DispIdAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.DispIdAttribute>
extern Il2CppType IEnumerator_1_t7400_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52895_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.DispIdAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52895_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9512_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7400_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52895_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9512_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52895_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9512_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9512_0_0_0;
extern Il2CppType IEnumerable_1_t9512_1_0_0;
struct IEnumerable_1_t9512;
extern Il2CppGenericClass IEnumerable_1_t9512_GenericClass;
TypeInfo IEnumerable_1_t9512_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9512_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9512_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9512_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9512_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9512_0_0_0/* byval_arg */
	, &IEnumerable_1_t9512_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9512_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9511_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>
extern MethodInfo IList_1_get_Item_m52896_MethodInfo;
extern MethodInfo IList_1_set_Item_m52897_MethodInfo;
static PropertyInfo IList_1_t9511____Item_PropertyInfo = 
{
	&IList_1_t9511_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52896_MethodInfo/* get */
	, &IList_1_set_Item_m52897_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9511_PropertyInfos[] =
{
	&IList_1_t9511____Item_PropertyInfo,
	NULL
};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
static ParameterInfo IList_1_t9511_IList_1_IndexOf_m52898_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DispIdAttribute_t2025_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52898_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52898_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9511_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9511_IList_1_IndexOf_m52898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52898_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DispIdAttribute_t2025_0_0_0;
static ParameterInfo IList_1_t9511_IList_1_Insert_m52899_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DispIdAttribute_t2025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52899_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52899_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9511_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9511_IList_1_Insert_m52899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52899_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9511_IList_1_RemoveAt_m52900_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52900_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52900_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9511_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9511_IList_1_RemoveAt_m52900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52900_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9511_IList_1_get_Item_m52896_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DispIdAttribute_t2025_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52896_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52896_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9511_il2cpp_TypeInfo/* declaring_type */
	, &DispIdAttribute_t2025_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9511_IList_1_get_Item_m52896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52896_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DispIdAttribute_t2025_0_0_0;
static ParameterInfo IList_1_t9511_IList_1_set_Item_m52897_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DispIdAttribute_t2025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52897_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52897_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9511_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9511_IList_1_set_Item_m52897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52897_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9511_MethodInfos[] =
{
	&IList_1_IndexOf_m52898_MethodInfo,
	&IList_1_Insert_m52899_MethodInfo,
	&IList_1_RemoveAt_m52900_MethodInfo,
	&IList_1_get_Item_m52896_MethodInfo,
	&IList_1_set_Item_m52897_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9511_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9510_il2cpp_TypeInfo,
	&IEnumerable_1_t9512_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9511_0_0_0;
extern Il2CppType IList_1_t9511_1_0_0;
struct IList_1_t9511;
extern Il2CppGenericClass IList_1_t9511_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9511_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9511_MethodInfos/* methods */
	, IList_1_t9511_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9511_il2cpp_TypeInfo/* element_class */
	, IList_1_t9511_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9511_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9511_0_0_0/* byval_arg */
	, &IList_1_t9511_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9511_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7402_il2cpp_TypeInfo;

// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.GCHandleType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.GCHandleType>
extern MethodInfo IEnumerator_1_get_Current_m52901_MethodInfo;
static PropertyInfo IEnumerator_1_t7402____Current_PropertyInfo = 
{
	&IEnumerator_1_t7402_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7402_PropertyInfos[] =
{
	&IEnumerator_1_t7402____Current_PropertyInfo,
	NULL
};
extern Il2CppType GCHandleType_t2026_0_0_0;
extern void* RuntimeInvoker_GCHandleType_t2026 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52901_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.GCHandleType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52901_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7402_il2cpp_TypeInfo/* declaring_type */
	, &GCHandleType_t2026_0_0_0/* return_type */
	, RuntimeInvoker_GCHandleType_t2026/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52901_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7402_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52901_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7402_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7402_0_0_0;
extern Il2CppType IEnumerator_1_t7402_1_0_0;
struct IEnumerator_1_t7402;
extern Il2CppGenericClass IEnumerator_1_t7402_GenericClass;
TypeInfo IEnumerator_1_t7402_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7402_MethodInfos/* methods */
	, IEnumerator_1_t7402_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7402_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7402_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7402_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7402_0_0_0/* byval_arg */
	, &IEnumerator_1_t7402_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7402_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_717.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5263_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_717MethodDeclarations.h"

extern TypeInfo GCHandleType_t2026_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31691_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGCHandleType_t2026_m41609_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.GCHandleType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.GCHandleType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisGCHandleType_t2026_m41609 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31687_MethodInfo;
 void InternalEnumerator_1__ctor_m31687 (InternalEnumerator_1_t5263 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688 (InternalEnumerator_1_t5263 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31691(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31691_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&GCHandleType_t2026_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31689_MethodInfo;
 void InternalEnumerator_1_Dispose_m31689 (InternalEnumerator_1_t5263 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31690_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31690 (InternalEnumerator_1_t5263 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31691 (InternalEnumerator_1_t5263 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisGCHandleType_t2026_m41609(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisGCHandleType_t2026_m41609_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5263____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5263, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5263____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5263, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5263_FieldInfos[] =
{
	&InternalEnumerator_1_t5263____array_0_FieldInfo,
	&InternalEnumerator_1_t5263____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5263____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5263_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5263____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5263_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31691_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5263_PropertyInfos[] =
{
	&InternalEnumerator_1_t5263____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5263____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5263_InternalEnumerator_1__ctor_m31687_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31687_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31687_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31687/* method */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5263_InternalEnumerator_1__ctor_m31687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31687_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688/* method */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31689_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31689_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31689/* method */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31689_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31690_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31690_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31690/* method */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31690_GenericMethod/* genericMethod */

};
extern Il2CppType GCHandleType_t2026_0_0_0;
extern void* RuntimeInvoker_GCHandleType_t2026 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31691_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31691_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31691/* method */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* declaring_type */
	, &GCHandleType_t2026_0_0_0/* return_type */
	, RuntimeInvoker_GCHandleType_t2026/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31691_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5263_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31687_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_MethodInfo,
	&InternalEnumerator_1_Dispose_m31689_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31690_MethodInfo,
	&InternalEnumerator_1_get_Current_m31691_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5263_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31688_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31690_MethodInfo,
	&InternalEnumerator_1_Dispose_m31689_MethodInfo,
	&InternalEnumerator_1_get_Current_m31691_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5263_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7402_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5263_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7402_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5263_0_0_0;
extern Il2CppType InternalEnumerator_1_t5263_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5263_GenericClass;
TypeInfo InternalEnumerator_1_t5263_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5263_MethodInfos/* methods */
	, InternalEnumerator_1_t5263_PropertyInfos/* properties */
	, InternalEnumerator_1_t5263_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5263_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5263_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5263_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5263_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5263_1_0_0/* this_arg */
	, InternalEnumerator_1_t5263_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5263_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5263)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9513_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>
extern MethodInfo ICollection_1_get_Count_m52902_MethodInfo;
static PropertyInfo ICollection_1_t9513____Count_PropertyInfo = 
{
	&ICollection_1_t9513_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52902_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52903_MethodInfo;
static PropertyInfo ICollection_1_t9513____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9513_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52903_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9513_PropertyInfos[] =
{
	&ICollection_1_t9513____Count_PropertyInfo,
	&ICollection_1_t9513____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52902_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::get_Count()
MethodInfo ICollection_1_get_Count_m52902_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52902_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52903_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52903_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52903_GenericMethod/* genericMethod */

};
extern Il2CppType GCHandleType_t2026_0_0_0;
extern Il2CppType GCHandleType_t2026_0_0_0;
static ParameterInfo ICollection_1_t9513_ICollection_1_Add_m52904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GCHandleType_t2026_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52904_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Add(T)
MethodInfo ICollection_1_Add_m52904_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9513_ICollection_1_Add_m52904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52904_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52905_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Clear()
MethodInfo ICollection_1_Clear_m52905_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52905_GenericMethod/* genericMethod */

};
extern Il2CppType GCHandleType_t2026_0_0_0;
static ParameterInfo ICollection_1_t9513_ICollection_1_Contains_m52906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GCHandleType_t2026_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52906_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Contains(T)
MethodInfo ICollection_1_Contains_m52906_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9513_ICollection_1_Contains_m52906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52906_GenericMethod/* genericMethod */

};
extern Il2CppType GCHandleTypeU5BU5D_t5562_0_0_0;
extern Il2CppType GCHandleTypeU5BU5D_t5562_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9513_ICollection_1_CopyTo_m52907_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GCHandleTypeU5BU5D_t5562_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52907_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52907_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9513_ICollection_1_CopyTo_m52907_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52907_GenericMethod/* genericMethod */

};
extern Il2CppType GCHandleType_t2026_0_0_0;
static ParameterInfo ICollection_1_t9513_ICollection_1_Remove_m52908_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GCHandleType_t2026_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52908_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>::Remove(T)
MethodInfo ICollection_1_Remove_m52908_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9513_ICollection_1_Remove_m52908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52908_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9513_MethodInfos[] =
{
	&ICollection_1_get_Count_m52902_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52903_MethodInfo,
	&ICollection_1_Add_m52904_MethodInfo,
	&ICollection_1_Clear_m52905_MethodInfo,
	&ICollection_1_Contains_m52906_MethodInfo,
	&ICollection_1_CopyTo_m52907_MethodInfo,
	&ICollection_1_Remove_m52908_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9515_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9513_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9515_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9513_0_0_0;
extern Il2CppType ICollection_1_t9513_1_0_0;
struct ICollection_1_t9513;
extern Il2CppGenericClass ICollection_1_t9513_GenericClass;
TypeInfo ICollection_1_t9513_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9513_MethodInfos/* methods */
	, ICollection_1_t9513_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9513_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9513_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9513_0_0_0/* byval_arg */
	, &ICollection_1_t9513_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9513_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.GCHandleType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.GCHandleType>
extern Il2CppType IEnumerator_1_t7402_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52909_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.GCHandleType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52909_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9515_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7402_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52909_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9515_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52909_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9515_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9515_0_0_0;
extern Il2CppType IEnumerable_1_t9515_1_0_0;
struct IEnumerable_1_t9515;
extern Il2CppGenericClass IEnumerable_1_t9515_GenericClass;
TypeInfo IEnumerable_1_t9515_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9515_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9515_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9515_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9515_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9515_0_0_0/* byval_arg */
	, &IEnumerable_1_t9515_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9515_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9514_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>
extern MethodInfo IList_1_get_Item_m52910_MethodInfo;
extern MethodInfo IList_1_set_Item_m52911_MethodInfo;
static PropertyInfo IList_1_t9514____Item_PropertyInfo = 
{
	&IList_1_t9514_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52910_MethodInfo/* get */
	, &IList_1_set_Item_m52911_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9514_PropertyInfos[] =
{
	&IList_1_t9514____Item_PropertyInfo,
	NULL
};
extern Il2CppType GCHandleType_t2026_0_0_0;
static ParameterInfo IList_1_t9514_IList_1_IndexOf_m52912_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GCHandleType_t2026_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52912_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52912_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9514_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9514_IList_1_IndexOf_m52912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52912_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GCHandleType_t2026_0_0_0;
static ParameterInfo IList_1_t9514_IList_1_Insert_m52913_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GCHandleType_t2026_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52913_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52913_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9514_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9514_IList_1_Insert_m52913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52913_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9514_IList_1_RemoveAt_m52914_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52914_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52914_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9514_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9514_IList_1_RemoveAt_m52914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52914_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9514_IList_1_get_Item_m52910_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GCHandleType_t2026_0_0_0;
extern void* RuntimeInvoker_GCHandleType_t2026_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52910_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52910_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9514_il2cpp_TypeInfo/* declaring_type */
	, &GCHandleType_t2026_0_0_0/* return_type */
	, RuntimeInvoker_GCHandleType_t2026_Int32_t123/* invoker_method */
	, IList_1_t9514_IList_1_get_Item_m52910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52910_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GCHandleType_t2026_0_0_0;
static ParameterInfo IList_1_t9514_IList_1_set_Item_m52911_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GCHandleType_t2026_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52911_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52911_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9514_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9514_IList_1_set_Item_m52911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52911_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9514_MethodInfos[] =
{
	&IList_1_IndexOf_m52912_MethodInfo,
	&IList_1_Insert_m52913_MethodInfo,
	&IList_1_RemoveAt_m52914_MethodInfo,
	&IList_1_get_Item_m52910_MethodInfo,
	&IList_1_set_Item_m52911_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9514_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9513_il2cpp_TypeInfo,
	&IEnumerable_1_t9515_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9514_0_0_0;
extern Il2CppType IList_1_t9514_1_0_0;
struct IList_1_t9514;
extern Il2CppGenericClass IList_1_t9514_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9514_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9514_MethodInfos/* methods */
	, IList_1_t9514_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9514_il2cpp_TypeInfo/* element_class */
	, IList_1_t9514_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9514_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9514_0_0_0/* byval_arg */
	, &IList_1_t9514_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9514_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7404_il2cpp_TypeInfo;

// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52915_MethodInfo;
static PropertyInfo IEnumerator_1_t7404____Current_PropertyInfo = 
{
	&IEnumerator_1_t7404_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52915_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7404_PropertyInfos[] =
{
	&IEnumerator_1_t7404____Current_PropertyInfo,
	NULL
};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52915_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52915_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7404_il2cpp_TypeInfo/* declaring_type */
	, &InterfaceTypeAttribute_t2027_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52915_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7404_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52915_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7404_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7404_0_0_0;
extern Il2CppType IEnumerator_1_t7404_1_0_0;
struct IEnumerator_1_t7404;
extern Il2CppGenericClass IEnumerator_1_t7404_GenericClass;
TypeInfo IEnumerator_1_t7404_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7404_MethodInfos/* methods */
	, IEnumerator_1_t7404_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7404_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7404_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7404_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7404_0_0_0/* byval_arg */
	, &IEnumerator_1_t7404_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7404_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_718.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5264_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_718MethodDeclarations.h"

extern TypeInfo InterfaceTypeAttribute_t2027_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31696_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInterfaceTypeAttribute_t2027_m41620_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisInterfaceTypeAttribute_t2027_m41620(__this, p0, method) (InterfaceTypeAttribute_t2027 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5264____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5264, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5264____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5264, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5264_FieldInfos[] =
{
	&InternalEnumerator_1_t5264____array_0_FieldInfo,
	&InternalEnumerator_1_t5264____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5264____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5264_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5264____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5264_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31696_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5264_PropertyInfos[] =
{
	&InternalEnumerator_1_t5264____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5264____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5264_InternalEnumerator_1__ctor_m31692_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31692_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31692_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5264_InternalEnumerator_1__ctor_m31692_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31692_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31694_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31694_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31694_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31695_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31695_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31695_GenericMethod/* genericMethod */

};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31696_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31696_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* declaring_type */
	, &InterfaceTypeAttribute_t2027_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31696_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5264_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31692_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_MethodInfo,
	&InternalEnumerator_1_Dispose_m31694_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31695_MethodInfo,
	&InternalEnumerator_1_get_Current_m31696_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31695_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31694_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5264_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31693_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31695_MethodInfo,
	&InternalEnumerator_1_Dispose_m31694_MethodInfo,
	&InternalEnumerator_1_get_Current_m31696_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5264_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7404_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5264_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7404_il2cpp_TypeInfo, 7},
};
extern TypeInfo InterfaceTypeAttribute_t2027_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5264_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31696_MethodInfo/* Method Usage */,
	&InterfaceTypeAttribute_t2027_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisInterfaceTypeAttribute_t2027_m41620_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5264_0_0_0;
extern Il2CppType InternalEnumerator_1_t5264_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5264_GenericClass;
TypeInfo InternalEnumerator_1_t5264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5264_MethodInfos/* methods */
	, InternalEnumerator_1_t5264_PropertyInfos/* properties */
	, InternalEnumerator_1_t5264_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5264_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5264_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5264_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5264_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5264_1_0_0/* this_arg */
	, InternalEnumerator_1_t5264_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5264_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5264_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5264)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9516_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern MethodInfo ICollection_1_get_Count_m52916_MethodInfo;
static PropertyInfo ICollection_1_t9516____Count_PropertyInfo = 
{
	&ICollection_1_t9516_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52916_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52917_MethodInfo;
static PropertyInfo ICollection_1_t9516____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9516_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52917_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9516_PropertyInfos[] =
{
	&ICollection_1_t9516____Count_PropertyInfo,
	&ICollection_1_t9516____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52916_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52916_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52916_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52917_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52917_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52917_GenericMethod/* genericMethod */

};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
static ParameterInfo ICollection_1_t9516_ICollection_1_Add_m52918_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttribute_t2027_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52918_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52918_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9516_ICollection_1_Add_m52918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52918_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52919_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52919_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52919_GenericMethod/* genericMethod */

};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
static ParameterInfo ICollection_1_t9516_ICollection_1_Contains_m52920_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttribute_t2027_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52920_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52920_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9516_ICollection_1_Contains_m52920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52920_GenericMethod/* genericMethod */

};
extern Il2CppType InterfaceTypeAttributeU5BU5D_t5563_0_0_0;
extern Il2CppType InterfaceTypeAttributeU5BU5D_t5563_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9516_ICollection_1_CopyTo_m52921_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttributeU5BU5D_t5563_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52921_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52921_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9516_ICollection_1_CopyTo_m52921_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52921_GenericMethod/* genericMethod */

};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
static ParameterInfo ICollection_1_t9516_ICollection_1_Remove_m52922_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttribute_t2027_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52922_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52922_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9516_ICollection_1_Remove_m52922_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52922_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9516_MethodInfos[] =
{
	&ICollection_1_get_Count_m52916_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52917_MethodInfo,
	&ICollection_1_Add_m52918_MethodInfo,
	&ICollection_1_Clear_m52919_MethodInfo,
	&ICollection_1_Contains_m52920_MethodInfo,
	&ICollection_1_CopyTo_m52921_MethodInfo,
	&ICollection_1_Remove_m52922_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9518_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9516_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9518_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9516_0_0_0;
extern Il2CppType ICollection_1_t9516_1_0_0;
struct ICollection_1_t9516;
extern Il2CppGenericClass ICollection_1_t9516_GenericClass;
TypeInfo ICollection_1_t9516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9516_MethodInfos/* methods */
	, ICollection_1_t9516_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9516_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9516_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9516_0_0_0/* byval_arg */
	, &ICollection_1_t9516_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9516_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern Il2CppType IEnumerator_1_t7404_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52923_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52923_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9518_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7404_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52923_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9518_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52923_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9518_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9518_0_0_0;
extern Il2CppType IEnumerable_1_t9518_1_0_0;
struct IEnumerable_1_t9518;
extern Il2CppGenericClass IEnumerable_1_t9518_GenericClass;
TypeInfo IEnumerable_1_t9518_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9518_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9518_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9518_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9518_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9518_0_0_0/* byval_arg */
	, &IEnumerable_1_t9518_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9518_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9517_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern MethodInfo IList_1_get_Item_m52924_MethodInfo;
extern MethodInfo IList_1_set_Item_m52925_MethodInfo;
static PropertyInfo IList_1_t9517____Item_PropertyInfo = 
{
	&IList_1_t9517_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52924_MethodInfo/* get */
	, &IList_1_set_Item_m52925_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9517_PropertyInfos[] =
{
	&IList_1_t9517____Item_PropertyInfo,
	NULL
};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
static ParameterInfo IList_1_t9517_IList_1_IndexOf_m52926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttribute_t2027_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52926_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52926_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9517_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9517_IList_1_IndexOf_m52926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52926_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
static ParameterInfo IList_1_t9517_IList_1_Insert_m52927_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttribute_t2027_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52927_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52927_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9517_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9517_IList_1_Insert_m52927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52927_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9517_IList_1_RemoveAt_m52928_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52928_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52928_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9517_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9517_IList_1_RemoveAt_m52928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52928_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9517_IList_1_get_Item_m52924_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52924_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52924_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9517_il2cpp_TypeInfo/* declaring_type */
	, &InterfaceTypeAttribute_t2027_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9517_IList_1_get_Item_m52924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52924_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType InterfaceTypeAttribute_t2027_0_0_0;
static ParameterInfo IList_1_t9517_IList_1_set_Item_m52925_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &InterfaceTypeAttribute_t2027_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52925_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52925_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9517_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9517_IList_1_set_Item_m52925_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52925_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9517_MethodInfos[] =
{
	&IList_1_IndexOf_m52926_MethodInfo,
	&IList_1_Insert_m52927_MethodInfo,
	&IList_1_RemoveAt_m52928_MethodInfo,
	&IList_1_get_Item_m52924_MethodInfo,
	&IList_1_set_Item_m52925_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9517_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9516_il2cpp_TypeInfo,
	&IEnumerable_1_t9518_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9517_0_0_0;
extern Il2CppType IList_1_t9517_1_0_0;
struct IList_1_t9517;
extern Il2CppGenericClass IList_1_t9517_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9517_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9517_MethodInfos/* methods */
	, IList_1_t9517_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9517_il2cpp_TypeInfo/* element_class */
	, IList_1_t9517_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9517_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9517_0_0_0/* byval_arg */
	, &IList_1_t9517_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9517_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7406_il2cpp_TypeInfo;

// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52929_MethodInfo;
static PropertyInfo IEnumerator_1_t7406____Current_PropertyInfo = 
{
	&IEnumerator_1_t7406_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7406_PropertyInfos[] =
{
	&IEnumerator_1_t7406____Current_PropertyInfo,
	NULL
};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52929_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52929_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7406_il2cpp_TypeInfo/* declaring_type */
	, &PreserveSigAttribute_t2029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52929_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7406_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52929_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7406_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7406_0_0_0;
extern Il2CppType IEnumerator_1_t7406_1_0_0;
struct IEnumerator_1_t7406;
extern Il2CppGenericClass IEnumerator_1_t7406_GenericClass;
TypeInfo IEnumerator_1_t7406_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7406_MethodInfos/* methods */
	, IEnumerator_1_t7406_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7406_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7406_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7406_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7406_0_0_0/* byval_arg */
	, &IEnumerator_1_t7406_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7406_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_719.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5265_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_719MethodDeclarations.h"

extern TypeInfo PreserveSigAttribute_t2029_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31701_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPreserveSigAttribute_t2029_m41631_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisPreserveSigAttribute_t2029_m41631(__this, p0, method) (PreserveSigAttribute_t2029 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5265____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5265, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5265____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5265, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5265_FieldInfos[] =
{
	&InternalEnumerator_1_t5265____array_0_FieldInfo,
	&InternalEnumerator_1_t5265____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5265____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5265_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5265____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5265_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31701_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5265_PropertyInfos[] =
{
	&InternalEnumerator_1_t5265____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5265____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5265_InternalEnumerator_1__ctor_m31697_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31697_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31697_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5265_InternalEnumerator_1__ctor_m31697_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31697_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31699_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31699_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31699_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31700_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31700_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31700_GenericMethod/* genericMethod */

};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31701_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31701_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* declaring_type */
	, &PreserveSigAttribute_t2029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31701_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5265_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31697_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_MethodInfo,
	&InternalEnumerator_1_Dispose_m31699_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31700_MethodInfo,
	&InternalEnumerator_1_get_Current_m31701_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31700_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31699_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5265_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31700_MethodInfo,
	&InternalEnumerator_1_Dispose_m31699_MethodInfo,
	&InternalEnumerator_1_get_Current_m31701_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5265_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7406_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5265_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7406_il2cpp_TypeInfo, 7},
};
extern TypeInfo PreserveSigAttribute_t2029_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5265_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31701_MethodInfo/* Method Usage */,
	&PreserveSigAttribute_t2029_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPreserveSigAttribute_t2029_m41631_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5265_0_0_0;
extern Il2CppType InternalEnumerator_1_t5265_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5265_GenericClass;
TypeInfo InternalEnumerator_1_t5265_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5265_MethodInfos/* methods */
	, InternalEnumerator_1_t5265_PropertyInfos/* properties */
	, InternalEnumerator_1_t5265_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5265_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5265_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5265_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5265_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5265_1_0_0/* this_arg */
	, InternalEnumerator_1_t5265_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5265_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5265_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5265)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9519_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern MethodInfo ICollection_1_get_Count_m52930_MethodInfo;
static PropertyInfo ICollection_1_t9519____Count_PropertyInfo = 
{
	&ICollection_1_t9519_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52930_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52931_MethodInfo;
static PropertyInfo ICollection_1_t9519____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9519_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52931_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9519_PropertyInfos[] =
{
	&ICollection_1_t9519____Count_PropertyInfo,
	&ICollection_1_t9519____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52930_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52930_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52930_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52931_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52931_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52931_GenericMethod/* genericMethod */

};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
static ParameterInfo ICollection_1_t9519_ICollection_1_Add_m52932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttribute_t2029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52932_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52932_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9519_ICollection_1_Add_m52932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52932_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52933_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52933_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52933_GenericMethod/* genericMethod */

};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
static ParameterInfo ICollection_1_t9519_ICollection_1_Contains_m52934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttribute_t2029_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52934_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52934_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9519_ICollection_1_Contains_m52934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52934_GenericMethod/* genericMethod */

};
extern Il2CppType PreserveSigAttributeU5BU5D_t5564_0_0_0;
extern Il2CppType PreserveSigAttributeU5BU5D_t5564_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9519_ICollection_1_CopyTo_m52935_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttributeU5BU5D_t5564_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52935_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52935_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9519_ICollection_1_CopyTo_m52935_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52935_GenericMethod/* genericMethod */

};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
static ParameterInfo ICollection_1_t9519_ICollection_1_Remove_m52936_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttribute_t2029_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52936_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52936_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9519_ICollection_1_Remove_m52936_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52936_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9519_MethodInfos[] =
{
	&ICollection_1_get_Count_m52930_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52931_MethodInfo,
	&ICollection_1_Add_m52932_MethodInfo,
	&ICollection_1_Clear_m52933_MethodInfo,
	&ICollection_1_Contains_m52934_MethodInfo,
	&ICollection_1_CopyTo_m52935_MethodInfo,
	&ICollection_1_Remove_m52936_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9521_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9519_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9521_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9519_0_0_0;
extern Il2CppType ICollection_1_t9519_1_0_0;
struct ICollection_1_t9519;
extern Il2CppGenericClass ICollection_1_t9519_GenericClass;
TypeInfo ICollection_1_t9519_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9519_MethodInfos/* methods */
	, ICollection_1_t9519_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9519_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9519_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9519_0_0_0/* byval_arg */
	, &ICollection_1_t9519_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9519_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.PreserveSigAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern Il2CppType IEnumerator_1_t7406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52937_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.PreserveSigAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52937_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9521_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52937_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9521_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52937_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9521_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9521_0_0_0;
extern Il2CppType IEnumerable_1_t9521_1_0_0;
struct IEnumerable_1_t9521;
extern Il2CppGenericClass IEnumerable_1_t9521_GenericClass;
TypeInfo IEnumerable_1_t9521_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9521_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9521_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9521_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9521_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9521_0_0_0/* byval_arg */
	, &IEnumerable_1_t9521_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9521_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9520_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern MethodInfo IList_1_get_Item_m52938_MethodInfo;
extern MethodInfo IList_1_set_Item_m52939_MethodInfo;
static PropertyInfo IList_1_t9520____Item_PropertyInfo = 
{
	&IList_1_t9520_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52938_MethodInfo/* get */
	, &IList_1_set_Item_m52939_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9520_PropertyInfos[] =
{
	&IList_1_t9520____Item_PropertyInfo,
	NULL
};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
static ParameterInfo IList_1_t9520_IList_1_IndexOf_m52940_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttribute_t2029_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52940_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52940_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9520_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9520_IList_1_IndexOf_m52940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52940_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
static ParameterInfo IList_1_t9520_IList_1_Insert_m52941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttribute_t2029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52941_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52941_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9520_IList_1_Insert_m52941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52941_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9520_IList_1_RemoveAt_m52942_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52942_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52942_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9520_IList_1_RemoveAt_m52942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52942_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9520_IList_1_get_Item_m52938_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52938_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52938_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9520_il2cpp_TypeInfo/* declaring_type */
	, &PreserveSigAttribute_t2029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9520_IList_1_get_Item_m52938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52938_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PreserveSigAttribute_t2029_0_0_0;
static ParameterInfo IList_1_t9520_IList_1_set_Item_m52939_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PreserveSigAttribute_t2029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52939_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52939_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9520_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9520_IList_1_set_Item_m52939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52939_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9520_MethodInfos[] =
{
	&IList_1_IndexOf_m52940_MethodInfo,
	&IList_1_Insert_m52941_MethodInfo,
	&IList_1_RemoveAt_m52942_MethodInfo,
	&IList_1_get_Item_m52938_MethodInfo,
	&IList_1_set_Item_m52939_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9520_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9519_il2cpp_TypeInfo,
	&IEnumerable_1_t9521_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9520_0_0_0;
extern Il2CppType IList_1_t9520_1_0_0;
struct IList_1_t9520;
extern Il2CppGenericClass IList_1_t9520_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9520_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9520_MethodInfos/* methods */
	, IList_1_t9520_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9520_il2cpp_TypeInfo/* element_class */
	, IList_1_t9520_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9520_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9520_0_0_0/* byval_arg */
	, &IList_1_t9520_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9520_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7408_il2cpp_TypeInfo;

// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52943_MethodInfo;
static PropertyInfo IEnumerator_1_t7408____Current_PropertyInfo = 
{
	&IEnumerator_1_t7408_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52943_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7408_PropertyInfos[] =
{
	&IEnumerator_1_t7408____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52943_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52943_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7408_il2cpp_TypeInfo/* declaring_type */
	, &TypeLibImportClassAttribute_t2030_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52943_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7408_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52943_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7408_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7408_0_0_0;
extern Il2CppType IEnumerator_1_t7408_1_0_0;
struct IEnumerator_1_t7408;
extern Il2CppGenericClass IEnumerator_1_t7408_GenericClass;
TypeInfo IEnumerator_1_t7408_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7408_MethodInfos/* methods */
	, IEnumerator_1_t7408_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7408_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7408_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7408_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7408_0_0_0/* byval_arg */
	, &IEnumerator_1_t7408_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7408_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_720.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5266_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_720MethodDeclarations.h"

extern TypeInfo TypeLibImportClassAttribute_t2030_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31706_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeLibImportClassAttribute_t2030_m41642_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTypeLibImportClassAttribute_t2030_m41642(__this, p0, method) (TypeLibImportClassAttribute_t2030 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5266____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5266, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5266____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5266, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5266_FieldInfos[] =
{
	&InternalEnumerator_1_t5266____array_0_FieldInfo,
	&InternalEnumerator_1_t5266____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5266____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5266_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5266____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5266_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5266_PropertyInfos[] =
{
	&InternalEnumerator_1_t5266____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5266____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5266_InternalEnumerator_1__ctor_m31702_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31702_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31702_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5266_InternalEnumerator_1__ctor_m31702_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31702_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31704_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31704_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31704_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31705_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31705_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31705_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31706_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31706_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* declaring_type */
	, &TypeLibImportClassAttribute_t2030_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31706_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5266_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31702_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_MethodInfo,
	&InternalEnumerator_1_Dispose_m31704_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31705_MethodInfo,
	&InternalEnumerator_1_get_Current_m31706_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31705_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31704_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5266_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31703_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31705_MethodInfo,
	&InternalEnumerator_1_Dispose_m31704_MethodInfo,
	&InternalEnumerator_1_get_Current_m31706_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5266_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7408_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5266_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7408_il2cpp_TypeInfo, 7},
};
extern TypeInfo TypeLibImportClassAttribute_t2030_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5266_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31706_MethodInfo/* Method Usage */,
	&TypeLibImportClassAttribute_t2030_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTypeLibImportClassAttribute_t2030_m41642_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5266_0_0_0;
extern Il2CppType InternalEnumerator_1_t5266_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5266_GenericClass;
TypeInfo InternalEnumerator_1_t5266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5266_MethodInfos/* methods */
	, InternalEnumerator_1_t5266_PropertyInfos/* properties */
	, InternalEnumerator_1_t5266_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5266_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5266_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5266_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5266_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5266_1_0_0/* this_arg */
	, InternalEnumerator_1_t5266_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5266_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5266_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5266)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9522_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern MethodInfo ICollection_1_get_Count_m52944_MethodInfo;
static PropertyInfo ICollection_1_t9522____Count_PropertyInfo = 
{
	&ICollection_1_t9522_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52944_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52945_MethodInfo;
static PropertyInfo ICollection_1_t9522____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9522_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52945_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9522_PropertyInfos[] =
{
	&ICollection_1_t9522____Count_PropertyInfo,
	&ICollection_1_t9522____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52944_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52944_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52944_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52945_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52945_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52945_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
static ParameterInfo ICollection_1_t9522_ICollection_1_Add_m52946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttribute_t2030_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52946_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52946_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9522_ICollection_1_Add_m52946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52946_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52947_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52947_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52947_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
static ParameterInfo ICollection_1_t9522_ICollection_1_Contains_m52948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttribute_t2030_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52948_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52948_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9522_ICollection_1_Contains_m52948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52948_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibImportClassAttributeU5BU5D_t5565_0_0_0;
extern Il2CppType TypeLibImportClassAttributeU5BU5D_t5565_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9522_ICollection_1_CopyTo_m52949_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttributeU5BU5D_t5565_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52949_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52949_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9522_ICollection_1_CopyTo_m52949_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52949_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
static ParameterInfo ICollection_1_t9522_ICollection_1_Remove_m52950_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttribute_t2030_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52950_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52950_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9522_ICollection_1_Remove_m52950_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52950_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9522_MethodInfos[] =
{
	&ICollection_1_get_Count_m52944_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52945_MethodInfo,
	&ICollection_1_Add_m52946_MethodInfo,
	&ICollection_1_Clear_m52947_MethodInfo,
	&ICollection_1_Contains_m52948_MethodInfo,
	&ICollection_1_CopyTo_m52949_MethodInfo,
	&ICollection_1_Remove_m52950_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9524_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9522_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9524_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9522_0_0_0;
extern Il2CppType ICollection_1_t9522_1_0_0;
struct ICollection_1_t9522;
extern Il2CppGenericClass ICollection_1_t9522_GenericClass;
TypeInfo ICollection_1_t9522_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9522_MethodInfos/* methods */
	, ICollection_1_t9522_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9522_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9522_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9522_0_0_0/* byval_arg */
	, &ICollection_1_t9522_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9522_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern Il2CppType IEnumerator_1_t7408_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52951_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52951_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9524_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7408_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52951_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9524_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52951_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9524_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9524_0_0_0;
extern Il2CppType IEnumerable_1_t9524_1_0_0;
struct IEnumerable_1_t9524;
extern Il2CppGenericClass IEnumerable_1_t9524_GenericClass;
TypeInfo IEnumerable_1_t9524_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9524_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9524_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9524_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9524_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9524_0_0_0/* byval_arg */
	, &IEnumerable_1_t9524_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9524_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9523_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern MethodInfo IList_1_get_Item_m52952_MethodInfo;
extern MethodInfo IList_1_set_Item_m52953_MethodInfo;
static PropertyInfo IList_1_t9523____Item_PropertyInfo = 
{
	&IList_1_t9523_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52952_MethodInfo/* get */
	, &IList_1_set_Item_m52953_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9523_PropertyInfos[] =
{
	&IList_1_t9523____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
static ParameterInfo IList_1_t9523_IList_1_IndexOf_m52954_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttribute_t2030_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52954_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52954_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9523_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9523_IList_1_IndexOf_m52954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52954_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
static ParameterInfo IList_1_t9523_IList_1_Insert_m52955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttribute_t2030_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52955_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52955_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9523_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9523_IList_1_Insert_m52955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52955_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9523_IList_1_RemoveAt_m52956_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52956_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52956_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9523_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9523_IList_1_RemoveAt_m52956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52956_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9523_IList_1_get_Item_m52952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52952_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52952_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9523_il2cpp_TypeInfo/* declaring_type */
	, &TypeLibImportClassAttribute_t2030_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9523_IList_1_get_Item_m52952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52952_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeLibImportClassAttribute_t2030_0_0_0;
static ParameterInfo IList_1_t9523_IList_1_set_Item_m52953_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeLibImportClassAttribute_t2030_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52953_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52953_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9523_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9523_IList_1_set_Item_m52953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52953_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9523_MethodInfos[] =
{
	&IList_1_IndexOf_m52954_MethodInfo,
	&IList_1_Insert_m52955_MethodInfo,
	&IList_1_RemoveAt_m52956_MethodInfo,
	&IList_1_get_Item_m52952_MethodInfo,
	&IList_1_set_Item_m52953_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9523_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9522_il2cpp_TypeInfo,
	&IEnumerable_1_t9524_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9523_0_0_0;
extern Il2CppType IList_1_t9523_1_0_0;
struct IList_1_t9523;
extern Il2CppGenericClass IList_1_t9523_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9523_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9523_MethodInfos/* methods */
	, IList_1_t9523_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9523_il2cpp_TypeInfo/* element_class */
	, IList_1_t9523_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9523_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9523_0_0_0/* byval_arg */
	, &IList_1_t9523_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9523_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7410_il2cpp_TypeInfo;

// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52957_MethodInfo;
static PropertyInfo IEnumerator_1_t7410____Current_PropertyInfo = 
{
	&IEnumerator_1_t7410_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52957_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7410_PropertyInfos[] =
{
	&IEnumerator_1_t7410____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52957_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52957_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7410_il2cpp_TypeInfo/* declaring_type */
	, &TypeLibVersionAttribute_t2031_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52957_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7410_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52957_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7410_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7410_0_0_0;
extern Il2CppType IEnumerator_1_t7410_1_0_0;
struct IEnumerator_1_t7410;
extern Il2CppGenericClass IEnumerator_1_t7410_GenericClass;
TypeInfo IEnumerator_1_t7410_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7410_MethodInfos/* methods */
	, IEnumerator_1_t7410_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7410_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7410_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7410_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7410_0_0_0/* byval_arg */
	, &IEnumerator_1_t7410_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7410_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_721.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5267_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_721MethodDeclarations.h"

extern TypeInfo TypeLibVersionAttribute_t2031_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31711_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeLibVersionAttribute_t2031_m41653_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTypeLibVersionAttribute_t2031_m41653(__this, p0, method) (TypeLibVersionAttribute_t2031 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5267____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5267, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5267____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5267, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5267_FieldInfos[] =
{
	&InternalEnumerator_1_t5267____array_0_FieldInfo,
	&InternalEnumerator_1_t5267____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5267____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5267_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5267____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5267_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31711_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5267_PropertyInfos[] =
{
	&InternalEnumerator_1_t5267____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5267____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5267_InternalEnumerator_1__ctor_m31707_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31707_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31707_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5267_InternalEnumerator_1__ctor_m31707_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31707_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31709_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31709_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31709_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31710_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31710_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31710_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31711_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31711_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* declaring_type */
	, &TypeLibVersionAttribute_t2031_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31711_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5267_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31707_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_MethodInfo,
	&InternalEnumerator_1_Dispose_m31709_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31710_MethodInfo,
	&InternalEnumerator_1_get_Current_m31711_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31710_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31709_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5267_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31708_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31710_MethodInfo,
	&InternalEnumerator_1_Dispose_m31709_MethodInfo,
	&InternalEnumerator_1_get_Current_m31711_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5267_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7410_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5267_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7410_il2cpp_TypeInfo, 7},
};
extern TypeInfo TypeLibVersionAttribute_t2031_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5267_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31711_MethodInfo/* Method Usage */,
	&TypeLibVersionAttribute_t2031_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTypeLibVersionAttribute_t2031_m41653_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5267_0_0_0;
extern Il2CppType InternalEnumerator_1_t5267_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5267_GenericClass;
TypeInfo InternalEnumerator_1_t5267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5267_MethodInfos/* methods */
	, InternalEnumerator_1_t5267_PropertyInfos/* properties */
	, InternalEnumerator_1_t5267_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5267_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5267_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5267_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5267_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5267_1_0_0/* this_arg */
	, InternalEnumerator_1_t5267_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5267_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5267_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5267)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9525_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern MethodInfo ICollection_1_get_Count_m52958_MethodInfo;
static PropertyInfo ICollection_1_t9525____Count_PropertyInfo = 
{
	&ICollection_1_t9525_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52958_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52959_MethodInfo;
static PropertyInfo ICollection_1_t9525____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9525_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52959_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9525_PropertyInfos[] =
{
	&ICollection_1_t9525____Count_PropertyInfo,
	&ICollection_1_t9525____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52958_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52958_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52958_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52959_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52959_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52959_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
static ParameterInfo ICollection_1_t9525_ICollection_1_Add_m52960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttribute_t2031_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52960_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52960_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9525_ICollection_1_Add_m52960_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52960_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52961_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52961_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52961_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
static ParameterInfo ICollection_1_t9525_ICollection_1_Contains_m52962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttribute_t2031_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52962_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52962_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9525_ICollection_1_Contains_m52962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52962_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibVersionAttributeU5BU5D_t5566_0_0_0;
extern Il2CppType TypeLibVersionAttributeU5BU5D_t5566_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9525_ICollection_1_CopyTo_m52963_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttributeU5BU5D_t5566_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52963_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52963_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9525_ICollection_1_CopyTo_m52963_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52963_GenericMethod/* genericMethod */

};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
static ParameterInfo ICollection_1_t9525_ICollection_1_Remove_m52964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttribute_t2031_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52964_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52964_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9525_ICollection_1_Remove_m52964_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52964_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9525_MethodInfos[] =
{
	&ICollection_1_get_Count_m52958_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52959_MethodInfo,
	&ICollection_1_Add_m52960_MethodInfo,
	&ICollection_1_Clear_m52961_MethodInfo,
	&ICollection_1_Contains_m52962_MethodInfo,
	&ICollection_1_CopyTo_m52963_MethodInfo,
	&ICollection_1_Remove_m52964_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9527_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9525_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9527_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9525_0_0_0;
extern Il2CppType ICollection_1_t9525_1_0_0;
struct ICollection_1_t9525;
extern Il2CppGenericClass ICollection_1_t9525_GenericClass;
TypeInfo ICollection_1_t9525_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9525_MethodInfos/* methods */
	, ICollection_1_t9525_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9525_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9525_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9525_0_0_0/* byval_arg */
	, &ICollection_1_t9525_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9525_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
