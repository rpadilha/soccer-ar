﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RC2Transform
struct RC2Transform_t2157;
// System.Security.Cryptography.RC2
struct RC2_t1730;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.RC2Transform::.ctor(System.Security.Cryptography.RC2,System.Boolean,System.Byte[],System.Byte[])
 void RC2Transform__ctor_m12145 (RC2Transform_t2157 * __this, RC2_t1730 * ___rc2Algo, bool ___encryption, ByteU5BU5D_t653* ___key, ByteU5BU5D_t653* ___iv, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2Transform::.cctor()
 void RC2Transform__cctor_m12146 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2Transform::ECB(System.Byte[],System.Byte[])
 void RC2Transform_ECB_m12147 (RC2Transform_t2157 * __this, ByteU5BU5D_t653* ___input, ByteU5BU5D_t653* ___output, MethodInfo* method) IL2CPP_METHOD_ATTR;
