﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCall
struct PersistentCall_t1135;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// System.String
struct String_t;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t1126;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1127;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1136;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"

// System.Void UnityEngine.Events.PersistentCall::.ctor()
 void PersistentCall__ctor_m6539 (PersistentCall_t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
 Object_t120 * PersistentCall_get_target_m6540 (PersistentCall_t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.PersistentCall::get_methodName()
 String_t* PersistentCall_get_methodName_m6541 (PersistentCall_t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
 int32_t PersistentCall_get_mode_m6542 (PersistentCall_t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
 ArgumentCache_t1126 * PersistentCall_get_arguments_m6543 (PersistentCall_t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
 bool PersistentCall_IsValid_m6544 (PersistentCall_t1135 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
 BaseInvokableCall_t1127 * PersistentCall_GetRuntimeCall_m6545 (PersistentCall_t1135 * __this, UnityEventBase_t1136 * ___theEvent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
 BaseInvokableCall_t1127 * PersistentCall_GetObjectCall_m6546 (Object_t * __this/* static, unused */, Object_t120 * ___target, MethodInfo_t141 * ___method, ArgumentCache_t1126 * ___arguments, MethodInfo* method) IL2CPP_METHOD_ATTR;
