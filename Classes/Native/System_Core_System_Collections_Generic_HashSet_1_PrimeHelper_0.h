﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t175;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
struct PrimeHelper_t4569  : public Object_t
{
};
struct PrimeHelper_t4569_StaticFields{
	// System.Int32[] System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::primes_table
	Int32U5BU5D_t175* ___primes_table_0;
};
