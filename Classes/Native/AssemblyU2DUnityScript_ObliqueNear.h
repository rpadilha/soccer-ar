﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ObliqueNear
struct ObliqueNear_t215  : public MonoBehaviour_t10
{
	// UnityEngine.Transform ObliqueNear::plane
	Transform_t74 * ___plane_2;
};
