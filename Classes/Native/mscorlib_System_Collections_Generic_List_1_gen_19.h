﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t3834;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t595  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_items
	ITrackableEventHandlerU5BU5D_t3834* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t595_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::EmptyArray
	ITrackableEventHandlerU5BU5D_t3834* ___EmptyArray_4;
};
