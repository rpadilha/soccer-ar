﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t772;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>
struct Enumerator_t4229 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::l
	List_1_t772 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::current
	TrackableBehaviour_t17 * ___current_3;
};
