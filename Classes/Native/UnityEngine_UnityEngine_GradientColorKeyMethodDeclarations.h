﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GradientColorKey
struct GradientColorKey_t992;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
 void GradientColorKey__ctor_m5725 (GradientColorKey_t992 * __this, Color_t66  ___col, float ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
