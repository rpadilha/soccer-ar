﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t4082;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
 void DefaultComparer__ctor_m23179 (DefaultComparer_t4082 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
 int32_t DefaultComparer_Compare_m23180 (DefaultComparer_t4082 * __this, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
