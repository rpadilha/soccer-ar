﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t1067;
struct AnimationCurve_t1067_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
 void AnimationCurve__ctor_m6309 (AnimationCurve_t1067 * __this, KeyframeU5BU5D_t1068* ___keys, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
 void AnimationCurve__ctor_m6310 (AnimationCurve_t1067 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
 void AnimationCurve_Cleanup_m6311 (AnimationCurve_t1067 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
 void AnimationCurve_Finalize_m6312 (AnimationCurve_t1067 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
 void AnimationCurve_Init_m6313 (AnimationCurve_t1067 * __this, KeyframeU5BU5D_t1068* ___keys, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t1067_marshal(const AnimationCurve_t1067& unmarshaled, AnimationCurve_t1067_marshaled& marshaled);
void AnimationCurve_t1067_marshal_back(const AnimationCurve_t1067_marshaled& marshaled, AnimationCurve_t1067& unmarshaled);
void AnimationCurve_t1067_marshal_cleanup(AnimationCurve_t1067_marshaled& marshaled);
