﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RotationConstraint
struct RotationConstraint_t220;

// System.Void RotationConstraint::.ctor()
 void RotationConstraint__ctor_m773 (RotationConstraint_t220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotationConstraint::Start()
 void RotationConstraint_Start_m774 (RotationConstraint_t220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotationConstraint::LateUpdate()
 void RotationConstraint_LateUpdate_m775 (RotationConstraint_t220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotationConstraint::Main()
 void RotationConstraint_Main_m776 (RotationConstraint_t220 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
