﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
struct UnityAction_1_t2789;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
struct InvokableCall_1_t2788  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Delegate
	UnityAction_1_t2789 * ___Delegate_0;
};
