﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider
struct Collider_t70;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"

// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
 void Collider_set_enabled_m286 (Collider_t70 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::INTERNAL_get_bounds(UnityEngine.Bounds&)
 void Collider_INTERNAL_get_bounds_m6263 (Collider_t70 * __this, Bounds_t198 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
 Bounds_t198  Collider_get_bounds_m662 (Collider_t70 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
