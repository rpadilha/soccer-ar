﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>
struct EqualityComparer_1_t4281;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>
struct EqualityComparer_1_t4281  : public Object_t
{
};
struct EqualityComparer_1_t4281_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::_default
	EqualityComparer_1_t4281 * ____default_0;
};
