﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct KeyValuePair_2_t4300;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t43;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m25295 (KeyValuePair_2_t4300 * __this, int32_t ___key, PropAbstractBehaviour_t43 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m25296 (KeyValuePair_2_t4300 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25297 (KeyValuePair_2_t4300 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
 PropAbstractBehaviour_t43 * KeyValuePair_2_get_Value_m25298 (KeyValuePair_2_t4300 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25299 (KeyValuePair_2_t4300 * __this, PropAbstractBehaviour_t43 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m25300 (KeyValuePair_2_t4300 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
