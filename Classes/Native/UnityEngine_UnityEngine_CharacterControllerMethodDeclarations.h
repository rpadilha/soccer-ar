﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CharacterController
struct CharacterController_t209;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.CollisionFlags
#include "UnityEngine_UnityEngine_CollisionFlags.h"

// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
 int32_t CharacterController_Move_m806 (CharacterController_t209 * __this, Vector3_t73  ___motion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
 int32_t CharacterController_INTERNAL_CALL_Move_m6264 (Object_t * __this/* static, unused */, CharacterController_t209 * ___self, Vector3_t73 * ___motion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
 bool CharacterController_get_isGrounded_m804 (CharacterController_t209 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::INTERNAL_get_velocity(UnityEngine.Vector3&)
 void CharacterController_INTERNAL_get_velocity_m6265 (CharacterController_t209 * __this, Vector3_t73 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
 Vector3_t73  CharacterController_get_velocity_m802 (CharacterController_t209 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
