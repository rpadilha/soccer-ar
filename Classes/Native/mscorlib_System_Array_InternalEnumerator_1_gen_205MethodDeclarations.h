﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>
struct InternalEnumerator_1_t3370;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m17715 (InternalEnumerator_1_t3370 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17716 (InternalEnumerator_1_t3370 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>::Dispose()
 void InternalEnumerator_1_Dispose_m17717 (InternalEnumerator_1_t3370 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m17718 (InternalEnumerator_1_t3370 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m17719 (InternalEnumerator_1_t3370 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
