﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UnityPlayer
struct UnityPlayer_t613;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t139;

// Vuforia.IUnityPlayer Vuforia.UnityPlayer::get_Instance()
 Object_t * UnityPlayer_get_Instance_m2866 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UnityPlayer::SetImplementation(Vuforia.IUnityPlayer)
 void UnityPlayer_SetImplementation_m2867 (Object_t * __this/* static, unused */, Object_t * ___implementation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UnityPlayer::.cctor()
 void UnityPlayer__cctor_m2868 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
