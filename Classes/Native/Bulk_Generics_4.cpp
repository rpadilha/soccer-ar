﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo ICollection_1_t7697_il2cpp_TypeInfo;

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Void
#include "mscorlib_System_Void.h"
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
#include "Assembly-CSharp_ArrayTypes.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>
extern MethodInfo ICollection_1_get_Count_m43247_MethodInfo;
static PropertyInfo ICollection_1_t7697____Count_PropertyInfo = 
{
	&ICollection_1_t7697_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43247_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43248_MethodInfo;
static PropertyInfo ICollection_1_t7697____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7697_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43248_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7697_PropertyInfos[] =
{
	&ICollection_1_t7697____Count_PropertyInfo,
	&ICollection_1_t7697____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43247_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43247_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43247_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43248_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43248_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43248_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo ICollection_1_t7697_ICollection_1_Add_m43249_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43249_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43249_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7697_ICollection_1_Add_m43249_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43249_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43250_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43250_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43250_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo ICollection_1_t7697_ICollection_1_Contains_m43251_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43251_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43251_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7697_ICollection_1_Contains_m43251_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43251_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviourU5BU5D_t5371_0_0_0;
extern Il2CppType MaskOutBehaviourU5BU5D_t5371_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7697_ICollection_1_CopyTo_m43252_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviourU5BU5D_t5371_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43252_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43252_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7697_ICollection_1_CopyTo_m43252_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43252_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo ICollection_1_t7697_ICollection_1_Remove_m43253_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43253_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43253_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7697_ICollection_1_Remove_m43253_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43253_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7697_MethodInfos[] =
{
	&ICollection_1_get_Count_m43247_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43248_MethodInfo,
	&ICollection_1_Add_m43249_MethodInfo,
	&ICollection_1_Clear_m43250_MethodInfo,
	&ICollection_1_Contains_m43251_MethodInfo,
	&ICollection_1_CopyTo_m43252_MethodInfo,
	&ICollection_1_Remove_m43253_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7699_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7697_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7699_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7697_0_0_0;
extern Il2CppType ICollection_1_t7697_1_0_0;
struct ICollection_1_t7697;
extern Il2CppGenericClass ICollection_1_t7697_GenericClass;
TypeInfo ICollection_1_t7697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7697_MethodInfos/* methods */
	, ICollection_1_t7697_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7697_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7697_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7697_0_0_0/* byval_arg */
	, &ICollection_1_t7697_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7697_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MaskOutBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.MaskOutBehaviour>
extern Il2CppType IEnumerator_1_t6047_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43254_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MaskOutBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43254_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7699_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6047_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43254_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7699_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43254_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7699_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7699_0_0_0;
extern Il2CppType IEnumerable_1_t7699_1_0_0;
struct IEnumerable_1_t7699;
extern Il2CppGenericClass IEnumerable_1_t7699_GenericClass;
TypeInfo IEnumerable_1_t7699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7699_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7699_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7699_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7699_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7699_0_0_0/* byval_arg */
	, &IEnumerable_1_t7699_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7699_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7698_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>
extern MethodInfo IList_1_get_Item_m43255_MethodInfo;
extern MethodInfo IList_1_set_Item_m43256_MethodInfo;
static PropertyInfo IList_1_t7698____Item_PropertyInfo = 
{
	&IList_1_t7698_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43255_MethodInfo/* get */
	, &IList_1_set_Item_m43256_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7698_PropertyInfos[] =
{
	&IList_1_t7698____Item_PropertyInfo,
	NULL
};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo IList_1_t7698_IList_1_IndexOf_m43257_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43257_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43257_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7698_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7698_IList_1_IndexOf_m43257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43257_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo IList_1_t7698_IList_1_Insert_m43258_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43258_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43258_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7698_IList_1_Insert_m43258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43258_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7698_IList_1_RemoveAt_m43259_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43259_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43259_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7698_IList_1_RemoveAt_m43259_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43259_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7698_IList_1_get_Item_m43255_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43255_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43255_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7698_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutBehaviour_t40_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7698_IList_1_get_Item_m43255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43255_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo IList_1_t7698_IList_1_set_Item_m43256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43256_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43256_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7698_IList_1_set_Item_m43256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43256_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7698_MethodInfos[] =
{
	&IList_1_IndexOf_m43257_MethodInfo,
	&IList_1_Insert_m43258_MethodInfo,
	&IList_1_RemoveAt_m43259_MethodInfo,
	&IList_1_get_Item_m43255_MethodInfo,
	&IList_1_set_Item_m43256_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7698_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7697_il2cpp_TypeInfo,
	&IEnumerable_1_t7699_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7698_0_0_0;
extern Il2CppType IList_1_t7698_1_0_0;
struct IList_1_t7698;
extern Il2CppGenericClass IList_1_t7698_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7698_MethodInfos/* methods */
	, IList_1_t7698_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7698_il2cpp_TypeInfo/* element_class */
	, IList_1_t7698_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7698_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7698_0_0_0/* byval_arg */
	, &IList_1_t7698_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7698_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7700_il2cpp_TypeInfo;

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43260_MethodInfo;
static PropertyInfo ICollection_1_t7700____Count_PropertyInfo = 
{
	&ICollection_1_t7700_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43261_MethodInfo;
static PropertyInfo ICollection_1_t7700____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7700_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7700_PropertyInfos[] =
{
	&ICollection_1_t7700____Count_PropertyInfo,
	&ICollection_1_t7700____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43260_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43260_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43260_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43261_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43261_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43261_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
static ParameterInfo ICollection_1_t7700_ICollection_1_Add_m43262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviour_t28_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43262_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43262_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7700_ICollection_1_Add_m43262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43262_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43263_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43263_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43263_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
static ParameterInfo ICollection_1_t7700_ICollection_1_Contains_m43264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviour_t28_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43264_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43264_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7700_ICollection_1_Contains_m43264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43264_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutAbstractBehaviourU5BU5D_t5634_0_0_0;
extern Il2CppType MaskOutAbstractBehaviourU5BU5D_t5634_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7700_ICollection_1_CopyTo_m43265_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviourU5BU5D_t5634_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43265_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43265_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7700_ICollection_1_CopyTo_m43265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43265_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
static ParameterInfo ICollection_1_t7700_ICollection_1_Remove_m43266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviour_t28_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43266_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MaskOutAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43266_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7700_ICollection_1_Remove_m43266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43266_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7700_MethodInfos[] =
{
	&ICollection_1_get_Count_m43260_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43261_MethodInfo,
	&ICollection_1_Add_m43262_MethodInfo,
	&ICollection_1_Clear_m43263_MethodInfo,
	&ICollection_1_Contains_m43264_MethodInfo,
	&ICollection_1_CopyTo_m43265_MethodInfo,
	&ICollection_1_Remove_m43266_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7702_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7700_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7702_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7700_0_0_0;
extern Il2CppType ICollection_1_t7700_1_0_0;
struct ICollection_1_t7700;
extern Il2CppGenericClass ICollection_1_t7700_GenericClass;
TypeInfo ICollection_1_t7700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7700_MethodInfos/* methods */
	, ICollection_1_t7700_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7700_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7700_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7700_0_0_0/* byval_arg */
	, &ICollection_1_t7700_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7700_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MaskOutAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.MaskOutAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6049_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43267_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MaskOutAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43267_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7702_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6049_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43267_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7702_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43267_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7702_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7702_0_0_0;
extern Il2CppType IEnumerable_1_t7702_1_0_0;
struct IEnumerable_1_t7702;
extern Il2CppGenericClass IEnumerable_1_t7702_GenericClass;
TypeInfo IEnumerable_1_t7702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7702_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7702_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7702_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7702_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7702_0_0_0/* byval_arg */
	, &IEnumerable_1_t7702_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6049_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.MaskOutAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43268_MethodInfo;
static PropertyInfo IEnumerator_1_t6049____Current_PropertyInfo = 
{
	&IEnumerator_1_t6049_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43268_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6049_PropertyInfos[] =
{
	&IEnumerator_1_t6049____Current_PropertyInfo,
	NULL
};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43268_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43268_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6049_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutAbstractBehaviour_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43268_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6049_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43268_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6049_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6049_0_0_0;
extern Il2CppType IEnumerator_1_t6049_1_0_0;
struct IEnumerator_1_t6049;
extern Il2CppGenericClass IEnumerator_1_t6049_GenericClass;
TypeInfo IEnumerator_1_t6049_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6049_MethodInfos/* methods */
	, IEnumerator_1_t6049_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6049_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6049_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6049_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6049_0_0_0/* byval_arg */
	, &IEnumerator_1_t6049_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6049_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2885_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo MaskOutAbstractBehaviour_t28_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14913_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMaskOutAbstractBehaviour_t28_m32983_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.MaskOutAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.MaskOutAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMaskOutAbstractBehaviour_t28_m32983(__this, p0, method) (MaskOutAbstractBehaviour_t28 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2885____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2885, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2885____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2885, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2885_FieldInfos[] =
{
	&InternalEnumerator_1_t2885____array_0_FieldInfo,
	&InternalEnumerator_1_t2885____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2885____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2885_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2885____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2885_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14913_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2885_PropertyInfos[] =
{
	&InternalEnumerator_1_t2885____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2885____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2885_InternalEnumerator_1__ctor_m14909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14909_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14909_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2885_InternalEnumerator_1__ctor_m14909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14909_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14911_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14911_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14911_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14912_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14912_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14912_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14913_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.MaskOutAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14913_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutAbstractBehaviour_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14913_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2885_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14909_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_MethodInfo,
	&InternalEnumerator_1_Dispose_m14911_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14912_MethodInfo,
	&InternalEnumerator_1_get_Current_m14913_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m14912_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14911_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2885_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14910_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14912_MethodInfo,
	&InternalEnumerator_1_Dispose_m14911_MethodInfo,
	&InternalEnumerator_1_get_Current_m14913_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2885_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6049_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2885_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6049_il2cpp_TypeInfo, 7},
};
extern TypeInfo MaskOutAbstractBehaviour_t28_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2885_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14913_MethodInfo/* Method Usage */,
	&MaskOutAbstractBehaviour_t28_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMaskOutAbstractBehaviour_t28_m32983_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2885_0_0_0;
extern Il2CppType InternalEnumerator_1_t2885_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2885_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2885_MethodInfos/* methods */
	, InternalEnumerator_1_t2885_PropertyInfos/* properties */
	, InternalEnumerator_1_t2885_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2885_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2885_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2885_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2885_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2885_1_0_0/* this_arg */
	, InternalEnumerator_1_t2885_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2885_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2885_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2885)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7701_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43269_MethodInfo;
extern MethodInfo IList_1_set_Item_m43270_MethodInfo;
static PropertyInfo IList_1_t7701____Item_PropertyInfo = 
{
	&IList_1_t7701_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43269_MethodInfo/* get */
	, &IList_1_set_Item_m43270_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7701_PropertyInfos[] =
{
	&IList_1_t7701____Item_PropertyInfo,
	NULL
};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
static ParameterInfo IList_1_t7701_IList_1_IndexOf_m43271_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviour_t28_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43271_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43271_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7701_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7701_IList_1_IndexOf_m43271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43271_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
static ParameterInfo IList_1_t7701_IList_1_Insert_m43272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviour_t28_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43272_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43272_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7701_IList_1_Insert_m43272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43272_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7701_IList_1_RemoveAt_m43273_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43273_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43273_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7701_IList_1_RemoveAt_m43273_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43273_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7701_IList_1_get_Item_m43269_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43269_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43269_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7701_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutAbstractBehaviour_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7701_IList_1_get_Item_m43269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43269_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MaskOutAbstractBehaviour_t28_0_0_0;
static ParameterInfo IList_1_t7701_IList_1_set_Item_m43270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MaskOutAbstractBehaviour_t28_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43270_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MaskOutAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43270_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7701_IList_1_set_Item_m43270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43270_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7701_MethodInfos[] =
{
	&IList_1_IndexOf_m43271_MethodInfo,
	&IList_1_Insert_m43272_MethodInfo,
	&IList_1_RemoveAt_m43273_MethodInfo,
	&IList_1_get_Item_m43269_MethodInfo,
	&IList_1_set_Item_m43270_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7701_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7700_il2cpp_TypeInfo,
	&IEnumerable_1_t7702_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7701_0_0_0;
extern Il2CppType IList_1_t7701_1_0_0;
struct IList_1_t7701;
extern Il2CppGenericClass IList_1_t7701_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7701_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7701_MethodInfos/* methods */
	, IList_1_t7701_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7701_il2cpp_TypeInfo/* element_class */
	, IList_1_t7701_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7701_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7701_0_0_0/* byval_arg */
	, &IList_1_t7701_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7701_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_17.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2886_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_17MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_13.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2887_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_13MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14916_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14918_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2886____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2886_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2886, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2886_FieldInfos[] =
{
	&CachedInvokableCall_1_t2886____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2886_CachedInvokableCall_1__ctor_m14914_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14914_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14914_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2886_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2886_CachedInvokableCall_1__ctor_m14914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14914_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2886_CachedInvokableCall_1_Invoke_m14915_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14915_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14915_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2886_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2886_CachedInvokableCall_1_Invoke_m14915_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14915_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2886_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14914_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14915_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m14915_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14919_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2886_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14915_MethodInfo,
	&InvokableCall_1_Find_m14919_MethodInfo,
};
extern Il2CppType UnityAction_1_t2888_0_0_0;
extern TypeInfo UnityAction_1_t2888_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMaskOutBehaviour_t40_m32993_MethodInfo;
extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14921_MethodInfo;
extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2886_RGCTXData[8] = 
{
	&UnityAction_1_t2888_0_0_0/* Type Usage */,
	&UnityAction_1_t2888_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMaskOutBehaviour_t40_m32993_MethodInfo/* Method Usage */,
	&MaskOutBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14921_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14916_MethodInfo/* Method Usage */,
	&MaskOutBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14918_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2886_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2886_1_0_0;
struct CachedInvokableCall_1_t2886;
extern Il2CppGenericClass CachedInvokableCall_1_t2886_GenericClass;
TypeInfo CachedInvokableCall_1_t2886_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2886_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2886_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2886_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2886_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2886_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2886_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2886_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2886_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2886_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2886)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_20.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2888_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_20MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.MaskOutBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.MaskOutBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMaskOutBehaviour_t40_m32993(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>
extern Il2CppType UnityAction_1_t2888_0_0_1;
FieldInfo InvokableCall_1_t2887____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2888_0_0_1/* type */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2887, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2887_FieldInfos[] =
{
	&InvokableCall_1_t2887____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2887_InvokableCall_1__ctor_m14916_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14916_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14916_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2887_InvokableCall_1__ctor_m14916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14916_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2888_0_0_0;
static ParameterInfo InvokableCall_1_t2887_InvokableCall_1__ctor_m14917_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2888_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14917_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14917_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2887_InvokableCall_1__ctor_m14917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14917_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2887_InvokableCall_1_Invoke_m14918_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14918_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14918_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2887_InvokableCall_1_Invoke_m14918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14918_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2887_InvokableCall_1_Find_m14919_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14919_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14919_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2887_InvokableCall_1_Find_m14919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14919_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2887_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14916_MethodInfo,
	&InvokableCall_1__ctor_m14917_MethodInfo,
	&InvokableCall_1_Invoke_m14918_MethodInfo,
	&InvokableCall_1_Find_m14919_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2887_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14918_MethodInfo,
	&InvokableCall_1_Find_m14919_MethodInfo,
};
extern TypeInfo UnityAction_1_t2888_il2cpp_TypeInfo;
extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2887_RGCTXData[5] = 
{
	&UnityAction_1_t2888_0_0_0/* Type Usage */,
	&UnityAction_1_t2888_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMaskOutBehaviour_t40_m32993_MethodInfo/* Method Usage */,
	&MaskOutBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14921_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2887_0_0_0;
extern Il2CppType InvokableCall_1_t2887_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2887;
extern Il2CppGenericClass InvokableCall_1_t2887_GenericClass;
TypeInfo InvokableCall_1_t2887_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2887_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2887_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2887_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2887_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2887_0_0_0/* byval_arg */
	, &InvokableCall_1_t2887_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2887_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2887_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2887)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2888_UnityAction_1__ctor_m14920_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14920_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14920_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2888_UnityAction_1__ctor_m14920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14920_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
static ParameterInfo UnityAction_1_t2888_UnityAction_1_Invoke_m14921_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14921_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14921_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2888_UnityAction_1_Invoke_m14921_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14921_GenericMethod/* genericMethod */

};
extern Il2CppType MaskOutBehaviour_t40_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2888_UnityAction_1_BeginInvoke_m14922_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MaskOutBehaviour_t40_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14922_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14922_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2888_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2888_UnityAction_1_BeginInvoke_m14922_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14922_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2888_UnityAction_1_EndInvoke_m14923_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14923_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MaskOutBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14923_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2888_UnityAction_1_EndInvoke_m14923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14923_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2888_MethodInfos[] =
{
	&UnityAction_1__ctor_m14920_MethodInfo,
	&UnityAction_1_Invoke_m14921_MethodInfo,
	&UnityAction_1_BeginInvoke_m14922_MethodInfo,
	&UnityAction_1_EndInvoke_m14923_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m14922_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14923_MethodInfo;
static MethodInfo* UnityAction_1_t2888_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14921_MethodInfo,
	&UnityAction_1_BeginInvoke_m14922_MethodInfo,
	&UnityAction_1_EndInvoke_m14923_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2888_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2888_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t2888;
extern Il2CppGenericClass UnityAction_1_t2888_GenericClass;
TypeInfo UnityAction_1_t2888_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2888_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2888_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2888_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2888_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2888_0_0_0/* byval_arg */
	, &UnityAction_1_t2888_1_0_0/* this_arg */
	, UnityAction_1_t2888_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2888_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2888)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6051_il2cpp_TypeInfo;

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Material>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Material>
extern MethodInfo IEnumerator_1_get_Current_m43274_MethodInfo;
static PropertyInfo IEnumerator_1_t6051____Current_PropertyInfo = 
{
	&IEnumerator_1_t6051_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43274_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6051_PropertyInfos[] =
{
	&IEnumerator_1_t6051____Current_PropertyInfo,
	NULL
};
extern Il2CppType Material_t64_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43274_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Material>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43274_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6051_il2cpp_TypeInfo/* declaring_type */
	, &Material_t64_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43274_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6051_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43274_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6051_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6051_0_0_0;
extern Il2CppType IEnumerator_1_t6051_1_0_0;
struct IEnumerator_1_t6051;
extern Il2CppGenericClass IEnumerator_1_t6051_GenericClass;
TypeInfo IEnumerator_1_t6051_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6051_MethodInfos/* methods */
	, IEnumerator_1_t6051_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6051_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6051_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6051_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6051_0_0_0/* byval_arg */
	, &IEnumerator_1_t6051_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6051_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Material>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_64.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2889_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Material>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_64MethodDeclarations.h"

extern TypeInfo Material_t64_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14928_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMaterial_t64_m32995_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Material>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Material>(System.Int32)
#define Array_InternalArray__get_Item_TisMaterial_t64_m32995(__this, p0, method) (Material_t64 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Material>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Material>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Material>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2889____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2889, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2889____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2889, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2889_FieldInfos[] =
{
	&InternalEnumerator_1_t2889____array_0_FieldInfo,
	&InternalEnumerator_1_t2889____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2889____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2889_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2889____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2889_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14928_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2889_PropertyInfos[] =
{
	&InternalEnumerator_1_t2889____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2889____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2889_InternalEnumerator_1__ctor_m14924_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14924_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14924_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2889_InternalEnumerator_1__ctor_m14924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14924_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14926_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14926_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14926_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14927_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Material>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14927_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14927_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t64_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14928_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Material>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14928_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* declaring_type */
	, &Material_t64_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14928_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2889_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14924_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_MethodInfo,
	&InternalEnumerator_1_Dispose_m14926_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14927_MethodInfo,
	&InternalEnumerator_1_get_Current_m14928_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14927_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14926_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2889_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14925_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14927_MethodInfo,
	&InternalEnumerator_1_Dispose_m14926_MethodInfo,
	&InternalEnumerator_1_get_Current_m14928_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2889_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6051_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2889_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6051_il2cpp_TypeInfo, 7},
};
extern TypeInfo Material_t64_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2889_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14928_MethodInfo/* Method Usage */,
	&Material_t64_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMaterial_t64_m32995_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2889_0_0_0;
extern Il2CppType InternalEnumerator_1_t2889_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2889_GenericClass;
TypeInfo InternalEnumerator_1_t2889_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2889_MethodInfos/* methods */
	, InternalEnumerator_1_t2889_PropertyInfos/* properties */
	, InternalEnumerator_1_t2889_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2889_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2889_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2889_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2889_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2889_1_0_0/* this_arg */
	, InternalEnumerator_1_t2889_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2889_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2889_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2889)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7703_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Material>
extern MethodInfo ICollection_1_get_Count_m43275_MethodInfo;
static PropertyInfo ICollection_1_t7703____Count_PropertyInfo = 
{
	&ICollection_1_t7703_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43275_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43276_MethodInfo;
static PropertyInfo ICollection_1_t7703____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7703_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43276_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7703_PropertyInfos[] =
{
	&ICollection_1_t7703____Count_PropertyInfo,
	&ICollection_1_t7703____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43275_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_Count()
MethodInfo ICollection_1_get_Count_m43275_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43275_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43276_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43276_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43276_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t64_0_0_0;
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo ICollection_1_t7703_ICollection_1_Add_m43277_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43277_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Add(T)
MethodInfo ICollection_1_Add_m43277_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7703_ICollection_1_Add_m43277_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43277_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43278_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Clear()
MethodInfo ICollection_1_Clear_m43278_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43278_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo ICollection_1_t7703_ICollection_1_Contains_m43279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43279_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Contains(T)
MethodInfo ICollection_1_Contains_m43279_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7703_ICollection_1_Contains_m43279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43279_GenericMethod/* genericMethod */

};
extern Il2CppType MaterialU5BU5D_t97_0_0_0;
extern Il2CppType MaterialU5BU5D_t97_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7703_ICollection_1_CopyTo_m43280_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MaterialU5BU5D_t97_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43280_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43280_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7703_ICollection_1_CopyTo_m43280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43280_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo ICollection_1_t7703_ICollection_1_Remove_m43281_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43281_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Remove(T)
MethodInfo ICollection_1_Remove_m43281_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7703_ICollection_1_Remove_m43281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43281_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7703_MethodInfos[] =
{
	&ICollection_1_get_Count_m43275_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43276_MethodInfo,
	&ICollection_1_Add_m43277_MethodInfo,
	&ICollection_1_Clear_m43278_MethodInfo,
	&ICollection_1_Contains_m43279_MethodInfo,
	&ICollection_1_CopyTo_m43280_MethodInfo,
	&ICollection_1_Remove_m43281_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7705_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7703_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7705_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7703_0_0_0;
extern Il2CppType ICollection_1_t7703_1_0_0;
struct ICollection_1_t7703;
extern Il2CppGenericClass ICollection_1_t7703_GenericClass;
TypeInfo ICollection_1_t7703_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7703_MethodInfos/* methods */
	, ICollection_1_t7703_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7703_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7703_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7703_0_0_0/* byval_arg */
	, &ICollection_1_t7703_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7703_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Material>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Material>
extern Il2CppType IEnumerator_1_t6051_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43282_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Material>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43282_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7705_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6051_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43282_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7705_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43282_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7705_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7705_0_0_0;
extern Il2CppType IEnumerable_1_t7705_1_0_0;
struct IEnumerable_1_t7705;
extern Il2CppGenericClass IEnumerable_1_t7705_GenericClass;
TypeInfo IEnumerable_1_t7705_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7705_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7705_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7705_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7705_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7705_0_0_0/* byval_arg */
	, &IEnumerable_1_t7705_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7705_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7704_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Material>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Material>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Material>
extern MethodInfo IList_1_get_Item_m43283_MethodInfo;
extern MethodInfo IList_1_set_Item_m43284_MethodInfo;
static PropertyInfo IList_1_t7704____Item_PropertyInfo = 
{
	&IList_1_t7704_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43283_MethodInfo/* get */
	, &IList_1_set_Item_m43284_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7704_PropertyInfos[] =
{
	&IList_1_t7704____Item_PropertyInfo,
	NULL
};
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo IList_1_t7704_IList_1_IndexOf_m43285_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43285_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Material>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43285_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7704_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7704_IList_1_IndexOf_m43285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43285_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo IList_1_t7704_IList_1_Insert_m43286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43286_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43286_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7704_IList_1_Insert_m43286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43286_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7704_IList_1_RemoveAt_m43287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43287_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43287_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7704_IList_1_RemoveAt_m43287_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43287_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7704_IList_1_get_Item_m43283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Material_t64_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43283_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Material>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43283_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7704_il2cpp_TypeInfo/* declaring_type */
	, &Material_t64_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7704_IList_1_get_Item_m43283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43283_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Material_t64_0_0_0;
static ParameterInfo IList_1_t7704_IList_1_set_Item_m43284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Material_t64_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43284_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43284_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7704_IList_1_set_Item_m43284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43284_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7704_MethodInfos[] =
{
	&IList_1_IndexOf_m43285_MethodInfo,
	&IList_1_Insert_m43286_MethodInfo,
	&IList_1_RemoveAt_m43287_MethodInfo,
	&IList_1_get_Item_m43283_MethodInfo,
	&IList_1_set_Item_m43284_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7704_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7703_il2cpp_TypeInfo,
	&IEnumerable_1_t7705_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7704_0_0_0;
extern Il2CppType IList_1_t7704_1_0_0;
struct IList_1_t7704;
extern Il2CppGenericClass IList_1_t7704_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7704_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7704_MethodInfos/* methods */
	, IList_1_t7704_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7704_il2cpp_TypeInfo/* element_class */
	, IList_1_t7704_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7704_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7704_0_0_0/* byval_arg */
	, &IList_1_t7704_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7704_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2890_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_2MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Renderer>
extern Il2CppType Renderer_t129_0_0_6;
FieldInfo CastHelper_1_t2890____t_0_FieldInfo = 
{
	"t"/* name */
	, &Renderer_t129_0_0_6/* type */
	, &CastHelper_1_t2890_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2890, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2890____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2890_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2890, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2890_FieldInfos[] =
{
	&CastHelper_1_t2890____t_0_FieldInfo,
	&CastHelper_1_t2890____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2890_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2890_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2890_0_0_0;
extern Il2CppType CastHelper_1_t2890_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2890_GenericClass;
TypeInfo CastHelper_1_t2890_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2890_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2890_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2890_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2890_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2890_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2890_0_0_0/* byval_arg */
	, &CastHelper_1_t2890_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2890_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2890)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6053_il2cpp_TypeInfo;

// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.MultiTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.MultiTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43288_MethodInfo;
static PropertyInfo IEnumerator_1_t6053____Current_PropertyInfo = 
{
	&IEnumerator_1_t6053_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6053_PropertyInfos[] =
{
	&IEnumerator_1_t6053____Current_PropertyInfo,
	NULL
};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43288_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.MultiTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43288_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6053_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetBehaviour_t41_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43288_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6053_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43288_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6053_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6053_0_0_0;
extern Il2CppType IEnumerator_1_t6053_1_0_0;
struct IEnumerator_1_t6053;
extern Il2CppGenericClass IEnumerator_1_t6053_GenericClass;
TypeInfo IEnumerator_1_t6053_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6053_MethodInfos/* methods */
	, IEnumerator_1_t6053_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6053_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6053_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6053_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6053_0_0_0/* byval_arg */
	, &IEnumerator_1_t6053_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6053_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_65.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2891_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_65MethodDeclarations.h"

extern TypeInfo MultiTargetBehaviour_t41_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14933_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMultiTargetBehaviour_t41_m33006_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.MultiTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.MultiTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMultiTargetBehaviour_t41_m33006(__this, p0, method) (MultiTargetBehaviour_t41 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2891____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2891, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2891____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2891, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2891_FieldInfos[] =
{
	&InternalEnumerator_1_t2891____array_0_FieldInfo,
	&InternalEnumerator_1_t2891____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2891____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2891_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2891____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2891_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14933_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2891_PropertyInfos[] =
{
	&InternalEnumerator_1_t2891____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2891____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2891_InternalEnumerator_1__ctor_m14929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14929_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14929_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2891_InternalEnumerator_1__ctor_m14929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14929_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14931_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14931_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14931_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14932_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14932_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14932_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14933_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.MultiTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14933_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetBehaviour_t41_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14933_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2891_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14929_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_MethodInfo,
	&InternalEnumerator_1_Dispose_m14931_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14932_MethodInfo,
	&InternalEnumerator_1_get_Current_m14933_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14932_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14931_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2891_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14930_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14932_MethodInfo,
	&InternalEnumerator_1_Dispose_m14931_MethodInfo,
	&InternalEnumerator_1_get_Current_m14933_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2891_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6053_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2891_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6053_il2cpp_TypeInfo, 7},
};
extern TypeInfo MultiTargetBehaviour_t41_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2891_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14933_MethodInfo/* Method Usage */,
	&MultiTargetBehaviour_t41_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMultiTargetBehaviour_t41_m33006_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2891_0_0_0;
extern Il2CppType InternalEnumerator_1_t2891_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2891_GenericClass;
TypeInfo InternalEnumerator_1_t2891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2891_MethodInfos/* methods */
	, InternalEnumerator_1_t2891_PropertyInfos/* properties */
	, InternalEnumerator_1_t2891_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2891_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2891_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2891_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2891_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2891_1_0_0/* this_arg */
	, InternalEnumerator_1_t2891_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2891_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2891_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2891)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7706_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m43289_MethodInfo;
static PropertyInfo ICollection_1_t7706____Count_PropertyInfo = 
{
	&ICollection_1_t7706_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43289_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43290_MethodInfo;
static PropertyInfo ICollection_1_t7706____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7706_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43290_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7706_PropertyInfos[] =
{
	&ICollection_1_t7706____Count_PropertyInfo,
	&ICollection_1_t7706____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43289_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43289_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43289_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43290_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43290_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43290_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo ICollection_1_t7706_ICollection_1_Add_m43291_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43291_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43291_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7706_ICollection_1_Add_m43291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43291_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43292_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43292_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43292_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo ICollection_1_t7706_ICollection_1_Contains_m43293_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43293_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43293_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7706_ICollection_1_Contains_m43293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43293_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviourU5BU5D_t5372_0_0_0;
extern Il2CppType MultiTargetBehaviourU5BU5D_t5372_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7706_ICollection_1_CopyTo_m43294_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviourU5BU5D_t5372_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43294_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43294_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7706_ICollection_1_CopyTo_m43294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43294_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo ICollection_1_t7706_ICollection_1_Remove_m43295_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43295_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43295_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7706_ICollection_1_Remove_m43295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43295_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7706_MethodInfos[] =
{
	&ICollection_1_get_Count_m43289_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43290_MethodInfo,
	&ICollection_1_Add_m43291_MethodInfo,
	&ICollection_1_Clear_m43292_MethodInfo,
	&ICollection_1_Contains_m43293_MethodInfo,
	&ICollection_1_CopyTo_m43294_MethodInfo,
	&ICollection_1_Remove_m43295_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7708_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7706_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7708_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7706_0_0_0;
extern Il2CppType ICollection_1_t7706_1_0_0;
struct ICollection_1_t7706;
extern Il2CppGenericClass ICollection_1_t7706_GenericClass;
TypeInfo ICollection_1_t7706_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7706_MethodInfos/* methods */
	, ICollection_1_t7706_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7706_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7706_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7706_0_0_0/* byval_arg */
	, &ICollection_1_t7706_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7706_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MultiTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.MultiTargetBehaviour>
extern Il2CppType IEnumerator_1_t6053_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43296_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MultiTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43296_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7708_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6053_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43296_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7708_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43296_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7708_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7708_0_0_0;
extern Il2CppType IEnumerable_1_t7708_1_0_0;
struct IEnumerable_1_t7708;
extern Il2CppGenericClass IEnumerable_1_t7708_GenericClass;
TypeInfo IEnumerable_1_t7708_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7708_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7708_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7708_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7708_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7708_0_0_0/* byval_arg */
	, &IEnumerable_1_t7708_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7708_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7707_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>
extern MethodInfo IList_1_get_Item_m43297_MethodInfo;
extern MethodInfo IList_1_set_Item_m43298_MethodInfo;
static PropertyInfo IList_1_t7707____Item_PropertyInfo = 
{
	&IList_1_t7707_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43297_MethodInfo/* get */
	, &IList_1_set_Item_m43298_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7707_PropertyInfos[] =
{
	&IList_1_t7707____Item_PropertyInfo,
	NULL
};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo IList_1_t7707_IList_1_IndexOf_m43299_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43299_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43299_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7707_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7707_IList_1_IndexOf_m43299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43299_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo IList_1_t7707_IList_1_Insert_m43300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43300_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43300_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7707_IList_1_Insert_m43300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43300_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7707_IList_1_RemoveAt_m43301_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43301_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43301_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7707_IList_1_RemoveAt_m43301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43301_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7707_IList_1_get_Item_m43297_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43297_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43297_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7707_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetBehaviour_t41_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7707_IList_1_get_Item_m43297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43297_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo IList_1_t7707_IList_1_set_Item_m43298_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43298_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43298_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7707_IList_1_set_Item_m43298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43298_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7707_MethodInfos[] =
{
	&IList_1_IndexOf_m43299_MethodInfo,
	&IList_1_Insert_m43300_MethodInfo,
	&IList_1_RemoveAt_m43301_MethodInfo,
	&IList_1_get_Item_m43297_MethodInfo,
	&IList_1_set_Item_m43298_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7707_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7706_il2cpp_TypeInfo,
	&IEnumerable_1_t7708_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7707_0_0_0;
extern Il2CppType IList_1_t7707_1_0_0;
struct IList_1_t7707;
extern Il2CppGenericClass IList_1_t7707_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7707_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7707_MethodInfos/* methods */
	, IList_1_t7707_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7707_il2cpp_TypeInfo/* element_class */
	, IList_1_t7707_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7707_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7707_0_0_0/* byval_arg */
	, &IList_1_t7707_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7707_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7709_il2cpp_TypeInfo;

// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43302_MethodInfo;
static PropertyInfo ICollection_1_t7709____Count_PropertyInfo = 
{
	&ICollection_1_t7709_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43302_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43303_MethodInfo;
static PropertyInfo ICollection_1_t7709____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7709_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43303_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7709_PropertyInfos[] =
{
	&ICollection_1_t7709____Count_PropertyInfo,
	&ICollection_1_t7709____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43302_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43302_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43302_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43303_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43303_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43303_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
static ParameterInfo ICollection_1_t7709_ICollection_1_Add_m43304_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43304_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43304_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7709_ICollection_1_Add_m43304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43304_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43305_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43305_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43305_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
static ParameterInfo ICollection_1_t7709_ICollection_1_Contains_m43306_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviour_t33_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43306_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43306_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7709_ICollection_1_Contains_m43306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43306_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetAbstractBehaviourU5BU5D_t5635_0_0_0;
extern Il2CppType MultiTargetAbstractBehaviourU5BU5D_t5635_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7709_ICollection_1_CopyTo_m43307_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviourU5BU5D_t5635_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43307_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43307_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7709_ICollection_1_CopyTo_m43307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43307_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
static ParameterInfo ICollection_1_t7709_ICollection_1_Remove_m43308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviour_t33_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43308_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.MultiTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43308_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7709_ICollection_1_Remove_m43308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43308_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7709_MethodInfos[] =
{
	&ICollection_1_get_Count_m43302_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43303_MethodInfo,
	&ICollection_1_Add_m43304_MethodInfo,
	&ICollection_1_Clear_m43305_MethodInfo,
	&ICollection_1_Contains_m43306_MethodInfo,
	&ICollection_1_CopyTo_m43307_MethodInfo,
	&ICollection_1_Remove_m43308_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7711_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7709_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7711_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7709_0_0_0;
extern Il2CppType ICollection_1_t7709_1_0_0;
struct ICollection_1_t7709;
extern Il2CppGenericClass ICollection_1_t7709_GenericClass;
TypeInfo ICollection_1_t7709_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7709_MethodInfos/* methods */
	, ICollection_1_t7709_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7709_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7709_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7709_0_0_0/* byval_arg */
	, &ICollection_1_t7709_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7709_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MultiTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.MultiTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6055_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43309_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.MultiTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43309_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7711_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6055_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43309_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7711_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43309_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7711_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7711_0_0_0;
extern Il2CppType IEnumerable_1_t7711_1_0_0;
struct IEnumerable_1_t7711;
extern Il2CppGenericClass IEnumerable_1_t7711_GenericClass;
TypeInfo IEnumerable_1_t7711_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7711_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7711_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7711_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7711_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7711_0_0_0/* byval_arg */
	, &IEnumerable_1_t7711_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7711_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6055_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43310_MethodInfo;
static PropertyInfo IEnumerator_1_t6055____Current_PropertyInfo = 
{
	&IEnumerator_1_t6055_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43310_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6055_PropertyInfos[] =
{
	&IEnumerator_1_t6055____Current_PropertyInfo,
	NULL
};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43310_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43310_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6055_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetAbstractBehaviour_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43310_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6055_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43310_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6055_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6055_0_0_0;
extern Il2CppType IEnumerator_1_t6055_1_0_0;
struct IEnumerator_1_t6055;
extern Il2CppGenericClass IEnumerator_1_t6055_GenericClass;
TypeInfo IEnumerator_1_t6055_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6055_MethodInfos/* methods */
	, IEnumerator_1_t6055_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6055_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6055_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6055_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6055_0_0_0/* byval_arg */
	, &IEnumerator_1_t6055_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6055_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_66.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2892_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_66MethodDeclarations.h"

extern TypeInfo MultiTargetAbstractBehaviour_t33_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14938_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMultiTargetAbstractBehaviour_t33_m33017_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.MultiTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.MultiTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMultiTargetAbstractBehaviour_t33_m33017(__this, p0, method) (MultiTargetAbstractBehaviour_t33 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2892____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2892, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2892____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2892, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2892_FieldInfos[] =
{
	&InternalEnumerator_1_t2892____array_0_FieldInfo,
	&InternalEnumerator_1_t2892____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2892____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2892_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2892____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2892_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14938_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2892_PropertyInfos[] =
{
	&InternalEnumerator_1_t2892____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2892____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2892_InternalEnumerator_1__ctor_m14934_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14934_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14934_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2892_InternalEnumerator_1__ctor_m14934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14934_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14936_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14936_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14936_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14937_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14937_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14937_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14938_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.MultiTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14938_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetAbstractBehaviour_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14938_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2892_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14934_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_MethodInfo,
	&InternalEnumerator_1_Dispose_m14936_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14937_MethodInfo,
	&InternalEnumerator_1_get_Current_m14938_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14937_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14936_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2892_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14935_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14937_MethodInfo,
	&InternalEnumerator_1_Dispose_m14936_MethodInfo,
	&InternalEnumerator_1_get_Current_m14938_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2892_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6055_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2892_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6055_il2cpp_TypeInfo, 7},
};
extern TypeInfo MultiTargetAbstractBehaviour_t33_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2892_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14938_MethodInfo/* Method Usage */,
	&MultiTargetAbstractBehaviour_t33_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMultiTargetAbstractBehaviour_t33_m33017_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2892_0_0_0;
extern Il2CppType InternalEnumerator_1_t2892_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2892_GenericClass;
TypeInfo InternalEnumerator_1_t2892_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2892_MethodInfos/* methods */
	, InternalEnumerator_1_t2892_PropertyInfos/* properties */
	, InternalEnumerator_1_t2892_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2892_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2892_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2892_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2892_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2892_1_0_0/* this_arg */
	, InternalEnumerator_1_t2892_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2892_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2892_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2892)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7710_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43311_MethodInfo;
extern MethodInfo IList_1_set_Item_m43312_MethodInfo;
static PropertyInfo IList_1_t7710____Item_PropertyInfo = 
{
	&IList_1_t7710_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43311_MethodInfo/* get */
	, &IList_1_set_Item_m43312_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7710_PropertyInfos[] =
{
	&IList_1_t7710____Item_PropertyInfo,
	NULL
};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
static ParameterInfo IList_1_t7710_IList_1_IndexOf_m43313_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviour_t33_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43313_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43313_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7710_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7710_IList_1_IndexOf_m43313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43313_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
static ParameterInfo IList_1_t7710_IList_1_Insert_m43314_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43314_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43314_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7710_IList_1_Insert_m43314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43314_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7710_IList_1_RemoveAt_m43315_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43315_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43315_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7710_IList_1_RemoveAt_m43315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43315_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7710_IList_1_get_Item_m43311_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43311_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43311_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7710_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetAbstractBehaviour_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7710_IList_1_get_Item_m43311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43311_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MultiTargetAbstractBehaviour_t33_0_0_0;
static ParameterInfo IList_1_t7710_IList_1_set_Item_m43312_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MultiTargetAbstractBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43312_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.MultiTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43312_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7710_IList_1_set_Item_m43312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43312_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7710_MethodInfos[] =
{
	&IList_1_IndexOf_m43313_MethodInfo,
	&IList_1_Insert_m43314_MethodInfo,
	&IList_1_RemoveAt_m43315_MethodInfo,
	&IList_1_get_Item_m43311_MethodInfo,
	&IList_1_set_Item_m43312_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7710_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7709_il2cpp_TypeInfo,
	&IEnumerable_1_t7711_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7710_0_0_0;
extern Il2CppType IList_1_t7710_1_0_0;
struct IList_1_t7710;
extern Il2CppGenericClass IList_1_t7710_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7710_MethodInfos/* methods */
	, IList_1_t7710_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7710_il2cpp_TypeInfo/* element_class */
	, IList_1_t7710_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7710_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7710_0_0_0/* byval_arg */
	, &IList_1_t7710_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7710_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7712_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m43316_MethodInfo;
static PropertyInfo ICollection_1_t7712____Count_PropertyInfo = 
{
	&ICollection_1_t7712_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43317_MethodInfo;
static PropertyInfo ICollection_1_t7712____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7712_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43317_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7712_PropertyInfos[] =
{
	&ICollection_1_t7712____Count_PropertyInfo,
	&ICollection_1_t7712____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43316_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43316_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43316_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43317_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43317_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43317_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
static ParameterInfo ICollection_1_t7712_ICollection_1_Add_m43318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviour_t157_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43318_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43318_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7712_ICollection_1_Add_m43318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43318_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43319_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43319_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43319_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
static ParameterInfo ICollection_1_t7712_ICollection_1_Contains_m43320_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviour_t157_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43320_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43320_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7712_ICollection_1_Contains_m43320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43320_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMultiTargetBehaviourU5BU5D_t5636_0_0_0;
extern Il2CppType IEditorMultiTargetBehaviourU5BU5D_t5636_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7712_ICollection_1_CopyTo_m43321_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviourU5BU5D_t5636_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43321_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43321_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7712_ICollection_1_CopyTo_m43321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43321_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
static ParameterInfo ICollection_1_t7712_ICollection_1_Remove_m43322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviour_t157_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43322_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorMultiTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43322_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7712_ICollection_1_Remove_m43322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43322_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7712_MethodInfos[] =
{
	&ICollection_1_get_Count_m43316_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43317_MethodInfo,
	&ICollection_1_Add_m43318_MethodInfo,
	&ICollection_1_Clear_m43319_MethodInfo,
	&ICollection_1_Contains_m43320_MethodInfo,
	&ICollection_1_CopyTo_m43321_MethodInfo,
	&ICollection_1_Remove_m43322_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7714_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7712_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7714_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7712_0_0_0;
extern Il2CppType ICollection_1_t7712_1_0_0;
struct ICollection_1_t7712;
extern Il2CppGenericClass ICollection_1_t7712_GenericClass;
TypeInfo ICollection_1_t7712_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7712_MethodInfos/* methods */
	, ICollection_1_t7712_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7712_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7712_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7712_0_0_0/* byval_arg */
	, &ICollection_1_t7712_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7712_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorMultiTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorMultiTargetBehaviour>
extern Il2CppType IEnumerator_1_t6057_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43323_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorMultiTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43323_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7714_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6057_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43323_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7714_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43323_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7714_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7714_0_0_0;
extern Il2CppType IEnumerable_1_t7714_1_0_0;
struct IEnumerable_1_t7714;
extern Il2CppGenericClass IEnumerable_1_t7714_GenericClass;
TypeInfo IEnumerable_1_t7714_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7714_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7714_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7714_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7714_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7714_0_0_0/* byval_arg */
	, &IEnumerable_1_t7714_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7714_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6057_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43324_MethodInfo;
static PropertyInfo IEnumerator_1_t6057____Current_PropertyInfo = 
{
	&IEnumerator_1_t6057_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43324_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6057_PropertyInfos[] =
{
	&IEnumerator_1_t6057____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43324_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43324_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6057_il2cpp_TypeInfo/* declaring_type */
	, &IEditorMultiTargetBehaviour_t157_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43324_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6057_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43324_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6057_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6057_0_0_0;
extern Il2CppType IEnumerator_1_t6057_1_0_0;
struct IEnumerator_1_t6057;
extern Il2CppGenericClass IEnumerator_1_t6057_GenericClass;
TypeInfo IEnumerator_1_t6057_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6057_MethodInfos/* methods */
	, IEnumerator_1_t6057_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6057_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6057_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6057_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6057_0_0_0/* byval_arg */
	, &IEnumerator_1_t6057_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6057_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_67.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2893_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_67MethodDeclarations.h"

extern TypeInfo IEditorMultiTargetBehaviour_t157_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14943_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorMultiTargetBehaviour_t157_m33028_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorMultiTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorMultiTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorMultiTargetBehaviour_t157_m33028(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2893____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2893, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2893____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2893, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2893_FieldInfos[] =
{
	&InternalEnumerator_1_t2893____array_0_FieldInfo,
	&InternalEnumerator_1_t2893____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2893____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2893_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2893____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2893_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14943_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2893_PropertyInfos[] =
{
	&InternalEnumerator_1_t2893____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2893____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2893_InternalEnumerator_1__ctor_m14939_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14939_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14939_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2893_InternalEnumerator_1__ctor_m14939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14939_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14941_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14941_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14941_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14942_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14942_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14942_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14943_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorMultiTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14943_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* declaring_type */
	, &IEditorMultiTargetBehaviour_t157_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14943_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2893_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14939_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_MethodInfo,
	&InternalEnumerator_1_Dispose_m14941_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14942_MethodInfo,
	&InternalEnumerator_1_get_Current_m14943_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14942_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14941_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2893_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14940_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14942_MethodInfo,
	&InternalEnumerator_1_Dispose_m14941_MethodInfo,
	&InternalEnumerator_1_get_Current_m14943_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2893_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6057_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2893_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6057_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorMultiTargetBehaviour_t157_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2893_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14943_MethodInfo/* Method Usage */,
	&IEditorMultiTargetBehaviour_t157_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorMultiTargetBehaviour_t157_m33028_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2893_0_0_0;
extern Il2CppType InternalEnumerator_1_t2893_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2893_GenericClass;
TypeInfo InternalEnumerator_1_t2893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2893_MethodInfos/* methods */
	, InternalEnumerator_1_t2893_PropertyInfos/* properties */
	, InternalEnumerator_1_t2893_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2893_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2893_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2893_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2893_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2893_1_0_0/* this_arg */
	, InternalEnumerator_1_t2893_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2893_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2893_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2893)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7713_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>
extern MethodInfo IList_1_get_Item_m43325_MethodInfo;
extern MethodInfo IList_1_set_Item_m43326_MethodInfo;
static PropertyInfo IList_1_t7713____Item_PropertyInfo = 
{
	&IList_1_t7713_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43325_MethodInfo/* get */
	, &IList_1_set_Item_m43326_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7713_PropertyInfos[] =
{
	&IList_1_t7713____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
static ParameterInfo IList_1_t7713_IList_1_IndexOf_m43327_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviour_t157_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43327_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43327_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7713_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7713_IList_1_IndexOf_m43327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43327_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
static ParameterInfo IList_1_t7713_IList_1_Insert_m43328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviour_t157_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43328_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43328_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7713_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7713_IList_1_Insert_m43328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43328_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7713_IList_1_RemoveAt_m43329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43329_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43329_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7713_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7713_IList_1_RemoveAt_m43329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43329_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7713_IList_1_get_Item_m43325_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43325_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43325_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7713_il2cpp_TypeInfo/* declaring_type */
	, &IEditorMultiTargetBehaviour_t157_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7713_IList_1_get_Item_m43325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43325_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorMultiTargetBehaviour_t157_0_0_0;
static ParameterInfo IList_1_t7713_IList_1_set_Item_m43326_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorMultiTargetBehaviour_t157_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43326_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorMultiTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43326_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7713_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7713_IList_1_set_Item_m43326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43326_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7713_MethodInfos[] =
{
	&IList_1_IndexOf_m43327_MethodInfo,
	&IList_1_Insert_m43328_MethodInfo,
	&IList_1_RemoveAt_m43329_MethodInfo,
	&IList_1_get_Item_m43325_MethodInfo,
	&IList_1_set_Item_m43326_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7713_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7712_il2cpp_TypeInfo,
	&IEnumerable_1_t7714_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7713_0_0_0;
extern Il2CppType IList_1_t7713_1_0_0;
struct IList_1_t7713;
extern Il2CppGenericClass IList_1_t7713_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7713_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7713_MethodInfos/* methods */
	, IList_1_t7713_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7713_il2cpp_TypeInfo/* element_class */
	, IList_1_t7713_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7713_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7713_0_0_0/* byval_arg */
	, &IList_1_t7713_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7713_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_18.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2894_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_18MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_14.h"
extern TypeInfo InvokableCall_1_t2895_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_14MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14946_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14948_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2894____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2894_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2894, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2894_FieldInfos[] =
{
	&CachedInvokableCall_1_t2894____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2894_CachedInvokableCall_1__ctor_m14944_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14944_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14944_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2894_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2894_CachedInvokableCall_1__ctor_m14944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14944_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2894_CachedInvokableCall_1_Invoke_m14945_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14945_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14945_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2894_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2894_CachedInvokableCall_1_Invoke_m14945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14945_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2894_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14944_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14945_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14945_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14949_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2894_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14945_MethodInfo,
	&InvokableCall_1_Find_m14949_MethodInfo,
};
extern Il2CppType UnityAction_1_t2896_0_0_0;
extern TypeInfo UnityAction_1_t2896_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMultiTargetBehaviour_t41_m33038_MethodInfo;
extern TypeInfo MultiTargetBehaviour_t41_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14951_MethodInfo;
extern TypeInfo MultiTargetBehaviour_t41_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2894_RGCTXData[8] = 
{
	&UnityAction_1_t2896_0_0_0/* Type Usage */,
	&UnityAction_1_t2896_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMultiTargetBehaviour_t41_m33038_MethodInfo/* Method Usage */,
	&MultiTargetBehaviour_t41_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14951_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14946_MethodInfo/* Method Usage */,
	&MultiTargetBehaviour_t41_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14948_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2894_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2894_1_0_0;
struct CachedInvokableCall_1_t2894;
extern Il2CppGenericClass CachedInvokableCall_1_t2894_GenericClass;
TypeInfo CachedInvokableCall_1_t2894_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2894_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2894_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2894_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2894_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2894_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2894_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2894_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2894_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2894_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2894)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_21.h"
extern TypeInfo UnityAction_1_t2896_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_21MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.MultiTargetBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.MultiTargetBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMultiTargetBehaviour_t41_m33038(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>
extern Il2CppType UnityAction_1_t2896_0_0_1;
FieldInfo InvokableCall_1_t2895____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2896_0_0_1/* type */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2895, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2895_FieldInfos[] =
{
	&InvokableCall_1_t2895____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2895_InvokableCall_1__ctor_m14946_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14946_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14946_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2895_InvokableCall_1__ctor_m14946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14946_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2896_0_0_0;
static ParameterInfo InvokableCall_1_t2895_InvokableCall_1__ctor_m14947_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2896_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14947_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14947_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2895_InvokableCall_1__ctor_m14947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14947_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2895_InvokableCall_1_Invoke_m14948_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14948_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14948_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2895_InvokableCall_1_Invoke_m14948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14948_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2895_InvokableCall_1_Find_m14949_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14949_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14949_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2895_InvokableCall_1_Find_m14949_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14949_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2895_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14946_MethodInfo,
	&InvokableCall_1__ctor_m14947_MethodInfo,
	&InvokableCall_1_Invoke_m14948_MethodInfo,
	&InvokableCall_1_Find_m14949_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2895_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14948_MethodInfo,
	&InvokableCall_1_Find_m14949_MethodInfo,
};
extern TypeInfo UnityAction_1_t2896_il2cpp_TypeInfo;
extern TypeInfo MultiTargetBehaviour_t41_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2895_RGCTXData[5] = 
{
	&UnityAction_1_t2896_0_0_0/* Type Usage */,
	&UnityAction_1_t2896_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMultiTargetBehaviour_t41_m33038_MethodInfo/* Method Usage */,
	&MultiTargetBehaviour_t41_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14951_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2895_0_0_0;
extern Il2CppType InvokableCall_1_t2895_1_0_0;
struct InvokableCall_1_t2895;
extern Il2CppGenericClass InvokableCall_1_t2895_GenericClass;
TypeInfo InvokableCall_1_t2895_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2895_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2895_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2895_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2895_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2895_0_0_0/* byval_arg */
	, &InvokableCall_1_t2895_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2895_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2895_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2895)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2896_UnityAction_1__ctor_m14950_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14950_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14950_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2896_UnityAction_1__ctor_m14950_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14950_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
static ParameterInfo UnityAction_1_t2896_UnityAction_1_Invoke_m14951_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14951_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14951_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2896_UnityAction_1_Invoke_m14951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14951_GenericMethod/* genericMethod */

};
extern Il2CppType MultiTargetBehaviour_t41_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2896_UnityAction_1_BeginInvoke_m14952_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MultiTargetBehaviour_t41_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14952_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14952_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2896_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2896_UnityAction_1_BeginInvoke_m14952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14952_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2896_UnityAction_1_EndInvoke_m14953_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14953_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14953_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2896_UnityAction_1_EndInvoke_m14953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14953_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2896_MethodInfos[] =
{
	&UnityAction_1__ctor_m14950_MethodInfo,
	&UnityAction_1_Invoke_m14951_MethodInfo,
	&UnityAction_1_BeginInvoke_m14952_MethodInfo,
	&UnityAction_1_EndInvoke_m14953_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14952_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14953_MethodInfo;
static MethodInfo* UnityAction_1_t2896_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14951_MethodInfo,
	&UnityAction_1_BeginInvoke_m14952_MethodInfo,
	&UnityAction_1_EndInvoke_m14953_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2896_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2896_1_0_0;
struct UnityAction_1_t2896;
extern Il2CppGenericClass UnityAction_1_t2896_GenericClass;
TypeInfo UnityAction_1_t2896_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2896_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2896_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2896_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2896_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2896_0_0_0/* byval_arg */
	, &UnityAction_1_t2896_1_0_0/* this_arg */
	, UnityAction_1_t2896_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2896_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2896)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6059_il2cpp_TypeInfo;

// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ObjectTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ObjectTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43330_MethodInfo;
static PropertyInfo IEnumerator_1_t6059____Current_PropertyInfo = 
{
	&IEnumerator_1_t6059_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6059_PropertyInfos[] =
{
	&IEnumerator_1_t6059____Current_PropertyInfo,
	NULL
};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43330_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ObjectTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43330_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6059_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetBehaviour_t42_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43330_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6059_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43330_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6059_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6059_0_0_0;
extern Il2CppType IEnumerator_1_t6059_1_0_0;
struct IEnumerator_1_t6059;
extern Il2CppGenericClass IEnumerator_1_t6059_GenericClass;
TypeInfo IEnumerator_1_t6059_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6059_MethodInfos/* methods */
	, IEnumerator_1_t6059_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6059_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6059_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6059_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6059_0_0_0/* byval_arg */
	, &IEnumerator_1_t6059_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6059_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_68.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2897_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_68MethodDeclarations.h"

extern TypeInfo ObjectTargetBehaviour_t42_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14958_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObjectTargetBehaviour_t42_m33040_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ObjectTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ObjectTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisObjectTargetBehaviour_t42_m33040(__this, p0, method) (ObjectTargetBehaviour_t42 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2897____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2897, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2897____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2897, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2897_FieldInfos[] =
{
	&InternalEnumerator_1_t2897____array_0_FieldInfo,
	&InternalEnumerator_1_t2897____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2897____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2897_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2897____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2897_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14958_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2897_PropertyInfos[] =
{
	&InternalEnumerator_1_t2897____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2897____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2897_InternalEnumerator_1__ctor_m14954_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14954_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14954_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2897_InternalEnumerator_1__ctor_m14954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14954_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14956_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14956_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14956_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14957_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14957_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14957_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14958_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ObjectTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14958_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetBehaviour_t42_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14958_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2897_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14954_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_MethodInfo,
	&InternalEnumerator_1_Dispose_m14956_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14957_MethodInfo,
	&InternalEnumerator_1_get_Current_m14958_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14957_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14956_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2897_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14955_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14957_MethodInfo,
	&InternalEnumerator_1_Dispose_m14956_MethodInfo,
	&InternalEnumerator_1_get_Current_m14958_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2897_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6059_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2897_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6059_il2cpp_TypeInfo, 7},
};
extern TypeInfo ObjectTargetBehaviour_t42_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2897_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14958_MethodInfo/* Method Usage */,
	&ObjectTargetBehaviour_t42_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObjectTargetBehaviour_t42_m33040_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2897_0_0_0;
extern Il2CppType InternalEnumerator_1_t2897_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2897_GenericClass;
TypeInfo InternalEnumerator_1_t2897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2897_MethodInfos/* methods */
	, InternalEnumerator_1_t2897_PropertyInfos/* properties */
	, InternalEnumerator_1_t2897_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2897_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2897_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2897_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2897_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2897_1_0_0/* this_arg */
	, InternalEnumerator_1_t2897_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2897_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2897_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2897)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7715_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m43331_MethodInfo;
static PropertyInfo ICollection_1_t7715____Count_PropertyInfo = 
{
	&ICollection_1_t7715_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43331_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43332_MethodInfo;
static PropertyInfo ICollection_1_t7715____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7715_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43332_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7715_PropertyInfos[] =
{
	&ICollection_1_t7715____Count_PropertyInfo,
	&ICollection_1_t7715____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43331_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43331_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43331_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43332_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43332_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43332_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo ICollection_1_t7715_ICollection_1_Add_m43333_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43333_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43333_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7715_ICollection_1_Add_m43333_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43333_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43334_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43334_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43334_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo ICollection_1_t7715_ICollection_1_Contains_m43335_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43335_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43335_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7715_ICollection_1_Contains_m43335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43335_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviourU5BU5D_t5373_0_0_0;
extern Il2CppType ObjectTargetBehaviourU5BU5D_t5373_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7715_ICollection_1_CopyTo_m43336_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviourU5BU5D_t5373_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43336_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43336_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7715_ICollection_1_CopyTo_m43336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43336_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo ICollection_1_t7715_ICollection_1_Remove_m43337_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43337_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43337_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7715_ICollection_1_Remove_m43337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43337_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7715_MethodInfos[] =
{
	&ICollection_1_get_Count_m43331_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43332_MethodInfo,
	&ICollection_1_Add_m43333_MethodInfo,
	&ICollection_1_Clear_m43334_MethodInfo,
	&ICollection_1_Contains_m43335_MethodInfo,
	&ICollection_1_CopyTo_m43336_MethodInfo,
	&ICollection_1_Remove_m43337_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7717_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7715_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7717_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7715_0_0_0;
extern Il2CppType ICollection_1_t7715_1_0_0;
struct ICollection_1_t7715;
extern Il2CppGenericClass ICollection_1_t7715_GenericClass;
TypeInfo ICollection_1_t7715_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7715_MethodInfos/* methods */
	, ICollection_1_t7715_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7715_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7715_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7715_0_0_0/* byval_arg */
	, &ICollection_1_t7715_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7715_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ObjectTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ObjectTargetBehaviour>
extern Il2CppType IEnumerator_1_t6059_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43338_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ObjectTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43338_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7717_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6059_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43338_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7717_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43338_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7717_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7717_0_0_0;
extern Il2CppType IEnumerable_1_t7717_1_0_0;
struct IEnumerable_1_t7717;
extern Il2CppGenericClass IEnumerable_1_t7717_GenericClass;
TypeInfo IEnumerable_1_t7717_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7717_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7717_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7717_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7717_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7717_0_0_0/* byval_arg */
	, &IEnumerable_1_t7717_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7717_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7716_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>
extern MethodInfo IList_1_get_Item_m43339_MethodInfo;
extern MethodInfo IList_1_set_Item_m43340_MethodInfo;
static PropertyInfo IList_1_t7716____Item_PropertyInfo = 
{
	&IList_1_t7716_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43339_MethodInfo/* get */
	, &IList_1_set_Item_m43340_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7716_PropertyInfos[] =
{
	&IList_1_t7716____Item_PropertyInfo,
	NULL
};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo IList_1_t7716_IList_1_IndexOf_m43341_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43341_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43341_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7716_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7716_IList_1_IndexOf_m43341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43341_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo IList_1_t7716_IList_1_Insert_m43342_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43342_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43342_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7716_IList_1_Insert_m43342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43342_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7716_IList_1_RemoveAt_m43343_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43343_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43343_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7716_IList_1_RemoveAt_m43343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43343_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7716_IList_1_get_Item_m43339_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43339_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43339_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7716_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetBehaviour_t42_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7716_IList_1_get_Item_m43339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43339_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo IList_1_t7716_IList_1_set_Item_m43340_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43340_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43340_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7716_IList_1_set_Item_m43340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43340_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7716_MethodInfos[] =
{
	&IList_1_IndexOf_m43341_MethodInfo,
	&IList_1_Insert_m43342_MethodInfo,
	&IList_1_RemoveAt_m43343_MethodInfo,
	&IList_1_get_Item_m43339_MethodInfo,
	&IList_1_set_Item_m43340_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7716_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7715_il2cpp_TypeInfo,
	&IEnumerable_1_t7717_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7716_0_0_0;
extern Il2CppType IList_1_t7716_1_0_0;
struct IList_1_t7716;
extern Il2CppGenericClass IList_1_t7716_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7716_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7716_MethodInfos/* methods */
	, IList_1_t7716_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7716_il2cpp_TypeInfo/* element_class */
	, IList_1_t7716_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7716_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7716_0_0_0/* byval_arg */
	, &IList_1_t7716_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7716_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7718_il2cpp_TypeInfo;

// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43344_MethodInfo;
static PropertyInfo ICollection_1_t7718____Count_PropertyInfo = 
{
	&ICollection_1_t7718_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43344_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43345_MethodInfo;
static PropertyInfo ICollection_1_t7718____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7718_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43345_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7718_PropertyInfos[] =
{
	&ICollection_1_t7718____Count_PropertyInfo,
	&ICollection_1_t7718____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43344_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43344_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43344_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43345_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43345_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43345_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
static ParameterInfo ICollection_1_t7718_ICollection_1_Add_m43346_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43346_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43346_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7718_ICollection_1_Add_m43346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43346_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43347_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43347_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43347_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
static ParameterInfo ICollection_1_t7718_ICollection_1_Contains_m43348_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43348_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43348_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7718_ICollection_1_Contains_m43348_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43348_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetAbstractBehaviourU5BU5D_t5637_0_0_0;
extern Il2CppType ObjectTargetAbstractBehaviourU5BU5D_t5637_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7718_ICollection_1_CopyTo_m43349_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviourU5BU5D_t5637_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43349_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43349_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7718_ICollection_1_CopyTo_m43349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43349_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
static ParameterInfo ICollection_1_t7718_ICollection_1_Remove_m43350_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43350_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ObjectTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43350_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7718_ICollection_1_Remove_m43350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43350_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7718_MethodInfos[] =
{
	&ICollection_1_get_Count_m43344_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43345_MethodInfo,
	&ICollection_1_Add_m43346_MethodInfo,
	&ICollection_1_Clear_m43347_MethodInfo,
	&ICollection_1_Contains_m43348_MethodInfo,
	&ICollection_1_CopyTo_m43349_MethodInfo,
	&ICollection_1_Remove_m43350_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7720_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7718_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7720_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7718_0_0_0;
extern Il2CppType ICollection_1_t7718_1_0_0;
struct ICollection_1_t7718;
extern Il2CppGenericClass ICollection_1_t7718_GenericClass;
TypeInfo ICollection_1_t7718_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7718_MethodInfos/* methods */
	, ICollection_1_t7718_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7718_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7718_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7718_0_0_0/* byval_arg */
	, &ICollection_1_t7718_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7718_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ObjectTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ObjectTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6061_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43351_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ObjectTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43351_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7720_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43351_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7720_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43351_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7720_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7720_0_0_0;
extern Il2CppType IEnumerable_1_t7720_1_0_0;
struct IEnumerable_1_t7720;
extern Il2CppGenericClass IEnumerable_1_t7720_GenericClass;
TypeInfo IEnumerable_1_t7720_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7720_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7720_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7720_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7720_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7720_0_0_0/* byval_arg */
	, &IEnumerable_1_t7720_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7720_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6061_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43352_MethodInfo;
static PropertyInfo IEnumerator_1_t6061____Current_PropertyInfo = 
{
	&IEnumerator_1_t6061_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43352_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6061_PropertyInfos[] =
{
	&IEnumerator_1_t6061____Current_PropertyInfo,
	NULL
};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43352_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43352_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6061_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43352_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6061_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43352_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6061_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6061_0_0_0;
extern Il2CppType IEnumerator_1_t6061_1_0_0;
struct IEnumerator_1_t6061;
extern Il2CppGenericClass IEnumerator_1_t6061_GenericClass;
TypeInfo IEnumerator_1_t6061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6061_MethodInfos/* methods */
	, IEnumerator_1_t6061_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6061_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6061_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6061_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6061_0_0_0/* byval_arg */
	, &IEnumerator_1_t6061_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6061_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_69.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2898_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_69MethodDeclarations.h"

extern TypeInfo ObjectTargetAbstractBehaviour_t36_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14963_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObjectTargetAbstractBehaviour_t36_m33051_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ObjectTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ObjectTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisObjectTargetAbstractBehaviour_t36_m33051(__this, p0, method) (ObjectTargetAbstractBehaviour_t36 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2898____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2898, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2898____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2898, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2898_FieldInfos[] =
{
	&InternalEnumerator_1_t2898____array_0_FieldInfo,
	&InternalEnumerator_1_t2898____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2898____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2898_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2898____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2898_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2898_PropertyInfos[] =
{
	&InternalEnumerator_1_t2898____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2898____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2898_InternalEnumerator_1__ctor_m14959_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14959_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14959_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2898_InternalEnumerator_1__ctor_m14959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14959_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14961_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14961_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14961_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14962_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14962_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14962_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14963_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14963_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14963_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2898_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14959_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_MethodInfo,
	&InternalEnumerator_1_Dispose_m14961_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14962_MethodInfo,
	&InternalEnumerator_1_get_Current_m14963_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14962_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14961_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2898_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14960_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14962_MethodInfo,
	&InternalEnumerator_1_Dispose_m14961_MethodInfo,
	&InternalEnumerator_1_get_Current_m14963_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2898_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6061_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2898_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6061_il2cpp_TypeInfo, 7},
};
extern TypeInfo ObjectTargetAbstractBehaviour_t36_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2898_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14963_MethodInfo/* Method Usage */,
	&ObjectTargetAbstractBehaviour_t36_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObjectTargetAbstractBehaviour_t36_m33051_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2898_0_0_0;
extern Il2CppType InternalEnumerator_1_t2898_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2898_GenericClass;
TypeInfo InternalEnumerator_1_t2898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2898_MethodInfos/* methods */
	, InternalEnumerator_1_t2898_PropertyInfos/* properties */
	, InternalEnumerator_1_t2898_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2898_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2898_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2898_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2898_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2898_1_0_0/* this_arg */
	, InternalEnumerator_1_t2898_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2898_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2898_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2898)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7719_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43353_MethodInfo;
extern MethodInfo IList_1_set_Item_m43354_MethodInfo;
static PropertyInfo IList_1_t7719____Item_PropertyInfo = 
{
	&IList_1_t7719_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43353_MethodInfo/* get */
	, &IList_1_set_Item_m43354_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7719_PropertyInfos[] =
{
	&IList_1_t7719____Item_PropertyInfo,
	NULL
};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
static ParameterInfo IList_1_t7719_IList_1_IndexOf_m43355_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43355_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43355_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7719_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7719_IList_1_IndexOf_m43355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43355_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
static ParameterInfo IList_1_t7719_IList_1_Insert_m43356_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43356_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43356_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7719_IList_1_Insert_m43356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43356_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7719_IList_1_RemoveAt_m43357_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43357_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43357_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7719_IList_1_RemoveAt_m43357_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43357_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7719_IList_1_get_Item_m43353_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43353_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43353_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7719_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7719_IList_1_get_Item_m43353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43353_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ObjectTargetAbstractBehaviour_t36_0_0_0;
static ParameterInfo IList_1_t7719_IList_1_set_Item_m43354_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ObjectTargetAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43354_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ObjectTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43354_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7719_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7719_IList_1_set_Item_m43354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43354_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7719_MethodInfos[] =
{
	&IList_1_IndexOf_m43355_MethodInfo,
	&IList_1_Insert_m43356_MethodInfo,
	&IList_1_RemoveAt_m43357_MethodInfo,
	&IList_1_get_Item_m43353_MethodInfo,
	&IList_1_set_Item_m43354_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7719_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7718_il2cpp_TypeInfo,
	&IEnumerable_1_t7720_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7719_0_0_0;
extern Il2CppType IList_1_t7719_1_0_0;
struct IList_1_t7719;
extern Il2CppGenericClass IList_1_t7719_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7719_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7719_MethodInfos/* methods */
	, IList_1_t7719_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7719_il2cpp_TypeInfo/* element_class */
	, IList_1_t7719_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7719_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7719_0_0_0/* byval_arg */
	, &IList_1_t7719_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7719_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7721_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m43358_MethodInfo;
static PropertyInfo ICollection_1_t7721____Count_PropertyInfo = 
{
	&ICollection_1_t7721_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43359_MethodInfo;
static PropertyInfo ICollection_1_t7721____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7721_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43359_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7721_PropertyInfos[] =
{
	&ICollection_1_t7721____Count_PropertyInfo,
	&ICollection_1_t7721____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43358_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43358_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43358_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43359_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43359_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43359_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
static ParameterInfo ICollection_1_t7721_ICollection_1_Add_m43360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviour_t158_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43360_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43360_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7721_ICollection_1_Add_m43360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43360_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43361_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43361_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43361_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
static ParameterInfo ICollection_1_t7721_ICollection_1_Contains_m43362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviour_t158_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43362_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43362_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7721_ICollection_1_Contains_m43362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43362_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorObjectTargetBehaviourU5BU5D_t5638_0_0_0;
extern Il2CppType IEditorObjectTargetBehaviourU5BU5D_t5638_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7721_ICollection_1_CopyTo_m43363_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviourU5BU5D_t5638_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43363_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43363_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7721_ICollection_1_CopyTo_m43363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43363_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
static ParameterInfo ICollection_1_t7721_ICollection_1_Remove_m43364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviour_t158_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43364_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorObjectTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43364_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7721_ICollection_1_Remove_m43364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43364_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7721_MethodInfos[] =
{
	&ICollection_1_get_Count_m43358_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43359_MethodInfo,
	&ICollection_1_Add_m43360_MethodInfo,
	&ICollection_1_Clear_m43361_MethodInfo,
	&ICollection_1_Contains_m43362_MethodInfo,
	&ICollection_1_CopyTo_m43363_MethodInfo,
	&ICollection_1_Remove_m43364_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7723_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7721_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7723_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7721_0_0_0;
extern Il2CppType ICollection_1_t7721_1_0_0;
struct ICollection_1_t7721;
extern Il2CppGenericClass ICollection_1_t7721_GenericClass;
TypeInfo ICollection_1_t7721_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7721_MethodInfos/* methods */
	, ICollection_1_t7721_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7721_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7721_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7721_0_0_0/* byval_arg */
	, &ICollection_1_t7721_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7721_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorObjectTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorObjectTargetBehaviour>
extern Il2CppType IEnumerator_1_t6063_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43365_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorObjectTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43365_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7723_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6063_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43365_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7723_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43365_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7723_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7723_0_0_0;
extern Il2CppType IEnumerable_1_t7723_1_0_0;
struct IEnumerable_1_t7723;
extern Il2CppGenericClass IEnumerable_1_t7723_GenericClass;
TypeInfo IEnumerable_1_t7723_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7723_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7723_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7723_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7723_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7723_0_0_0/* byval_arg */
	, &IEnumerable_1_t7723_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7723_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6063_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43366_MethodInfo;
static PropertyInfo IEnumerator_1_t6063____Current_PropertyInfo = 
{
	&IEnumerator_1_t6063_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43366_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6063_PropertyInfos[] =
{
	&IEnumerator_1_t6063____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43366_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43366_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6063_il2cpp_TypeInfo/* declaring_type */
	, &IEditorObjectTargetBehaviour_t158_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43366_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6063_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43366_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6063_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6063_0_0_0;
extern Il2CppType IEnumerator_1_t6063_1_0_0;
struct IEnumerator_1_t6063;
extern Il2CppGenericClass IEnumerator_1_t6063_GenericClass;
TypeInfo IEnumerator_1_t6063_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6063_MethodInfos/* methods */
	, IEnumerator_1_t6063_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6063_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6063_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6063_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6063_0_0_0/* byval_arg */
	, &IEnumerator_1_t6063_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6063_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_70.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2899_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_70MethodDeclarations.h"

extern TypeInfo IEditorObjectTargetBehaviour_t158_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14968_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorObjectTargetBehaviour_t158_m33062_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorObjectTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorObjectTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorObjectTargetBehaviour_t158_m33062(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2899____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2899, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2899____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2899, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2899_FieldInfos[] =
{
	&InternalEnumerator_1_t2899____array_0_FieldInfo,
	&InternalEnumerator_1_t2899____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2899____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2899_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2899____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2899_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14968_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2899_PropertyInfos[] =
{
	&InternalEnumerator_1_t2899____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2899____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2899_InternalEnumerator_1__ctor_m14964_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14964_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14964_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2899_InternalEnumerator_1__ctor_m14964_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14964_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14966_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14966_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14966_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14967_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14967_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14967_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14968_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14968_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* declaring_type */
	, &IEditorObjectTargetBehaviour_t158_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14968_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2899_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14964_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_MethodInfo,
	&InternalEnumerator_1_Dispose_m14966_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14967_MethodInfo,
	&InternalEnumerator_1_get_Current_m14968_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14967_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14966_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2899_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14965_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14967_MethodInfo,
	&InternalEnumerator_1_Dispose_m14966_MethodInfo,
	&InternalEnumerator_1_get_Current_m14968_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2899_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6063_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2899_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6063_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorObjectTargetBehaviour_t158_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2899_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14968_MethodInfo/* Method Usage */,
	&IEditorObjectTargetBehaviour_t158_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorObjectTargetBehaviour_t158_m33062_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2899_0_0_0;
extern Il2CppType InternalEnumerator_1_t2899_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2899_GenericClass;
TypeInfo InternalEnumerator_1_t2899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2899_MethodInfos/* methods */
	, InternalEnumerator_1_t2899_PropertyInfos/* properties */
	, InternalEnumerator_1_t2899_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2899_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2899_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2899_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2899_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2899_1_0_0/* this_arg */
	, InternalEnumerator_1_t2899_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2899_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2899_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2899)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7722_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>
extern MethodInfo IList_1_get_Item_m43367_MethodInfo;
extern MethodInfo IList_1_set_Item_m43368_MethodInfo;
static PropertyInfo IList_1_t7722____Item_PropertyInfo = 
{
	&IList_1_t7722_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43367_MethodInfo/* get */
	, &IList_1_set_Item_m43368_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7722_PropertyInfos[] =
{
	&IList_1_t7722____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
static ParameterInfo IList_1_t7722_IList_1_IndexOf_m43369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviour_t158_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43369_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43369_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7722_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7722_IList_1_IndexOf_m43369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43369_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
static ParameterInfo IList_1_t7722_IList_1_Insert_m43370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviour_t158_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43370_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43370_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7722_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7722_IList_1_Insert_m43370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43370_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7722_IList_1_RemoveAt_m43371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43371_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43371_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7722_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7722_IList_1_RemoveAt_m43371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43371_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7722_IList_1_get_Item_m43367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43367_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43367_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7722_il2cpp_TypeInfo/* declaring_type */
	, &IEditorObjectTargetBehaviour_t158_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7722_IList_1_get_Item_m43367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43367_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorObjectTargetBehaviour_t158_0_0_0;
static ParameterInfo IList_1_t7722_IList_1_set_Item_m43368_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorObjectTargetBehaviour_t158_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43368_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorObjectTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43368_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7722_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7722_IList_1_set_Item_m43368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43368_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7722_MethodInfos[] =
{
	&IList_1_IndexOf_m43369_MethodInfo,
	&IList_1_Insert_m43370_MethodInfo,
	&IList_1_RemoveAt_m43371_MethodInfo,
	&IList_1_get_Item_m43367_MethodInfo,
	&IList_1_set_Item_m43368_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7722_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7721_il2cpp_TypeInfo,
	&IEnumerable_1_t7723_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7722_0_0_0;
extern Il2CppType IList_1_t7722_1_0_0;
struct IList_1_t7722;
extern Il2CppGenericClass IList_1_t7722_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7722_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7722_MethodInfos/* methods */
	, IList_1_t7722_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7722_il2cpp_TypeInfo/* element_class */
	, IList_1_t7722_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7722_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7722_0_0_0/* byval_arg */
	, &IList_1_t7722_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7722_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_19.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2900_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_19MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_15.h"
extern TypeInfo InvokableCall_1_t2901_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_15MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14971_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14973_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2900____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2900_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2900, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2900_FieldInfos[] =
{
	&CachedInvokableCall_1_t2900____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2900_CachedInvokableCall_1__ctor_m14969_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14969_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2900_CachedInvokableCall_1__ctor_m14969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14969_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2900_CachedInvokableCall_1_Invoke_m14970_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14970_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14970_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2900_CachedInvokableCall_1_Invoke_m14970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14970_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2900_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14969_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14970_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14970_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14974_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2900_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14970_MethodInfo,
	&InvokableCall_1_Find_m14974_MethodInfo,
};
extern Il2CppType UnityAction_1_t2902_0_0_0;
extern TypeInfo UnityAction_1_t2902_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisObjectTargetBehaviour_t42_m33072_MethodInfo;
extern TypeInfo ObjectTargetBehaviour_t42_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14976_MethodInfo;
extern TypeInfo ObjectTargetBehaviour_t42_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2900_RGCTXData[8] = 
{
	&UnityAction_1_t2902_0_0_0/* Type Usage */,
	&UnityAction_1_t2902_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObjectTargetBehaviour_t42_m33072_MethodInfo/* Method Usage */,
	&ObjectTargetBehaviour_t42_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14976_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14971_MethodInfo/* Method Usage */,
	&ObjectTargetBehaviour_t42_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14973_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2900_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2900_1_0_0;
struct CachedInvokableCall_1_t2900;
extern Il2CppGenericClass CachedInvokableCall_1_t2900_GenericClass;
TypeInfo CachedInvokableCall_1_t2900_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2900_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2900_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2900_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2900_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2900_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2900_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2900_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2900_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2900_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2900)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_22.h"
extern TypeInfo UnityAction_1_t2902_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_22MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ObjectTargetBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ObjectTargetBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisObjectTargetBehaviour_t42_m33072(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>
extern Il2CppType UnityAction_1_t2902_0_0_1;
FieldInfo InvokableCall_1_t2901____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2902_0_0_1/* type */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2901, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2901_FieldInfos[] =
{
	&InvokableCall_1_t2901____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2901_InvokableCall_1__ctor_m14971_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14971_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14971_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2901_InvokableCall_1__ctor_m14971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14971_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2902_0_0_0;
static ParameterInfo InvokableCall_1_t2901_InvokableCall_1__ctor_m14972_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2902_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14972_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14972_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2901_InvokableCall_1__ctor_m14972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14972_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2901_InvokableCall_1_Invoke_m14973_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14973_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14973_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2901_InvokableCall_1_Invoke_m14973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14973_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2901_InvokableCall_1_Find_m14974_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14974_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14974_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2901_InvokableCall_1_Find_m14974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14974_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2901_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14971_MethodInfo,
	&InvokableCall_1__ctor_m14972_MethodInfo,
	&InvokableCall_1_Invoke_m14973_MethodInfo,
	&InvokableCall_1_Find_m14974_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2901_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14973_MethodInfo,
	&InvokableCall_1_Find_m14974_MethodInfo,
};
extern TypeInfo UnityAction_1_t2902_il2cpp_TypeInfo;
extern TypeInfo ObjectTargetBehaviour_t42_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2901_RGCTXData[5] = 
{
	&UnityAction_1_t2902_0_0_0/* Type Usage */,
	&UnityAction_1_t2902_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObjectTargetBehaviour_t42_m33072_MethodInfo/* Method Usage */,
	&ObjectTargetBehaviour_t42_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14976_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2901_0_0_0;
extern Il2CppType InvokableCall_1_t2901_1_0_0;
struct InvokableCall_1_t2901;
extern Il2CppGenericClass InvokableCall_1_t2901_GenericClass;
TypeInfo InvokableCall_1_t2901_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2901_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2901_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2901_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2901_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2901_0_0_0/* byval_arg */
	, &InvokableCall_1_t2901_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2901_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2901_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2901)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2902_UnityAction_1__ctor_m14975_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14975_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14975_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2902_UnityAction_1__ctor_m14975_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14975_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
static ParameterInfo UnityAction_1_t2902_UnityAction_1_Invoke_m14976_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14976_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14976_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2902_UnityAction_1_Invoke_m14976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14976_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectTargetBehaviour_t42_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2902_UnityAction_1_BeginInvoke_m14977_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ObjectTargetBehaviour_t42_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14977_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14977_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2902_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2902_UnityAction_1_BeginInvoke_m14977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14977_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2902_UnityAction_1_EndInvoke_m14978_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14978_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14978_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2902_UnityAction_1_EndInvoke_m14978_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14978_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2902_MethodInfos[] =
{
	&UnityAction_1__ctor_m14975_MethodInfo,
	&UnityAction_1_Invoke_m14976_MethodInfo,
	&UnityAction_1_BeginInvoke_m14977_MethodInfo,
	&UnityAction_1_EndInvoke_m14978_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14977_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14978_MethodInfo;
static MethodInfo* UnityAction_1_t2902_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14976_MethodInfo,
	&UnityAction_1_BeginInvoke_m14977_MethodInfo,
	&UnityAction_1_EndInvoke_m14978_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2902_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2902_1_0_0;
struct UnityAction_1_t2902;
extern Il2CppGenericClass UnityAction_1_t2902_GenericClass;
TypeInfo UnityAction_1_t2902_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2902_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2902_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2902_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2902_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2902_0_0_0/* byval_arg */
	, &UnityAction_1_t2902_1_0_0/* this_arg */
	, UnityAction_1_t2902_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2902_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2902)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6065_il2cpp_TypeInfo;

// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.PropBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.PropBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43372_MethodInfo;
static PropertyInfo IEnumerator_1_t6065____Current_PropertyInfo = 
{
	&IEnumerator_1_t6065_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6065_PropertyInfos[] =
{
	&IEnumerator_1_t6065____Current_PropertyInfo,
	NULL
};
extern Il2CppType PropBehaviour_t12_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43372_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.PropBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43372_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6065_il2cpp_TypeInfo/* declaring_type */
	, &PropBehaviour_t12_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43372_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6065_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43372_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6065_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6065_0_0_0;
extern Il2CppType IEnumerator_1_t6065_1_0_0;
struct IEnumerator_1_t6065;
extern Il2CppGenericClass IEnumerator_1_t6065_GenericClass;
TypeInfo IEnumerator_1_t6065_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6065_MethodInfos/* methods */
	, IEnumerator_1_t6065_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6065_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6065_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6065_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6065_0_0_0/* byval_arg */
	, &IEnumerator_1_t6065_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6065_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_71.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2903_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_71MethodDeclarations.h"

extern TypeInfo PropBehaviour_t12_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14983_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPropBehaviour_t12_m33074_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.PropBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.PropBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisPropBehaviour_t12_m33074(__this, p0, method) (PropBehaviour_t12 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2903____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2903, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2903____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2903, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2903_FieldInfos[] =
{
	&InternalEnumerator_1_t2903____array_0_FieldInfo,
	&InternalEnumerator_1_t2903____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2903____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2903_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2903____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2903_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14983_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2903_PropertyInfos[] =
{
	&InternalEnumerator_1_t2903____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2903____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2903_InternalEnumerator_1__ctor_m14979_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14979_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14979_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2903_InternalEnumerator_1__ctor_m14979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14979_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14981_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14981_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14981_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14982_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14982_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14982_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t12_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14983_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.PropBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14983_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* declaring_type */
	, &PropBehaviour_t12_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14983_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2903_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14979_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_MethodInfo,
	&InternalEnumerator_1_Dispose_m14981_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14982_MethodInfo,
	&InternalEnumerator_1_get_Current_m14983_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14982_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14981_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2903_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14980_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14982_MethodInfo,
	&InternalEnumerator_1_Dispose_m14981_MethodInfo,
	&InternalEnumerator_1_get_Current_m14983_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2903_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6065_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2903_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6065_il2cpp_TypeInfo, 7},
};
extern TypeInfo PropBehaviour_t12_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2903_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14983_MethodInfo/* Method Usage */,
	&PropBehaviour_t12_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPropBehaviour_t12_m33074_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2903_0_0_0;
extern Il2CppType InternalEnumerator_1_t2903_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2903_GenericClass;
TypeInfo InternalEnumerator_1_t2903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2903_MethodInfos/* methods */
	, InternalEnumerator_1_t2903_PropertyInfos/* properties */
	, InternalEnumerator_1_t2903_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2903_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2903_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2903_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2903_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2903_1_0_0/* this_arg */
	, InternalEnumerator_1_t2903_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2903_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2903_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2903)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7724_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>
extern MethodInfo ICollection_1_get_Count_m43373_MethodInfo;
static PropertyInfo ICollection_1_t7724____Count_PropertyInfo = 
{
	&ICollection_1_t7724_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43374_MethodInfo;
static PropertyInfo ICollection_1_t7724____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7724_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43374_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7724_PropertyInfos[] =
{
	&ICollection_1_t7724____Count_PropertyInfo,
	&ICollection_1_t7724____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43373_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43373_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43373_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43374_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43374_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43374_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t12_0_0_0;
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo ICollection_1_t7724_ICollection_1_Add_m43375_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43375_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43375_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7724_ICollection_1_Add_m43375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43375_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43376_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43376_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43376_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo ICollection_1_t7724_ICollection_1_Contains_m43377_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43377_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43377_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7724_ICollection_1_Contains_m43377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43377_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviourU5BU5D_t5374_0_0_0;
extern Il2CppType PropBehaviourU5BU5D_t5374_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7724_ICollection_1_CopyTo_m43378_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviourU5BU5D_t5374_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43378_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43378_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7724_ICollection_1_CopyTo_m43378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43378_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo ICollection_1_t7724_ICollection_1_Remove_m43379_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43379_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43379_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7724_ICollection_1_Remove_m43379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43379_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7724_MethodInfos[] =
{
	&ICollection_1_get_Count_m43373_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43374_MethodInfo,
	&ICollection_1_Add_m43375_MethodInfo,
	&ICollection_1_Clear_m43376_MethodInfo,
	&ICollection_1_Contains_m43377_MethodInfo,
	&ICollection_1_CopyTo_m43378_MethodInfo,
	&ICollection_1_Remove_m43379_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7726_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7724_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7726_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7724_0_0_0;
extern Il2CppType ICollection_1_t7724_1_0_0;
struct ICollection_1_t7724;
extern Il2CppGenericClass ICollection_1_t7724_GenericClass;
TypeInfo ICollection_1_t7724_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7724_MethodInfos/* methods */
	, ICollection_1_t7724_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7724_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7724_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7724_0_0_0/* byval_arg */
	, &ICollection_1_t7724_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7724_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.PropBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.PropBehaviour>
extern Il2CppType IEnumerator_1_t6065_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43380_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.PropBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43380_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7726_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6065_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43380_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7726_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43380_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7726_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7726_0_0_0;
extern Il2CppType IEnumerable_1_t7726_1_0_0;
struct IEnumerable_1_t7726;
extern Il2CppGenericClass IEnumerable_1_t7726_GenericClass;
TypeInfo IEnumerable_1_t7726_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7726_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7726_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7726_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7726_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7726_0_0_0/* byval_arg */
	, &IEnumerable_1_t7726_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7726_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7725_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.PropBehaviour>
extern MethodInfo IList_1_get_Item_m43381_MethodInfo;
extern MethodInfo IList_1_set_Item_m43382_MethodInfo;
static PropertyInfo IList_1_t7725____Item_PropertyInfo = 
{
	&IList_1_t7725_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43381_MethodInfo/* get */
	, &IList_1_set_Item_m43382_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7725_PropertyInfos[] =
{
	&IList_1_t7725____Item_PropertyInfo,
	NULL
};
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo IList_1_t7725_IList_1_IndexOf_m43383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43383_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43383_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7725_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7725_IList_1_IndexOf_m43383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43383_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo IList_1_t7725_IList_1_Insert_m43384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43384_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43384_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7725_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7725_IList_1_Insert_m43384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7725_IList_1_RemoveAt_m43385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43385_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43385_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7725_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7725_IList_1_RemoveAt_m43385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43385_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7725_IList_1_get_Item_m43381_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PropBehaviour_t12_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43381_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43381_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7725_il2cpp_TypeInfo/* declaring_type */
	, &PropBehaviour_t12_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7725_IList_1_get_Item_m43381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43381_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo IList_1_t7725_IList_1_set_Item_m43382_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43382_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.PropBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43382_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7725_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7725_IList_1_set_Item_m43382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43382_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7725_MethodInfos[] =
{
	&IList_1_IndexOf_m43383_MethodInfo,
	&IList_1_Insert_m43384_MethodInfo,
	&IList_1_RemoveAt_m43385_MethodInfo,
	&IList_1_get_Item_m43381_MethodInfo,
	&IList_1_set_Item_m43382_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7725_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7724_il2cpp_TypeInfo,
	&IEnumerable_1_t7726_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7725_0_0_0;
extern Il2CppType IList_1_t7725_1_0_0;
struct IList_1_t7725;
extern Il2CppGenericClass IList_1_t7725_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7725_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7725_MethodInfos/* methods */
	, IList_1_t7725_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7725_il2cpp_TypeInfo/* element_class */
	, IList_1_t7725_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7725_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7725_0_0_0/* byval_arg */
	, &IList_1_t7725_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7725_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7727_il2cpp_TypeInfo;

// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43386_MethodInfo;
static PropertyInfo ICollection_1_t7727____Count_PropertyInfo = 
{
	&ICollection_1_t7727_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43387_MethodInfo;
static PropertyInfo ICollection_1_t7727____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7727_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7727_PropertyInfos[] =
{
	&ICollection_1_t7727____Count_PropertyInfo,
	&ICollection_1_t7727____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43386_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43386_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43386_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43387_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43387_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43387_GenericMethod/* genericMethod */

};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
static ParameterInfo ICollection_1_t7727_ICollection_1_Add_m43388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviour_t43_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43388_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43388_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7727_ICollection_1_Add_m43388_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43388_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43389_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43389_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43389_GenericMethod/* genericMethod */

};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
static ParameterInfo ICollection_1_t7727_ICollection_1_Contains_m43390_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviour_t43_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43390_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43390_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7727_ICollection_1_Contains_m43390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43390_GenericMethod/* genericMethod */

};
extern Il2CppType PropAbstractBehaviourU5BU5D_t901_0_0_0;
extern Il2CppType PropAbstractBehaviourU5BU5D_t901_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7727_ICollection_1_CopyTo_m43391_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviourU5BU5D_t901_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43391_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43391_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7727_ICollection_1_CopyTo_m43391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43391_GenericMethod/* genericMethod */

};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
static ParameterInfo ICollection_1_t7727_ICollection_1_Remove_m43392_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviour_t43_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43392_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.PropAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43392_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7727_ICollection_1_Remove_m43392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43392_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7727_MethodInfos[] =
{
	&ICollection_1_get_Count_m43386_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43387_MethodInfo,
	&ICollection_1_Add_m43388_MethodInfo,
	&ICollection_1_Clear_m43389_MethodInfo,
	&ICollection_1_Contains_m43390_MethodInfo,
	&ICollection_1_CopyTo_m43391_MethodInfo,
	&ICollection_1_Remove_m43392_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7729_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7727_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7729_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7727_0_0_0;
extern Il2CppType ICollection_1_t7727_1_0_0;
struct ICollection_1_t7727;
extern Il2CppGenericClass ICollection_1_t7727_GenericClass;
TypeInfo ICollection_1_t7727_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7727_MethodInfos/* methods */
	, ICollection_1_t7727_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7727_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7727_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7727_0_0_0/* byval_arg */
	, &ICollection_1_t7727_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7727_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.PropAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.PropAbstractBehaviour>
extern Il2CppType IEnumerator_1_t4306_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43393_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.PropAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43393_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7729_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4306_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43393_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7729_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43393_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7729_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7729_0_0_0;
extern Il2CppType IEnumerable_1_t7729_1_0_0;
struct IEnumerable_1_t7729;
extern Il2CppGenericClass IEnumerable_1_t7729_GenericClass;
TypeInfo IEnumerable_1_t7729_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7729_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7729_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7729_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7729_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7729_0_0_0/* byval_arg */
	, &IEnumerable_1_t7729_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7729_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4306_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.PropAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.PropAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43394_MethodInfo;
static PropertyInfo IEnumerator_1_t4306____Current_PropertyInfo = 
{
	&IEnumerator_1_t4306_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43394_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4306_PropertyInfos[] =
{
	&IEnumerator_1_t4306____Current_PropertyInfo,
	NULL
};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43394_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.PropAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43394_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4306_il2cpp_TypeInfo/* declaring_type */
	, &PropAbstractBehaviour_t43_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43394_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4306_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43394_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4306_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4306_0_0_0;
extern Il2CppType IEnumerator_1_t4306_1_0_0;
struct IEnumerator_1_t4306;
extern Il2CppGenericClass IEnumerator_1_t4306_GenericClass;
TypeInfo IEnumerator_1_t4306_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4306_MethodInfos/* methods */
	, IEnumerator_1_t4306_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4306_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4306_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4306_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4306_0_0_0/* byval_arg */
	, &IEnumerator_1_t4306_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4306_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_72.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2904_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_72MethodDeclarations.h"

extern TypeInfo PropAbstractBehaviour_t43_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14988_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPropAbstractBehaviour_t43_m33085_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.PropAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.PropAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisPropAbstractBehaviour_t43_m33085(__this, p0, method) (PropAbstractBehaviour_t43 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2904____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2904, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2904____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2904, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2904_FieldInfos[] =
{
	&InternalEnumerator_1_t2904____array_0_FieldInfo,
	&InternalEnumerator_1_t2904____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2904____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2904_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2904____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2904_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2904_PropertyInfos[] =
{
	&InternalEnumerator_1_t2904____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2904____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2904_InternalEnumerator_1__ctor_m14984_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14984_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14984_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2904_InternalEnumerator_1__ctor_m14984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14984_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14986_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14986_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14986_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14987_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14987_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14987_GenericMethod/* genericMethod */

};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14988_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.PropAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14988_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* declaring_type */
	, &PropAbstractBehaviour_t43_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14988_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2904_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14984_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_MethodInfo,
	&InternalEnumerator_1_Dispose_m14986_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14987_MethodInfo,
	&InternalEnumerator_1_get_Current_m14988_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14987_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14986_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2904_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14985_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14987_MethodInfo,
	&InternalEnumerator_1_Dispose_m14986_MethodInfo,
	&InternalEnumerator_1_get_Current_m14988_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2904_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t4306_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2904_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4306_il2cpp_TypeInfo, 7},
};
extern TypeInfo PropAbstractBehaviour_t43_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2904_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14988_MethodInfo/* Method Usage */,
	&PropAbstractBehaviour_t43_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPropAbstractBehaviour_t43_m33085_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2904_0_0_0;
extern Il2CppType InternalEnumerator_1_t2904_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2904_GenericClass;
TypeInfo InternalEnumerator_1_t2904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2904_MethodInfos/* methods */
	, InternalEnumerator_1_t2904_PropertyInfos/* properties */
	, InternalEnumerator_1_t2904_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2904_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2904_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2904_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2904_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2904_1_0_0/* this_arg */
	, InternalEnumerator_1_t2904_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2904_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2904_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2904)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7728_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43395_MethodInfo;
extern MethodInfo IList_1_set_Item_m43396_MethodInfo;
static PropertyInfo IList_1_t7728____Item_PropertyInfo = 
{
	&IList_1_t7728_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43395_MethodInfo/* get */
	, &IList_1_set_Item_m43396_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7728_PropertyInfos[] =
{
	&IList_1_t7728____Item_PropertyInfo,
	NULL
};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
static ParameterInfo IList_1_t7728_IList_1_IndexOf_m43397_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviour_t43_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43397_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43397_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7728_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7728_IList_1_IndexOf_m43397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43397_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
static ParameterInfo IList_1_t7728_IList_1_Insert_m43398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviour_t43_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43398_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43398_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7728_IList_1_Insert_m43398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7728_IList_1_RemoveAt_m43399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43399_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43399_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7728_IList_1_RemoveAt_m43399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43399_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7728_IList_1_get_Item_m43395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43395_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43395_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7728_il2cpp_TypeInfo/* declaring_type */
	, &PropAbstractBehaviour_t43_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7728_IList_1_get_Item_m43395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43395_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PropAbstractBehaviour_t43_0_0_0;
static ParameterInfo IList_1_t7728_IList_1_set_Item_m43396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PropAbstractBehaviour_t43_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43396_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.PropAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43396_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7728_IList_1_set_Item_m43396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43396_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7728_MethodInfos[] =
{
	&IList_1_IndexOf_m43397_MethodInfo,
	&IList_1_Insert_m43398_MethodInfo,
	&IList_1_RemoveAt_m43399_MethodInfo,
	&IList_1_get_Item_m43395_MethodInfo,
	&IList_1_set_Item_m43396_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7728_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7727_il2cpp_TypeInfo,
	&IEnumerable_1_t7729_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7728_0_0_0;
extern Il2CppType IList_1_t7728_1_0_0;
struct IList_1_t7728;
extern Il2CppGenericClass IList_1_t7728_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7728_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7728_MethodInfos/* methods */
	, IList_1_t7728_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7728_il2cpp_TypeInfo/* element_class */
	, IList_1_t7728_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7728_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7728_0_0_0/* byval_arg */
	, &IList_1_t7728_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7728_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7730_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>
extern MethodInfo ICollection_1_get_Count_m43400_MethodInfo;
static PropertyInfo ICollection_1_t7730____Count_PropertyInfo = 
{
	&ICollection_1_t7730_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43401_MethodInfo;
static PropertyInfo ICollection_1_t7730____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7730_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43401_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7730_PropertyInfos[] =
{
	&ICollection_1_t7730____Count_PropertyInfo,
	&ICollection_1_t7730____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43400_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43400_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43400_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43401_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43401_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43401_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
static ParameterInfo ICollection_1_t7730_ICollection_1_Add_m43402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t159_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43402_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43402_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7730_ICollection_1_Add_m43402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43402_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43403_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43403_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43403_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
static ParameterInfo ICollection_1_t7730_ICollection_1_Contains_m43404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t159_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43404_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43404_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7730_ICollection_1_Contains_m43404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43404_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorPropBehaviourU5BU5D_t5639_0_0_0;
extern Il2CppType IEditorPropBehaviourU5BU5D_t5639_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7730_ICollection_1_CopyTo_m43405_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviourU5BU5D_t5639_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43405_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43405_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7730_ICollection_1_CopyTo_m43405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43405_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
static ParameterInfo ICollection_1_t7730_ICollection_1_Remove_m43406_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t159_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43406_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorPropBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43406_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7730_ICollection_1_Remove_m43406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43406_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7730_MethodInfos[] =
{
	&ICollection_1_get_Count_m43400_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43401_MethodInfo,
	&ICollection_1_Add_m43402_MethodInfo,
	&ICollection_1_Clear_m43403_MethodInfo,
	&ICollection_1_Contains_m43404_MethodInfo,
	&ICollection_1_CopyTo_m43405_MethodInfo,
	&ICollection_1_Remove_m43406_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7732_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7730_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7732_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7730_0_0_0;
extern Il2CppType ICollection_1_t7730_1_0_0;
struct ICollection_1_t7730;
extern Il2CppGenericClass ICollection_1_t7730_GenericClass;
TypeInfo ICollection_1_t7730_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7730_MethodInfos/* methods */
	, ICollection_1_t7730_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7730_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7730_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7730_0_0_0/* byval_arg */
	, &ICollection_1_t7730_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7730_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorPropBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorPropBehaviour>
extern Il2CppType IEnumerator_1_t6067_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43407_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorPropBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43407_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7732_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6067_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43407_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7732_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43407_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7732_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7732_0_0_0;
extern Il2CppType IEnumerable_1_t7732_1_0_0;
struct IEnumerable_1_t7732;
extern Il2CppGenericClass IEnumerable_1_t7732_GenericClass;
TypeInfo IEnumerable_1_t7732_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7732_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7732_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7732_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7732_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7732_0_0_0/* byval_arg */
	, &IEnumerable_1_t7732_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7732_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6067_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorPropBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorPropBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43408_MethodInfo;
static PropertyInfo IEnumerator_1_t6067____Current_PropertyInfo = 
{
	&IEnumerator_1_t6067_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43408_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6067_PropertyInfos[] =
{
	&IEnumerator_1_t6067____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43408_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorPropBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43408_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6067_il2cpp_TypeInfo/* declaring_type */
	, &IEditorPropBehaviour_t159_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43408_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6067_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43408_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6067_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6067_0_0_0;
extern Il2CppType IEnumerator_1_t6067_1_0_0;
struct IEnumerator_1_t6067;
extern Il2CppGenericClass IEnumerator_1_t6067_GenericClass;
TypeInfo IEnumerator_1_t6067_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6067_MethodInfos/* methods */
	, IEnumerator_1_t6067_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6067_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6067_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6067_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6067_0_0_0/* byval_arg */
	, &IEnumerator_1_t6067_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6067_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_73.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2905_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_73MethodDeclarations.h"

extern TypeInfo IEditorPropBehaviour_t159_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14993_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorPropBehaviour_t159_m33096_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorPropBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorPropBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorPropBehaviour_t159_m33096(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2905____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2905, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2905____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2905, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2905_FieldInfos[] =
{
	&InternalEnumerator_1_t2905____array_0_FieldInfo,
	&InternalEnumerator_1_t2905____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2905____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2905_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2905____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2905_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14993_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2905_PropertyInfos[] =
{
	&InternalEnumerator_1_t2905____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2905____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2905_InternalEnumerator_1__ctor_m14989_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14989_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14989_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2905_InternalEnumerator_1__ctor_m14989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14989_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14991_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14991_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14991_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14992_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14992_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14992_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14993_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorPropBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14993_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* declaring_type */
	, &IEditorPropBehaviour_t159_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14993_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2905_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14989_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_MethodInfo,
	&InternalEnumerator_1_Dispose_m14991_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14992_MethodInfo,
	&InternalEnumerator_1_get_Current_m14993_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14992_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14991_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2905_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14990_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14992_MethodInfo,
	&InternalEnumerator_1_Dispose_m14991_MethodInfo,
	&InternalEnumerator_1_get_Current_m14993_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2905_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6067_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2905_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6067_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorPropBehaviour_t159_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2905_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14993_MethodInfo/* Method Usage */,
	&IEditorPropBehaviour_t159_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorPropBehaviour_t159_m33096_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2905_0_0_0;
extern Il2CppType InternalEnumerator_1_t2905_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2905_GenericClass;
TypeInfo InternalEnumerator_1_t2905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2905_MethodInfos/* methods */
	, InternalEnumerator_1_t2905_PropertyInfos/* properties */
	, InternalEnumerator_1_t2905_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2905_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2905_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2905_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2905_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2905_1_0_0/* this_arg */
	, InternalEnumerator_1_t2905_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2905_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2905)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7731_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>
extern MethodInfo IList_1_get_Item_m43409_MethodInfo;
extern MethodInfo IList_1_set_Item_m43410_MethodInfo;
static PropertyInfo IList_1_t7731____Item_PropertyInfo = 
{
	&IList_1_t7731_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43409_MethodInfo/* get */
	, &IList_1_set_Item_m43410_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7731_PropertyInfos[] =
{
	&IList_1_t7731____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
static ParameterInfo IList_1_t7731_IList_1_IndexOf_m43411_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t159_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43411_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43411_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7731_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7731_IList_1_IndexOf_m43411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43411_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
static ParameterInfo IList_1_t7731_IList_1_Insert_m43412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t159_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43412_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43412_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7731_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7731_IList_1_Insert_m43412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7731_IList_1_RemoveAt_m43413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43413_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43413_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7731_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7731_IList_1_RemoveAt_m43413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43413_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7731_IList_1_get_Item_m43409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43409_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43409_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7731_il2cpp_TypeInfo/* declaring_type */
	, &IEditorPropBehaviour_t159_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7731_IList_1_get_Item_m43409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43409_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorPropBehaviour_t159_0_0_0;
static ParameterInfo IList_1_t7731_IList_1_set_Item_m43410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t159_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43410_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43410_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7731_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7731_IList_1_set_Item_m43410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43410_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7731_MethodInfos[] =
{
	&IList_1_IndexOf_m43411_MethodInfo,
	&IList_1_Insert_m43412_MethodInfo,
	&IList_1_RemoveAt_m43413_MethodInfo,
	&IList_1_get_Item_m43409_MethodInfo,
	&IList_1_set_Item_m43410_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7731_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7730_il2cpp_TypeInfo,
	&IEnumerable_1_t7732_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7731_0_0_0;
extern Il2CppType IList_1_t7731_1_0_0;
struct IList_1_t7731;
extern Il2CppGenericClass IList_1_t7731_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7731_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7731_MethodInfos/* methods */
	, IList_1_t7731_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7731_il2cpp_TypeInfo/* element_class */
	, IList_1_t7731_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7731_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7731_0_0_0/* byval_arg */
	, &IList_1_t7731_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7731_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7733_il2cpp_TypeInfo;

// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m43414_MethodInfo;
static PropertyInfo ICollection_1_t7733____Count_PropertyInfo = 
{
	&ICollection_1_t7733_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43415_MethodInfo;
static PropertyInfo ICollection_1_t7733____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7733_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7733_PropertyInfos[] =
{
	&ICollection_1_t7733____Count_PropertyInfo,
	&ICollection_1_t7733____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43414_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43414_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43414_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43415_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43415_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43415_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
static ParameterInfo ICollection_1_t7733_ICollection_1_Add_m43416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t627_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43416_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43416_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7733_ICollection_1_Add_m43416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43416_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43417_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43417_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43417_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
static ParameterInfo ICollection_1_t7733_ICollection_1_Contains_m43418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t627_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43418_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43418_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7733_ICollection_1_Contains_m43418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43418_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviourU5BU5D_t864_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviourU5BU5D_t864_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7733_ICollection_1_CopyTo_m43419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviourU5BU5D_t864_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43419_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43419_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7733_ICollection_1_CopyTo_m43419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43419_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
static ParameterInfo ICollection_1_t7733_ICollection_1_Remove_m43420_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t627_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43420_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43420_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7733_ICollection_1_Remove_m43420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43420_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7733_MethodInfos[] =
{
	&ICollection_1_get_Count_m43414_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43415_MethodInfo,
	&ICollection_1_Add_m43416_MethodInfo,
	&ICollection_1_Clear_m43417_MethodInfo,
	&ICollection_1_Contains_m43418_MethodInfo,
	&ICollection_1_CopyTo_m43419_MethodInfo,
	&ICollection_1_Remove_m43420_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7735_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7733_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7735_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7733_0_0_0;
extern Il2CppType ICollection_1_t7733_1_0_0;
struct ICollection_1_t7733;
extern Il2CppGenericClass ICollection_1_t7733_GenericClass;
TypeInfo ICollection_1_t7733_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7733_MethodInfos/* methods */
	, ICollection_1_t7733_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7733_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7733_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7733_0_0_0/* byval_arg */
	, &ICollection_1_t7733_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7733_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackableBehaviour>
extern Il2CppType IEnumerator_1_t6069_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43421_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43421_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7735_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6069_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43421_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7735_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43421_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7735_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7735_0_0_0;
extern Il2CppType IEnumerable_1_t7735_1_0_0;
struct IEnumerable_1_t7735;
extern Il2CppGenericClass IEnumerable_1_t7735_GenericClass;
TypeInfo IEnumerable_1_t7735_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7735_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7735_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7735_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7735_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7735_0_0_0/* byval_arg */
	, &IEnumerable_1_t7735_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7735_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6069_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43422_MethodInfo;
static PropertyInfo IEnumerator_1_t6069____Current_PropertyInfo = 
{
	&IEnumerator_1_t6069_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6069_PropertyInfos[] =
{
	&IEnumerator_1_t6069____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43422_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43422_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6069_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackableBehaviour_t627_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43422_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6069_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43422_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6069_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6069_0_0_0;
extern Il2CppType IEnumerator_1_t6069_1_0_0;
struct IEnumerator_1_t6069;
extern Il2CppGenericClass IEnumerator_1_t6069_GenericClass;
TypeInfo IEnumerator_1_t6069_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6069_MethodInfos/* methods */
	, IEnumerator_1_t6069_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6069_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6069_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6069_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6069_0_0_0/* byval_arg */
	, &IEnumerator_1_t6069_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6069_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_74.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2906_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_74MethodDeclarations.h"

extern TypeInfo SmartTerrainTrackableBehaviour_t627_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14998_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackableBehaviour_t627_m33107_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSmartTerrainTrackableBehaviour_t627_m33107(__this, p0, method) (SmartTerrainTrackableBehaviour_t627 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2906____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2906, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2906____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2906, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2906_FieldInfos[] =
{
	&InternalEnumerator_1_t2906____array_0_FieldInfo,
	&InternalEnumerator_1_t2906____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2906____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2906_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2906____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2906_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14998_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2906_PropertyInfos[] =
{
	&InternalEnumerator_1_t2906____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2906____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2906_InternalEnumerator_1__ctor_m14994_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14994_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14994_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2906_InternalEnumerator_1__ctor_m14994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14994_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14996_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14996_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14996_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14997_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14997_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14997_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14998_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14998_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackableBehaviour_t627_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14998_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2906_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14994_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_MethodInfo,
	&InternalEnumerator_1_Dispose_m14996_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14997_MethodInfo,
	&InternalEnumerator_1_get_Current_m14998_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14997_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14996_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2906_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14995_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14997_MethodInfo,
	&InternalEnumerator_1_Dispose_m14996_MethodInfo,
	&InternalEnumerator_1_get_Current_m14998_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2906_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6069_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2906_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6069_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmartTerrainTrackableBehaviour_t627_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2906_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14998_MethodInfo/* Method Usage */,
	&SmartTerrainTrackableBehaviour_t627_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackableBehaviour_t627_m33107_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2906_0_0_0;
extern Il2CppType InternalEnumerator_1_t2906_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2906_GenericClass;
TypeInfo InternalEnumerator_1_t2906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2906_MethodInfos/* methods */
	, InternalEnumerator_1_t2906_PropertyInfos/* properties */
	, InternalEnumerator_1_t2906_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2906_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2906_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2906_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2906_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2906_1_0_0/* this_arg */
	, InternalEnumerator_1_t2906_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2906_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2906_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2906)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7734_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m43423_MethodInfo;
extern MethodInfo IList_1_set_Item_m43424_MethodInfo;
static PropertyInfo IList_1_t7734____Item_PropertyInfo = 
{
	&IList_1_t7734_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43423_MethodInfo/* get */
	, &IList_1_set_Item_m43424_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7734_PropertyInfos[] =
{
	&IList_1_t7734____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
static ParameterInfo IList_1_t7734_IList_1_IndexOf_m43425_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t627_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43425_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43425_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7734_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7734_IList_1_IndexOf_m43425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43425_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
static ParameterInfo IList_1_t7734_IList_1_Insert_m43426_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t627_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43426_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43426_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7734_IList_1_Insert_m43426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43426_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7734_IList_1_RemoveAt_m43427_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43427_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43427_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7734_IList_1_RemoveAt_m43427_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43427_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7734_IList_1_get_Item_m43423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43423_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43423_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7734_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackableBehaviour_t627_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7734_IList_1_get_Item_m43423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43423_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviour_t627_0_0_0;
static ParameterInfo IList_1_t7734_IList_1_set_Item_m43424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t627_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43424_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43424_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7734_IList_1_set_Item_m43424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43424_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7734_MethodInfos[] =
{
	&IList_1_IndexOf_m43425_MethodInfo,
	&IList_1_Insert_m43426_MethodInfo,
	&IList_1_RemoveAt_m43427_MethodInfo,
	&IList_1_get_Item_m43423_MethodInfo,
	&IList_1_set_Item_m43424_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7734_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7733_il2cpp_TypeInfo,
	&IEnumerable_1_t7735_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7734_0_0_0;
extern Il2CppType IList_1_t7734_1_0_0;
struct IList_1_t7734;
extern Il2CppGenericClass IList_1_t7734_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7734_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7734_MethodInfos/* methods */
	, IList_1_t7734_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7734_il2cpp_TypeInfo/* element_class */
	, IList_1_t7734_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7734_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7734_0_0_0/* byval_arg */
	, &IList_1_t7734_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7734_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_20.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2907_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_20MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_16.h"
extern TypeInfo InvokableCall_1_t2908_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_16MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15001_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15003_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2907____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2907_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2907, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2907_FieldInfos[] =
{
	&CachedInvokableCall_1_t2907____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2907_CachedInvokableCall_1__ctor_m14999_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14999_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14999_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2907_CachedInvokableCall_1__ctor_m14999_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14999_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2907_CachedInvokableCall_1_Invoke_m15000_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15000_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15000_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2907_CachedInvokableCall_1_Invoke_m15000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15000_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2907_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14999_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15000_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15000_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15004_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2907_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15000_MethodInfo,
	&InvokableCall_1_Find_m15004_MethodInfo,
};
extern Il2CppType UnityAction_1_t2909_0_0_0;
extern TypeInfo UnityAction_1_t2909_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t12_m33117_MethodInfo;
extern TypeInfo PropBehaviour_t12_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15006_MethodInfo;
extern TypeInfo PropBehaviour_t12_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2907_RGCTXData[8] = 
{
	&UnityAction_1_t2909_0_0_0/* Type Usage */,
	&UnityAction_1_t2909_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t12_m33117_MethodInfo/* Method Usage */,
	&PropBehaviour_t12_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15006_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15001_MethodInfo/* Method Usage */,
	&PropBehaviour_t12_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15003_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2907_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2907_1_0_0;
struct CachedInvokableCall_1_t2907;
extern Il2CppGenericClass CachedInvokableCall_1_t2907_GenericClass;
TypeInfo CachedInvokableCall_1_t2907_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2907_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2907_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2907_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2907_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2907_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2907_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2907_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2907)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_23.h"
extern TypeInfo UnityAction_1_t2909_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_23MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.PropBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.PropBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t12_m33117(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
extern Il2CppType UnityAction_1_t2909_0_0_1;
FieldInfo InvokableCall_1_t2908____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2909_0_0_1/* type */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2908, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2908_FieldInfos[] =
{
	&InvokableCall_1_t2908____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2908_InvokableCall_1__ctor_m15001_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15001_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15001_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2908_InvokableCall_1__ctor_m15001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15001_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2909_0_0_0;
static ParameterInfo InvokableCall_1_t2908_InvokableCall_1__ctor_m15002_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2909_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15002_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15002_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2908_InvokableCall_1__ctor_m15002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15002_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2908_InvokableCall_1_Invoke_m15003_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15003_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15003_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2908_InvokableCall_1_Invoke_m15003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15003_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2908_InvokableCall_1_Find_m15004_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15004_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15004_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2908_InvokableCall_1_Find_m15004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15004_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2908_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15001_MethodInfo,
	&InvokableCall_1__ctor_m15002_MethodInfo,
	&InvokableCall_1_Invoke_m15003_MethodInfo,
	&InvokableCall_1_Find_m15004_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2908_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15003_MethodInfo,
	&InvokableCall_1_Find_m15004_MethodInfo,
};
extern TypeInfo UnityAction_1_t2909_il2cpp_TypeInfo;
extern TypeInfo PropBehaviour_t12_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2908_RGCTXData[5] = 
{
	&UnityAction_1_t2909_0_0_0/* Type Usage */,
	&UnityAction_1_t2909_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t12_m33117_MethodInfo/* Method Usage */,
	&PropBehaviour_t12_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15006_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2908_0_0_0;
extern Il2CppType InvokableCall_1_t2908_1_0_0;
struct InvokableCall_1_t2908;
extern Il2CppGenericClass InvokableCall_1_t2908_GenericClass;
TypeInfo InvokableCall_1_t2908_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2908_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2908_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2908_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2908_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2908_0_0_0/* byval_arg */
	, &InvokableCall_1_t2908_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2908_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2908)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2909_UnityAction_1__ctor_m15005_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15005_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15005_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2909_UnityAction_1__ctor_m15005_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15005_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t12_0_0_0;
static ParameterInfo UnityAction_1_t2909_UnityAction_1_Invoke_m15006_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15006_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15006_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2909_UnityAction_1_Invoke_m15006_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15006_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t12_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2909_UnityAction_1_BeginInvoke_m15007_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t12_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15007_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15007_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2909_UnityAction_1_BeginInvoke_m15007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15007_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2909_UnityAction_1_EndInvoke_m15008_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15008_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15008_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2909_UnityAction_1_EndInvoke_m15008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15008_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2909_MethodInfos[] =
{
	&UnityAction_1__ctor_m15005_MethodInfo,
	&UnityAction_1_Invoke_m15006_MethodInfo,
	&UnityAction_1_BeginInvoke_m15007_MethodInfo,
	&UnityAction_1_EndInvoke_m15008_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15007_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15008_MethodInfo;
static MethodInfo* UnityAction_1_t2909_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15006_MethodInfo,
	&UnityAction_1_BeginInvoke_m15007_MethodInfo,
	&UnityAction_1_EndInvoke_m15008_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2909_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2909_1_0_0;
struct UnityAction_1_t2909;
extern Il2CppGenericClass UnityAction_1_t2909_GenericClass;
TypeInfo UnityAction_1_t2909_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2909_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2909_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2909_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2909_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2909_0_0_0/* byval_arg */
	, &UnityAction_1_t2909_1_0_0/* this_arg */
	, UnityAction_1_t2909_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2909)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6071_il2cpp_TypeInfo;

// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.QCARBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43428_MethodInfo;
static PropertyInfo IEnumerator_1_t6071____Current_PropertyInfo = 
{
	&IEnumerator_1_t6071_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43428_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6071_PropertyInfos[] =
{
	&IEnumerator_1_t6071____Current_PropertyInfo,
	NULL
};
extern Il2CppType QCARBehaviour_t44_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43428_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43428_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6071_il2cpp_TypeInfo/* declaring_type */
	, &QCARBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43428_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6071_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43428_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6071_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6071_0_0_0;
extern Il2CppType IEnumerator_1_t6071_1_0_0;
struct IEnumerator_1_t6071;
extern Il2CppGenericClass IEnumerator_1_t6071_GenericClass;
TypeInfo IEnumerator_1_t6071_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6071_MethodInfos/* methods */
	, IEnumerator_1_t6071_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6071_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6071_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6071_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6071_0_0_0/* byval_arg */
	, &IEnumerator_1_t6071_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6071_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_75.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2910_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_75MethodDeclarations.h"

extern TypeInfo QCARBehaviour_t44_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15013_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisQCARBehaviour_t44_m33119_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.QCARBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.QCARBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisQCARBehaviour_t44_m33119(__this, p0, method) (QCARBehaviour_t44 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2910____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2910, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2910____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2910, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2910_FieldInfos[] =
{
	&InternalEnumerator_1_t2910____array_0_FieldInfo,
	&InternalEnumerator_1_t2910____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2910____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2910_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2910____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2910_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15013_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2910_PropertyInfos[] =
{
	&InternalEnumerator_1_t2910____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2910____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2910_InternalEnumerator_1__ctor_m15009_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15009_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15009_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2910_InternalEnumerator_1__ctor_m15009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15009_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15011_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15011_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15011_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15012_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15012_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15012_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t44_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15013_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15013_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &QCARBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15013_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2910_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15009_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_MethodInfo,
	&InternalEnumerator_1_Dispose_m15011_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15012_MethodInfo,
	&InternalEnumerator_1_get_Current_m15013_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15012_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15011_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2910_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15010_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15012_MethodInfo,
	&InternalEnumerator_1_Dispose_m15011_MethodInfo,
	&InternalEnumerator_1_get_Current_m15013_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2910_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6071_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2910_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6071_il2cpp_TypeInfo, 7},
};
extern TypeInfo QCARBehaviour_t44_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2910_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15013_MethodInfo/* Method Usage */,
	&QCARBehaviour_t44_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisQCARBehaviour_t44_m33119_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2910_0_0_0;
extern Il2CppType InternalEnumerator_1_t2910_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2910_GenericClass;
TypeInfo InternalEnumerator_1_t2910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2910_MethodInfos/* methods */
	, InternalEnumerator_1_t2910_PropertyInfos/* properties */
	, InternalEnumerator_1_t2910_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2910_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2910_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2910_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2910_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2910_1_0_0/* this_arg */
	, InternalEnumerator_1_t2910_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2910_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2910)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7736_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>
extern MethodInfo ICollection_1_get_Count_m43429_MethodInfo;
static PropertyInfo ICollection_1_t7736____Count_PropertyInfo = 
{
	&ICollection_1_t7736_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43429_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43430_MethodInfo;
static PropertyInfo ICollection_1_t7736____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7736_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7736_PropertyInfos[] =
{
	&ICollection_1_t7736____Count_PropertyInfo,
	&ICollection_1_t7736____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43429_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43429_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43429_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43430_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43430_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43430_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t44_0_0_0;
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo ICollection_1_t7736_ICollection_1_Add_m43431_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43431_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43431_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7736_ICollection_1_Add_m43431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43431_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43432_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43432_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43432_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo ICollection_1_t7736_ICollection_1_Contains_m43433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43433_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43433_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7736_ICollection_1_Contains_m43433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43433_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviourU5BU5D_t5375_0_0_0;
extern Il2CppType QCARBehaviourU5BU5D_t5375_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7736_ICollection_1_CopyTo_m43434_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviourU5BU5D_t5375_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43434_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43434_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7736_ICollection_1_CopyTo_m43434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43434_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo ICollection_1_t7736_ICollection_1_Remove_m43435_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43435_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43435_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7736_ICollection_1_Remove_m43435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43435_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7736_MethodInfos[] =
{
	&ICollection_1_get_Count_m43429_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43430_MethodInfo,
	&ICollection_1_Add_m43431_MethodInfo,
	&ICollection_1_Clear_m43432_MethodInfo,
	&ICollection_1_Contains_m43433_MethodInfo,
	&ICollection_1_CopyTo_m43434_MethodInfo,
	&ICollection_1_Remove_m43435_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7738_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7736_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7738_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7736_0_0_0;
extern Il2CppType ICollection_1_t7736_1_0_0;
struct ICollection_1_t7736;
extern Il2CppGenericClass ICollection_1_t7736_GenericClass;
TypeInfo ICollection_1_t7736_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7736_MethodInfos/* methods */
	, ICollection_1_t7736_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7736_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7736_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7736_0_0_0/* byval_arg */
	, &ICollection_1_t7736_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7736_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.QCARBehaviour>
extern Il2CppType IEnumerator_1_t6071_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43436_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43436_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7738_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6071_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43436_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7738_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43436_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7738_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7738_0_0_0;
extern Il2CppType IEnumerable_1_t7738_1_0_0;
struct IEnumerable_1_t7738;
extern Il2CppGenericClass IEnumerable_1_t7738_GenericClass;
TypeInfo IEnumerable_1_t7738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7738_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7738_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7738_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7738_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7738_0_0_0/* byval_arg */
	, &IEnumerable_1_t7738_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7738_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7737_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>
extern MethodInfo IList_1_get_Item_m43437_MethodInfo;
extern MethodInfo IList_1_set_Item_m43438_MethodInfo;
static PropertyInfo IList_1_t7737____Item_PropertyInfo = 
{
	&IList_1_t7737_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43437_MethodInfo/* get */
	, &IList_1_set_Item_m43438_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7737_PropertyInfos[] =
{
	&IList_1_t7737____Item_PropertyInfo,
	NULL
};
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo IList_1_t7737_IList_1_IndexOf_m43439_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43439_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43439_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7737_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7737_IList_1_IndexOf_m43439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43439_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo IList_1_t7737_IList_1_Insert_m43440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43440_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43440_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7737_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7737_IList_1_Insert_m43440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43440_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7737_IList_1_RemoveAt_m43441_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43441_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43441_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7737_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7737_IList_1_RemoveAt_m43441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43441_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7737_IList_1_get_Item_m43437_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType QCARBehaviour_t44_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43437_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43437_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7737_il2cpp_TypeInfo/* declaring_type */
	, &QCARBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7737_IList_1_get_Item_m43437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43437_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo IList_1_t7737_IList_1_set_Item_m43438_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43438_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43438_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7737_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7737_IList_1_set_Item_m43438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43438_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7737_MethodInfos[] =
{
	&IList_1_IndexOf_m43439_MethodInfo,
	&IList_1_Insert_m43440_MethodInfo,
	&IList_1_RemoveAt_m43441_MethodInfo,
	&IList_1_get_Item_m43437_MethodInfo,
	&IList_1_set_Item_m43438_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7737_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7736_il2cpp_TypeInfo,
	&IEnumerable_1_t7738_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7737_0_0_0;
extern Il2CppType IList_1_t7737_1_0_0;
struct IList_1_t7737;
extern Il2CppGenericClass IList_1_t7737_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7737_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7737_MethodInfos/* methods */
	, IList_1_t7737_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7737_il2cpp_TypeInfo/* element_class */
	, IList_1_t7737_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7737_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7737_0_0_0/* byval_arg */
	, &IList_1_t7737_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7737_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_21.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2911_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_21MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_17.h"
extern TypeInfo InvokableCall_1_t2912_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_17MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15016_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15018_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2911____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2911_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2911, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2911_FieldInfos[] =
{
	&CachedInvokableCall_1_t2911____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2911_CachedInvokableCall_1__ctor_m15014_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15014_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15014_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2911_CachedInvokableCall_1__ctor_m15014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15014_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2911_CachedInvokableCall_1_Invoke_m15015_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15015_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15015_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2911_CachedInvokableCall_1_Invoke_m15015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15015_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2911_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15014_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15015_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15015_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15019_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2911_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15015_MethodInfo,
	&InvokableCall_1_Find_m15019_MethodInfo,
};
extern Il2CppType UnityAction_1_t2913_0_0_0;
extern TypeInfo UnityAction_1_t2913_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t44_m33129_MethodInfo;
extern TypeInfo QCARBehaviour_t44_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15021_MethodInfo;
extern TypeInfo QCARBehaviour_t44_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2911_RGCTXData[8] = 
{
	&UnityAction_1_t2913_0_0_0/* Type Usage */,
	&UnityAction_1_t2913_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t44_m33129_MethodInfo/* Method Usage */,
	&QCARBehaviour_t44_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15021_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15016_MethodInfo/* Method Usage */,
	&QCARBehaviour_t44_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15018_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2911_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2911_1_0_0;
struct CachedInvokableCall_1_t2911;
extern Il2CppGenericClass CachedInvokableCall_1_t2911_GenericClass;
TypeInfo CachedInvokableCall_1_t2911_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2911_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2911_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2911_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2911_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2911_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2911_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2911_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2911_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2911)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_24.h"
extern TypeInfo UnityAction_1_t2913_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_24MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.QCARBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.QCARBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t44_m33129(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
extern Il2CppType UnityAction_1_t2913_0_0_1;
FieldInfo InvokableCall_1_t2912____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2913_0_0_1/* type */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2912, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2912_FieldInfos[] =
{
	&InvokableCall_1_t2912____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2912_InvokableCall_1__ctor_m15016_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15016_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15016_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2912_InvokableCall_1__ctor_m15016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15016_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2913_0_0_0;
static ParameterInfo InvokableCall_1_t2912_InvokableCall_1__ctor_m15017_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2913_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15017_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15017_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2912_InvokableCall_1__ctor_m15017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15017_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2912_InvokableCall_1_Invoke_m15018_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15018_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15018_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2912_InvokableCall_1_Invoke_m15018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15018_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2912_InvokableCall_1_Find_m15019_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15019_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15019_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2912_InvokableCall_1_Find_m15019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15019_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2912_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15016_MethodInfo,
	&InvokableCall_1__ctor_m15017_MethodInfo,
	&InvokableCall_1_Invoke_m15018_MethodInfo,
	&InvokableCall_1_Find_m15019_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2912_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15018_MethodInfo,
	&InvokableCall_1_Find_m15019_MethodInfo,
};
extern TypeInfo UnityAction_1_t2913_il2cpp_TypeInfo;
extern TypeInfo QCARBehaviour_t44_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2912_RGCTXData[5] = 
{
	&UnityAction_1_t2913_0_0_0/* Type Usage */,
	&UnityAction_1_t2913_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t44_m33129_MethodInfo/* Method Usage */,
	&QCARBehaviour_t44_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15021_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2912_0_0_0;
extern Il2CppType InvokableCall_1_t2912_1_0_0;
struct InvokableCall_1_t2912;
extern Il2CppGenericClass InvokableCall_1_t2912_GenericClass;
TypeInfo InvokableCall_1_t2912_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2912_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2912_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2912_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2912_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2912_0_0_0/* byval_arg */
	, &InvokableCall_1_t2912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2912_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2912)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2913_UnityAction_1__ctor_m15020_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15020_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15020_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2913_UnityAction_1__ctor_m15020_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15020_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t44_0_0_0;
static ParameterInfo UnityAction_1_t2913_UnityAction_1_Invoke_m15021_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15021_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15021_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2913_UnityAction_1_Invoke_m15021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15021_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t44_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2913_UnityAction_1_BeginInvoke_m15022_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t44_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15022_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15022_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2913_UnityAction_1_BeginInvoke_m15022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15022_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2913_UnityAction_1_EndInvoke_m15023_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15023_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15023_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2913_UnityAction_1_EndInvoke_m15023_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15023_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2913_MethodInfos[] =
{
	&UnityAction_1__ctor_m15020_MethodInfo,
	&UnityAction_1_Invoke_m15021_MethodInfo,
	&UnityAction_1_BeginInvoke_m15022_MethodInfo,
	&UnityAction_1_EndInvoke_m15023_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15022_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15023_MethodInfo;
static MethodInfo* UnityAction_1_t2913_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15021_MethodInfo,
	&UnityAction_1_BeginInvoke_m15022_MethodInfo,
	&UnityAction_1_EndInvoke_m15023_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2913_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2913_1_0_0;
struct UnityAction_1_t2913;
extern Il2CppGenericClass UnityAction_1_t2913_GenericClass;
TypeInfo UnityAction_1_t2913_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2913_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2913_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2913_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2913_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2913_0_0_0/* byval_arg */
	, &UnityAction_1_t2913_1_0_0/* this_arg */
	, UnityAction_1_t2913_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2913)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6073_il2cpp_TypeInfo;

// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43442_MethodInfo;
static PropertyInfo IEnumerator_1_t6073____Current_PropertyInfo = 
{
	&IEnumerator_1_t6073_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43442_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6073_PropertyInfos[] =
{
	&IEnumerator_1_t6073____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43442_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43442_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6073_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionBehaviour_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43442_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6073_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43442_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6073_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6073_0_0_0;
extern Il2CppType IEnumerator_1_t6073_1_0_0;
struct IEnumerator_1_t6073;
extern Il2CppGenericClass IEnumerator_1_t6073_GenericClass;
TypeInfo IEnumerator_1_t6073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6073_MethodInfos/* methods */
	, IEnumerator_1_t6073_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6073_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6073_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6073_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6073_0_0_0/* byval_arg */
	, &IEnumerator_1_t6073_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6073_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_76.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2914_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_76MethodDeclarations.h"

extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15028_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionBehaviour_t11_m33131_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionBehaviour_t11_m33131(__this, p0, method) (ReconstructionBehaviour_t11 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2914____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2914, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2914____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2914, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2914_FieldInfos[] =
{
	&InternalEnumerator_1_t2914____array_0_FieldInfo,
	&InternalEnumerator_1_t2914____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2914____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2914_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2914____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2914_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15028_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2914_PropertyInfos[] =
{
	&InternalEnumerator_1_t2914____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2914____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2914_InternalEnumerator_1__ctor_m15024_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15024_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15024_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2914_InternalEnumerator_1__ctor_m15024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15024_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15026_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15026_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15026_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15027_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15027_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15027_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15028_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15028_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionBehaviour_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15028_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2914_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15024_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_MethodInfo,
	&InternalEnumerator_1_Dispose_m15026_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15027_MethodInfo,
	&InternalEnumerator_1_get_Current_m15028_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15027_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15026_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2914_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15025_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15027_MethodInfo,
	&InternalEnumerator_1_Dispose_m15026_MethodInfo,
	&InternalEnumerator_1_get_Current_m15028_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2914_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6073_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2914_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6073_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2914_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15028_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t11_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionBehaviour_t11_m33131_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2914_0_0_0;
extern Il2CppType InternalEnumerator_1_t2914_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2914_GenericClass;
TypeInfo InternalEnumerator_1_t2914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2914_MethodInfos/* methods */
	, InternalEnumerator_1_t2914_PropertyInfos/* properties */
	, InternalEnumerator_1_t2914_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2914_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2914_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2914_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2914_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2914_1_0_0/* this_arg */
	, InternalEnumerator_1_t2914_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2914_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2914)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7739_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>
extern MethodInfo ICollection_1_get_Count_m43443_MethodInfo;
static PropertyInfo ICollection_1_t7739____Count_PropertyInfo = 
{
	&ICollection_1_t7739_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43443_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43444_MethodInfo;
static PropertyInfo ICollection_1_t7739____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7739_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43444_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7739_PropertyInfos[] =
{
	&ICollection_1_t7739____Count_PropertyInfo,
	&ICollection_1_t7739____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43443_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43443_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43443_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43444_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43444_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43444_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo ICollection_1_t7739_ICollection_1_Add_m43445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43445_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43445_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7739_ICollection_1_Add_m43445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43445_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43446_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43446_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43446_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo ICollection_1_t7739_ICollection_1_Contains_m43447_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43447_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43447_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7739_ICollection_1_Contains_m43447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43447_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviourU5BU5D_t5376_0_0_0;
extern Il2CppType ReconstructionBehaviourU5BU5D_t5376_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7739_ICollection_1_CopyTo_m43448_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviourU5BU5D_t5376_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43448_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43448_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7739_ICollection_1_CopyTo_m43448_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43448_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo ICollection_1_t7739_ICollection_1_Remove_m43449_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43449_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43449_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7739_ICollection_1_Remove_m43449_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43449_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7739_MethodInfos[] =
{
	&ICollection_1_get_Count_m43443_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43444_MethodInfo,
	&ICollection_1_Add_m43445_MethodInfo,
	&ICollection_1_Clear_m43446_MethodInfo,
	&ICollection_1_Contains_m43447_MethodInfo,
	&ICollection_1_CopyTo_m43448_MethodInfo,
	&ICollection_1_Remove_m43449_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7741_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7739_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7741_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7739_0_0_0;
extern Il2CppType ICollection_1_t7739_1_0_0;
struct ICollection_1_t7739;
extern Il2CppGenericClass ICollection_1_t7739_GenericClass;
TypeInfo ICollection_1_t7739_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7739_MethodInfos/* methods */
	, ICollection_1_t7739_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7739_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7739_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7739_0_0_0/* byval_arg */
	, &ICollection_1_t7739_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7739_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType IEnumerator_1_t6073_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43450_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43450_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7741_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6073_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43450_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7741_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43450_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7741_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7741_0_0_0;
extern Il2CppType IEnumerable_1_t7741_1_0_0;
struct IEnumerable_1_t7741;
extern Il2CppGenericClass IEnumerable_1_t7741_GenericClass;
TypeInfo IEnumerable_1_t7741_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7741_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7741_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7741_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7741_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7741_0_0_0/* byval_arg */
	, &IEnumerable_1_t7741_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7741_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7740_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>
extern MethodInfo IList_1_get_Item_m43451_MethodInfo;
extern MethodInfo IList_1_set_Item_m43452_MethodInfo;
static PropertyInfo IList_1_t7740____Item_PropertyInfo = 
{
	&IList_1_t7740_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43451_MethodInfo/* get */
	, &IList_1_set_Item_m43452_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7740_PropertyInfos[] =
{
	&IList_1_t7740____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo IList_1_t7740_IList_1_IndexOf_m43453_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43453_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43453_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7740_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7740_IList_1_IndexOf_m43453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43453_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo IList_1_t7740_IList_1_Insert_m43454_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43454_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43454_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7740_IList_1_Insert_m43454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43454_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7740_IList_1_RemoveAt_m43455_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43455_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43455_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7740_IList_1_RemoveAt_m43455_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43455_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7740_IList_1_get_Item_m43451_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43451_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43451_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7740_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionBehaviour_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7740_IList_1_get_Item_m43451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43451_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo IList_1_t7740_IList_1_set_Item_m43452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43452_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43452_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7740_IList_1_set_Item_m43452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43452_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7740_MethodInfos[] =
{
	&IList_1_IndexOf_m43453_MethodInfo,
	&IList_1_Insert_m43454_MethodInfo,
	&IList_1_RemoveAt_m43455_MethodInfo,
	&IList_1_get_Item_m43451_MethodInfo,
	&IList_1_set_Item_m43452_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7740_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7739_il2cpp_TypeInfo,
	&IEnumerable_1_t7741_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7740_0_0_0;
extern Il2CppType IList_1_t7740_1_0_0;
struct IList_1_t7740;
extern Il2CppGenericClass IList_1_t7740_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7740_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7740_MethodInfos/* methods */
	, IList_1_t7740_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7740_il2cpp_TypeInfo/* element_class */
	, IList_1_t7740_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7740_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7740_0_0_0/* byval_arg */
	, &IList_1_t7740_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7740_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4102_il2cpp_TypeInfo;

// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m36793_MethodInfo;
static PropertyInfo ICollection_1_t4102____Count_PropertyInfo = 
{
	&ICollection_1_t4102_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m36793_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43456_MethodInfo;
static PropertyInfo ICollection_1_t4102____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4102_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43456_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4102_PropertyInfos[] =
{
	&ICollection_1_t4102____Count_PropertyInfo,
	&ICollection_1_t4102____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m36793_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m36793_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m36793_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43456_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43456_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43456_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
static ParameterInfo ICollection_1_t4102_ICollection_1_Add_m43457_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t46_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43457_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43457_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t4102_ICollection_1_Add_m43457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43457_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43458_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43458_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43458_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
static ParameterInfo ICollection_1_t4102_ICollection_1_Contains_m36243_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t46_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m36243_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m36243_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4102_ICollection_1_Contains_m36243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m36243_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviourU5BU5D_t865_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviourU5BU5D_t865_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t4102_ICollection_1_CopyTo_m43459_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviourU5BU5D_t865_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43459_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43459_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t4102_ICollection_1_CopyTo_m43459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43459_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
static ParameterInfo ICollection_1_t4102_ICollection_1_Remove_m43460_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t46_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43460_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43460_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4102_ICollection_1_Remove_m43460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43460_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4102_MethodInfos[] =
{
	&ICollection_1_get_Count_m36793_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43456_MethodInfo,
	&ICollection_1_Add_m43457_MethodInfo,
	&ICollection_1_Clear_m43458_MethodInfo,
	&ICollection_1_Contains_m36243_MethodInfo,
	&ICollection_1_CopyTo_m43459_MethodInfo,
	&ICollection_1_Remove_m43460_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t619_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4102_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t619_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4102_0_0_0;
extern Il2CppType ICollection_1_t4102_1_0_0;
struct ICollection_1_t4102;
extern Il2CppGenericClass ICollection_1_t4102_GenericClass;
TypeInfo ICollection_1_t4102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4102_MethodInfos/* methods */
	, ICollection_1_t4102_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4102_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4102_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4102_0_0_0/* byval_arg */
	, &ICollection_1_t4102_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4102_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
extern Il2CppType IEnumerator_1_t828_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m4605_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m4605_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t619_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t828_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m4605_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t619_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m4605_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t619_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t619_0_0_0;
extern Il2CppType IEnumerable_1_t619_1_0_0;
struct IEnumerable_1_t619;
extern Il2CppGenericClass IEnumerable_1_t619_GenericClass;
TypeInfo IEnumerable_1_t619_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t619_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t619_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t619_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t619_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t619_0_0_0/* byval_arg */
	, &IEnumerable_1_t619_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t619_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t828_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m4606_MethodInfo;
static PropertyInfo IEnumerator_1_t828____Current_PropertyInfo = 
{
	&IEnumerator_1_t828_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m4606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t828_PropertyInfos[] =
{
	&IEnumerator_1_t828____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m4606_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m4606_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t828_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionAbstractBehaviour_t46_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m4606_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t828_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m4606_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t828_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t828_0_0_0;
extern Il2CppType IEnumerator_1_t828_1_0_0;
struct IEnumerator_1_t828;
extern Il2CppGenericClass IEnumerator_1_t828_GenericClass;
TypeInfo IEnumerator_1_t828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t828_MethodInfos/* methods */
	, IEnumerator_1_t828_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t828_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t828_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t828_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t828_0_0_0/* byval_arg */
	, &IEnumerator_1_t828_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_77.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2915_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_77MethodDeclarations.h"

extern TypeInfo ReconstructionAbstractBehaviour_t46_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15033_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionAbstractBehaviour_t46_m33142_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionAbstractBehaviour_t46_m33142(__this, p0, method) (ReconstructionAbstractBehaviour_t46 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2915____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2915, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2915____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2915, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2915_FieldInfos[] =
{
	&InternalEnumerator_1_t2915____array_0_FieldInfo,
	&InternalEnumerator_1_t2915____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2915____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2915____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15033_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2915_PropertyInfos[] =
{
	&InternalEnumerator_1_t2915____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2915____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2915_InternalEnumerator_1__ctor_m15029_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15029_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15029_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2915_InternalEnumerator_1__ctor_m15029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15029_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15031_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15031_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15031_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15032_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15032_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15032_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15033_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15033_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionAbstractBehaviour_t46_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15033_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2915_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15029_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_MethodInfo,
	&InternalEnumerator_1_Dispose_m15031_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15032_MethodInfo,
	&InternalEnumerator_1_get_Current_m15033_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15032_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15031_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2915_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15030_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15032_MethodInfo,
	&InternalEnumerator_1_Dispose_m15031_MethodInfo,
	&InternalEnumerator_1_get_Current_m15033_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2915_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t828_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2915_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t828_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionAbstractBehaviour_t46_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2915_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15033_MethodInfo/* Method Usage */,
	&ReconstructionAbstractBehaviour_t46_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionAbstractBehaviour_t46_m33142_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2915_0_0_0;
extern Il2CppType InternalEnumerator_1_t2915_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2915_GenericClass;
TypeInfo InternalEnumerator_1_t2915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2915_MethodInfos/* methods */
	, InternalEnumerator_1_t2915_PropertyInfos/* properties */
	, InternalEnumerator_1_t2915_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2915_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2915_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2915_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2915_1_0_0/* this_arg */
	, InternalEnumerator_1_t2915_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2915_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2915)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4106_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43461_MethodInfo;
extern MethodInfo IList_1_set_Item_m43462_MethodInfo;
static PropertyInfo IList_1_t4106____Item_PropertyInfo = 
{
	&IList_1_t4106_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43461_MethodInfo/* get */
	, &IList_1_set_Item_m43462_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4106_PropertyInfos[] =
{
	&IList_1_t4106____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
static ParameterInfo IList_1_t4106_IList_1_IndexOf_m43463_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t46_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43463_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43463_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4106_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4106_IList_1_IndexOf_m43463_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43463_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
static ParameterInfo IList_1_t4106_IList_1_Insert_m43464_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t46_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43464_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43464_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4106_IList_1_Insert_m43464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43464_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4106_IList_1_RemoveAt_m43465_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43465_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43465_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t4106_IList_1_RemoveAt_m43465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43465_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4106_IList_1_get_Item_m43461_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43461_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43461_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4106_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionAbstractBehaviour_t46_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t4106_IList_1_get_Item_m43461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43461_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviour_t46_0_0_0;
static ParameterInfo IList_1_t4106_IList_1_set_Item_m43462_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t46_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43462_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43462_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4106_IList_1_set_Item_m43462_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43462_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4106_MethodInfos[] =
{
	&IList_1_IndexOf_m43463_MethodInfo,
	&IList_1_Insert_m43464_MethodInfo,
	&IList_1_RemoveAt_m43465_MethodInfo,
	&IList_1_get_Item_m43461_MethodInfo,
	&IList_1_set_Item_m43462_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4106_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t4102_il2cpp_TypeInfo,
	&IEnumerable_1_t619_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4106_0_0_0;
extern Il2CppType IList_1_t4106_1_0_0;
struct IList_1_t4106;
extern Il2CppGenericClass IList_1_t4106_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t4106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4106_MethodInfos/* methods */
	, IList_1_t4106_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4106_il2cpp_TypeInfo/* element_class */
	, IList_1_t4106_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4106_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4106_0_0_0/* byval_arg */
	, &IList_1_t4106_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4106_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7742_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>
extern MethodInfo ICollection_1_get_Count_m43466_MethodInfo;
static PropertyInfo ICollection_1_t7742____Count_PropertyInfo = 
{
	&ICollection_1_t7742_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43466_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43467_MethodInfo;
static PropertyInfo ICollection_1_t7742____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7742_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43467_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7742_PropertyInfos[] =
{
	&ICollection_1_t7742____Count_PropertyInfo,
	&ICollection_1_t7742____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43466_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43466_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43466_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43467_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43467_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43467_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
static ParameterInfo ICollection_1_t7742_ICollection_1_Add_m43468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t163_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43468_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43468_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7742_ICollection_1_Add_m43468_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43468_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43469_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43469_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43469_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
static ParameterInfo ICollection_1_t7742_ICollection_1_Contains_m43470_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t163_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43470_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43470_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7742_ICollection_1_Contains_m43470_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43470_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviourU5BU5D_t5640_0_0_0;
extern Il2CppType IEditorReconstructionBehaviourU5BU5D_t5640_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7742_ICollection_1_CopyTo_m43471_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviourU5BU5D_t5640_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43471_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43471_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7742_ICollection_1_CopyTo_m43471_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43471_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
static ParameterInfo ICollection_1_t7742_ICollection_1_Remove_m43472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t163_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43472_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43472_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7742_ICollection_1_Remove_m43472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43472_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7742_MethodInfos[] =
{
	&ICollection_1_get_Count_m43466_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43467_MethodInfo,
	&ICollection_1_Add_m43468_MethodInfo,
	&ICollection_1_Clear_m43469_MethodInfo,
	&ICollection_1_Contains_m43470_MethodInfo,
	&ICollection_1_CopyTo_m43471_MethodInfo,
	&ICollection_1_Remove_m43472_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7744_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7742_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7744_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7742_0_0_0;
extern Il2CppType ICollection_1_t7742_1_0_0;
struct ICollection_1_t7742;
extern Il2CppGenericClass ICollection_1_t7742_GenericClass;
TypeInfo ICollection_1_t7742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7742_MethodInfos/* methods */
	, ICollection_1_t7742_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7742_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7742_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7742_0_0_0/* byval_arg */
	, &ICollection_1_t7742_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7742_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorReconstructionBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorReconstructionBehaviour>
extern Il2CppType IEnumerator_1_t6076_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43473_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorReconstructionBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43473_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7744_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6076_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43473_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7744_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43473_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7744_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7744_0_0_0;
extern Il2CppType IEnumerable_1_t7744_1_0_0;
struct IEnumerable_1_t7744;
extern Il2CppGenericClass IEnumerable_1_t7744_GenericClass;
TypeInfo IEnumerable_1_t7744_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7744_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7744_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7744_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7744_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7744_0_0_0/* byval_arg */
	, &IEnumerable_1_t7744_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7744_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6076_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43474_MethodInfo;
static PropertyInfo IEnumerator_1_t6076____Current_PropertyInfo = 
{
	&IEnumerator_1_t6076_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43474_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6076_PropertyInfos[] =
{
	&IEnumerator_1_t6076____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43474_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43474_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6076_il2cpp_TypeInfo/* declaring_type */
	, &IEditorReconstructionBehaviour_t163_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43474_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6076_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43474_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6076_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6076_0_0_0;
extern Il2CppType IEnumerator_1_t6076_1_0_0;
struct IEnumerator_1_t6076;
extern Il2CppGenericClass IEnumerator_1_t6076_GenericClass;
TypeInfo IEnumerator_1_t6076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6076_MethodInfos/* methods */
	, IEnumerator_1_t6076_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6076_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6076_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6076_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6076_0_0_0/* byval_arg */
	, &IEnumerator_1_t6076_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6076_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_78.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2916_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_78MethodDeclarations.h"

extern TypeInfo IEditorReconstructionBehaviour_t163_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15038_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorReconstructionBehaviour_t163_m33153_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorReconstructionBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorReconstructionBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorReconstructionBehaviour_t163_m33153(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2916____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2916, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2916____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2916, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2916_FieldInfos[] =
{
	&InternalEnumerator_1_t2916____array_0_FieldInfo,
	&InternalEnumerator_1_t2916____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2916____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2916____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15038_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2916_PropertyInfos[] =
{
	&InternalEnumerator_1_t2916____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2916____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2916_InternalEnumerator_1__ctor_m15034_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15034_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15034_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2916_InternalEnumerator_1__ctor_m15034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15034_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15036_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15036_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15036_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15037_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15037_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15037_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15038_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15038_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &IEditorReconstructionBehaviour_t163_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15038_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2916_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15034_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_MethodInfo,
	&InternalEnumerator_1_Dispose_m15036_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15037_MethodInfo,
	&InternalEnumerator_1_get_Current_m15038_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15037_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15036_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2916_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15035_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15037_MethodInfo,
	&InternalEnumerator_1_Dispose_m15036_MethodInfo,
	&InternalEnumerator_1_get_Current_m15038_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2916_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6076_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2916_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6076_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorReconstructionBehaviour_t163_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2916_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15038_MethodInfo/* Method Usage */,
	&IEditorReconstructionBehaviour_t163_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorReconstructionBehaviour_t163_m33153_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2916_0_0_0;
extern Il2CppType InternalEnumerator_1_t2916_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2916_GenericClass;
TypeInfo InternalEnumerator_1_t2916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2916_MethodInfos/* methods */
	, InternalEnumerator_1_t2916_PropertyInfos/* properties */
	, InternalEnumerator_1_t2916_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2916_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2916_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2916_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2916_1_0_0/* this_arg */
	, InternalEnumerator_1_t2916_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2916_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2916)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7743_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>
extern MethodInfo IList_1_get_Item_m43475_MethodInfo;
extern MethodInfo IList_1_set_Item_m43476_MethodInfo;
static PropertyInfo IList_1_t7743____Item_PropertyInfo = 
{
	&IList_1_t7743_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43475_MethodInfo/* get */
	, &IList_1_set_Item_m43476_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7743_PropertyInfos[] =
{
	&IList_1_t7743____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
static ParameterInfo IList_1_t7743_IList_1_IndexOf_m43477_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t163_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43477_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43477_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7743_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7743_IList_1_IndexOf_m43477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43477_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
static ParameterInfo IList_1_t7743_IList_1_Insert_m43478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t163_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43478_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43478_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7743_IList_1_Insert_m43478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43478_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7743_IList_1_RemoveAt_m43479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43479_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43479_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7743_IList_1_RemoveAt_m43479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43479_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7743_IList_1_get_Item_m43475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43475_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43475_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7743_il2cpp_TypeInfo/* declaring_type */
	, &IEditorReconstructionBehaviour_t163_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7743_IList_1_get_Item_m43475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43475_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorReconstructionBehaviour_t163_0_0_0;
static ParameterInfo IList_1_t7743_IList_1_set_Item_m43476_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t163_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43476_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43476_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7743_IList_1_set_Item_m43476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43476_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7743_MethodInfos[] =
{
	&IList_1_IndexOf_m43477_MethodInfo,
	&IList_1_Insert_m43478_MethodInfo,
	&IList_1_RemoveAt_m43479_MethodInfo,
	&IList_1_get_Item_m43475_MethodInfo,
	&IList_1_set_Item_m43476_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7743_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7742_il2cpp_TypeInfo,
	&IEnumerable_1_t7744_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7743_0_0_0;
extern Il2CppType IList_1_t7743_1_0_0;
struct IList_1_t7743;
extern Il2CppGenericClass IList_1_t7743_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7743_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7743_MethodInfos/* methods */
	, IList_1_t7743_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7743_il2cpp_TypeInfo/* element_class */
	, IList_1_t7743_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7743_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7743_0_0_0/* byval_arg */
	, &IList_1_t7743_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7743_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_22.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2917_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_22MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_18.h"
extern TypeInfo InvokableCall_1_t2918_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_18MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15041_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15043_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2917____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2917_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2917, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2917_FieldInfos[] =
{
	&CachedInvokableCall_1_t2917____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2917_CachedInvokableCall_1__ctor_m15039_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15039_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15039_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2917_CachedInvokableCall_1__ctor_m15039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15039_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2917_CachedInvokableCall_1_Invoke_m15040_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15040_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15040_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2917_CachedInvokableCall_1_Invoke_m15040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15040_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2917_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15039_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15040_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15040_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15044_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2917_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15040_MethodInfo,
	&InvokableCall_1_Find_m15044_MethodInfo,
};
extern Il2CppType UnityAction_1_t2919_0_0_0;
extern TypeInfo UnityAction_1_t2919_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t11_m33163_MethodInfo;
extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15046_MethodInfo;
extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2917_RGCTXData[8] = 
{
	&UnityAction_1_t2919_0_0_0/* Type Usage */,
	&UnityAction_1_t2919_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t11_m33163_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t11_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15046_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15041_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t11_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15043_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2917_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2917_1_0_0;
struct CachedInvokableCall_1_t2917;
extern Il2CppGenericClass CachedInvokableCall_1_t2917_GenericClass;
TypeInfo CachedInvokableCall_1_t2917_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2917_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2917_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2917_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2917_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2917_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2917_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2917_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2917_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2917)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
