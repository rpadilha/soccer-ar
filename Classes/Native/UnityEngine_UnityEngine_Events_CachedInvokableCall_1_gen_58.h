﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_54.h"
// UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>
struct CachedInvokableCall_1_t3114  : public InvokableCall_1_t3115
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
