﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>
struct InternalEnumerator_1_t5302;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31882 (InternalEnumerator_1_t5302 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31883 (InternalEnumerator_1_t5302 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>::Dispose()
 void InternalEnumerator_1_Dispose_m31884 (InternalEnumerator_1_t5302 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31885 (InternalEnumerator_1_t5302 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.StreamingContextStates>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31886 (InternalEnumerator_1_t5302 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
