﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t5353;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
 void DefaultComparer__ctor_m32210 (DefaultComparer_t5353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
 int32_t DefaultComparer_Compare_m32211 (DefaultComparer_t5353 * __this, TimeSpan_t852  ___x, TimeSpan_t852  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
