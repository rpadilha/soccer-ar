﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t2712;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
 void GenericEqualityComparer_1__ctor_m14124 (GenericEqualityComparer_1_t2712 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
 int32_t GenericEqualityComparer_1_GetHashCode_m32107 (GenericEqualityComparer_1_t2712 * __this, DateTime_t674  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
 bool GenericEqualityComparer_1_Equals_m32108 (GenericEqualityComparer_1_t2712 * __this, DateTime_t674  ___x, DateTime_t674  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
