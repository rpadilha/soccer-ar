﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t33;
// Vuforia.MultiTarget
struct MultiTarget_t617;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t597;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::get_MultiTarget()
 Object_t * MultiTargetAbstractBehaviour_get_MultiTarget_m4254 (MultiTargetAbstractBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::InternalUnregisterTrackable()
 void MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m371 (MultiTargetAbstractBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m373 (MultiTargetAbstractBehaviour_t33 * __this, Vector3_t73 * ___boundsMin, Vector3_t73 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m374 (MultiTargetAbstractBehaviour_t33 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorMultiTargetBehaviour.InitializeMultiTarget(Vuforia.MultiTarget)
 void MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m375 (MultiTargetAbstractBehaviour_t33 * __this, Object_t * ___multiTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::.ctor()
 void MultiTargetAbstractBehaviour__ctor_m366 (MultiTargetAbstractBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m367 (MultiTargetAbstractBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m368 (MultiTargetAbstractBehaviour_t33 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m369 (MultiTargetAbstractBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m370 (MultiTargetAbstractBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
