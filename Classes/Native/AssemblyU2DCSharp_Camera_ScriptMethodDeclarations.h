﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Camera_Script
struct Camera_Script_t75;

// System.Void Camera_Script::.ctor()
 void Camera_Script__ctor_m114 (Camera_Script_t75 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Camera_Script::Start()
 void Camera_Script_Start_m115 (Camera_Script_t75 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Camera_Script::LateUpdate()
 void Camera_Script_LateUpdate_m116 (Camera_Script_t75 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
