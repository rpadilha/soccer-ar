﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CapsuleCollider
struct CapsuleCollider_t81;

// System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
 void CapsuleCollider_set_direction_m612 (CapsuleCollider_t81 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
