﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t42;

// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
 void ObjectTargetBehaviour__ctor_m78 (ObjectTargetBehaviour_t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
