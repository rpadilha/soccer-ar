﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IO.MonoIOError>
struct InternalEnumerator_1_t5200;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"

// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31374 (InternalEnumerator_1_t5200 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoIOError>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375 (InternalEnumerator_1_t5200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::Dispose()
 void InternalEnumerator_1_Dispose_m31376 (InternalEnumerator_1_t5200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoIOError>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31377 (InternalEnumerator_1_t5200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.IO.MonoIOError>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31378 (InternalEnumerator_1_t5200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
