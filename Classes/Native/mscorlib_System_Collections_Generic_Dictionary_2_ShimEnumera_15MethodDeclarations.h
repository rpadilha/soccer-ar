﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>
struct ShimEnumerator_t4264;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t759;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m24990 (ShimEnumerator_t4264 * __this, Dictionary_2_t759 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>::MoveNext()
 bool ShimEnumerator_MoveNext_m24991 (ShimEnumerator_t4264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m24992 (ShimEnumerator_t4264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>::get_Key()
 Object_t * ShimEnumerator_get_Key_m24993 (ShimEnumerator_t4264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>::get_Value()
 Object_t * ShimEnumerator_get_Value_m24994 (ShimEnumerator_t4264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Surface>::get_Current()
 Object_t * ShimEnumerator_get_Current_m24995 (ShimEnumerator_t4264 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
