﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.GUIElement>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_148.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIElement>
struct CachedInvokableCall_1_t4668  : public InvokableCall_1_t4669
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIElement>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
