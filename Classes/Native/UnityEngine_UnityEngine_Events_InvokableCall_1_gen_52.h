﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Sphere>
struct UnityAction_1_t3108;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Sphere>
struct InvokableCall_1_t3107  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Sphere>::Delegate
	UnityAction_1_t3108 * ___Delegate_0;
};
