﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>
struct Transform_1_t4278;
// System.Object
struct Object_t;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t51;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m25104 (Transform_1_t4278 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::Invoke(TKey,TValue)
 SurfaceAbstractBehaviour_t51 * Transform_1_Invoke_m25105 (Transform_1_t4278 * __this, int32_t ___key, SurfaceAbstractBehaviour_t51 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m25106 (Transform_1_t4278 * __this, int32_t ___key, SurfaceAbstractBehaviour_t51 * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::EndInvoke(System.IAsyncResult)
 SurfaceAbstractBehaviour_t51 * Transform_1_EndInvoke_m25107 (Transform_1_t4278 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
