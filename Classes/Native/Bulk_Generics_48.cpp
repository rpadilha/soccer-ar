﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t7106_il2cpp_TypeInfo;

// Mono.Math.BigInteger
#include "Mono_Security_Mono_Math_BigInteger.h"

// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger>
extern MethodInfo IEnumerator_1_get_Current_m50763_MethodInfo;
static PropertyInfo IEnumerator_1_t7106____Current_PropertyInfo = 
{
	&IEnumerator_1_t7106_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7106_PropertyInfos[] =
{
	&IEnumerator_1_t7106____Current_PropertyInfo,
	NULL
};
extern Il2CppType BigInteger_t1590_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50763_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50763_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7106_il2cpp_TypeInfo/* declaring_type */
	, &BigInteger_t1590_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50763_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7106_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50763_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t7106_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7106_0_0_0;
extern Il2CppType IEnumerator_1_t7106_1_0_0;
struct IEnumerator_1_t7106;
extern Il2CppGenericClass IEnumerator_1_t7106_GenericClass;
TypeInfo IEnumerator_1_t7106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7106_MethodInfos/* methods */
	, IEnumerator_1_t7106_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7106_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7106_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7106_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7106_0_0_0/* byval_arg */
	, &IEnumerator_1_t7106_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7106_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Math.BigInteger>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_565.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5104_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Math.BigInteger>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_565MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo BigInteger_t1590_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m30898_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBigInteger_t1590_m39907_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Math.BigInteger>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Math.BigInteger>(System.Int32)
#define Array_InternalArray__get_Item_TisBigInteger_t1590_m39907(__this, p0, method) (BigInteger_t1590 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::MoveNext()
// T System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.BigInteger>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5104____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5104, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5104____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5104, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5104_FieldInfos[] =
{
	&InternalEnumerator_1_t5104____array_0_FieldInfo,
	&InternalEnumerator_1_t5104____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5104____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5104_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5104____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5104_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30898_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5104_PropertyInfos[] =
{
	&InternalEnumerator_1_t5104____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5104____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5104_InternalEnumerator_1__ctor_m30894_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30894_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30894_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5104_InternalEnumerator_1__ctor_m30894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30894_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30896_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30896_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30896_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30897_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30897_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30897_GenericMethod/* genericMethod */

};
extern Il2CppType BigInteger_t1590_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30898_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Math.BigInteger>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30898_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* declaring_type */
	, &BigInteger_t1590_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30898_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5104_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30894_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_MethodInfo,
	&InternalEnumerator_1_Dispose_m30896_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30897_MethodInfo,
	&InternalEnumerator_1_get_Current_m30898_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m30897_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30896_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5104_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30895_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30897_MethodInfo,
	&InternalEnumerator_1_Dispose_m30896_MethodInfo,
	&InternalEnumerator_1_get_Current_m30898_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5104_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7106_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5104_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7106_il2cpp_TypeInfo, 7},
};
extern TypeInfo BigInteger_t1590_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5104_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30898_MethodInfo/* Method Usage */,
	&BigInteger_t1590_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBigInteger_t1590_m39907_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5104_0_0_0;
extern Il2CppType InternalEnumerator_1_t5104_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5104_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5104_MethodInfos/* methods */
	, InternalEnumerator_1_t5104_PropertyInfos/* properties */
	, InternalEnumerator_1_t5104_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5104_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5104_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5104_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5104_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5104_1_0_0/* this_arg */
	, InternalEnumerator_1_t5104_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5104_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5104_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5104)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9057_il2cpp_TypeInfo;

#include "Mono.Security_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>
extern MethodInfo ICollection_1_get_Count_m50764_MethodInfo;
static PropertyInfo ICollection_1_t9057____Count_PropertyInfo = 
{
	&ICollection_1_t9057_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50764_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50765_MethodInfo;
static PropertyInfo ICollection_1_t9057____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9057_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50765_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9057_PropertyInfos[] =
{
	&ICollection_1_t9057____Count_PropertyInfo,
	&ICollection_1_t9057____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50764_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::get_Count()
MethodInfo ICollection_1_get_Count_m50764_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50764_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50765_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50765_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50765_GenericMethod/* genericMethod */

};
extern Il2CppType BigInteger_t1590_0_0_0;
extern Il2CppType BigInteger_t1590_0_0_0;
static ParameterInfo ICollection_1_t9057_ICollection_1_Add_m50766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BigInteger_t1590_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50766_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Add(T)
MethodInfo ICollection_1_Add_m50766_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9057_ICollection_1_Add_m50766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50766_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50767_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Clear()
MethodInfo ICollection_1_Clear_m50767_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50767_GenericMethod/* genericMethod */

};
extern Il2CppType BigInteger_t1590_0_0_0;
static ParameterInfo ICollection_1_t9057_ICollection_1_Contains_m50768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BigInteger_t1590_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50768_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Contains(T)
MethodInfo ICollection_1_Contains_m50768_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9057_ICollection_1_Contains_m50768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50768_GenericMethod/* genericMethod */

};
extern Il2CppType BigIntegerU5BU5D_t1593_0_0_0;
extern Il2CppType BigIntegerU5BU5D_t1593_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9057_ICollection_1_CopyTo_m50769_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BigIntegerU5BU5D_t1593_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50769_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50769_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9057_ICollection_1_CopyTo_m50769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50769_GenericMethod/* genericMethod */

};
extern Il2CppType BigInteger_t1590_0_0_0;
static ParameterInfo ICollection_1_t9057_ICollection_1_Remove_m50770_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BigInteger_t1590_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50770_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>::Remove(T)
MethodInfo ICollection_1_Remove_m50770_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9057_ICollection_1_Remove_m50770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50770_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9057_MethodInfos[] =
{
	&ICollection_1_get_Count_m50764_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50765_MethodInfo,
	&ICollection_1_Add_m50766_MethodInfo,
	&ICollection_1_Clear_m50767_MethodInfo,
	&ICollection_1_Contains_m50768_MethodInfo,
	&ICollection_1_CopyTo_m50769_MethodInfo,
	&ICollection_1_Remove_m50770_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t9059_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9057_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9059_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9057_0_0_0;
extern Il2CppType ICollection_1_t9057_1_0_0;
struct ICollection_1_t9057;
extern Il2CppGenericClass ICollection_1_t9057_GenericClass;
TypeInfo ICollection_1_t9057_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9057_MethodInfos/* methods */
	, ICollection_1_t9057_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9057_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9057_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9057_0_0_0/* byval_arg */
	, &ICollection_1_t9057_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9057_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger>
extern Il2CppType IEnumerator_1_t7106_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50771_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50771_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9059_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7106_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50771_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9059_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50771_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9059_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9059_0_0_0;
extern Il2CppType IEnumerable_1_t9059_1_0_0;
struct IEnumerable_1_t9059;
extern Il2CppGenericClass IEnumerable_1_t9059_GenericClass;
TypeInfo IEnumerable_1_t9059_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9059_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9059_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9059_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9059_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9059_0_0_0/* byval_arg */
	, &IEnumerable_1_t9059_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9059_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9058_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Math.BigInteger>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Math.BigInteger>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger>
extern MethodInfo IList_1_get_Item_m50772_MethodInfo;
extern MethodInfo IList_1_set_Item_m50773_MethodInfo;
static PropertyInfo IList_1_t9058____Item_PropertyInfo = 
{
	&IList_1_t9058_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50772_MethodInfo/* get */
	, &IList_1_set_Item_m50773_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9058_PropertyInfos[] =
{
	&IList_1_t9058____Item_PropertyInfo,
	NULL
};
extern Il2CppType BigInteger_t1590_0_0_0;
static ParameterInfo IList_1_t9058_IList_1_IndexOf_m50774_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BigInteger_t1590_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50774_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Math.BigInteger>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50774_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9058_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9058_IList_1_IndexOf_m50774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50774_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BigInteger_t1590_0_0_0;
static ParameterInfo IList_1_t9058_IList_1_Insert_m50775_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BigInteger_t1590_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50775_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50775_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9058_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9058_IList_1_Insert_m50775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50775_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9058_IList_1_RemoveAt_m50776_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50776_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50776_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9058_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9058_IList_1_RemoveAt_m50776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50776_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9058_IList_1_get_Item_m50772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BigInteger_t1590_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50772_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Math.BigInteger>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50772_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9058_il2cpp_TypeInfo/* declaring_type */
	, &BigInteger_t1590_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9058_IList_1_get_Item_m50772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50772_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BigInteger_t1590_0_0_0;
static ParameterInfo IList_1_t9058_IList_1_set_Item_m50773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BigInteger_t1590_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50773_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50773_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9058_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9058_IList_1_set_Item_m50773_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50773_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9058_MethodInfos[] =
{
	&IList_1_IndexOf_m50774_MethodInfo,
	&IList_1_Insert_m50775_MethodInfo,
	&IList_1_RemoveAt_m50776_MethodInfo,
	&IList_1_get_Item_m50772_MethodInfo,
	&IList_1_set_Item_m50773_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9058_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9057_il2cpp_TypeInfo,
	&IEnumerable_1_t9059_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9058_0_0_0;
extern Il2CppType IList_1_t9058_1_0_0;
struct IList_1_t9058;
extern Il2CppGenericClass IList_1_t9058_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9058_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9058_MethodInfos/* methods */
	, IList_1_t9058_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9058_il2cpp_TypeInfo/* element_class */
	, IList_1_t9058_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9058_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9058_0_0_0/* byval_arg */
	, &IList_1_t9058_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9058_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7108_il2cpp_TypeInfo;

// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger/Sign>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger/Sign>
extern MethodInfo IEnumerator_1_get_Current_m50777_MethodInfo;
static PropertyInfo IEnumerator_1_t7108____Current_PropertyInfo = 
{
	&IEnumerator_1_t7108_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50777_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7108_PropertyInfos[] =
{
	&IEnumerator_1_t7108____Current_PropertyInfo,
	NULL
};
extern Il2CppType Sign_t1589_0_0_0;
extern void* RuntimeInvoker_Sign_t1589 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50777_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger/Sign>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50777_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7108_il2cpp_TypeInfo/* declaring_type */
	, &Sign_t1589_0_0_0/* return_type */
	, RuntimeInvoker_Sign_t1589/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50777_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7108_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50777_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7108_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7108_0_0_0;
extern Il2CppType IEnumerator_1_t7108_1_0_0;
struct IEnumerator_1_t7108;
extern Il2CppGenericClass IEnumerator_1_t7108_GenericClass;
TypeInfo IEnumerator_1_t7108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7108_MethodInfos/* methods */
	, IEnumerator_1_t7108_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7108_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7108_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7108_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7108_0_0_0/* byval_arg */
	, &IEnumerator_1_t7108_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7108_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_566.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5105_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_566MethodDeclarations.h"

extern TypeInfo Sign_t1589_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30903_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSign_t1589_m39918_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Math.BigInteger/Sign>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Math.BigInteger/Sign>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSign_t1589_m39918 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30899_MethodInfo;
 void InternalEnumerator_1__ctor_m30899 (InternalEnumerator_1_t5105 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900 (InternalEnumerator_1_t5105 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30903(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30903_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Sign_t1589_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30901_MethodInfo;
 void InternalEnumerator_1_Dispose_m30901 (InternalEnumerator_1_t5105 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30902_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30902 (InternalEnumerator_1_t5105 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30903 (InternalEnumerator_1_t5105 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSign_t1589_m39918(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSign_t1589_m39918_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5105____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5105, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5105____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5105, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5105_FieldInfos[] =
{
	&InternalEnumerator_1_t5105____array_0_FieldInfo,
	&InternalEnumerator_1_t5105____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5105____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5105_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5105____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5105_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30903_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5105_PropertyInfos[] =
{
	&InternalEnumerator_1_t5105____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5105____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5105_InternalEnumerator_1__ctor_m30899_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30899_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30899_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30899/* method */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5105_InternalEnumerator_1__ctor_m30899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30899_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900/* method */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30901_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30901_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30901/* method */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30901_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30902_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30902_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30902/* method */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30902_GenericMethod/* genericMethod */

};
extern Il2CppType Sign_t1589_0_0_0;
extern void* RuntimeInvoker_Sign_t1589 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30903_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30903_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30903/* method */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* declaring_type */
	, &Sign_t1589_0_0_0/* return_type */
	, RuntimeInvoker_Sign_t1589/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30903_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5105_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30899_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_MethodInfo,
	&InternalEnumerator_1_Dispose_m30901_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30902_MethodInfo,
	&InternalEnumerator_1_get_Current_m30903_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5105_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30900_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30902_MethodInfo,
	&InternalEnumerator_1_Dispose_m30901_MethodInfo,
	&InternalEnumerator_1_get_Current_m30903_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5105_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7108_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5105_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7108_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5105_0_0_0;
extern Il2CppType InternalEnumerator_1_t5105_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5105_GenericClass;
TypeInfo InternalEnumerator_1_t5105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5105_MethodInfos/* methods */
	, InternalEnumerator_1_t5105_PropertyInfos/* properties */
	, InternalEnumerator_1_t5105_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5105_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5105_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5105_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5105_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5105_1_0_0/* this_arg */
	, InternalEnumerator_1_t5105_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5105_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5105)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9060_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>
extern MethodInfo ICollection_1_get_Count_m50778_MethodInfo;
static PropertyInfo ICollection_1_t9060____Count_PropertyInfo = 
{
	&ICollection_1_t9060_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50778_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50779_MethodInfo;
static PropertyInfo ICollection_1_t9060____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9060_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9060_PropertyInfos[] =
{
	&ICollection_1_t9060____Count_PropertyInfo,
	&ICollection_1_t9060____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50778_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::get_Count()
MethodInfo ICollection_1_get_Count_m50778_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50778_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50779_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50779_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50779_GenericMethod/* genericMethod */

};
extern Il2CppType Sign_t1589_0_0_0;
extern Il2CppType Sign_t1589_0_0_0;
static ParameterInfo ICollection_1_t9060_ICollection_1_Add_m50780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sign_t1589_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50780_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Add(T)
MethodInfo ICollection_1_Add_m50780_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9060_ICollection_1_Add_m50780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50780_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50781_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Clear()
MethodInfo ICollection_1_Clear_m50781_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50781_GenericMethod/* genericMethod */

};
extern Il2CppType Sign_t1589_0_0_0;
static ParameterInfo ICollection_1_t9060_ICollection_1_Contains_m50782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sign_t1589_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50782_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Contains(T)
MethodInfo ICollection_1_Contains_m50782_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9060_ICollection_1_Contains_m50782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50782_GenericMethod/* genericMethod */

};
extern Il2CppType SignU5BU5D_t5914_0_0_0;
extern Il2CppType SignU5BU5D_t5914_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9060_ICollection_1_CopyTo_m50783_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SignU5BU5D_t5914_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50783_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50783_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9060_ICollection_1_CopyTo_m50783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50783_GenericMethod/* genericMethod */

};
extern Il2CppType Sign_t1589_0_0_0;
static ParameterInfo ICollection_1_t9060_ICollection_1_Remove_m50784_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sign_t1589_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50784_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>::Remove(T)
MethodInfo ICollection_1_Remove_m50784_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9060_ICollection_1_Remove_m50784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50784_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9060_MethodInfos[] =
{
	&ICollection_1_get_Count_m50778_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50779_MethodInfo,
	&ICollection_1_Add_m50780_MethodInfo,
	&ICollection_1_Clear_m50781_MethodInfo,
	&ICollection_1_Contains_m50782_MethodInfo,
	&ICollection_1_CopyTo_m50783_MethodInfo,
	&ICollection_1_Remove_m50784_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9062_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9060_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9062_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9060_0_0_0;
extern Il2CppType ICollection_1_t9060_1_0_0;
struct ICollection_1_t9060;
extern Il2CppGenericClass ICollection_1_t9060_GenericClass;
TypeInfo ICollection_1_t9060_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9060_MethodInfos/* methods */
	, ICollection_1_t9060_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9060_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9060_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9060_0_0_0/* byval_arg */
	, &ICollection_1_t9060_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9060_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>
extern Il2CppType IEnumerator_1_t7108_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50785_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50785_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9062_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7108_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50785_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9062_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50785_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9062_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9062_0_0_0;
extern Il2CppType IEnumerable_1_t9062_1_0_0;
struct IEnumerable_1_t9062;
extern Il2CppGenericClass IEnumerable_1_t9062_GenericClass;
TypeInfo IEnumerable_1_t9062_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9062_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9062_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9062_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9062_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9062_0_0_0/* byval_arg */
	, &IEnumerable_1_t9062_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9062_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9061_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>
extern MethodInfo IList_1_get_Item_m50786_MethodInfo;
extern MethodInfo IList_1_set_Item_m50787_MethodInfo;
static PropertyInfo IList_1_t9061____Item_PropertyInfo = 
{
	&IList_1_t9061_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50786_MethodInfo/* get */
	, &IList_1_set_Item_m50787_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9061_PropertyInfos[] =
{
	&IList_1_t9061____Item_PropertyInfo,
	NULL
};
extern Il2CppType Sign_t1589_0_0_0;
static ParameterInfo IList_1_t9061_IList_1_IndexOf_m50788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sign_t1589_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50788_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50788_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9061_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9061_IList_1_IndexOf_m50788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50788_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sign_t1589_0_0_0;
static ParameterInfo IList_1_t9061_IList_1_Insert_m50789_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Sign_t1589_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50789_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50789_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9061_IList_1_Insert_m50789_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50789_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9061_IList_1_RemoveAt_m50790_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50790_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50790_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9061_IList_1_RemoveAt_m50790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50790_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9061_IList_1_get_Item_m50786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Sign_t1589_0_0_0;
extern void* RuntimeInvoker_Sign_t1589_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50786_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50786_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9061_il2cpp_TypeInfo/* declaring_type */
	, &Sign_t1589_0_0_0/* return_type */
	, RuntimeInvoker_Sign_t1589_Int32_t123/* invoker_method */
	, IList_1_t9061_IList_1_get_Item_m50786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50786_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sign_t1589_0_0_0;
static ParameterInfo IList_1_t9061_IList_1_set_Item_m50787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Sign_t1589_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50787_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50787_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9061_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9061_IList_1_set_Item_m50787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50787_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9061_MethodInfos[] =
{
	&IList_1_IndexOf_m50788_MethodInfo,
	&IList_1_Insert_m50789_MethodInfo,
	&IList_1_RemoveAt_m50790_MethodInfo,
	&IList_1_get_Item_m50786_MethodInfo,
	&IList_1_set_Item_m50787_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9061_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9060_il2cpp_TypeInfo,
	&IEnumerable_1_t9062_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9061_0_0_0;
extern Il2CppType IList_1_t9061_1_0_0;
struct IList_1_t9061;
extern Il2CppGenericClass IList_1_t9061_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9061_MethodInfos/* methods */
	, IList_1_t9061_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9061_il2cpp_TypeInfo/* element_class */
	, IList_1_t9061_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9061_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9061_0_0_0/* byval_arg */
	, &IList_1_t9061_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9061_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7110_il2cpp_TypeInfo;

// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo IEnumerator_1_get_Current_m50791_MethodInfo;
static PropertyInfo IEnumerator_1_t7110____Current_PropertyInfo = 
{
	&IEnumerator_1_t7110_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7110_PropertyInfos[] =
{
	&IEnumerator_1_t7110____Current_PropertyInfo,
	NULL
};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
extern void* RuntimeInvoker_ConfidenceFactor_t1596 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50791_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50791_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7110_il2cpp_TypeInfo/* declaring_type */
	, &ConfidenceFactor_t1596_0_0_0/* return_type */
	, RuntimeInvoker_ConfidenceFactor_t1596/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50791_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7110_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50791_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7110_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7110_0_0_0;
extern Il2CppType IEnumerator_1_t7110_1_0_0;
struct IEnumerator_1_t7110;
extern Il2CppGenericClass IEnumerator_1_t7110_GenericClass;
TypeInfo IEnumerator_1_t7110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7110_MethodInfos/* methods */
	, IEnumerator_1_t7110_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7110_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7110_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7110_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7110_0_0_0/* byval_arg */
	, &IEnumerator_1_t7110_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7110_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_567.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5106_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_567MethodDeclarations.h"

extern TypeInfo ConfidenceFactor_t1596_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30908_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisConfidenceFactor_t1596_m39929_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Math.Prime.ConfidenceFactor>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Math.Prime.ConfidenceFactor>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisConfidenceFactor_t1596_m39929 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30904_MethodInfo;
 void InternalEnumerator_1__ctor_m30904 (InternalEnumerator_1_t5106 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905 (InternalEnumerator_1_t5106 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30908(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30908_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ConfidenceFactor_t1596_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30906_MethodInfo;
 void InternalEnumerator_1_Dispose_m30906 (InternalEnumerator_1_t5106 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30907_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30907 (InternalEnumerator_1_t5106 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30908 (InternalEnumerator_1_t5106 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisConfidenceFactor_t1596_m39929(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisConfidenceFactor_t1596_m39929_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5106____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5106, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5106____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5106, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5106_FieldInfos[] =
{
	&InternalEnumerator_1_t5106____array_0_FieldInfo,
	&InternalEnumerator_1_t5106____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5106____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5106_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5106____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5106_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5106_PropertyInfos[] =
{
	&InternalEnumerator_1_t5106____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5106____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5106_InternalEnumerator_1__ctor_m30904_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30904_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30904_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30904/* method */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5106_InternalEnumerator_1__ctor_m30904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30904_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905/* method */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30906_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30906_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30906/* method */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30906_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30907_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30907_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30907/* method */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30907_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
extern void* RuntimeInvoker_ConfidenceFactor_t1596 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30908_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30908_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30908/* method */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* declaring_type */
	, &ConfidenceFactor_t1596_0_0_0/* return_type */
	, RuntimeInvoker_ConfidenceFactor_t1596/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30908_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5106_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30904_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_MethodInfo,
	&InternalEnumerator_1_Dispose_m30906_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30907_MethodInfo,
	&InternalEnumerator_1_get_Current_m30908_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5106_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30905_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30907_MethodInfo,
	&InternalEnumerator_1_Dispose_m30906_MethodInfo,
	&InternalEnumerator_1_get_Current_m30908_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5106_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7110_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5106_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7110_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5106_0_0_0;
extern Il2CppType InternalEnumerator_1_t5106_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5106_GenericClass;
TypeInfo InternalEnumerator_1_t5106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5106_MethodInfos/* methods */
	, InternalEnumerator_1_t5106_PropertyInfos/* properties */
	, InternalEnumerator_1_t5106_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5106_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5106_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5106_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5106_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5106_1_0_0/* this_arg */
	, InternalEnumerator_1_t5106_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5106_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5106)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9063_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo ICollection_1_get_Count_m50792_MethodInfo;
static PropertyInfo ICollection_1_t9063____Count_PropertyInfo = 
{
	&ICollection_1_t9063_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50792_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50793_MethodInfo;
static PropertyInfo ICollection_1_t9063____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9063_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50793_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9063_PropertyInfos[] =
{
	&ICollection_1_t9063____Count_PropertyInfo,
	&ICollection_1_t9063____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50792_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_Count()
MethodInfo ICollection_1_get_Count_m50792_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50792_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50793_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50793_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50793_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
static ParameterInfo ICollection_1_t9063_ICollection_1_Add_m50794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1596_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50794_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Add(T)
MethodInfo ICollection_1_Add_m50794_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9063_ICollection_1_Add_m50794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50794_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50795_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Clear()
MethodInfo ICollection_1_Clear_m50795_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50795_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
static ParameterInfo ICollection_1_t9063_ICollection_1_Contains_m50796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1596_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50796_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Contains(T)
MethodInfo ICollection_1_Contains_m50796_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9063_ICollection_1_Contains_m50796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50796_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactorU5BU5D_t5915_0_0_0;
extern Il2CppType ConfidenceFactorU5BU5D_t5915_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9063_ICollection_1_CopyTo_m50797_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactorU5BU5D_t5915_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50797_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50797_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9063_ICollection_1_CopyTo_m50797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50797_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
static ParameterInfo ICollection_1_t9063_ICollection_1_Remove_m50798_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1596_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50798_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Remove(T)
MethodInfo ICollection_1_Remove_m50798_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9063_ICollection_1_Remove_m50798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50798_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9063_MethodInfos[] =
{
	&ICollection_1_get_Count_m50792_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50793_MethodInfo,
	&ICollection_1_Add_m50794_MethodInfo,
	&ICollection_1_Clear_m50795_MethodInfo,
	&ICollection_1_Contains_m50796_MethodInfo,
	&ICollection_1_CopyTo_m50797_MethodInfo,
	&ICollection_1_Remove_m50798_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9065_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9063_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9065_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9063_0_0_0;
extern Il2CppType ICollection_1_t9063_1_0_0;
struct ICollection_1_t9063;
extern Il2CppGenericClass ICollection_1_t9063_GenericClass;
TypeInfo ICollection_1_t9063_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9063_MethodInfos/* methods */
	, ICollection_1_t9063_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9063_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9063_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9063_0_0_0/* byval_arg */
	, &ICollection_1_t9063_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9063_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType IEnumerator_1_t7110_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50799_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50799_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9065_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7110_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50799_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9065_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50799_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9065_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9065_0_0_0;
extern Il2CppType IEnumerable_1_t9065_1_0_0;
struct IEnumerable_1_t9065;
extern Il2CppGenericClass IEnumerable_1_t9065_GenericClass;
TypeInfo IEnumerable_1_t9065_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9065_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9065_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9065_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9065_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9065_0_0_0/* byval_arg */
	, &IEnumerable_1_t9065_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9065_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9064_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo IList_1_get_Item_m50800_MethodInfo;
extern MethodInfo IList_1_set_Item_m50801_MethodInfo;
static PropertyInfo IList_1_t9064____Item_PropertyInfo = 
{
	&IList_1_t9064_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50800_MethodInfo/* get */
	, &IList_1_set_Item_m50801_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9064_PropertyInfos[] =
{
	&IList_1_t9064____Item_PropertyInfo,
	NULL
};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
static ParameterInfo IList_1_t9064_IList_1_IndexOf_m50802_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1596_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50802_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50802_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9064_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9064_IList_1_IndexOf_m50802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50802_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
static ParameterInfo IList_1_t9064_IList_1_Insert_m50803_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1596_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50803_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50803_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9064_IList_1_Insert_m50803_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50803_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9064_IList_1_RemoveAt_m50804_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50804_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50804_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9064_IList_1_RemoveAt_m50804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50804_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9064_IList_1_get_Item_m50800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
extern void* RuntimeInvoker_ConfidenceFactor_t1596_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50800_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50800_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9064_il2cpp_TypeInfo/* declaring_type */
	, &ConfidenceFactor_t1596_0_0_0/* return_type */
	, RuntimeInvoker_ConfidenceFactor_t1596_Int32_t123/* invoker_method */
	, IList_1_t9064_IList_1_get_Item_m50800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50800_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ConfidenceFactor_t1596_0_0_0;
static ParameterInfo IList_1_t9064_IList_1_set_Item_m50801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1596_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50801_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50801_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9064_IList_1_set_Item_m50801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50801_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9064_MethodInfos[] =
{
	&IList_1_IndexOf_m50802_MethodInfo,
	&IList_1_Insert_m50803_MethodInfo,
	&IList_1_RemoveAt_m50804_MethodInfo,
	&IList_1_get_Item_m50800_MethodInfo,
	&IList_1_set_Item_m50801_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9064_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9063_il2cpp_TypeInfo,
	&IEnumerable_1_t9065_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9064_0_0_0;
extern Il2CppType IList_1_t9064_1_0_0;
struct IList_1_t9064;
extern Il2CppGenericClass IList_1_t9064_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9064_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9064_MethodInfos/* methods */
	, IList_1_t9064_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9064_il2cpp_TypeInfo/* element_class */
	, IList_1_t9064_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9064_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9064_0_0_0/* byval_arg */
	, &IList_1_t9064_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9064_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7112_il2cpp_TypeInfo;

// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizes.h"


// T System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>
extern MethodInfo IEnumerator_1_get_Current_m50805_MethodInfo;
static PropertyInfo IEnumerator_1_t7112____Current_PropertyInfo = 
{
	&IEnumerator_1_t7112_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50805_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7112_PropertyInfos[] =
{
	&IEnumerator_1_t7112____Current_PropertyInfo,
	NULL
};
extern Il2CppType KeySizes_t1717_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50805_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50805_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7112_il2cpp_TypeInfo/* declaring_type */
	, &KeySizes_t1717_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50805_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7112_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50805_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7112_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7112_0_0_0;
extern Il2CppType IEnumerator_1_t7112_1_0_0;
struct IEnumerator_1_t7112;
extern Il2CppGenericClass IEnumerator_1_t7112_GenericClass;
TypeInfo IEnumerator_1_t7112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7112_MethodInfos/* methods */
	, IEnumerator_1_t7112_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7112_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7112_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7112_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7112_0_0_0/* byval_arg */
	, &IEnumerator_1_t7112_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7112_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_568.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5107_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_568MethodDeclarations.h"

extern TypeInfo KeySizes_t1717_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30913_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeySizes_t1717_m39940_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.KeySizes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.KeySizes>(System.Int32)
#define Array_InternalArray__get_Item_TisKeySizes_t1717_m39940(__this, p0, method) (KeySizes_t1717 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5107____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5107, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5107____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5107, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5107_FieldInfos[] =
{
	&InternalEnumerator_1_t5107____array_0_FieldInfo,
	&InternalEnumerator_1_t5107____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5107____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5107_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5107____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5107_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30913_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5107_PropertyInfos[] =
{
	&InternalEnumerator_1_t5107____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5107____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5107_InternalEnumerator_1__ctor_m30909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30909_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30909_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5107_InternalEnumerator_1__ctor_m30909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30909_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30911_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30911_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30911_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30912_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30912_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30912_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1717_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30913_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30913_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* declaring_type */
	, &KeySizes_t1717_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30913_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5107_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30909_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_MethodInfo,
	&InternalEnumerator_1_Dispose_m30911_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30912_MethodInfo,
	&InternalEnumerator_1_get_Current_m30913_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30912_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30911_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5107_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30910_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30912_MethodInfo,
	&InternalEnumerator_1_Dispose_m30911_MethodInfo,
	&InternalEnumerator_1_get_Current_m30913_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5107_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7112_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5107_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7112_il2cpp_TypeInfo, 7},
};
extern TypeInfo KeySizes_t1717_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5107_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30913_MethodInfo/* Method Usage */,
	&KeySizes_t1717_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisKeySizes_t1717_m39940_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5107_0_0_0;
extern Il2CppType InternalEnumerator_1_t5107_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5107_GenericClass;
TypeInfo InternalEnumerator_1_t5107_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5107_MethodInfos/* methods */
	, InternalEnumerator_1_t5107_PropertyInfos/* properties */
	, InternalEnumerator_1_t5107_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5107_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5107_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5107_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5107_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5107_1_0_0/* this_arg */
	, InternalEnumerator_1_t5107_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5107_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5107_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5107)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9066_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>
extern MethodInfo ICollection_1_get_Count_m50806_MethodInfo;
static PropertyInfo ICollection_1_t9066____Count_PropertyInfo = 
{
	&ICollection_1_t9066_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50806_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50807_MethodInfo;
static PropertyInfo ICollection_1_t9066____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9066_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50807_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9066_PropertyInfos[] =
{
	&ICollection_1_t9066____Count_PropertyInfo,
	&ICollection_1_t9066____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50806_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_Count()
MethodInfo ICollection_1_get_Count_m50806_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50806_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50807_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50807_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50807_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1717_0_0_0;
extern Il2CppType KeySizes_t1717_0_0_0;
static ParameterInfo ICollection_1_t9066_ICollection_1_Add_m50808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1717_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50808_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Add(T)
MethodInfo ICollection_1_Add_m50808_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9066_ICollection_1_Add_m50808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50808_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50809_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Clear()
MethodInfo ICollection_1_Clear_m50809_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50809_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1717_0_0_0;
static ParameterInfo ICollection_1_t9066_ICollection_1_Contains_m50810_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1717_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50810_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Contains(T)
MethodInfo ICollection_1_Contains_m50810_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9066_ICollection_1_Contains_m50810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50810_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizesU5BU5D_t1618_0_0_0;
extern Il2CppType KeySizesU5BU5D_t1618_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9066_ICollection_1_CopyTo_m50811_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeySizesU5BU5D_t1618_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50811_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50811_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9066_ICollection_1_CopyTo_m50811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50811_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1717_0_0_0;
static ParameterInfo ICollection_1_t9066_ICollection_1_Remove_m50812_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1717_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50812_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Remove(T)
MethodInfo ICollection_1_Remove_m50812_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9066_ICollection_1_Remove_m50812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50812_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9066_MethodInfos[] =
{
	&ICollection_1_get_Count_m50806_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50807_MethodInfo,
	&ICollection_1_Add_m50808_MethodInfo,
	&ICollection_1_Clear_m50809_MethodInfo,
	&ICollection_1_Contains_m50810_MethodInfo,
	&ICollection_1_CopyTo_m50811_MethodInfo,
	&ICollection_1_Remove_m50812_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9068_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9066_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9068_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9066_0_0_0;
extern Il2CppType ICollection_1_t9066_1_0_0;
struct ICollection_1_t9066;
extern Il2CppGenericClass ICollection_1_t9066_GenericClass;
TypeInfo ICollection_1_t9066_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9066_MethodInfos/* methods */
	, ICollection_1_t9066_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9066_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9066_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9066_0_0_0/* byval_arg */
	, &ICollection_1_t9066_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9066_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>
extern Il2CppType IEnumerator_1_t7112_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50813_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50813_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9068_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50813_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9068_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50813_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9068_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9068_0_0_0;
extern Il2CppType IEnumerable_1_t9068_1_0_0;
struct IEnumerable_1_t9068;
extern Il2CppGenericClass IEnumerable_1_t9068_GenericClass;
TypeInfo IEnumerable_1_t9068_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9068_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9068_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9068_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9068_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9068_0_0_0/* byval_arg */
	, &IEnumerable_1_t9068_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9068_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9067_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>
extern MethodInfo IList_1_get_Item_m50814_MethodInfo;
extern MethodInfo IList_1_set_Item_m50815_MethodInfo;
static PropertyInfo IList_1_t9067____Item_PropertyInfo = 
{
	&IList_1_t9067_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50814_MethodInfo/* get */
	, &IList_1_set_Item_m50815_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9067_PropertyInfos[] =
{
	&IList_1_t9067____Item_PropertyInfo,
	NULL
};
extern Il2CppType KeySizes_t1717_0_0_0;
static ParameterInfo IList_1_t9067_IList_1_IndexOf_m50816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1717_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50816_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50816_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9067_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9067_IList_1_IndexOf_m50816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50816_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeySizes_t1717_0_0_0;
static ParameterInfo IList_1_t9067_IList_1_Insert_m50817_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1717_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50817_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50817_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9067_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9067_IList_1_Insert_m50817_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50817_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9067_IList_1_RemoveAt_m50818_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50818_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50818_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9067_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9067_IList_1_RemoveAt_m50818_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50818_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9067_IList_1_get_Item_m50814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType KeySizes_t1717_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50814_GenericMethod;
// T System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50814_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9067_il2cpp_TypeInfo/* declaring_type */
	, &KeySizes_t1717_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9067_IList_1_get_Item_m50814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeySizes_t1717_0_0_0;
static ParameterInfo IList_1_t9067_IList_1_set_Item_m50815_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1717_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50815_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50815_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9067_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9067_IList_1_set_Item_m50815_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50815_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9067_MethodInfos[] =
{
	&IList_1_IndexOf_m50816_MethodInfo,
	&IList_1_Insert_m50817_MethodInfo,
	&IList_1_RemoveAt_m50818_MethodInfo,
	&IList_1_get_Item_m50814_MethodInfo,
	&IList_1_set_Item_m50815_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9067_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9066_il2cpp_TypeInfo,
	&IEnumerable_1_t9068_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9067_0_0_0;
extern Il2CppType IList_1_t9067_1_0_0;
struct IList_1_t9067;
extern Il2CppGenericClass IList_1_t9067_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9067_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9067_MethodInfos/* methods */
	, IList_1_t9067_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9067_il2cpp_TypeInfo/* element_class */
	, IList_1_t9067_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9067_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9067_0_0_0/* byval_arg */
	, &IList_1_t9067_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9067_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7114_il2cpp_TypeInfo;

// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo IEnumerator_1_get_Current_m50819_MethodInfo;
static PropertyInfo IEnumerator_1_t7114____Current_PropertyInfo = 
{
	&IEnumerator_1_t7114_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50819_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7114_PropertyInfos[] =
{
	&IEnumerator_1_t7114____Current_PropertyInfo,
	NULL
};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
extern void* RuntimeInvoker_X509ChainStatusFlags_t1625 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50819_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50819_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7114_il2cpp_TypeInfo/* declaring_type */
	, &X509ChainStatusFlags_t1625_0_0_0/* return_type */
	, RuntimeInvoker_X509ChainStatusFlags_t1625/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50819_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7114_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50819_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7114_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7114_0_0_0;
extern Il2CppType IEnumerator_1_t7114_1_0_0;
struct IEnumerator_1_t7114;
extern Il2CppGenericClass IEnumerator_1_t7114_GenericClass;
TypeInfo IEnumerator_1_t7114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7114_MethodInfos/* methods */
	, IEnumerator_1_t7114_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7114_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7114_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7114_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7114_0_0_0/* byval_arg */
	, &IEnumerator_1_t7114_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7114_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_569.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5108_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_569MethodDeclarations.h"

extern TypeInfo X509ChainStatusFlags_t1625_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30918_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1625_m39951_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.X509ChainStatusFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.X509ChainStatusFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1625_m39951 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30914_MethodInfo;
 void InternalEnumerator_1__ctor_m30914 (InternalEnumerator_1_t5108 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915 (InternalEnumerator_1_t5108 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30918(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30918_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&X509ChainStatusFlags_t1625_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30916_MethodInfo;
 void InternalEnumerator_1_Dispose_m30916 (InternalEnumerator_1_t5108 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30917_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30917 (InternalEnumerator_1_t5108 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30918 (InternalEnumerator_1_t5108 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1625_m39951(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1625_m39951_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5108____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5108, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5108____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5108, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5108_FieldInfos[] =
{
	&InternalEnumerator_1_t5108____array_0_FieldInfo,
	&InternalEnumerator_1_t5108____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5108____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5108_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5108____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5108_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30918_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5108_PropertyInfos[] =
{
	&InternalEnumerator_1_t5108____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5108____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5108_InternalEnumerator_1__ctor_m30914_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30914_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30914_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30914/* method */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5108_InternalEnumerator_1__ctor_m30914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30914_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915/* method */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30916_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30916_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30916/* method */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30916_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30917_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30917_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30917/* method */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30917_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
extern void* RuntimeInvoker_X509ChainStatusFlags_t1625 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30918_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30918_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30918/* method */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* declaring_type */
	, &X509ChainStatusFlags_t1625_0_0_0/* return_type */
	, RuntimeInvoker_X509ChainStatusFlags_t1625/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30918_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5108_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30914_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_MethodInfo,
	&InternalEnumerator_1_Dispose_m30916_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30917_MethodInfo,
	&InternalEnumerator_1_get_Current_m30918_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5108_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30915_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30917_MethodInfo,
	&InternalEnumerator_1_Dispose_m30916_MethodInfo,
	&InternalEnumerator_1_get_Current_m30918_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5108_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7114_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5108_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7114_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5108_0_0_0;
extern Il2CppType InternalEnumerator_1_t5108_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5108_GenericClass;
TypeInfo InternalEnumerator_1_t5108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5108_MethodInfos/* methods */
	, InternalEnumerator_1_t5108_PropertyInfos/* properties */
	, InternalEnumerator_1_t5108_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5108_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5108_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5108_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5108_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5108_1_0_0/* this_arg */
	, InternalEnumerator_1_t5108_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5108_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5108)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9069_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo ICollection_1_get_Count_m50820_MethodInfo;
static PropertyInfo ICollection_1_t9069____Count_PropertyInfo = 
{
	&ICollection_1_t9069_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50820_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50821_MethodInfo;
static PropertyInfo ICollection_1_t9069____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9069_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50821_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9069_PropertyInfos[] =
{
	&ICollection_1_t9069____Count_PropertyInfo,
	&ICollection_1_t9069____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50820_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m50820_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50820_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50821_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50821_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50821_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
static ParameterInfo ICollection_1_t9069_ICollection_1_Add_m50822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1625_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50822_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Add(T)
MethodInfo ICollection_1_Add_m50822_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9069_ICollection_1_Add_m50822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50822_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50823_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Clear()
MethodInfo ICollection_1_Clear_m50823_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50823_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
static ParameterInfo ICollection_1_t9069_ICollection_1_Contains_m50824_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1625_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50824_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m50824_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9069_ICollection_1_Contains_m50824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50824_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlagsU5BU5D_t5916_0_0_0;
extern Il2CppType X509ChainStatusFlagsU5BU5D_t5916_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9069_ICollection_1_CopyTo_m50825_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlagsU5BU5D_t5916_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50825_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50825_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9069_ICollection_1_CopyTo_m50825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50825_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
static ParameterInfo ICollection_1_t9069_ICollection_1_Remove_m50826_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1625_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50826_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m50826_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9069_ICollection_1_Remove_m50826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50826_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9069_MethodInfos[] =
{
	&ICollection_1_get_Count_m50820_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50821_MethodInfo,
	&ICollection_1_Add_m50822_MethodInfo,
	&ICollection_1_Clear_m50823_MethodInfo,
	&ICollection_1_Contains_m50824_MethodInfo,
	&ICollection_1_CopyTo_m50825_MethodInfo,
	&ICollection_1_Remove_m50826_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9071_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9069_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9071_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9069_0_0_0;
extern Il2CppType ICollection_1_t9069_1_0_0;
struct ICollection_1_t9069;
extern Il2CppGenericClass ICollection_1_t9069_GenericClass;
TypeInfo ICollection_1_t9069_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9069_MethodInfos/* methods */
	, ICollection_1_t9069_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9069_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9069_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9069_0_0_0/* byval_arg */
	, &ICollection_1_t9069_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9069_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>
extern Il2CppType IEnumerator_1_t7114_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50827_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50827_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9071_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50827_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9071_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50827_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9071_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9071_0_0_0;
extern Il2CppType IEnumerable_1_t9071_1_0_0;
struct IEnumerable_1_t9071;
extern Il2CppGenericClass IEnumerable_1_t9071_GenericClass;
TypeInfo IEnumerable_1_t9071_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9071_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9071_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9071_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9071_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9071_0_0_0/* byval_arg */
	, &IEnumerable_1_t9071_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9071_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9070_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo IList_1_get_Item_m50828_MethodInfo;
extern MethodInfo IList_1_set_Item_m50829_MethodInfo;
static PropertyInfo IList_1_t9070____Item_PropertyInfo = 
{
	&IList_1_t9070_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50828_MethodInfo/* get */
	, &IList_1_set_Item_m50829_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9070_PropertyInfos[] =
{
	&IList_1_t9070____Item_PropertyInfo,
	NULL
};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
static ParameterInfo IList_1_t9070_IList_1_IndexOf_m50830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1625_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50830_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50830_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9070_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9070_IList_1_IndexOf_m50830_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50830_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
static ParameterInfo IList_1_t9070_IList_1_Insert_m50831_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1625_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50831_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50831_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9070_IList_1_Insert_m50831_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50831_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9070_IList_1_RemoveAt_m50832_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50832_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50832_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9070_IList_1_RemoveAt_m50832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50832_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9070_IList_1_get_Item_m50828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
extern void* RuntimeInvoker_X509ChainStatusFlags_t1625_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50828_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50828_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9070_il2cpp_TypeInfo/* declaring_type */
	, &X509ChainStatusFlags_t1625_0_0_0/* return_type */
	, RuntimeInvoker_X509ChainStatusFlags_t1625_Int32_t123/* invoker_method */
	, IList_1_t9070_IList_1_get_Item_m50828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType X509ChainStatusFlags_t1625_0_0_0;
static ParameterInfo IList_1_t9070_IList_1_set_Item_m50829_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1625_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50829_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50829_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9070_IList_1_set_Item_m50829_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50829_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9070_MethodInfos[] =
{
	&IList_1_IndexOf_m50830_MethodInfo,
	&IList_1_Insert_m50831_MethodInfo,
	&IList_1_RemoveAt_m50832_MethodInfo,
	&IList_1_get_Item_m50828_MethodInfo,
	&IList_1_set_Item_m50829_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9070_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9069_il2cpp_TypeInfo,
	&IEnumerable_1_t9071_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9070_0_0_0;
extern Il2CppType IList_1_t9070_1_0_0;
struct IList_1_t9070;
extern Il2CppGenericClass IList_1_t9070_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9070_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9070_MethodInfos/* methods */
	, IList_1_t9070_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9070_il2cpp_TypeInfo/* element_class */
	, IList_1_t9070_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9070_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9070_0_0_0/* byval_arg */
	, &IList_1_t9070_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9070_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7116_il2cpp_TypeInfo;

// Mono.Security.X509.Extensions.KeyUsages
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsages.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo IEnumerator_1_get_Current_m50833_MethodInfo;
static PropertyInfo IEnumerator_1_t7116____Current_PropertyInfo = 
{
	&IEnumerator_1_t7116_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50833_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7116_PropertyInfos[] =
{
	&IEnumerator_1_t7116____Current_PropertyInfo,
	NULL
};
extern Il2CppType KeyUsages_t1630_0_0_0;
extern void* RuntimeInvoker_KeyUsages_t1630 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50833_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50833_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7116_il2cpp_TypeInfo/* declaring_type */
	, &KeyUsages_t1630_0_0_0/* return_type */
	, RuntimeInvoker_KeyUsages_t1630/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50833_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7116_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50833_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7116_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7116_0_0_0;
extern Il2CppType IEnumerator_1_t7116_1_0_0;
struct IEnumerator_1_t7116;
extern Il2CppGenericClass IEnumerator_1_t7116_GenericClass;
TypeInfo IEnumerator_1_t7116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7116_MethodInfos/* methods */
	, IEnumerator_1_t7116_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7116_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7116_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7116_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7116_0_0_0/* byval_arg */
	, &IEnumerator_1_t7116_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7116_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_570.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5109_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_570MethodDeclarations.h"

extern TypeInfo KeyUsages_t1630_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30923_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyUsages_t1630_m39962_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.KeyUsages>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.KeyUsages>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisKeyUsages_t1630_m39962 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30919_MethodInfo;
 void InternalEnumerator_1__ctor_m30919 (InternalEnumerator_1_t5109 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920 (InternalEnumerator_1_t5109 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30923(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30923_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&KeyUsages_t1630_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30921_MethodInfo;
 void InternalEnumerator_1_Dispose_m30921 (InternalEnumerator_1_t5109 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30922_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30922 (InternalEnumerator_1_t5109 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30923 (InternalEnumerator_1_t5109 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisKeyUsages_t1630_m39962(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisKeyUsages_t1630_m39962_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5109____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5109, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5109____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5109, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5109_FieldInfos[] =
{
	&InternalEnumerator_1_t5109____array_0_FieldInfo,
	&InternalEnumerator_1_t5109____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5109____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5109_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5109____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5109_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5109_PropertyInfos[] =
{
	&InternalEnumerator_1_t5109____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5109____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5109_InternalEnumerator_1__ctor_m30919_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30919_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30919_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30919/* method */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5109_InternalEnumerator_1__ctor_m30919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30919_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920/* method */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30921_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30921_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30921/* method */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30921_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30922_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30922_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30922/* method */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30922_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1630_0_0_0;
extern void* RuntimeInvoker_KeyUsages_t1630 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30923_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30923_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30923/* method */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* declaring_type */
	, &KeyUsages_t1630_0_0_0/* return_type */
	, RuntimeInvoker_KeyUsages_t1630/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30923_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5109_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30919_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_MethodInfo,
	&InternalEnumerator_1_Dispose_m30921_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30922_MethodInfo,
	&InternalEnumerator_1_get_Current_m30923_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5109_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30920_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30922_MethodInfo,
	&InternalEnumerator_1_Dispose_m30921_MethodInfo,
	&InternalEnumerator_1_get_Current_m30923_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5109_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7116_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5109_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7116_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5109_0_0_0;
extern Il2CppType InternalEnumerator_1_t5109_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5109_GenericClass;
TypeInfo InternalEnumerator_1_t5109_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5109_MethodInfos/* methods */
	, InternalEnumerator_1_t5109_PropertyInfos/* properties */
	, InternalEnumerator_1_t5109_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5109_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5109_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5109_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5109_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5109_1_0_0/* this_arg */
	, InternalEnumerator_1_t5109_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5109_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5109)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9072_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo ICollection_1_get_Count_m50834_MethodInfo;
static PropertyInfo ICollection_1_t9072____Count_PropertyInfo = 
{
	&ICollection_1_t9072_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50834_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50835_MethodInfo;
static PropertyInfo ICollection_1_t9072____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9072_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50835_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9072_PropertyInfos[] =
{
	&ICollection_1_t9072____Count_PropertyInfo,
	&ICollection_1_t9072____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50834_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_Count()
MethodInfo ICollection_1_get_Count_m50834_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50834_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50835_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50835_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50835_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1630_0_0_0;
extern Il2CppType KeyUsages_t1630_0_0_0;
static ParameterInfo ICollection_1_t9072_ICollection_1_Add_m50836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1630_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50836_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Add(T)
MethodInfo ICollection_1_Add_m50836_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9072_ICollection_1_Add_m50836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50836_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50837_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Clear()
MethodInfo ICollection_1_Clear_m50837_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50837_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1630_0_0_0;
static ParameterInfo ICollection_1_t9072_ICollection_1_Contains_m50838_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1630_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50838_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Contains(T)
MethodInfo ICollection_1_Contains_m50838_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9072_ICollection_1_Contains_m50838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50838_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsagesU5BU5D_t5917_0_0_0;
extern Il2CppType KeyUsagesU5BU5D_t5917_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9072_ICollection_1_CopyTo_m50839_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsagesU5BU5D_t5917_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50839_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50839_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9072_ICollection_1_CopyTo_m50839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50839_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1630_0_0_0;
static ParameterInfo ICollection_1_t9072_ICollection_1_Remove_m50840_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1630_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50840_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Remove(T)
MethodInfo ICollection_1_Remove_m50840_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9072_ICollection_1_Remove_m50840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50840_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9072_MethodInfos[] =
{
	&ICollection_1_get_Count_m50834_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50835_MethodInfo,
	&ICollection_1_Add_m50836_MethodInfo,
	&ICollection_1_Clear_m50837_MethodInfo,
	&ICollection_1_Contains_m50838_MethodInfo,
	&ICollection_1_CopyTo_m50839_MethodInfo,
	&ICollection_1_Remove_m50840_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9074_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9072_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9074_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9072_0_0_0;
extern Il2CppType ICollection_1_t9072_1_0_0;
struct ICollection_1_t9072;
extern Il2CppGenericClass ICollection_1_t9072_GenericClass;
TypeInfo ICollection_1_t9072_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9072_MethodInfos/* methods */
	, ICollection_1_t9072_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9072_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9072_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9072_0_0_0/* byval_arg */
	, &ICollection_1_t9072_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9072_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>
extern Il2CppType IEnumerator_1_t7116_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50841_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50841_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9074_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50841_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9074_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50841_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9074_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9074_0_0_0;
extern Il2CppType IEnumerable_1_t9074_1_0_0;
struct IEnumerable_1_t9074;
extern Il2CppGenericClass IEnumerable_1_t9074_GenericClass;
TypeInfo IEnumerable_1_t9074_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9074_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9074_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9074_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9074_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9074_0_0_0/* byval_arg */
	, &IEnumerable_1_t9074_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9074_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9073_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo IList_1_get_Item_m50842_MethodInfo;
extern MethodInfo IList_1_set_Item_m50843_MethodInfo;
static PropertyInfo IList_1_t9073____Item_PropertyInfo = 
{
	&IList_1_t9073_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50842_MethodInfo/* get */
	, &IList_1_set_Item_m50843_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9073_PropertyInfos[] =
{
	&IList_1_t9073____Item_PropertyInfo,
	NULL
};
extern Il2CppType KeyUsages_t1630_0_0_0;
static ParameterInfo IList_1_t9073_IList_1_IndexOf_m50844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1630_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50844_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50844_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9073_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9073_IList_1_IndexOf_m50844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50844_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeyUsages_t1630_0_0_0;
static ParameterInfo IList_1_t9073_IList_1_Insert_m50845_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1630_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50845_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50845_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9073_IList_1_Insert_m50845_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50845_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9073_IList_1_RemoveAt_m50846_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50846_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50846_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9073_IList_1_RemoveAt_m50846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50846_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9073_IList_1_get_Item_m50842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType KeyUsages_t1630_0_0_0;
extern void* RuntimeInvoker_KeyUsages_t1630_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50842_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50842_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9073_il2cpp_TypeInfo/* declaring_type */
	, &KeyUsages_t1630_0_0_0/* return_type */
	, RuntimeInvoker_KeyUsages_t1630_Int32_t123/* invoker_method */
	, IList_1_t9073_IList_1_get_Item_m50842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50842_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType KeyUsages_t1630_0_0_0;
static ParameterInfo IList_1_t9073_IList_1_set_Item_m50843_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1630_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50843_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50843_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9073_IList_1_set_Item_m50843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50843_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9073_MethodInfos[] =
{
	&IList_1_IndexOf_m50844_MethodInfo,
	&IList_1_Insert_m50845_MethodInfo,
	&IList_1_RemoveAt_m50846_MethodInfo,
	&IList_1_get_Item_m50842_MethodInfo,
	&IList_1_set_Item_m50843_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9073_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9072_il2cpp_TypeInfo,
	&IEnumerable_1_t9074_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9073_0_0_0;
extern Il2CppType IList_1_t9073_1_0_0;
struct IList_1_t9073;
extern Il2CppGenericClass IList_1_t9073_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9073_MethodInfos/* methods */
	, IList_1_t9073_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9073_il2cpp_TypeInfo/* element_class */
	, IList_1_t9073_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9073_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9073_0_0_0/* byval_arg */
	, &IList_1_t9073_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9073_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7118_il2cpp_TypeInfo;

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo IEnumerator_1_get_Current_m50847_MethodInfo;
static PropertyInfo IEnumerator_1_t7118____Current_PropertyInfo = 
{
	&IEnumerator_1_t7118_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50847_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7118_PropertyInfos[] =
{
	&IEnumerator_1_t7118____Current_PropertyInfo,
	NULL
};
extern Il2CppType CertTypes_t1632_0_0_0;
extern void* RuntimeInvoker_CertTypes_t1632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50847_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50847_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7118_il2cpp_TypeInfo/* declaring_type */
	, &CertTypes_t1632_0_0_0/* return_type */
	, RuntimeInvoker_CertTypes_t1632/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50847_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7118_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50847_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7118_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7118_0_0_0;
extern Il2CppType IEnumerator_1_t7118_1_0_0;
struct IEnumerator_1_t7118;
extern Il2CppGenericClass IEnumerator_1_t7118_GenericClass;
TypeInfo IEnumerator_1_t7118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7118_MethodInfos/* methods */
	, IEnumerator_1_t7118_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7118_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7118_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7118_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7118_0_0_0/* byval_arg */
	, &IEnumerator_1_t7118_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7118_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_571.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5110_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_571MethodDeclarations.h"

extern TypeInfo CertTypes_t1632_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30928_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCertTypes_t1632_m39973_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCertTypes_t1632_m39973 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30924_MethodInfo;
 void InternalEnumerator_1__ctor_m30924 (InternalEnumerator_1_t5110 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925 (InternalEnumerator_1_t5110 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30928(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30928_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CertTypes_t1632_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30926_MethodInfo;
 void InternalEnumerator_1_Dispose_m30926 (InternalEnumerator_1_t5110 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30927_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30927 (InternalEnumerator_1_t5110 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30928 (InternalEnumerator_1_t5110 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCertTypes_t1632_m39973(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCertTypes_t1632_m39973_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5110____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5110, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5110____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5110, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5110_FieldInfos[] =
{
	&InternalEnumerator_1_t5110____array_0_FieldInfo,
	&InternalEnumerator_1_t5110____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5110____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5110_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5110____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5110_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30928_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5110_PropertyInfos[] =
{
	&InternalEnumerator_1_t5110____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5110____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5110_InternalEnumerator_1__ctor_m30924_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30924_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30924_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30924/* method */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5110_InternalEnumerator_1__ctor_m30924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30924_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925/* method */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30926_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30926_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30926/* method */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30926_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30927_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30927_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30927/* method */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30927_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1632_0_0_0;
extern void* RuntimeInvoker_CertTypes_t1632 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30928_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30928_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30928/* method */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* declaring_type */
	, &CertTypes_t1632_0_0_0/* return_type */
	, RuntimeInvoker_CertTypes_t1632/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30928_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5110_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30924_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_MethodInfo,
	&InternalEnumerator_1_Dispose_m30926_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30927_MethodInfo,
	&InternalEnumerator_1_get_Current_m30928_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5110_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30925_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30927_MethodInfo,
	&InternalEnumerator_1_Dispose_m30926_MethodInfo,
	&InternalEnumerator_1_get_Current_m30928_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5110_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7118_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5110_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7118_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5110_0_0_0;
extern Il2CppType InternalEnumerator_1_t5110_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5110_GenericClass;
TypeInfo InternalEnumerator_1_t5110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5110_MethodInfos/* methods */
	, InternalEnumerator_1_t5110_PropertyInfos/* properties */
	, InternalEnumerator_1_t5110_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5110_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5110_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5110_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5110_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5110_1_0_0/* this_arg */
	, InternalEnumerator_1_t5110_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5110_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5110)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9075_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo ICollection_1_get_Count_m50848_MethodInfo;
static PropertyInfo ICollection_1_t9075____Count_PropertyInfo = 
{
	&ICollection_1_t9075_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50848_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50849_MethodInfo;
static PropertyInfo ICollection_1_t9075____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9075_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50849_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9075_PropertyInfos[] =
{
	&ICollection_1_t9075____Count_PropertyInfo,
	&ICollection_1_t9075____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50848_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Count()
MethodInfo ICollection_1_get_Count_m50848_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50848_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50849_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50849_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50849_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1632_0_0_0;
extern Il2CppType CertTypes_t1632_0_0_0;
static ParameterInfo ICollection_1_t9075_ICollection_1_Add_m50850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1632_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50850_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Add(T)
MethodInfo ICollection_1_Add_m50850_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9075_ICollection_1_Add_m50850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50850_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50851_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Clear()
MethodInfo ICollection_1_Clear_m50851_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50851_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1632_0_0_0;
static ParameterInfo ICollection_1_t9075_ICollection_1_Contains_m50852_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1632_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50852_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Contains(T)
MethodInfo ICollection_1_Contains_m50852_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9075_ICollection_1_Contains_m50852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50852_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypesU5BU5D_t5918_0_0_0;
extern Il2CppType CertTypesU5BU5D_t5918_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9075_ICollection_1_CopyTo_m50853_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CertTypesU5BU5D_t5918_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50853_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50853_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9075_ICollection_1_CopyTo_m50853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50853_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1632_0_0_0;
static ParameterInfo ICollection_1_t9075_ICollection_1_Remove_m50854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1632_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50854_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Remove(T)
MethodInfo ICollection_1_Remove_m50854_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9075_ICollection_1_Remove_m50854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50854_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9075_MethodInfos[] =
{
	&ICollection_1_get_Count_m50848_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50849_MethodInfo,
	&ICollection_1_Add_m50850_MethodInfo,
	&ICollection_1_Clear_m50851_MethodInfo,
	&ICollection_1_Contains_m50852_MethodInfo,
	&ICollection_1_CopyTo_m50853_MethodInfo,
	&ICollection_1_Remove_m50854_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9077_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9075_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9077_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9075_0_0_0;
extern Il2CppType ICollection_1_t9075_1_0_0;
struct ICollection_1_t9075;
extern Il2CppGenericClass ICollection_1_t9075_GenericClass;
TypeInfo ICollection_1_t9075_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9075_MethodInfos/* methods */
	, ICollection_1_t9075_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9075_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9075_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9075_0_0_0/* byval_arg */
	, &ICollection_1_t9075_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9075_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern Il2CppType IEnumerator_1_t7118_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50855_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50855_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9077_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7118_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50855_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9077_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50855_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9077_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9077_0_0_0;
extern Il2CppType IEnumerable_1_t9077_1_0_0;
struct IEnumerable_1_t9077;
extern Il2CppGenericClass IEnumerable_1_t9077_GenericClass;
TypeInfo IEnumerable_1_t9077_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9077_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9077_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9077_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9077_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9077_0_0_0/* byval_arg */
	, &IEnumerable_1_t9077_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9077_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9076_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo IList_1_get_Item_m50856_MethodInfo;
extern MethodInfo IList_1_set_Item_m50857_MethodInfo;
static PropertyInfo IList_1_t9076____Item_PropertyInfo = 
{
	&IList_1_t9076_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50856_MethodInfo/* get */
	, &IList_1_set_Item_m50857_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9076_PropertyInfos[] =
{
	&IList_1_t9076____Item_PropertyInfo,
	NULL
};
extern Il2CppType CertTypes_t1632_0_0_0;
static ParameterInfo IList_1_t9076_IList_1_IndexOf_m50858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1632_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50858_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50858_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9076_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9076_IList_1_IndexOf_m50858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50858_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CertTypes_t1632_0_0_0;
static ParameterInfo IList_1_t9076_IList_1_Insert_m50859_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1632_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50859_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50859_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9076_IList_1_Insert_m50859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50859_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9076_IList_1_RemoveAt_m50860_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50860_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50860_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9076_IList_1_RemoveAt_m50860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50860_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9076_IList_1_get_Item_m50856_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CertTypes_t1632_0_0_0;
extern void* RuntimeInvoker_CertTypes_t1632_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50856_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50856_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9076_il2cpp_TypeInfo/* declaring_type */
	, &CertTypes_t1632_0_0_0/* return_type */
	, RuntimeInvoker_CertTypes_t1632_Int32_t123/* invoker_method */
	, IList_1_t9076_IList_1_get_Item_m50856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50856_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CertTypes_t1632_0_0_0;
static ParameterInfo IList_1_t9076_IList_1_set_Item_m50857_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1632_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50857_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50857_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9076_IList_1_set_Item_m50857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50857_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9076_MethodInfos[] =
{
	&IList_1_IndexOf_m50858_MethodInfo,
	&IList_1_Insert_m50859_MethodInfo,
	&IList_1_RemoveAt_m50860_MethodInfo,
	&IList_1_get_Item_m50856_MethodInfo,
	&IList_1_set_Item_m50857_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9076_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9075_il2cpp_TypeInfo,
	&IEnumerable_1_t9077_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9076_0_0_0;
extern Il2CppType IList_1_t9076_1_0_0;
struct IList_1_t9076;
extern Il2CppGenericClass IList_1_t9076_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9076_MethodInfos/* methods */
	, IList_1_t9076_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9076_il2cpp_TypeInfo/* element_class */
	, IList_1_t9076_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9076_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9076_0_0_0/* byval_arg */
	, &IList_1_t9076_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9076_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7120_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo IEnumerator_1_get_Current_m50861_MethodInfo;
static PropertyInfo IEnumerator_1_t7120____Current_PropertyInfo = 
{
	&IEnumerator_1_t7120_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50861_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7120_PropertyInfos[] =
{
	&IEnumerator_1_t7120____Current_PropertyInfo,
	NULL
};
extern Il2CppType AlertLevel_t1638_0_0_0;
extern void* RuntimeInvoker_AlertLevel_t1638 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50861_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50861_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7120_il2cpp_TypeInfo/* declaring_type */
	, &AlertLevel_t1638_0_0_0/* return_type */
	, RuntimeInvoker_AlertLevel_t1638/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50861_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7120_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50861_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7120_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7120_0_0_0;
extern Il2CppType IEnumerator_1_t7120_1_0_0;
struct IEnumerator_1_t7120;
extern Il2CppGenericClass IEnumerator_1_t7120_GenericClass;
TypeInfo IEnumerator_1_t7120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7120_MethodInfos/* methods */
	, IEnumerator_1_t7120_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7120_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7120_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7120_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7120_0_0_0/* byval_arg */
	, &IEnumerator_1_t7120_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7120_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_572.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5111_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_572MethodDeclarations.h"

extern TypeInfo AlertLevel_t1638_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30933_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAlertLevel_t1638_m39984_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertLevel>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertLevel>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisAlertLevel_t1638_m39984 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30929_MethodInfo;
 void InternalEnumerator_1__ctor_m30929 (InternalEnumerator_1_t5111 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930 (InternalEnumerator_1_t5111 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30933(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30933_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AlertLevel_t1638_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30931_MethodInfo;
 void InternalEnumerator_1_Dispose_m30931 (InternalEnumerator_1_t5111 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30932_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30932 (InternalEnumerator_1_t5111 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30933 (InternalEnumerator_1_t5111 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisAlertLevel_t1638_m39984(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAlertLevel_t1638_m39984_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5111____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5111, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5111____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5111, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5111_FieldInfos[] =
{
	&InternalEnumerator_1_t5111____array_0_FieldInfo,
	&InternalEnumerator_1_t5111____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5111____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5111_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5111____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5111_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30933_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5111_PropertyInfos[] =
{
	&InternalEnumerator_1_t5111____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5111____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5111_InternalEnumerator_1__ctor_m30929_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30929_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30929_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30929/* method */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5111_InternalEnumerator_1__ctor_m30929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30929_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930/* method */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30931_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30931_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30931/* method */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30931_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30932_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30932_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30932/* method */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30932_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1638_0_0_0;
extern void* RuntimeInvoker_AlertLevel_t1638 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30933_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30933_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30933/* method */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* declaring_type */
	, &AlertLevel_t1638_0_0_0/* return_type */
	, RuntimeInvoker_AlertLevel_t1638/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30933_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5111_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30929_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_MethodInfo,
	&InternalEnumerator_1_Dispose_m30931_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30932_MethodInfo,
	&InternalEnumerator_1_get_Current_m30933_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5111_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30930_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30932_MethodInfo,
	&InternalEnumerator_1_Dispose_m30931_MethodInfo,
	&InternalEnumerator_1_get_Current_m30933_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5111_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7120_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5111_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7120_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5111_0_0_0;
extern Il2CppType InternalEnumerator_1_t5111_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5111_GenericClass;
TypeInfo InternalEnumerator_1_t5111_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5111_MethodInfos/* methods */
	, InternalEnumerator_1_t5111_PropertyInfos/* properties */
	, InternalEnumerator_1_t5111_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5111_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5111_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5111_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5111_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5111_1_0_0/* this_arg */
	, InternalEnumerator_1_t5111_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5111_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5111)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9078_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo ICollection_1_get_Count_m50862_MethodInfo;
static PropertyInfo ICollection_1_t9078____Count_PropertyInfo = 
{
	&ICollection_1_t9078_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50862_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50863_MethodInfo;
static PropertyInfo ICollection_1_t9078____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9078_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50863_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9078_PropertyInfos[] =
{
	&ICollection_1_t9078____Count_PropertyInfo,
	&ICollection_1_t9078____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50862_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Count()
MethodInfo ICollection_1_get_Count_m50862_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50862_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50863_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50863_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50863_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1638_0_0_0;
extern Il2CppType AlertLevel_t1638_0_0_0;
static ParameterInfo ICollection_1_t9078_ICollection_1_Add_m50864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1638_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50864_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Add(T)
MethodInfo ICollection_1_Add_m50864_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9078_ICollection_1_Add_m50864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50864_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50865_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Clear()
MethodInfo ICollection_1_Clear_m50865_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50865_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1638_0_0_0;
static ParameterInfo ICollection_1_t9078_ICollection_1_Contains_m50866_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1638_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50866_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Contains(T)
MethodInfo ICollection_1_Contains_m50866_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9078_ICollection_1_Contains_m50866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50866_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevelU5BU5D_t5919_0_0_0;
extern Il2CppType AlertLevelU5BU5D_t5919_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9078_ICollection_1_CopyTo_m50867_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevelU5BU5D_t5919_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50867_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50867_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9078_ICollection_1_CopyTo_m50867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50867_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1638_0_0_0;
static ParameterInfo ICollection_1_t9078_ICollection_1_Remove_m50868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1638_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50868_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Remove(T)
MethodInfo ICollection_1_Remove_m50868_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9078_ICollection_1_Remove_m50868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50868_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9078_MethodInfos[] =
{
	&ICollection_1_get_Count_m50862_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50863_MethodInfo,
	&ICollection_1_Add_m50864_MethodInfo,
	&ICollection_1_Clear_m50865_MethodInfo,
	&ICollection_1_Contains_m50866_MethodInfo,
	&ICollection_1_CopyTo_m50867_MethodInfo,
	&ICollection_1_Remove_m50868_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9080_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9078_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9080_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9078_0_0_0;
extern Il2CppType ICollection_1_t9078_1_0_0;
struct ICollection_1_t9078;
extern Il2CppGenericClass ICollection_1_t9078_GenericClass;
TypeInfo ICollection_1_t9078_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9078_MethodInfos/* methods */
	, ICollection_1_t9078_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9078_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9078_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9078_0_0_0/* byval_arg */
	, &ICollection_1_t9078_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9078_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>
extern Il2CppType IEnumerator_1_t7120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50869_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50869_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9080_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50869_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9080_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50869_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9080_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9080_0_0_0;
extern Il2CppType IEnumerable_1_t9080_1_0_0;
struct IEnumerable_1_t9080;
extern Il2CppGenericClass IEnumerable_1_t9080_GenericClass;
TypeInfo IEnumerable_1_t9080_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9080_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9080_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9080_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9080_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9080_0_0_0/* byval_arg */
	, &IEnumerable_1_t9080_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9080_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9079_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo IList_1_get_Item_m50870_MethodInfo;
extern MethodInfo IList_1_set_Item_m50871_MethodInfo;
static PropertyInfo IList_1_t9079____Item_PropertyInfo = 
{
	&IList_1_t9079_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50870_MethodInfo/* get */
	, &IList_1_set_Item_m50871_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9079_PropertyInfos[] =
{
	&IList_1_t9079____Item_PropertyInfo,
	NULL
};
extern Il2CppType AlertLevel_t1638_0_0_0;
static ParameterInfo IList_1_t9079_IList_1_IndexOf_m50872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1638_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50872_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50872_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9079_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9079_IList_1_IndexOf_m50872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50872_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AlertLevel_t1638_0_0_0;
static ParameterInfo IList_1_t9079_IList_1_Insert_m50873_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1638_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50873_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50873_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9079_IList_1_Insert_m50873_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50873_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9079_IList_1_RemoveAt_m50874_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50874_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50874_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9079_IList_1_RemoveAt_m50874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50874_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9079_IList_1_get_Item_m50870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AlertLevel_t1638_0_0_0;
extern void* RuntimeInvoker_AlertLevel_t1638_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50870_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50870_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9079_il2cpp_TypeInfo/* declaring_type */
	, &AlertLevel_t1638_0_0_0/* return_type */
	, RuntimeInvoker_AlertLevel_t1638_Int32_t123/* invoker_method */
	, IList_1_t9079_IList_1_get_Item_m50870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50870_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AlertLevel_t1638_0_0_0;
static ParameterInfo IList_1_t9079_IList_1_set_Item_m50871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1638_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50871_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50871_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9079_IList_1_set_Item_m50871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50871_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9079_MethodInfos[] =
{
	&IList_1_IndexOf_m50872_MethodInfo,
	&IList_1_Insert_m50873_MethodInfo,
	&IList_1_RemoveAt_m50874_MethodInfo,
	&IList_1_get_Item_m50870_MethodInfo,
	&IList_1_set_Item_m50871_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9079_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9078_il2cpp_TypeInfo,
	&IEnumerable_1_t9080_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9079_0_0_0;
extern Il2CppType IList_1_t9079_1_0_0;
struct IList_1_t9079;
extern Il2CppGenericClass IList_1_t9079_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9079_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9079_MethodInfos/* methods */
	, IList_1_t9079_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9079_il2cpp_TypeInfo/* element_class */
	, IList_1_t9079_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9079_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9079_0_0_0/* byval_arg */
	, &IList_1_t9079_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9079_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7122_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo IEnumerator_1_get_Current_m50875_MethodInfo;
static PropertyInfo IEnumerator_1_t7122____Current_PropertyInfo = 
{
	&IEnumerator_1_t7122_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50875_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7122_PropertyInfos[] =
{
	&IEnumerator_1_t7122____Current_PropertyInfo,
	NULL
};
extern Il2CppType AlertDescription_t1639_0_0_0;
extern void* RuntimeInvoker_AlertDescription_t1639 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50875_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50875_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7122_il2cpp_TypeInfo/* declaring_type */
	, &AlertDescription_t1639_0_0_0/* return_type */
	, RuntimeInvoker_AlertDescription_t1639/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50875_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7122_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50875_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7122_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7122_0_0_0;
extern Il2CppType IEnumerator_1_t7122_1_0_0;
struct IEnumerator_1_t7122;
extern Il2CppGenericClass IEnumerator_1_t7122_GenericClass;
TypeInfo IEnumerator_1_t7122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7122_MethodInfos/* methods */
	, IEnumerator_1_t7122_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7122_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7122_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7122_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7122_0_0_0/* byval_arg */
	, &IEnumerator_1_t7122_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7122_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_573.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5112_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_573MethodDeclarations.h"

extern TypeInfo AlertDescription_t1639_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30938_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAlertDescription_t1639_m39995_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertDescription>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertDescription>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisAlertDescription_t1639_m39995 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30934_MethodInfo;
 void InternalEnumerator_1__ctor_m30934 (InternalEnumerator_1_t5112 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935 (InternalEnumerator_1_t5112 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30938(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30938_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AlertDescription_t1639_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30936_MethodInfo;
 void InternalEnumerator_1_Dispose_m30936 (InternalEnumerator_1_t5112 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30937_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30937 (InternalEnumerator_1_t5112 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30938 (InternalEnumerator_1_t5112 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisAlertDescription_t1639_m39995(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAlertDescription_t1639_m39995_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5112____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5112, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5112____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5112, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5112_FieldInfos[] =
{
	&InternalEnumerator_1_t5112____array_0_FieldInfo,
	&InternalEnumerator_1_t5112____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5112____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5112_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5112____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5112_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30938_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5112_PropertyInfos[] =
{
	&InternalEnumerator_1_t5112____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5112____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5112_InternalEnumerator_1__ctor_m30934_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30934_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30934_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30934/* method */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5112_InternalEnumerator_1__ctor_m30934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30934_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935/* method */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30936_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30936_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30936/* method */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30936_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30937_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30937_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30937/* method */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30937_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1639_0_0_0;
extern void* RuntimeInvoker_AlertDescription_t1639 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30938_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30938_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30938/* method */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* declaring_type */
	, &AlertDescription_t1639_0_0_0/* return_type */
	, RuntimeInvoker_AlertDescription_t1639/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30938_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5112_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30934_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_MethodInfo,
	&InternalEnumerator_1_Dispose_m30936_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30937_MethodInfo,
	&InternalEnumerator_1_get_Current_m30938_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5112_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30937_MethodInfo,
	&InternalEnumerator_1_Dispose_m30936_MethodInfo,
	&InternalEnumerator_1_get_Current_m30938_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5112_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7122_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5112_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7122_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5112_0_0_0;
extern Il2CppType InternalEnumerator_1_t5112_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5112_GenericClass;
TypeInfo InternalEnumerator_1_t5112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5112_MethodInfos/* methods */
	, InternalEnumerator_1_t5112_PropertyInfos/* properties */
	, InternalEnumerator_1_t5112_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5112_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5112_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5112_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5112_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5112_1_0_0/* this_arg */
	, InternalEnumerator_1_t5112_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5112_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5112)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9081_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo ICollection_1_get_Count_m50876_MethodInfo;
static PropertyInfo ICollection_1_t9081____Count_PropertyInfo = 
{
	&ICollection_1_t9081_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50876_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50877_MethodInfo;
static PropertyInfo ICollection_1_t9081____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9081_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50877_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9081_PropertyInfos[] =
{
	&ICollection_1_t9081____Count_PropertyInfo,
	&ICollection_1_t9081____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50876_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Count()
MethodInfo ICollection_1_get_Count_m50876_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50876_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50877_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50877_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50877_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1639_0_0_0;
extern Il2CppType AlertDescription_t1639_0_0_0;
static ParameterInfo ICollection_1_t9081_ICollection_1_Add_m50878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1639_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50878_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Add(T)
MethodInfo ICollection_1_Add_m50878_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9081_ICollection_1_Add_m50878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50878_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50879_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Clear()
MethodInfo ICollection_1_Clear_m50879_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50879_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1639_0_0_0;
static ParameterInfo ICollection_1_t9081_ICollection_1_Contains_m50880_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1639_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50880_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Contains(T)
MethodInfo ICollection_1_Contains_m50880_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9081_ICollection_1_Contains_m50880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50880_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescriptionU5BU5D_t5920_0_0_0;
extern Il2CppType AlertDescriptionU5BU5D_t5920_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9081_ICollection_1_CopyTo_m50881_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescriptionU5BU5D_t5920_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50881_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50881_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9081_ICollection_1_CopyTo_m50881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50881_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1639_0_0_0;
static ParameterInfo ICollection_1_t9081_ICollection_1_Remove_m50882_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1639_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50882_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Remove(T)
MethodInfo ICollection_1_Remove_m50882_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9081_ICollection_1_Remove_m50882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50882_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9081_MethodInfos[] =
{
	&ICollection_1_get_Count_m50876_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50877_MethodInfo,
	&ICollection_1_Add_m50878_MethodInfo,
	&ICollection_1_Clear_m50879_MethodInfo,
	&ICollection_1_Contains_m50880_MethodInfo,
	&ICollection_1_CopyTo_m50881_MethodInfo,
	&ICollection_1_Remove_m50882_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9083_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9081_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9083_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9081_0_0_0;
extern Il2CppType ICollection_1_t9081_1_0_0;
struct ICollection_1_t9081;
extern Il2CppGenericClass ICollection_1_t9081_GenericClass;
TypeInfo ICollection_1_t9081_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9081_MethodInfos/* methods */
	, ICollection_1_t9081_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9081_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9081_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9081_0_0_0/* byval_arg */
	, &ICollection_1_t9081_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9081_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>
extern Il2CppType IEnumerator_1_t7122_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50883_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50883_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9083_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50883_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9083_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50883_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9083_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9083_0_0_0;
extern Il2CppType IEnumerable_1_t9083_1_0_0;
struct IEnumerable_1_t9083;
extern Il2CppGenericClass IEnumerable_1_t9083_GenericClass;
TypeInfo IEnumerable_1_t9083_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9083_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9083_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9083_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9083_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9083_0_0_0/* byval_arg */
	, &IEnumerable_1_t9083_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9083_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9082_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo IList_1_get_Item_m50884_MethodInfo;
extern MethodInfo IList_1_set_Item_m50885_MethodInfo;
static PropertyInfo IList_1_t9082____Item_PropertyInfo = 
{
	&IList_1_t9082_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50884_MethodInfo/* get */
	, &IList_1_set_Item_m50885_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9082_PropertyInfos[] =
{
	&IList_1_t9082____Item_PropertyInfo,
	NULL
};
extern Il2CppType AlertDescription_t1639_0_0_0;
static ParameterInfo IList_1_t9082_IList_1_IndexOf_m50886_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1639_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50886_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50886_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9082_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9082_IList_1_IndexOf_m50886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50886_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AlertDescription_t1639_0_0_0;
static ParameterInfo IList_1_t9082_IList_1_Insert_m50887_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1639_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50887_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50887_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9082_IList_1_Insert_m50887_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50887_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9082_IList_1_RemoveAt_m50888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50888_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50888_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9082_IList_1_RemoveAt_m50888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50888_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9082_IList_1_get_Item_m50884_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AlertDescription_t1639_0_0_0;
extern void* RuntimeInvoker_AlertDescription_t1639_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50884_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50884_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9082_il2cpp_TypeInfo/* declaring_type */
	, &AlertDescription_t1639_0_0_0/* return_type */
	, RuntimeInvoker_AlertDescription_t1639_Int32_t123/* invoker_method */
	, IList_1_t9082_IList_1_get_Item_m50884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50884_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AlertDescription_t1639_0_0_0;
static ParameterInfo IList_1_t9082_IList_1_set_Item_m50885_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1639_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50885_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50885_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9082_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9082_IList_1_set_Item_m50885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50885_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9082_MethodInfos[] =
{
	&IList_1_IndexOf_m50886_MethodInfo,
	&IList_1_Insert_m50887_MethodInfo,
	&IList_1_RemoveAt_m50888_MethodInfo,
	&IList_1_get_Item_m50884_MethodInfo,
	&IList_1_set_Item_m50885_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9082_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9081_il2cpp_TypeInfo,
	&IEnumerable_1_t9083_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9082_0_0_0;
extern Il2CppType IList_1_t9082_1_0_0;
struct IList_1_t9082;
extern Il2CppGenericClass IList_1_t9082_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9082_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9082_MethodInfos/* methods */
	, IList_1_t9082_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9082_il2cpp_TypeInfo/* element_class */
	, IList_1_t9082_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9082_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9082_0_0_0/* byval_arg */
	, &IList_1_t9082_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9082_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7124_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo IEnumerator_1_get_Current_m50889_MethodInfo;
static PropertyInfo IEnumerator_1_t7124____Current_PropertyInfo = 
{
	&IEnumerator_1_t7124_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50889_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7124_PropertyInfos[] =
{
	&IEnumerator_1_t7124____Current_PropertyInfo,
	NULL
};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
extern void* RuntimeInvoker_CipherAlgorithmType_t1641 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50889_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50889_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7124_il2cpp_TypeInfo/* declaring_type */
	, &CipherAlgorithmType_t1641_0_0_0/* return_type */
	, RuntimeInvoker_CipherAlgorithmType_t1641/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50889_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7124_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50889_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7124_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7124_0_0_0;
extern Il2CppType IEnumerator_1_t7124_1_0_0;
struct IEnumerator_1_t7124;
extern Il2CppGenericClass IEnumerator_1_t7124_GenericClass;
TypeInfo IEnumerator_1_t7124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7124_MethodInfos/* methods */
	, IEnumerator_1_t7124_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7124_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7124_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7124_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7124_0_0_0/* byval_arg */
	, &IEnumerator_1_t7124_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7124_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_574.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5113_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_574MethodDeclarations.h"

extern TypeInfo CipherAlgorithmType_t1641_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30943_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCipherAlgorithmType_t1641_m40006_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.CipherAlgorithmType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.CipherAlgorithmType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCipherAlgorithmType_t1641_m40006 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30939_MethodInfo;
 void InternalEnumerator_1__ctor_m30939 (InternalEnumerator_1_t5113 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940 (InternalEnumerator_1_t5113 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30943(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30943_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CipherAlgorithmType_t1641_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30941_MethodInfo;
 void InternalEnumerator_1_Dispose_m30941 (InternalEnumerator_1_t5113 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30942_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30942 (InternalEnumerator_1_t5113 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30943 (InternalEnumerator_1_t5113 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCipherAlgorithmType_t1641_m40006(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCipherAlgorithmType_t1641_m40006_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5113____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5113, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5113____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5113, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5113_FieldInfos[] =
{
	&InternalEnumerator_1_t5113____array_0_FieldInfo,
	&InternalEnumerator_1_t5113____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5113____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5113_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5113____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5113_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30943_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5113_PropertyInfos[] =
{
	&InternalEnumerator_1_t5113____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5113____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5113_InternalEnumerator_1__ctor_m30939_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30939_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30939_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30939/* method */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5113_InternalEnumerator_1__ctor_m30939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30939_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940/* method */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30941_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30941_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30941/* method */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30941_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30942_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30942_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30942/* method */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30942_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
extern void* RuntimeInvoker_CipherAlgorithmType_t1641 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30943_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30943_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30943/* method */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* declaring_type */
	, &CipherAlgorithmType_t1641_0_0_0/* return_type */
	, RuntimeInvoker_CipherAlgorithmType_t1641/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30943_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5113_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30939_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_MethodInfo,
	&InternalEnumerator_1_Dispose_m30941_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30942_MethodInfo,
	&InternalEnumerator_1_get_Current_m30943_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5113_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30940_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30942_MethodInfo,
	&InternalEnumerator_1_Dispose_m30941_MethodInfo,
	&InternalEnumerator_1_get_Current_m30943_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5113_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7124_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5113_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7124_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5113_0_0_0;
extern Il2CppType InternalEnumerator_1_t5113_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5113_GenericClass;
TypeInfo InternalEnumerator_1_t5113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5113_MethodInfos/* methods */
	, InternalEnumerator_1_t5113_PropertyInfos/* properties */
	, InternalEnumerator_1_t5113_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5113_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5113_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5113_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5113_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5113_1_0_0/* this_arg */
	, InternalEnumerator_1_t5113_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5113_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5113)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9084_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo ICollection_1_get_Count_m50890_MethodInfo;
static PropertyInfo ICollection_1_t9084____Count_PropertyInfo = 
{
	&ICollection_1_t9084_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50890_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50891_MethodInfo;
static PropertyInfo ICollection_1_t9084____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9084_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50891_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9084_PropertyInfos[] =
{
	&ICollection_1_t9084____Count_PropertyInfo,
	&ICollection_1_t9084____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50890_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Count()
MethodInfo ICollection_1_get_Count_m50890_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50890_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50891_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50891_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50891_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
static ParameterInfo ICollection_1_t9084_ICollection_1_Add_m50892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1641_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50892_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Add(T)
MethodInfo ICollection_1_Add_m50892_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9084_ICollection_1_Add_m50892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50892_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50893_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Clear()
MethodInfo ICollection_1_Clear_m50893_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50893_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
static ParameterInfo ICollection_1_t9084_ICollection_1_Contains_m50894_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1641_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50894_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Contains(T)
MethodInfo ICollection_1_Contains_m50894_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9084_ICollection_1_Contains_m50894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50894_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmTypeU5BU5D_t5921_0_0_0;
extern Il2CppType CipherAlgorithmTypeU5BU5D_t5921_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9084_ICollection_1_CopyTo_m50895_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmTypeU5BU5D_t5921_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50895_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50895_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9084_ICollection_1_CopyTo_m50895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50895_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
static ParameterInfo ICollection_1_t9084_ICollection_1_Remove_m50896_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1641_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50896_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Remove(T)
MethodInfo ICollection_1_Remove_m50896_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9084_ICollection_1_Remove_m50896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50896_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9084_MethodInfos[] =
{
	&ICollection_1_get_Count_m50890_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50891_MethodInfo,
	&ICollection_1_Add_m50892_MethodInfo,
	&ICollection_1_Clear_m50893_MethodInfo,
	&ICollection_1_Contains_m50894_MethodInfo,
	&ICollection_1_CopyTo_m50895_MethodInfo,
	&ICollection_1_Remove_m50896_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9086_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9084_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9086_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9084_0_0_0;
extern Il2CppType ICollection_1_t9084_1_0_0;
struct ICollection_1_t9084;
extern Il2CppGenericClass ICollection_1_t9084_GenericClass;
TypeInfo ICollection_1_t9084_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9084_MethodInfos/* methods */
	, ICollection_1_t9084_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9084_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9084_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9084_0_0_0/* byval_arg */
	, &ICollection_1_t9084_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9084_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern Il2CppType IEnumerator_1_t7124_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50897_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50897_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9086_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50897_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9086_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50897_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9086_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9086_0_0_0;
extern Il2CppType IEnumerable_1_t9086_1_0_0;
struct IEnumerable_1_t9086;
extern Il2CppGenericClass IEnumerable_1_t9086_GenericClass;
TypeInfo IEnumerable_1_t9086_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9086_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9086_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9086_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9086_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9086_0_0_0/* byval_arg */
	, &IEnumerable_1_t9086_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9086_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9085_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo IList_1_get_Item_m50898_MethodInfo;
extern MethodInfo IList_1_set_Item_m50899_MethodInfo;
static PropertyInfo IList_1_t9085____Item_PropertyInfo = 
{
	&IList_1_t9085_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50898_MethodInfo/* get */
	, &IList_1_set_Item_m50899_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9085_PropertyInfos[] =
{
	&IList_1_t9085____Item_PropertyInfo,
	NULL
};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
static ParameterInfo IList_1_t9085_IList_1_IndexOf_m50900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1641_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50900_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50900_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9085_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9085_IList_1_IndexOf_m50900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50900_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
static ParameterInfo IList_1_t9085_IList_1_Insert_m50901_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1641_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50901_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50901_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9085_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9085_IList_1_Insert_m50901_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50901_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9085_IList_1_RemoveAt_m50902_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50902_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50902_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9085_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9085_IList_1_RemoveAt_m50902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50902_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9085_IList_1_get_Item_m50898_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
extern void* RuntimeInvoker_CipherAlgorithmType_t1641_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50898_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50898_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9085_il2cpp_TypeInfo/* declaring_type */
	, &CipherAlgorithmType_t1641_0_0_0/* return_type */
	, RuntimeInvoker_CipherAlgorithmType_t1641_Int32_t123/* invoker_method */
	, IList_1_t9085_IList_1_get_Item_m50898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50898_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CipherAlgorithmType_t1641_0_0_0;
static ParameterInfo IList_1_t9085_IList_1_set_Item_m50899_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1641_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50899_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50899_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9085_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9085_IList_1_set_Item_m50899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50899_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9085_MethodInfos[] =
{
	&IList_1_IndexOf_m50900_MethodInfo,
	&IList_1_Insert_m50901_MethodInfo,
	&IList_1_RemoveAt_m50902_MethodInfo,
	&IList_1_get_Item_m50898_MethodInfo,
	&IList_1_set_Item_m50899_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9085_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9084_il2cpp_TypeInfo,
	&IEnumerable_1_t9086_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9085_0_0_0;
extern Il2CppType IList_1_t9085_1_0_0;
struct IList_1_t9085;
extern Il2CppGenericClass IList_1_t9085_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9085_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9085_MethodInfos/* methods */
	, IList_1_t9085_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9085_il2cpp_TypeInfo/* element_class */
	, IList_1_t9085_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9085_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9085_0_0_0/* byval_arg */
	, &IList_1_t9085_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9085_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7125_il2cpp_TypeInfo;

// System.Byte
#include "mscorlib_System_Byte.h"


// T System.Collections.Generic.IEnumerator`1<System.Byte[]>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Byte[]>
extern MethodInfo IEnumerator_1_get_Current_m50903_MethodInfo;
static PropertyInfo IEnumerator_1_t7125____Current_PropertyInfo = 
{
	&IEnumerator_1_t7125_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50903_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7125_PropertyInfos[] =
{
	&IEnumerator_1_t7125____Current_PropertyInfo,
	NULL
};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50903_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Byte[]>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50903_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7125_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t653_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50903_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7125_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50903_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7125_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7125_0_0_0;
extern Il2CppType IEnumerator_1_t7125_1_0_0;
struct IEnumerator_1_t7125;
extern Il2CppGenericClass IEnumerator_1_t7125_GenericClass;
TypeInfo IEnumerator_1_t7125_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7125_MethodInfos/* methods */
	, IEnumerator_1_t7125_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7125_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7125_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7125_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7125_0_0_0/* byval_arg */
	, &IEnumerator_1_t7125_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7125_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Byte[]>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_575.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5114_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Byte[]>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_575MethodDeclarations.h"

extern TypeInfo ByteU5BU5D_t653_il2cpp_TypeInfo;
extern TypeInfo Byte_t838_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30948_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisByteU5BU5D_t653_m40017_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Byte[]>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Byte[]>(System.Int32)
#define Array_InternalArray__get_Item_TisByteU5BU5D_t653_m40017(__this, p0, method) (ByteU5BU5D_t653*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Byte[]>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Byte[]>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Byte[]>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Byte[]>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5114____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5114, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5114____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5114, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5114_FieldInfos[] =
{
	&InternalEnumerator_1_t5114____array_0_FieldInfo,
	&InternalEnumerator_1_t5114____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5114____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5114_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5114____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5114_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30948_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5114_PropertyInfos[] =
{
	&InternalEnumerator_1_t5114____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5114____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5114_InternalEnumerator_1__ctor_m30944_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30944_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30944_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5114_InternalEnumerator_1__ctor_m30944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30944_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Byte[]>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30946_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30946_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30946_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30947_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Byte[]>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30947_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30947_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30948_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Byte[]>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30948_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t653_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30948_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5114_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30944_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_MethodInfo,
	&InternalEnumerator_1_Dispose_m30946_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30947_MethodInfo,
	&InternalEnumerator_1_get_Current_m30948_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30947_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30946_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5114_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30945_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30947_MethodInfo,
	&InternalEnumerator_1_Dispose_m30946_MethodInfo,
	&InternalEnumerator_1_get_Current_m30948_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5114_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7125_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5114_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7125_il2cpp_TypeInfo, 7},
};
extern TypeInfo ByteU5BU5D_t653_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5114_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30948_MethodInfo/* Method Usage */,
	&ByteU5BU5D_t653_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisByteU5BU5D_t653_m40017_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5114_0_0_0;
extern Il2CppType InternalEnumerator_1_t5114_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5114_GenericClass;
TypeInfo InternalEnumerator_1_t5114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5114_MethodInfos/* methods */
	, InternalEnumerator_1_t5114_PropertyInfos/* properties */
	, InternalEnumerator_1_t5114_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5114_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5114_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5114_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5114_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5114_1_0_0/* this_arg */
	, InternalEnumerator_1_t5114_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5114_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5114_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5114)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9087_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Byte[]>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Byte[]>
extern MethodInfo ICollection_1_get_Count_m50904_MethodInfo;
static PropertyInfo ICollection_1_t9087____Count_PropertyInfo = 
{
	&ICollection_1_t9087_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50904_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50905_MethodInfo;
static PropertyInfo ICollection_1_t9087____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9087_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50905_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9087_PropertyInfos[] =
{
	&ICollection_1_t9087____Count_PropertyInfo,
	&ICollection_1_t9087____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50904_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Byte[]>::get_Count()
MethodInfo ICollection_1_get_Count_m50904_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50904_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50905_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50905_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50905_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
extern Il2CppType ByteU5BU5D_t653_0_0_0;
static ParameterInfo ICollection_1_t9087_ICollection_1_Add_m50906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t653_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50906_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Add(T)
MethodInfo ICollection_1_Add_m50906_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9087_ICollection_1_Add_m50906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50906_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50907_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Clear()
MethodInfo ICollection_1_Clear_m50907_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50907_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
static ParameterInfo ICollection_1_t9087_ICollection_1_Contains_m50908_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t653_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50908_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Contains(T)
MethodInfo ICollection_1_Contains_m50908_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9087_ICollection_1_Contains_m50908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50908_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5DU5BU5D_t1727_0_0_0;
extern Il2CppType ByteU5BU5DU5BU5D_t1727_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9087_ICollection_1_CopyTo_m50909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5DU5BU5D_t1727_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50909_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50909_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9087_ICollection_1_CopyTo_m50909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50909_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
static ParameterInfo ICollection_1_t9087_ICollection_1_Remove_m50910_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t653_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50910_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Remove(T)
MethodInfo ICollection_1_Remove_m50910_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9087_ICollection_1_Remove_m50910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50910_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9087_MethodInfos[] =
{
	&ICollection_1_get_Count_m50904_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50905_MethodInfo,
	&ICollection_1_Add_m50906_MethodInfo,
	&ICollection_1_Clear_m50907_MethodInfo,
	&ICollection_1_Contains_m50908_MethodInfo,
	&ICollection_1_CopyTo_m50909_MethodInfo,
	&ICollection_1_Remove_m50910_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9089_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9087_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9089_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9087_0_0_0;
extern Il2CppType ICollection_1_t9087_1_0_0;
struct ICollection_1_t9087;
extern Il2CppGenericClass ICollection_1_t9087_GenericClass;
TypeInfo ICollection_1_t9087_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9087_MethodInfos/* methods */
	, ICollection_1_t9087_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9087_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9087_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9087_0_0_0/* byval_arg */
	, &ICollection_1_t9087_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9087_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Byte[]>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Byte[]>
extern Il2CppType IEnumerator_1_t7125_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50911_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Byte[]>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50911_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9089_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50911_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9089_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50911_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9089_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9089_0_0_0;
extern Il2CppType IEnumerable_1_t9089_1_0_0;
struct IEnumerable_1_t9089;
extern Il2CppGenericClass IEnumerable_1_t9089_GenericClass;
TypeInfo IEnumerable_1_t9089_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9089_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9089_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9089_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9089_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9089_0_0_0/* byval_arg */
	, &IEnumerable_1_t9089_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9089_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9088_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Byte[]>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Byte[]>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Byte[]>
extern MethodInfo IList_1_get_Item_m50912_MethodInfo;
extern MethodInfo IList_1_set_Item_m50913_MethodInfo;
static PropertyInfo IList_1_t9088____Item_PropertyInfo = 
{
	&IList_1_t9088_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50912_MethodInfo/* get */
	, &IList_1_set_Item_m50913_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9088_PropertyInfos[] =
{
	&IList_1_t9088____Item_PropertyInfo,
	NULL
};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
static ParameterInfo IList_1_t9088_IList_1_IndexOf_m50914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t653_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50914_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Byte[]>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50914_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9088_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9088_IList_1_IndexOf_m50914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50914_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ByteU5BU5D_t653_0_0_0;
static ParameterInfo IList_1_t9088_IList_1_Insert_m50915_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t653_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50915_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50915_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9088_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9088_IList_1_Insert_m50915_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50915_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9088_IList_1_RemoveAt_m50916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50916_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50916_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9088_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9088_IList_1_RemoveAt_m50916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50916_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9088_IList_1_get_Item_m50912_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ByteU5BU5D_t653_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50912_GenericMethod;
// T System.Collections.Generic.IList`1<System.Byte[]>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50912_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9088_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t653_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9088_IList_1_get_Item_m50912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50912_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ByteU5BU5D_t653_0_0_0;
static ParameterInfo IList_1_t9088_IList_1_set_Item_m50913_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t653_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50913_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50913_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9088_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9088_IList_1_set_Item_m50913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50913_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9088_MethodInfos[] =
{
	&IList_1_IndexOf_m50914_MethodInfo,
	&IList_1_Insert_m50915_MethodInfo,
	&IList_1_RemoveAt_m50916_MethodInfo,
	&IList_1_get_Item_m50912_MethodInfo,
	&IList_1_set_Item_m50913_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9088_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9087_il2cpp_TypeInfo,
	&IEnumerable_1_t9089_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9088_0_0_0;
extern Il2CppType IList_1_t9088_1_0_0;
struct IList_1_t9088;
extern Il2CppGenericClass IList_1_t9088_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9088_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9088_MethodInfos/* methods */
	, IList_1_t9088_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9088_il2cpp_TypeInfo/* element_class */
	, IList_1_t9088_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9088_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9088_0_0_0/* byval_arg */
	, &IList_1_t9088_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9088_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7127_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo IEnumerator_1_get_Current_m50917_MethodInfo;
static PropertyInfo IEnumerator_1_t7127____Current_PropertyInfo = 
{
	&IEnumerator_1_t7127_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50917_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7127_PropertyInfos[] =
{
	&IEnumerator_1_t7127____Current_PropertyInfo,
	NULL
};
extern Il2CppType ContentType_t1657_0_0_0;
extern void* RuntimeInvoker_ContentType_t1657 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50917_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50917_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7127_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t1657_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t1657/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50917_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7127_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50917_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7127_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7127_0_0_0;
extern Il2CppType IEnumerator_1_t7127_1_0_0;
struct IEnumerator_1_t7127;
extern Il2CppGenericClass IEnumerator_1_t7127_GenericClass;
TypeInfo IEnumerator_1_t7127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7127_MethodInfos/* methods */
	, IEnumerator_1_t7127_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7127_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7127_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7127_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7127_0_0_0/* byval_arg */
	, &IEnumerator_1_t7127_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7127_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_576.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5115_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_576MethodDeclarations.h"

extern TypeInfo ContentType_t1657_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30953_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisContentType_t1657_m40028_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ContentType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ContentType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisContentType_t1657_m40028 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30949_MethodInfo;
 void InternalEnumerator_1__ctor_m30949 (InternalEnumerator_1_t5115 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950 (InternalEnumerator_1_t5115 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30953(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30953_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ContentType_t1657_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30951_MethodInfo;
 void InternalEnumerator_1_Dispose_m30951 (InternalEnumerator_1_t5115 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30952_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30952 (InternalEnumerator_1_t5115 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30953 (InternalEnumerator_1_t5115 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisContentType_t1657_m40028(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisContentType_t1657_m40028_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5115____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5115, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5115____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5115, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5115_FieldInfos[] =
{
	&InternalEnumerator_1_t5115____array_0_FieldInfo,
	&InternalEnumerator_1_t5115____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5115____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5115_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5115____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5115_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30953_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5115_PropertyInfos[] =
{
	&InternalEnumerator_1_t5115____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5115____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5115_InternalEnumerator_1__ctor_m30949_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30949_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30949_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30949/* method */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5115_InternalEnumerator_1__ctor_m30949_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30949_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950/* method */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30951_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30951_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30951/* method */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30951_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30952_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30952_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30952/* method */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30952_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1657_0_0_0;
extern void* RuntimeInvoker_ContentType_t1657 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30953_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30953_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30953/* method */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t1657_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t1657/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30953_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5115_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30949_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_MethodInfo,
	&InternalEnumerator_1_Dispose_m30951_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30952_MethodInfo,
	&InternalEnumerator_1_get_Current_m30953_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5115_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30950_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30952_MethodInfo,
	&InternalEnumerator_1_Dispose_m30951_MethodInfo,
	&InternalEnumerator_1_get_Current_m30953_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5115_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7127_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5115_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7127_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5115_0_0_0;
extern Il2CppType InternalEnumerator_1_t5115_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5115_GenericClass;
TypeInfo InternalEnumerator_1_t5115_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5115_MethodInfos/* methods */
	, InternalEnumerator_1_t5115_PropertyInfos/* properties */
	, InternalEnumerator_1_t5115_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5115_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5115_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5115_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5115_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5115_1_0_0/* this_arg */
	, InternalEnumerator_1_t5115_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5115_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5115)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9090_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo ICollection_1_get_Count_m50918_MethodInfo;
static PropertyInfo ICollection_1_t9090____Count_PropertyInfo = 
{
	&ICollection_1_t9090_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50918_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50919_MethodInfo;
static PropertyInfo ICollection_1_t9090____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9090_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50919_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9090_PropertyInfos[] =
{
	&ICollection_1_t9090____Count_PropertyInfo,
	&ICollection_1_t9090____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50918_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_Count()
MethodInfo ICollection_1_get_Count_m50918_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50918_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50919_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50919_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50919_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1657_0_0_0;
extern Il2CppType ContentType_t1657_0_0_0;
static ParameterInfo ICollection_1_t9090_ICollection_1_Add_m50920_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1657_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50920_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Add(T)
MethodInfo ICollection_1_Add_m50920_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9090_ICollection_1_Add_m50920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50920_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50921_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Clear()
MethodInfo ICollection_1_Clear_m50921_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50921_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1657_0_0_0;
static ParameterInfo ICollection_1_t9090_ICollection_1_Contains_m50922_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1657_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50922_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Contains(T)
MethodInfo ICollection_1_Contains_m50922_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9090_ICollection_1_Contains_m50922_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50922_GenericMethod/* genericMethod */

};
extern Il2CppType ContentTypeU5BU5D_t5922_0_0_0;
extern Il2CppType ContentTypeU5BU5D_t5922_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9090_ICollection_1_CopyTo_m50923_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ContentTypeU5BU5D_t5922_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50923_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50923_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9090_ICollection_1_CopyTo_m50923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50923_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1657_0_0_0;
static ParameterInfo ICollection_1_t9090_ICollection_1_Remove_m50924_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1657_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50924_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Remove(T)
MethodInfo ICollection_1_Remove_m50924_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9090_ICollection_1_Remove_m50924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50924_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9090_MethodInfos[] =
{
	&ICollection_1_get_Count_m50918_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50919_MethodInfo,
	&ICollection_1_Add_m50920_MethodInfo,
	&ICollection_1_Clear_m50921_MethodInfo,
	&ICollection_1_Contains_m50922_MethodInfo,
	&ICollection_1_CopyTo_m50923_MethodInfo,
	&ICollection_1_Remove_m50924_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9092_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9090_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9092_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9090_0_0_0;
extern Il2CppType ICollection_1_t9090_1_0_0;
struct ICollection_1_t9090;
extern Il2CppGenericClass ICollection_1_t9090_GenericClass;
TypeInfo ICollection_1_t9090_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9090_MethodInfos/* methods */
	, ICollection_1_t9090_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9090_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9090_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9090_0_0_0/* byval_arg */
	, &ICollection_1_t9090_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9090_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>
extern Il2CppType IEnumerator_1_t7127_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50925_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50925_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9092_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50925_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9092_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50925_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9092_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9092_0_0_0;
extern Il2CppType IEnumerable_1_t9092_1_0_0;
struct IEnumerable_1_t9092;
extern Il2CppGenericClass IEnumerable_1_t9092_GenericClass;
TypeInfo IEnumerable_1_t9092_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9092_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9092_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9092_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9092_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9092_0_0_0/* byval_arg */
	, &IEnumerable_1_t9092_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9092_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9091_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo IList_1_get_Item_m50926_MethodInfo;
extern MethodInfo IList_1_set_Item_m50927_MethodInfo;
static PropertyInfo IList_1_t9091____Item_PropertyInfo = 
{
	&IList_1_t9091_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50926_MethodInfo/* get */
	, &IList_1_set_Item_m50927_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9091_PropertyInfos[] =
{
	&IList_1_t9091____Item_PropertyInfo,
	NULL
};
extern Il2CppType ContentType_t1657_0_0_0;
static ParameterInfo IList_1_t9091_IList_1_IndexOf_m50928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1657_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50928_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50928_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9091_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9091_IList_1_IndexOf_m50928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50928_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ContentType_t1657_0_0_0;
static ParameterInfo IList_1_t9091_IList_1_Insert_m50929_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ContentType_t1657_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50929_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50929_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9091_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9091_IList_1_Insert_m50929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50929_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9091_IList_1_RemoveAt_m50930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50930_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50930_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9091_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9091_IList_1_RemoveAt_m50930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50930_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9091_IList_1_get_Item_m50926_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ContentType_t1657_0_0_0;
extern void* RuntimeInvoker_ContentType_t1657_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50926_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50926_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9091_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t1657_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t1657_Int32_t123/* invoker_method */
	, IList_1_t9091_IList_1_get_Item_m50926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50926_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ContentType_t1657_0_0_0;
static ParameterInfo IList_1_t9091_IList_1_set_Item_m50927_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ContentType_t1657_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50927_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50927_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9091_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9091_IList_1_set_Item_m50927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50927_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9091_MethodInfos[] =
{
	&IList_1_IndexOf_m50928_MethodInfo,
	&IList_1_Insert_m50929_MethodInfo,
	&IList_1_RemoveAt_m50930_MethodInfo,
	&IList_1_get_Item_m50926_MethodInfo,
	&IList_1_set_Item_m50927_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9091_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9090_il2cpp_TypeInfo,
	&IEnumerable_1_t9092_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9091_0_0_0;
extern Il2CppType IList_1_t9091_1_0_0;
struct IList_1_t9091;
extern Il2CppGenericClass IList_1_t9091_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9091_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9091_MethodInfos/* methods */
	, IList_1_t9091_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9091_il2cpp_TypeInfo/* element_class */
	, IList_1_t9091_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9091_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9091_0_0_0/* byval_arg */
	, &IList_1_t9091_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9091_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7129_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo IEnumerator_1_get_Current_m50931_MethodInfo;
static PropertyInfo IEnumerator_1_t7129____Current_PropertyInfo = 
{
	&IEnumerator_1_t7129_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50931_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7129_PropertyInfos[] =
{
	&IEnumerator_1_t7129____Current_PropertyInfo,
	NULL
};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
extern void* RuntimeInvoker_ExchangeAlgorithmType_t1661 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50931_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50931_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7129_il2cpp_TypeInfo/* declaring_type */
	, &ExchangeAlgorithmType_t1661_0_0_0/* return_type */
	, RuntimeInvoker_ExchangeAlgorithmType_t1661/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50931_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7129_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50931_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7129_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7129_0_0_0;
extern Il2CppType IEnumerator_1_t7129_1_0_0;
struct IEnumerator_1_t7129;
extern Il2CppGenericClass IEnumerator_1_t7129_GenericClass;
TypeInfo IEnumerator_1_t7129_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7129_MethodInfos/* methods */
	, IEnumerator_1_t7129_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7129_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7129_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7129_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7129_0_0_0/* byval_arg */
	, &IEnumerator_1_t7129_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7129_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_577.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5116_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_577MethodDeclarations.h"

extern TypeInfo ExchangeAlgorithmType_t1661_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30958_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1661_m40039_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1661_m40039 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30954_MethodInfo;
 void InternalEnumerator_1__ctor_m30954 (InternalEnumerator_1_t5116 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955 (InternalEnumerator_1_t5116 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30958(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30958_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ExchangeAlgorithmType_t1661_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30956_MethodInfo;
 void InternalEnumerator_1_Dispose_m30956 (InternalEnumerator_1_t5116 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30957_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30957 (InternalEnumerator_1_t5116 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30958 (InternalEnumerator_1_t5116 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1661_m40039(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1661_m40039_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5116____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5116, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5116____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5116, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5116_FieldInfos[] =
{
	&InternalEnumerator_1_t5116____array_0_FieldInfo,
	&InternalEnumerator_1_t5116____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5116____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5116_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5116____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5116_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30958_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5116_PropertyInfos[] =
{
	&InternalEnumerator_1_t5116____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5116____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5116_InternalEnumerator_1__ctor_m30954_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30954_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30954_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30954/* method */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5116_InternalEnumerator_1__ctor_m30954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30954_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955/* method */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30956_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30956_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30956/* method */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30956_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30957_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30957_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30957/* method */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30957_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
extern void* RuntimeInvoker_ExchangeAlgorithmType_t1661 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30958_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30958_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30958/* method */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* declaring_type */
	, &ExchangeAlgorithmType_t1661_0_0_0/* return_type */
	, RuntimeInvoker_ExchangeAlgorithmType_t1661/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30958_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5116_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30954_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_MethodInfo,
	&InternalEnumerator_1_Dispose_m30956_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30957_MethodInfo,
	&InternalEnumerator_1_get_Current_m30958_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5116_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30955_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30957_MethodInfo,
	&InternalEnumerator_1_Dispose_m30956_MethodInfo,
	&InternalEnumerator_1_get_Current_m30958_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5116_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7129_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5116_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7129_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5116_0_0_0;
extern Il2CppType InternalEnumerator_1_t5116_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5116_GenericClass;
TypeInfo InternalEnumerator_1_t5116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5116_MethodInfos/* methods */
	, InternalEnumerator_1_t5116_PropertyInfos/* properties */
	, InternalEnumerator_1_t5116_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5116_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5116_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5116_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5116_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5116_1_0_0/* this_arg */
	, InternalEnumerator_1_t5116_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5116_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5116)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9093_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo ICollection_1_get_Count_m50932_MethodInfo;
static PropertyInfo ICollection_1_t9093____Count_PropertyInfo = 
{
	&ICollection_1_t9093_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50933_MethodInfo;
static PropertyInfo ICollection_1_t9093____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9093_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50933_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9093_PropertyInfos[] =
{
	&ICollection_1_t9093____Count_PropertyInfo,
	&ICollection_1_t9093____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50932_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Count()
MethodInfo ICollection_1_get_Count_m50932_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50932_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50933_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50933_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50933_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
static ParameterInfo ICollection_1_t9093_ICollection_1_Add_m50934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1661_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50934_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Add(T)
MethodInfo ICollection_1_Add_m50934_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9093_ICollection_1_Add_m50934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50934_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50935_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Clear()
MethodInfo ICollection_1_Clear_m50935_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50935_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
static ParameterInfo ICollection_1_t9093_ICollection_1_Contains_m50936_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1661_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50936_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Contains(T)
MethodInfo ICollection_1_Contains_m50936_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9093_ICollection_1_Contains_m50936_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50936_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmTypeU5BU5D_t5923_0_0_0;
extern Il2CppType ExchangeAlgorithmTypeU5BU5D_t5923_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9093_ICollection_1_CopyTo_m50937_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmTypeU5BU5D_t5923_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50937_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50937_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9093_ICollection_1_CopyTo_m50937_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50937_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
static ParameterInfo ICollection_1_t9093_ICollection_1_Remove_m50938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1661_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50938_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Remove(T)
MethodInfo ICollection_1_Remove_m50938_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9093_ICollection_1_Remove_m50938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50938_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9093_MethodInfos[] =
{
	&ICollection_1_get_Count_m50932_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50933_MethodInfo,
	&ICollection_1_Add_m50934_MethodInfo,
	&ICollection_1_Clear_m50935_MethodInfo,
	&ICollection_1_Contains_m50936_MethodInfo,
	&ICollection_1_CopyTo_m50937_MethodInfo,
	&ICollection_1_Remove_m50938_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9095_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9093_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9095_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9093_0_0_0;
extern Il2CppType ICollection_1_t9093_1_0_0;
struct ICollection_1_t9093;
extern Il2CppGenericClass ICollection_1_t9093_GenericClass;
TypeInfo ICollection_1_t9093_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9093_MethodInfos/* methods */
	, ICollection_1_t9093_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9093_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9093_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9093_0_0_0/* byval_arg */
	, &ICollection_1_t9093_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9093_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern Il2CppType IEnumerator_1_t7129_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50939_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50939_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9095_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50939_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9095_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50939_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9095_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9095_0_0_0;
extern Il2CppType IEnumerable_1_t9095_1_0_0;
struct IEnumerable_1_t9095;
extern Il2CppGenericClass IEnumerable_1_t9095_GenericClass;
TypeInfo IEnumerable_1_t9095_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9095_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9095_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9095_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9095_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9095_0_0_0/* byval_arg */
	, &IEnumerable_1_t9095_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9095_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9094_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo IList_1_get_Item_m50940_MethodInfo;
extern MethodInfo IList_1_set_Item_m50941_MethodInfo;
static PropertyInfo IList_1_t9094____Item_PropertyInfo = 
{
	&IList_1_t9094_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50940_MethodInfo/* get */
	, &IList_1_set_Item_m50941_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9094_PropertyInfos[] =
{
	&IList_1_t9094____Item_PropertyInfo,
	NULL
};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
static ParameterInfo IList_1_t9094_IList_1_IndexOf_m50942_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1661_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50942_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50942_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9094_IList_1_IndexOf_m50942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50942_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
static ParameterInfo IList_1_t9094_IList_1_Insert_m50943_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1661_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50943_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50943_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9094_IList_1_Insert_m50943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50943_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9094_IList_1_RemoveAt_m50944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50944_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50944_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9094_IList_1_RemoveAt_m50944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50944_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9094_IList_1_get_Item_m50940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
extern void* RuntimeInvoker_ExchangeAlgorithmType_t1661_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50940_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50940_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9094_il2cpp_TypeInfo/* declaring_type */
	, &ExchangeAlgorithmType_t1661_0_0_0/* return_type */
	, RuntimeInvoker_ExchangeAlgorithmType_t1661_Int32_t123/* invoker_method */
	, IList_1_t9094_IList_1_get_Item_m50940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50940_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ExchangeAlgorithmType_t1661_0_0_0;
static ParameterInfo IList_1_t9094_IList_1_set_Item_m50941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1661_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50941_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50941_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9094_IList_1_set_Item_m50941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50941_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9094_MethodInfos[] =
{
	&IList_1_IndexOf_m50942_MethodInfo,
	&IList_1_Insert_m50943_MethodInfo,
	&IList_1_RemoveAt_m50944_MethodInfo,
	&IList_1_get_Item_m50940_MethodInfo,
	&IList_1_set_Item_m50941_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9094_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9093_il2cpp_TypeInfo,
	&IEnumerable_1_t9095_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9094_0_0_0;
extern Il2CppType IList_1_t9094_1_0_0;
struct IList_1_t9094;
extern Il2CppGenericClass IList_1_t9094_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9094_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9094_MethodInfos/* methods */
	, IList_1_t9094_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9094_il2cpp_TypeInfo/* element_class */
	, IList_1_t9094_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9094_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9094_0_0_0/* byval_arg */
	, &IList_1_t9094_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9094_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7131_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo IEnumerator_1_get_Current_m50945_MethodInfo;
static PropertyInfo IEnumerator_1_t7131____Current_PropertyInfo = 
{
	&IEnumerator_1_t7131_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50945_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7131_PropertyInfos[] =
{
	&IEnumerator_1_t7131____Current_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeState_t1662_0_0_0;
extern void* RuntimeInvoker_HandshakeState_t1662 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50945_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50945_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7131_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeState_t1662_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeState_t1662/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50945_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7131_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50945_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7131_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7131_0_0_0;
extern Il2CppType IEnumerator_1_t7131_1_0_0;
struct IEnumerator_1_t7131;
extern Il2CppGenericClass IEnumerator_1_t7131_GenericClass;
TypeInfo IEnumerator_1_t7131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7131_MethodInfos/* methods */
	, IEnumerator_1_t7131_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7131_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7131_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7131_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7131_0_0_0/* byval_arg */
	, &IEnumerator_1_t7131_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7131_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_578.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5117_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_578MethodDeclarations.h"

extern TypeInfo HandshakeState_t1662_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30963_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHandshakeState_t1662_m40050_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HandshakeState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HandshakeState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisHandshakeState_t1662_m40050 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30959_MethodInfo;
 void InternalEnumerator_1__ctor_m30959 (InternalEnumerator_1_t5117 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960 (InternalEnumerator_1_t5117 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30963(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30963_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HandshakeState_t1662_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30961_MethodInfo;
 void InternalEnumerator_1_Dispose_m30961 (InternalEnumerator_1_t5117 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30962_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30962 (InternalEnumerator_1_t5117 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30963 (InternalEnumerator_1_t5117 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisHandshakeState_t1662_m40050(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHandshakeState_t1662_m40050_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5117____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5117, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5117____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5117, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5117_FieldInfos[] =
{
	&InternalEnumerator_1_t5117____array_0_FieldInfo,
	&InternalEnumerator_1_t5117____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5117____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5117_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5117____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5117_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5117_PropertyInfos[] =
{
	&InternalEnumerator_1_t5117____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5117____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5117_InternalEnumerator_1__ctor_m30959_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30959_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30959_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30959/* method */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5117_InternalEnumerator_1__ctor_m30959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30959_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960/* method */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30961_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30961_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30961/* method */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30961_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30962_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30962_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30962/* method */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30962_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1662_0_0_0;
extern void* RuntimeInvoker_HandshakeState_t1662 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30963_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30963_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30963/* method */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeState_t1662_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeState_t1662/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30963_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5117_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30959_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_MethodInfo,
	&InternalEnumerator_1_Dispose_m30961_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30962_MethodInfo,
	&InternalEnumerator_1_get_Current_m30963_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5117_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30960_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30962_MethodInfo,
	&InternalEnumerator_1_Dispose_m30961_MethodInfo,
	&InternalEnumerator_1_get_Current_m30963_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5117_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7131_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5117_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7131_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5117_0_0_0;
extern Il2CppType InternalEnumerator_1_t5117_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5117_GenericClass;
TypeInfo InternalEnumerator_1_t5117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5117_MethodInfos/* methods */
	, InternalEnumerator_1_t5117_PropertyInfos/* properties */
	, InternalEnumerator_1_t5117_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5117_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5117_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5117_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5117_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5117_1_0_0/* this_arg */
	, InternalEnumerator_1_t5117_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5117_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5117)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9096_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo ICollection_1_get_Count_m50946_MethodInfo;
static PropertyInfo ICollection_1_t9096____Count_PropertyInfo = 
{
	&ICollection_1_t9096_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50946_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50947_MethodInfo;
static PropertyInfo ICollection_1_t9096____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9096_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9096_PropertyInfos[] =
{
	&ICollection_1_t9096____Count_PropertyInfo,
	&ICollection_1_t9096____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50946_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Count()
MethodInfo ICollection_1_get_Count_m50946_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50946_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50947_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50947_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50947_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1662_0_0_0;
extern Il2CppType HandshakeState_t1662_0_0_0;
static ParameterInfo ICollection_1_t9096_ICollection_1_Add_m50948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1662_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50948_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Add(T)
MethodInfo ICollection_1_Add_m50948_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9096_ICollection_1_Add_m50948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50948_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50949_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Clear()
MethodInfo ICollection_1_Clear_m50949_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50949_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1662_0_0_0;
static ParameterInfo ICollection_1_t9096_ICollection_1_Contains_m50950_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1662_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50950_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Contains(T)
MethodInfo ICollection_1_Contains_m50950_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9096_ICollection_1_Contains_m50950_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50950_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeStateU5BU5D_t5924_0_0_0;
extern Il2CppType HandshakeStateU5BU5D_t5924_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9096_ICollection_1_CopyTo_m50951_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeStateU5BU5D_t5924_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50951_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50951_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9096_ICollection_1_CopyTo_m50951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50951_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1662_0_0_0;
static ParameterInfo ICollection_1_t9096_ICollection_1_Remove_m50952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1662_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50952_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Remove(T)
MethodInfo ICollection_1_Remove_m50952_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9096_ICollection_1_Remove_m50952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50952_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9096_MethodInfos[] =
{
	&ICollection_1_get_Count_m50946_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50947_MethodInfo,
	&ICollection_1_Add_m50948_MethodInfo,
	&ICollection_1_Clear_m50949_MethodInfo,
	&ICollection_1_Contains_m50950_MethodInfo,
	&ICollection_1_CopyTo_m50951_MethodInfo,
	&ICollection_1_Remove_m50952_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9098_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9096_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9098_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9096_0_0_0;
extern Il2CppType ICollection_1_t9096_1_0_0;
struct ICollection_1_t9096;
extern Il2CppGenericClass ICollection_1_t9096_GenericClass;
TypeInfo ICollection_1_t9096_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9096_MethodInfos/* methods */
	, ICollection_1_t9096_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9096_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9096_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9096_0_0_0/* byval_arg */
	, &ICollection_1_t9096_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9096_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>
extern Il2CppType IEnumerator_1_t7131_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50953_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50953_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9098_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50953_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9098_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50953_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9098_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9098_0_0_0;
extern Il2CppType IEnumerable_1_t9098_1_0_0;
struct IEnumerable_1_t9098;
extern Il2CppGenericClass IEnumerable_1_t9098_GenericClass;
TypeInfo IEnumerable_1_t9098_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9098_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9098_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9098_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9098_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9098_0_0_0/* byval_arg */
	, &IEnumerable_1_t9098_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9098_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9097_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo IList_1_get_Item_m50954_MethodInfo;
extern MethodInfo IList_1_set_Item_m50955_MethodInfo;
static PropertyInfo IList_1_t9097____Item_PropertyInfo = 
{
	&IList_1_t9097_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50954_MethodInfo/* get */
	, &IList_1_set_Item_m50955_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9097_PropertyInfos[] =
{
	&IList_1_t9097____Item_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeState_t1662_0_0_0;
static ParameterInfo IList_1_t9097_IList_1_IndexOf_m50956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1662_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50956_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50956_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9097_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9097_IList_1_IndexOf_m50956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50956_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HandshakeState_t1662_0_0_0;
static ParameterInfo IList_1_t9097_IList_1_Insert_m50957_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1662_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50957_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50957_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9097_IList_1_Insert_m50957_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50957_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9097_IList_1_RemoveAt_m50958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50958_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50958_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9097_IList_1_RemoveAt_m50958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50958_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9097_IList_1_get_Item_m50954_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HandshakeState_t1662_0_0_0;
extern void* RuntimeInvoker_HandshakeState_t1662_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50954_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50954_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9097_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeState_t1662_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeState_t1662_Int32_t123/* invoker_method */
	, IList_1_t9097_IList_1_get_Item_m50954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50954_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HandshakeState_t1662_0_0_0;
static ParameterInfo IList_1_t9097_IList_1_set_Item_m50955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1662_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50955_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50955_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9097_IList_1_set_Item_m50955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50955_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9097_MethodInfos[] =
{
	&IList_1_IndexOf_m50956_MethodInfo,
	&IList_1_Insert_m50957_MethodInfo,
	&IList_1_RemoveAt_m50958_MethodInfo,
	&IList_1_get_Item_m50954_MethodInfo,
	&IList_1_set_Item_m50955_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9097_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9096_il2cpp_TypeInfo,
	&IEnumerable_1_t9098_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9097_0_0_0;
extern Il2CppType IList_1_t9097_1_0_0;
struct IList_1_t9097;
extern Il2CppGenericClass IList_1_t9097_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9097_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9097_MethodInfos/* methods */
	, IList_1_t9097_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9097_il2cpp_TypeInfo/* element_class */
	, IList_1_t9097_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9097_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9097_0_0_0/* byval_arg */
	, &IList_1_t9097_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9097_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7133_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo IEnumerator_1_get_Current_m50959_MethodInfo;
static PropertyInfo IEnumerator_1_t7133____Current_PropertyInfo = 
{
	&IEnumerator_1_t7133_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50959_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7133_PropertyInfos[] =
{
	&IEnumerator_1_t7133____Current_PropertyInfo,
	NULL
};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
extern void* RuntimeInvoker_HashAlgorithmType_t1663 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50959_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50959_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7133_il2cpp_TypeInfo/* declaring_type */
	, &HashAlgorithmType_t1663_0_0_0/* return_type */
	, RuntimeInvoker_HashAlgorithmType_t1663/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50959_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7133_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50959_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7133_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7133_0_0_0;
extern Il2CppType IEnumerator_1_t7133_1_0_0;
struct IEnumerator_1_t7133;
extern Il2CppGenericClass IEnumerator_1_t7133_GenericClass;
TypeInfo IEnumerator_1_t7133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7133_MethodInfos/* methods */
	, IEnumerator_1_t7133_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7133_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7133_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7133_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7133_0_0_0/* byval_arg */
	, &IEnumerator_1_t7133_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7133_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_579.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5118_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_579MethodDeclarations.h"

extern TypeInfo HashAlgorithmType_t1663_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30968_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHashAlgorithmType_t1663_m40061_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HashAlgorithmType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HashAlgorithmType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisHashAlgorithmType_t1663_m40061 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30964_MethodInfo;
 void InternalEnumerator_1__ctor_m30964 (InternalEnumerator_1_t5118 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965 (InternalEnumerator_1_t5118 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30968(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30968_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HashAlgorithmType_t1663_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30966_MethodInfo;
 void InternalEnumerator_1_Dispose_m30966 (InternalEnumerator_1_t5118 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30967_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30967 (InternalEnumerator_1_t5118 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30968 (InternalEnumerator_1_t5118 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisHashAlgorithmType_t1663_m40061(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHashAlgorithmType_t1663_m40061_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5118____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5118, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5118____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5118, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5118_FieldInfos[] =
{
	&InternalEnumerator_1_t5118____array_0_FieldInfo,
	&InternalEnumerator_1_t5118____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5118____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5118_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5118____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5118_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30968_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5118_PropertyInfos[] =
{
	&InternalEnumerator_1_t5118____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5118____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5118_InternalEnumerator_1__ctor_m30964_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30964_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30964_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30964/* method */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5118_InternalEnumerator_1__ctor_m30964_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30964_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965/* method */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30966_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30966_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30966/* method */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30966_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30967_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30967_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30967/* method */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30967_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
extern void* RuntimeInvoker_HashAlgorithmType_t1663 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30968_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30968_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30968/* method */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* declaring_type */
	, &HashAlgorithmType_t1663_0_0_0/* return_type */
	, RuntimeInvoker_HashAlgorithmType_t1663/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30968_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5118_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30964_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_MethodInfo,
	&InternalEnumerator_1_Dispose_m30966_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30967_MethodInfo,
	&InternalEnumerator_1_get_Current_m30968_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5118_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30965_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30967_MethodInfo,
	&InternalEnumerator_1_Dispose_m30966_MethodInfo,
	&InternalEnumerator_1_get_Current_m30968_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5118_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7133_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5118_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7133_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5118_0_0_0;
extern Il2CppType InternalEnumerator_1_t5118_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5118_GenericClass;
TypeInfo InternalEnumerator_1_t5118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5118_MethodInfos/* methods */
	, InternalEnumerator_1_t5118_PropertyInfos/* properties */
	, InternalEnumerator_1_t5118_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5118_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5118_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5118_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5118_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5118_1_0_0/* this_arg */
	, InternalEnumerator_1_t5118_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5118_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5118)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9099_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo ICollection_1_get_Count_m50960_MethodInfo;
static PropertyInfo ICollection_1_t9099____Count_PropertyInfo = 
{
	&ICollection_1_t9099_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50961_MethodInfo;
static PropertyInfo ICollection_1_t9099____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9099_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50961_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9099_PropertyInfos[] =
{
	&ICollection_1_t9099____Count_PropertyInfo,
	&ICollection_1_t9099____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50960_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Count()
MethodInfo ICollection_1_get_Count_m50960_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50960_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50961_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50961_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50961_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
static ParameterInfo ICollection_1_t9099_ICollection_1_Add_m50962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1663_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50962_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Add(T)
MethodInfo ICollection_1_Add_m50962_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9099_ICollection_1_Add_m50962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50962_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50963_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Clear()
MethodInfo ICollection_1_Clear_m50963_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50963_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
static ParameterInfo ICollection_1_t9099_ICollection_1_Contains_m50964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1663_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50964_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Contains(T)
MethodInfo ICollection_1_Contains_m50964_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9099_ICollection_1_Contains_m50964_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50964_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmTypeU5BU5D_t5925_0_0_0;
extern Il2CppType HashAlgorithmTypeU5BU5D_t5925_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9099_ICollection_1_CopyTo_m50965_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmTypeU5BU5D_t5925_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50965_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50965_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9099_ICollection_1_CopyTo_m50965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50965_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
static ParameterInfo ICollection_1_t9099_ICollection_1_Remove_m50966_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1663_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50966_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Remove(T)
MethodInfo ICollection_1_Remove_m50966_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9099_ICollection_1_Remove_m50966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50966_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9099_MethodInfos[] =
{
	&ICollection_1_get_Count_m50960_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50961_MethodInfo,
	&ICollection_1_Add_m50962_MethodInfo,
	&ICollection_1_Clear_m50963_MethodInfo,
	&ICollection_1_Contains_m50964_MethodInfo,
	&ICollection_1_CopyTo_m50965_MethodInfo,
	&ICollection_1_Remove_m50966_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9101_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9099_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9101_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9099_0_0_0;
extern Il2CppType ICollection_1_t9099_1_0_0;
struct ICollection_1_t9099;
extern Il2CppGenericClass ICollection_1_t9099_GenericClass;
TypeInfo ICollection_1_t9099_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9099_MethodInfos/* methods */
	, ICollection_1_t9099_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9099_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9099_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9099_0_0_0/* byval_arg */
	, &ICollection_1_t9099_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9099_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern Il2CppType IEnumerator_1_t7133_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50967_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50967_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9101_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7133_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50967_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9101_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50967_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9101_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9101_0_0_0;
extern Il2CppType IEnumerable_1_t9101_1_0_0;
struct IEnumerable_1_t9101;
extern Il2CppGenericClass IEnumerable_1_t9101_GenericClass;
TypeInfo IEnumerable_1_t9101_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9101_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9101_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9101_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9101_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9101_0_0_0/* byval_arg */
	, &IEnumerable_1_t9101_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9101_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9100_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo IList_1_get_Item_m50968_MethodInfo;
extern MethodInfo IList_1_set_Item_m50969_MethodInfo;
static PropertyInfo IList_1_t9100____Item_PropertyInfo = 
{
	&IList_1_t9100_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50968_MethodInfo/* get */
	, &IList_1_set_Item_m50969_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9100_PropertyInfos[] =
{
	&IList_1_t9100____Item_PropertyInfo,
	NULL
};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
static ParameterInfo IList_1_t9100_IList_1_IndexOf_m50970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1663_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50970_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50970_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9100_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9100_IList_1_IndexOf_m50970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50970_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
static ParameterInfo IList_1_t9100_IList_1_Insert_m50971_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1663_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50971_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50971_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9100_IList_1_Insert_m50971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50971_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9100_IList_1_RemoveAt_m50972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50972_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50972_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9100_IList_1_RemoveAt_m50972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50972_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9100_IList_1_get_Item_m50968_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
extern void* RuntimeInvoker_HashAlgorithmType_t1663_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50968_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50968_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9100_il2cpp_TypeInfo/* declaring_type */
	, &HashAlgorithmType_t1663_0_0_0/* return_type */
	, RuntimeInvoker_HashAlgorithmType_t1663_Int32_t123/* invoker_method */
	, IList_1_t9100_IList_1_get_Item_m50968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50968_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HashAlgorithmType_t1663_0_0_0;
static ParameterInfo IList_1_t9100_IList_1_set_Item_m50969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1663_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50969_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50969_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9100_IList_1_set_Item_m50969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50969_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9100_MethodInfos[] =
{
	&IList_1_IndexOf_m50970_MethodInfo,
	&IList_1_Insert_m50971_MethodInfo,
	&IList_1_RemoveAt_m50972_MethodInfo,
	&IList_1_get_Item_m50968_MethodInfo,
	&IList_1_set_Item_m50969_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9100_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9099_il2cpp_TypeInfo,
	&IEnumerable_1_t9101_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9100_0_0_0;
extern Il2CppType IList_1_t9100_1_0_0;
struct IList_1_t9100;
extern Il2CppGenericClass IList_1_t9100_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9100_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9100_MethodInfos/* methods */
	, IList_1_t9100_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9100_il2cpp_TypeInfo/* element_class */
	, IList_1_t9100_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9100_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9100_0_0_0/* byval_arg */
	, &IList_1_t9100_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9100_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7135_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo IEnumerator_1_get_Current_m50973_MethodInfo;
static PropertyInfo IEnumerator_1_t7135____Current_PropertyInfo = 
{
	&IEnumerator_1_t7135_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7135_PropertyInfos[] =
{
	&IEnumerator_1_t7135____Current_PropertyInfo,
	NULL
};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
extern void* RuntimeInvoker_SecurityCompressionType_t1675 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50973_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50973_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7135_il2cpp_TypeInfo/* declaring_type */
	, &SecurityCompressionType_t1675_0_0_0/* return_type */
	, RuntimeInvoker_SecurityCompressionType_t1675/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50973_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7135_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50973_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7135_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7135_0_0_0;
extern Il2CppType IEnumerator_1_t7135_1_0_0;
struct IEnumerator_1_t7135;
extern Il2CppGenericClass IEnumerator_1_t7135_GenericClass;
TypeInfo IEnumerator_1_t7135_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7135_MethodInfos/* methods */
	, IEnumerator_1_t7135_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7135_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7135_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7135_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7135_0_0_0/* byval_arg */
	, &IEnumerator_1_t7135_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7135_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_580.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5119_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_580MethodDeclarations.h"

extern TypeInfo SecurityCompressionType_t1675_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30973_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSecurityCompressionType_t1675_m40072_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityCompressionType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityCompressionType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSecurityCompressionType_t1675_m40072 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30969_MethodInfo;
 void InternalEnumerator_1__ctor_m30969 (InternalEnumerator_1_t5119 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970 (InternalEnumerator_1_t5119 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30973(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30973_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SecurityCompressionType_t1675_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30971_MethodInfo;
 void InternalEnumerator_1_Dispose_m30971 (InternalEnumerator_1_t5119 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30972_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30972 (InternalEnumerator_1_t5119 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30973 (InternalEnumerator_1_t5119 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSecurityCompressionType_t1675_m40072(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSecurityCompressionType_t1675_m40072_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5119____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5119, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5119____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5119, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5119_FieldInfos[] =
{
	&InternalEnumerator_1_t5119____array_0_FieldInfo,
	&InternalEnumerator_1_t5119____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5119____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5119_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5119____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5119_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5119_PropertyInfos[] =
{
	&InternalEnumerator_1_t5119____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5119____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5119_InternalEnumerator_1__ctor_m30969_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30969_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30969_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30969/* method */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5119_InternalEnumerator_1__ctor_m30969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30969_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970/* method */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30971_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30971_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30971/* method */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30971_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30972_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30972_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30972/* method */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30972_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
extern void* RuntimeInvoker_SecurityCompressionType_t1675 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30973_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30973_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30973/* method */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* declaring_type */
	, &SecurityCompressionType_t1675_0_0_0/* return_type */
	, RuntimeInvoker_SecurityCompressionType_t1675/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30973_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5119_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30969_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_MethodInfo,
	&InternalEnumerator_1_Dispose_m30971_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30972_MethodInfo,
	&InternalEnumerator_1_get_Current_m30973_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5119_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30970_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30972_MethodInfo,
	&InternalEnumerator_1_Dispose_m30971_MethodInfo,
	&InternalEnumerator_1_get_Current_m30973_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5119_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7135_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5119_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7135_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5119_0_0_0;
extern Il2CppType InternalEnumerator_1_t5119_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5119_GenericClass;
TypeInfo InternalEnumerator_1_t5119_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5119_MethodInfos/* methods */
	, InternalEnumerator_1_t5119_PropertyInfos/* properties */
	, InternalEnumerator_1_t5119_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5119_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5119_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5119_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5119_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5119_1_0_0/* this_arg */
	, InternalEnumerator_1_t5119_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5119_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5119)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9102_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo ICollection_1_get_Count_m50974_MethodInfo;
static PropertyInfo ICollection_1_t9102____Count_PropertyInfo = 
{
	&ICollection_1_t9102_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50975_MethodInfo;
static PropertyInfo ICollection_1_t9102____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9102_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9102_PropertyInfos[] =
{
	&ICollection_1_t9102____Count_PropertyInfo,
	&ICollection_1_t9102____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50974_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Count()
MethodInfo ICollection_1_get_Count_m50974_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50974_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50975_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50975_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50975_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
static ParameterInfo ICollection_1_t9102_ICollection_1_Add_m50976_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1675_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50976_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Add(T)
MethodInfo ICollection_1_Add_m50976_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9102_ICollection_1_Add_m50976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50976_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50977_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Clear()
MethodInfo ICollection_1_Clear_m50977_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50977_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
static ParameterInfo ICollection_1_t9102_ICollection_1_Contains_m50978_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1675_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50978_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Contains(T)
MethodInfo ICollection_1_Contains_m50978_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9102_ICollection_1_Contains_m50978_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50978_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionTypeU5BU5D_t5926_0_0_0;
extern Il2CppType SecurityCompressionTypeU5BU5D_t5926_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9102_ICollection_1_CopyTo_m50979_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionTypeU5BU5D_t5926_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50979_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50979_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9102_ICollection_1_CopyTo_m50979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50979_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
static ParameterInfo ICollection_1_t9102_ICollection_1_Remove_m50980_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1675_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50980_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Remove(T)
MethodInfo ICollection_1_Remove_m50980_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9102_ICollection_1_Remove_m50980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50980_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9102_MethodInfos[] =
{
	&ICollection_1_get_Count_m50974_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50975_MethodInfo,
	&ICollection_1_Add_m50976_MethodInfo,
	&ICollection_1_Clear_m50977_MethodInfo,
	&ICollection_1_Contains_m50978_MethodInfo,
	&ICollection_1_CopyTo_m50979_MethodInfo,
	&ICollection_1_Remove_m50980_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9104_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9102_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9104_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9102_0_0_0;
extern Il2CppType ICollection_1_t9102_1_0_0;
struct ICollection_1_t9102;
extern Il2CppGenericClass ICollection_1_t9102_GenericClass;
TypeInfo ICollection_1_t9102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9102_MethodInfos/* methods */
	, ICollection_1_t9102_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9102_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9102_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9102_0_0_0/* byval_arg */
	, &ICollection_1_t9102_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9102_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern Il2CppType IEnumerator_1_t7135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50981_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50981_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9104_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50981_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9104_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50981_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9104_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9104_0_0_0;
extern Il2CppType IEnumerable_1_t9104_1_0_0;
struct IEnumerable_1_t9104;
extern Il2CppGenericClass IEnumerable_1_t9104_GenericClass;
TypeInfo IEnumerable_1_t9104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9104_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9104_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9104_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9104_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9104_0_0_0/* byval_arg */
	, &IEnumerable_1_t9104_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9104_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9103_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo IList_1_get_Item_m50982_MethodInfo;
extern MethodInfo IList_1_set_Item_m50983_MethodInfo;
static PropertyInfo IList_1_t9103____Item_PropertyInfo = 
{
	&IList_1_t9103_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50982_MethodInfo/* get */
	, &IList_1_set_Item_m50983_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9103_PropertyInfos[] =
{
	&IList_1_t9103____Item_PropertyInfo,
	NULL
};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
static ParameterInfo IList_1_t9103_IList_1_IndexOf_m50984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1675_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50984_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50984_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9103_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9103_IList_1_IndexOf_m50984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50984_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
static ParameterInfo IList_1_t9103_IList_1_Insert_m50985_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1675_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50985_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50985_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9103_IList_1_Insert_m50985_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50985_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9103_IList_1_RemoveAt_m50986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50986_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50986_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9103_IList_1_RemoveAt_m50986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50986_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9103_IList_1_get_Item_m50982_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
extern void* RuntimeInvoker_SecurityCompressionType_t1675_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50982_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50982_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9103_il2cpp_TypeInfo/* declaring_type */
	, &SecurityCompressionType_t1675_0_0_0/* return_type */
	, RuntimeInvoker_SecurityCompressionType_t1675_Int32_t123/* invoker_method */
	, IList_1_t9103_IList_1_get_Item_m50982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50982_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SecurityCompressionType_t1675_0_0_0;
static ParameterInfo IList_1_t9103_IList_1_set_Item_m50983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1675_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50983_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50983_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9103_IList_1_set_Item_m50983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50983_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9103_MethodInfos[] =
{
	&IList_1_IndexOf_m50984_MethodInfo,
	&IList_1_Insert_m50985_MethodInfo,
	&IList_1_RemoveAt_m50986_MethodInfo,
	&IList_1_get_Item_m50982_MethodInfo,
	&IList_1_set_Item_m50983_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9103_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9102_il2cpp_TypeInfo,
	&IEnumerable_1_t9104_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9103_0_0_0;
extern Il2CppType IList_1_t9103_1_0_0;
struct IList_1_t9103;
extern Il2CppGenericClass IList_1_t9103_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9103_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9103_MethodInfos/* methods */
	, IList_1_t9103_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9103_il2cpp_TypeInfo/* element_class */
	, IList_1_t9103_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9103_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9103_0_0_0/* byval_arg */
	, &IList_1_t9103_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9103_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7137_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo IEnumerator_1_get_Current_m50987_MethodInfo;
static PropertyInfo IEnumerator_1_t7137____Current_PropertyInfo = 
{
	&IEnumerator_1_t7137_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50987_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7137_PropertyInfos[] =
{
	&IEnumerator_1_t7137____Current_PropertyInfo,
	NULL
};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
extern void* RuntimeInvoker_SecurityProtocolType_t1676 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50987_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50987_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7137_il2cpp_TypeInfo/* declaring_type */
	, &SecurityProtocolType_t1676_0_0_0/* return_type */
	, RuntimeInvoker_SecurityProtocolType_t1676/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50987_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7137_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50987_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7137_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7137_0_0_0;
extern Il2CppType IEnumerator_1_t7137_1_0_0;
struct IEnumerator_1_t7137;
extern Il2CppGenericClass IEnumerator_1_t7137_GenericClass;
TypeInfo IEnumerator_1_t7137_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7137_MethodInfos/* methods */
	, IEnumerator_1_t7137_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7137_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7137_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7137_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7137_0_0_0/* byval_arg */
	, &IEnumerator_1_t7137_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7137_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_581.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5120_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_581MethodDeclarations.h"

extern TypeInfo SecurityProtocolType_t1676_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30978_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSecurityProtocolType_t1676_m40083_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityProtocolType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityProtocolType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSecurityProtocolType_t1676_m40083 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30974_MethodInfo;
 void InternalEnumerator_1__ctor_m30974 (InternalEnumerator_1_t5120 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975 (InternalEnumerator_1_t5120 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30978(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30978_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SecurityProtocolType_t1676_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30976_MethodInfo;
 void InternalEnumerator_1_Dispose_m30976 (InternalEnumerator_1_t5120 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30977_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30977 (InternalEnumerator_1_t5120 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30978 (InternalEnumerator_1_t5120 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSecurityProtocolType_t1676_m40083(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSecurityProtocolType_t1676_m40083_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5120____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5120, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5120____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5120, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5120_FieldInfos[] =
{
	&InternalEnumerator_1_t5120____array_0_FieldInfo,
	&InternalEnumerator_1_t5120____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5120____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5120_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5120____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5120_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5120_PropertyInfos[] =
{
	&InternalEnumerator_1_t5120____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5120____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5120_InternalEnumerator_1__ctor_m30974_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30974_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30974_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30974/* method */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5120_InternalEnumerator_1__ctor_m30974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30974_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975/* method */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30976_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30976_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30976/* method */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30976_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30977_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30977_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30977/* method */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30977_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
extern void* RuntimeInvoker_SecurityProtocolType_t1676 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30978_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30978_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30978/* method */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* declaring_type */
	, &SecurityProtocolType_t1676_0_0_0/* return_type */
	, RuntimeInvoker_SecurityProtocolType_t1676/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30978_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5120_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30974_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_MethodInfo,
	&InternalEnumerator_1_Dispose_m30976_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30977_MethodInfo,
	&InternalEnumerator_1_get_Current_m30978_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5120_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30975_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30977_MethodInfo,
	&InternalEnumerator_1_Dispose_m30976_MethodInfo,
	&InternalEnumerator_1_get_Current_m30978_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5120_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7137_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5120_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7137_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5120_0_0_0;
extern Il2CppType InternalEnumerator_1_t5120_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5120_GenericClass;
TypeInfo InternalEnumerator_1_t5120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5120_MethodInfos/* methods */
	, InternalEnumerator_1_t5120_PropertyInfos/* properties */
	, InternalEnumerator_1_t5120_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5120_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5120_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5120_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5120_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5120_1_0_0/* this_arg */
	, InternalEnumerator_1_t5120_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5120_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5120)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9105_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo ICollection_1_get_Count_m50988_MethodInfo;
static PropertyInfo ICollection_1_t9105____Count_PropertyInfo = 
{
	&ICollection_1_t9105_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50989_MethodInfo;
static PropertyInfo ICollection_1_t9105____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9105_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50989_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9105_PropertyInfos[] =
{
	&ICollection_1_t9105____Count_PropertyInfo,
	&ICollection_1_t9105____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50988_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Count()
MethodInfo ICollection_1_get_Count_m50988_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50988_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50989_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50989_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50989_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
static ParameterInfo ICollection_1_t9105_ICollection_1_Add_m50990_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1676_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50990_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Add(T)
MethodInfo ICollection_1_Add_m50990_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9105_ICollection_1_Add_m50990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50990_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50991_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Clear()
MethodInfo ICollection_1_Clear_m50991_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50991_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
static ParameterInfo ICollection_1_t9105_ICollection_1_Contains_m50992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1676_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50992_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Contains(T)
MethodInfo ICollection_1_Contains_m50992_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9105_ICollection_1_Contains_m50992_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50992_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolTypeU5BU5D_t5927_0_0_0;
extern Il2CppType SecurityProtocolTypeU5BU5D_t5927_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9105_ICollection_1_CopyTo_m50993_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolTypeU5BU5D_t5927_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50993_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50993_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9105_ICollection_1_CopyTo_m50993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50993_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
static ParameterInfo ICollection_1_t9105_ICollection_1_Remove_m50994_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1676_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50994_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Remove(T)
MethodInfo ICollection_1_Remove_m50994_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9105_ICollection_1_Remove_m50994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50994_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9105_MethodInfos[] =
{
	&ICollection_1_get_Count_m50988_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50989_MethodInfo,
	&ICollection_1_Add_m50990_MethodInfo,
	&ICollection_1_Clear_m50991_MethodInfo,
	&ICollection_1_Contains_m50992_MethodInfo,
	&ICollection_1_CopyTo_m50993_MethodInfo,
	&ICollection_1_Remove_m50994_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9107_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9105_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9107_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9105_0_0_0;
extern Il2CppType ICollection_1_t9105_1_0_0;
struct ICollection_1_t9105;
extern Il2CppGenericClass ICollection_1_t9105_GenericClass;
TypeInfo ICollection_1_t9105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9105_MethodInfos/* methods */
	, ICollection_1_t9105_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9105_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9105_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9105_0_0_0/* byval_arg */
	, &ICollection_1_t9105_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9105_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern Il2CppType IEnumerator_1_t7137_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50995_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50995_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9107_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7137_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50995_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9107_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50995_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9107_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9107_0_0_0;
extern Il2CppType IEnumerable_1_t9107_1_0_0;
struct IEnumerable_1_t9107;
extern Il2CppGenericClass IEnumerable_1_t9107_GenericClass;
TypeInfo IEnumerable_1_t9107_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9107_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9107_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9107_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9107_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9107_0_0_0/* byval_arg */
	, &IEnumerable_1_t9107_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9107_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9106_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo IList_1_get_Item_m50996_MethodInfo;
extern MethodInfo IList_1_set_Item_m50997_MethodInfo;
static PropertyInfo IList_1_t9106____Item_PropertyInfo = 
{
	&IList_1_t9106_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50996_MethodInfo/* get */
	, &IList_1_set_Item_m50997_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9106_PropertyInfos[] =
{
	&IList_1_t9106____Item_PropertyInfo,
	NULL
};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
static ParameterInfo IList_1_t9106_IList_1_IndexOf_m50998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1676_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50998_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50998_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9106_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9106_IList_1_IndexOf_m50998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50998_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
static ParameterInfo IList_1_t9106_IList_1_Insert_m50999_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1676_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50999_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50999_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9106_IList_1_Insert_m50999_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50999_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9106_IList_1_RemoveAt_m51000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51000_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51000_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9106_IList_1_RemoveAt_m51000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51000_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9106_IList_1_get_Item_m50996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
extern void* RuntimeInvoker_SecurityProtocolType_t1676_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50996_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50996_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9106_il2cpp_TypeInfo/* declaring_type */
	, &SecurityProtocolType_t1676_0_0_0/* return_type */
	, RuntimeInvoker_SecurityProtocolType_t1676_Int32_t123/* invoker_method */
	, IList_1_t9106_IList_1_get_Item_m50996_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50996_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SecurityProtocolType_t1676_0_0_0;
static ParameterInfo IList_1_t9106_IList_1_set_Item_m50997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1676_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50997_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50997_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9106_IList_1_set_Item_m50997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50997_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9106_MethodInfos[] =
{
	&IList_1_IndexOf_m50998_MethodInfo,
	&IList_1_Insert_m50999_MethodInfo,
	&IList_1_RemoveAt_m51000_MethodInfo,
	&IList_1_get_Item_m50996_MethodInfo,
	&IList_1_set_Item_m50997_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9106_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9105_il2cpp_TypeInfo,
	&IEnumerable_1_t9107_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9106_0_0_0;
extern Il2CppType IList_1_t9106_1_0_0;
struct IList_1_t9106;
extern Il2CppGenericClass IList_1_t9106_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9106_MethodInfos/* methods */
	, IList_1_t9106_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9106_il2cpp_TypeInfo/* element_class */
	, IList_1_t9106_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9106_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9106_0_0_0/* byval_arg */
	, &IList_1_t9106_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9106_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7139_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo IEnumerator_1_get_Current_m51001_MethodInfo;
static PropertyInfo IEnumerator_1_t7139____Current_PropertyInfo = 
{
	&IEnumerator_1_t7139_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51001_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7139_PropertyInfos[] =
{
	&IEnumerator_1_t7139____Current_PropertyInfo,
	NULL
};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
extern void* RuntimeInvoker_ClientCertificateType_t1687 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51001_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51001_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7139_il2cpp_TypeInfo/* declaring_type */
	, &ClientCertificateType_t1687_0_0_0/* return_type */
	, RuntimeInvoker_ClientCertificateType_t1687/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51001_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7139_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51001_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7139_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7139_0_0_0;
extern Il2CppType IEnumerator_1_t7139_1_0_0;
struct IEnumerator_1_t7139;
extern Il2CppGenericClass IEnumerator_1_t7139_GenericClass;
TypeInfo IEnumerator_1_t7139_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7139_MethodInfos/* methods */
	, IEnumerator_1_t7139_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7139_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7139_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7139_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7139_0_0_0/* byval_arg */
	, &IEnumerator_1_t7139_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7139_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_582.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5121_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_582MethodDeclarations.h"

extern TypeInfo ClientCertificateType_t1687_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30983_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisClientCertificateType_t1687_m40094_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisClientCertificateType_t1687_m40094 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30979_MethodInfo;
 void InternalEnumerator_1__ctor_m30979 (InternalEnumerator_1_t5121 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980 (InternalEnumerator_1_t5121 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30983(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30983_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ClientCertificateType_t1687_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30981_MethodInfo;
 void InternalEnumerator_1_Dispose_m30981 (InternalEnumerator_1_t5121 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30982_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30982 (InternalEnumerator_1_t5121 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30983 (InternalEnumerator_1_t5121 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisClientCertificateType_t1687_m40094(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisClientCertificateType_t1687_m40094_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5121____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5121, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5121____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5121, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5121_FieldInfos[] =
{
	&InternalEnumerator_1_t5121____array_0_FieldInfo,
	&InternalEnumerator_1_t5121____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5121____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5121_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5121____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5121_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30983_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5121_PropertyInfos[] =
{
	&InternalEnumerator_1_t5121____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5121____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5121_InternalEnumerator_1__ctor_m30979_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30979_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30979_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30979/* method */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5121_InternalEnumerator_1__ctor_m30979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30979_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980/* method */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30981_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30981_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30981/* method */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30981_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30982_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30982_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30982/* method */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30982_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
extern void* RuntimeInvoker_ClientCertificateType_t1687 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30983_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30983_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30983/* method */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* declaring_type */
	, &ClientCertificateType_t1687_0_0_0/* return_type */
	, RuntimeInvoker_ClientCertificateType_t1687/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30983_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5121_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30979_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_MethodInfo,
	&InternalEnumerator_1_Dispose_m30981_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30982_MethodInfo,
	&InternalEnumerator_1_get_Current_m30983_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5121_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30980_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30982_MethodInfo,
	&InternalEnumerator_1_Dispose_m30981_MethodInfo,
	&InternalEnumerator_1_get_Current_m30983_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5121_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7139_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5121_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7139_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5121_0_0_0;
extern Il2CppType InternalEnumerator_1_t5121_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5121_GenericClass;
TypeInfo InternalEnumerator_1_t5121_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5121_MethodInfos/* methods */
	, InternalEnumerator_1_t5121_PropertyInfos/* properties */
	, InternalEnumerator_1_t5121_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5121_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5121_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5121_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5121_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5121_1_0_0/* this_arg */
	, InternalEnumerator_1_t5121_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5121_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5121)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9108_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo ICollection_1_get_Count_m51002_MethodInfo;
static PropertyInfo ICollection_1_t9108____Count_PropertyInfo = 
{
	&ICollection_1_t9108_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51002_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51003_MethodInfo;
static PropertyInfo ICollection_1_t9108____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9108_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51003_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9108_PropertyInfos[] =
{
	&ICollection_1_t9108____Count_PropertyInfo,
	&ICollection_1_t9108____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51002_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Count()
MethodInfo ICollection_1_get_Count_m51002_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51002_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51003_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51003_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51003_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
extern Il2CppType ClientCertificateType_t1687_0_0_0;
static ParameterInfo ICollection_1_t9108_ICollection_1_Add_m51004_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1687_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51004_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Add(T)
MethodInfo ICollection_1_Add_m51004_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9108_ICollection_1_Add_m51004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51004_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51005_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Clear()
MethodInfo ICollection_1_Clear_m51005_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51005_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
static ParameterInfo ICollection_1_t9108_ICollection_1_Contains_m51006_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1687_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51006_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Contains(T)
MethodInfo ICollection_1_Contains_m51006_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9108_ICollection_1_Contains_m51006_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51006_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateTypeU5BU5D_t1686_0_0_0;
extern Il2CppType ClientCertificateTypeU5BU5D_t1686_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9108_ICollection_1_CopyTo_m51007_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateTypeU5BU5D_t1686_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51007_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51007_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9108_ICollection_1_CopyTo_m51007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51007_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
static ParameterInfo ICollection_1_t9108_ICollection_1_Remove_m51008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1687_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51008_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Remove(T)
MethodInfo ICollection_1_Remove_m51008_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9108_ICollection_1_Remove_m51008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51008_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9108_MethodInfos[] =
{
	&ICollection_1_get_Count_m51002_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51003_MethodInfo,
	&ICollection_1_Add_m51004_MethodInfo,
	&ICollection_1_Clear_m51005_MethodInfo,
	&ICollection_1_Contains_m51006_MethodInfo,
	&ICollection_1_CopyTo_m51007_MethodInfo,
	&ICollection_1_Remove_m51008_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9110_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9108_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9110_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9108_0_0_0;
extern Il2CppType ICollection_1_t9108_1_0_0;
struct ICollection_1_t9108;
extern Il2CppGenericClass ICollection_1_t9108_GenericClass;
TypeInfo ICollection_1_t9108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9108_MethodInfos/* methods */
	, ICollection_1_t9108_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9108_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9108_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9108_0_0_0/* byval_arg */
	, &ICollection_1_t9108_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9108_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern Il2CppType IEnumerator_1_t7139_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51009_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51009_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9110_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7139_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51009_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9110_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51009_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9110_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9110_0_0_0;
extern Il2CppType IEnumerable_1_t9110_1_0_0;
struct IEnumerable_1_t9110;
extern Il2CppGenericClass IEnumerable_1_t9110_GenericClass;
TypeInfo IEnumerable_1_t9110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9110_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9110_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9110_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9110_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9110_0_0_0/* byval_arg */
	, &IEnumerable_1_t9110_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9110_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9109_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo IList_1_get_Item_m51010_MethodInfo;
extern MethodInfo IList_1_set_Item_m51011_MethodInfo;
static PropertyInfo IList_1_t9109____Item_PropertyInfo = 
{
	&IList_1_t9109_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51010_MethodInfo/* get */
	, &IList_1_set_Item_m51011_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9109_PropertyInfos[] =
{
	&IList_1_t9109____Item_PropertyInfo,
	NULL
};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
static ParameterInfo IList_1_t9109_IList_1_IndexOf_m51012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1687_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51012_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51012_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9109_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9109_IList_1_IndexOf_m51012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51012_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ClientCertificateType_t1687_0_0_0;
static ParameterInfo IList_1_t9109_IList_1_Insert_m51013_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1687_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51013_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51013_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9109_IList_1_Insert_m51013_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51013_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9109_IList_1_RemoveAt_m51014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51014_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51014_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9109_IList_1_RemoveAt_m51014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51014_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9109_IList_1_get_Item_m51010_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ClientCertificateType_t1687_0_0_0;
extern void* RuntimeInvoker_ClientCertificateType_t1687_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51010_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51010_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9109_il2cpp_TypeInfo/* declaring_type */
	, &ClientCertificateType_t1687_0_0_0/* return_type */
	, RuntimeInvoker_ClientCertificateType_t1687_Int32_t123/* invoker_method */
	, IList_1_t9109_IList_1_get_Item_m51010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51010_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ClientCertificateType_t1687_0_0_0;
static ParameterInfo IList_1_t9109_IList_1_set_Item_m51011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1687_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51011_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51011_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9109_IList_1_set_Item_m51011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51011_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9109_MethodInfos[] =
{
	&IList_1_IndexOf_m51012_MethodInfo,
	&IList_1_Insert_m51013_MethodInfo,
	&IList_1_RemoveAt_m51014_MethodInfo,
	&IList_1_get_Item_m51010_MethodInfo,
	&IList_1_set_Item_m51011_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9109_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9108_il2cpp_TypeInfo,
	&IEnumerable_1_t9110_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9109_0_0_0;
extern Il2CppType IList_1_t9109_1_0_0;
struct IList_1_t9109;
extern Il2CppGenericClass IList_1_t9109_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9109_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9109_MethodInfos/* methods */
	, IList_1_t9109_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9109_il2cpp_TypeInfo/* element_class */
	, IList_1_t9109_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9109_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9109_0_0_0/* byval_arg */
	, &IList_1_t9109_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9109_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7141_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo IEnumerator_1_get_Current_m51015_MethodInfo;
static PropertyInfo IEnumerator_1_t7141____Current_PropertyInfo = 
{
	&IEnumerator_1_t7141_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51015_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7141_PropertyInfos[] =
{
	&IEnumerator_1_t7141____Current_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeType_t1688_0_0_0;
extern void* RuntimeInvoker_HandshakeType_t1688 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51015_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51015_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7141_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeType_t1688_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeType_t1688/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51015_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7141_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51015_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7141_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7141_0_0_0;
extern Il2CppType IEnumerator_1_t7141_1_0_0;
struct IEnumerator_1_t7141;
extern Il2CppGenericClass IEnumerator_1_t7141_GenericClass;
TypeInfo IEnumerator_1_t7141_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7141_MethodInfos/* methods */
	, IEnumerator_1_t7141_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7141_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7141_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7141_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7141_0_0_0/* byval_arg */
	, &IEnumerator_1_t7141_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7141_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_583.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5122_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_583MethodDeclarations.h"

extern TypeInfo HandshakeType_t1688_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30988_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHandshakeType_t1688_m40105_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.HandshakeType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.HandshakeType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisHandshakeType_t1688_m40105 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30984_MethodInfo;
 void InternalEnumerator_1__ctor_m30984 (InternalEnumerator_1_t5122 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985 (InternalEnumerator_1_t5122 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30988(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30988_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HandshakeType_t1688_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30986_MethodInfo;
 void InternalEnumerator_1_Dispose_m30986 (InternalEnumerator_1_t5122 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30987_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30987 (InternalEnumerator_1_t5122 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30988 (InternalEnumerator_1_t5122 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisHandshakeType_t1688_m40105(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHandshakeType_t1688_m40105_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5122____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5122, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5122____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5122, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5122_FieldInfos[] =
{
	&InternalEnumerator_1_t5122____array_0_FieldInfo,
	&InternalEnumerator_1_t5122____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5122____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5122_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5122____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5122_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5122_PropertyInfos[] =
{
	&InternalEnumerator_1_t5122____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5122____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5122_InternalEnumerator_1__ctor_m30984_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30984_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30984_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30984/* method */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5122_InternalEnumerator_1__ctor_m30984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30984_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985/* method */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30986_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30986_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30986/* method */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30986_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30987_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30987_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30987/* method */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30987_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1688_0_0_0;
extern void* RuntimeInvoker_HandshakeType_t1688 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30988_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30988_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30988/* method */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeType_t1688_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeType_t1688/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30988_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5122_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30984_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_MethodInfo,
	&InternalEnumerator_1_Dispose_m30986_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30987_MethodInfo,
	&InternalEnumerator_1_get_Current_m30988_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5122_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30985_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30987_MethodInfo,
	&InternalEnumerator_1_Dispose_m30986_MethodInfo,
	&InternalEnumerator_1_get_Current_m30988_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5122_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7141_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5122_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7141_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5122_0_0_0;
extern Il2CppType InternalEnumerator_1_t5122_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5122_GenericClass;
TypeInfo InternalEnumerator_1_t5122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5122_MethodInfos/* methods */
	, InternalEnumerator_1_t5122_PropertyInfos/* properties */
	, InternalEnumerator_1_t5122_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5122_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5122_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5122_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5122_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5122_1_0_0/* this_arg */
	, InternalEnumerator_1_t5122_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5122_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5122)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9111_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo ICollection_1_get_Count_m51016_MethodInfo;
static PropertyInfo ICollection_1_t9111____Count_PropertyInfo = 
{
	&ICollection_1_t9111_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51016_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51017_MethodInfo;
static PropertyInfo ICollection_1_t9111____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9111_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51017_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9111_PropertyInfos[] =
{
	&ICollection_1_t9111____Count_PropertyInfo,
	&ICollection_1_t9111____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51016_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Count()
MethodInfo ICollection_1_get_Count_m51016_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51016_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51017_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51017_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51017_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1688_0_0_0;
extern Il2CppType HandshakeType_t1688_0_0_0;
static ParameterInfo ICollection_1_t9111_ICollection_1_Add_m51018_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1688_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51018_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Add(T)
MethodInfo ICollection_1_Add_m51018_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9111_ICollection_1_Add_m51018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51018_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51019_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Clear()
MethodInfo ICollection_1_Clear_m51019_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51019_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1688_0_0_0;
static ParameterInfo ICollection_1_t9111_ICollection_1_Contains_m51020_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1688_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51020_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Contains(T)
MethodInfo ICollection_1_Contains_m51020_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9111_ICollection_1_Contains_m51020_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51020_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeTypeU5BU5D_t5928_0_0_0;
extern Il2CppType HandshakeTypeU5BU5D_t5928_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9111_ICollection_1_CopyTo_m51021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeTypeU5BU5D_t5928_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51021_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51021_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9111_ICollection_1_CopyTo_m51021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51021_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1688_0_0_0;
static ParameterInfo ICollection_1_t9111_ICollection_1_Remove_m51022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1688_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51022_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Remove(T)
MethodInfo ICollection_1_Remove_m51022_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9111_ICollection_1_Remove_m51022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51022_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9111_MethodInfos[] =
{
	&ICollection_1_get_Count_m51016_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51017_MethodInfo,
	&ICollection_1_Add_m51018_MethodInfo,
	&ICollection_1_Clear_m51019_MethodInfo,
	&ICollection_1_Contains_m51020_MethodInfo,
	&ICollection_1_CopyTo_m51021_MethodInfo,
	&ICollection_1_Remove_m51022_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9113_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9111_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9113_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9111_0_0_0;
extern Il2CppType ICollection_1_t9111_1_0_0;
struct ICollection_1_t9111;
extern Il2CppGenericClass ICollection_1_t9111_GenericClass;
TypeInfo ICollection_1_t9111_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9111_MethodInfos/* methods */
	, ICollection_1_t9111_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9111_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9111_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9111_0_0_0/* byval_arg */
	, &ICollection_1_t9111_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9111_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern Il2CppType IEnumerator_1_t7141_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51023_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51023_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9113_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51023_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9113_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51023_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9113_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9113_0_0_0;
extern Il2CppType IEnumerable_1_t9113_1_0_0;
struct IEnumerable_1_t9113;
extern Il2CppGenericClass IEnumerable_1_t9113_GenericClass;
TypeInfo IEnumerable_1_t9113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9113_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9113_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9113_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9113_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9113_0_0_0/* byval_arg */
	, &IEnumerable_1_t9113_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9113_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9112_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo IList_1_get_Item_m51024_MethodInfo;
extern MethodInfo IList_1_set_Item_m51025_MethodInfo;
static PropertyInfo IList_1_t9112____Item_PropertyInfo = 
{
	&IList_1_t9112_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51024_MethodInfo/* get */
	, &IList_1_set_Item_m51025_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9112_PropertyInfos[] =
{
	&IList_1_t9112____Item_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeType_t1688_0_0_0;
static ParameterInfo IList_1_t9112_IList_1_IndexOf_m51026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1688_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51026_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51026_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9112_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9112_IList_1_IndexOf_m51026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51026_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HandshakeType_t1688_0_0_0;
static ParameterInfo IList_1_t9112_IList_1_Insert_m51027_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1688_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51027_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51027_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9112_IList_1_Insert_m51027_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51027_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9112_IList_1_RemoveAt_m51028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51028_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51028_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9112_IList_1_RemoveAt_m51028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51028_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9112_IList_1_get_Item_m51024_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HandshakeType_t1688_0_0_0;
extern void* RuntimeInvoker_HandshakeType_t1688_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51024_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51024_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9112_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeType_t1688_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeType_t1688_Int32_t123/* invoker_method */
	, IList_1_t9112_IList_1_get_Item_m51024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51024_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HandshakeType_t1688_0_0_0;
static ParameterInfo IList_1_t9112_IList_1_set_Item_m51025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1688_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51025_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51025_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9112_IList_1_set_Item_m51025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51025_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9112_MethodInfos[] =
{
	&IList_1_IndexOf_m51026_MethodInfo,
	&IList_1_Insert_m51027_MethodInfo,
	&IList_1_RemoveAt_m51028_MethodInfo,
	&IList_1_get_Item_m51024_MethodInfo,
	&IList_1_set_Item_m51025_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9112_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9111_il2cpp_TypeInfo,
	&IEnumerable_1_t9113_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9112_0_0_0;
extern Il2CppType IList_1_t9112_1_0_0;
struct IList_1_t9112;
extern Il2CppGenericClass IList_1_t9112_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9112_MethodInfos/* methods */
	, IList_1_t9112_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9112_il2cpp_TypeInfo/* element_class */
	, IList_1_t9112_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9112_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9112_0_0_0/* byval_arg */
	, &IList_1_t9112_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9112_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7143_il2cpp_TypeInfo;

// System.SerializableAttribute
#include "mscorlib_System_SerializableAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>
extern MethodInfo IEnumerator_1_get_Current_m51029_MethodInfo;
static PropertyInfo IEnumerator_1_t7143____Current_PropertyInfo = 
{
	&IEnumerator_1_t7143_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51029_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7143_PropertyInfos[] =
{
	&IEnumerator_1_t7143____Current_PropertyInfo,
	NULL
};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51029_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51029_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7143_il2cpp_TypeInfo/* declaring_type */
	, &SerializableAttribute_t1741_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51029_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7143_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51029_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7143_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7143_0_0_0;
extern Il2CppType IEnumerator_1_t7143_1_0_0;
struct IEnumerator_1_t7143;
extern Il2CppGenericClass IEnumerator_1_t7143_GenericClass;
TypeInfo IEnumerator_1_t7143_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7143_MethodInfos/* methods */
	, IEnumerator_1_t7143_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7143_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7143_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7143_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7143_0_0_0/* byval_arg */
	, &IEnumerator_1_t7143_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7143_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.SerializableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_584.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5123_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.SerializableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_584MethodDeclarations.h"

extern TypeInfo SerializableAttribute_t1741_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30993_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSerializableAttribute_t1741_m40116_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.SerializableAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.SerializableAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSerializableAttribute_t1741_m40116(__this, p0, method) (SerializableAttribute_t1741 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.SerializableAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.SerializableAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.SerializableAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.SerializableAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5123____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5123, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5123____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5123, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5123_FieldInfos[] =
{
	&InternalEnumerator_1_t5123____array_0_FieldInfo,
	&InternalEnumerator_1_t5123____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5123____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5123_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5123____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5123_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30993_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5123_PropertyInfos[] =
{
	&InternalEnumerator_1_t5123____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5123____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5123_InternalEnumerator_1__ctor_m30989_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30989_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30989_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5123_InternalEnumerator_1__ctor_m30989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30989_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.SerializableAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30991_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30991_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30991_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30992_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.SerializableAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30992_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30992_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30993_GenericMethod;
// T System.Array/InternalEnumerator`1<System.SerializableAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30993_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* declaring_type */
	, &SerializableAttribute_t1741_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30993_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5123_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30989_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_MethodInfo,
	&InternalEnumerator_1_Dispose_m30991_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30992_MethodInfo,
	&InternalEnumerator_1_get_Current_m30993_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30992_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30991_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5123_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30990_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30992_MethodInfo,
	&InternalEnumerator_1_Dispose_m30991_MethodInfo,
	&InternalEnumerator_1_get_Current_m30993_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5123_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7143_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5123_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7143_il2cpp_TypeInfo, 7},
};
extern TypeInfo SerializableAttribute_t1741_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5123_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30993_MethodInfo/* Method Usage */,
	&SerializableAttribute_t1741_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSerializableAttribute_t1741_m40116_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5123_0_0_0;
extern Il2CppType InternalEnumerator_1_t5123_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5123_GenericClass;
TypeInfo InternalEnumerator_1_t5123_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5123_MethodInfos/* methods */
	, InternalEnumerator_1_t5123_PropertyInfos/* properties */
	, InternalEnumerator_1_t5123_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5123_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5123_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5123_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5123_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5123_1_0_0/* this_arg */
	, InternalEnumerator_1_t5123_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5123_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5123_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5123)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9114_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.SerializableAttribute>
extern MethodInfo ICollection_1_get_Count_m51030_MethodInfo;
static PropertyInfo ICollection_1_t9114____Count_PropertyInfo = 
{
	&ICollection_1_t9114_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51030_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51031_MethodInfo;
static PropertyInfo ICollection_1_t9114____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9114_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51031_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9114_PropertyInfos[] =
{
	&ICollection_1_t9114____Count_PropertyInfo,
	&ICollection_1_t9114____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51030_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m51030_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51030_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51031_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51031_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51031_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
extern Il2CppType SerializableAttribute_t1741_0_0_0;
static ParameterInfo ICollection_1_t9114_ICollection_1_Add_m51032_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1741_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51032_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Add(T)
MethodInfo ICollection_1_Add_m51032_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9114_ICollection_1_Add_m51032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51032_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51033_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Clear()
MethodInfo ICollection_1_Clear_m51033_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51033_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
static ParameterInfo ICollection_1_t9114_ICollection_1_Contains_m51034_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1741_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51034_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m51034_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9114_ICollection_1_Contains_m51034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51034_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttributeU5BU5D_t5452_0_0_0;
extern Il2CppType SerializableAttributeU5BU5D_t5452_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9114_ICollection_1_CopyTo_m51035_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttributeU5BU5D_t5452_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51035_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51035_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9114_ICollection_1_CopyTo_m51035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51035_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
static ParameterInfo ICollection_1_t9114_ICollection_1_Remove_m51036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1741_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51036_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m51036_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9114_ICollection_1_Remove_m51036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51036_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9114_MethodInfos[] =
{
	&ICollection_1_get_Count_m51030_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51031_MethodInfo,
	&ICollection_1_Add_m51032_MethodInfo,
	&ICollection_1_Clear_m51033_MethodInfo,
	&ICollection_1_Contains_m51034_MethodInfo,
	&ICollection_1_CopyTo_m51035_MethodInfo,
	&ICollection_1_Remove_m51036_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9116_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9114_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9116_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9114_0_0_0;
extern Il2CppType ICollection_1_t9114_1_0_0;
struct ICollection_1_t9114;
extern Il2CppGenericClass ICollection_1_t9114_GenericClass;
TypeInfo ICollection_1_t9114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9114_MethodInfos/* methods */
	, ICollection_1_t9114_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9114_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9114_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9114_0_0_0/* byval_arg */
	, &ICollection_1_t9114_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9114_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>
extern Il2CppType IEnumerator_1_t7143_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51037_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51037_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9116_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7143_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51037_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9116_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51037_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9116_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9116_0_0_0;
extern Il2CppType IEnumerable_1_t9116_1_0_0;
struct IEnumerable_1_t9116;
extern Il2CppGenericClass IEnumerable_1_t9116_GenericClass;
TypeInfo IEnumerable_1_t9116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9116_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9116_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9116_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9116_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9116_0_0_0/* byval_arg */
	, &IEnumerable_1_t9116_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9116_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9115_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.SerializableAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.SerializableAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.SerializableAttribute>
extern MethodInfo IList_1_get_Item_m51038_MethodInfo;
extern MethodInfo IList_1_set_Item_m51039_MethodInfo;
static PropertyInfo IList_1_t9115____Item_PropertyInfo = 
{
	&IList_1_t9115_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51038_MethodInfo/* get */
	, &IList_1_set_Item_m51039_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9115_PropertyInfos[] =
{
	&IList_1_t9115____Item_PropertyInfo,
	NULL
};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
static ParameterInfo IList_1_t9115_IList_1_IndexOf_m51040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1741_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51040_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.SerializableAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51040_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9115_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9115_IList_1_IndexOf_m51040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51040_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SerializableAttribute_t1741_0_0_0;
static ParameterInfo IList_1_t9115_IList_1_Insert_m51041_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1741_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51041_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51041_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9115_IList_1_Insert_m51041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51041_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9115_IList_1_RemoveAt_m51042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51042_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51042_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9115_IList_1_RemoveAt_m51042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51042_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9115_IList_1_get_Item_m51038_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SerializableAttribute_t1741_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51038_GenericMethod;
// T System.Collections.Generic.IList`1<System.SerializableAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51038_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9115_il2cpp_TypeInfo/* declaring_type */
	, &SerializableAttribute_t1741_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9115_IList_1_get_Item_m51038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51038_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SerializableAttribute_t1741_0_0_0;
static ParameterInfo IList_1_t9115_IList_1_set_Item_m51039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1741_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51039_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51039_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9115_IList_1_set_Item_m51039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51039_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9115_MethodInfos[] =
{
	&IList_1_IndexOf_m51040_MethodInfo,
	&IList_1_Insert_m51041_MethodInfo,
	&IList_1_RemoveAt_m51042_MethodInfo,
	&IList_1_get_Item_m51038_MethodInfo,
	&IList_1_set_Item_m51039_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9115_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9114_il2cpp_TypeInfo,
	&IEnumerable_1_t9116_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9115_0_0_0;
extern Il2CppType IList_1_t9115_1_0_0;
struct IList_1_t9115;
extern Il2CppGenericClass IList_1_t9115_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9115_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9115_MethodInfos/* methods */
	, IList_1_t9115_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9115_il2cpp_TypeInfo/* element_class */
	, IList_1_t9115_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9115_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9115_0_0_0/* byval_arg */
	, &IList_1_t9115_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9115_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
