﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
struct KeyValuePair_2_t4138;
// Vuforia.WordResult
struct WordResult_t747;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m23688 (KeyValuePair_2_t4138 * __this, int32_t ___key, WordResult_t747 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::get_Key()
 int32_t KeyValuePair_2_get_Key_m23689 (KeyValuePair_2_t4138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m23690 (KeyValuePair_2_t4138 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::get_Value()
 WordResult_t747 * KeyValuePair_2_get_Value_m23691 (KeyValuePair_2_t4138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m23692 (KeyValuePair_2_t4138 * __this, WordResult_t747 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::ToString()
 String_t* KeyValuePair_2_ToString_m23693 (KeyValuePair_2_t4138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
