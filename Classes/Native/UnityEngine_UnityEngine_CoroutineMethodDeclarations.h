﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t381;
struct Coroutine_t381_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
 void Coroutine__ctor_m5585 (Coroutine_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
 void Coroutine_ReleaseCoroutine_m5586 (Coroutine_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
 void Coroutine_Finalize_m5587 (Coroutine_t381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t381_marshal(const Coroutine_t381& unmarshaled, Coroutine_t381_marshaled& marshaled);
void Coroutine_t381_marshal_back(const Coroutine_t381_marshaled& marshaled, Coroutine_t381& unmarshaled);
void Coroutine_t381_marshal_cleanup(Coroutine_t381_marshaled& marshaled);
