﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t241;
// UnityEngine.GameObject
struct GameObject_t29;
// System.String
struct String_t;

// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::get_gameObject()
 GameObject_t29 * RaycastResult_get_gameObject_m952 (RaycastResult_t241 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.RaycastResult::set_gameObject(UnityEngine.GameObject)
 void RaycastResult_set_gameObject_m953 (RaycastResult_t241 * __this, GameObject_t29 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.RaycastResult::get_isValid()
 bool RaycastResult_get_isValid_m954 (RaycastResult_t241 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.RaycastResult::Clear()
 void RaycastResult_Clear_m955 (RaycastResult_t241 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.EventSystems.RaycastResult::ToString()
 String_t* RaycastResult_ToString_m956 (RaycastResult_t241 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
