﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RollABall
struct RollABall_t218;

// System.Void RollABall::.ctor()
 void RollABall__ctor_m768 (RollABall_t218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollABall::Start()
 void RollABall_Start_m769 (RollABall_t218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollABall::Update()
 void RollABall_Update_m770 (RollABall_t218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollABall::LateUpdate()
 void RollABall_LateUpdate_m771 (RollABall_t218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RollABall::Main()
 void RollABall_Main_m772 (RollABall_t218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
