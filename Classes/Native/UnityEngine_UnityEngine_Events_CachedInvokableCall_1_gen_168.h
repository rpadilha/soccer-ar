﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_170.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody2D>
struct CachedInvokableCall_1_t4842  : public InvokableCall_1_t4843
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody2D>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
