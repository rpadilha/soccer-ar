﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ArrayTypeMismatchException
struct ArrayTypeMismatchException_t2243;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ArrayTypeMismatchException::.ctor()
 void ArrayTypeMismatchException__ctor_m12720 (ArrayTypeMismatchException_t2243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArrayTypeMismatchException::.ctor(System.String)
 void ArrayTypeMismatchException__ctor_m12721 (ArrayTypeMismatchException_t2243 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArrayTypeMismatchException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void ArrayTypeMismatchException__ctor_m12722 (ArrayTypeMismatchException_t2243 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
