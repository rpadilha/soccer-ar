﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>
struct InternalEnumerator_1_t5181;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Configuration.Assemblies.AssemblyVersionCompatibility
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionComp.h"

// System.Void System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31279 (InternalEnumerator_1_t5181 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31280 (InternalEnumerator_1_t5181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>::Dispose()
 void InternalEnumerator_1_Dispose_m31281 (InternalEnumerator_1_t5181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31282 (InternalEnumerator_1_t5181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Configuration.Assemblies.AssemblyVersionCompatibility>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31283 (InternalEnumerator_1_t5181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
