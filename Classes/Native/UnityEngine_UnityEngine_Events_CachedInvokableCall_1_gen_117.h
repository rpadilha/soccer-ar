﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_119.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceAbstractBehaviour>
struct CachedInvokableCall_1_t3902  : public InvokableCall_1_t3903
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
