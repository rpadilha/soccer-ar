﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.GenericTypeParameterBuilder
struct GenericTypeParameterBuilder_t1961;
// System.Type
struct Type_t;
// System.Reflection.Assembly
struct Assembly_t1556;
// System.String
struct String_t;
// System.Reflection.Module
struct Module_t1755;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1245;
// System.Reflection.Binder
struct Binder_t1215;
// System.Type[]
struct TypeU5BU5D_t922;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1216;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1759;
// System.Reflection.EventInfo
struct EventInfo_t1756;
// System.Reflection.FieldInfo
struct FieldInfo_t1757;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t140;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// System.Reflection.PropertyInfo
struct PropertyInfo_t1758;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Globalization.CultureInfo
struct CultureInfo_t1218;
// System.String[]
struct StringU5BU5D_t862;
// System.Exception
struct Exception_t151;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsSubclassOf(System.Type)
 bool GenericTypeParameterBuilder_IsSubclassOf_m11183 (GenericTypeParameterBuilder_t1961 * __this, Type_t * ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.Emit.GenericTypeParameterBuilder::GetAttributeFlagsImpl()
 int32_t GenericTypeParameterBuilder_GetAttributeFlagsImpl_m11184 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
 ConstructorInfo_t1245 * GenericTypeParameterBuilder_GetConstructorImpl_m11185 (GenericTypeParameterBuilder_t1961 * __this, int32_t ___bindingAttr, Binder_t1215 * ___binder, int32_t ___callConvention, TypeU5BU5D_t922* ___types, ParameterModifierU5BU5D_t1216* ___modifiers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetConstructors(System.Reflection.BindingFlags)
 ConstructorInfoU5BU5D_t1759* GenericTypeParameterBuilder_GetConstructors_m11186 (GenericTypeParameterBuilder_t1961 * __this, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetEvent(System.String,System.Reflection.BindingFlags)
 EventInfo_t1756 * GenericTypeParameterBuilder_GetEvent_m11187 (GenericTypeParameterBuilder_t1961 * __this, String_t* ___name, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetField(System.String,System.Reflection.BindingFlags)
 FieldInfo_t1757 * GenericTypeParameterBuilder_GetField_m11188 (GenericTypeParameterBuilder_t1961 * __this, String_t* ___name, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::GetInterfaces()
 TypeU5BU5D_t922* GenericTypeParameterBuilder_GetInterfaces_m11189 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetMethods(System.Reflection.BindingFlags)
 MethodInfoU5BU5D_t140* GenericTypeParameterBuilder_GetMethods_m11190 (GenericTypeParameterBuilder_t1961 * __this, int32_t ___bindingAttr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
 MethodInfo_t141 * GenericTypeParameterBuilder_GetMethodImpl_m11191 (GenericTypeParameterBuilder_t1961 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1215 * ___binder, int32_t ___callConvention, TypeU5BU5D_t922* ___types, ParameterModifierU5BU5D_t1216* ___modifiers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
 PropertyInfo_t1758 * GenericTypeParameterBuilder_GetPropertyImpl_m11192 (GenericTypeParameterBuilder_t1961 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1215 * ___binder, Type_t * ___returnType, TypeU5BU5D_t922* ___types, ParameterModifierU5BU5D_t1216* ___modifiers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::HasElementTypeImpl()
 bool GenericTypeParameterBuilder_HasElementTypeImpl_m11193 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsAssignableFrom(System.Type)
 bool GenericTypeParameterBuilder_IsAssignableFrom_m11194 (GenericTypeParameterBuilder_t1961 * __this, Type_t * ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsInstanceOfType(System.Object)
 bool GenericTypeParameterBuilder_IsInstanceOfType_m11195 (GenericTypeParameterBuilder_t1961 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsArrayImpl()
 bool GenericTypeParameterBuilder_IsArrayImpl_m11196 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsByRefImpl()
 bool GenericTypeParameterBuilder_IsByRefImpl_m11197 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsPointerImpl()
 bool GenericTypeParameterBuilder_IsPointerImpl_m11198 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsPrimitiveImpl()
 bool GenericTypeParameterBuilder_IsPrimitiveImpl_m11199 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsValueTypeImpl()
 bool GenericTypeParameterBuilder_IsValueTypeImpl_m11200 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.GenericTypeParameterBuilder::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
 Object_t * GenericTypeParameterBuilder_InvokeMember_m11201 (GenericTypeParameterBuilder_t1961 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1215 * ___binder, Object_t * ___target, ObjectU5BU5D_t130* ___args, ParameterModifierU5BU5D_t1216* ___modifiers, CultureInfo_t1218 * ___culture, StringU5BU5D_t862* ___namedParameters, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::GetElementType()
 Type_t * GenericTypeParameterBuilder_GetElementType_m11202 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_UnderlyingSystemType()
 Type_t * GenericTypeParameterBuilder_get_UnderlyingSystemType_m11203 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Emit.GenericTypeParameterBuilder::get_Assembly()
 Assembly_t1556 * GenericTypeParameterBuilder_get_Assembly_m11204 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_AssemblyQualifiedName()
 String_t* GenericTypeParameterBuilder_get_AssemblyQualifiedName_m11205 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_BaseType()
 Type_t * GenericTypeParameterBuilder_get_BaseType_m11206 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_FullName()
 String_t* GenericTypeParameterBuilder_get_FullName_m11207 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsDefined(System.Type,System.Boolean)
 bool GenericTypeParameterBuilder_IsDefined_m11208 (GenericTypeParameterBuilder_t1961 * __this, Type_t * ___attributeType, bool ___inherit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.GenericTypeParameterBuilder::GetCustomAttributes(System.Boolean)
 ObjectU5BU5D_t130* GenericTypeParameterBuilder_GetCustomAttributes_m11209 (GenericTypeParameterBuilder_t1961 * __this, bool ___inherit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.GenericTypeParameterBuilder::GetCustomAttributes(System.Type,System.Boolean)
 ObjectU5BU5D_t130* GenericTypeParameterBuilder_GetCustomAttributes_m11210 (GenericTypeParameterBuilder_t1961 * __this, Type_t * ___attributeType, bool ___inherit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_Name()
 String_t* GenericTypeParameterBuilder_get_Name_m11211 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_Namespace()
 String_t* GenericTypeParameterBuilder_get_Namespace_m11212 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.GenericTypeParameterBuilder::get_Module()
 Module_t1755 * GenericTypeParameterBuilder_get_Module_m11213 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_DeclaringType()
 Type_t * GenericTypeParameterBuilder_get_DeclaringType_m11214 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_ReflectedType()
 Type_t * GenericTypeParameterBuilder_get_ReflectedType_m11215 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.Reflection.Emit.GenericTypeParameterBuilder::get_TypeHandle()
 RuntimeTypeHandle_t1754  GenericTypeParameterBuilder_get_TypeHandle_m11216 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::GetGenericArguments()
 TypeU5BU5D_t922* GenericTypeParameterBuilder_GetGenericArguments_m11217 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::GetGenericTypeDefinition()
 Type_t * GenericTypeParameterBuilder_GetGenericTypeDefinition_m11218 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_ContainsGenericParameters()
 bool GenericTypeParameterBuilder_get_ContainsGenericParameters_m11219 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_IsGenericParameter()
 bool GenericTypeParameterBuilder_get_IsGenericParameter_m11220 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_IsGenericType()
 bool GenericTypeParameterBuilder_get_IsGenericType_m11221 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_IsGenericTypeDefinition()
 bool GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m11222 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.GenericTypeParameterBuilder::not_supported()
 Exception_t151 * GenericTypeParameterBuilder_not_supported_m11223 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::ToString()
 String_t* GenericTypeParameterBuilder_ToString_m11224 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::Equals(System.Object)
 bool GenericTypeParameterBuilder_Equals_m11225 (GenericTypeParameterBuilder_t1961 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.GenericTypeParameterBuilder::GetHashCode()
 int32_t GenericTypeParameterBuilder_GetHashCode_m11226 (GenericTypeParameterBuilder_t1961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::MakeGenericType(System.Type[])
 Type_t * GenericTypeParameterBuilder_MakeGenericType_m11227 (GenericTypeParameterBuilder_t1961 * __this, TypeU5BU5D_t922* ___typeArguments, MethodInfo* method) IL2CPP_METHOD_ATTR;
