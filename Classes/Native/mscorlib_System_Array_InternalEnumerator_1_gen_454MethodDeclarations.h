﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.CollisionFlags>
struct InternalEnumerator_1_t4823;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.CollisionFlags
#include "UnityEngine_UnityEngine_CollisionFlags.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CollisionFlags>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29103 (InternalEnumerator_1_t4823 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CollisionFlags>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29104 (InternalEnumerator_1_t4823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CollisionFlags>::Dispose()
 void InternalEnumerator_1_Dispose_m29105 (InternalEnumerator_1_t4823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CollisionFlags>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29106 (InternalEnumerator_1_t4823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.CollisionFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29107 (InternalEnumerator_1_t4823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
