﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ShimEnumerator_t4204  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::host_enumerator
	Enumerator_t4196  ___host_enumerator_0;
};
