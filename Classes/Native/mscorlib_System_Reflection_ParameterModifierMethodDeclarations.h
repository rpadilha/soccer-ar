﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t1217;
struct ParameterModifier_t1217_marshaled;

void ParameterModifier_t1217_marshal(const ParameterModifier_t1217& unmarshaled, ParameterModifier_t1217_marshaled& marshaled);
void ParameterModifier_t1217_marshal_back(const ParameterModifier_t1217_marshaled& marshaled, ParameterModifier_t1217& unmarshaled);
void ParameterModifier_t1217_marshal_cleanup(ParameterModifier_t1217_marshaled& marshaled);
