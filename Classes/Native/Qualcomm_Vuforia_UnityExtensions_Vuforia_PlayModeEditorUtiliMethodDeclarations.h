﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
struct NullPlayModeEditorUtility_t676;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String)
 void NullPlayModeEditorUtility_DisplayDialog_m3133 (NullPlayModeEditorUtility_t676 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamProfile/ProfileCollection Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::LoadAndParseWebcamProfiles(System.String)
 ProfileCollection_t677  NullPlayModeEditorUtility_LoadAndParseWebcamProfiles_m3134 (NullPlayModeEditorUtility_t676 * __this, String_t* ___path, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::RestartPlayMode()
 void NullPlayModeEditorUtility_RestartPlayMode_m3135 (NullPlayModeEditorUtility_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::.ctor()
 void NullPlayModeEditorUtility__ctor_m3136 (NullPlayModeEditorUtility_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
