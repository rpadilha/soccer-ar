﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Prop>
struct EqualityComparer_1_t4295;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Prop>
struct EqualityComparer_1_t4295  : public Object_t
{
};
struct EqualityComparer_1_t4295_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Prop>::_default
	EqualityComparer_1_t4295 * ____default_0;
};
