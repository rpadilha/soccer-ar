﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable>
struct EqualityComparer_1_t3651;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable>
struct EqualityComparer_1_t3651  : public Object_t
{
};
struct EqualityComparer_1_t3651_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable>::_default
	EqualityComparer_1_t3651 * ____default_0;
};
