﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1620;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct KeyGeneratedEventHandler_t1621  : public MulticastDelegate_t373
{
};
