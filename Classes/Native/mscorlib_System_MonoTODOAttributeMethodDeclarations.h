﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1776;
// System.String
struct String_t;

// System.Void System.MonoTODOAttribute::.ctor()
 void MonoTODOAttribute__ctor_m9930 (MonoTODOAttribute_t1776 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
 void MonoTODOAttribute__ctor_m9931 (MonoTODOAttribute_t1776 * __this, String_t* ___comment, MethodInfo* method) IL2CPP_METHOD_ATTR;
