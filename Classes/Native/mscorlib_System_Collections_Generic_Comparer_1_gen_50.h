﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.GUILayoutEntry>
struct Comparer_1_t4711;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.GUILayoutEntry>
struct Comparer_1_t4711  : public Object_t
{
};
struct Comparer_1_t4711_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.GUILayoutEntry>::_default
	Comparer_1_t4711 * ____default_0;
};
