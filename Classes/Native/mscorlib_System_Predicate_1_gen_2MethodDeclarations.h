﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct Predicate_1_t856;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
 void Predicate_1__ctor_m5014 (Predicate_1_t856 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::Invoke(T)
 bool Predicate_1_Invoke_m23185 (Predicate_1_t856 * __this, TrackableResultData_t684  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
 Object_t * Predicate_1_BeginInvoke_m23186 (Predicate_1_t856 * __this, TrackableResultData_t684  ___obj, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
 bool Predicate_1_EndInvoke_m23187 (Predicate_1_t856 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
