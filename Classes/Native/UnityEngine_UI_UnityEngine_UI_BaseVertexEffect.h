﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t344;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.UI.BaseVertexEffect
struct BaseVertexEffect_t459  : public UIBehaviour_t238
{
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::m_Graphic
	Graphic_t344 * ___m_Graphic_2;
};
