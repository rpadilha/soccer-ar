﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.Word>
struct Comparer_1_t4169;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.Word>
struct Comparer_1_t4169  : public Object_t
{
};
struct Comparer_1_t4169_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Word>::_default
	Comparer_1_t4169 * ____default_0;
};
